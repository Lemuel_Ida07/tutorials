<?php
	include('connect.php');
	session_start();
	$_SESSION['pnumber'] = "";
	
	if(!isset($_POST['request']))
	{
		$projectnumber = $_SESSION['pnumber'];
	}
	else
	{
		$projectnumber = $_POST['request'];
		$_SESSION['pnumber'] = $projectnumber;
	}
?>

<style>
	#menu_item_logo1
	{
		background-color:#f2f2f2;
	}
	#menu_item_logo1:hover
	{
		background-color:#f2f2f2;
	}
	#tab1
	{
		color:#515151;
		font-weight: bold;
		text-shadow: none;
	}
</style>

<strong> Billable Projects</strong>
				
				<input name="ProjectNumberBill" id="pNumberB" autocomplete="Off" type="text" list="ProjectNumberB" placeholder="Project Number">
				
				<datalist id="ProjectNumberB" required="required">
					<?php
						 $designer_id = $_SESSION["ID"];
						 $team_id = $_SESSION["team_id"];
						 $sql = "SELECT ID,Project_Number ,Project_Name FROM project WHERE Remarks = 1 ORDER BY ProjectNumber ";
						 $result = mysqli_query($conn,$sql);
						 if(mysqli_num_rows($result) > 0)
						 {
							 while($rows = mysqli_fetch_assoc($result))
							 {
								 echo "<option value='".$rows["ID"]."'> ".$rows["Project_Number"]." - ".$rows["Project_Name"]."</option>";
							 }
						 } 
					?>
				</datalist>

<label> Please select a ticket number. </label>
<input type="text" list="ticketN"  name="TicketNumber" id="ticket"  placeholder="Ticket Number">
	<datalist id="ticketN" name="ticketN">
		<?php
			$designer_id = $_SESSION["ID"];
			$team_id = $_SESSION["team_id"];
	
			$sql = "SELECT Ticket_Number FROM ticket WHERE Project_ID = ";
			
			$result = mysqli_query($conn,$sql);
			if(mysqli_num_rows($result) > 0)
			{
				while($rows = mysqli_fetch_assoc($result))
				{
					echo "<option value='".$rows["Ticket_Number"]."'> ".$rows["Ticket_Number"]."</option>";
				}
			}
		?>
	</datalist>
	
	<label id="label1"> From </label><label id="label2"> To </label><br/> 
				<input id="inptext1" type="time" name="TimeIn" required/><input id="inptext2" type="time" name="TimeOut"required/>
				<input type="submit" value="Add" name="add" id="addbtn"/>			
				
<script>
	$(document).ready(function(){
$('#ticket').on('change',function(){
    var value = $(this).val();
        $.ajax(
        {
            url:'retrieve.php',
            type:'post',
            data:'ticket='+value,
            success:function(data)
            {   
                $('#updata').html(data);
            },
        });
});
});

</script>