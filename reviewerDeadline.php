<?php
include('connect.php');
include('phpScript.php');
session_start();	
$Trade = $_SESSION["Trade"];
$month_string = $month_array[date("m")];
$tradeString = "";
$current_day = date("d");
$readsql = "SELECT Trade FROM user WHERE ID = ".$_SESSION['ID']."";
$result = mysqli_query($conn,$readsql);
$row = mysqli_fetch_assoc($result);
$tradeString = $row['Trade'];

$trade = array("C","E","A","AU","S","P","M","FP");
								 
/* GET LAST DAYS FROM PREVIOUS  MONTH */
	$NoLastDays = 0;
	$base_month = 4; //default month (don't cahnge)
	$base_year = 2018; //default year (don't cahnge)
	$month_selected = (int)date("m") -1;
	$year_selected = (int)date("Y");
	for($date_count = $base_month; $year_selected >= $base_year; $date_count++)
	{
		$NoLastDays = getPrevMonthNoLastDays($base_month,$base_year,$NoLastDays);	 
		$base_month ++;
		if($base_month > 12)
		{
			$base_year++;
			$base_month = 1 ;
		}						
		//stops the loop				
		if($base_year >= $year_selected && $base_month > $month_selected)
		{
			break;
		}
	}

	$month_selected = (int)date("m");
?>

<style>


	.pop_tbl1{
		width:550px;
	}

	.pop_tbl1 thead tr {
		text-align:left;
	}

	#mainform .pop_tbl1 .project_search, #mainform .pop_tbl1 select,#mainform .pop_tbl1 tbody tr td input[type=number]{
		padding:4px;
		border:solid 1px #ccc;
		border-radius:4px;
		width:100%;
		margin:0px;
	}

	#mainform textarea{
		width:390px;
		margin:0px;
	}

	#mainform h3{
		margin:0px;
	}

</style>
		
<!-- POP UP -->
<div id="pop_up_bg" onmousedown="hideList('datalist');">
	<div id="pop_up" style="width:600px;height:70%; overflow-y:auto;" onmousedown="hideList('datalist');">
		<div id="close_button" onClick="closePop()"> <label style="font-family: Tahoma, Geneva, sans-serif; font-size: 24px; color: #FFFFFF;">x</label> </div>

		<h1 id="date"> April 5, 2018 </h1>

		<table class="pop_tbl1">
			<thead>
				<tr>
					<th><h3><i> Please select a project </i></h3></th>
          <th><h3><i> Budget </i></h3></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<input type="text" name="searchbox" class="project_search" id="searchbox" onclick="showList('datalist')" placeholder="Select a project." autocomplete="off" onkeyup="filter('searchbox','datalist')" />
					</td>
          <td style="width:150px;">
            <input type="number" min="1" max="8" style="width:48%;" id="budget" value="1"/>
            <input id="assign_designer" class="width-48pc btn-normal bg-blue white" type="submit" value="Assign" />
            <input id="edit_designer" class="width-20pc btn-normal bg-green white" type="submit" value="Edit" />
            <input id="delete_designer" class="width-20pc btn-normal bg-red white" type="submit" value="Delete" />
          </td>
				</tr>
			</tbody>
		</table>
		<input type="text" id="deadline" style="display:none;" />
		<input type="text" name="projectid" id="projectid" style="display:none;" />
		<input type="text" name="internaldl_id" id="internaldl_id" style="display:none;" />
		<input type="text" name="designerid" id="designerid" style="display:none;" />
		<div id="datalist" style="margin-left:0px;width:27%;height:auto;overflow:auto;max-height:20%;">
			<!-- data from rGetProject.php -->
		</div>
		<div id="review_checklist">
			<table class="simple_table">
				<thead>
					<tr>
						<th> <h3> Requirements </h3> </th>
						<th style="width:150px;"> <h3>Expected Sheets</h3> </th>
					</tr>
				</thead>
				<tbody id="drawing_sheets">
					<!-- data from reviewAssignDes.php -->
				</tbody>
			</table>
		</div>
			<table class="pop_tbl1">
				<thead>
					<tr>
						<th><h3 class='assign_activity'><i> Assign Activity </i></h3></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						
					</tr>
				</tbody>
			</table>
	
		<hr />
		<div id="table_container" style="min-height:100px;overflow-y:auto;">
			<table class="general_table width-100pc">
        <thead>
					<tr>
						<th> Project </th>
						<th> Activity </th>
						<th> Budget </th>
					</tr>
				</thead>
				<tbody id="dnotiftable">
					<!-- Data from dnotif_table.php and submit.php when editted or deleted-->
				</tbody>
			</table>
		</div>
	</div>
</div>
<div id="bg_bg">
</div>

<!-- END POP UP -->

<div id="info" onmousedown="hideList('designer_list')">
<p id="monthnumber" style="display: none;float:left;"><?php echo $month_selected; ?></p>
	<label class="h1 float-left"> <span id="currentMonth"> <?php echo $month_string; ?> </span><span id="currentYear">2018</span> </label>
  <br />
	<input type="text" name="designer_name" id="designer_name" class="designer_name" placeholder="Select Designer"  onmouseup="showList('designer_list')" onkeyup="filter('designer_name','designer_list')" />
	<input type="text" name="designer_id" id="designer_id" style="display:none;" />
	<div id="designer_list">		
		<?php
		$sql = "SELECT ID,CONCAT(Firstname, ' ' , Middlename ,' ', Lastname) AS Name FROM user WHERE Status = 'Active' AND User_Type = 0 AND Trade = '$tradeString' ORDER BY Firstname";
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			while($rows = mysqli_fetch_assoc($result))
			{
				echo "<h4 id='Des".$rows['ID']."' onmousedown='passDataDes".$rows['ID']."();'> ".utf8_decode($rows['Name'])." </h4>";
				echo "<script>
						
						function passDataDes".$rows['ID']."()
						{
							document.getElementById('designer_name').value = document.getElementById('Des".$rows['ID']."').innerHTML;
							document.getElementById('designer_id').value='".$rows['ID']."';
							document.getElementById('designerid').value='".$rows['ID']."';
						}
						$(document).ready(function(){
								$('#Des".$rows['ID']."').on('mousedown',function(){
									var value = $(this).val();
									$.ajax(
									{
										url:'main_reviewerDeadline.php',
										type:'post',
										data:'month_selected=$month_selected&year_selected=$year_selected&tradeString=$tradeString&NoLastDays=$NoLastDays&designer_id='+document.getElementById('designerid').value,
										success:function(data)
										{
											$('#calendar').html(data);
										},
									});
								});
							});
					  </script>";
			}
		}
        ?>
	</div>
<fieldset id="fset_legends">
	<legend> <label class="title">Legends</label> </legend>
	<table width="100%" class="legends">
		<thead>
			<tr>
				<th><label class="h3">Internal Deadline</label></th>
				<th><label class="h3"> Budget Allocation </label></th>
			</tr>
		</thead>
		<tbody>
			<tr>																	 
				<td> <div id="arryn_indl" class="event" > House Arryn </div> </td>
				<td> <div id="arryn_budget" class="event" > Budget </div> </td>
			</tr>
			<tr>																 
				<td> <div id="stark_indl" class="event" > House Stark </div> </td>
				<td> <div id="stark_budget" class="event" > Budget </div> </td>
			</tr>
			<tr>																 
				<td> <div id="lannister_indl" class="event" > House Lannister </div> </td>
				<td> <div id="lannister_budget" class="event" > Budget </div> </td>
			</tr>
			<tr>																 
				<td> <div id="tyrell_indl" class="event" > House Tyrell </div> </td>
				<td> <div id="tyrell_budget" class="event" > Budget </div> </td>
			</tr>	 
			<tr>																 
				<td> <div id="baratheon_indl" class="event" > House Baratheon </div> </td>
				<td> <div id="baratheon_budget" class="event" > Budget </div> </td>
			</tr>	 
			<tr>																 
				<td> <div id="targaryen_indl" class="event" > House Targaryen </div> </td>
				<td> <div id="targaryen_budget" class="event" > Budget </div> </td>
			</tr>
		</tbody>
	</table>
	</fieldset>
	<fieldset id="fset_legends">
<legend> <label class="title"> Deadline Details </label> </legend>
	</fieldset>
<div id="content_tray">
</div>
<input type="text" id="extraDaysPrevMonth" style="display:none" />
<input type="text" id="one" style="display:none" />	
<input type="text" id="tradeString" style="display:none" value="<?php echo $tradeString; ?>"/>	
<input type="text" id="NoLastDays" style="display:none" value="<?php echo $NoLastDays; ?>"/>	
<input type="text" id="dnotif_id" style="display:none;" />
</div>

<div id="list" onmousedown="hideList('designer_list')" >


<style>
	p
	{
		text-align:center;
	}
</style>
<div id="dates">
<table id="calendar" style="height:500px;">
	<tr>
			<th id="weekends" class="title"> Sunday </th>
			<th id="weekdays" class="title"> Monday </th>
			<th id="weekdays" class="title"> Tuesday </th>
			<th id="weekdays" class="title"> Wednesday </th>
			<th id="weekdays" class="title"> Thursday </th>
			<th id="weekdays" class="title"> Friday </th>
			<th id="weekends" class="title"> Saturday </th>
	</tr>
	<?php
	/* GET LAST DAYS FROM PREVIOUS  MONTH */
	$NoLastDays = 0;
	$base_month = 4; //default month (don't cahnge)
	$base_year = 2018; //default year (don't cahnge)
	$month_selected = (int)date("m") -1;
	$year_selected = (int)date("Y");
	for($date_count = $base_month; $year_selected >= $base_year; $date_count++)
	{
		$NoLastDays = getPrevMonthNoLastDays($base_month,$base_year,$NoLastDays);	 
		$base_month ++;
		if($base_month > 12)
		{
			$base_year++;
			$base_month = 1 ;
		}						
		//stops the loop				
		if($base_year >= $year_selected && $base_month > $month_selected)
		{
			break;
		}
	}

	/* END OF GET LAST DAYS FROM PREV MONTH */
	$month_selected = (int)date("m");
	$in_dead_bg = array ("none", "#A74CAB", "00B0F0", "FF9999", "#92D050", "FFFF99;color:#34495e", "#A5A5A5");
	$ex_dead_bg = array ("none", "#4c0056", "#005878", "FF0000", "#497E10", "FFC000;color:#34495e", "#545454");
	$totaldays=cal_days_in_month(CAL_GREGORIAN,$month_selected,$year_selected);
	$loop = ($NoLastDays + $totaldays) / 7;
	$extradays = ($NoLastDays + $totaldays) % 7;
	if($extradays != 0)
		$loop ++;
	$daycounter = $NoLastDays - (($NoLastDays * 2)-1);
	$dayweek = 7 -$NoLastDays;
	$count4 = 0; // para sa count ng kung ilan yung event per day
	for($count = 1; $count <= $loop; $count ++)
	{
		echo"<tr>";
		for($count2 = $daycounter; $count2 <= $dayweek; $count2 ++)
		{
			$sql2 = "SELECT internal_deadline.ID,
									project.Project_Name,
									project.Team_ID,
									project.Project_Number,
									internal_deadline.Trade,
									internal_deadline.Day_ID,
									internal_deadline.Phase
								FROM project
								INNER JOIN external_deadline 
									ON project.ID = external_deadline.Project_ID
                INNER JOIN internal_deadline 
									ON external_deadline.ID = internal_deadline.Externaldl_ID
								WHERE 
									internal_deadline.Day_ID = $count2 
									AND internal_deadline.Month_ID = $month_selected 
									AND internal_deadline.Year_ID = $year_selected and internal_deadline.Trade ='$tradeString'";	
      $result2 = mysqli_query($conn,$sql2);
			
			if(mysqli_num_rows($result2) > 0)
			{
		
				if($NoLastDays > 0 || $count2 == $daycounter || $count2 == $daycounter + 6)
				{
					if($count2 <= 0)
					{
						echo "<td id='days' >  </td>";
					}
					else
					{
						echo "<td id='days' > $count2 </td>";
					}
					$NoLastDays = $NoLastDays-1;
				}
				else{
					echo "<td id='days'";
							if($count2 == $current_day)
							{				
								echo "style = 'border:dotted 3px #3396d9;'";
							}
              $readsql3 = "SELECT MAX(Day_ID) as Day_ID  FROM internal_deadline WHERE Month_ID = $month_selected and Year_ID = $year_selected and Trade ='$tradeString'";
              $result3 = mysqli_query($conn,$readsql3);
              $row = mysqli_fetch_assoc($result3);
              $lastinternaldl = $row['Day_ID'];
              if($count2 <= $lastinternaldl)
              {
                if($count2 >= $current_day)
                echo "onClick='checkDesigner();  getDate($count2,$month_selected,$year_selected)'";
              }	
							echo "> $count2
						<div class='$count2' id='daily_container'>";
					
					// SHOW INTERNAL DEADLINES
					while($rows2 = mysqli_fetch_assoc($result2))
					{
						$INcurID = $rows2['Team_ID']; // get TEAM ID
						$font_color = "white";
						
						if($INcurID == 5)
            {
              $font_color = "black";
            }
						echo "<p style='background-color: $in_dead_bg[$INcurID]; color:$font_color;' id='indeadline".$rows2['ID']."' class='event'>  ".$rows2['Project_Name']." - ".$rows2["Phase"]." (".$rows2['Trade'].") </p>";	
					
						echo "<script> 
											$(document).ready(function(){
												$('#indeadline".$rows2['ID']."').on('click',function(){
													var value = $(this).val();
														$.ajax(
														{
															url:'deadline.php',
															type:'post',
															data:'internal_details=true&internaldl_id=".$rows2['ID']."&phase=".$rows2['Phase']."',
															success:function(data)
															{   
																$('#content_tray').html(data);
															},
															error:function(data)
															{
																	alert(data);
															}
														});
												});
												});
										  </script>";
					}	
					echo"</div>
						</td>";
				}

			}
			
			else
			{
				if($NoLastDays > 0 || $count2 == $daycounter || $count2 == $daycounter + 6)
				{
					if($count2 <= 0)
					{
						echo "<td id='days' >  </td>";
					}
					else
					{
						echo "<td id='days' > $count2 </td>";
					}
					$NoLastDays = $NoLastDays-1;
				}
				else{
				echo "<td id='days'";
							if($count2 == $current_day)
							{				
								echo "style = 'border:dotted 3px #3396d9;'";
							}
              $readsql3 = "SELECT MAX(Day_ID) as Day_ID  FROM internal_deadline WHERE Month_ID = $month_selected and Year_ID = $year_selected and Trade ='$tradeString'";
              $result3 = mysqli_query($conn,$readsql3);
              $row = mysqli_fetch_assoc($result3);
              $lastinternaldl = $row['Day_ID'];
              if($count2 <= $lastinternaldl)
              {
                if($count2 >= $current_day)
                echo "onClick='checkDesigner();  getDate($count2,$month_selected,$year_selected)'";
              }	
							echo "> $count2
						<div class='$count2' id='daily_container'>";
         
								echo " </div>";
				}
			}
		}
		echo "</tr>";
		$daycounter = $count2;
		if($extradays!=0)
		{
			if($count < $loop - 2)
			{
				$dayweek = $count2 + 6; 
			}
			else
			{
				$dayweek = $totaldays;
				echo "<script> document.getElementById('extraDaysPrevMonth').value ='$totaldays'; </script>";
			}
		}
		else
		{
			$dayweek = $count2 + 6; 
		}
	}
    ?>

</table>
</div>
</div>
<script>
var string_month = ["index","January","February","March","April","May","June","July","August","September","October","November","December"];

var requirements = [];
var expected_sheets = [];

$(document).ready(function(){
	$('#review_checklist').hide();
	$('#edit_designer').hide();
	$('#delete_designer').hide();
	$('.assign_activity').hide();
	$('.assign_activity_text').hide();

	$("input[type=number]").on('keydown',function (e) {
		var keyCode = e.which;
		// restrict alpha characters
		if (keyCode == 173 ||keyCode == 61 || keyCode == 59 || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 106 && keyCode <= 111) || (keyCode >= 186 && keyCode <= 222)) {
			e.preventDefault();
		}
	});
	/* hide and Unhide the textarea */
	$('#activity_cbox').on('click',function(){
		if($('#activity_cbox').prop("checked"))
		{
			$('.assign_activity').show();
			$('.assign_activity_text').show();
		}
		else
		{
			$('.assign_activity').hide();
			$('.assign_activity_text').hide();
		}
			$('.assign_activity_text').val("Sheets");
	});
});

function resetAssignPop(){
	$('#review_checklist').hide();
	$('.assign_activity').hide();
	$('.assign_activity_text').hide();
	$('#code').html("");
	$('#budget').val("");
	$('#activity_cbox').prop("checked",false);
	$('#assign_designer').show();
	$('#edit_designer').hide();
	$('#delete_designer').hide();
}

function openPop() {
	document.getElementById('pop_up').style.visibility = "visible";
	document.getElementById('pop_up_bg').style.visibility = "visible";
	document.getElementById('bg_bg').style.visibility = "visible";
}

function closePop() {
		document.getElementById('pop_up').style.visibility = "hidden";
		document.getElementById('pop_up_bg').style.visibility = "hidden";
		document.getElementById('bg_bg').style.visibility = "hidden";

		// RESET ALL DATA
		clearPop();
		resetAssignPop();
}

function clearPop()
{															 
		$('#searchbox').val("");
}

function getDate(id,month,year)
{
	$('#date').html(string_month[month]+" "+id+", "+year);
	$('#deadline').val(year+"-"+month+"-"+id);
}

function showList(id) {
	var element = document.getElementById(id).style.display = "block";
}

	function hideList(id) {
	if(document.getElementById(id).style.display == "block")
	var element = document.getElementById(id).style.display = "none";
	}

function filter(inputid,divid) {
	var input, filter, list, count;
	input = document.getElementById(inputid);
	filter = input.value.toUpperCase();
	div = document.getElementById(divid);
	list = div.getElementsByTagName("h4");
	for(count = 0; count < list.length; count ++)
	{
		if (list[count].innerHTML.toUpperCase().indexOf(filter) > -1)
		{
			list[count].style.display = "";
		} else {
			list[count].style.display = "none";
		}
	}
}
$(document).ready(function(){
	/* Assign Clicked */
	$('#assign_designer').on('click',function(){
		var value = $(this).val();
		/* gets requirements in the table and place it into array */
		$('.simple_table #drawing_sheets tr td:nth-child(1)').each(function(e){
			var value = $(this).html();
			requirements.push(value);
		});
		
		/* gets expected sheets in the table and place it into array */
		$('.simple_table #drawing_sheets tr td:nth-child(2) input[type=number]').each(function(e){
			var value = $(this).val();
			expected_sheets.push(value);
		});

		$.ajax(
		{
			beforeSend:function(data){
				var send = false;
				/* returns true if expected sheets is not all 0 */
				$.each(expected_sheets,function(key,value){
					if(value > 0)
					{
						send = true;
						return false;
					}
					if(value == 0)
					{
						
							send = false;
					}
				});
				/* returns true if activity_cbox is checked and is not empty */
				if($('#activity_cbox').prop("checked") == true && $('#assign_activity_text').val() != "")
				{
					send = true;
				}
				return send;
			},
			url:'reviewSubmit.php',
			type:'post',
			data:{ 
							assign_designer:value,
	            project_id: $('#projectid').val(),
	            internaldl_id: $('#internaldl_id').val(),
	            designer_id: $('#designer_id').val(),
	            budget: $('#budget').val(),
	            deadline: $('#deadline').val(),					
							activity: $('#activity').val(),
							requirement:requirements,
							expected:expected_sheets,
							code: $('#code').val()
						},
			success:function(data)
			{
				refreshUpdate();
	    },
			error:function(data)
			{
				alert(data);
			}
		});
	});

	/* Edit Clicked */
	$('#edit_designer').on('click',function(){
		/* gets requirements in the table and place it into array */
		$('.simple_table #drawing_sheets tr td:nth-child(1)').each(function(e){
			var value = $(this).html();
			requirements.push(value);
		});
		
		/* gets expected sheets in the table and place it into array */
		$('.simple_table #drawing_sheets tr td:nth-child(2) input[type=number]').each(function(e){
			var value = $(this).val();
			expected_sheets.push(value);
		});
		$.ajax(
		{
			beforeSend:function(){
				return confirm('Are you sure you want to edit?');
			},
			url:'reviewSubmit.php',
			type:'post',
			data:
					{
						edit_designer: true,
						dnotif_id: $('#dnotif_id').val(),
					  budget: $('#budget').val(),
						activity: $('.assign_activity_text').val(),
						requirement:requirements,
						expected:expected_sheets,
						code: $('#code').val()
					},
			success:function(data){
				refreshUpdate();
			},
			error:function(data)
			{
				alert(data);
			}
		});
	});

	/* Delete Clicked */
	$('#delete_designer').on('click',function(){
		$.ajax(
		{
			beforeSend:function(){
				return confirm('Are you sure you want to delete?');
			},
			url:'reviewSubmit.php',
			type:'post',
			data:'delete_designer=true'+
					 '&dnotif_id='+$('#dnotif_id').val(),
			success:function(data){
				refreshUpdate();
			},
		});
	});
});

function refreshUpdate(){
	var date  = new Date($('#deadline').val());
				var selected_day = date.getDate();
				var selected_month = date.getMonth() + 1;
				var selected_year = date.getFullYear();
				/* Refresh budget list */
				$.ajax(
				{
					url:'dnotif_table.php',
					type:'post',
					data:'designer_id='+$('#designerid').val()+
							 '&trade='+$('#tradeString').val()+
							 '&day_dn='+selected_day,
					success:function(data)
					{
						$('#dnotiftable').html(data);
					},
				});

				/* Refresh calendar */
				$.ajax(
				{
					url:'main_reviewerDeadline.php',
					type:'post',
					data:'month_selected='+selected_month+
							 '&year_selected='+selected_year+
							 '&tradeString='+$('#tradeString').val()+	
							 '&NoLastDays='+$('#NoLastDays').val()+
							 '&designer_id='+$('#designer_id').val(),
					success:function(data)
					{
						$('#calendar').html(data);
					},
				});
				requirements = [];
				expected_sheets = [];

				alert('Update Success!');
}

$("#designer_name").keypress(function (e) {
			$("#designer_list").show();
			e.stopPropagation();
		});

$("#designer_name").click(function (e) {
			$("#designer_list").show();
			e.stopPropagation();
		});

		$("#designer_list").click(function (e) {
			e.stopPropagation();
		});

		$(document).click(function () {
			$("#designer_list").hide();
		});
    
    function checkDesigner(){
      if ($.trim($('#designer_name').val()) != '')
      {
        alert("Please select designer!");
      }
      else{
        openPop();
        $('#designer_name').val("");
      }
    }
    
      $(".event").click(function (e) {
        e.stopPropagation();
      });
</script>