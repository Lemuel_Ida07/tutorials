<?php
	session_start();
	include("connect.php");
	$curr_date = date("Y-m-d");
	$designer_id = $_POST["designer_id"];
	$trade = $_SESSION["Trade"];
	
	$time = new DateTime('', new DateTimeZone('Asia/Manila'));
	$hour = $time->format('H');
	//cad manager
	if($_SESSION["User_Type"] == 8)
	{
		$sql = "SELECT CONCAT(user.Firstname , ' ' , user.Middlename , ' ' , user.Lastname) AS Designer,
									user.Trade,
									project.ID AS Project_ID,
									project.Project_Number,
									project.Project_Name,
									review_logs.ID,	 
									review_logs.Ticket_Number,
									review_logs.Review_Date,
									review_logs.Review_Date,
									activity_logs.Activity
						FROM review_logs
							INNER JOIN project
								ON project.ID = review_logs.Project_ID
							INNER JOIN user
								ON user.ID = review_logs.Designer_ID
							INNER JOIN activity_logs
								ON activity_logs.ID = review_logs.ActivityLogs_ID
						WHERE review_logs.Designer_ID = $designer_id 
						ORDER BY review_logs.Designer_ID";
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			while($rows = mysqli_fetch_assoc($result))
			{
				echo "<tr>															
								<td> ".$rows["Designer"]." </td>
								<td> ".$rows["Trade"]." </td>
								<td> ".$rows["Project_Number"]." ".$rows["Project_Name"]." </td> 
								<td> ".$rows["Activity"]." </td>
								<td> ".$rows["Review_Date"]." </td>
								<td><input type='submit' class='btn-normal bg-blue white' value='Assign' onclick='TradeToPop".$rows["ID"]."();showPop()'/></td>
								<script>
									function TradeToPop".$rows["ID"]."()
									{																								 
										$('#trade_for_pop').val('".$rows["Trade"]."');
										$('#pop_reviewlogs_id').val('".$rows["ID"]."');	 
										$('#pop_project_id').val('".$rows["Project_ID"]."');
								
										$('#pop_deadline').val('".getNextDay($hour)."');
										$.ajax(
										{
											url:'cadmngrSubmit.php',
											type:'post',
											data:'get_cadtech=true'+
													 '&trade='+$('#trade_for_pop').val(),
											success:function(data)
											{
												$('#cad_tech_list').html(data);
											}
										});
										$.ajax(
										{
											url:'cadmngrSubmit.php',
											type:'post',
											data:'get_assigned=true'+
													 '&reviewlogs_id=".$rows["ID"]."',
											success:function(data)
											{
												$('#assigned_list').html(data);
											}
										});
									}
								</script>
							</tr>";
			}
		}
		else
		{
			echo "<tr>
							<td colspan='5'><label>No Results! </label></td>

						</tr>";
		}
					
	}
	else
	{
		$sql = "SELECT CONCAT(user.Firstname , ' ' , user.Middlename , ' ' , user.Lastname) AS Designer,
									project.Project_Number,
									project.Project_Name,
									review_logs.Ticket_Number,
									review_logs.Comments,
									review_logs.Design_Accuracy,
 									review_logs.Timeliness,
									review_logs.Completeness,
									review_logs.Rating,
									review_logs.Review_Date,
									review_logs.Time_In,
									review_logs.Time_Out
						FROM review_logs
							INNER JOIN project
								ON project.ID = review_logs.Project_ID
							INNER JOIN user
								ON user.ID = review_logs.Designer_ID
						WHERE review_logs.Reviewer_ID = ".$_SESSION["ID"]."
								AND review_logs.Designer_ID = $designer_id 
								AND user.Trade = '$trade'
						ORDER BY review_logs.Designer_ID";
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			while($rows = mysqli_fetch_assoc($result))
			{
				echo "<tr>
								<td> ".$rows["Designer"]." </td>
								<td> ".$rows["Project_Number"]." ".$rows["Project_Name"]." </td>
								<td> ".$rows["Ticket_Number"]." </td>
								<td> ".$rows["Comments"]." </td>
								<td> ".$rows["Design_Accuracy"]." </td>
								<td> ".$rows["Timeliness"]." </td>
								<td> ".$rows["Completeness"]." </td>
								<td> ".$rows["Rating"]." </td>
								<td> ".$rows["Review_Date"]." </td>
								<td> ".$rows["Time_In"]." </td>
								<td> ".$rows["Time_Out"]." </td>
							</tr>";
			}
		}
		else
		{
			echo "<tr>
							<td colspan='8'><label>No Results! </label></td>
						</tr>";
		}
	}
	
	function getNextDay($hour)
	{												 
		$year = (int)date("Y");
		$month = (int)date("m");
		$day = (int)date("d");	
			
		$days_no=cal_days_in_month(CAL_GREGORIAN,$month,$year);	 
		
		for($count = 1; $count <= $days_no; $count++)
		{
			$day_count =0;
			if($day == $count && $day != $days_no)
			{
				if($day == 1 || $day == 7 || $day == 7)
{}			
			}	
		}

		if($month < 10)
		$month = "0".$month;

		if($hour >= 15)
		$day++;

		if($day < 10)
		$day = "0".$day;

		$curr_date = $year."-".$month."-".$day;

		return $curr_date;	
	}				