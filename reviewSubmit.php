<?php
	session_start();
	include('connect.php');
	$curr_date = date("Y-m-d");
	if(isset($_POST["add_review"]))
	{
		$activity_logs_id = $_POST["activity_logs_id"];
		$reviewer_id=$_SESSION["ID"];
		$project_id = $_POST["project_id"];
		$ticket_number = $_POST["ticket_number"];
		$designer_id = $_POST["designer_id"];
		$comments = $_POST["comments"];
		$design_accuracy = $_POST["design_accuracy"];
		$timeliness = $_POST["timeliness"];
		$completeness = $_POST["completeness"];
		$rating = $_POST["rating"];
		$review_date = $curr_date;
		$time_in = $_POST["time_in"];
		$time_out = $_POST["time_out"];
		$dnotif_id = $_POST["dnotif_id"];
		$sql = "INSERT INTO review_logs(Reviewer_ID, 
					 Project_ID, 
					 Ticket_Number,
					 Designer_ID,
					 Comments,
					 Design_Accuracy,
					 Timeliness,
           Completeness, 
					 Rating, 
					 Review_Date,
					 Time_In,
					 Time_Out,
					 ActivityLogs_ID)
				 VALUES($reviewer_id,
								$project_id,
								'$ticket_number',
								$designer_id,
								'$comments',
								$design_accuracy,
								$timeliness,
								$completeness,
								$rating,
								'$review_date',
								'$time_in',
								'$time_out',
								$activity_logs_id)";
								echo $sql;
		if(mysqli_query($conn,$sql))
		{
			$updatesql = "UPDATE designer_notif SET Status = 'For Drafting' WHERE ID = $dnotif_id";
			if(mysqli_query($conn,$updatesql))
			{}
		}
	}
	
	/* REJECTED */
	if(isset($_POST["incomplete"]))
	{
		$dnotif_id = $_POST["dnotif_id"];
		$updatesql = "UPDATE designer_notif 
										SET Status = 'Incomplete'
									WHERE ID = $dnotif_id";
		if(mysqli_query($conn,$updatesql))
		{

		}
	}
  
	/* Assign Designer */
	if(isset($_POST["assign_designer"]))
	{
		$project_id = $_POST["project_id"];
		$internaldl_id = $_POST["internaldl_id"];
		$designer_id = $_POST["designer_id"];
		$total_budget = $_POST["total_budget"];
		$deadline = $_POST["deadline"];
		$activity = $_POST["activity"];
    $budgets = $_POST["budgets"];
    $reviewchecklist_id = $_POST["reviewchecklist_id"];
    $review_checklist_id = "";
    //get checklist.ID
    for($count = 0; $count <= (count($budgets) - 1); $count++)
    {
      $budget = $budgets[$count];
      $rc_id = $reviewchecklist_id[$count];
      
      if($budget != 0)
      {
        $review_checklist_id .= $rc_id.",";
      }
    }  
      $sql = "INSERT INTO designer_notif(
                Project_ID,
                Internaldl_ID,
                Designer_ID,
                Budget,
                Deadline,
                Input_Date,
                Activity,
                RC_ID)
              VALUES($project_id,
                $internaldl_id,
                $designer_id,
                $total_budget,
                '$deadline',
                CURDATE(),
                '$activity',
                '$review_checklist_id')";
      if(mysqli_query($conn,$sql))
      {
        $dnotifsql = "SELECT MAX(ID) AS ID FROM designer_notif";
        $result = mysqli_query($conn,$dnotifsql);
        $row = mysqli_fetch_assoc($result);
        $dnotif_id = $row["ID"];

        for($count = 0; $count <= (count($budgets) - 1); $count++)
        {
          $budget = $budgets[$count];
          $rc_id = $reviewchecklist_id[$count];
          if($budget != 0)
          {
            setRequirements($dnotif_id,$rc_id,$budget,$designer_id);
          }
        }
      }
	}
  
	/* Save Requirements */
	function setRequirements($dnotif_id,$reviewchecklist_id,$budget,$designer_id){
		include("connect.php");
    $readsql = "SELECT ID
                FROM requirements_for_des
                WHERE Designer_ID = $designer_id
                  AND ReviewChecklist_ID = $reviewchecklist_id
                  AND DNotif_ID = $dnotif_id";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      $updatesql = "UPDATE requirements_for_des
                    SET Budget = $budget
                    WHERE Designer_ID = $designer_id
                      AND ReviewChecklist_ID = $reviewchecklist_id
                      AND DNotif_ID = $dnotif_id";
      if(mysqli_query($conn,$updatesql))
      {
      }
    }
    else
    {
      $sql = "INSERT INTO requirements_for_des(
            DNotif_ID,
            ReviewChecklist_ID,
            Budget,
            Designer_ID)
          VALUES('$dnotif_id',
              $reviewchecklist_id,
              $budget,
              $designer_id)";
      if(mysqli_query($conn,$sql))
      {}
    }
 }

  /* Check if designer_notif already exists */
  function isDNotifExists($conn,$budget,$rc_id,$designer_id,$project_id){
    $value = [];
    $rc_id = explode(",",$rc_id);
    foreach($rc_id as $checklist)
    {
      $sql = "SELECT ID
              FROM designer_notif
              WHERE RC_ID LIKE '%$checklist%'
              AND Designer_ID = $designer_id
              AND Project_ID = $project_id";
      echo $sql;
      $result = mysqli_query($conn,$sql);
      if(mysqli_num_rows($result) > 0)
      {
        $row = mysqli_fetch_assoc($result);
        $value[0] = $row["ID"];
        $value[1] = true;
      }
      else
      {
        $value[0] = 0;
        $value[1] = false;
        
      }
    }
    return $value;
  }
  
  
  //update designer_notif
  function updateDNotif($conn,$budget,$rc_id,$dnotif_id,$designer_id,$deadline,$project_id){
    $sql = "UPDATE designer_notif
            SET Budget = $budget,
                RC_ID = '$rc_id',
                Deadline = '$deadline'
            WHERE ID = $dnotif_id
              AND Designer_ID = $designer_id
              AND Project_ID = $project_id";
    mysqli_query($conn,$sql);
  }
  
	/* Delete Designer */
	if(isset($_POST["delete_designer"]))
	{
		$dnotif_id = $_POST["dnotif_id"];
		$sql = "DELETE FROM requirements_for_des
						WHERE DNotif_ID = $dnotif_id";
		if(mysqli_query($conn,$sql))
		{
			$delsql = "DELETE FROM designer_notif
								 WHERE ID = $dnotif_id";
			if(mysqli_query($conn,$delsql))
			{}
		}
	}

	/* Edit Designer */
	if(isset($_POST["edit_designer"]))
	{
		$project_id = $_POST["project_id"];
		$internaldl_id = $_POST["internaldl_id"];
		$designer_id = $_POST["designer_id"];
		$total_budget = $_POST["total_budget"];
		$deadline = $_POST["deadline"];
		$activity = $_POST["activity"];
    $budgets = $_POST["budgets"];
    $reviewchecklist_id = $_POST["reviewchecklist_id"];
    $sched_dnotif_id = $_POST["sched_dnotif_id"];
    for($count = 0; $count <= (count($budgets) - 1); $count++)
    {
      $budget = $budgets[$count];
      $rc_id = $reviewchecklist_id[$count];
      $dnotif_id = $sched_dnotif_id[$count];
      if($budget != 0)
      {
        $updatesql = "UPDATE requirements_for_des
                   SET Budget = $budget
                   WHERE Designer_ID = $designer_id
                     AND ReviewChecklist_ID = $rc_id
                     AND DNotif_ID = $dnotif_id";
        if(mysqli_query($conn,$updatesql))
        {
          $budgetsql = "SELECT SUM(Budget) AS Total_Budget
                        FROM requirements_for_des
                        WHERE Designer_ID = $designer_id
                          AND ReviewChecklist_ID = $rc_id
                          AND DNotif_ID = $dnotif_id";
          $result = mysqli_query($conn,$budgetsql);
          if(mysqli_num_rows($result) > 0)
          {
            $row = mysqli_fetch_assoc($result);
            $updatednoitif = "UPDATE designer_notif
                              SET Budget = ".$row["Total_Budget"]."
                              WHERE ID = $dnotif_id";
            mysqli_query($conn,$updatednoitif);
          }
        }
      }
      else if($budget == 0)
      {
        $sql = "DELETE FROM requirements_for_des
						WHERE Designer_ID = $designer_id
                     AND ReviewChecklist_ID = $rc_id
                     AND DNotif_ID = $dnotif_id";
        if(mysqli_query($conn,$sql))
        {
          $budgetsql = "SELECT SUM(Budget) AS Total_Budget
                        FROM requirements_for_des
                        WHERE Designer_ID = $designer_id
                          AND ReviewChecklist_ID = $rc_id
                          AND DNotif_ID = $dnotif_id";
          echo $budgetsql;
          $result = mysqli_query($conn,$budgetsql);
          if(mysqli_num_rows($result) > 0)
          {
            $row = mysqli_fetch_assoc($result);
            if($row["Total_Budget"] > 0)
            {
            $updatednotif = "UPDATE designer_notif
                              SET Budget = ".$row["Total_Budget"]."
                              WHERE ID = $dnotif_id";
            
            mysqli_query($conn,$updatednotif);
            }
            else
            {
              $deletednotif = "DELETE FROM designer_notif
                                WHERE ID = $dnotif_id";
              mysqli_query($conn,$deletednotif);
            }
          }
          else
          {
            $deletednotif = "DELETE FROM designer_notif
                              WHERE ID = $dnotif_id";
            mysqli_query($conn,$deletednotif);
          }
        }
      }
    }
	}

	/* Delete Requirements */
	function deleteRequirements($dnotif_id)
	{
		include("connect.php");
		$sql = "DELETE FROM requirements_for_des
						WHERE DNotif_ID = $dnotif_id";
		if(mysqli_query($conn,$sql))
		{}
	}
  
  /* Accept Requirements request */ 
  
  if($_POST["approve_requirement"])
  {
    $rfd_id = $_POST["rfd_id"];
    $sql = "UPDATE requirements_for_des
            SET Status = 'Approved'
            WHERE ID = $rfd_id";
    if(mysqli_query($conn,$sql)){
      $reviewer_id = $_POST["reviewer_id"];
      $project_id = $_POST["project_id"];
      $ticket_number = $_POST["ticket_number"];
      $designer_id = $_POST["designer_id"];
      $design_acuracy = $_POST["design_accuracy"];
      $timeliness = $_POST["timeliness"];
      $completeness = $_POST["completeness"];
      $rating = $_POST["rating"];
      $sql2 = "INSERT INTO review_logs(
                `Reviewer_ID`,
                `Project_ID`,
                `Ticket_Number`,
                `Designer_ID`,
                `Design_Accuracy`,
                `Timeliness`,
                `Completeness`,
                `Rating`)
              VALUES(
                $reviewer_id,
                $project_id,
                '$ticket_number',
                $designer_id,
                $design_acuracy,
                $timeliness,
                $completeness,
                $rating)";
      echo $sql2;
      if(mysqli_query($conn,$sql2))
      {
        
      }
    }
  }
  
  
  if(isset($_POST["reject_requirement"]))
  {
    $rfd_id = $_POST["rfd_id"];
    $sql = "UPDATE requirements_for_des
            SET Status = 'Rejected'
            WHERE ID = $rfd_id";
    if(mysqli_query($conn,$sql)){
      
    }
  }
  
/* for cad */

if(isset($_POST["accept_cad"]))
{
  $cadtech_notif_id = $_POST["cadtech_notif_id"];
    $sql = "UPDATE cadtech_notif
            SET Status = 'Reviewer CAD Accepted'
            WHERE ID = $cadtech_notif_id";
    if(mysqli_query($conn,$sql)){
    }
}

if(isset($_POST["reject_cad"]))
{
  $cadtech_notif_id = $_POST["cadtech_notif_id"];
  $sql = "UPDATE cadtech_notif
          SET Status = 'Reviewer CAD Rejected'
          WHERE ID = $cadtech_notif_id";
  if(mysqli_query($conn,$sql)){
  }
}

//edit designer_notif
//move designer deadline 
if(isset($_POST["move_desdl"]))
{
  $deadline = $_POST["deadline"];
  $dnotif_id = $_POST["dnotif_id"];
  $sql = "UPDATE designer_notif
          SET Deadline = '$deadline'
          WHERE ID = $dnotif_id";
  if(mysqli_query($conn,$sql))
  {}
}