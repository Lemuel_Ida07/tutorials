<?php
	include('connect.php');
	session_start();	

	$month_selected = 4;
	
?>
<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="scripts/internal_deadline_computation.js"></script>
	<link rel="stylesheet" type="text/css" href = "css/calendar.css"/>
	<link rel="stylesheet" type="text/css" href = "css/loader.css"/>
		<script src="jquery-3.2.1.min.js"></script>
	
</head>

<style>
	#menu_item_logo4
	{
		background-color:#f2f2f2;
	}
	#menu_item_logo4:hover
	{
		background-color:#f2f2f2;
	}
	#tab4
	{
		color:#515151;
		font-weight: bold;
		text-shadow: none;
	}
</style>

<!-- LOADER -->
<div id="loader_bg">
	<center>
	<div class="loader" id="cir">
	</div>
	<progress style="display: none;" value="0" max="1" id="loading"></progress>
	</center>
</div>
		
<!-- POP UP -->
<div id="pop_up_bg">
	<div id="pop_up">
		<div id="close_button" onClick="closePop()"> <label style="font-family: Tahoma, Geneva, sans-serif; font-size: 24px; color: #FFFFFF;">x</label> </div>
		<form method="post">
			<h1 id="date"> April 5, 2018 </h1>
			
			<label> Please Select a Project Name. </label><br>
				<input type="text" name="searchbox" id="searchbox" style="padding:20px;" onfocus="showList()" placeholder="Select a project." onkeyup="filter('searchbox','datalist')"/>

				<input type="text" name="projectid" id="projectid" style="display:none"/>
				<input type="text" name="external_deadline" id="external_deadline" style="display:none" />
				<input type="text" name="day" id="day" style="display:none" />
				<input type="text" name="inday" id="inday" style="display:none" />
				<input type="text" name="month" id="month" style="display:none" />
				<input type="text" name="year" id="year" style="display:none" />
				<div id="datalist">
					<?php
						$sql = "SELECT ID,ProjectNumber,Project_Name FROM projects";
						$result = mysqli_query($conn,$sql);
						if(mysqli_num_rows($result) > 0)
						{
							while($rows = mysqli_fetch_assoc($result))
							{
								echo "<h4 id='".$rows['ID']."' onclick='hideList();passData".$rows['ID']."();'> ".$rows['ProjectNumber']." - ".$rows['Project_Name']." </h4>";
								echo "<script>
											function passData".$rows['ID']."()
											{
												document.getElementById('searchbox').value = document.getElementById('".$rows['ID']."').innerHTML;
												document.getElementById('projectid').value='".$rows['ID']."';
											}
									  </script>";
							}								
						}
					?>
				</div>
			<br>
			
			<center>
			<table id="checklist1" border=0 style="margin-top: 10px; table-layout: fixed">
			
				<tr>
					<td>
						<input id="tradeC" onClick="cOut()" type="checkbox" name="trade"/> C
					</td>
					<td>
						<input id="c_in_dead" name="internal_deadline" style="padding: 0px; height: 1%; width: 50%; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 100%; margin-bottom: 0px;" type="text" placeholder="yyyy-mm-dd" readonly/>
						
						<input style="padding: 0px; height: 1%; width: 50%; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 100%; margin-bottom: 0px;" type="text" list="ticketN"  name="TicketNumber" id="id_get"  placeholder="Ticket Number">
							<datalist id="ticketN" name="ticketN">
								<?php
	
									$sql = "SELECT Ticket_Number FROM tickets WHERE ProjectNumber = '".$projectnumber."'";
			
									$result = mysqli_query($conn,$sql);
									if(mysqli_num_rows($result) > 0)
									{
										while($rows = mysqli_fetch_assoc($result))
										{
											echo "<option value='".$rows["Ticket_Number"]."'> ".$rows["Ticket_Number"]."</option>";
										}
									}
								?>
							</datalist>
					</td>
					
					<td>
						&nbsp
					</td>
					
					<td>
						&nbsp
					</td>
					
					<td>
						&nbsp
					</td>
					
					<td>
						<input id="tradeE" onClick="eOut()" type="checkbox" name="trade"/> E
					</td>
					<td>
						<input id="e_in_dead" style="padding: 0px; height: 1%; width: 50%; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 100%; margin-bottom: 0px;" type="text" placeholder="yyyy-mm-dd" readonly></input>
						
						<input style="padding: 0px; height: 1%; width: 50%; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 100%; margin-bottom: 0px;" type="text" list="ticketN"  name="TicketNumber" id="id_get"  placeholder="Ticket Number">
							<datalist id="ticketN" name="ticketN">
								<?php
	
									$sql = "SELECT Ticket_Number FROM tickets WHERE ProjectNumber = '".$projectnumber."'";
			
									$result = mysqli_query($conn,$sql);
									if(mysqli_num_rows($result) > 0)
									{
										while($rows = mysqli_fetch_assoc($result))
										{
											echo "<option value='".$rows["Ticket_Number"]."'> ".$rows["Ticket_Number"]."</option>";
										}
									}
								?>
							</datalist>
					</td>
				</tr>
				<tr>
					<td>
						<input id="tradeA" onClick="aOut()" type="checkbox" name="trade"/> A
					</td>
					<td>
						<input id="a_in_dead" style="padding: 0px; height: 1%; width: 50%; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 100%; margin-bottom: 0px;" type="text" placeholder="yyyy-mm-dd" readonly></input>
						
						<input style="padding: 0px; height: 1%; width: 50%; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 100%; margin-bottom: 0px;" type="text" list="ticketN"  name="TicketNumber" id="id_get"  placeholder="Ticket Number">
							<datalist id="ticketN" name="ticketN">
								<?php
	
									$sql = "SELECT Ticket_Number FROM tickets WHERE ProjectNumber = '".$projectnumber."'";
			
									$result = mysqli_query($conn,$sql);
									if(mysqli_num_rows($result) > 0)
									{
										while($rows = mysqli_fetch_assoc($result))
										{
											echo "<option value='".$rows["Ticket_Number"]."'> ".$rows["Ticket_Number"]."</option>";
										}
									}
								?>
							</datalist>
					</td>
					
					<td>
						&nbsp
					</td>
					
					<td>
						&nbsp
					</td>
					
					<td>
						&nbsp
					</td>
					
					<td>
						<input id="tradeAU" onClick="auOut()" type="checkbox" name="trade"/> AU
					</td>
					<td>
						<input id="au_in_dead" style="padding: 0px; height: 1%; width: 50%; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 100%; margin-bottom: 0px;" type="text" placeholder="yyyy-mm-dd" readonly></input>
						
						<input style="padding: 0px; height: 1%; width: 50%; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 100%; margin-bottom: 0px;" type="text" list="ticketN"  name="TicketNumber" id="id_get"  placeholder="Ticket Number">
							<datalist id="ticketN" name="ticketN">
								<?php
	
									$sql = "SELECT Ticket_Number FROM tickets WHERE ProjectNumber = '".$projectnumber."'";
			
									$result = mysqli_query($conn,$sql);
									if(mysqli_num_rows($result) > 0)
									{
										while($rows = mysqli_fetch_assoc($result))
										{
											echo "<option value='".$rows["Ticket_Number"]."'> ".$rows["Ticket_Number"]."</option>";
										}
									}
								?>
							</datalist>
					</td>
				</tr>
				<tr>
					<td>
						<input id="tradeS" onClick="sOut()" type="checkbox" name="trade"/> S
					</td>
					<td>
						<input id="s_in_dead" style="padding: 0px; height: 1%; width: 50%; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 100%; margin-bottom: 0px;" type="text" placeholder="yyyy-mm-dd" readonly></input>
						
						<input style="padding: 0px; height: 1%; width: 50%; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 100%; margin-bottom: 0px;" type="text" list="ticketN"  name="TicketNumber" id="id_get"  placeholder="Ticket Number">
							<datalist id="ticketN" name="ticketN">
								<?php
	
									$sql = "SELECT Ticket_Number FROM tickets WHERE ProjectNumber = '".$projectnumber."'";
			
									$result = mysqli_query($conn,$sql);
									if(mysqli_num_rows($result) > 0)
									{
										while($rows = mysqli_fetch_assoc($result))
										{
											echo "<option value='".$rows["Ticket_Number"]."'> ".$rows["Ticket_Number"]."</option>";
										}
									}
								?>
							</datalist>
					</td>
					
					<td>
						&nbsp
					</td>
					
					<td>
						&nbsp
					</td>
					
					<td>
						&nbsp
					</td>
					
					<td>
						<input id="tradeP" onClick="pOut()" type="checkbox" name="trade"/> P
					</td>
					<td>
						<input id="p_in_dead" style="padding: 0px; height: 1%; width: 50%; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 100%; margin-bottom: 0px;" type="text" placeholder="yyyy-mm-dd" readonly></input>
						
						<input style="padding: 0px; height: 1%; width: 50%; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 100%; margin-bottom: 0px;" type="text" list="ticketN"  name="TicketNumber" id="id_get"  placeholder="Ticket Number">
							<datalist id="ticketN" name="ticketN">
								<?php
	
									$sql = "SELECT Ticket_Number FROM tickets WHERE ProjectNumber = '".$projectnumber."'";
			
									$result = mysqli_query($conn,$sql);
									if(mysqli_num_rows($result) > 0)
									{
										while($rows = mysqli_fetch_assoc($result))
										{
											echo "<option value='".$rows["Ticket_Number"]."'> ".$rows["Ticket_Number"]."</option>";
										}
									}
								?>
							</datalist>
					</td>
				</tr>
				<tr>
					<td>
						<input id="tradeM" onClick="mOut()" type="checkbox" name="trade"/> M
					</td>
					<td>
						<input id="m_in_dead" style="padding: 0px; height: 1%; width: 50%; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 100%; margin-bottom: 0px;" type="text" placeholder="yyyy-mm-dd" readonly></input>
						
						<input style="padding: 0px; height: 1%; width: 50%; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 100%; margin-bottom: 0px;" type="text" list="ticketN"  name="TicketNumber" id="id_get"  placeholder="Ticket Number">
							<datalist id="ticketN" name="ticketN">
								<?php
	
									$sql = "SELECT Ticket_Number FROM tickets WHERE ProjectNumber = '".$projectnumber."'";
			
									$result = mysqli_query($conn,$sql);
									if(mysqli_num_rows($result) > 0)
									{
										while($rows = mysqli_fetch_assoc($result))
										{
											echo "<option value='".$rows["Ticket_Number"]."'> ".$rows["Ticket_Number"]."</option>";
										}
									}
								?>
							</datalist>
					</td>
					
					<td>
						&nbsp
					</td>
					
					<td>
						&nbsp
					</td>
					
					<td>
						&nbsp
					</td>
					
					<td>
						<input id="tradeFP" onClick="fpOut()" type="checkbox" name="trade"/> FP
					</td>
					<td>
						<input id="fp_in_dead" style="padding: 0px; height: 1%; width: 50%; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 100%; margin-bottom: 0px;" type="text" placeholder="yyyy-mm-dd" readonly></input>
						
						<input style="padding: 0px; height: 1%; width: 50%; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 100%; margin-bottom: 0px;" type="text" list="ticketN"  name="TicketNumber" id="id_get"  placeholder="Ticket Number">
							<datalist id="ticketN" name="ticketN">
								<?php
	
									$sql = "SELECT Ticket_Number FROM tickets WHERE ProjectNumber = '".$projectnumber."'";
			
									$result = mysqli_query($conn,$sql);
									if(mysqli_num_rows($result) > 0)
									{
										while($rows = mysqli_fetch_assoc($result))
										{
											echo "<option value='".$rows["Ticket_Number"]."'> ".$rows["Ticket_Number"]."</option>";
										}
									}
								?>
							</datalist>
					</td>
				</tr>
				
			</table>
			
			<input id="save_button" type="submit" name="add_deadline" value="Generate" style="margin-top: 3%; vertical-align: bottom;"/>
			
		</form>
		</center>
			
			
	</div>

</div>
</div>
<div id="bg_bg">
</div>

<!-- END POP UP -->

<div id="main_header">

<img id="toogle_button" onClick="toogle_side_menu()" src="img/left_arrow.png"/>
<img id="toogle_button_sml" onClick="prevMonth()" src="img/left_arrow.png" style="margin-left: 10%;"/>
<img id="toogle_button_sml" onClick="nextMonth()" src="img/left_arrow.png" style="transform: rotate(180deg);"/>
<p id="monthnumber" style="display: none;"> 4 </p>
<h1 style="padding: 10px; margin-left: 20px; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 36px; float: left;"> Calendar </h1>
<h1 style="padding: 10px; margin-top: 0.6%; margin-left: 0px; font-family: 'Arial Unicode MS',Arial, Helvetica, sans-serif; font-size: 22px; float: left;"> for month of <span id="currentMonth">April</span> </h1>
<center>
</center>

</div>

<!-- PREVIOUS MONTH VIEWER -->

<div id="info" style="border-radius: 0px; width: 15%; margin: 0px; margin-top: 3%; transform: translateX(40px);">
	<center>

		<?php

			$selected_month = $month_selected;
			$totaldays=cal_days_in_month(CAL_GREGORIAN,$selected_month,2018);
			$loop = $totaldays / 7;
			$extradays = $totaldays % 7;
		
			if($extradays != 0)
			$loop ++;
			$daycounter = 1;
			$dayweek = 7;
			$count4 = 0; // para sa count ng kung ilan yung event per day

			echo "<table id='prev_mo' border=0>";
			echo "<tr>";
			echo "<th id='pmh' colspan=1><button onClick='prevMonth()' id='btn_prev_mon'> < </button></th>";
			echo "<th id='pmh' colspan=5> <label id='previous_month_ind'>March</label> </th>";
			echo "<th id='pmh' colspan=1><button onClick='nextMonth()' id='btn_next_mon'> > </button></th>";
			echo "</tr>";

			for($count = 1; $count <= $loop; $count ++) {

				echo"<tr>";
				
				for($count2 = $daycounter; $count2 <= $dayweek; $count2 ++) {
					echo "<td id='prev_days'> $count2 </td>";	
				}

				echo "</tr>";

				$daycounter = $count2;
				if($extradays!=0) {
					if($count < $loop - 2) {
						$dayweek = $count2 + 6; 
					}
					else {
						$dayweek = $count2 + ($extradays - 1);
					}
				}
				else {
					$dayweek = $count2 + 6; 
				}
			}

			echo "</table>";
		?>

	</center>
		
		<br>
		<br>

<!-- END PREVIOUS MONTH VIEWER -->
	
<div id="legends_b" onClick="showLegends()"><strong id="lol"> Legends </strong></div>
<div id="legends">


<br>
<br>

<style>

#the_teams {
	color: #FFFFFF;
}

#teams {
	width: 350px;
	display: inline;
	color:  #6c6c6c;
	text-shadow: 5px 5px 5px #000000;
}

#teamss {
	width: 160px;
}

dt {
	color: #000000;
	text-shadow: none;
}

</style>
<center>
<table border=0 id="the_teams">
	<center>
	<tr id="teams">
		<td id="teamss">
		<dl>
			<dt> House Arryn </dt>
				<dt> <input value="Internal Due Date" style=" border: none; border-radius: 100px; width: 150px; height: 30px; padding: 3px; margin-bottom: 5px; background-color: #000000; color: #FFFFFF; text-align: center; text-shadow: 3px 3px 5px #000000;"></input> </dt>
				<dt> <input value="External Due Date" style=" border: none; border-radius: 100px; width: 150px; height: 30px; padding: 3px; margin-bottom: 5px; background-color: #cccccc; color: #FFFFFF; text-align: center; text-shadow: 3px 3px 5px #000000;"></input> </dt>
				<br>
			<dt> House Stark </dt>
				<dt> <input value="Internal Due Date" style=" border: none; border-radius: 100px; width: 150px; height: 30px; padding: 3px; margin-bottom: 5px; background-color: #00B0F0; color: #FFFFFF; text-align: center; text-shadow: 3px 3px 5px #000000;"></input> </dt>
				<dt> <input value="External Due Date" style=" border: none; border-radius: 100px; width: 150px; height: 30px; padding: 3px; margin-bottom: 5px; background-color: #005878; color: #FFFFFF; text-align: center; text-shadow: 3px 3px 5px #000000;"></input> </dt>
				<br>
			<dt> House Lanninster </dt>
				<dt> <input value="Internal Due Date" style=" border: none; border-radius: 100px; width: 150px; height: 30px; padding: 3px; margin-bottom: 5px; background-color: #FF0000; color: #FFFFFF; text-align: center; text-shadow: 3px 3px 5px #000000;"></input> </dt>
				<dt> <input value="External Due Date" style=" border: none; border-radius: 100px; width: 150px; height: 30px; padding: 3px; margin-bottom: 5px; background-color: #800000; color: #FFFFFF; text-align: center; text-shadow: 3px 3px 5px #000000;"></input> </dt>
				<br>
			<dt> House Tyrell </dt>
				<dt> <input value="Internal Due Date" style=" border: none; border-radius: 100px; width: 150px; height: 30px; padding: 3px; margin-bottom: 5px; background-color: #92D050; color: #FFFFFF; text-align: center; text-shadow: 3px 3px 5px #000000;"></input> </dt>
				<dt> <input value="External Due Date" style=" border: none; border-radius: 100px; width: 150px; height: 30px; padding: 3px; margin-bottom: 5px; background-color: #497E10; color: #FFFFFF; text-align: center; text-shadow: 3px 3px 5px #000000;"></input> </dt>
				<br>
			<dt> House Baratheon </dt>
				<dt> <input value="Internal Due Date" style=" border: none; border-radius: 100px; width: 150px; height: 30px; padding: 3px; margin-bottom: 5px; background-color: #FFFF00; color: #FFFFFF; text-align: center; text-shadow: 3px 3px 5px #000000;"></input> </dt>
				<dt> <input value="External Due Date" style=" border: none; border-radius: 100px; width: 150px; height: 30px; padding: 3px; margin-bottom: 5px; background-color: #808000; color: #FFFFFF; text-align: center; text-shadow: 3px 3px 5px #000000;"></input> </dt>
				<br>
			<dt> House Targaryen </dt>
				<dt> <input value="Internal Due Date" style=" border: none; border-radius: 100px; width: 150px; height: 30px; padding: 3px; margin-bottom: 5px; background-color: #A5A5A5; color: #FFFFFF; text-align: center; text-shadow: 3px 3px 5px #000000;"></input> </dt>
				<dt> <input value="External Due Date" style=" border: none; border-radius: 100px; width: 150px; height: 30px; padding: 3px; margin-bottom: 5px; background-color: #545454; color: #FFFFFF; text-align: center; text-shadow: 3px 3px 5px #000000;"></input> </dt>
				<br>
		</dl>
		</td>
	</tr>
	</center>
</table>
</center>
</div>

<div id="content_tray">

</div>	
</div>

<div id="list" style="border-radius: 0px; width: 82.5%; margin: 0px; margin-top: 3%;">

<table id="weeks">
<tr>
		<th id="weekends"> Sunday </th>
		<th id="weekdays"> Monday </th>
		<th id="weekdays"> Tuesday </th>
		<th id="weekdays"> Wednesday </th>
		<th id="weekdays"> Thursday </th>
		<th id="weekdays"> Friday </th>
		<th id="weekends"> Saturday </th>
</tr>
</table>
<style>
	p
	{
		text-align:center;
	}
</style>
<table id="calendar">
	<?php

		$in_dead_bg = array ("none", "#000000", "00B0F0", "#FF0000", "#92D050", "#FFFF00", "#A5A5A5");
		$ex_dead_bg = array ("none", "#CCCCCC", "#005878", "#800000", "#497E10", "#808000", "#545454");
		$totaldays=cal_days_in_month(CAL_GREGORIAN,$month_selected,2018);
		$loop = $totaldays / 7;
		$extradays = $totaldays % 7;
		if($extradays != 0)
		$loop ++;
		$daycounter = 1;
		$dayweek = 7;
		$count4 = 0; // para sa count ng kung ilan yung event per day
		for($count = 1; $count <= $loop; $count ++)
		{
				echo"<tr>";
				for($count2 = $daycounter; $count2 <= $dayweek; $count2 ++)
				{
					$sql = "SELECT projects.ID, projects.Team_ID, projects.ProjectNumber FROM projects INNER JOIN deadlines ON projects.ID = deadlines.Project_ID WHERE day = $count2";
					$result = mysqli_query($conn,$sql);

					$sql2 = "SELECT projects.ID, projects.Team_ID, projects.ProjectNumber FROM projects INNER JOIN internaldeadline ON projects.ID = internaldeadline.Externaldl_ID WHERE day = $count2";
					$result2 = mysqli_query($conn,$sql2);
					
					if(mysqli_num_rows($result) > 0 || mysqli_num_rows($result2) > 0)
					{
						echo "<td id='days'> $count2 
						<div class='$count2' id='daily_container'>";
							
							// SHOW EXTERNAL DEADLINES  
							while($rows = mysqli_fetch_assoc($result))
							{
								$EXcurID = $rows['Team_ID']; // get TEAM ID
									echo "<p style='background-color: ".$ex_dead_bg[$EXcurID].";' id='deadline".$rows['ID']."' class='event' onClick='showDetails()'> ".$rows['ProjectNumber']." </p>";	
									echo "<script> 
											$(document).ready(function(){
												$('#deadline".$rows['ID']."').on('click',function(){
													var value = $(this).val();
														$.ajax(
														{
															url:'deadline.php',
															type:'post',
															data:'request='+".$rows['ID'].",
															success:function(data)
															{   
																$('#content_tray').html(data);
															},
														});
												});
												});
										  </script>";
							}

							// SHOW INTERNAL DEADLINES
							while($rows2 = mysqli_fetch_assoc($result2))
							{
								$INcurID = $rows2['Team_ID']; // get TEAM ID
									echo "<p style='background-color: ".$in_dead_bg[$INcurID].";' id='indeadline".$rows2['ID']."' class='event' onClick='showDetails()'> ".$rows2['ProjectNumber']." </p>";	
									echo "<script> 
											$(document).ready(function(){
												$('#indeadline".$rows2['ID']."').on('click',function(){
													var value = $(this).val();
														$.ajax(
														{
															url:'deadline.php',
															type:'post',
															data:'request='+".$rows2['ID'].",
															success:function(data)
															{   
																$('#content_tray').html(data);
															},
														});
												});
												});
										  </script>";
							}
		
						echo"</div>
						<img id='add_event' onClick='clock(); setSelected($count2); getDate($count2)' src='img/plus_unselected.png'>
						</td>";
						
					}
					
					else
					{
					echo "
					<td id='days'> $count2 <div class='$count2' id='daily_container'></div><img id='add_event' onClick='clock(); setSelected($count2); getDate($count2)' src='img/plus_unselected.png'></td>";
					}
					}
				echo "</tr>";
				$daycounter = $count2;
				if($extradays!=0)
				{
					if($count < $loop - 2)
					{
						$dayweek = $count2 + 6; 
					}
					else
					{
						$dayweek = $count2 + ($extradays - 1);
					}
				}
				else
				{
					$dayweek = $count2 + 6; 
				}
		}
	?>

</table>
</div>
<script>

// FINISH THE SCRIPT FOR THE MONTH TOOGLE CHANGE

function prevMonth() {
		
	var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
	
	var monthholder = document.getElementById('currentMonth').innerHTML;
	var monthnumber = document.getElementById('monthnumber').innerHTML;
	
	if(monthnumber <= 1)
	{
		monthnumber = 12;
	}
	else
	{
		monthnumber--;
	}
	document.getElementById('monthnumber').innerHTML = monthnumber;
	document.getElementById('previous_month_ind').innerHTML = months[monthnumber-1];
	document.getElementById('currentMonth').innerHTML = months[monthnumber];
	
	passToGenerator(monthnumber);
	
}

function nextMonth() {
	var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
	
	var monthholder = document.getElementById('currentMonth').innerHTML;
	var monthnumber = document.getElementById('monthnumber').innerHTML;
	
	if(monthnumber >= 12)
	{
		monthnumber = 1;
	}
	else
	{
		monthnumber++;
	}
	document.getElementById('monthnumber').innerHTML = monthnumber;
	document.getElementById('previous_month_ind').innerHTML = months[monthnumber-1];
	document.getElementById('currentMonth').innerHTML = months[monthnumber];
	
	passToGenerator(monthnumber);

}

function passToGenerator(mn) {
	// AJAX CODE TO GENERATE THE CALENDAR
	var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("prev_mo").innerHTML = this.responseText;
            }
        };

        xmlhttp.open("GET", "calendar_generator.php?q=" + mn, true);
        xmlhttp.send();
}

function openPop() {
	document.getElementById('pop_up').style.visibility = "visible";
	document.getElementById('pop_up_bg').style.visibility = "visible";
	document.getElementById('bg_bg').style.visibility = "visible";
}

function closePop() {
	document.getElementById('pop_up').style.visibility = "hidden";
	document.getElementById('pop_up_bg').style.visibility = "hidden";
	document.getElementById('bg_bg').style.visibility = "hidden";
	
	// RESET ALL DATA
	document.getElementById('c_in_dead').value = "yyyy-mm-dd";
	document.getElementById('a_in_dead').value = "yyyy-mm-dd";
	document.getElementById('s_in_dead').value = "yyyy-mm-dd";
	document.getElementById('m_in_dead').value = "yyyy-mm-dd";
	document.getElementById('e_in_dead').value = "yyyy-mm-dd";
	document.getElementById('au_in_dead').value = "yyyy-mm-dd";
	document.getElementById('p_in_dead').value = "yyyy-mm-dd";
	document.getElementById('fp_in_dead').value = "yyyy-mm-dd";
	document.getElementById('tradeC').checked = false;
	document.getElementById('tradeA').checked = false;
	document.getElementById('tradeS').checked = false;
	document.getElementById('tradeM').checked = false;
	document.getElementById('tradeE').checked = false;
	document.getElementById('tradeAU').checked = false;
	document.getElementById('tradeP').checked = false;
	document.getElementById('tradeFP').checked = false;
}

function toogle() {
	var x = document.getElementById("toogle_view_side_menu");
    if (x.style.transform === "rotate(180deg") {
        x.style.transform = "rotate(-180deg)";
    } else {
        x.style.transform = "rotate(180deg)";
    }
}

function getDate(id,day)
{
	document.getElementById('date').innerHTML = "April "+id+", 2018";
	document.getElementById('external_deadline').value = "2018-04-"+id+"";
	document.getElementById('day').value = id;
}


// BACKGROUND LOADING
function clock() {

var timeleft = 1;



var downloadTimer = setInterval(function(){
	
	//DATE VALIDATOR
	var x = new Date(); // DATE TODAY
	var y = x.getDate(); // EXTRACT THE DAY
	
  document.getElementById("loading").value = 1 - --timeleft;
  if(timeleft <= 0)
	//COMPARE
	if(y > selectedDay){ // selected day from the js file
		alert('ERROR: Cannot Create Deadline Schedule \n\n Deadline Schedule should be beyond the current date.');
	}else{openPop();}
    clearInterval(downloadTimer);
	
},10);	
	
}


// SHOW YUNG SIDE MENU
var drawer = true;

function toogle_side_menu() {
	
	if(drawer == true) {
		document.getElementById("toogle_button").style.transform = "rotate(180deg)";
		document.getElementById("info").style.transform = "translateX(-300px)";
		document.getElementById("list").style.width = "98%";
		document.getElementById("weeks").style.width = "98%";
		drawer = false;
	}
	
	else {
		document.getElementById("toogle_button").style.transform = "rotate(-360deg)";
		document.getElementById("info").style.transform = "translateX(40px)";
		document.getElementById("list").style.width = "82.5%";
		document.getElementById("weeks").style.width = "82.5%";
		drawer = true;
	}
	
		
}

// SHOW YUNG MGA LEGENDS SA INFO
var legendOn = false;
function showLegends() {
	if(legendOn == false) {
		document.getElementById("legends").style.height = "350px";	
		document.getElementById("legends").style.backgroundColor = "#FFFFFF";	
		document.getElementById("legends").style.overflow = "auto";	
		document.getElementById("lol").style.textShadow = " 3px 3px 10px #000000";	
		legendOn = true;
	}
	else if(legendOn == true) {
		document.getElementById("legends").style.height = "0px";	
		document.getElementById("legends").style.backgroundColor = "#00008B";	
		legendOn = false;
	}
}

// SHOW DETAILS NG EVENT SA CALENDAR
function showDetails() {
	if(drawer == false || drawer == false && legendOn == true ) {
		document.getElementById("toogle_button").style.transform = "rotate(-360deg)";
		document.getElementById("info").style.transform = "translateX(40px)";
		document.getElementById("list").style.width = "82.5%";
		document.getElementById("weeks").style.width = "82.5%";
		document.getElementById("legends").style.height = "0px";	
		document.getElementById("legends").style.backgroundColor = "#00008B";
	}
	else if(drawer == true && legendOn == true) {
		document.getElementById("info").style.transform = "translateX(40px)";
		document.getElementById("list").style.width = "82.5%";
		document.getElementById("weeks").style.width = "82.5%";
		document.getElementById("legends").style.height = "0px";	
		document.getElementById("legends").style.backgroundColor = "#00008B";
	}
}

function showList() {
	var element = document.getElementById('datalist').style.display = "block";
}

function hideList() {
	var element = document.getElementById('datalist').style.display = "none";
}

function filter(inputid,divid) {
	var input, filter, list, count;
	input = document.getElementById(inputid);
	filter = input.value.toUpperCase();
	div = document.getElementById(divid);
	list = div.getElementsByTagName("h4");
	for(count = 0; count < list.length; count ++)
	{
		if (list[count].innerHTML.toUpperCase().indexOf(filter) > -1) 
		{
			list[count].style.display = "";
		} else {
			list[count].style.display = "none";
		}
	}
}
</script>
</html>