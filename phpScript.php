<?php
	function alert($string)
	{
		echo "<script> alert('$string'); </script>";
	}
	function get_time_difference($time1, $time2)
{
	$time1 = strtotime("$time1");
	$time2 = strtotime("$time2");

	if ($time2 < $time1)
	{
		$time2 = $time2 + 86400;
	}
	$whole_time = (int)(($time2 - $time1) / 3600);
	$minutes = (int)((($time2 - $time1) % 3600) / 60);
	if($whole_time > 8)
	{
		$whole_time = 8;
		$minutes = 0;
	}
  
	if($whole_time < 10)
		$whole_time = "0".$whole_time;
	if($minutes < 10)
		$minutes = "0".$minutes;
	return $whole_time.":".$minutes;
}

	function getTotalHours()
	{
		include('connect.php');
		$total_time = "0 hrs";
		$designer_id =  $_SESSION["ID"];
		$sql = "SELECT SEC_TO_TIME( SUM( TIME_TO_SEC(Duration) ) ) AS Duration FROM activity_logs WHERE Designer_ID = '$designer_id' and Date = CURDATE()";
			$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_assoc($result);
			$total_time = $row["Duration"];
		}
    else
    {
    $total_time = "0 hrs";
		  
    }

		return $total_time;
	}
	/* GENERATE CALENDAR */
	//generate calendar of current month and year
function generateCalendar($current_month,$current_year) {
	$NoLastDays = 0;
	$base_month = 4; //default month (don't cahnge)
	$base_year = 2018; //default year (don't cahnge)

	for($date_count = $base_month; $current_year >= $base_year; $date_count++)
	{
		$NoLastDays = getPrevMonthNoLastDays($base_month,$base_year,$NoLastDays);	 
		$base_month ++;
		if($base_month > 12)
		{
			$base_year++;
			$base_month = 1 ;
		}						
		//stops the loop				
		if($base_year >= $current_year && $base_month > $current_month)
		{
			break;
		}
	}
	}
	//get the number of last days from the base month and year
function getPrevMonthNoLastDays($base_month,$base_year,$LastDays)
{
		$NoLastDays = $LastDays;
		$month_selected = $base_month;
		$year_selected = $base_year;

		$totaldays=cal_days_in_month(CAL_GREGORIAN,$month_selected,$year_selected);
		$loop = $totaldays / 7; //get no. of weeks(row)
		$extradays = $totaldays % 7; //get last row if any
		if($extradays != 0)
		$loop = ($NoLastDays + $totaldays) / 7; //get no. of weeks(row)
		$extradays = ($NoLastDays + $totaldays) % 7; //get last row if any
		if($extradays != 0)
		$loop ++;
		$daycounter = $NoLastDays - (($NoLastDays * 2)-1);
		$dayweek = 7 -$NoLastDays;
		$lastweekdays = 0;
		for($count = 1; $count < $loop; $count ++)
		{
			$lastweekdays = 0;
			for($count2 = $daycounter; $count2 <= $dayweek; $count2 ++)
			{
				$lastweekdays ++;
			}
			$daycounter = $count2;
			if($extradays != 0)
			{
				if($count < $loop - 2)
				{
					$dayweek = $count2 + 6;
				}
				else
				{
					$dayweek = $totaldays;
				}
			}
			else
			{
				$dayweek = $count2 + 6;
			}
		}
		return $lastweekdays;		
	}
  
function userPic($pic){
  if($pic != "")
  {
    $picture = $pic;
  }
  else
  {
    $picture = "default.png";
  }
  return $picture;
}

function addZero($number){
	if($number <= 9 && $number > 0)
	{
		$number = "0".$number;
	}
	return $number;
}

$weekDays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
