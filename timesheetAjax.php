<?php
	session_start();
	include('connect.php');
	include('phpScript.php');
	$user_id = $_SESSION["ID"];
	if($_POST["proj_state"] == "utilized")
	{
		$date_from = $_POST["date_from"];
		$date_to = $_POST["date_to"];
		$year = $_POST["year"];
		$month = $_POST["month"];
		$sql = "SELECT 
					DISTINCT project.ID,
					project.Project_Number,
					project.Project_Name
				FROM project
				INNER JOIN activity_logs
				ON project.ID = activity_logs.Project_ID
				WHERE 
					activity_logs.Designer_ID = $user_id
					AND activity_logs.Status = 'Normal'
					AND activity_logs.Date >= '$date_from' 
					AND activity_logs.Date <= '$date_to'";
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			$count = 0;
			$date1 = date_create($date_from);
			$date2 = date_create($date_to);
			$days = date_diff($date1,$date2);
			$days = $days->format("%a");
			
			//for utilization
			
			$workDays = 0;
			$actualWorkHours = 0;
			
			while($rows = mysqli_fetch_assoc($result))
			{
				echo "<tr><td style='display:none;'>".$rows["ID"]."</td><td  class='td_project'>".trim($rows["Project_Number"])." - ".trim($rows["Project_Name"])."</td>";
				for($count2 = 0;$count2 <= $days;$count2++)
				{
					
					$durationsql = "SELECT 
										FORMAT((SUM( TIME_TO_SEC(Duration) ) / 3600),2) AS Duration,
										Date,
										Project_ID
									FROM activity_logs
									WHERE 
										Designer_ID = $user_id
										AND Status = 'Normal'
										AND Date = '$year-".addzero($month)."-".addzero($count2+1)."'";
					$durationresult = mysqli_query($conn,$durationsql);
					$durationrow = mysqli_fetch_assoc($durationresult);
					
					$selected_date = $durationrow["Date"];
					$dayOfWeek = date("l", strtotime("$year-".addzero($month)."-".addzero($count2+1).""));
					if(("$year-".addzero($month)."-".addzero($count2+1)."" == "$selected_date") && $rows["ID"] == $durationrow["Project_ID"])
					{
						if($dayOfWeek == "Saturday" || $dayOfWeek == "Sunday")
						{
							echo "<td class='td_hours'><input type='text' value='' class='inp_hours' readonly='readonly' readonly='readonly'/></td>";
						}
						else
						{
							$workDays ++;
							echo "<td class='td_hours'><input type='text' value='".$durationrow["Duration"]."' class='inp_hours' readonly='readonly' readonly='readonly'/></td>";
							$actualWorkHours += $durationrow["Duration"];
						}
					}
					else
					{
						if($dayOfWeek == "Saturday" || $dayOfWeek == "Sunday")
						{
							echo "<td class='td_hours'><input type='text' value='' class='inp_hours' readonly='readonly' readonly='readonly'/></td>";
						}
						else
						{
							$workDays ++;
							echo "<td class='td_hours'><input type='text' value='0' class='inp_hours' readonly='readonly' readonly='readonly'/></td>";
						}
					}
				}
				echo"</tr>";
				$count ++;
			}
			$utilization = number_format(($actualWorkHours / (8 * $workDays)) * 100,2);
		echo "<script>
				$('#utilization').html('$utilization');
			</script>";
		}
	}
	else if($_POST["proj_state"] == "unutilized")
	{
		$date_from = $_POST["date_from"];
		$date_to = $_POST["date_to"];
		$year = $_POST["year"];
		$month = $_POST["month"];
		$sql = "SELECT 
				DISTINCT project.ID,
				project.Project_Number,
				project.Project_Name
			FROM project
			INNER JOIN activity_logs
			ON project.ID = activity_logs.Project_ID
			WHERE 
				activity_logs.Designer_ID = $user_id
				AND activity_logs.Status = 'For Approval'
				AND activity_logs.Date >= '$date_from' 
				AND activity_logs.Date <= '$date_to'";
    $result = mysqli_query($conn,$sql);
    if(mysqli_num_rows($result) > 0)
    {
		$count = 0;
		$date1 = date_create($date_from);
		$date2 = date_create($date_to);
        $days = date_diff($date1,$date2);
		$days = $days->format("%a");
      while($rows = mysqli_fetch_assoc($result))
      {
		$selected_proj_id = $rows["ID"];
		echo "<tr><td style='display:none;'>".$rows["ID"]."</td><td  class='td_project'>".trim($rows["Project_Number"])." - ".trim($rows["Project_Name"])."</td>";
		for($count2 = 0;$count2 <= $days;$count2++)
		{
			$durationsql = "SELECT 
								FORMAT((SUM( TIME_TO_SEC(Duration) ) / 3600),2) AS Duration,
								Date,
								Project_ID
							FROM activity_logs
							WHERE 
								Designer_ID = $user_id
								AND Status = 'For Approval'
								AND Date = '$year-".addzero($month)."-".addzero($count2+1)."'";
			$durationresult = mysqli_query($conn,$durationsql);
			$durationrow = mysqli_fetch_assoc($durationresult);
				
			$selected_date = $durationrow["Date"];
			$dayOfWeek = date("l", strtotime("$year-".addzero($month)."-".addzero($count2+1).""));
			if(("$year-".addzero($month)."-".addzero($count2+1)."" == "$selected_date") && $rows["ID"] == $durationrow["Project_ID"])
			{
				if($dayOfWeek == "Saturday" || $dayOfWeek == "Sunday")
				{
					echo "<td class='td_hours'><input type='text' value='' class='inp_hours_unu' readonly='readonly' readonly='readonly'/></td>";
				}
				else
				{
					echo "<td class='td_hours'><input type='text' value='".$durationrow["Duration"]."' class='inp_hours_unu' readonly='readonly' readonly='readonly'/></td>";
				}
			}
			else
			{
				if($dayOfWeek == "Saturday" || $dayOfWeek == "Sunday")
				{
					echo "<td class='td_hours'><input type='text' value='' class='inp_hours_unu' readonly='readonly' readonly='readonly'/></td>";
				}
				else
				{
					echo "<td class='td_hours'><input type='text' value='0' class='inp_hours_unu' readonly='readonly' readonly='readonly'/></td>";
				}
			}
		}
		echo"</tr>";
			  $count ++;
      }
    }
	}
    ?>
	