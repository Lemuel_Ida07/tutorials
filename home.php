<?php
	session_start();
	include('connect.php');
	include("phpScript.php");
	if(isset($_SESSION["User_Type"]))
	{
    $designer_id = $_SESSION["ID"];
	$user_type = $_SESSION['User_Type'];
	switch($user_type)
	{
		case 1:	//Reviewer
			header("Location:reviewer.php");
			break;
		case 2:	//DC
			header("Location:dc.php");
			break;
		case 3:	//PM
			header("Location:pm.php");
			break;
		case 4:	//Operations
			header("Location:ops.php");
			break;
		case 8: //CAD Manager
			header("Location:cadManager.php");
			break;
		case 9: //CAD Technician
			header("Location:cadTechnician.php");	
			break;
		default:
			break;
	}
            
	$today = date("F j, Y");
	$date_today = date("Y-m-d");
	$total_time = getTotalHours();
}
else
{
  header("Location:index.php");
}
?>
<html>
	<head>
		<title> Activity Monitoring </title>
		<link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/calendar.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
		<link rel="icon" href="img/logoblue.png">
		<script src="scripts/javaScript.js"></script>
		<script src="jquery-3.2.1.min.js"></script>
		<script src="jquery-ui.js"></script>
    <script src="scripts/dateScript.js"></script>
    <style>
      #navbar tr td#activity_monitoring{
        background-color: #deebff;
        color:#0747a6;
      }

      #info{
        height:89%;
      }

      #mainform{
          position:fixed;
          height:100%;
          overflow:hidden;
      }

      #tsheet_container{
        position:absolute;
        width:79.5%;
        height:89%;
        min-width:1080px;
      }

      #tsheet_head{
        position:sticky;
        margin:0px;
      }

      #tsheet_head{
        display:flex;
      }

      .tsheet_weekdays{
        border:solid 1px #cccccc;
        width:20%;
        margin:0px;
        text-align:center;
      }

      #tsheet_body{
        height:95%;
        display:flex;
      }

      .tsheet_container{
        padding:0px;
        border-top:none;
        width:20%;
        margin:0px;
        height:100%;
        text-align:center;
      }

      .activity_box{
        width:98%;
        margin-left:1%;
        margin-top:2%;
        box-shadow:0px 1px 3px 0px;
        text-align:left;
        display:none;
        overflow:auto;
      }

      .activity_box:hover{
        box-shadow:0px 1px 6px 0px;
      }

      .ab_head{
        width:100%;
        padding:4px 0px 4px 0px;
      }

      .ab_body{
          text-align:left;
          padding-left:10px;
          padding-right:10px;
      }

      .ab_textarea{
          border:none;
          font-style: italic;
          word-wrap:break-word;
          padding-right:10px;
      }

      .ab_time{
          padding:0px;
          margin:0px;
          width:auto;
          display:inline;
      }
      
      .ab_footer{
          padding:4px 10px 4px 10px;
      }
      
      .ab_icon{
          width:14px;
          height:14px;
          float:right;
          text-align:center;
          padding:1px 2px 1px 2px;
          cursor:pointer;
      }
      
      .ab_icon:hover{
        box-shadow:0px 0px 2px 0px;
      }
      
      .hidden{
          display:none;
      }
      
      .switch {
  position: relative;
  display: inline-block;
  width: 48px;
  height: 18px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 14px;
  width: 14px;
  left: 4px;
  bottom: 2px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #81D4FA;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

#rfi{
	display:none;
}

#bf{
    position:absolute;
    margin-right:100px;
}
    </style>
	</head>
	<body>
    <?php include('header.php') ?>
    <div id="mainform">
		<div id="info">		
		<div id='mini_calendar'>
			</div>
			<br />
      <table style="width:100%;border-collapse:collapse;">
        <tr>
          <td style="text-align: left;">
            <label class="h1"> Activity Monitoring </label>
          </td>
          <td style="text-align: center;">
            <h1><?php echo $total_time; ?></h1>
          </td>
        </tr>
        <tr>
          <td style="text-align: left;">
            <label class="h3"><?php echo $today; ?></label>
          </td>
          <td style="text-align: center;">
            <label class="text">Total Hours</label>
          </td>
        </tr>
      </table>
        <br/> 
        <label class="title"> Activity </label>
				<textarea id="Activity" class="text" name="Activity" rows="2" cols="20" placeholder="What did you work on?"></textarea>
				<label class="title"> Project </label>
        <div id="proj_type_container" class="float-right">
          <label id="proj_type" class="bold">Assigned</label>
          <label class="switch">
            <input type="checkbox" id="proj_switch">
            <span class="slider round"></span>
          </label>
        </div>
        <br />
				<input type="text" name="searchbox" id="searchbox" class="text"  placeholder="Select a project." autocomplete="off" onkeyup="filter('searchbox','datalist')"/>			
				<div id="datalist" class="datalist"><?php
					$sql = 
            "SELECT project.ID,
              project.Project_Number,
              project.Project_Name 
            FROM project
            INNER JOIN designer_notif
              ON project.ID = designer_notif.Project_ID
            WHERE designer_notif.Designer_ID = $designer_id";
					$result = mysqli_query($conn,$sql);
					if(mysqli_num_rows($result) > 0)
					{
					while($rows = mysqli_fetch_assoc($result))
					{
					echo "<h4 id='".$rows['ID']."' onmousedown='hideList();passData".$rows['ID']."();'> ".$rows['Project_Number']." - ".$rows['Project_Name']." </h4>";
					echo "<script>
					function passData".$rows['ID']."()
					{
					document.getElementById('searchbox').value = document.getElementById('".$rows['ID']."').innerHTML;
					document.getElementById('project_id').value='".$rows['ID']."';
					}
					</script>";
					}
					}
					?>
				</div>
        <label class="title"> Ticket </label><br>
        <input type="text" name="project_id" id="project_id" class="text" style="display:none;" />
				<input type="text"  name="TicketNumber" id="ticket"  placeholder="Ticket Number"/>
				<label id="label1" class="title"> From </label><label id="label2" class="title"> To </label><br/> 
        <input id="inptext1" type="time" name="TimeIn" class="classy-time" value="<?php echo $time_now;?>"/>
        <input id="inptext2" type="time" name="TimeOut" class="classy-time" value="<?php echo $time_now;?>" />
				<input type="submit" id="save_activity_btn" value="Save" class="btn-normal bg-green white bold width-20pc"/>
			
			</div>
			<div id="list">
          <div id="tsheet_container">
            <div id="tsheet_head">
            </div>
            <div id="tsheet_body">
              <div class="tsheet_container" id="tc0"></div>
              <div class="tsheet_container" id="tc1"></div>
              <div class="tsheet_container" id="tc2"></div>
              <div class="tsheet_container" id="tc3"></div>
              <div class="tsheet_container" id="tc4"></div>
          </div>
			</div>	
		</div>
  </div>
	<script>
	$(document).ready(function(){
		$('#mini_calendar').datepicker({
		  showOtherMonths: true,
		  selectOtherMonths: true
		});
	});	
    var proj_type = 1;
  $(document).ready(function(){
    Init();
  });
  
	function Init(){
    fillProjDropDown();
    makeTHead();
	updateReport();
    generatePosts();
    $('#proj_switch').prop('checked',true);
	}
  
  $('textarea').on('paste input', function () {
    if ($(this).outerHeight() > this.scrollHeight){
        $(this).height(1)
    }
    while ($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))){
        $(this).height($(this).height() + 1)
    }
  });
  
  //create header in activity Monitoring Monday - Friday
  function makeTHead(){
    var weekDays = getMonFri();
    var dat = new Date();
    var d = dat.getDate();
    var color = "";
    $.each(weekDays,function(index,value){
      var html = $('#tsheet_head').html();
      if(d === value)
      {
      color = "blue";
    }
    else
    {
      color = "";
    }
      $('#tsheet_head').html(html + "<div class='tsheet_weekdays title'><span class='big-text "+color+"'>"+value+"</span> "+monfri[index]+" </div>");
    });
  }
  //post the activity logs in the week view
  function generatePosts(){
  var weekDays = getMonFri();
    $.each(weekDays,function(index,value){
      var html = $('#tc'+index).html();
      $.ajax({
        url:'adGetActivity.php',
        type:'post',
        data:'from_admin=true'+
             '&day='+value+
             '&index='+index,
        success:function(data){
          $('#tc'+index).html(html + data);
        },
        error:function(data){
          alert(data);
        } 
      });
    });
  }
  
  function fillProjDropDown(){
    $.ajax({
			url: "generateDesignerSources.php",
			type: "post",
      data:'getAvailableProj=true'+
           '&proj_type='+proj_type,
			success: function (data) {
        $('#datalist').html(data);
			},
			error: function () {
        $('#datalist').html(data);
			}
    });
  }
  
	function updateReport()
	{
		$.ajax(
		{
			url:'table.php',
			type:'post',
			data:'submit=true',
			success:function(data)
			{
			},
		});
	}

	function hideList() {
		if(document.getElementById('datalist').style.display == "block")
		var element = document.getElementById('datalist').style.display = "none";
	}

	function filter(inputid,divid) {
		var input, filter, list, count;
		input = document.getElementById(inputid);
		filter = input.value.toUpperCase();
		div = document.getElementById(divid);
		list = div.getElementsByTagName("h4");
		for(count = 0; count < list.length; count ++)
		{
			if (list[count].innerHTML.toUpperCase().indexOf(filter) > -1)
			{
				list[count].style.display = "";
			} else {
				list[count].style.display = "none";
			}
		}
	}

function removeName(node) {
	var child = node;

	parent = child.parentNode;

	$('#' + parent.id).remove();
}

$("#searchbox").keypress(function (e) {
			$(".datalist").slideDown()();
			e.stopPropagation();
		});

$("#searchbox").click(function (e) {
			$(".datalist").slideDown();
			e.stopPropagation();
		});

		$(".datalist").click(function (e) {
			e.stopPropagation();
		});

		$(document).click(function () {
			$(".datalist").slideUp();
		});
						
		loadSubmitFunctions();			
    
   	//FOR project dropdown
	/*function addmsg3(type, msg) {
		$('#datalist').html(msg);
	}

	function waitForMsg3() {
		$.ajax({
			type: "GET",
			url: "generateDesignerSources.php",
			async: true,
			cache: false,
			timeout: 10000,
      data:'getAvailableProj=true',

			success: function (data) {
				addmsg3("new", data);
				setTimeout(waitForMsg3, 1000);
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				addmsg3("error", textStatus + " (" + errorThrown + ")");
				setTimeout(waitForMsg3, 15000);
			}
		});
	}

	$(document).ready(function () {
		waitForMsg3();
	});
*/  
  $(document).ready(function(){
    $('#activity_monitoring').on('click',function(){
      location.reload();
    });
  });

  $(document).ready(function(){
    $('#timesheet').on('click',function(){
      $.ajax({
        url:'timesheet.php',
        type:'post',
        data:'request=true',
        success:function(data){
          $('#mainform').html(data);
          $('#active_page').html("Timesheet");
          $('#btn_hamburger').trigger('click');
        }
      });
    });
  });

  $(document).ready(function(){
    $('#mycalendar').on('click',function(){
      var value = $(this).val();
        $.ajax(
        {
          url:'designerCalendar.php',
          type:'post',
          data:'request='+value,
          success:function(data)
          {   	   		
            $('#mainform').html(data);
            $('#active_page').html("My Calendar");
            $('#btn_hamburger').trigger('click');
          },
        });
    });
  });

  $(document).ready(function(){
    $('#tasks').on('click',function(){
      var value = $(this).val();
        $.ajax(
        {
          url:'designerTasks.php',
          type:'post',
          data:'request='+value,
          success:function(data)
          {   	   		
            $('#mainform').html(data);
          },
        });
    });
  });
  
   $(document).ready(function(){
    $('#des_tracking').on('click',function(){
      var value = $(this).val();
        $.ajax(
        {
          url:'designerDesignTracking.php',
          type:'post',
          data:'request='+value,
          success:function(data)
          {   	   		
            $('#mainform').html(data);
            $('#active_page').html("Design Tracking");
            $('#btn_hamburger').trigger('click');
            $.ajax({
              type:'get',
              url:'pushSchedule.php',
              data:'dnotif_id='+$('#dnotif_idInfo').val()+
                   '&for_design_tracking=true',
              success: function(data)
              {
                $("#weeklySchedule").html(data);
                $('#percent_number').html(computeProgress()+ "%");
              }
            });
          },
        });
    });
  });
  
   $(document).ready(function(){
    $('#cad_tracking').on('click',function(){
      var value = $(this).val();
        $.ajax(
        {
          url:'designerCadTracking.php',
          type:'post',
          data:'request='+value,
          success:function(data)
          {   	   		
            $('#mainform').html(data);
            $('#active_page').html("CAD Tracking");
            $('#btn_hamburger').trigger('click');
            $.ajax({
              type:'get',
              url:'pushSchedule.php',
              data:'dnotif_id='+$('#dnotif_idInfo').val()+
                   '&for_cad_tracking=true',
              success: function(data)
              {
                $("#weeklySchedule").html(data);
                $('#percent_number').html(computeProgressCAD()+ "%");
              }
            });
          },
        });
    });
  });

  $(document).ready(function(){
    $('#history').on('click',function(){
      var value = $(this).val();
        $.ajax(
        {
          url:'activityHistory.php',
          type:'post',
          data:'request='+value,
          success:function(data)
          {   	   		
            $('#mainform').html(data);
            $('#active_page').html("History");
            $('#btn_hamburger').trigger('click');
          },
        });
    });
  });
  
  function computeProgress(){
    var standby = $('#pending_count').html().replace("(","");
     standby = parseInt(standby.replace(")",""));
    var requested = $('#requested_count').html().replace("(","");
     requested = parseInt(requested.replace(")",""));
    var rejected = $('#rejected_count').html().replace("(","");
     rejected = parseInt(rejected.replace(")",""));
    var approved = $('#approved_count').html().replace("(","");
     approved = parseInt(approved.replace(")",""));

     var total = parseInt(standby + requested + rejected + approved);
    var percent = parseInt(approved * (100/total));
    $('#progress').css("width",percent+"%");
    if(percent <= 15)
    {
      $('#progress').css("background-color","#ed1c26");
    }
    else if(percent <= 75)
    {
      $('#progress').css("background-color","#ffc107");
    }
    else if(percent <= 100)
    {
      $('#progress').css("background-color","#4caf50");
    }
    if(isNaN(percent))
    {
      percent = 0;
    }
    return percent;
  }
  
  function computeProgressCAD(){
        var standby = $('#standby_count').html().replace("(","");
         standby = parseInt(standby.replace(")",""));
        var assigned = $('#assigned_count').html().replace("(","");
         assigned = parseInt(assigned.replace(")",""));
        var requested = $('#requested_count').html().replace("(","");
         requested = parseInt(requested.replace(")",""));
        var approved = $('#approved_count').html().replace("(","");
         approved = parseInt(approved.replace(")",""));
        var rejected = $('#rejected_count').html().replace("(","");
         rejected = parseInt(rejected.replace(")",""));
         var total = parseInt(standby + assigned + requested + rejected + approved);
        var percent = parseInt(approved * (100/total));
        $('#progress').css("width",percent+"%");
        if(percent <= 15)
        {
          $('#progress').css("background-color","#ed1c26");
        }
        else if(percent <= 75)
        {
          $('#progress').css("background-color","#ffc107");
        }
        else if(percent <= 100)
        {
          $('#progress').css("background-color","#4caf50");
        }
        if(isNaN(percent))
        {
          percent = 0;
        }
        return percent;
  }
  
  $('#save_activity_btn').on('click',function(){
    var status = "Normal";
    if(proj_type == 1)
    {
      status = "Normal";
    }
    else{
      status = "For Approval";
    }
    $.ajax({
      url:'adSubmit.php',
      type:'post',
      data:'save_activity=true'+
           '&project_id='+$('#project_id').val()+
           '&TicketNumber='+$('#ticket').val()+
           '&Activity='+$('#Activity').val()+
           '&TimeIn='+$('#inptext1').val()+
           '&TimeOut='+$('#inptext2').val()+
           '&status='+status,
      success:function(){
        location.replace("redirect.html");
      },
      error:function(data){
        alert(data);
      }
    });
  });
  
  $('#proj_switch').on('change',function(){
    if($(this).prop('checked') == true)
    {
      $('#proj_type').html('Assigned');
      proj_type = 1;
    }
    else
    {
      $('#proj_type').html('Unassigned');
      proj_type = 0;
    }
    fillProjDropDown();
  });
</script>
	</body>
</html>