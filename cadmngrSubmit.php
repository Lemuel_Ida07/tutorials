<?php
	session_start();
	include("connect.php");
  $cadmngr_id = $_SESSION["ID"];
  
	if(isset($_POST["assign_cadTech"]))
	{
		$cadtech_id = $_POST["cadtech_id"];
		$budget = $_POST["budget"];
		$deadline = $_POST["deadline"];
		$project_id = $_POST["project_id"];
		$pop_req_for_des_id = $_POST["pop_req_for_des_id"];
    $designer_id = $_POST["designer_id"];
		
    $sql = "INSERT INTO cadtech_notif( 
                          Designer_ID,
													CadTech_ID,
													CadManager_ID,
													Assigned_Date,
													Budget,
													Deadline,
													Project_ID,
                          ReqForDes_ID)
						VALUES(
                    $designer_id,
									 $cadtech_id,
									 $cadmngr_id,
									 CURDATE(),
									 $budget,
									 '$deadline',
									 $project_id,
                   $pop_req_for_des_id
									)";
    echo $sql;
		if(mysqli_query($conn,$sql))
		{
		}
	}

  if(isset($_POST["accept_cad"]))
  {
    $cadtech_notif_id = $_POST["cadtech_notif_id"];
    
    $sql = "UPDATE cadtech_notif
            SET Status = 'CAD Accepted'
            WHERE ID = $cadtech_notif_id";
    
    if(mysqli_query($conn,$sql))
    {
    }
  }
  
  
  if(isset($_POST["reject_cad"]))
  {
    $cadtech_notif_id = $_POST["cadtech_notif_id"];
    $req_for_des_id = $_POST["req_for_des_id"];
    
    $sql = "UPDATE cadtech_notif
            SET Status = 'CAD Rejected'
            WHERE ID = $cadtech_notif_id";
    
    if(mysqli_query($conn,$sql))
    {
    }
  }
	//GET CADTECHNICIAN
	if(isset($_POST["get_cadtech"]))
	{
		$trade = $_POST["trade"];
		if($trade == "C" ||$trade == "A" ||$trade == "S")
		{
			$trade = "CAS";
		}
		else if($trade == "M" ||$trade == "E" || $trade == "P" || $trade == "FP")
		{
			$trade = "MEPF";
		}
		$sql = "SELECT ID,
              CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Name
						FROM user
						WHERE User_Type = 9
              AND Trade = '$trade'
              AND Status = 'Active'";
														
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			while($rows = mysqli_fetch_assoc($result))
			{
				echo "<h4 onclick='selectCadTech".$rows["ID"]."();'> ".$rows["Name"]." </h4>";
				echo "<script>
								function selectCadTech".$rows["ID"]."()
								{
									$('#pop_cadtech').val('".$rows["Name"]."');	
									$('#pop_cadtech_id').val('".$rows["ID"]."');
									$('.datalist').hide();
								}
							</script>";
			}
		}
	}

	//GET ASSIGNED CADTECHNICIAN
	if(isset($_POST["get_assigned"]))
	{
		getAssigned();
	}

	//EDIT ASSIGNED CADTECHNICIAN
	if(isset($_POST["edit_assigned"]))
	{			
		$cadtech_notif_id=$_POST["cadtech_notif_id"];												
		$budget = $_POST["budget"];
		$deadline = $_POST["deadline"];
		$sql = "UPDATE cadtech_notif
						SET
							Budget = $budget,
							Deadline = '$deadline'
						WHERE
							ID = $cadtech_notif_id;";
							echo $sql;
		if(mysqli_query($conn,$sql))
		{
			getAssigned();
		}
	}

	//DELETE ASSIGNED CADTECHNICIAN
	if(isset($_POST["delete_assigned"]))
	{			
		$cadtech_notif_id=$_POST["cadtech_notif_id"];		
		$sql = "DELETE FROM cadtech_notif
						WHERE ID = $cadtech_notif_id";
		if(mysqli_query($conn,$sql))
		{
			getAssigned();
		}
	}

	//FUNCTIONS
	function getAssigned()
	{
		include("connect.php");
		$cad_manager_id = $_SESSION["ID"];
		$readsql = "SELECT 
										cadtech_notif.ID,
										cadtech_notif.CadTech_ID,
										cadtech_notif.CadManager_ID,
										cadtech_notif.Assigned_Date,
										requirements_for_des.Budget,
										cadtech_notif.Deadline,
                    requirements_for_des.ID AS ReqForDes_ID
									FROM cadtech_notif
                  INNER JOIN requirements_for_des
                    ON cadtech_notif.ReqForDes_ID = requirements_for_des.ID
									WHERE 
										cadtech_notif.CadManager_ID = $cad_manager_id";
			$result = mysqli_query($conn,$readsql);
			if(mysqli_num_rows($result) > 0)
			{
				while($rows = mysqli_fetch_assoc($result))
				{			
					echo "<tr>";													
					$usersql 
          = "SELECT 
                cadtech_notif.ID,
                CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS CADTech
              FROM cadtech_notif 
              INNER JOIN user 
                ON cadtech_notif.CadTech_ID = user.ID ";  		
					$userresult = mysqli_query($conn,$usersql); 					
					$userrow = mysqli_fetch_assoc($userresult); 

					echo "<td> ".$userrow["CADTech"]." </td>"; 
					echo "<td> <input type='number' min='1' id='budget".$rows["ID"]."' max='8' value='".$rows["Budget"]."' style='margin:0px;' /> hrs </td>"; 
					echo "<td> <input type='date' id='deadline".$rows["ID"]."' value='".$rows["Deadline"]."' style='margin:0px;' /> </td>"; 
					echo "<td> 
									<input type='submit' id='edit_assignedBtn".$rows["ID"]."' onclick='editAssigned".$rows["ID"]."();' class='bg-blue white bold btn-normal' value='Edit' />
									<input type='submit' id='del_assignedBtn".$rows["ID"]."' onclick='deleteAssigned".$rows["ID"]."()' class='bg-red white bold btn-normal' value='Delete' /> 
								";
					echo "<script>
									function editAssigned".$rows["ID"]."()
									{
                    $.ajax(
                    {
                      url:'cadmngrSubmit.php',
                      type:'post',
                      data:'edit_assigned=true'+
                           '&cadtech_notif_id=".$rows["ID"]."'+
                           '&budget='+$('#budget".$rows["ID"]."').val()+
                           '&deadline='+$('#deadline".$rows["ID"]."').val()',
                      success:function(data)
                      {
                        $('#assigned_list').html(data);
                        alert('Edit Success!');
                      },
                      error:function(data)
                      {
                        alert(data);
                      }
                    });
									}
									function deleteAssigned".$rows["ID"]."()
									{
											$.ajax(
											{
												url:'cadmngrSubmit.php',
												type:'post',
												data:'delete_assigned=true'+
														 '&cadtech_notif_id=".$rows["ID"]."',
												success:function(data)
												{
													alert(data);
													$('#assigned_list').html(data);
												},
												error:function(data)
												{
													alert(data);
												}
											});
									}
								</script>
</td>"; 		
					echo "</tr>";	
				}
			}
	}