<!DOCTYPE html>
<?php
include('connect.php');
if(isset($_POST['logout']))
{
	unset($_SESSION['ID']);
	unset($_SESSION['Firstname']);
	unset($_SESSION['Middlename']);
	unset($_SESSION['Lastname']);
	unset($_SESSION['Picture']);
	unset($_SESSION['User_Type']);
	unset($_SESSION['Team_ID']);
	unset($_SESSION['Trade']);
	unset($_SESSION['Name']);
	session_destroy();
	header("Location:index.php");
}
if(isset($_POST["User_Type"]))
{
	$id = $_SESSION["ID"];
}
?>

	<link rel="stylesheet" type="text/css" href="css/header.css" />
	<style id="style">
		<?php
			if(!isset($_SESSION["User_Type"]))
			{
				echo "#profile_glimpse{display:none;}";
			}
		?>
	</style>
  <style>
    /*for hamburger menu*/
    .container {
      display: inline-block;
      cursor: pointer;
      margin-left:1em;
    }

    .bar1, .bar2, .bar3 {
      width: 25px;
      height: 3px;
      background-color: #deebff;
      margin: 6px 0;
      transition: 0.4s;
      border-radius:2px;
    }

    .change .bar1 {
      -webkit-transform: rotate(-45deg) translate(-6px, 3px);
      transform: rotate(-45deg) translate(-6px, 3px);
    }

    .change .bar2 {opacity: 0;}

    .change .bar3 {
      -webkit-transform: rotate(45deg) translate(-8px, -8px);
      transform: rotate(45deg) translate(-8px, -8px);
    }
  </style>
<div id="mainheader">
  <div class="container" id="btn_hamburger" onclick="myFunction(this)">
    <div class="bar1"></div>
    <div class="bar2"></div>
    <div class="bar3"></div>
  </div>
	<img id="logo" class="left-align" src="img/logo.png" />
	<div id="profile_glimpse">
		<span id="notifcount" onClick="showNotification();"> 0 </span>
		<img class="icons" id="notif" onClick="showNotification();" src="img/notifi.png"><?php
		if(isset($_SESSION['User_Type']))
		{
		$id = $_SESSION["ID"];
		$sql = "SELECT Picture FROM user WHERE ID = $id";
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
		$row = mysqli_fetch_assoc($result);
		$checkPic = $row['Picture'];
		if($checkPic == '') {
		$checkPic = 'default.png';
		}
		echo "<img class='icons' id='mini_avatar' onClick='showProfile();notify();' src='images/profile_pictures/$checkPic'>";
		}
		else{echo "<img class='icons' id='mini_avatar' onClick='showProfile();notify();' src='images/profile_pictures/default.png'>";}
		}
		?>
	</div>
  <?php
    if(isset($_SESSION["User_Type"]))
    {
      switch($_SESSION["User_Type"])
      {
        case 0://Designer
          echo 
            "<div id='sidebar'>
              <ul>
                 <li> <div id='activity_monitoring' class='nav-btn'>Activity Monitoring</div> </li>
                 <li> <div id='mycalendar' class='nav-btn'>My Calendar</div> </li>
                 <li> <div id='des_tracking' class='nav-btn'>Design Tracking</div> </li>
                 <li> <div id='cad_tracking' class='nav-btn'>CAD Tracking</div> </li>
                 <li> <div id='history' class='nav-btn'>History</div> </li>
                 <li> <div id='activitylogs' class='nav-btn'>Activity Logs</div> </li>
                 <li> <div id='timesheet' class='nav-btn'>Timesheet</div> </li>
                 <li> <div id='kiosk_btn' class='nav-btn'>Kiosk</div> </li>
              </ul>
              <script>
                $('#kiosk_btn').on('click',function(){
                  $.ajax({
                    url:'Kiosk/kiosk.php',
                    type:'post',
                    data:'request=true',
                    success:function(data){
                      location.replace('Kiosk/kiosk.php');
                    },
                    error:function(data){
                      alert(data);
                    }
                  });
                });
              </script>
            </div>
              <div id='active_page'> <label>Activity Monitoring</label> </div>";
          break;
        case 1://Reviewer
          echo "<div id='sidebar'>
                  <ul>
                     <li> <div id='schedule' class='nav-btn'> Schedule </div> </li>
                     <li> <div id='des_tracking' class='nav-btn'> Design Tracking </div> </li>
                     <li> <div id='cad_tracking' class='nav-btn'> CAD Tracking </div> </li>
                     <li> <div id='kiosk_btn' class='nav-btn'>Kiosk</div> </li>
                  </ul>
                  <script>
                    $('#kiosk_btn').on('click',function(){
                      $.ajax({
                        url:'Kiosk/kiosk.php',
                        type:'post',
                        data:'request=true',
                        success:function(data){
                          location.replace('Kiosk/kiosk.php');
                        },
                        error:function(data){
                          alert(data);
                        }
                      });
                    });
                  </script>
                </div>
              <div id='active_page'> <label>Design Tracking</label> </div>
                <table id='navbar' style='width:40%;display:none;'>
                  <tr>
                    <td id='schedule' > Schedule </td>
                    <td style='box-shadow:none;'> </td>
                    <td id='des_tracking'> Design Tracking </td>
                    <td style='box-shadow:none;'> </td>
                    <td id='cad_tracking'> CAD Tracking </td>
                    <td style='box-shadow:none;'> </td>
                    <td id='history'> History </td>
                  </tr>
                  <tr>
                    <td id='' style='visibility:hidden;'>  </td>
                    <td style='box-shadow:none;visibility:hidden;'> </td>
                    <td id='trackingcad' style='background-color:white;color:#003399;display:none;border-radius:0px;padding:4px;'> CAD </td>
                    <td style='box-shadow:none;visibility:hidden;'> </td>
                    <td id='' style='visibility:hidden;'>  </td>
                  </tr>
                </table>";
          break;
        case 4://Operations
          echo "<div id='sidebar'>
                  <ul>
                     <li> <div id='projects' class='nav-btn'>Projects</div> </li>
                     <li> <div id='users' class='nav-btn'>Users</div> </li>
                     <li> <div id='cal' class='nav-btn'>Calendar</div> </li>
                     <li> <div id='kiosk_btn' class='nav-btn'>Kiosk</div> </li>
                  </ul>
                  <script>
                    $('#kiosk_btn').on('click',function(){
                      $.ajax({
                        url:'Kiosk/kiosk.php',
                        type:'post',
                        data:'request=true',
                        success:function(data){
                          location.replace('Kiosk/kiosk.php');
                        },
                        error:function(data){
                          alert(data);
                        }
                      });
                    });
                  </script>
                </div>
              <div id='active_page'> <label>Projects</label> </div>
                <table id='navbar' style='display:none;'>
                  <tr>
                    <td id='projects'> Projects </td>
                    <td style='box-shadow:none;'> </td>
                    <td id='users'> Users </td>
                    <td style='box-shadow:none;'> </td>
                    <td id='cal'> Calendar </td>
                  </tr>
                </table>";
          break;
        case 5://Admin
          echo 
            "<div id='sidebar'>
              <ul>
                 <li> <div id='activity_monitoring' class='nav-btn'>Activity Monitoring</div> </li>
                 <li> <div id='history' class='nav-btn'>History</div> </li>
                 <li> <div id='kiosk_btn' class='nav-btn'>Kiosk</div> </li>
              </ul>
              <script>
                $('#kiosk_btn').on('click',function(){
                  $.ajax({
                    url:'Kiosk/kiosk.php',
                    type:'post',
                    data:'request=true',
                    success:function(data){
                      location.replace('Kiosk/kiosk.php');
                    },
                    error:function(data){
                      alert(data);
                    }
                  });
                });
              </script>
            </div>
              <div id='active_page'> <label>Activity Monitoring</label> </div>";
          break;
        case 8://CADManager
           echo "<div id='sidebar'>
              <ul>
                 <li> <div id='activity_monitoring' class='nav-btn'>Activity Monitoring</div> </li>
                 <li> <div id='reviews' class='nav-btn'>Reviews</div> </li>
                 <li> <div id='tasks' class='nav-btn'> CAD Tracking </div> </li>
                 <li> <div id='history' class='nav-btn'>History</div> </li>
                 <li> <div id='kiosk_btn' class='nav-btn'>Kiosk</div> </li>
              </ul>
              <script>
                $('#kiosk_btn').on('click',function(){
                  $.ajax({
                    url:'Kiosk/kiosk.php',
                    type:'post',
                    data:'request=true',
                    success:function(data){
                      location.replace('Kiosk/kiosk.php');
                    },
                    error:function(data){
                      alert(data);
                    }
                  });
                });
              </script>
            </div>
              <div id='active_page'> <label>CAD Tracking</label> </div>
              <table id='navbar' style='display:none;'>
                  <tr>
                    <td id='reviews'> Reviews </td>
                    <td style='box-shadow:none;'> </td>
                    <td id='tasks'> CAD Tracking </td>
                    <!--<td style='box-shadow:none;'> </td>
                    <td id='cad_tracking'> CAD Tracking </td>-->
                    <td style='box-shadow:none;'> </td>
                    <td id='history'> History </td>
                  </tr>
                </table>";
          break;
        case 9://cad technician
          echo 
            "<div id='sidebar'>
              <ul>
                 <li> <div id='activity_monitoring' class='nav-btn'>Activity Monitoring</div> </li>
                 <li> <div id='tasks' class='nav-btn'> CAD Tracking </div> </li>
                 <li> <div id='history' class='nav-btn'>History</div> </li>
                 <li> <div id='kiosk_btn' class='nav-btn'>Kiosk</div> </li>
              </ul>
              <script>
                $('#kiosk_btn').on('click',function(){
                  $.ajax({
                    url:'Kiosk/kiosk.php',
                    type:'post',
                    data:'request=true',
                    success:function(data){
                      location.replace('Kiosk/kiosk.php');
                    },
                    error:function(data){
                      alert(data);
                    }
                  });
                });
              </script>
            </div>
              <div id='active_page'> <label>CAD Tracking</label> </div>
              <table id='navbar' style='width:50%;display:none;'>
              <tr>
                <td id='activity_monitoring'> Activity Monitoring </td>
                    <td style='box-shadow:none;'> </td>
                <td id='tasks'> CAD Tracking </td>
                <!--    <td style='box-shadow:none;'> </td>
                <td id='cad_tracking'> CAD Tracking </td>-->
                    <td style='box-shadow:none;'> </td>
                <td id='history'> History </td>
                    <td style='box-shadow:none;'> </td>
                <td id='activitylogs'> Activity Logs </td>
              </tr>
            </table>";
          break;
        case 12://Project Director
          echo "<div id='sidebar'>
                  <ul>
                     <li> <div id='cal' class='nav-btn'>Calendar</div> </li>
                     <li> <div id='bnc' class='nav-btn'>BNC</div> </li>
                     <li> <div id='tracking' class='nav-btn'>Tracking</div> </li>
                     <li> <div id='kiosk_btn' class='nav-btn'>Kiosk</div> </li>
                  </ul>
                  <script>
                    $('#kiosk_btn').on('click',function(){
                      $.ajax({
                        url:'Kiosk/kiosk.php',
                        type:'post',
                        data:'request=true',
                        success:function(data){
                      location.replace('Kiosk/kiosk.php');
                        },
                        error:function(data){
                          alert(data);
                        }
                      });
                    });
                  </script>
                </div>
              <div id='active_page'> <label>Calendar</label> </div>
                <table id='navbar' style='display:none;'>
                  <tr>
                    <td id='projects'> Projects </td>
                    <td style='box-shadow:none;'> </td>
                    <td id='users'> Users </td>
                    <td style='box-shadow:none;'> </td>
                    <td id='cal'> Calendar </td>
                  </tr>
                </table>";
          break;
        case 14://BNC
          echo"<div id='active_page'> <label>BNC</label> </div>";
          break;
      }
    }
  ?>
	<div id="view_profile">
		<dl id="profile_content">
			<dt>
				<center>					<?php
					if(isset($_SESSION['User_Type']))
					{
					$sql = "SELECT Picture FROM user WHERE ID = $id";
					$result = mysqli_query($conn,$sql);
					if(mysqli_num_rows($result) > 0)
					{
            $row = mysqli_fetch_assoc($result);
            $checkPic = $row['Picture'];
            if($checkPic == '') {
            $checkPic = 'default.png';
            }
            echo "<a style='width: 100%;' href='changepass.php'><img id='avatar' src='images/profile_pictures/$checkPic'></a>";
					}
					}
					?>
				</center>
			</dt>
			<dt>
				<center>
					<label class='h3'>
							Hi!							<?php
							if(isset($_SESSION['User_Type']))
							{
							echo $_SESSION["Name"];
							}
							?>
					</label>
				</center>
			</dt>
			<dt>
				<center>  
						<a href="manageAccount.php"><input type="submit" Value="Manage Account" name="changepass" id="nav_btn" /></a>
					<form method="post" id="Logout">
						<input type="submit" Value="Logout" name="logout" id="nav_btn" />
					</form>
				</center>
			</dt>
		</dl>
	</div>

	<div id="view_notification">
		<input type="text" id="notif_header" readonly />
		<div id="notif_container">
			<table id="notif_table" cellpadding="0">
				<!-- data from generateMessage.php -->
			</table>
		</div>
	</div>
  
  <div id="backgrounddata" style="display:none;">
      <input type="text" id="user_id" value="<?php echo $_SESSION['ID']?>" />
  </div>
</div>
<script>

	var notif = false;
	var toogle = false;
	var profile = false;

	function changeBg() {
		document.getElementById('nav_btn').style.backgroundColor = "#626567";
	}

	function showProfile() {
		if (profile == false) {
			document.getElementById("view_profile").style.height = "270px";
			document.getElementById("view_profile").style.boxShadow = "1px 1px 5px #000000";
			document.getElementById("view_profile").style.opacity = "1";
			document.getElementById("view_profile").style.position = "fixed";
			document.getElementById("view_profile").style.right = "0";
			profile = true;
			hideNotification();
		}
		else {
			document.getElementById("view_profile").style.height = "0px";
			document.getElementById("view_profile").style.boxShadow = "none";
			document.getElementById("view_profile").style.opacity = "0";
			profile = false;
		}
	}

	function hideProfile() {
		document.getElementById("view_profile").style.height = "0px";
		document.getElementById("view_profile").style.boxShadow = "none";
		document.getElementById("view_profile").style.opacity = "0";
	}

	function showNotification() {
		if (toogle == false) {
			document.getElementById("view_notification").style.height = "304px";
			document.getElementById("view_notification").style.boxShadow = "1px 1px 5px #000000";
			document.getElementById("view_notification").style.opacity = "1";
			document.getElementById("view_notification").style.position = "fixed";
			document.getElementById("view_notification").style.right = "0";
			document.getElementById("notif_container").style.height = "260px";
			toogle = true;
			hideProfile();
		}
		else {
			document.getElementById("view_notification").style.height = "0px";
			document.getElementById("view_notification").style.opacity = "0";
			document.getElementById("notif_container").style.height = "0px";
			toogle = false;
		}
	}

	function hideNotification() {
		document.getElementById("view_notification").style.height = "0px";
		document.getElementById("view_notification").style.opacity = "0";
		document.getElementById("notif_container").style.height = "0px";
		toogle = false;
	}

	function notifAlert() {
		if (notif == true) {
			document.getElementById('notif').style.animation = "ring 1s linear 0s infinite alternate";
			document.getElementById('notif').style.WebkitAnimation = "ring 1s linear 0s infinite alternate";
			document.getElementById('notif').style.MozAnimation = "ring 1s linear 0s infinite alternate";
		} else { document.getElementById('notif').style.animation = "none"; }
	}
	//FOR NOTIF BELL
	function addmsg(type, msg) {
		$('#style').html(msg);
	}

	function waitForMsg() {
		$.ajax({
			type: "GET",
			url: "generateCSS.php",
			async: true,
			cache: false,
			timeout: 10000,

			success: function (data) {
				addmsg("new", data);
				setTimeout(waitForMsg, 1000);
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				addmsg("error", textStatus + " (" + errorThrown + ")");
				setTimeout(waitForMsg, 15000);
			}
		});
	}

	$(document).ready(function () {
		waitForMsg();
	});

	//FOR NOTIF MESSAGE
	function addmsg2(type, msg) {
		$('#notif_table').html(msg);
	}

	function waitForMsg2() {
		$.ajax({
			type: "GET",
			url: "generateMessage.php",
			async: true,
			cache: false,
			timeout: 10000,

			success: function (data) {
				addmsg2("new", data);
				setTimeout(waitForMsg2, 1000);
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				addmsg2("error", textStatus + " (" + errorThrown + ")");
				setTimeout(waitForMsg2, 15000);
			}
		});
	}

	$(document).ready(function () {
		waitForMsg2();
	});

 //for hamburger menu
 function myFunction(x) {
  x.classList.toggle("change");
  $("#sidebar").slideToggle();
}

//for kiosk
$(document).ready(function(){
  $("#kiosk_btn").on("click",function(){
    $.ajax({
      url:"Kiosk/kiosk.php",
      type:"post",
      data:"request=true",
      success:function(data){
        $("#mainform").html(data);
      },
      error:function(data){
        alert(data);
      }
    });
  });
});
</script> 
