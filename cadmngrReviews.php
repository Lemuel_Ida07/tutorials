<?php
	session_start();
	include('connect.php');
	
	$today = date("F j, Y");
	$curr_date = date("Y-m-d");	
	$tradeString = $_SESSION['Trade'];
?>
	<div id="info">
			<label class="h2">My Daily Review Summary </label>
      <br />
			<label class="h3"><?php echo $today; ?></label>
			<br />
      <button id="view_all" class="btn-normal bg-blue white bold"> View All </button>							 
			<br />
			<br />
			<input type="text" placeholder="Search" style="display:none;"/>
			<div id="dname_list">
				<?php
				$sql = "SELECT ID,CONCAT(Firstname, ' ' , Middlename ,' ', Lastname) AS Name FROM user WHERE User_Type = 9 ORDER BY Firstname";
				$result = mysqli_query($conn,$sql);
				if(mysqli_num_rows($result) > 0)
				{
					echo "<table class='general_table width-100pc'><tbody>";
					while($rows = mysqli_fetch_assoc($result))
					{
						echo "<tr class='clickable' id='Des".$rows['ID']."'>
										<td><label class='clickable'> ".$rows['Name']." </label></td>
										<script>
											$(document).ready(function(){
												$('#Des".$rows['ID']."').on('click',function(){
													$.ajax({
														url:'cadGetReviews.php',
														type:'post',
														data:'cadtech_id=".$rows['ID']."',
														success:function(data){
															$('#review_list').html(data);
														},
													});
												});
											});
										</script>
									</tr>";
					}
					echo "</tbody></table>";
				}
						?>
			</div>
		</div>
		<div id="list">
			<table  class="general_table width-100pc">
				<thead>							 
					<th> CAD Technician </th>
					<th> Project </th>
					<th> Ticket </th>
					<th> Comments </th>
					<th> Accuracy </th>
					<th> Timeliness </th>
					<th> Completeness </th>
					<th> Rating </th>
					<th> Review Date </th>
					<th> Time In </th>
					<th> Time Out </th>
				</thead>	
				<tbody id="review_list">		
					<!-- data from cadGetReviews.php-->
					<tr>
						<td colspan='11'><div class='note'><label><strong>Note!</strong> Select a <strong>Name</strong> from the list on the left to view reviews.</label></div></td>
					</tr>
				</tbody>
			</table>
		</div>
<script>
$(document).ready(function(){
		$('#view_all').on('click',function(){
			$.ajax({
				url:'cadGetReviews.php',
				type:'post',
				data:'cadtech_id=1 OR 1 = 1',
				success:function(data){	
					$('#review_list').html(data);
				},
			});
		});
	});
	$(".searchbox").click(function (e) {
			$(".datalist").show();
			e.stopPropagation();
		});

		$(".datalist").click(function (e) {
			e.stopPropagation();
		});

		$(document).click(function () {
			$(".datalist").hide();
		});
</script>