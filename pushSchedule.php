<?php
	session_start();
	include('connect.php');

	$time = new DateTime('', new DateTimeZone('Asia/Manila'));
	$hour = $time->format('H');
  $curr_date = $time->format('mm/dd/yyyy');
  $tradeString = $_SESSION["Trade"];
  $designer_id = $_SESSION["ID"];
  
  
  //to designer's design tracking page (designerDesignTracking.php)
  if(isset($_GET["for_design_tracking"]))
  {
    $curr_time = date("h:i");
		$designer_id = $_SESSION['ID'];
		$dnotif_id = 0;
		$curr_year = date("Y");
		$curr_month = date("m");
		$curr_day = date("d");
    $tradeString = $_SESSION["Trade"];
    
		if($curr_day <= 15)
    {
      $day_limit = 15;																																																																													 
    }
    
    if($curr_day > 15)
		{
      $day_limit = 31; 
    }
    //pending
    /*$pendingsql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        requirements_for_des.Budget,
        designer_notif.Deadline,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID
      INNER JOIN requirements_for_des
        ON designer_notif.ID = requirements_for_des.DNotif_ID
      INNER JOIN review_checklist
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID
      WHERE designer_notif.Designer_ID = $designer_id	
        AND EXTRACT(YEAR FROM(designer_notif.Deadline)) <= $curr_year
        AND EXTRACT(MONTH FROM(designer_notif.Deadline)) <= $curr_month
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) <= $day_limit
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) >= EXTRACT(DAY FROM(CURDATE()))
        AND requirements_for_des.Budget > 0
        AND review_checklist.Trade = '$tradeString'
        AND requirements_for_des.Status = 'Pending'
      ORDER BY designer_notif.ID ASC";*/
    $pendingsql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        requirements_for_des.Budget,
        designer_notif.Deadline,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID
      INNER JOIN requirements_for_des
        ON designer_notif.ID = requirements_for_des.DNotif_ID
      INNER JOIN review_checklist
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID
      WHERE designer_notif.Designer_ID = $designer_id	
        AND requirements_for_des.Budget > 0
        AND review_checklist.Trade = '$tradeString'
        AND requirements_for_des.Status = 'Pending'
      ORDER BY designer_notif.ID ASC";
    $pendingresult = mysqli_query($conn,$pendingsql);

    if(mysqli_num_rows($pendingresult) > 0)
    {
      $count = 0;
      while($rows = mysqli_fetch_assoc($pendingresult))
      {
        $count ++;
        echo "<tr id='tr".$rows["DNotif_ID"]."'>";
        echo "
              <td class='emphasize'> ".$rows["Deadline"]." </td>
              <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
              <td> ".$rows["Check_Item"]." </td>
              <td class='emphasize'> ".$rows["Budget"]." Hours </td>
              <td> ".$rows["Ticket_Number"]." </td>
              <td>";
        
        echo"<input 
          id='".$rows["req_for_des_id"]."'
          type='submit' 
          value='Request' 
          class='btn-normal bg-blue white request_btn bold'
          />";
        echo "<script>
                $('#pending_count').html('($count)');
                $('#".$rows["req_for_des_id"]."').on('click',function(){
                  $.ajax({
                    beforeSend:function(){
                      return confirm('Request ".$rows["Check_Item"]." for review?');
                    },
                    url:'designerSubmit.php',
                    type:'post',
                    data:
                      {
                        start_review:true,
                        reqfordes_id:".$rows["req_for_des_id"]."
                      },
                    success:function(data){
                      $('#des_tracking').click();
                    },
                    error:function(data){
                      alert(data);
                    }
                  });
                });
              </script>";       
        echo "</td>
              </tr>";
      }
    }
    //requested
    $requestedsql = "SELECT COUNT(requirements_for_des.Status) AS Status_Count
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID
      INNER JOIN requirements_for_des
        ON designer_notif.ID = requirements_for_des.DNotif_ID
      INNER JOIN review_checklist
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID
      WHERE designer_notif.Designer_ID = $designer_id	
        AND requirements_for_des.Budget > 0
        AND review_checklist.Trade = '$tradeString'
        AND requirements_for_des.Status = 'Requested'
      ORDER BY designer_notif.ID ASC";
    
    $requestedresult = mysqli_query($conn,$requestedsql);
    
    if(mysqli_num_rows($requestedresult) > 0)
    {
      $row = mysqli_fetch_assoc($requestedresult);
      echo "<script>
                $('#requested_count').html('(".$row["Status_Count"].")');
            </script>";
    }
    //rejected
    $rejectedsql = "SELECT COUNT(requirements_for_des.Status) AS Status_Count
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID
      INNER JOIN requirements_for_des
        ON designer_notif.ID = requirements_for_des.DNotif_ID
      INNER JOIN review_checklist
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID
      WHERE designer_notif.Designer_ID = $designer_id	
        AND requirements_for_des.Budget > 0
        AND review_checklist.Trade = '$tradeString'
        AND requirements_for_des.Status = 'Rejected'
      ORDER BY designer_notif.ID ASC";
    
    $rejectedresult = mysqli_query($conn,$rejectedsql);
    
    if(mysqli_num_rows($rejectedresult) > 0)
    {
      $row = mysqli_fetch_assoc($rejectedresult);
      echo "<script>
                $('#rejected_count').html('(".$row["Status_Count"].")');
            </script>";
    }
    //approved
    $approvedsql = "SELECT COUNT(requirements_for_des.Status) AS Status_Count
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID
      INNER JOIN requirements_for_des
        ON designer_notif.ID = requirements_for_des.DNotif_ID
      INNER JOIN review_checklist
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID
      WHERE designer_notif.Designer_ID = $designer_id	
        AND requirements_for_des.Budget > 0
        AND review_checklist.Trade = '$tradeString'
        AND (requirements_for_des.Status = 'Approved'
              OR requirements_for_des.Status = 'Assigned'
              OR requirements_for_des.Status = 'CAD Requested'
              OR requirements_for_des.Status = 'CAD Accepted'
              OR requirements_for_des.Status = 'Designer CAD Accepted'
              OR requirements_for_des.Status = 'Reviewer CAD Accepted')
      ORDER BY designer_notif.ID ASC";
    
    $approvedresult = mysqli_query($conn,$approvedsql);
    
    if(mysqli_num_rows($approvedresult) > 0)
    {
      $row = mysqli_fetch_assoc($approvedresult);
      echo "<script>
                $('#approved_count').html('(".$row["Status_Count"].")');
            </script>";
    }
  }
 
  //for design tracking page in reviewer.php
  if(isset($_GET["for_reviewer"]))
  {
    $curr_time = date("h:i");
		$designer_id = $_SESSION['ID'];
		$dnotif_id = 0;
		$curr_year = date("Y");
		$curr_month = date("m");
		$curr_day = date("d");
    $tradeString = $_SESSION["Trade"];
		if($curr_day <= 15)
    {
      $day_limit = 15;																																																																													 
    }
    if($curr_day > 15)
		{
      $day_limit = 31; 
    }
    /*$sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        requirements_for_des.Budget,
        designer_notif.Deadline,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status,
        CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID
      INNER JOIN requirements_for_des
        ON designer_notif.ID = requirements_for_des.DNotif_ID
      INNER JOIN review_checklist
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID
      INNER JOIN user 
        ON user.ID = designer_notif.Designer_ID
      WHERE
        EXTRACT(YEAR FROM(designer_notif.Deadline)) <= $curr_year
        AND EXTRACT(MONTH FROM(designer_notif.Deadline)) <= $curr_month
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) <= $day_limit
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) >= EXTRACT(DAY FROM(CURDATE()))
        AND requirements_for_des.Budget > 0
        AND review_checklist.Trade = '$tradeString'
      ORDER BY requirements_for_des.Status DESC,
        designer_notif.ID ASC";*/
    $sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        requirements_for_des.Budget,
        designer_notif.Deadline,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status,
        CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID
      INNER JOIN requirements_for_des
        ON designer_notif.ID = requirements_for_des.DNotif_ID
      INNER JOIN review_checklist
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID
      INNER JOIN user 
        ON user.ID = designer_notif.Designer_ID
      WHERE
        requirements_for_des.Budget > 0
        AND review_checklist.Trade = '$tradeString'
      ORDER BY requirements_for_des.Status DESC,
        designer_notif.ID ASC";
    
		$result = mysqli_query($conn,$sql);
		if(isset($_GET["dnotif_id"]))
		{
			$dnotif_id = $_GET["dnotif_id"];
		}
		if(mysqli_num_rows($result) > 0)
		{
      $pending_count = 0;
      $requested_count = 0;
      $approved_count = 0;
      $rejected_count = 0;
			while($rows = mysqli_fetch_assoc($result))
			{
        switch($rows["Status"])  
        {  
          case "Requested":  
            $requested_count ++;
            echo "<tr style='display:none;'>
                   <td>
                    <script>
                      $('#requested_count').html('($requested_count)');
                    </script>";
          break;
          case "Approved":
             $approved_count ++;
            echo "<tr style='display:none;'>
                   <td>
                    <script>
                      $('#approved_count').html('($approved_count)');
                    </script>";
            break;
          case "Rejected":
             $rejected_count ++;
            echo "<tr style='display:none;'>
                   <td>
                    <script>
                      $('#rejected_count').html('($rejected_count)');
                    </script>";
            break;
          case "Pending":
            $pending_count ++;
            echo "<tr id='tr".$rows["DNotif_ID"]."'>";
            echo "<td style='display:none;'><script>
                    $('#pending_count').html('($pending_count)');
                  </script></td>";
            echo "
                  <td> ".$rows["Designer"]." </td>
                  <td class='emphasize'> ".$rows["Deadline"]." </td>
                  <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                  <td> ".$rows["Check_Item"]." </td>
                  <td class='emphasize'> ".$rows["Budget"]." Hours </td>
                  <td> ".$rows["Ticket_Number"]." </td>
                  <td>";
              echo "<input type='button'
                       value='".$rows["Status"]."'
                       class='btn-no bd-white blue emphasize />'  ";
            break;
          case "Assigned":
             $approved_count ++;
            echo "<tr style='display:none;'>
                   <td>
                    <script>
                      $('#approved_count').html('($approved_count)');
                    </script>";
            break;
          case "CAD Requested":
             $approved_count ++;
            echo "<tr style='display:none;'>
                   <td>
                    <script>
                      $('#approved_count').html('($approved_count)');
                    </script>";
            break;
          case "CAD Accepted":
             $approved_count ++;
            echo "<tr style='display:none;'>
                   <td>
                    <script>
                      $('#approved_count').html('($approved_count)');
                    </script>";
            break;
          
          case "Designer CAD Accepted":
             $approved_count ++;
            echo "<tr style='display:none;'>
                   <td>
                    <script>
                      $('#approved_count').html('($approved_count)');
                    </script>";
            break;
          
          case "Reviewer CAD Accepted":
             $approved_count ++;
            echo "<tr style='display:none;'>
                   <td>
                    <script>
                      $('#approved_count').html('($approved_count)');
                    </script>";
            break;
        }
        echo" </td>
            </tr>";
			}
		}
    else
    {
      echo "<tr style='display:none;'>
              <td>
                <script>
                  $('#pending_count').html('(0)');
                  $('#requested_count').html('(0)');
                  $('#approved_count').html('(0)');
                </script>
              </td>
            </tr>";
    }
  }
  
  //for cad trackings
  
  
//for cad manager's CAD tracking
  if(isset($_POST["for_cadmngr_designtracking"]))
  {
    $curr_time = date("h:i");
		$cadmngr_id = $_SESSION['ID'];
		$dnotif_id = 0;
		$curr_year = date("Y");
		$curr_month = date("m");
		$curr_day = date("d");
    $tradeString = $_SESSION["Trade"];
		
    if($curr_day <= 15)
    {
      $day_limit = 15;																																																																													 
    }
    
    if($curr_day > 15)
		{
      $day_limit = 31; 
    }
    
    /*$sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        requirements_for_des.Budget,
        designer_notif.Deadline,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status,
        CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer,
        user.Trade,
        user.ID as CADTech_ID
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID
      INNER JOIN requirements_for_des
        ON designer_notif.ID = requirements_for_des.DNotif_ID
      INNER JOIN review_checklist
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID
      INNER JOIN user 
        ON user.ID = designer_notif.Designer_ID
      WHERE 
        EXTRACT(YEAR FROM(designer_notif.Deadline)) <= $curr_year
        AND EXTRACT(MONTH FROM(designer_notif.Deadline)) <= $curr_month
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) <= $day_limit
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) >= EXTRACT(DAY FROM(CURDATE()))
        AND requirements_for_des.Budget > 0
      ORDER BY designer_notif.Deadline";*/
    $sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        requirements_for_des.Budget,
        designer_notif.Deadline,
        designer_notif.Designer_ID,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status,
        CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer,
        user.Trade,
        user.ID as CADTech_ID
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID
      INNER JOIN requirements_for_des
        ON designer_notif.ID = requirements_for_des.DNotif_ID
      INNER JOIN review_checklist
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID
      INNER JOIN user 
        ON user.ID = designer_notif.Designer_ID
      WHERE 
        requirements_for_des.Budget > 0
      ORDER BY designer_notif.Deadline";
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
      $standby_count = 0;
      $assigned_count = 0;
      $requested_count = 0;
      $approved_count = 0;
      $rejected_count = 0;
			while($rows = mysqli_fetch_assoc($result))
			{
        switch($rows["Status"]){
          case "Approved": 
            $cadtechsql 
            = "SELECT 
                user.Username, 
                user.ID AS CADTech_ID, 
                cadtech_notif.Status
              FROM cadtech_notif 
              INNER JOIN user  
                ON cadtech_notif.CadTech_ID = user.ID
              WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"]."";
            $cadtechresult = mysqli_query($conn,$cadtechsql);
            if(mysqli_num_rows($cadtechresult) > 0){
              while($cadtechrow = mysqli_fetch_assoc($cadtechresult)){
                switch($cadtechrow["Status"]) 
                { 
                  case "Standby": 
                    $assigned_count ++; 
                    echo "<tr style='display:none;'> 
                            <td> 
                            <script>  
                              $('#assigned_count').html('($assigned_count)'); 
                            </script> 
                          </td> 
                          </tr>"; 
                    break; 
                  case "Working": 
                    $assigned_count ++; 
                    echo "<tr style='display:none;'> 
                            <td> 
                            <script> 
                              $('#assigned_count').html('($assigned_count)'); 
                            </script> 
                          </td>
                          </tr>";
                    break;
                  case "CAD Requested":
                    $requested_count ++;
                    echo "<tr style='display:none;'>
                            <td>
                            <script> 
                              $('#requested_count').html('($requested_count)');
                            </script>
                          </td>
                          </tr>";
                    break;
                  case "CAD Accepted":
                    $approved_count ++;
                    echo "<tr style='display:none;'>
                            <td>
                            <script> 
                              $('#approved_count').html('($approved_count)');
                            </script>
                          </td>
                          </tr>";
                    break;
                  case "CAD Rejected":
                    $rejected_count ++;
                    echo "<tr style='display:none;'>
                            <td>
                            <script> 
                              $('#rejected_count').html('($rejected_count)');
                            </script>
                          </td>
                          </tr>";
                    break;
                  case "Designer CAD Accepted":
                  $approved_count ++;
                  echo "<tr style='display:none'>
                          <td>
                            <script>
                              $('#approved_count').html('($approved_count)');
                            </script>
                          </td>
                        </tr>";
                  break;
                  case "Designer CAD Rejected":
                    $rejected_count ++;
                    echo "<tr style='display:none'>
                            <td>
                              <script>
                                $('#rejected_count').html('($rejected_count)');
                              </script>
                            </td>
                          </tr>";
                  break;
                  case "Reviewer CAD Accepted":
                  $approved_count ++;
                  echo "<tr style='display:none'>
                          <td>
                            <script>
                              $('#approved_count').html('($approved_count)');
                            </script>
                          </td>
                        </tr>";
                  break;
                  case "Reviewer CAD Rejected":
                    $rejected_count ++;
                    echo "<tr style='display:none;'>
                            <td>
                              <script>
                                $('#rejected_count').html('($rejected_count)');
                              </script>
                            </td>
                          </tr>";
                }
              }
            }
            else
            {
              $standby_count ++;
              echo "<tr id='tr".$rows["DNotif_ID"]."'>";
              echo "
                <td> ".$rows["Designer"]." </td>
                <td class='emphasize'> ".$rows["Deadline"]." </td>
                <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                <td> ".$rows["Check_Item"]." </td>
                <td> ".$rows["Ticket_Number"]." </td>";
              echo"<td><a 
                      type='submit'
                      id='assign_link".$rows["req_for_des_id"]."'
                      class='assign_link'>
                      +
                      </a>
                      <script> 
                        $('#standby_count').html('($standby_count)');
                        $(document).ready(function(){
                          $('#assign_link".$rows["req_for_des_id"]."').on('click',function(e){
                            $('#white_pop').css({'top':e.pageY,'left':e.pageX-300});
                            $('#white_pop').slideDown();	
                            $('#selected_designer_id').val('".$rows["Designer_ID"]."'); 	 
                            $('#selected_des_req_id').val('".$rows["req_for_des_id"]."'); 
                            $('#selected_project_id').val('".$rows["Project_ID"]."'); 
                          });
                        });
                      </script>
                  </td>
                </tr>";
            }
        
        break;
        } 
			}
		}
  }
   
  //to designer's cad tracking page (designerCadTracking.php)
  if(isset($_GET["for_cad_tracking"]))
  {
    $curr_time = date("h:i");
		$designer_id = $_SESSION['ID'];
		$dnotif_id = 0;
		$curr_year = date("Y");
		$curr_month = date("m");
		$curr_day = date("d");
    $tradeString = $_SESSION["Trade"];
    
		if($curr_day <= 15)
    {
      $day_limit = 15;																																																																													 
    }
    
    if($curr_day > 15)
		{
      $day_limit = 31; 
    }
    
    /*&$sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        requirements_for_des.Budget,
        designer_notif.Deadline,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status,
        CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID 
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID 
      INNER JOIN requirements_for_des 
        ON designer_notif.ID = requirements_for_des.DNotif_ID 
      INNER JOIN review_checklist 
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID 
      INNER JOIN user 
        ON user.ID = designer_notif.Designer_ID 
      WHERE 
        user.ID = $designer_id
        AND EXTRACT(YEAR FROM(designer_notif.Deadline)) <= $curr_year 
        AND EXTRACT(MONTH FROM(designer_notif.Deadline)) <= $curr_month 
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) <= $day_limit 
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) >= EXTRACT(DAY FROM(CURDATE())) 
        AND requirements_for_des.Budget > 0 
        AND review_checklist.Trade = '$tradeString' 
      ORDER BY designer_notif.Deadline";*/
    $sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        requirements_for_des.Budget,
        designer_notif.Deadline,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status,
        CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID 
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID 
      INNER JOIN requirements_for_des 
        ON designer_notif.ID = requirements_for_des.DNotif_ID 
      INNER JOIN review_checklist 
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID 
      INNER JOIN user 
        ON user.ID = designer_notif.Designer_ID 
      WHERE 
        user.ID = $designer_id
        AND requirements_for_des.Budget > 0 
        AND review_checklist.Trade = '$tradeString' 
      ORDER BY designer_notif.Deadline";
		$result = mysqli_query($conn,$sql);
		
    if(isset($_GET["dnotif_id"]))
		{
			$dnotif_id = $_GET["dnotif_id"];
		}
    
    if(mysqli_num_rows($result) > 0)
    {
      $standby_count = 0;
      $assigned_count = 0;
      $requested_count = 0;
      $rejected_count = 0;
      $approved_count = 0;
      while($rows = mysqli_fetch_assoc($result))
			{
        if($rows["Status"] == "Approved")
        {
          $cadtechsql 
            = "SELECT 
                user.Username, 
                user.ID AS CADTech_ID, 
                cadtech_notif.Status, 
                cadtech_notif.Budget 
              FROM cadtech_notif 
              INNER JOIN user 
                ON cadtech_notif.CadTech_ID = user.ID 
              WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"]."";  
          $cadtechresult = mysqli_query($conn,$cadtechsql); 
          if(mysqli_num_rows($cadtechresult) > 0){ 
            while($cadtechrows = mysqli_fetch_assoc($cadtechresult)) 
            {
              switch($cadtechrows["Status"]) 
              { 
                case "Standby": 
                  $assigned_count ++; 
                  echo "<tr style='display:none;'> 
                          <td> 
                            <script> 
                              $('#assigned_count').html('($assigned_count)'); 
                            </script> 
                          </td> 
                        </tr>"; 
                break;
                case "Working": 
                  $assigned_count ++; 
                  echo "<tr style='display:none;'> 
                          <td>
                            <script>
                              $('#assigned_count').html('($assigned_count)');
                            </script>
                          </td>
                        </tr>";
                break;
                case "CAD Requested":
                  $requested_count ++;
                  echo "<tr style='display:none;'>
                          <td>
                            <script>
                              $('#requested_count').html('($requested_count)');
                            </script>
                          </td>
                        </tr>";
                break;
                case "CAD Rejected":
                    $rejected_count ++;
                    echo "<tr style='display:none;'>
                            <td>
                              <script>
                                $('#rejected_count').html('($rejected_count)');
                              </script>
                            </td>
                          </tr>";
                  break;
                case "CAD Accepted":
                    $approved_count ++;
                    echo "<tr style='display:none;'>
                            <td>
                              <script>
                                $('#approved_count').html('($approved_count)');
                              </script>
                            </td>
                          </tr>";
                    $requested_count ++;
                    echo "<tr style='display:none;'>
                          <td>
                            <script>
                              $('#requested_count').html('($requested_count)');
                            </script>
                          </td>
                        </tr>";
                  break;
                case "Designer CAD Accepted":
                    $approved_count ++;
                    echo "<tr style='display:none;'>
                            <td>
                              <script>
                                $('#approved_count').html('($approved_count)');
                              </script>
                            </td>
                          </tr>";
                  break;
                case "Designer CAD Rejected":
                    $rejected_count ++;
                    echo "<tr style='display:none;'>
                            <td>
                              <script>
                                $('#rejected_count').html('($rejected_count)');
                              </script>
                            </td>
                          </tr>";
                  break;
                case "Reviewer CAD Accepted":
                    $approved_count ++;
                    echo "<tr style='display:none;'>
                            <td>
                              <script>
                                $('#approved_count').html('($approved_count)');
                              </script>
                            </td>
                          </tr>";
                  break;
                case "Reviewer CAD Rejected":
                  $rejected_count ++;
                  echo "<tr style='display:none;'>
                          <td>
                            <script>
                              $('#rejected_count').html('($rejected_count)');
                            </script>
                          </td>
                        </tr>";
              }
            }
          }
          else
          {
            $standby_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                  echo "
                    <td> ".$rows["Designer"]." </td>
                    <td class='emphasize'> ".$rows["Deadline"]." </td>
                    <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                    <td> ".$rows["Check_Item"]." </td>
                    <td class='red emphasize'> N/A </td>
                    <td> ".$rows["Ticket_Number"]." </td>";
                  echo "<td style='display:none;'>
                          <script>
                            $('#standby_count').html('($standby_count)');
                          </script>
                        </td>";
                  echo"
                    <td><input
                    type='button' 
                    value='Standby' 
                    class='btn-no bg-white red emphasize'
                    /></td>
                    <td><input
                    type='button' 
                    value='N/A' 
                    class='btn-no bg-white red emphasize'
                    /></td>
                    ";
          }
        }
      }
    }
  }
  
  //for reiviewer's cad tracking page
  if(isset($_GET["for_reviewer_cadTracking"]))
  {
    $curr_time = date("h:i");
		$designer_id = $_SESSION['ID'];
		$dnotif_id = 0;
		$curr_year = date("Y");
		$curr_month = date("m");
		$curr_day = date("d");
    $tradeString = $_SESSION["Trade"];
    
		if($curr_day <= 15)
    {
      $day_limit = 15;																																																																													 
    }
    
    if($curr_day > 15)
		{
      $day_limit = 31; 
    }
    
    /*$sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        requirements_for_des.Budget,
        designer_notif.Deadline,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status,
        CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID 
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID 
      INNER JOIN requirements_for_des 
        ON designer_notif.ID = requirements_for_des.DNotif_ID 
      INNER JOIN review_checklist 
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID 
      INNER JOIN user 
        ON user.ID = designer_notif.Designer_ID 
      WHERE 
        EXTRACT(YEAR FROM(designer_notif.Deadline)) <= $curr_year 
        AND EXTRACT(MONTH FROM(designer_notif.Deadline)) <= $curr_month 
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) <= $day_limit 
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) >= EXTRACT(DAY FROM(CURDATE())) 
        AND requirements_for_des.Budget > 0 
        AND review_checklist.Trade = '$tradeString' 
      ORDER BY designer_notif.Deadline";*/
    $sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        requirements_for_des.Budget,
        designer_notif.Deadline,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status,
        CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID 
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID 
      INNER JOIN requirements_for_des 
        ON designer_notif.ID = requirements_for_des.DNotif_ID 
      INNER JOIN review_checklist 
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID 
      INNER JOIN user 
        ON user.ID = designer_notif.Designer_ID 
      WHERE 
        requirements_for_des.Budget > 0 
        AND review_checklist.Trade = '$tradeString' 
      ORDER BY designer_notif.Deadline";
		$result = mysqli_query($conn,$sql);
		
    if(isset($_GET["dnotif_id"]))
		{
			$dnotif_id = $_GET["dnotif_id"];
		}
    
		if(mysqli_num_rows($result) > 0)
		{
      
      $standby_count = 0;
      $assigned_count = 0;
      $cad_requested = 0;
      $cad_accepted = 0;
      $cad_rejected = 0;
      $rejected_count = 0;
      
			while($rows = mysqli_fetch_assoc($result))
			{
        switch($rows["Status"]){
          case "Approved":
            $cadtechsql 
            = "SELECT 
                user.Username, 
                user.ID AS CADTech_ID,  
                cadtech_notif.Status  
              FROM cadtech_notif  
              INNER JOIN user  
                ON cadtech_notif.CadTech_ID = user.ID  
              WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"]."";  
            $cadtechresult = mysqli_query($conn,$cadtechsql);  
            if(mysqli_num_rows($cadtechresult) > 0){  
              while($cadtechrow = mysqli_fetch_assoc($cadtechresult)){  
                switch($cadtechrow["Status"]) 
                { 
                  case "Standby": 
                    $assigned_count ++; 
                    echo "<tr style='display:none'>
                            <td>
                              <script>
                                $('#assigned_count').html('($assigned_count)');
                              </script>
                            </td>
                          </tr>";
                    break;
                  case "Working":
                    $assigned_count ++;
                    echo "<tr style='display:none'>
                            <td>
                              <script>
                                $('#assigned_count').html('($assigned_count)');
                              </script>
                            </td>
                          </tr>";
                    break;
                  case "CAD Requested":
                    $cad_requested ++;
                    echo "<tr style='display:none'>
                            <td>
                              <script>
                                $('#requested_count').html('($cad_requested)');
                              </script>
                            </td>
                          </tr>";
                    break;
                  case "CAD Rejected":
                    $cad_rejected ++;
                    echo "<tr style='display:none'>
                            <td>
                              <script>
                                $('#rejected_count').html('($cad_rejected)');
                              </script>
                            </td>
                          </tr>";
                    break;
                  case "CAD Accepted":
                    $cad_accepted ++;
                    echo "<tr style='display:none'>
                            <td>
                              <script>
                                $('#approved_count').html('($cad_accepted)');
                              </script>
                            </td>
                          </tr>";
                    break; 
                  case "Designer CAD Accepted":
                    $cad_accepted ++;
                    echo "<tr id='tr".$rows["DNotif_ID"]."' style='display:none;'>";
                    echo "<td>
                            <script> 
                              $('#approved_count').html('($cad_accepted)');
                            </script>
                          </td></tr>";  
                    $cad_requested ++;
                    echo "<tr style='display:none'>
                            <td>
                              <script>
                                $('#requested_count').html('($cad_requested)');
                              </script>
                            </td>
                          </tr>";
                  break;
                  case "Designer CAD Rejected":
                    $rejected_count ++;
                    echo "<tr style='display:none;'>
                            <td>
                              <script>
                                $('#rejected_count').html('($rejected_count)');
                              </script>
                            </td>
                          </tr>";
                  case "Reviewer CAD Accepted":
                    $cad_accepted ++;
                    echo "<tr id='tr".$rows["DNotif_ID"]."' style='display:none;'>";
                    echo "<td>
                            <script> 
                              $('#approved_count').html('($cad_accepted)');
                            </script>
                          </td></tr>";  
                  break;
                  case "Reviewer CAD Rejected":
                    $rejected_count ++;
                    echo "<tr style='display:none;'>
                            <td>
                              <script>
                                $('#rejected_count').html('($rejected_count)');
                              </script>
                            </td>
                          </tr>";
                }
              }
            }
            else {
              $standby_count ++;
              echo "<tr id='tr".$rows["DNotif_ID"]."'>";
              echo "
                <td> ".$rows["Designer"]." </td>
                <td class='emphasize'> ".$rows["Deadline"]." </td> 
                <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                <td> ".$rows["Check_Item"]." </td>
                <td class='red emphasize'> N/A</td>
                <td> ".$rows["Ticket_Number"]." </td>";
              echo "<td style='display:none;'>
                      <script>
                        $('#standby_count').html('($standby_count)');
                      </script>
                    </td>";
              echo"
                <td><input
                type='button' 
                value='Standby' 
                class='btn-no bg-white red emphasize'
                /></td>
                <td><input
                type='button' 
                value='N/A' 
                class='btn-no bg-white red emphasize'
                /></td>
                ";
            }
            break;
        }
			}
		}
    else
    {
      echo "<tr style='display:none;'>
              <td>
                <script>
                  $('#standby_count').html('(0)');
                  $('#assigned_count').html('(0)');
                  $('#requested_count').html('(0)');
                  $('#rejected_count').html('(0)');
                  $('#approved_count').html('(0)');
                </script>
              </td>
            </tr>";
    }
  }
  
  
  //for cad technician's tasks page (cadTechnician.php)
   if(isset($_GET["for_cadTech_tasks"]))
  {
    $curr_time = date("h:i");
		$cadtech_id = $_SESSION['ID'];
		$dnotif_id = 0;
		$curr_year = date("Y");
		$curr_month = date("m");
		$curr_day = date("d");
    $tradeString = $_SESSION["Trade"];
    
		if($curr_day <= 15)
    {
      $day_limit = 15;																																																																													 
    }
    if($curr_day > 15)
		{
      $day_limit = 31; 
    }
    
    $sql = "SELECT 
              cadtech_notif.ID,
              CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname)
              AS Designer,
              cadtech_notif.Deadline,
              CONCAT(project.Project_Number,' - ',
                     project.Project_Name,' - ',
                     internal_deadline.Phase) AS Project,
              review_checklist.Check_Item,
              cadtech_notif.Budget,
              internal_deadline.Ticket_Number,
              cadtech_notif.Status,
              requirements_for_des.ID AS req_for_des_id
              FROM cadtech_notif
              INNER JOIN project
                ON cadtech_notif.Project_ID = project.ID
              INNER JOIN requirements_for_des
                ON cadtech_notif.ReqForDes_ID = requirements_for_des.ID
              INNER JOIN designer_notif
                ON requirements_for_des.DNotif_ID = designer_notif.ID
              INNER JOIN internal_deadline
                ON designer_notif.Internaldl_ID = internal_deadline.ID
              INNER JOIN user
                ON designer_notif.Designer_ID = user.ID
              INNER JOIN review_checklist
                ON requirements_for_des.ReviewChecklist_ID = review_checklist.ID
              WHERE 
                cadtech_notif.CadTech_ID = $cadtech_id";
    $result = mysqli_query($conn,$sql);
    if(mysqli_num_rows($result) > 0)
    {
        $working_count = 0;
        $pending_count = 0;
        $cad_requested = 0;
        $cad_accepted = 0;
        $cad_rejected = 0;
      while($rows = mysqli_fetch_assoc($result))
      {
        switch($rows["Status"])
        {
          case "Standby":
              if($rows["Status"] == "Standby"){
              $pending_count ++;
            echo "<tr id='tr".$rows["ID"]."'>";
              echo "<td> ".$rows["Designer"]." </td>
                <td class='emphasize'> ".$rows["Deadline"]." </td>
                <td> ".$rows["Project"]." </td>
                <td> ".$rows["Check_Item"]." </td>
                <td class='emphasize'> ".$rows["Budget"]." Hours </td>
                <td> ".$rows["Ticket_Number"]." </td>
                <td>";
              echo"<input 
                type='button' 
                value='START' 
                id='start_btn".$rows["ID"]."' 
                class='btn-normal bg-green white bold' 
                />";
              echo "<script>
                      $('#pending_count').html('($pending_count)');
                    </script>";
              echo "<script>
                
                $('#start_btn".$rows["ID"]."').on('click',function(){
                  $.ajax({
                    beforeSend:function(e){
                      return confirm('Are you sure you want to start?');
                    },
                    url:'cadtechSubmit.php',
                    type:'post',
                    data:'start_working=true'+
                         '&cadtech_notif_id=".$rows["ID"]."',
                    success:function(data){
                      location.reload();
                    }
                  });
                });
                
                $('#request_btn".$rows["ID"]."').on('click',function(){
                  $.ajax({
                    beforeSend:function(){
                      return confirm('Are you sure you want to request?');
                    },
                    url:'cadtechSubmit.php',
                    type:'post',
                    data:'req_for_des_id=".$rows["req_for_des_id"]."'+
                         '&cadtech_notif_id=".$rows["ID"]."',
                    success:function(data){
                      location.reload();
                    }
                  });
                });
              </script>";
        echo "</td></tr>";
            }
            break;
          case "Working":
            echo "<tr id='tr".$rows["ID"]."' style='display:none;'>";
            $working_count ++;
            echo "<td style='display:none;'><script>
                    $('#working_count').html('($working_count)');
                  </script>";
            break;
          case "CAD Requested":
            echo "<tr id='tr".$rows["ID"]."' style='display:none;'>";
            $cad_requested ++;
            echo "<td style='display:none;'><script>
                    $('#requested_count').html('($cad_requested)');
                  </script>";
            break;
          case "CAD Accepted":
            echo "<tr id='tr".$rows["ID"]."' style='display:none;'>";
            $cad_accepted ++;
            echo "
                    <td>
                      <script>
                        $('#approved_count').html('($cad_accepted)');
                      </script>
                    </td>";
            break;
          case "CAD Rejected":
            echo "<tr id='tr".$rows["ID"]."' style='display:none'>";
              $cad_rejected ++;
              echo "<td> ".$rows["Designer"]." </td>
                <td class='emphasize'> ".$rows["Deadline"]." </td>
                <td> ".$rows["Project"]." </td>
                <td> ".$rows["Check_Item"]." </td>
                <td class='emphasize'> ".$rows["Budget"]." Hours </td>
                <td> ".$rows["Ticket_Number"]." </td>
                <td>";
              echo"<input 
                type='button' 
                value='START' 
                id='start_btn".$rows["ID"]."' 
                class='btn-normal bg-green white bold' 
                />";
              echo "<script>
                      $('#rejected_count').html('($cad_rejected)');
                    </script>";
              
            break;
          case "Designer CAD Accepted":
            echo "<tr id='tr".$rows["ID"]."' style='display:none;'>";
            $cad_accepted ++;
            echo "
                    <td>
                      <script>
                        $('#approved_count').html('($cad_accepted)');
                      </script>
                    </td>";
            break;
          case "Designer CAD Rejected":
            $cad_rejected ++;
            echo "<tr style='display:none'>
                    <td>
                      <script>
                        $('#rejected_count').html('($cad_rejected)');
                      </script>
                    </td>
                  </tr>";
          break;
          case "Reviewer CAD Accepted":
            echo "<tr id='tr".$rows["ID"]."' style='display:none;'>";
            $cad_accepted ++;
            echo "
                    <td>
                      <script>
                        $('#approved_count').html('($cad_accepted)');
                      </script>
                    </td>";
            break;
          case "Reviewer CAD Rejected":
            $cad_rejected ++;
            echo "<tr style='display:none;'>
                    <td>
                      <script>
                        $('#rejected_count').html('($cad_rejected)');
                      </script>
                    </td>
                  </tr>";
        }
      }
    }
  }