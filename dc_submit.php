<?php
		session_start();
		include('connect.php');
		include('phpScript.php');

		//save submittal
		if(isset($_POST['submittal']))
		{
			$project_id = $_POST['project_id'];
			$ticket_number = $_POST['ticket_number'];
			$deadline =  date($_POST['deadline']);
			$drawings = $_POST['drawings'];
			$drawing_list = $_POST['drawing_list'];
			$blueprint_request = $_POST['blueprint_request'];
			$specifications = $_POST['specifications'];
			$calculations = $_POST['calculations'];
			$reports = $_POST['reports'];
			$outgoing_email = $_POST['outgoing_email'];
			$submittal_form_transmittal = $_POST['submittal_form_transmittal'];
			$deliverables_list = $_POST['deliverables_list'];

			$sql = "INSERT INTO `dc_submittal`( `Project_ID`, `Ticket_Number`, `Deadline`, `Drawings`, `Drawing_List`, `Blueprint_Request`, `Specifications`, `Calculations`, `Reports`, `Outgoing_Email`, `Submittal_Form_Transmittal`, `Deliverables_List`) 
							VALUES ($project_id,'$ticket_number','$deadline','$drawings','$drawing_list','$blueprint_request','$specifications','$calculations','$reports','$outgoing_email','$submittal_form_transmittal','$deliverables_list')";
														 
			if(mysqli_query($conn,$sql))
			{
					$readsql = "SELECT dc_submittal.ID, 
												project.Project_Number, 
												project.Project_Name, 
												dc_submittal.Ticket_Number, 
												dc_submittal.Deadline, 
												dc_submittal.Drawings, 
												dc_submittal.Drawing_List, 
												dc_submittal.Blueprint_Request, 
												dc_submittal.Specifications, 
												dc_submittal.Calculations, 
												dc_submittal.Reports, 
												dc_submittal.Outgoing_Email, 
												dc_submittal.Submittal_Form_Transmittal, 
												dc_submittal.Deliverables_List
											FROM `dc_submittal`
											INNER JOIN project 
												ON dc_submittal.Project_ID = project.ID";
					$result = mysqli_query($conn,$readsql);
					if(mysqli_num_rows($result) > 0)
					{
						echo "<table class='general_table'>";
						echo "<thead>";
						echo "<tr>";
						echo "	<th>Project Number</th>";
						echo "	<th>Project Name</th>";
						echo "	<th>Ticket Number</th>";
						echo "	<th>Deadline</th>";
						echo "	<th>Drawings</th>";
						echo "	<th>Drawing List</th>";
						echo "	<th>Blueprint Request</th>";
						echo "	<th>Specifications</th>";
						echo "	<th>Calculations</th>";
						echo "	<th>Reports</th>";
						echo "	<th>Outgoing Email</th>";
						echo "	<th>Submittal Form Transmittal</th>";			
						echo "	<th>Deliverables List</th>";				
						echo "  <th>Actions</th>";
						echo "</tr>";
						echo "</thead>";
						echo "<tbody>";
						while($rows = mysqli_fetch_assoc($result))
						{
							echo "<tr>";
							echo "<td> ".$rows['Project_Number']." </td>";	
							echo "<td> ".$rows['Project_Name']." </td>";
							echo "<td> ".$rows['Ticket_Number']." </td>";
							echo "<td> ".$rows['Deadline']." </td>";
							echo "<td><a> ".$rows['Drawings']." </a></td>";
							echo "<td><a> ".$rows['Drawing_List']." </a></td>";
							echo "<td><a> ".$rows['Blueprint_Request']." </a></td>";
							echo "<td><a> ".$rows['Specifications']." </a></td>";
							echo "<td><a> ".$rows['Calculations']." </a></td>";
							echo "<td><a> ".$rows['Reports']." </a></td>";
							echo "<td><a> ".$rows['Outgoing_Email']." </a></td>";
							echo "<td><a> ".$rows['Submittal_Form_Transmittal']." </a></td>";
							echo "<td><a> ".$rows['Deliverables_List']." </a></td>";
							echo "<td><input id='deletebtn' class='".$rows['ID']."' type='submit' value='Delete' name='delete' /></td>";
							echo "<script>
											$(document).ready(function(){
												$('.".$rows['ID']."').on('click',function(){
													var value = $(this).val();
													$.ajax(
														{
															url:'dc_submit.php',
															type:'post',
															data:'delete_submittal='+value+
																	 '&submittal_id=".$rows['ID']."',
															success:function(data)
																{
																	$('#list').html(data);
																},
														});
												});
											});
										</script>";
							echo"</tr>";
						}
						echo "</tbody>";
						echo "</table>";
					}
			}
			else
					{
						$readsql = "SELECT dc_submittal.ID, 
												project.Project_Number, 
												project.Project_Name, 
												dc_submittal.Ticket_Number, 
												dc_submittal.Deadline, 
												dc_submittal.Drawings, 
												dc_submittal.Drawing_List, 
												dc_submittal.Blueprint_Request, 
												dc_submittal.Specifications, 
												dc_submittal.Calculations, 
												dc_submittal.Reports, 
												dc_submittal.Outgoing_Email, 
												dc_submittal.Submittal_Form_Transmittal, 
												dc_submittal.Deliverables_List
											FROM `dc_submittal`
											INNER JOIN project 
												ON dc_submittal.Project_ID = project.ID";
					$result = mysqli_query($conn,$readsql);
					if(mysqli_num_rows($result) > 0)
					{
						echo "<table class='general_table'>";
						echo "<thead>";
						echo "<tr>";
						echo "	<th>Project Number</th>";
						echo "	<th>Project Name</th>";
						echo "	<th>Ticket Number</th>";
						echo "	<th>Deadline</th>";
						echo "	<th>Drawings</th>";
						echo "	<th>Drawing List</th>";
						echo "	<th>Blueprint Request</th>";
						echo "	<th>Specifications</th>";
						echo "	<th>Calculations</th>";
						echo "	<th>Reports</th>";
						echo "	<th>Outgoing Email</th>";
						echo "	<th>Submittal Form Transmittal</th>";			
						echo "	<th>Deliverables List</th>";				
						echo "  <th>Actions</th>";
						echo "</tr>";
						echo "</thead>";
						echo "<tbody>";
						while($rows = mysqli_fetch_assoc($result))
						{
							echo "<tr>";
							echo "<td> ".$rows['Project_Number']." </td>";	
							echo "<td> ".$rows['Project_Name']." </td>";
							echo "<td> ".$rows['Ticket_Number']." </td>";
							echo "<td> ".$rows['Deadline']." </td>";
							echo "<td><a> ".$rows['Drawings']." </a></td>";
							echo "<td><a> ".$rows['Drawing_List']." </a></td>";
							echo "<td><a> ".$rows['Blueprint_Request']." </a></td>";
							echo "<td><a> ".$rows['Specifications']." </a></td>";
							echo "<td><a> ".$rows['Calculations']." </a></td>";
							echo "<td><a> ".$rows['Reports']." </a></td>";
							echo "<td><a> ".$rows['Outgoing_Email']." </a></td>";
							echo "<td><a> ".$rows['Submittal_Form_Transmittal']." </a></td>";
							echo "<td><a> ".$rows['Deliverables_List']." </a></td>";
							echo "<td><input id='deletebtn' class='".$rows['ID']."' type='submit' value='Delete' name='delete' /></td>";
							echo "<script>
											$(document).ready(function(){
												$('.".$rows['ID']."').on('click',function(){
													var value = $(this).val();
													$.ajax(
														{
															url:'dc_submit.php',
															type:'post',
															data:'delete_submittal='+value+
																	 '&submittal_id=".$rows['ID']."',
															success:function(data)
																{
																	$('#list').html(data);
																},
														});
												});
											});
										</script>";
							echo"</tr>";
						}
						echo "</tbody>";
						echo "</table>";
					}
					}
		}

		//edit Submittal
		if(isset($_POST['edit_submittal']))
		{
			$submittal_id = $_POST['submittal_id'];
			$project_id = $_POST['project_id'];
			$ticket_number = $_POST['ticket_number'];
			$deadline = $_POST['deadline'];
			$drawings = $_POST['drawings'];
			$drawing_list = $_POST['drawing_list'];
			$blueprint_request = $_POST['blueprint_request'];
			$specifications = $_POST['specifications'];
			$calculations = $_POST['calculations'];
			$reports = $_POST['reports'];
			$outgoing_email = $_POST['outgoing_email'];
			$submittal_form_transmittal = $_POST['submittal_form_transmittal'];
			$deliverables_list = $_POST['deliverables_list'];

			$sql = "UPDATE dc_submittal
							SET Project_ID=$project_id,
									Ticket_Number='$ticket_number',
									Deadline='$deadline',
									Drawings='$drawings',
									Drawing_List='$drawing_list',
									Blueprint_Request='$blueprint_request',
									Specifications='$specifications',
									Calculations='$calculations',
									Reports='$reports',
									Outgoing_Email='$outgoing_email',
									Submittal_Form_Transmittal='$submittal_form_transmittal',
									Deliverables_List='$deliverables_list'
							WHERE ID=$submittal_id";
			if(mysqli_query($conn,$sql))
			{
					$readsql = "SELECT dc_submittal.ID, 
												project.Project_Number, 
												project.Project_Name, 
												dc_submittal.Ticket_Number, 
												dc_submittal.Deadline, 
												dc_submittal.Drawings, 
												dc_submittal.Drawing_List, 
												dc_submittal.Blueprint_Request, 
												dc_submittal.Specifications, 
												dc_submittal.Calculations, 
												dc_submittal.Reports, 
												dc_submittal.Outgoing_Email, 
												dc_submittal.Submittal_Form_Transmittal, 
												dc_submittal.Deliverables_List
											FROM `dc_submittal`
											INNER JOIN project 
												ON dc_submittal.Project_ID = project.ID";
					$result = mysqli_query($conn,$readsql);
					if(mysqli_num_rows($result) > 0)
					{
						echo "<table class='general_table'>";
						echo "<thead>";
						echo "<tr>";
						echo "	<th>Project Number</th>";
						echo "	<th>Project Name</th>";
						echo "	<th>Ticket Number</th>";
						echo "	<th>Deadline</th>";
						echo "	<th>Drawings</th>";
						echo "	<th>Drawing List</th>";
						echo "	<th>Blueprint Request</th>";
						echo "	<th>Specifications</th>";
						echo "	<th>Calculations</th>";
						echo "	<th>Reports</th>";
						echo "	<th>Outgoing Email</th>";
						echo "	<th>Submittal Form Transmittal</th>";			
						echo "	<th>Deliverables List</th>";	 	
						echo "  <th>Actions</th>";
						echo "</tr>";
						echo "</thead>";
						echo "<tbody>";
						while($rows = mysqli_fetch_assoc($result))
						{
							echo "<tr>";
							echo "<td> ".$rows['Project_Number']." </td>";	
							echo "<td> ".$rows['Project_Name']." </td>";
							echo "<td> ".$rows['Ticket_Number']." </td>";
							echo "<td> ".$rows['Deadline']." </td>";
							echo "<td><a> ".$rows['Drawings']." </a></td>";
							echo "<td><a> ".$rows['Drawing_List']." </a></td>";
							echo "<td><a> ".$rows['Blueprint_Request']." </a></td>";
							echo "<td><a> ".$rows['Specifications']." </a></td>";
							echo "<td><a> ".$rows['Calculations']." </a></td>";
							echo "<td><a> ".$rows['Reports']." </a></td>";
							echo "<td><a> ".$rows['Outgoing_Email']." </a></td>";
							echo "<td><a> ".$rows['Submittal_Form_Transmittal']." </a></td>";
							echo "<td><a> ".$rows['Deliverables_List']." </a></td>";
							echo "<td><input id='deletebtn' class='".$rows['ID']."' type='submit' value='Delete' name='delete' /></td>";
							echo "<script>
											$(document).ready(function(){
												$('.".$rows['ID']."').on('click',function(){
													var value = $(this).val();
													$.ajax(
														{
															url:'dc_submit.php',
															type:'post',
															data:'delete_submittal='+value+
																	 '&submittal_id=".$rows['ID']."',
															success:function(data)
																{
																	$('#list').html(data);
																},
														});
												});
											});
										</script>";
							echo"</tr>";
						}
						echo "</tbody>";
						echo "</table>";
					}
			}
		}

		//delete Submittal
		if(isset($_POST['delete_submittal']))
		{
			$submittal_id = $_POST['submittal_id'];
			$sql = "DELETE FROM dc_submittal WHERE ID = $submittal_id";
			if(mysqli_query($conn,$sql))
			{
					$readsql = "SELECT dc_submittal.ID, 
												project.Project_Number, 
												project.Project_Name, 
												dc_submittal.Ticket_Number, 
												dc_submittal.Deadline, 
												dc_submittal.Drawings, 
												dc_submittal.Drawing_List, 
												dc_submittal.Blueprint_Request, 
												dc_submittal.Specifications, 
												dc_submittal.Calculations, 
												dc_submittal.Reports, 
												dc_submittal.Outgoing_Email, 
												dc_submittal.Submittal_Form_Transmittal, 
												dc_submittal.Deliverables_List
											FROM `dc_submittal`
											INNER JOIN project 
												ON dc_submittal.Project_ID = project.ID";
					$result = mysqli_query($conn,$readsql);
					if(mysqli_num_rows($result) > 0)
					{
						echo "<table class='general_table'>";
						echo "<thead>";
						echo "<tr>";
						echo "	<th>Project Number</th>";
						echo "	<th>Project Name</th>";
						echo "	<th>Ticket Number</th>";
						echo "	<th>Deadline</th>";
						echo "	<th>Drawings</th>";
						echo "	<th>Drawing List</th>";
						echo "	<th>Blueprint Request</th>";
						echo "	<th>Specifications</th>";
						echo "	<th>Calculations</th>";
						echo "	<th>Reports</th>";
						echo "	<th>Outgoing Email</th>";
						echo "	<th>Submittal Form Transmittal</th>";			
						echo "	<th>Deliverables List</th>";	
						echo "  <th>Actions</th>";
						echo "</tr>";
						echo "</thead>";
						echo "<tbody>";
						while($rows = mysqli_fetch_assoc($result))
						{
							echo "<tr>";
							echo "<td> ".$rows['Project_Number']." </td>";	
							echo "<td> ".$rows['Project_Name']." </td>";
							echo "<td> ".$rows['Ticket_Number']." </td>";
							echo "<td> ".$rows['Deadline']." </td>";
							echo "<td><a> ".$rows['Drawings']." </a></td>";
							echo "<td><a> ".$rows['Drawing_List']." </a></td>";
							echo "<td><a> ".$rows['Blueprint_Request']." </a></td>";
							echo "<td><a> ".$rows['Specifications']." </a></td>";
							echo "<td><a> ".$rows['Calculations']." </a></td>";
							echo "<td><a> ".$rows['Reports']." </a></td>";
							echo "<td><a> ".$rows['Outgoing_Email']." </a></td>";
							echo "<td><a> ".$rows['Submittal_Form_Transmittal']." </a></td>";
							echo "<td><a> ".$rows['Deliverables_List']." </a></td>";
							echo "<td><input id='deletebtn' class='".$rows['ID']."' type='submit' value='Delete' name='delete' /></td>";
							echo "<script>
											$(document).ready(function(){
												$('.".$rows['ID']."').on('click',function(){
													var value = $(this).val();
													$.ajax(
														{
															url:'dc_submit.php',
															type:'post',
															data:'delete_submittal='+value+
																	 '&submittal_id=".$rows['ID']."',
															success:function(data)
																{
																	$('#list').html(data);
																},
														});
												});
											});
										</script>";
							echo"</tr>";
						}
						echo "</tbody>";
						echo "</table>";
					}
			}
		}
		//save E-mail Log
		if(isset($_POST['email_log']))
		{
			$project_id = $_POST['project_id'];
			$email_link = $_POST['email_link'];
			$classification = $_POST['classification'];

			$sql = "INSERT INTO dc_email_log(Project_ID, Email_Link, Classification)
							 VALUES($project_id,'$email_link','$classification')";
			if(mysqli_query($conn,$sql))
			{
				$readsql = "SELECT project.Project_Number, 
														 project.Project_Name,
														 dc_email_log.ID,
														 dc_email_log.Email_Link, 
														 dc_email_log.Classification 
											FROM dc_email_log 
											INNER JOIN project ON dc_email_log.Project_ID = project.ID";
					$result = mysqli_query($conn,$readsql);
					if(mysqli_num_rows($result) > 0)
					{
						echo "<table class='general_table'>";
							echo "<thead>";
							echo "<tr>";
							echo "	<th>Project</th>";
							echo "	<th>Email Link</th>";
							echo "	<th>Classification</th>";
							echo "  <th>Action</th>";
							echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
						while($rows = mysqli_fetch_assoc($result))
						{
							
							echo "<tr>";											
							echo "	<td> ".$rows['Project_Number']." - ".$rows['Project_Name']."</td>";
							echo "	<td><a> ".$rows['Email_Link']." </a></td>";
							echo "	<td> ".$rows['Classification']." </td>";
							echo "<td><input id='deletebtn' class='".$rows['ID']."' type='submit' value='Delete' name='delete' /></td>";
							echo "<script>
											$(document).ready(function(){
												$('.".$rows['ID']."').on('click',function(){
													var value = $(this).val();
													$.ajax(
														{																					 
															url:'dc_submit.php',
															type:'post',
															data:'delete_email='+value+
																	 '&email_id=".$rows['ID']."',
															success:function(data)
																{
																	$('#list').html(data);
																},
														});
												});
											});
										</script>";
							echo "</tr>";
						}				
							echo "</tbody>";
							echo "</table>";
					}
			}
			else
			{
				$readsql = "SELECT project.Project_Number, 
														 project.Project_Name,
														 dc_email_log.ID,
														 dc_email_log.Email_Link, 
														 dc_email_log.Classification 
											FROM dc_email_log 
											INNER JOIN project ON dc_email_log.Project_ID = project.ID";
					$result = mysqli_query($conn,$readsql);
					if(mysqli_num_rows($result) > 0)
					{
						echo "<table class='general_table'>";
							echo "<thead>";
							echo "<tr>";
							echo "	<th>Project</th>";
							echo "	<th>Email Link</th>";
							echo "	<th>Classification</th>";
							echo "  <th>Action</th>";
							echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
						while($rows = mysqli_fetch_assoc($result))
						{
							
							echo "<tr>";											
							echo "	<td> ".$rows['Project_Number']." - ".$rows['Project_Name']."</td>";
							echo "	<td><a> ".$rows['Email_Link']." </a></td>";
							echo "	<td> ".$rows['Classification']." </td>";
							echo "<td><input id='deletebtn' class='".$rows['ID']."' type='submit' value='Delete' name='delete' /></td>";
							echo "<script>
											$(document).ready(function(){
												$('.".$rows['ID']."').on('click',function(){
													var value = $(this).val();
													$.ajax(
														{																					 
															url:'dc_submit.php',
															type:'post',
															data:'delete_email='+value+
																	 '&email_id=".$rows['ID']."',
															success:function(data)
																{
																	$('#list').html(data);
																},
														});
												});
											});
										</script>";
							echo "</tr>";
						}				
							echo "</tbody>";
							echo "</table>";
					}
			}
		}

		//delete E-mail Log
		if(isset($_POST['delete_email']))
		{
			$email_id = $_POST['email_id'];
			$sql = "DELETE FROM dc_email_log WHERE ID = $email_id";
			if(mysqli_query($conn,$sql))
			{
				$readsql = "SELECT project.Project_Number, 
														 project.Project_Name,
														 dc_email_log.ID,
														 dc_email_log.Email_Link, 
														 dc_email_log.Classification 
											FROM dc_email_log 
											INNER JOIN project ON dc_email_log.Project_ID = project.ID";
					$result = mysqli_query($conn,$readsql);
					if(mysqli_num_rows($result) > 0)
					{
						echo "<table class='general_table'>";
							echo "<thead>";
							echo "<tr>";
							echo "	<th>Project</th>";
							echo "	<th>Email Link</th>";
							echo "	<th>Classification</th>";
							echo "  <th>Action</th>";
							echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
						while($rows = mysqli_fetch_assoc($result))
						{
							
							echo "<tr>";											
							echo "	<td> ".$rows['Project_Number']." - ".$rows['Project_Name']."</td>";
							echo "	<td><a> ".$rows['Email_Link']." </a></td>";
							echo "	<td> ".$rows['Classification']." </td>";
							echo "<td><input id='deletebtn' class='".$rows['ID']."' type='submit' value='Delete' name='delete' /></td>";
							echo "<script>
											$(document).ready(function(){
												$('.".$rows['ID']."').on('click',function(){
													var value = $(this).val();
													$.ajax(
														{																					 
															url:'dc_submit.php',
															type:'post',
															data:'delete_email='+value+
																	 '&email_id=".$rows['ID']."',
															success:function(data)
																{
																	$('#list').html(data);
																},
														});
												});
											});
										</script>";
							echo "</tr>";
						}				
							echo "</tbody>";
							echo "</table>";
					}
			}
		}

		//save Meeting Log
		if(isset($_POST['meeting_log']))
		{
			$project_id = $_POST['project_id'];
			$scanned_copy_link = $_POST['scanned_copy_link'];
			$commitments = $_POST['commitments'];	
			$mc_no = $_POST['mc_no'];

			$sql = "INSERT INTO dc_meeting_log(Project_ID, Scanned_Copy_Link, MC_No, Commitments)
							VALUES($project_id,'$scanned_copy_link','$commitments','$mc_no')";
			if(mysqli_query($conn,$sql))
			{
				$readsql = "SELECT project.Project_Number, 
														 project.Project_Name,
														 dc_meeting_log.ID,
														 dc_meeting_log.Scanned_Copy_Link, 
														 dc_meeting_log.MC_No, 
														 dc_meeting_log.Commitments 
											FROM dc_meeting_log 
											INNER JOIN project ON dc_meeting_log.Project_ID = project.ID";
					$result = mysqli_query($conn,$readsql);
					if(mysqli_num_rows($result) > 0)
					{
						echo "<table class='general_table'>";
							echo "<thead>";
							echo "<tr>";
							echo "	<th>Project</th>";
							echo "	<th>Scanned Copy Link</th>";			 
							echo "	<th>Meeting Confirmation No.</th>";
							echo "	<th>Commitments</th>";
							echo "  <th>Actions</th>";
							echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
						while($rows = mysqli_fetch_assoc($result))
						{
							echo "<tr>";											
							echo "	<td> ".$rows['Project_Number']." - ".$rows['Project_Name']."</td>";							
							echo "	<td><a> ".$rows['Scanned_Copy_Link']." </a></td>";		 
							echo "	<td> ".$rows['MC_No']." </td>";
							echo "	<td> ".$rows['Commitments']." </td>";
							echo "<td><input id='deletebtn' class='".$rows['ID']."' type='submit' value='Delete' name='delete' /></td>";
							echo "<script>
											$(document).ready(function(){
												$('.".$rows['ID']."').on('click',function(){
													var value = $(this).val();
													$.ajax(
														{																					 
															url:'dc_submit.php',
															type:'post',
															data:'delete_meeting_log='+value+
																	 '&meeting_log_id=".$rows['ID']."',
															success:function(data)
																{
																	$('#list').html(data);
																},
														});
												});
											});
										</script>";
							echo "</tr>";
						}				
							echo "</tbody>";
							echo "</table>";
					}
			}
		}

		//delete Meeting Log
		if(isset($_POST['delete_meeting_log']))
		{
			$meeting_log_id = $_POST['meeting_log_id'];
			$sql = "DELETE FROM dc_meeting_log WHERE ID = $meeting_log_id";
			if(mysqli_query($conn,$sql))
			{
				$readsql = "SELECT project.Project_Number, 
														 project.Project_Name,
														 dc_meeting_log.ID,
														 dc_meeting_log.Scanned_Copy_Link, 
														 dc_meeting_log.MC_No, 
														 dc_meeting_log.Commitments 
											FROM dc_meeting_log 
											INNER JOIN project ON dc_meeting_log.Project_ID = project.ID";
					$result = mysqli_query($conn,$readsql);
					if(mysqli_num_rows($result) > 0)
					{
						echo "<table class='general_table'>";
							echo "<thead>";
							echo "<tr>";
							echo "	<th>Project</th>";
							echo "	<th>Scanned Copy Link</th>";			 
							echo "	<th>Meeting Confirmation No.</th>";
							echo "	<th>Commitments</th>";
							echo "  <th>Actions</th>";
							echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
						while($rows = mysqli_fetch_assoc($result))
						{
							echo "<tr>";											
							echo "	<td> ".$rows['Project_Number']." - ".$rows['Project_Name']."</td>";							
							echo "	<td><a> ".$rows['Scanned_Copy_Link']." </a></td>";		 
							echo "	<td> ".$rows['MC_No']." </td>";
							echo "	<td> ".$rows['Commitments']." </td>";
							echo "<td><input id='deletebtn' class='".$rows['ID']."' type='submit' value='Delete' name='delete' /></td>";
							echo "<script>
											$(document).ready(function(){
												$('.".$rows['ID']."').on('click',function(){
													var value = $(this).val();
													$.ajax(
														{																					 
															url:'dc_submit.php',
															type:'post',
															data:'delete_meeting_log='+value+
																	 '&meeting_log_id=".$rows['ID']."',
															success:function(data)
																{
																	$('#list').html(data);
																},
														});
												});
											});
										</script>";
							echo "</tr>";
						}				
							echo "</tbody>";
							echo "</table>";
					}
			}
		}
		//SAVE DOCUMENTS
		if(isset($_POST['documents']))
		{
			$status = $_POST["status"];
			$project_id = $_POST["project_id"];
			$from_to = $_POST["from_to"];
			$attached_scanned_submittal = $_POST["attached_scanned_submittal"];

			$sql = "INSERT INTO dc_documents(Status, Project_ID, From_To, Attached_Scanned_Submittal) 
							VALUES('$status',$project_id,'$from_to','$attached_scanned_submittal')";
			if(mysqli_query($conn,$sql))
			{
				 $readsql = "SELECT dc_documents.ID,
														dc_documents.Status, 
														dc_documents.Project_ID, 
														dc_documents.From_To, 
														dc_documents.Attached_Scanned_Submittal,
														project.Project_Number,
														project.Project_Name
											FROM dc_documents 
											INNER JOIN project ON dc_documents.Project_ID = project.ID";
				 $result = mysqli_query($conn,$readsql);

				 if(mysqli_num_rows($result) > 0)
				 {	 
						echo "<table class='general_table'>";
							echo "<thead>";
							echo "<tr>";
							echo "	<th>Status</th>";
							echo "	<th>Project</th>";			 
							echo "	<th>From/To</th>";
							echo "	<th>Attached Scanned Submittal</th>";
							echo "  <th>Actions</th>";
							echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
						while($rows = mysqli_fetch_assoc($result))
						{
							echo "<tr>";						
							echo "<td> ".$rows["Status"]." </td>";					
							echo "	<td> ".$rows['Project_Number']." - ".$rows['Project_Name']."</td>";																					 
							echo "	<td> ".$rows['From_To']." </td>";
							echo "	<td><a> ".$rows['Attached_Scanned_Submittal']." </a></td>";
							echo "<td><input id='deletebtn' class='".$rows['ID']."' type='submit' value='Delete' name='delete' /></td>";
							echo "<script>
											$(document).ready(function(){
												$('.".$rows['ID']."').on('click',function(){
													var value = $(this).val();
													$.ajax(
														{																					 
															url:'dc_submit.php',
															type:'post',
															data:'delete_documents='+value+
																	 '&documents_id=".$rows['ID']."',
															success:function(data)
																{
																	$('#list').html(data);
																},
														});
												});
											});
										</script>";
							echo "</tr>";
						}				
							echo "</tbody>";
							echo "</table>";
				 }
			}
		}

		//delete Documents
		if(isset($_POST['delete_documents']))
		{
			$documents_id = $_POST['documents_id'];
			$sql = "DELETE FROM dc_documents WHERE ID = $documents_id";
			if(mysqli_query($conn,$sql))
			{
				 $readsql = "SELECT dc_documents.ID,
														dc_documents.Status, 
														dc_documents.Project_ID, 
														dc_documents.From_To, 
														dc_documents.Attached_Scanned_Submittal,
														project.Project_Number,
														project.Project_Name
											FROM dc_documents 
											INNER JOIN project ON dc_documents.Project_ID = project.ID";
				 $result = mysqli_query($conn,$readsql);

				 if(mysqli_num_rows($result) > 0)
				 {	 
						echo "<table class='general_table'>";
							echo "<thead>";
							echo "<tr>";
							echo "	<th>Status</th>";
							echo "	<th>Project</th>";			 
							echo "	<th>From/To</th>";
							echo "	<th>Attached Scanned Submittal</th>";
							echo "  <th>Actions</th>";
							echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
						while($rows = mysqli_fetch_assoc($result))
						{
							echo "<tr>";						
							echo "<td> ".$rows["Status"]." </td>";					
							echo "	<td> ".$rows['Project_Number']." - ".$rows['Project_Name']."</td>";																					 
							echo "	<td> ".$rows['From_To']." </td>";
							echo "	<td><a> ".$rows['Attached_Scanned_Submittal']." </a></td>";
							echo "<td><input id='deletebtn' class='".$rows['ID']."' type='submit' value='Delete' name='delete' /></td>";
							echo "<script>
											$(document).ready(function(){
												$('.".$rows['ID']."').on('click',function(){
													var value = $(this).val();
													$.ajax(
														{																					 
															url:'dc_submit.php',
															type:'post',
															data:'delete_documents='+value+
																	 '&documents_id=".$rows['ID']."',
															success:function(data)
																{
																	$('#list').html(data);
																},
														});
												});
											});
										</script>";
							echo "</tr>";
						}				
							echo "</tbody>";
							echo "</table>";
				 }
			}
		}
?>