<?php
session_start();
	include('connect.php');
	include('sql.php');
	$current_day = (int)date("d");
	$month_selected = (int)date("m");
	$year_selected = (int)date("Y");
	$totaldays=cal_days_in_month(CAL_GREGORIAN,$month_selected,$year_selected);
	$id = "";
	$trade = "";
	$deliverables = "";
	//INTERNAL DEADLINES FOR POPUP
	if(isset($_POST['internal_deadlines']))
	{
			$externaldl_id = $_POST["externaldl_id"];
		//INTERNAL DEADLINES
			$internal_deadlines = "";
			$internal_sql = "SELECT 
			                        internal_deadline.ID,
                              internal_deadline.Trade,
															internal_deadline.Day_ID,
															internal_deadline.Month_ID,
															internal_deadline.Year_ID,
															internal_deadline.Ticket_Number,
															external_deadline.External_Deadline,
															external_deadline.Day_ED,
															external_deadline.Month_ED,
															external_deadline.Year_ED
												FROM Internal_Deadline 
												INNER JOIN external_deadline 
													ON internal_deadline.Externaldl_ID = external_deadline.ID
												WHERE Externaldl_ID = $externaldl_id";
			$internal_result = mysqli_query($conn,$internal_sql);
			if(mysqli_num_rows($internal_result) > 0)
			{
				while($internal_rows = mysqli_fetch_assoc($internal_result))
				{
					//INTERNAL DATE
					$idl_day = $internal_rows["Day_ID"];
					$idl_month = $internal_rows["Month_ID"];
					$idl_year = $internal_rows["Year_ID"];

					if($idl_day < 10 && $idl_day > 0)
						$idl_day = "0".$idl_day;
					
					if($idl_month < 10 && $idl_month > 0)
						$idl_month = "0".$idl_month;

					//EXTERNAL DATE
					$edl_day = $internal_rows["Day_ED"];
					$edl_month = $internal_rows["Month_ED"];
					$edl_year = $internal_rows["Year_ED"];

					if($edl_day < 10 && $edl_day > 0)
						$edl_day = "0".$edl_day;
					
					if($edl_month < 10 && $edl_month > 0)
						$edl_month = "0".$edl_month;

					echo "<tr>
									<td> ".$internal_rows["Trade"]." </td>
									<td> <input type='date' id='indead".$internal_rows["ID"]."' max='$edl_year-$edl_month-$edl_day' value='$idl_year-$idl_month-$idl_day' /> </td>
									<td> ".$internal_rows["Ticket_Number"]." </td>
									<td> 
										<input type='submit' class='btn-normal bg-blue white bold' onclick='edit_idl".$internal_rows["ID"]."();' value='Edit' />
										<input type='submit' class='btn-normal bg-red white bold' onclick='delete_idl".$internal_rows["ID"]."();' value='Delete' />
									</td>
									<script>
										function edit_idl".$internal_rows["ID"]."(){
											$.ajax(
											{
												beforeSend:function(request){
													return confirm('Are you sure you want to edit?');
												},
												url:'opsSubmit.php',
												type:'post',
												data:'edit_internal_deadline=true'+
														 '&internaldl_id=".$internal_rows["ID"]."'+
														 '&internal_deadline='+$('#indead".$internal_rows["ID"]."').val()+
														 '&trade=".$internal_rows["Trade"]."'+
														 '&ticket=".$internal_rows["Ticket_Number"]."',
												success:function(data){
														alert(data);		
														$.ajax(
														{
															url:'main_calendar.php',
															type:'post',
															data:'by_all=true'+
																	'&month='+monthnumber+
																	'&year='+year+
																	'&lastday='+getDayName(monthnumber,year,1,0),
															success:function(data){
																$('#calendar').html(data);
															}
														});
												},
											});
										}

										function delete_idl".$internal_rows["ID"]."(){
											$.ajax(
											{ 
												beforeSend:function(request){
													return confirm('Are you sure you want to delete?');
												},
												url:'opsSubmit.php',
												type:'post',
												data:'delete_internal_deadline=true'+
														 '&internaldl_id=".$internal_rows["ID"]."'+
														 '&internal_deadline='+$('#indead".$internal_rows["ID"]."').val(),
												success:function(data){		
														$.ajax(
														{	
															url:'deadline.php',
															type:'post',
															data:'internal_deadlines=true'+
																		'&externaldl_id='+$('#pop_externaldl_id').val(),
															success:function(data)
															{
																$('#internal_list').html(data);
															}
														});

														$.ajax(
														{
															url:'main_calendar.php',
															type:'post',
															data:'by_all=true'+
																	'&month='+monthnumber+
																	'&year='+year+
																	'&lastday='+getDayName(monthnumber,year,1,0),
															success:function(data){
																$('#calendar').html(data);
															}
														});
												},
											});
										}
									</script>
								</tr>";
				}
			}
	}
	else if(isset($_POST["internal_deadlines_for_pm"]))
	{
		$externaldl_id = $_POST["externaldl_id"];
		//INTERNAL DEADLINES
			$internal_deadlines = "";
			$internal_sql = "SELECT Trade,
															Day_ID,
															Month_ID,
															Year_ID,
															Ticket_Number
												FROM Internal_Deadline 
												WHERE Externaldl_ID = $externaldl_id";
			$internal_result = mysqli_query($conn,$internal_sql);
			if(mysqli_num_rows($internal_result) > 0)
			{
				while($internal_rows = mysqli_fetch_assoc($internal_result))
				{
					echo "<tr>
									<td> ".$internal_rows["Trade"]." </td>
									<td> ".$internal_rows["Year_ID"]."-".$internal_rows["Month_ID"]."-".$internal_rows["Day_ID"]."</td>
									<td> ".$internal_rows["Ticket_Number"]." </td>
								</tr>";
				}
			}
	}
	else
	{
	echo "<div id='content_tray_submit'>";
	/* External Deadline Clicked from calendar.php */
	if(isset($_POST['request']))
	{
		//FROM calendar.php
		$id = $_POST['request'];
		$phase = $_POST['phase'];
		$externaldl_ID = "";
		$sql = "SELECT
						external_deadline.ID,
						DATE_FORMAT(external_deadline.External_Deadline, '%Y-%m-%d') as External_Deadline,
						external_deadline.Year_ED,
						external_deadline.Month_ED,
						external_deadline.Day_ED,
						external_deadline.Deliverables,
						project.ID as Project_ID,
						project.Project_Number,
						project.Project_Name,
						external_deadline.Phase,
            external_deadline.Percent
				FROM project
					INNER JOIN external_deadline ON external_deadline.Project_ID = project.ID
				WHERE project.ID = $id AND external_deadline.Phase = '$phase'";

		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0) {
			$row = mysqli_fetch_assoc($result);
			//EXTERNAL DEADLINES DETAILS
			$month = $row["Month_ED"];
			$day = $row["Day_ED"];
			if($month < 10)
			$month = "0".$month;
			if($day < 10)
			$day = "0".$day; 
	
			echo "<script>
							var forPop = 
										{
											project_id:'".$row["Project_ID"]."',
											project:'".$row['Project_Number']." - ".$row['Project_Name']."',
											phase:'".$row['Phase']."',
											external_deadline:'".$row["Year_ED"]."-$month-$day',
											deliverables:'".$row["Deliverables"]."',
											externaldl_id:'".$row['ID']."'	,
                      percent:'".$row['Percent']."'
										}; 
						</script>";
		}
	}

	if(isset($_POST['request_for_pm']))
	{
	//FROM pm.php
		$id = $_POST['request_for_pm'];
		$phase = $_POST['phase'];
		$externaldl_ID = "";
		$sql = "SELECT
						external_deadline.ID,
						DATE_FORMAT(external_deadline.External_Deadline, '%Y-%m-%d') as External_Deadline,
						external_deadline.Year_ED,
						external_deadline.Month_ED,
						external_deadline.Day_ED,
						external_deadline.Deliverables,
						project.ID as Project_ID,
						project.Project_Number,
						project.Project_Name,
						external_deadline.Phase
				FROM project
					INNER JOIN external_deadline ON external_deadline.Project_ID = project.ID
				WHERE project.ID = $id AND external_deadline.Phase = '$phase'";

		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0) {
			$row = mysqli_fetch_assoc($result);
			//EXTERNAL DEADLINES DETAILS
			$month = $row["Month_ED"];
			$day = $row["Day_ED"];
			if($month < 10)
			$month = "0".$month;
			if($day < 10)
			$day = "0".$day; 
	
			echo "<script>
							var forPop = 
										{			 
											project_id:'".$row["Project_ID"]."',
											project:'".$row['Project_Number']." - ".$row['Project_Name']."',
											phase:'".$row['Phase']."',
											external_deadline:'".$row["Year_ED"]."-$month-$day',
											deliverables:'".$row["Deliverables"]."',
											externaldl_id:'".$row['ID']."'											
										}; 
						</script>";
		}
	}
	/* Designer Deadline Clicked */
	if(isset($_POST['designerdl_id']))
	{
		$id = $_POST['internaldeadline'];
		$designerdl_id = $_POST['designerdl_id'];
		$sql = "SELECT DISTINCT project.Project_Number,
						project.Project_Name,
						team.Team_Name,
						external_deadline.External_Deadline,
						external_deadline.Deliverables,
						external_deadline.Day_ED,
						internal_deadline.ID,
						internal_deadline.Internal_Deadline,
						internal_deadline.Trade,
						internal_deadline.Ticket_Number
				FROM internal_deadline
					INNER JOIN external_deadline ON internal_deadline.Externaldl_ID = external_deadline.ID
					INNER JOIN project ON external_deadline.Project_ID = project.ID
					INNER JOIN team ON project.Team_ID = team.ID
					INNER JOIN designer_notif ON internal_deadline.ID = designer_notif.Internaldl_ID
				WHERE internal_deadline.ID = $id";

		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) >= 0) {
			$row = mysqli_fetch_assoc($result);
			echo "<h3 style='margin-bottom:0px;'> Project: </h3><label style='margin-left:16px;'> ".$row["Project_Number"]." - ".$row["Project_Name"]." </label>";
			echo "<h3 style='margin-bottom:0px;'> Deliverables: </h3><label style='margin-left:16px;'> ".$row["Deliverables"]." </label>";
			echo "<h3 style='margin-bottom:0px;'> External Deadline: </h3><label style='margin-left:16px;'> ".$row["External_Deadline"]." </label>";
			echo "<h3 style='margin-bottom:0px;'> Internal Deadline: </h3><label style='margin-left:16px;'> ".$row["Internal_Deadline"]." </label>";
			echo "<h3 style='margin-bottom:0px;'> Ticket Number: </h3><label style='margin-left:16px;'> ".$row["Ticket_Number"]." </label>";
		}
		$designernotifsql = "SELECT `ID`, `Budget`, `Status` FROM `designer_notif` WHERE ID = $designerdl_id";
		$resultdesignernotifsql = mysqli_query($conn,$designernotifsql);
		if(mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_assoc($resultdesignernotifsql);
			echo "<fieldset id='fset_legends'>
							<legend> <label><b>Allocated Budget Details</b></label> </legend>";
			echo "<dl>";
			echo "<dt><h3> Budget: ".$row['Budget']." hrs </h3></dt>";
			echo "<dt><h3> Status: ".$row['Status']." </h3></dt>";
			echo "</dl>
						</fieldset>";
		}
		//echo $sql;
	}
	/* Internal Deadline Clicked */
	if(isset($_POST['internal_details']))
	{
		$internaldl_id = $_POST["internaldl_id"];
		$sql = "SELECT
							project.Project_Number,
							project.Project_Name,
							external_deadline.External_Deadline,
							external_deadline.Deliverables,
							internal_deadline.Ticket_Number,
							internal_deadline.Internal_Deadline
						FROM internal_deadline
						INNER JOIN external_deadline
							ON internal_deadline.Externaldl_ID = external_deadline.ID
						INNER JOIN project
							ON external_deadline.Project_ID = project.ID
						WHERE internal_deadline.ID = $internaldl_id";
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_assoc($result);
			echo "<h3 style='margin-bottom:0px;'> Project: </h3><label style='margin-left:16px;'> ".$row["Project_Number"]." - ".$row["Project_Name"]." </label>";
			echo "<h3 style='margin-bottom:0px;'> Deliverables: </h3><label style='margin-left:16px;'> ".$row["Deliverables"]." </label>";
			echo "<h3 style='margin-bottom:0px;'> External Deadline: </h3><label style='margin-left:16px;'> ".$row["External_Deadline"]." </label>";
			echo "<h3 style='margin-bottom:0px;'> Internal Deadline: </h3><label style='margin-left:16px;'> ".$row["Internal_Deadline"]." </label>";
			echo "<h3 style='margin-bottom:0px;'> Ticket Number: </h3><label style='margin-left:16px;'> ".$row["Ticket_Number"]." </label>";
		}
	}

	echo "<input type='text' id='request' value='$id' style='display:none;'/>";
	echo "</div>";
	}
?>

