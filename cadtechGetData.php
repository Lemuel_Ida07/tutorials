<?php
session_start();
include("connect.php");
if(isset($_POST["get_cad_tracking"]))
  {
    $status = $_POST["status"];
    
    $curr_time = date("h:i");
		$cadtech_id = $_SESSION['ID'];
		$dnotif_id = 0;
		$curr_year = date("Y");
		$curr_month = date("m");
		$curr_day = date("d");
    $tradeString = $_SESSION["Trade"];
    
		if($curr_day <= 15)
    {
      $day_limit = 15;																																																																													 
    }
    if($curr_day > 15)
		{
      $day_limit = 31; 
    }
    
    $sql = "SELECT 
              cadtech_notif.ID,
              CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname)
              AS Designer,
              cadtech_notif.Deadline,
              CONCAT(project.Project_Number,' - ',
                     project.Project_Name,' - ',
                     internal_deadline.Phase) AS Project,
              review_checklist.Check_Item,
              cadtech_notif.Budget,
              internal_deadline.Ticket_Number,
              cadtech_notif.Status,
              requirements_for_des.ID AS req_for_des_id,
              designer_notif.ID AS DNotif_ID,
              project.Project_Number,
              project.Project_Name,
              requirements_for_des.Status AS rfdStatus
              FROM cadtech_notif
              INNER JOIN project
                ON cadtech_notif.Project_ID = project.ID
              INNER JOIN requirements_for_des
                ON cadtech_notif.ReqForDes_ID = requirements_for_des.ID
              INNER JOIN designer_notif
                ON requirements_for_des.DNotif_ID = designer_notif.ID
              INNER JOIN internal_deadline
                ON designer_notif.Internaldl_ID = internal_deadline.ID
              INNER JOIN user
                ON designer_notif.Designer_ID = user.ID
              INNER JOIN review_checklist
                ON requirements_for_des.ReviewChecklist_ID = review_checklist.ID
              WHERE 
                cadtech_notif.CADTech_ID = $cadtech_id
                AND cadtech_notif.CadTech_ID = $cadtech_id";
    $result = mysqli_query($conn,$sql);
    if(mysqli_num_rows($result) > 0)
    {
        $working_count = 0;
        $pending_count = 0;
        $approved_count = 0;
        $rejected_count = 0;
      while($rows = mysqli_fetch_assoc($result))
      {
        switch($status)
        {
          case "Standby":
            if($rows["Status"] == "Standby"){
              $pending_count ++;
            echo "<tr id='tr".$rows["ID"]."'>";
              echo "<td> ".$rows["Designer"]." </td>
                <td class='emphasize'> ".$rows["Deadline"]." </td>
                <td> ".$rows["Project"]." </td>
                <td> ".$rows["Check_Item"]." </td>
                <td class='emphasize'> ".$rows["Budget"]." Hours </td>
                <td> ".$rows["Ticket_Number"]." </td>
                <td>";
              echo"<input 
                type='button' 
                value='START' 
                id='start_btn".$rows["ID"]."' 
                class='btn-normal bg-green white bold' 
                />";
              echo "<script>
                      $('#pending_count').html('($pending_count)');
                    </script>";
              echo "<script>
                
                $('#start_btn".$rows["ID"]."').on('click',function(){
                  $.ajax({
                    beforeSend:function(e){
                      return confirm('Are you sure you want to start?');
                    },
                    url:'cadtechSubmit.php',
                    type:'post',
                    data:'start_working=true'+
                         '&cadtech_notif_id=".$rows["ID"]."',
                    success:function(data){
                      location.reload();
                    }
                  });
                });
                
                $('#request_btn".$rows["ID"]."').on('click',function(){
                  $.ajax({
                    beforeSend:function(){
                      return confirm('Are you sure you want to request?');
                    },
                    url:'cadtechSubmit.php',
                    type:'post',
                    data:'req_for_des_id=".$rows["req_for_des_id"]."'+
                         '&cadtech_notif_id=".$rows["ID"]."',
                    success:function(data){
                      location.reload();
                    }
                  });
                });
              </script>";
        echo "</td></tr>";
            }
            break;
          case "Working":
            if($rows["Status"] == "Working")
            {
              $working_count ++;
              echo "<tr id='tr".$rows["ID"]."'>";
              echo "<td> ".$rows["Designer"]." </td>
                  <td class='emphasize'> ".$rows["Deadline"]." </td>
                  <td> ".$rows["Project"]." </td>
                  <td> ".$rows["Check_Item"]." </td>
                  <td class='emphasize'> ".$rows["Budget"]." Hours </td>
                  <td> ".$rows["Ticket_Number"]." </td>
                  <td>";
                  echo"<input 
                    type='button' 
                    value='REQUEST' 
                    id='request_btn".$rows["ID"]."' 
                    class='btn-normal bg-blue white bold' 
                    />";
              echo "<script>
                      $('#working_count').html('($working_count)');
                    </script>";
              echo "<script>
                
                $('#start_btn".$rows["ID"]."').on('click',function(){
                  $.ajax({
                    beforeSend:function(e){
                      return confirm('Are you sure you want to start?');
                    },
                    url:'cadtechSubmit.php',
                    type:'post',
                    data:'start_working=true'+
                         '&cadtech_notif_id=".$rows["ID"]."',
                    success:function(data){
                      location.reload();
                    }
                  });
                });
                
                $('#request_btn".$rows["ID"]."').on('click',function(){
                  $.ajax({
                    beforeSend:function(){
                      return confirm('Are you sure you want to request?');
                    },
                    url:'cadtechSubmit.php',
                    type:'post',
                    data:'req_for_des_id=".$rows["req_for_des_id"]."'+
                         '&cadtech_notif_id=".$rows["ID"]."',
                    success:function(data){
                      location.reload();
                    }
                  });
                });
              </script>";
        echo "</td></tr>";
            }
            break;
          case "CADRequested":
            if($rows["Status"] == "CAD Requested")
            {
              $working_count ++;
              echo "<tr id='tr".$rows["ID"]."'>";
              echo "<td> ".$rows["Designer"]." </td>
                  <td class='emphasize'> ".$rows["Deadline"]." </td>
                  <td> ".$rows["Project"]." </td>
                  <td> ".$rows["Check_Item"]." </td>
                  <td class='emphasize'> ".$rows["Budget"]." Hours </td>
                  <td> ".$rows["Ticket_Number"]." </td>
                  <td>";
                  echo" <td>
                          <input 
                          type='button' 
                          value='C' 
                          class='btn-no bg-blue white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='D' 
                          class='btn-no bg-gray white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='R' 
                          class='btn-no bg-gray white emphasize' 
                          />
                        </td>";  
                  /*echo"<input 
                    type='button' 
                    value='REQUESTED' 
                    id='request_btn".$rows["ID"]."' 
                    class='btn-no bg-white blue emphasize' 
                    />";*/
              echo "<script>
                      $('#requested_count').html('($working_count)');
                    </script>";
        echo "</td></tr>";
            }
            break;
          case "CADAccepted":
            if($rows["Status"] == "CAD Accepted")
            {
              $approved_count ++;
              echo "<tr id='tr".$rows["DNotif_ID"]."'>";
              echo "
                    <td> ".$rows["Designer"]." </td>
                    <td class='emphasize'> ".$rows["Deadline"]." </td>
                    <td> ".$rows["Project"]." </td>
                    <td> ".$rows["Check_Item"]." </td>
                    <td> ".$rows["Budget"]." Hours </td>
                    <td> ".$rows["Ticket_Number"]." </td>";
              echo "<td style='display:none;'>
                      <script> 
                     $('#approved_count').html('($approved_count)');
                        </script>
                      </td>";
              echo" <td>
                          <input 
                          type='button' 
                          value='C' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='D' 
                          class='btn-no bg-gray white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='R' 
                          class='btn-no bg-gray white emphasize' 
                          />
                        </td>
                        </tr>";  
            }
            if($rows["Status"] == "Designer CAD Accepted")
            {
                  $approved_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                  echo "<td> ".$rows["Designer"]." </td>
                        <td class='emphasize'> ".$rows["Deadline"]." </td>
                        <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                        <td> ".$rows["Check_Item"]." </td>
                    <td> ".$rows["Budget"]." Hours </td>
                        <td> ".$rows["Ticket_Number"]." </td>";
                   echo "<td style='display:none;'>
                      <script> 
                     $('#approved_count').html('($approved_count)');
                        </script>
                      </td>";
                  echo" <td>
                          <input 
                          type='button' 
                          value='C' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='D' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='R' 
                          class='btn-no bg-gray white emphasize' 
                          />
                        </td>
                        </tr>";  
                  
            }
            if($rows["Status"] == "Reviewer CAD Accepted")
            {
                  $approved_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                  echo "<td> ".$rows["Designer"]." </td>
                        <td class='emphasize'> ".$rows["Deadline"]." </td>
                        <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                        <td> ".$rows["Check_Item"]." </td>
                    <td> ".$rows["Budget"]." Hours </td>
                        <td> ".$rows["Ticket_Number"]." </td>";
                   echo "<td style='display:none;'>
                      <script> 
                     $('#approved_count').html('($approved_count)');
                        </script>
                      </td>";
                  echo" <td>
                          <input 
                          type='button' 
                          value='C' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='D' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='R' 
                          class='btn-no bg-green white emphasize' 
                          />
                        </td>
                        </tr>";  
                  
            }
          break;
          case "CADRejected":
            if($rows["Status"] == "CAD Rejected"){
              $rejected_count ++;
              echo "<tr id='tr".$rows["ID"]."'>";
              echo "<td> ".$rows["Designer"]." </td>
                <td class='emphasize'> ".$rows["Deadline"]." </td>
                <td> ".$rows["Project"]." </td>
                <td> ".$rows["Check_Item"]." </td>
                <td class='emphasize'> ".$rows["Budget"]." Hours </td>
                <td> ".$rows["Ticket_Number"]." </td>";
              echo"<td>
                    <input 
                    type='button' 
                    value='C' 
                    class='btn-no bg-red white emphasize' 
                    />

                    <input 
                    type='button' 
                    value='D' 
                    class='btn-no bg-gray white emphasize' 
                    />

                    <input 
                    type='button' 
                    value='R' 
                    class='btn-no bg-gray white emphasize' 
                    />
                  </td>";  
              echo
                "<td><input 
                type='button' 
                value='START' 
                id='start_btn".$rows["ID"]."' 
                class='btn-normal bg-green white bold' 
                />";
              echo "<script>
                      $('#rejected_count').html('($rejected_count)');
                    </script>";
              echo "<script>
                $('#start_btn".$rows["ID"]."').on('click',function(){
                  $.ajax({
                    beforeSend:function(e){
                      return confirm('Are you sure you want to start?');
                    },
                    url:'cadtechSubmit.php',
                    type:'post',
                    data:'start_working=true'+
                         '&cadtech_notif_id=".$rows["ID"]."',
                    success:function(data){
                      location.reload();
                    }
                  });
                });
                
                $('#request_btn".$rows["ID"]."').on('click',function(){
                  $.ajax({
                    beforeSend:function(){
                      return confirm('Are you sure you want to request?');
                    },
                    url:'cadtechSubmit.php',
                    type:'post',
                    data:'req_for_des_id=".$rows["req_for_des_id"]."'+
                         '&cadtech_notif_id=".$rows["ID"]."',
                    success:function(data){
                      location.reload();
                    }
                  });
                });
              </script>";
              echo "</td></tr>";
            }
            if($rows["Status"] == "Designer CAD Rejected"){
              $rejected_count ++;
              echo "<tr id='tr".$rows["ID"]."'>";
              echo "<td> ".$rows["Designer"]." </td>
                <td class='emphasize'> ".$rows["Deadline"]." </td>
                <td> ".$rows["Project"]." </td>
                <td> ".$rows["Check_Item"]." </td>
                <td class='emphasize'> ".$rows["Budget"]." Hours </td>
                <td> ".$rows["Ticket_Number"]." </td>";
              echo"<td>
                    <input 
                    type='button' 
                    value='C' 
                    class='btn-no bg-green white emphasize' 
                    />

                    <input 
                    type='button' 
                    value='D' 
                    class='btn-no bg-red white emphasize' 
                    />

                    <input 
                    type='button' 
                    value='R' 
                    class='btn-no bg-gray white emphasize' 
                    />
                  </td>";  
              echo
                "<td><input 
                type='button' 
                value='START' 
                id='start_btn".$rows["ID"]."' 
                class='btn-normal bg-green white bold' 
                />";
              echo "<script>
                      $('#rejected_count').html('($rejected_count)');
                    </script>";
              echo "<script>
                $('#start_btn".$rows["ID"]."').on('click',function(){
                  $.ajax({
                    beforeSend:function(e){
                      return confirm('Are you sure you want to start?');
                    },
                    url:'cadtechSubmit.php',
                    type:'post',
                    data:'start_working=true'+
                         '&cadtech_notif_id=".$rows["ID"]."',
                    success:function(data){
                      location.reload();
                    }
                  });
                });
                
                $('#request_btn".$rows["ID"]."').on('click',function(){
                  $.ajax({
                    beforeSend:function(){
                      return confirm('Are you sure you want to request?');
                    },
                    url:'cadtechSubmit.php',
                    type:'post',
                    data:'req_for_des_id=".$rows["req_for_des_id"]."'+
                         '&cadtech_notif_id=".$rows["ID"]."',
                    success:function(data){
                      location.reload();
                    }
                  });
                });
              </script>";
              echo "</td></tr>";
            }
            if($rows["Status"] == "Reviewer CAD Rejected"){
              $rejected_count ++;
              echo "<tr id='tr".$rows["ID"]."'>";
              echo "<td> ".$rows["Designer"]." </td>
                <td class='emphasize'> ".$rows["Deadline"]." </td>
                <td> ".$rows["Project"]." </td>
                <td> ".$rows["Check_Item"]." </td>
                <td class='emphasize'> ".$rows["Budget"]." Hours </td>
                <td> ".$rows["Ticket_Number"]." </td>";
              echo"<td>
                    <input 
                    type='button' 
                    value='C' 
                    class='btn-no bg-green white emphasize' 
                    />

                    <input 
                    type='button' 
                    value='D' 
                    class='btn-no bg-green white emphasize' 
                    />

                    <input 
                    type='button' 
                    value='R' 
                    class='btn-no bg-red white emphasize' 
                    />
                  </td>";  
              echo
                "<td><input 
                type='button' 
                value='START' 
                id='start_btn".$rows["ID"]."' 
                class='btn-normal bg-green white bold' 
                />";
              echo "<script>
                      $('#rejected_count').html('($rejected_count)');
                    </script>";
              echo "<script>
                $('#start_btn".$rows["ID"]."').on('click',function(){
                  $.ajax({
                    beforeSend:function(e){
                      return confirm('Are you sure you want to start?');
                    },
                    url:'cadtechSubmit.php',
                    type:'post',
                    data:'start_working=true'+
                         '&cadtech_notif_id=".$rows["ID"]."',
                    success:function(data){
                      location.reload();
                    }
                  });
                });
                
                $('#request_btn".$rows["ID"]."').on('click',function(){
                  $.ajax({
                    beforeSend:function(){
                      return confirm('Are you sure you want to request?');
                    },
                    url:'cadtechSubmit.php',
                    type:'post',
                    data:'req_for_des_id=".$rows["req_for_des_id"]."'+
                         '&cadtech_notif_id=".$rows["ID"]."',
                    success:function(data){
                      location.reload();
                    }
                  });
                });
              </script>";
              echo "</td></tr>";
            }
            break;
        }
      }
    }
  }