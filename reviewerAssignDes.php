<?php
include("connect.php");
//get Code for reviewer_deadline pop
	if(isset($_POST["get_code"]))
	{
		$trade = $_POST["trade"];
		$phase = $_POST["phase"];
		$sql = "SELECT 
							ID,
							Code 
						FROM review_checklist
						WHERE 
							Trade = '$trade'
							AND Phase = '$phase'";
		$result = mysqli_query($conn,$sql);
		echo $sql;
		if(mysqli_num_rows($result) > 0)
		{
			echo "<option value='0'> - Select Code - </option>";
			echo "<option value='custom'> Custom Activity </option>";
			while($row = mysqli_fetch_assoc($result))
			{
				echo "<option value='".$row["ID"]."'> ".$row["Code"]." </option>";
			}	
		}
		else
		{
			echo "<option value='0'> - Select Code - </option>";
			echo "<option value='custom'> Custom Activity </option>";
		}
	}

	//get requirements Checlist
	if(isset($_POST["getRequirements"]))
	{
		$code_id = $_POST["code_id"];
		$dnotif_id = 0;
		$sql = "SELECT 
							ID,
							Requirements
						FROM requirements_checklist
						WHERE review_checklist_id=$code_id";
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			while($rows = mysqli_fetch_assoc($result))
			{
				echo "<tr>
						<td id=recq".$rows["Requirements"].">".$rows["Requirements"]."</td>
						<td style='width:150px;'> <input type='number' id=expected".$rows["ID"]." min='0' value='0'/> </td>	
					</tr>";
			}
		}
		else
		{
			echo "<tr><td><script>
							$('#review_checklist').hide();
						</script></td></tr>";
		}
	}

	//get requirements for designer

	if(isset($_POST["getRequirementsForDes"]))
	{
		$dnotif_id = $_POST["dnotif_id"];
		$sql = "SELECT 
							ID,
							Requirement,
							Expected
						FROM requirements_for_des
						WHERE DNotif_ID = $dnotif_id";
						echo $sql;
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			while($rows = mysqli_fetch_assoc($result))
			{
				echo "<tr>
						<td id=recq".$rows["Requirement"].">".$rows["Requirement"]."</td>
						<td style='width:150px;'> <input type='number' id=expected".$rows["ID"]." min='0' value='".$rows["Expected"]."'/> </td>	
					</tr>";
			}
		}
		else
		{
			echo "<tr><td><script>
							$('#review_checklist').hide();
						</script></td></tr>";
		}
	}

	//show requirements in designer's page 
	if(isset($_POST["showRequirements"]))
		{
			$dnotif_id = $_POST["dnotif_id"];
			$sql = "SELECT 
								requirements_for_des.ID,
								review_checklist.Check_Item,
                requirements_for_des.Status
							FROM requirements_for_des
              INNER JOIN review_checklist
                ON requirements_for_des.ReviewChecklist_ID = review_checklist.ID
							WHERE requirements_for_des.DNotif_ID = $dnotif_id
                AND requirements_for_des.Budget != 0";
			$result = mysqli_query($conn,$sql);
      echo $sql;
			if(mysqli_num_rows($result) > 0)
			{
				while($rows = mysqli_fetch_assoc($result))
				{
					echo "<tr class='bg-white'>
							<td id=recq".$rows["ID"].">".$rows["Check_Item"]." </td>";
          echo"
              <td><input type='checkbox'";
            if($rows["Status"] == "Submitted")
              {
                echo "checked='checked'";
              }
          echo "class='budget_inp' value='".$rows["ID"]."'/></td>
						</tr>";
				}
			}
			else
			{
				echo "<tr><td colspan='2' style='text-align:center;'> No Requirements </td></tr>";
			}
		}
    
  //show actual requirements in designer's page
  if(isset($_POST["showActualRequirements"]))
  {
    $dnotif_id = $_POST["dnotif_id"];
			$sql = "SELECT 
								ID,
								Requirement,
								Expected,
                Completed
							FROM requirements_for_des
							WHERE DNotif_ID = $dnotif_id";
			$result = mysqli_query($conn,$sql);
			if(mysqli_num_rows($result) > 0)
			{
				while($rows = mysqli_fetch_assoc($result))
				{
					echo "<tr class='bg-white'>
							<td id=recq".$rows["Requirement"].">".$rows["Requirement"]."</td>
							<td style='width:150px;'> ".$rows["Expected"]." </td>	
							<td style='width:150px;'> ".$rows["Completed"]." </td>
              <td style='width:150px;'> <input type='number' id=expected".$rows["ID"]." min='0' value='0'/> </td>	
						</tr>";
				}
			}
			else
			{
				echo "<tr><td colspan='4' style='text-align:center;'> No Requirements </td></tr>";
			}
  }
?>