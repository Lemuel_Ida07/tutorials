<?php 
    session_start();
    include('connect.php');

	$today = date("F j, Y");
?>
<style>
	#menu_item_logo5
	{
		background-color:#f2f2f2;
	}
	#menu_item_logo5:hover
	{
		background-color:#f2f2f2;
	}
	#tab5
	{
		color:#515151;
		font-weight: bold;
		text-shadow: none;
	}

    #topic{
        border:none;
    }
</style>
<div id="info" style="width:100%;margin-left:0;padding-left:0;max-width:100%;">
				<h2> Documents </h2>
				<h2><?php echo $_SESSION['Team_Name']; ?></h2>
				<h3 style="padding-bottom:0;margin-bottom:0;"><?php echo $today; ?></h3>
					<div id="topic" >
					<h2 style="visibility:hidden;"> Incoming E-mail Log </h2>
					<label> Status </label>
					<select style="padding:3%;"name="status" id="status" required="required">
						<option> IN </option>
						<option> OUT </option>
					</select>
					</div>
                    <div id="topic" >
					<h2 style="visibility:hidden;"> Incoming E-mail Log </h2>
					<label> Project </label>
					<select style="padding:3%;"name="ProjectNumber" id="ProjectNumber" required="required">
                        <option> Select Project Number </option>
							<?php
								 $designer_id = $_SESSION["ID"];
								 $team_id = $_SESSION["Team_ID"];
								 $sql = "SELECT ID,Project_Number ,Project_Name FROM project WHERE Team_ID = '".$team_id."'";
								 $result = mysqli_query($conn,$sql);
								 if(mysqli_num_rows($result) > 0)
								 {
									 while($rows = mysqli_fetch_assoc($result))
									 {
										 echo "<option value='".$rows["ID"]."'> ".$rows["Project_Number"]." - ".$rows["Project_Name"]."</option>";
									 }
								 }
							?>
					</select>
					</div>
					<div id="topic">
                         <h2 style="visibility:hidden;">ds </h2>
							<label> Form/To </label>   
					    <input type="text" id="from_to" placeholder="From/To" />
					</div>
					<div id="topic">
						<h2 style="visibility:hidden;">df</h2>
						<label> Attached Scanned Submittal </label>
					    <input type="text" id="attached_scanned_submittal" placeholder="Attached Scanned Submittal" />
					</div>																																															 
					    <button style="display:inline-block; width:15%; margin-left:0;" id="submit1" class="submit_documents">Submit</button>
							<button style="display:inline-block; width:15%; margin:0;" id="submit1" class="generate_report">Generate Report</button>
                   
			<div style="width:99%;height:30%;display:block;max-width:10000px;" id="list">
				<?php
					 $readsql = "SELECT dc_documents.ID,
														dc_documents.Status, 
														dc_documents.Project_ID, 
														dc_documents.From_To, 
														dc_documents.Attached_Scanned_Submittal,
														project.Project_Number,
														project.Project_Name
											FROM dc_documents 
											INNER JOIN project ON dc_documents.Project_ID = project.ID";
				 $result = mysqli_query($conn,$readsql);

				 if(mysqli_num_rows($result) > 0)
				 {	 
						echo "<table class='general_table width-100pc'>";
							echo "<thead>";
							echo "<tr>";
							echo "	<th>Status</th>";
							echo "	<th>Project</th>";			 
							echo "	<th>From/To</th>";
							echo "	<th>Attached Scanned Submittal</th>";
							echo "  <th>Actions</th>";
							echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
						while($rows = mysqli_fetch_assoc($result))
						{
							echo "<tr>";						
							echo "<td> ".$rows["Status"]." </td>";					
							echo "	<td> ".$rows['Project_Number']." - ".$rows['Project_Name']."</td>";																					 
							echo "	<td> ".$rows['From_To']." </td>";
							echo "	<td><a> ".$rows['Attached_Scanned_Submittal']." </a></td>";
							echo "<td><input id='deletebtn' class='".$rows['ID']."' type='submit' value='Delete' name='delete' /></td>";
							echo "<script>
											$(document).ready(function(){
												$('.".$rows['ID']."').on('click',function(){
													var value = $(this).val();
													$.ajax(
														{																					 
															url:'dc_submit.php',
															type:'post',
															data:'delete_documents='+value+
																	 '&documents_id=".$rows['ID']."',
															success:function(data)
																{
																	$('#list').html(data);
																},
														});
												});
											});
										</script>";
							echo "</tr>";
						}				
							echo "</tbody>";
							echo "</table>";
				 }
				?>
			</div>
			<script>
				$(document).ready(function(){
					$('.submit_documents').on('click',function(){
						var value = $(this).val();
						$.ajax(
							{
								url:'dc_submit.php',
								type:'post',
								data:'documents='+value+
										 '&project_id=' + document.getElementById('ProjectNumber').value +	
										 '&status=' + document.getElementById('status').value +	
										 '&from_to=' + document.getElementById('from_to').value +
										 '&attached_scanned_submittal=' + document.getElementById('attached_scanned_submittal').value,
								success:function(data)
								{
									$('#list').html(data);
								},
							});
					});
				});
				
				$(document).ready(function(){
					$('.generate_report').on('click',function(){
						var value = $(this).val();
						$.ajax(
							{
								url:'dc_table.php',
								type:'post',
								data:'documents='+value,
								success:function(data)
								{
									$('#mainform').html(data);
								},
							});
					});
				});
			</script>