<?php
	session_start();
	include('connect.php');
	
	$inprogress = false;
	$designer_id = $_SESSION["ID"];
	$date = date("Y-m-d");				
	$curr_day = (int)date("d");	 
	$curr_month = (int)date("m");
	$curr_year = (int)date("Y");
  
  //select activity within the day
	$readsql = "SELECT activity_logs.ID,
										 activity_logs.Designer_ID,
										 activity_logs.Time_Out,
										 activity_logs.Status 
              FROM activity_logs 
							INNER JOIN designer_notif 
								ON activity_logs.DNotif_ID = designer_notif.ID
							WHERE activity_logs.Designer_ID = $designer_id
                AND EXTRACT(DAY FROM(designer_notif.deadline)) >= EXTRACT(DAY FROM(CURDATE()))
                AND EXTRACT(MONTH FROM(designer_notif.deadline)) = $curr_month
                AND EXTRACT(YEAR FROM(designer_notif.deadline)) = $curr_year";
	$readresult = mysqli_query($conn,$readsql);
	if(mysqli_num_rows($readresult) > 0)
	{
    //update duration of In Progress activity
		while($rows = mysqli_fetch_assoc($readresult))
		{
			if($rows["Status"] == "In Progress")
			{
					$updatesql = "UPDATE activity_logs 
													SET Duration = ADDTIME(DATE_FORMAT(CAST(TIMEDIFF(Time_End,Time_In) AS DATETIME),'%T'),DATE_FORMAT(CAST(TIMEDIFF(CURTIME(),Time_Start) AS DATETIME),'%T'))
												WHERE ID = ".$rows["ID"]."";
				if(mysqli_query($conn,$updatesql))
				{
				}
				$inprogress = true;
			}
		}
    //
		$readsql2 = 
      "SELECT
        project.Project_Number,
        project.Project_Name,
        activity_logs.ID,
        activity_logs.Ticket_Number,
        activity_logs.Date,
        activity_logs.Activity,
        activity_logs.Time_In,
        activity_logs.Time_Out,
        activity_logs.Duration,
        activity_logs.DNotif_ID,
        activity_logs.Time_Start,
        activity_logs.Status,
        internal_deadline.Phase
      FROM activity_logs
      INNER JOIN project 
        ON project.ID = activity_logs.Project_ID
      INNER JOIN designer_notif 
        ON activity_logs.DNotif_ID = designer_notif.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID
      WHERE
        activity_logs.Designer_ID = $designer_id 
        AND EXTRACT(DAY FROM(designer_notif.deadline)) >= 
          EXTRACT(DAY FROM(CURDATE())) 
        AND EXTRACT(MONTH FROM(designer_notif.deadline)) = $curr_month 
        AND EXTRACT(YEAR FROM(designer_notif.deadline)) = $curr_year
      ORDER BY ID";
    
    $result = mysqli_query($conn,$readsql2);
							
    if(mysqli_num_rows($result) > 0)
    {
      echo 
      "<table class='general_table width-100pc'>
        <thead>
          <th> Project </th>
          <th> Ticket No. </th>
          <th> Activity </th>
          <th> Date	</th>
          <th> Time In </th>
          <th> Time Out </th> 
          <th> Time Used </th>
          <th> Status </th>
        </thead>
        <tbody>";
      echo "<tr><td style='text-align:center' colspan='8'> Design Activities </td></tr>";
      while($rows = mysqli_fetch_assoc($result))
      {
        echo "<tr id='actual".$rows["ID"]."' class='clickable'>";
        echo "<td> ".$rows["Project_Number"]." ".$rows["Project_Name"]." - ".$rows["Phase"]."</td>
              <td> ".$rows["Ticket_Number"]." </td>
              <td> ".$rows["Activity"]." </td>
              <td> ".$rows["Date"]." </td> 
              <td> ".$rows["Time_In"]." </td>	
              <td> ".$rows["Time_Out"]." </td>
              <td id='time_used".$rows["ID"]."'> ".$rows["Duration"]." </td>
              <td ";
              if($rows["Status"] == "In Progress")
              {
                echo "class='bold yellow'";
              }
              echo"> ".$rows["Status"]." </td>
              <td class='display-none'>
                <script>
                  $('#actual".$rows["ID"]."').on('click',function(){
                    $('#activity_edit_pop_bg').show();
                    $('#design_submit_pop').show();
                    $('#actual_project').html('".$rows["Project_Number"]
                      ." - ".$rows["Project_Name"]." - ".$rows["Phase"]."');
                    $('#actual_activity').html('".$rows["Activity"]."');
                    $.ajax(
                    {
                      url:'reviewerAssignDes.php',
                      type:'post',
                      data:'showActualRequirements=true'+
                        '&dnotif_id=".$rows["DNotif_ID"]."',
                      success:function(data){
                        $('#actual_requirements').html(data);
                        $('#act_continue_charging_btn').attr('class',
                          'bg-green btn-normal white btn-hide');
                        $('#act_switch_btn').attr('class',
                          'bg-yellow btn-normal white btn-hide');
                        $('#act_break_btn').attr('class',
                          'bg-red btn-normal white btn-hide');
                        $('#act_meeting_btn').attr('class',
                          'bg-blue btn-normal white btn-hide');
                        $('#start_review_btn').attr('class',
                          'bg-blue btn-normal white btn-show');
                      }
                    });
                  });
                </script>
              </td>";	

        echo "<script>
              function fillPop".$rows["ID"]."()
              {																																									 
                $('#pop_project').html('".$rows["Project_Number"]." 
                  ".$rows["Project_Name"]."');
                $('#pop_designer').html('".$_SESSION["Name"]."');
                $('#pop_date').html('".$rows["Date"]."');
                $('#pop_ticket').html('".$rows["Ticket_Number"]."');
                $('#pop_activity').val('".$rows["Activity"]."');	 
                var time_in = '".$rows["Time_In"]."';
                var time_in = time_in.substring(0,5);
                $('#pop_time_in').val(time_in);	 
                var time_out = '".$rows["Time_Out"]."';
                var time_out = time_out.substring(0,5);
                $('#pop_time_out').val(time_out);	
                $('#pop_activitylog_id').val('".$rows["ID"]."'); ;
              }

              function continueTime".$rows["DNotif_ID"]."()
              {
                $.ajax({
                  url:'designerSubmit.php',
                  type:'post',
                  data:'continue=true'+
                      '&dnotif_id=".$rows["DNotif_ID"]."'+
                      '&activitylogs_id=".$rows["ID"]."',
                  success:function(data){},
                  error:function(data){
                       alert(data);
                  }
                });
              }

              function startReview".$rows["ID"]."()
              {
                $.ajax({
                  url:'designerSubmit.php',
                  type:'post',
                  data:'start_review=true'+
                        '&dnotif_id=".$rows["DNotif_ID"]."'+
                        '&activitylogs_id=".$rows["ID"]."',
                  success:function(data){},
                  error:function(data){
                       alert(data);
                    }
                  });
              }
            </script>
            </tr>";
      }
    }
                 $extrasql = "SELECT 
                                        project.Project_Number,
                                        project.Project_Name,
                                        extra_activity.ID,
                                        extra_activity.Extra_Activity,
                                        extra_activity.Time_Start,
                                        extra_activity.Time_End,
                                        extra_activity.Date_Modified,
                                        extra_activity.Duration,
                                        extra_activity.Status
                                      FROM extra_activity
                                      RIGHT JOIN project ON project.ID = extra_activity.Project_ID
                                      WHERE extra_activity.Designer_ID = $designer_id
                                        AND extra_activity.Date_Modified = CURDATE()";
                    $extraresult = mysqli_query($conn,$extrasql);
                    if(mysqli_num_rows($extraresult) > 0)
                    {
                      echo "<tr><td style='text-align:center' colspan='8'> Other Activities </td></tr>";
                      while($extrarow = mysqli_fetch_assoc($extraresult))
                      {
                      echo "<tr>
                              <td> ".$extrarow["Project_Number"]." - ".$extrarow["Project_Name"]." </td>
                              <td>  </td>
                              <td> ".$extrarow["Extra_Activity"]." </td>
                              <td> ".$extrarow["Date_Modified"]." </td>
                              <td> ".$extrarow["Time_Start"]." </td>
                              <td> ".$extrarow["Time_End"]." </td>
                              <td class='bold'> ".$extrarow["Duration"]." </td>
                              <td class='bold'> ".$extrarow["Status"]." </td>
                            </tr>";                                           
                      }
                    }
							 echo "</tbody></table>";
	}
//For Button and lapsed activity
	$startsql = "SELECT 
                activity_logs.ID,
                activity_logs.Designer_ID,
                designer_notif.Deadline,
                designer_notif.ID AS DNotif_ID,
                designer_notif.Status
							FROM activity_logs 
							INNER JOIN designer_notif 
								ON activity_logs.DNotif_ID = designer_notif.ID
							WHERE activity_logs.Designer_ID = $designer_id
                AND EXTRACT(DAY FROM(designer_notif.deadline)) >= EXTRACT(DAY FROM(CURDATE())) 
                AND EXTRACT(MONTH FROM(designer_notif.deadline)) = $curr_month 
                AND EXTRACT(YEAR FROM(designer_notif.deadline)) = $curr_year
                AND designer_notif.Status = 'In Progress'";
	$resultstart = mysqli_query($conn,$startsql);
	if(mysqli_num_rows($resultstart) > 0)
	{
		$row = mysqli_fetch_assoc($resultstart);
    $selected_deadline = $row["Deadline"];
    $dnotif_id = $row["DNotif_ID"];
      echo "<script> 
						$('#startBtn').hide();
						$('#submitBtn').show();	 
						$('#switchBtn').show(); 
						$('#activitylogs_idInfo').val('".$row["ID"]."'); 
						$('#total_time').val();
					</script>";
	}
	else
	{	
		echo "<script> 
						$('#startBtn').show(); 
						$('#switchBtn').hide(); 
						$('#submitBtn').hide();
					</script>";
    //update designer_notif for in progreess or hold
    $updatesql = "UPDATE designer_notif
                    SET Status = 'Lapsed'
                    WHERE (Status = 'In Progress' OR Status = 'On Hold')
                      AND EXTRACT(DAY FROM(designer_notif.deadline)) < EXTRACT(DAY FROM(CURDATE())) 
                      AND EXTRACT(MONTH FROM(designer_notif.deadline)) = $curr_month 
                      AND EXTRACT(YEAR FROM(designer_notif.deadline)) = $curr_year
                      AND Designer_ID = $designer_id";
      if(mysqli_query($conn,$updatesql))
      {}
    //update designer_notif for standby
    $updatesql2 = "UPDATE designer_notif
                    SET Status = 'Missed'
                    WHERE (Status = 'Standby')
                      AND EXTRACT(DAY FROM(designer_notif.deadline)) < EXTRACT(DAY FROM(CURDATE())) 
                      AND EXTRACT(MONTH FROM(designer_notif.deadline)) = $curr_month 
                      AND EXTRACT(YEAR FROM(designer_notif.deadline)) = $curr_year
                      AND Designer_ID = $designer_id";
      if(mysqli_query($conn,$updatesql2))
      {}
      $updateextra = "UPDATE extra_activity
                      SET Status = 'Done'
                      WHERE Status = 'In Progress'
                        AND EXTRACT(DAY FROM(extra_activity.Date_Modified)) < EXTRACT(DAY FROM(CURDATE())) 
                        AND EXTRACT(MONTH FROM(extra_activity.Date_Modified)) = $curr_month 
                        AND EXTRACT(YEAR FROM(extra_activity.Date_Modified)) = $curr_year
                        AND Designer_ID = $designer_id";
      if(mysqli_query($conn,$updateextra))
      {}
      $readlapsed = "SELECT 
                      DNotif_ID
                     FROM activity_logs
                     WHERE (Status = 'In Progress' OR Status = 'On Hold')
                      AND EXTRACT(DAY FROM(Date)) < EXTRACT(DAY FROM(CURDATE())) 
                      AND EXTRACT(MONTH FROM(Date)) = $curr_month 
                      AND EXTRACT(YEAR FROM(Date)) = $curr_year
                      AND Designer_ID = $designer_id";
      $resultlapsed = mysqli_query($conn,$readlapsed);
      if(mysqli_num_rows($resultlapsed) > 0)
      {
        $row = mysqli_fetch_assoc($resultlapsed);
        $dnotif_id = $row["DNotif_ID"];
        $updatednotif = "UPDATE designer_notif
                          SET Status = 'Lapsed'
                         WHERE ID = $dnotif_id";
        if(mysqli_query($conn,$updatednotif))
        {}
      }
    $updatesql2 = "UPDATE activity_logs
                    SET Status = 'Lapsed'
                    WHERE (Status = 'In Progress' OR Status = 'On Hold')
                      AND EXTRACT(DAY FROM(Date)) < EXTRACT(DAY FROM(CURDATE())) 
                      AND EXTRACT(MONTH FROM(Date)) = $curr_month 
                      AND EXTRACT(YEAR FROM(Date)) = $curr_year
                      AND Designer_ID = $designer_id";
      if(mysqli_query($conn,$updatesql2))
      {}
	}
  //Generate Time Used for break and meeting
  $extrasql = "UPDATE extra_activity 
							SET Duration = CAST(TIMEDIFF(CURTIME(),Time_Start) AS DATETIME)
                    WHERE Status = 'In Progress'
                      AND EXTRACT(DAY FROM(extra_activity.Date_Modified)) >= $curr_day 
                      AND EXTRACT(MONTH FROM(extra_activity.Date_Modified)) = $curr_month 
                      AND EXTRACT(YEAR FROM(extra_activity.Date_Modified)) = $curr_year";
  if(mysqli_query($conn,$extrasql))
  {}
	$total_time = "";
	$time = new DateTime('', new DateTimeZone('Asia/Manila'));
	$time = $time->format('H:i');

  $checksql = "SELECT Duration,Extra_Activity FROM extra_activity WHERE Designer_ID = $designer_id AND Date_Modified = CURDATE() AND Status = 'In Progress'";
  $result = mysqli_query($conn,$checksql);
  $sql = "";
  if(mysqli_num_rows($result) > 0)
  {
    $row = mysqli_fetch_assoc($result);
    $extra_activity = $row["Extra_Activity"];
    $sql = "SELECT
    SEC_TO_TIME(SUM(TIME_TO_SEC(Duration)) + 
      (SELECT
          SUM(TIME_TO_SEC(Duration)) AS Duration
        FROM
          extra_activity
        WHERE
          Designer_ID = $designer_id 
          AND Date_Modified = CURDATE())
      ) AS Duration,
      (SELECT 
        Extra_Activity 
       FROM 
        extra_activity
       WHERE 
        Designer_ID = $designer_id 
        AND Date_Modified = CURDATE() 
        AND Status = 'In Progress'
      ) AS Extra_Activity,
      (SELECT
        SEC_TO_TIME(SUM(TIME_TO_SEC(Duration))) AS Duration
       FROM
        extra_activity
       WHERE
        Designer_ID = $designer_id 
        AND Date_Modified = CURDATE()
        AND Extra_Activity = '$extra_activity'
      ) AS ExtraActivity_Hour
FROM
    activity_logs
WHERE
    Designer_ID = $designer_id AND DATE = CURDATE()";
  }
  else
  {
    $sql = "SELECT
    SEC_TO_TIME(SUM(TIME_TO_SEC(Duration))) AS Duration
FROM
    activity_logs
WHERE
    Designer_ID = $designer_id AND DATE = CURDATE()";
  }
	
    $result2 = mysqli_query($conn,$sql);
	if(mysqli_num_rows($result2) > 0)
	{
		$row = mysqli_fetch_assoc($result2);
		if($row["Duration"] == "")
		{		 
      $total_time = "00:00:00";
		}
		else
		{
			$total_time = $row["Duration"];
      
      echo "<script> 
              $('#total_time').html('$total_time');
              $('#time_inInfo').val('$time');
            </script>";
		}
    $checksql = "SELECT Duration 
                  FROM extra_activity 
                  WHERE Designer_ID = $designer_id 
                    AND Date_Modified = CURDATE()
                    AND Status = 'In Progress'";
  $result = mysqli_query($conn,$checksql);
  $sql = "";
  if(mysqli_num_rows($result) > 0)
  {
    if($row["Extra_Activity"] == "Break" || $row["Extra_Activity"] == "Meeting" )
    {
       echo "<script>
              $('#extra_activity_div').show();
              $('#extra_activity_bg').show();
              $('#extra_activity').html('".$row["Extra_Activity"]." ".$row["ExtraActivity_Hour"]."');
              $('#extra_hours').html('Total Hours: ".$row["Duration"]."');
              $('#total_time').html('".$row["Duration"]."');";
       if($row["Extra_Activity"] == "Break")
       {
         echo "$('#extra_meeting_btn').show();";
         echo "$('#extra_break_btn').hide();";
       }
       if($row["Extra_Activity"] == "Meeting")
       {
         echo "$('#extra_break_btn').show();";
         echo "$('#extra_meeting_btn').hide();";
       }
         echo "</script>";
    }
    else
    {
      echo "<script>
              $('#extra_activity_div').hide();
              $('#extra_activity_bg').hide();
              $('#extra_activity').html('".$row["Extra_Activity"]." ".$row["ExtraActivity_Hour"]."');
              $('#extra_hours').html('Total Hours: ".$row["Duration"]."');
              $('#total_time').html('".$row["Duration"]."');
              </script>";
    }
  }
  else
    {
      echo "<script>
              $('#extra_activity_div').hide();
              $('#extra_hours').html('Total Hours: ".$row["Duration"]."');
              $('#total_time').html('".$row["Duration"]."');
              </script>";
    }
  }
	else
	{
		$total_time = "00:00:00";
	}
	echo "<script> 
					$('#total_time').html('$total_time');
					$('#time_inInfo').val('$time');
				</script>";
