<?php
session_start();
/* Van Hudson Galvoso
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
	unset($_SESSION['ID']);
	unset($_SESSION['Firstname']);
	unset($_SESSION['Middlename']);
	unset($_SESSION['Lastname']);
	unset($_SESSION['Picture']);
	unset($_SESSION['User_Type']);
	unset($_SESSION['Team_ID']);
	unset($_SESSION['Trade']);
	unset($_SESSION['Username']);
	unset($_SESSION['Name']);
	session_destroy();
	header("Location:index.php");

