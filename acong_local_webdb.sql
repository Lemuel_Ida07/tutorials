-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2018 at 11:28 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `acong_local_webdb`
--
CREATE DATABASE IF NOT EXISTS `acong_local_webdb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `acong_local_webdb`;

-- --------------------------------------------------------

--
-- Table structure for table `activity_logs`
--

CREATE TABLE `activity_logs` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `Designer_ID` int(10) UNSIGNED NOT NULL,
  `Date` date NOT NULL,
  `Project_ID` int(7) NOT NULL,
  `Ticket_Number` varchar(30) NOT NULL,
  `Activity` varchar(255) NOT NULL,
  `Time_In` time NOT NULL,
  `Time_Out` time NOT NULL,
  `Duration` time NOT NULL,
  `Status` varchar(11) NOT NULL,
  `DNotif_ID` int(11) NOT NULL,
  `Time_Start` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dc_documents`
--

CREATE TABLE `dc_documents` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `Status` varchar(11) NOT NULL,
  `Project_ID` int(6) NOT NULL,
  `From_To` varchar(200) NOT NULL,
  `Attached_Scanned_Submittal` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dc_email_log`
--

CREATE TABLE `dc_email_log` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `Project_ID` int(6) NOT NULL,
  `Email_Link` varchar(200) NOT NULL,
  `Classification` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dc_email_log`
--

INSERT INTO `dc_email_log` (`ID`, `Project_ID`, `Email_Link`, `Classification`) VALUES
(1, 3, 'ghjgfd', 'RFI/A'),
(2, 3, 'ghjgfd', 'RFI/A'),
(3, 3, 'ghjgfd', 'RFI/A'),
(4, 3, 'ghjgfd', 'RFI/A'),
(5, 3, 'ghjgfd', 'RFI/A'),
(6, 3, 'ghjgfd', 'RFI/A'),
(7, 3, 'ghjgfd', 'RFI/A'),
(8, 3, 'ghjgfd', 'RFI/A'),
(9, 3, 'ghjgfd', 'RFI/A'),
(10, 3, 'ghjgfd', 'RFI/A'),
(11, 3, 'ghjgfd', 'RFI/A'),
(12, 3, 'ghjgfd', 'RFI/A');

-- --------------------------------------------------------

--
-- Table structure for table `dc_meeting_log`
--

CREATE TABLE `dc_meeting_log` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `Project_ID` int(6) NOT NULL,
  `Scanned_Copy_Link` varchar(200) NOT NULL,
  `MC_No` varchar(100) NOT NULL,
  `Commitments` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dc_meeting_log`
--

INSERT INTO `dc_meeting_log` (`ID`, `Project_ID`, `Scanned_Copy_Link`, `MC_No`, `Commitments`) VALUES
(1, 3, 'dsaf', 'jghfghgf', 'fgsdhgf'),
(2, 3, 'dsaf', 'jghfghgf', 'fgsdhgf'),
(3, 3, 'dsaf', 'jghfghgf', 'fgsdhgf'),
(4, 3, 'dsaf', 'jghfghgf', 'fgsdhgf'),
(5, 3, 'dsaf', 'jghfghgf', 'fgsdhgf'),
(6, 3, 'dsaf', 'jghfghgf', 'fgsdhgf'),
(7, 3, 'dsaf', 'jghfghgf', 'fgsdhgf'),
(8, 3, 'dsaf', 'jghfghgf', 'fgsdhgf'),
(9, 3, 'dsaf', 'jghfghgf', 'fgsdhgf');

-- --------------------------------------------------------

--
-- Table structure for table `dc_submittal`
--

CREATE TABLE `dc_submittal` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `Project_ID` int(11) NOT NULL,
  `Ticket_Number` varchar(30) NOT NULL,
  `Deadline` date NOT NULL,
  `Drawings` varchar(100) NOT NULL,
  `Drawing_List` varchar(100) NOT NULL,
  `Blueprint_Request` varchar(100) NOT NULL,
  `Specifications` varchar(100) NOT NULL,
  `Calculations` varchar(100) NOT NULL,
  `Reports` varchar(100) NOT NULL,
  `Outgoing_Email` varchar(100) NOT NULL,
  `Submittal_Form_Transmittal` varchar(100) NOT NULL,
  `Deliverables_List` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dc_submittal`
--

INSERT INTO `dc_submittal` (`ID`, `Project_ID`, `Ticket_Number`, `Deadline`, `Drawings`, `Drawing_List`, `Blueprint_Request`, `Specifications`, `Calculations`, `Reports`, `Outgoing_Email`, `Submittal_Form_Transmittal`, `Deliverables_List`) VALUES
(1, 22, 'hfgdshgfh', '2018-08-17', 'gfgh', 'hgfh', 'dgfdsh', 'gfshgvfshj', 'htgrsj', 'fgrhsgrs', 'tehjgfs', 'gfshgaer', 'hjgvfshtrjhfd'),
(2, 22, 'hfgdshgfh', '2018-08-17', 'gfgh', 'hgfh', 'dgfdsh', 'gfshgvfshj', 'htgrsj', 'fgrhsgrs', 'tehjgfs', 'gfshgaer', 'hjgvfshtrjhfd'),
(3, 22, 'hfgdshgfh', '2018-08-17', 'gfgh', 'hgfh', 'dgfdsh', 'gfshgvfshj', 'htgrsj', 'fgrhsgrs', 'tehjgfs', 'gfshgaer', 'hjgvfshtrjhfd'),
(4, 22, 'hfgdshgfh', '2018-08-17', 'gfgh', 'hgfh', 'dgfdsh', 'gfshgvfshj', 'htgrsj', 'fgrhsgrs', 'tehjgfs', 'gfshgaer', 'hjgvfshtrjhfd'),
(5, 22, 'hfgdshgfh', '2018-08-17', 'gfgh', 'hgfh', 'dgfdsh', 'gfshgvfshj', 'htgrsj', 'fgrhsgrs', 'tehjgfs', 'gfshgaer', 'hjgvfshtrjhfd'),
(6, 22, 'hfgdshgfh', '2018-08-17', 'gfgh', 'hgfh', 'dgfdsh', 'gfshgvfshj', 'htgrsj', 'fgrhsgrs', 'tehjgfs', 'gfshgaer', 'hjgvfshtrjhfd'),
(7, 22, 'hfgdshgfh', '2018-08-17', 'gfgh', 'hgfh', 'dgfdsh', 'gfshgvfshj', 'htgrsj', 'fgrhsgrs', 'tehjgfs', 'gfshgaer', 'hjgvfshtrjhfd');

-- --------------------------------------------------------

--
-- Table structure for table `designer_notif`
--

CREATE TABLE `designer_notif` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `Project_ID` int(11) NOT NULL,
  `Internaldl_ID` int(11) NOT NULL,
  `Designer_ID` int(11) NOT NULL,
  `Time_In` time NOT NULL,
  `Time_Out` time NOT NULL,
  `Duration` time NOT NULL,
  `Status` varchar(20) NOT NULL DEFAULT 'Standby',
  `Day_DN` int(11) NOT NULL,
  `Month_DN` int(2) NOT NULL,
  `Year_DN` int(4) NOT NULL,
  `Input_Date` date NOT NULL,
  `Activity` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `external_deadline`
--

CREATE TABLE `external_deadline` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Project_ID` int(10) UNSIGNED NOT NULL,
  `External_Deadline` varchar(11) NOT NULL,
  `Day_ED` int(2) UNSIGNED NOT NULL,
  `Month_ED` int(2) UNSIGNED NOT NULL,
  `Year_ED` int(4) UNSIGNED NOT NULL,
  `Deliverables` varchar(255) DEFAULT NULL,
  `Input_Date` date NOT NULL,
  `Phase` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `external_deadline`
--

INSERT INTO `external_deadline` (`ID`, `Project_ID`, `External_Deadline`, `Day_ED`, `Month_ED`, `Year_ED`, `Deliverables`, `Input_Date`, `Phase`) VALUES
(5, 51, '2018-8-28', 28, 8, 2018, 'S-DD-100', '0000-00-00', 'DD'),
(6, 29, '2018-8-28', 28, 8, 2018, 'CD-100', '0000-00-00', 'TR'),
(7, 211, '2018-8-28', 28, 8, 2018, 'CD-60', '0000-00-00', 'FCD');

-- --------------------------------------------------------

--
-- Table structure for table `internal_deadline`
--

CREATE TABLE `internal_deadline` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Externaldl_ID` int(10) UNSIGNED NOT NULL,
  `Internal_Deadline` varchar(11) NOT NULL,
  `Day_ID` int(2) UNSIGNED NOT NULL,
  `Month_ID` int(2) UNSIGNED NOT NULL,
  `Year_ID` int(4) UNSIGNED NOT NULL,
  `Trade` varchar(11) NOT NULL,
  `Ticket_Number` varchar(50) NOT NULL,
  `Input_Date` date NOT NULL,
  `Phase` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internal_deadline`
--

INSERT INTO `internal_deadline` (`ID`, `Externaldl_ID`, `Internal_Deadline`, `Day_ID`, `Month_ID`, `Year_ID`, `Trade`, `Ticket_Number`, `Input_Date`, `Phase`) VALUES
(12, 5, '2018-08-27', 28, 8, 2018, 'S', 'M-P1745-S-DD-01', '0000-00-00', 'DD'),
(13, 6, '2018-08-27', 28, 8, 2018, 'E', 'M-P1737-E-TR-001', '0000-00-00', 'TR'),
(14, 6, '2018-08-24', 28, 8, 2018, 'A', 'M-P1737-A-TR-001', '0000-00-00', 'TR'),
(15, 6, '2018-08-27', 28, 8, 2018, 'S', 'M-P1737-S-TR-001', '0000-00-00', 'TR'),
(16, 6, '2018-08-27', 28, 8, 2018, 'M', 'M-P1737-M-TR-001', '0000-00-00', 'TR'),
(17, 6, '2018-08-27', 28, 8, 2018, 'AU', 'M-P1737-AU-TR-001', '0000-00-00', 'TR'),
(18, 6, '2018-08-27', 28, 8, 2018, 'P', 'M-P1737-P-TR-001', '0000-00-00', 'TR'),
(19, 6, '2018-08-27', 28, 8, 2018, 'FP', 'M-P1737-FP-TR-001', '0000-00-00', 'TR'),
(20, 7, '2018-08-27', 28, 8, 2018, 'E', 'M-P1737-E-CD-001', '0000-00-00', 'FCD'),
(21, 7, '2018-08-27', 28, 8, 2018, 'S', 'M-P1737-S-CD-001', '0000-00-00', 'FCD'),
(22, 7, '2018-08-27', 28, 8, 2018, 'P', 'M-P1737-P-CD-001', '0000-00-00', 'FCD');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `ID` int(10) UNSIGNED NOT NULL,
  `User_ID` int(10) UNSIGNED NOT NULL,
  `Internaldl_ID` int(10) UNSIGNED DEFAULT '0',
  `Trade` varchar(11) NOT NULL,
  `Status` int(1) UNSIGNED NOT NULL,
  `Input_Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`ID`, `User_ID`, `Internaldl_ID`, `Trade`, `Status`, `Input_Date`) VALUES
(13, 124, 12, 'S', 0, '0000-00-00'),
(14, 126, 12, 'S', 0, '0000-00-00'),
(15, 132, 12, 'S', 0, '0000-00-00'),
(16, 149, 13, 'E', 0, '0000-00-00'),
(17, 129, 14, 'A', 0, '0000-00-00'),
(18, 124, 15, 'S', 0, '0000-00-00'),
(19, 126, 15, 'S', 0, '0000-00-00'),
(20, 132, 15, 'S', 0, '0000-00-00'),
(21, 131, 16, 'M', 0, '0000-00-00'),
(22, 128, 17, 'AU', 0, '0000-00-00'),
(23, 127, 18, 'P', 0, '0000-00-00'),
(24, 149, 20, 'E', 0, '0000-00-00'),
(25, 124, 21, 'S', 0, '0000-00-00'),
(26, 126, 21, 'S', 0, '0000-00-00'),
(27, 132, 21, 'S', 0, '0000-00-00'),
(28, 127, 22, 'P', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `overtime`
--

CREATE TABLE `overtime` (
  `ID` int(10) UNSIGNED NOT NULL,
  `User_ID` int(10) UNSIGNED NOT NULL,
  `Control_Number` varchar(50) NOT NULL,
  `Project_Number` varchar(50) NOT NULL,
  `Ticket_Number` varchar(50) NOT NULL,
  `Activity` varchar(255) NOT NULL,
  `Date` date NOT NULL,
  `Time_In` time NOT NULL,
  `Time_Out` time NOT NULL,
  `Duration` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Project_Number` varchar(30) NOT NULL,
  `Project_Name` varchar(100) NOT NULL,
  `Team_ID` int(10) UNSIGNED DEFAULT NULL,
  `Remarks` int(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`ID`, `Project_Number`, `Project_Name`, `Team_ID`, `Remarks`) VALUES
(1, 'SO-14-012', 'Philippine Genome Center Building Phase 1', 1, 1),
(2, 'SO-G-15-001', 'Marina', 1, 1),
(3, 'SO-G-15-004', 'LBP Head Office Mechanical As-Built', 2, 1),
(4, 'SO-G-15-009', 'UPOU Teaching and Learning Hub', 1, 1),
(5, 'SO-G-15-010', 'Manila and Laoag Branch', 4, 1),
(6, 'SO-G-15-013', ' Zamboanga City Medical Center', 1, 1),
(7, 'SO-G-15-016', 'DA Region VIII Integrated Laboratory Building', 1, 1),
(8, 'SO-G-15-017', 'DMMME Building', 1, 1),
(9, 'SO-G-16-001', 'UP Tourism Heritage Complex   ', 1, 1),
(10, 'SO-G-16-007', 'Philippine Genome Center Building Phase 2', 1, 1),
(11, 'SO-G-16-009', 'DILG Regional Office IX Complex', 1, 1),
(12, 'SO-G-16-011', 'Sabang Integrated Wharf Development', 4, 1),
(13, 'SO-G-16-014-01', 'DPD Building Expansion - CM', 1, 1),
(14, 'SO-G-16-015', 'Tanauan Public Market', 1, 1),
(15, 'SO-G-17-005-01', 'DPD Building (BIR San Pablo) - CM', 1, 1),
(16, 'SO-G-17-007', 'Office Buildings (Caloocan)', 4, 1),
(17, 'SO-G-17-007-01', 'Office Buildings (Caloocan) - CM', 1, 1),
(18, 'SO-G-17-011', 'Administration and Laboratory Building', 1, 1),
(19, 'SO-G-17-015', 'PCARRD Steel Parking Building', 4, 1),
(20, 'SO-G-17-016', 'Small Scale Meat Establishment', 4, 1),
(21, 'SO-G-17-019', 'MIMAROPA Office Building', 4, 1),
(22, 'SO-G-17-020', 'SSS Batangas Building Expansion', 2, 1),
(23, 'SO-G-17-021', 'MIST School Building', 4, 1),
(24, 'SO-G-17-025', 'Arroceros', 3, 1),
(25, 'SO-G-17-029', 'Rizal Hall and Bonifacio Bldg.', 2, 1),
(26, 'SO-G-17-030', 'SWC Building', 2, 1),
(27, 'SO-G-17-031', 'Physical Plant Office', 4, 1),
(28, 'SO-G-17-033', 'San Lazaro Hospital Buildings', 3, 1),
(29, 'SO-G-17-037', 'Matapat and Magalang Buildings', 2, 1),
(30, 'SO-G-17-038', 'UPLB Graduate School', 1, 1),
(31, 'SO-G-17-040', 'Management Building Phase 2', 4, 1),
(32, 'SO-G-17-041', 'Economics and Management Studies Center', 1, 1),
(33, 'SO-G-17-042', 'NBI Main Building', 2, 1),
(34, 'SO-G-17-043', 'Bulwagang Valdez', 2, 1),
(35, 'SO-G-18-001', 'MWS1 Building As-Built Plan Consolidation', 2, 1),
(36, 'SO-G-18-002', 'QMMC Structural Investigation', 2, 1),
(37, 'SO-G-18-003', 'FPRDI Buildings', 2, 1),
(38, 'SO-G-18-004', 'UP-International Center', 1, 1),
(39, 'SO-G-18-005', 'SLRWH-Main Building', 3, 1),
(40, 'SO-G-18-007', '5-Storey LBP Koronadal Assessment', 4, 1),
(41, 'SO-G-18-008', 'LBP Digos Assessment', 4, 1),
(42, 'SO-G-18-009', 'Valenzuela Medical Center', 4, 1),
(43, 'SO-P-16-062&063/02', 'CM-Dalupan Building & Former Dentistry Fire Protection System', 1, 1),
(44, 'SO-P-16-067-02', 'Proposed Peninsula Project - Diezmo River', 2, 1),
(45, 'SO-P-16-094', 'Dagupan City Center - BPO', 3, 1),
(46, 'SO-P-16-097', 'Mix Plant Technical Building', 3, 1),
(47, 'SO-P-17-019/DC2', 'Vistamall Mintal Davao', 5, 1),
(48, 'SO-P-17-027-02', 'Suysing Isabela', 4, 1),
(49, 'SO-P-17-028', 'Parreno Residence', 4, 1),
(50, 'SO-P-17-044', 'Robinsons Homes Pueblo Angono', 5, 1),
(51, 'SO-P-17-045', 'Filmore Apartment', 5, 1),
(52, 'SO-P-17-048-02', 'Ohtsuka Poly-Tech Work Center', 3, 1),
(53, 'SO-P-17-050-01', 'BFC Dorm - Basement', 3, 1),
(54, 'SO-P-17-067', 'Gorordo Suites Hotel', 5, 1),
(55, 'SO-P-17-067-01', 'Gorordo Suites Hotel', 5, 1),
(56, 'SO-P-17-068', 'Triplex Warehouse', 3, 1),
(57, 'SO-P-17-068/02', 'Triplex Warehouse - VE', 3, 1),
(58, 'SO-P-17-070', 'Vistamall BCDA Campus 1 - Building 1', 3, 1),
(59, 'SO-P-17-079-01', 'Sta Elena Condominiums', 3, 1),
(60, 'SO-P-17-090-01', 'T3 Development - ECC', 3, 1),
(61, 'SO-P-17-092', 'Centralis', 3, 1),
(62, 'SO-P-17-098', 'Vista Place Daang Hari', 5, 1),
(63, 'SO-P-17-101', 'Dormitory Building 1', 5, 1),
(64, 'SO-P-17-103', 'Marilao Warehouse', 5, 1),
(65, 'SO-P-18-000-04', 'Malabon Warehouse', 3, 1),
(66, 'SO-P-18-000-05', 'Lim Residence', 5, 1),
(67, 'SO-P-18-000-07', 'Filinvest City Marian Chapel', 3, 1),
(68, 'SO-P-18-002 - DC', 'Vistamall Cabanatuan', 5, 1),
(69, 'SO-P-18-003', 'Vistamall Sta. Maria', 5, 1),
(70, 'SO-P-18-003 - DC', 'Vistamall Sta. Maria', 5, 1),
(71, 'SO-P-18-003 - DC2', 'Vistamall Sta. Maria', 5, 1),
(72, 'SO-P-18-004', 'Insular Alabang', 3, 1),
(73, 'SO-P-18-008', 'Crown Asia House Models', 5, 1),
(74, 'SO-P-18-010', 'Rimaven Homes Dau', 1, 1),
(75, 'SO-P-18-012', 'Proposed Rubrix Office Building', 1, 1),
(76, 'SO-P-18-013', 'Proposed Cemex Solid K4-Road and Slope Protections', 3, 1),
(77, 'SO-P-18-014', 'Valeza Mansions Building 4', 5, 1),
(78, 'SO-P-18-015', 'DMPTI Davao Port', 3, 1),
(79, 'SO-P-18-016', 'Jubilation Enclave Amenities', 5, 1),
(80, 'SO-P-18-017', 'La Sema Building', 4, 1),
(81, 'SO-P-18-018', 'Vistaland Bridges', 3, 1),
(82, 'SO-P-18-019', 'Pear Plaza ', 5, 1),
(83, 'SO-P-18-020', 'Cagayan Agri-Commercial Complex', 4, 1),
(84, 'SO-P-18-021', 'Fersal Hotel', 3, 1),
(85, 'SO-13-003', 'DOST Main Building Strucural Assessment', 2, 0),
(86, 'SO-13-024', 'BISU Admin Building', 4, 0),
(87, 'SO-13-025', 'CARAGA Regional Hospital', 4, 0),
(88, 'SO-13-042', 'DA Agri Pinoy', 4, 0),
(89, 'SO-13-072', 'PCARRD DOST Railway System', 4, 0),
(90, 'SO-14-011', 'Gasan Public Market', 4, 0),
(91, 'SO-14-049', 'PSHS Lab and Tech Building', 4, 0),
(92, 'SO-14-079/02', 'NHA Davao Low-Rise (LRB)', 3, 0),
(93, 'SO-G-15-003', 'BIR RR14 RDO No. 86 Borongan', 4, 0),
(94, 'SO-G-15-006', 'CHSS Cultural Complex', 4, 0),
(95, 'SO-G-15-007', 'LBP Office Buildings - Intramuros', 2, 0),
(96, 'SO-G-15-007', 'LBP Office Buildings - west Ave', 2, 0),
(97, 'SO-G-15-007', 'LBP Office Buildings Dasma', 2, 0),
(98, 'SO-G-15-007', 'LBP Office Buildings Subic', 2, 0),
(99, 'SO-G-15-007', 'LBP Office Buildings - Baguio', 2, 0),
(100, 'SO-G-15-008', 'CSM Research Laboratories', 4, 0),
(101, 'SO-G-15-014', 'Academic Building', 4, 0),
(102, 'SO-G-16-002', '5-Storey and Cafetorium Buildings', 4, 0),
(103, 'SO-G-17-008', 'New Municipal Building', 2, 0),
(104, 'SO-G-17-012', 'HRDC Building', 4, 0),
(105, 'SO-G-17-014', 'EC Head Office Building 1', 4, 0),
(106, 'SO-G-17-018', 'Main Library and Learning Commons Building', 4, 0),
(107, 'SO-G-17-023', 'RDO-088 Building', 4, 0),
(108, 'SO-G-17-035', 'BFAR XI Office Building', 4, 0),
(109, 'SO-08-032', 'AMA Towers', 5, 0),
(110, 'SO-13-006', 'Brentville Village Front', 4, 0),
(111, 'SO-13-012', 'Avida Asten', 4, 0),
(112, 'SO-13-013', 'Avida Aspira Tower 1', 3, 0),
(113, 'SO-13-013/02', 'Avida Aspira Tower 2 - New Contract', 3, 0),
(114, 'SO-13-019', 'Northbay Launching Pier', 3, 0),
(115, 'SO-13-031', 'Field Residence', 4, 0),
(116, 'SO-13-043', 'Avida Serin/ Cheers', 3, 0),
(117, 'SO-13-047', 'Euro Tower Lagro', 3, 0),
(118, 'SO-13-047', 'Euro Tower Lagro - New Contract', 5, 0),
(119, 'SO-13-057', 'Nora Dungo Residence', 3, 0),
(120, 'SO-14-025', 'Avida Arca South', 3, 0),
(121, 'SO-14-025', 'Avida Arca South - Additional', 3, 0),
(122, 'SO-14-025', 'Mix Plant Expansion', 5, 0),
(123, 'SO-14-045', 'Jason Ong Residence', 2, 0),
(124, 'SO-P-15-035', 'Jerry Uy Residence', 5, 0),
(125, 'SO-P-15-046', 'Bonaventure Plaza Building Pylon', 3, 0),
(126, 'SO-P-15-047', '5-Storey Mixed-use Building', 2, 0),
(127, 'SO-P-15-058', 'Tanauan Victory Mall and Public Market', 2, 0),
(128, 'SO-P-15-062-02', 'Proposed FEU Tertiary Education - Building', 5, 0),
(129, 'SO-P-16-002-01', 'Ricky Yu Residence', 5, 0),
(130, 'SO-P-16-002-02', 'Ricky Yu Residence - Option 2', 5, 0),
(131, 'SO-P-16-004-01', '(FELCO) 11/F WITH MEZZANINE ', 5, 0),
(132, 'SO-P-16-004-02', '(FELCO) 11/F WITH MEZZANINE - Additional Work', 5, 0),
(133, 'SO-P-16-005', 'Amy and Hayley Chua', 5, 0),
(134, 'SO-P-16-012', 'Patrick DY Residence', 5, 0),
(135, 'SO-P-16-013', 'Jerome Deguzman Rsidence', 5, 0),
(136, 'SO-P-16-020', 'UTAMCHANDANI RESIDENCE', 5, 0),
(137, 'SO-P-16-021', 'Lifeline Building', 5, 0),
(138, 'SO-P-16-022', 'Natalie Sia Residence', 5, 0),
(139, 'SO-P-16-026', 'Tantoco-Enriquez Residence', 5, 0),
(140, 'SO-P-16-029', 'Aranzaso Residence', 5, 0),
(141, 'SO-P-16-031', 'Villadolid Residence', 3, 0),
(142, 'SO-P-16-032', 'Toyota Marikina', 5, 0),
(143, 'SO-P-16-033', 'Johnson Tan Residence', 5, 0),
(144, 'SO-P-16-034', 'Chico Residence', 2, 0),
(145, 'SO-P-16-035-01', 'PROPOSED UE LAGUNA CAMPUS MASTER PLAN', 2, 0),
(146, 'SO-P-16-035-02', 'PROPOSED UE ELEMENTARY AND HIGH SCHOOL BUILDING', 3, 0),
(147, 'SO-P-16-036', 'PROPOSED SM URDANETA', 5, 0),
(148, 'SO-P-16-037', 'Nazareno Residence', 5, 0),
(149, 'SO-P-16-040', 'St. Anthony Chancery Building', 5, 0),
(150, 'SO-P-16-041', 'Davao Retail Building', 5, 0),
(151, 'SO-P-16-048', 'SM Telabastagan', 5, 0),
(152, 'SO-P-16-049', 'ADB Building', 3, 0),
(153, 'SO-P-16-062&63/01', 'Dalupan Building & Former Dentistry Fire Protection System', 3, 0),
(154, 'SO-P-16-064/02-01', 'MG Exeo Network Inc. Training Facility-Construction', 4, 0),
(155, 'SO-P-16-069', 'Proposed Unitop Kalibo', 5, 0),
(156, 'SO-P-16-073', 'Proposed Ecopark Warehouse', 5, 0),
(157, 'SO-P-16-075', 'My Town Project Phase 1', 5, 0),
(158, 'SO-P-16-076-01', 'Proposed Sabangan Beach Resort', 5, 0),
(159, 'SO-P-16-076-02', 'Proposed Sabangan Beach Resort', 2, 0),
(160, 'SO-P-16-077', 'Proposed Mr. Kirby & Kathleen Sy Residence', 3, 0),
(161, 'SO-P-16-078', 'Proposed Arnedo Residence Expansion', 3, 0),
(162, 'SO-P-16-083', 'Proposed 6-Storey Escudero Apartments', 5, 0),
(163, 'SO-P-16-085', 'Unitop Baliuag', 5, 0),
(164, 'SO-P-16-085', 'Unitop Baliuag-Additional Work', 5, 0),
(165, 'SO-P-16-086', 'Guerreo Residence', 3, 0),
(166, 'SO-P-16-089', 'Proposed Royal Tower', 5, 0),
(167, 'SO-P-16-090', 'Savemore SM Legazpi Project', 5, 0),
(168, 'SO-P-16-094', 'Dagupan City Center - City Hall', 5, 0),
(169, 'SO-P-16-095', 'Mandarin Staff House', 5, 0),
(170, 'SO-P-16-098/02', 'Proposed H&M Spring 2017 Sta. Rosa', 3, 0),
(171, 'SO-P-16-099', 'Proposed Monocrete Warehouse ', 3, 0),
(172, 'SO-P-16-102', 'Proposed Lorenzana Residence', 2, 0),
(173, 'SO-P-17-001', 'Olongapo Civic Center', 2, 0),
(174, 'SO-P-17-002', 'SM Olongapo 2', 2, 1),
(175, 'SO-P-17-003', 'YALE Hardware Building', 2, 0),
(176, 'SO-P-17-006', 'Paseo Covered Walkways-Additional Work No. 1 & 2', 3, 0),
(177, 'SO-P-17-006-01', 'Paseo Covered Walkways-Additional Work No. 1 & 2', 3, 0),
(178, 'SO-P-17-007', 'Honda Motor World', 3, 0),
(179, 'SO-P-17-010', 'RPI Depot', 2, 0),
(180, 'SO-P-17-011', 'Main Majlis', 5, 0),
(181, 'SO-P-17-015', 'Toyota San Jose Del Monte Bulacan', 5, 0),
(182, 'SO-P-17-023', 'Carmona Warehouse', 5, 0),
(183, 'SO-P-17-024', 'Nidec Office Fitout', 5, 0),
(184, 'SO-P-17-030-01', 'GPOI Central Training Complex', 2, 0),
(185, 'SO-P-17-032', 'Kerwin Law Residence', 2, 0),
(186, 'SO-P-17-034', 'UE Tan Yan Kee Building Expansion', 3, 0),
(187, 'SO-P-17-035', 'Malapitan Residence', 4, 0),
(188, 'SO-P-17-036', 'Guang Ming College', 5, 0),
(189, 'SO-P-17-038', 'Proposed Moldex Injection Plant Expansion', 5, 0),
(190, 'SO-P-17-042', 'Tan Residence', 2, 0),
(191, 'SO-P-17-043', 'Mix-Use Isabela', 3, 0),
(192, 'SO-P-17-047', 'Unionbank Fit-out Branches', 5, 0),
(193, 'SO-P-17-050', 'BFC Dorm', 5, 0),
(194, 'SO-P-17-053', 'Eric Lao Residence', 5, 0),
(195, 'SO-P-17-054', 'SM City Baguio Skyranch', 5, 1),
(196, 'SO-P-17-055', 'Co Residence', 3, 0),
(197, 'SO-P-17-057', 'Eugene Co Residence', 3, 0),
(198, 'SO-P-17-060', 'Ipil Place Residence', 5, 0),
(199, 'SO-P-17-061', 'Ramada Hotel', 5, 0),
(200, 'SO-P-17-063', 'James Lorenzana Residence', 3, 0),
(201, 'SO-P-17-065', 'Woodlands Point Cabin Type 2A Project', 3, 0),
(202, 'SO-P-17-069', 'Banocnoc Residence', 3, 0),
(203, 'SO-P-17-075', 'Escaler Residence', 3, 0),
(204, 'SO-P-17-077', 'Stephen Ang Residence', 3, 0),
(205, 'SO-P-17-078', 'Ricardo Po Residence', 3, 0),
(206, 'SO-P-17-081', 'The District Commercial Building', 5, 0),
(207, 'SO-P-17-093', 'Lu Residence', 3, 0),
(208, 'SO-P-17-096', 'JTI Office Fit-Out', 3, 0),
(209, 'SO-P-17-097', 'DLSU-D CEAT Building', 5, 0),
(210, 'SO-P-17-099', 'Calumpang Residence', 5, 0),
(211, 'SO-P-18-022', 'Suclaban Residential Development', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `review_logs`
--

CREATE TABLE `review_logs` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Reviewer_ID` int(10) UNSIGNED NOT NULL,
  `Project_ID` int(10) UNSIGNED NOT NULL,
  `Ticket_Number` varchar(30) NOT NULL,
  `Designer_ID` int(10) UNSIGNED NOT NULL,
  `Comments` varchar(255) NOT NULL,
  `Design_Accuracy` int(11) NOT NULL,
  `Timeliness` int(11) NOT NULL,
  `Completeness` int(11) NOT NULL,
  `Rating` int(1) UNSIGNED NOT NULL,
  `Review_Date` date NOT NULL,
  `Time_In` time NOT NULL,
  `Time_Out` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `Task` varchar(255) NOT NULL,
  `Code` varchar(50) NOT NULL,
  `Trade` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`ID`, `Task`, `Code`, `Trade`) VALUES
(1, 'General Notes\r\n', 'CD-30-P', 'P'),
(2, 'WW & RW Layouts\r\n', 'CD-30-P', 'P'),
(3, 'WS Layouts\r\n', 'CD-30-P', 'P'),
(4, 'Standard Details\r\n', 'CD-30-P', 'P'),
(5, 'Calculations\r\n', 'CD-30-P', 'P'),
(6, 'Specifications\r\n', 'CD-30-P', 'P'),
(7, 'Model\r\n', 'CD-30-P', 'P'),
(8, 'Equipment Schedule & Tanks and Pumps Det.\r\n', 'CD-20-P', 'P'),
(9, 'Calculations\r\n', 'CD-20-P', 'P'),
(10, 'Specifications\r\n', 'CD-20-P', 'P'),
(11, 'Model\r\n', 'CD-20-P', 'P'),
(12, 'Schematic RW Drainage Riser Diagram\r\n', 'CD-50-P', 'P'),
(13, 'Schematic WW Drainage Riser Diagram\r\n', 'CD-50-P', 'P'),
(14, 'Schematic W Riser Diagram\r\n', 'CD-50-P', 'P'),
(15, 'Miscellaneous Details\r\n', 'CD-50-P', 'P'),
(16, 'Blowup Toilet and Isometry\r\n', 'CD-50-P', 'P'),
(17, 'Calculations\r\n', 'CD-50-P', 'P'),
(18, 'Specifications\r\n', 'CD-50-P', 'P'),
(19, 'Model\r\n', 'CD-50-P', 'P'),
(20, 'A4 sheets\r\n', 'CD-40-P', 'P'),
(21, 'Calculations\r\n', 'CD-40-P', 'P'),
(22, 'Specifications\r\n', 'CD-40-P', 'P'),
(23, 'Model\r\n', 'CD-40-P', 'P');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Team_Name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`ID`, `Team_Name`) VALUES
(1, 'House Arryn'),
(5, 'House Baratheon'),
(3, 'House Lannister'),
(2, 'House Stark'),
(6, 'House Targaryen'),
(4, 'House Tyrell');

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Ticket_Number` varchar(50) NOT NULL,
  `Project_ID` int(10) UNSIGNED NOT NULL,
  `Internaldl_ID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket`
--

INSERT INTO `ticket` (`ID`, `Ticket_Number`, `Project_ID`, `Internaldl_ID`) VALUES
(12, 'M-P1745-S-DD-01', 5, 12),
(13, 'M-P1737-E-TR-001', 6, 13),
(14, 'M-P1737-A-TR-001', 6, 14),
(15, 'M-P1737-S-TR-001', 6, 15),
(16, 'M-P1737-M-TR-001', 6, 16),
(17, 'M-P1737-AU-TR-001', 6, 17),
(18, 'M-P1737-P-TR-001', 6, 18),
(19, 'M-P1737-FP-TR-001', 6, 19),
(20, 'M-P1737-E-CD-001', 7, 20),
(21, 'M-P1737-S-CD-001', 7, 21),
(22, 'M-P1737-P-CD-001', 7, 22);

-- --------------------------------------------------------

--
-- Table structure for table `trigger_events`
--

CREATE TABLE `trigger_events` (
  `pushSched` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trigger_events`
--

INSERT INTO `trigger_events` (`pushSched`) VALUES
(0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL DEFAULT 'P@ssw0rd',
  `Firstname` varchar(50) NOT NULL,
  `Middlename` varchar(30) NOT NULL,
  `Lastname` varchar(30) NOT NULL,
  `Picture` varchar(100) NOT NULL,
  `User_Type` int(1) UNSIGNED NOT NULL,
  `Team_ID` int(10) UNSIGNED DEFAULT NULL,
  `Trade` varchar(3) NOT NULL,
  `Status` varchar(11) NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `Username`, `Password`, `Firstname`, `Middlename`, `Lastname`, `Picture`, `User_Type`, `Team_ID`, `Trade`, `Status`) VALUES
(1, 'rpaule', 'P@ssw0rd', 'Rolando', 'Lintag', 'Paule', '', 0, 1, 'CM', 'Active'),
(2, 'lvalencia', 'P@ssw0rd', 'Lutgard', 'De Leon', 'Valencia', '', 0, 1, 'CM', 'Active'),
(3, 'riguis', 'P@ssw0rd', 'Raymond', 'Ciriaco', 'Iguis', '', 0, 2, 'C', 'Active'),
(4, 'acarr', 'P@ssw0rd', 'Anthony ', 'Boaloy', 'Carr', '', 0, 2, 'E', 'Active'),
(5, 'jrigos', 'P@ssw0rd', 'Jarrell Ervyn', 'Vicente', 'Rigos', '', 0, 2, 'M', 'Active'),
(6, 'jtablate', 'P@ssw0rd', 'Johnbert', 'Encinas', 'Tablate', '', 0, 2, 'M', 'Active'),
(7, 'lartillaga', 'P@ssw0rd', 'Leomer', 'Amat', 'Artillaga', '', 3, 2, 'X', 'Active'),
(8, 'acarino', 'P@ssw0rd', 'Alden', 'Sagario', 'Cariño', '', 0, 2, 'S', 'Active'),
(9, 'erosales', 'P@ssw0rd', 'Emerson', 'Gabarda', 'Rosales', '', 0, 2, 'C', 'Active'),
(10, 'aregala', 'P@ssw0rd', 'Abegail', 'Calimlim', 'Regala', '', 0, 2, 'C', 'Active'),
(11, 'bcipres', 'P@ssw0rd', 'Bryan', 'Evangelista', 'Cipres', '', 0, 2, 'C', 'Active'),
(12, 'ecampaner', 'P@ssw0rd', 'Earl Jethro', 'Ferraer', 'Campaner', '', 0, 2, 'P', 'Active'),
(13, 'dfilarca', 'P@ssw0rd', 'Donald Ray', 'Sanchez', 'Filarca', '', 0, 2, 'S', 'Active'),
(14, 'kbien', 'P@ssw0rd', 'Krizel', 'Castelo', 'Bien', '', 0, 2, 'F', 'Active'),
(15, 'eobnamia', 'P@ssw0rd', 'Erica Nira', 'Ingal', 'Obnamia', '', 0, 2, 'S', 'Active'),
(16, 'jtolin', 'P@ssw0rd', 'John Paul ', 'Bello', 'Tolin', '', 0, 2, 'S', 'Active'),
(17, 'mresurreccion', 'P@ssw0rd', 'Matthew', 'Laureano', 'Resurreccion', '', 0, 2, 'E', 'Active'),
(18, 'mbelgar', 'P@ssw0rd', 'Macy', 'Collantes', 'Belgar', '', 0, 2, 'C', 'Active'),
(19, 'jcuales', 'P@ssw0rd', 'Janelin', 'Pamares', 'Cuales', '', 0, 2, 'C', 'Active'),
(20, 'lbasit', 'P@ssw0rd', 'Leica Reana', 'Afable', 'Basit', '', 2, 2, 'D', 'Active'),
(21, 'jsanchez', 'P@ssw0rd', 'Joey ', 'Cabales', 'Sanchez', '', 3, 2, 'X', 'Active'),
(22, 'apacaco', 'P@ssw0rd', 'Angel', 'Arcega', 'Pacaco', '', 0, 3, 'C', 'Active'),
(23, 'arebustillo', 'P@ssw0rd', 'Aldrin ', 'Bornales', 'Rebustillo', '', 0, 3, 'C', 'Active'),
(24, 'shyodo', 'P@ssw0rd', 'Stephanie Chariz ', 'Enriquez', 'Hyodo', '', 0, 3, 'C', 'Active'),
(25, 'bbueno', 'P@ssw0rd', 'Bernie ', 'Volante', 'Bueno', '', 0, 3, 'M', 'Active'),
(26, 'balmodovar', 'P@ssw0rd', 'Bryan Jose', 'Santurdio', 'Almodovar', '', 3, 3, 'X', 'Active'),
(27, 'kadic', 'P@ssw0rd', 'Karen Joyce', 'Mariño', 'Adic', '', 0, 3, 'E', 'Active'),
(28, 'msubijano', 'P@ssw0rd', 'Mary Jane', 'Castillo', 'Subijano', '', 0, 3, 'S', 'Active'),
(29, 'tfelicia', 'P@ssw0rd', 'Timothy Daniel', 'De Juan', 'Felicia', '', 3, 3, 'X', 'Active'),
(30, 'rtungcab', 'P@ssw0rd', 'Rei Kevin', 'Catantan', 'Tungcab', '', 0, 3, 'S', 'Active'),
(31, 'dayochok', 'P@ssw0rd', 'Diane Oniedelle ', 'Domingo', 'Ayochok', '', 0, 3, 'S', 'Active'),
(32, 'kmupada', 'P@ssw0rd', 'Kristine Camille', 'Villalon', 'Mupada', '', 0, 3, 'C', 'Active'),
(33, 'renriquez', 'P@ssw0rd', 'Raymart', 'Banawa', 'Enriquez', '', 2, 3, 'D', 'Active'),
(34, 'mmiranda', 'P@ssw0rd', 'Mark Joseph', 'Aperocho', 'Miranda', '', 0, 3, 'S', 'Active'),
(35, 'kvillanueva', 'P@ssw0rd', 'Karen Mae', 'Desepeda', 'Villanueva', '', 0, 3, 'S', 'Active'),
(36, 'mbagtas', 'P@ssw0rd', 'Marites', 'Ramos', 'Bagtas', '', 0, 3, 'E', 'Active'),
(37, 'mbelen', 'P@ssw0rd', 'Marvin', 'Macalindol', 'Belen', '', 0, 3, 'M', 'Active'),
(38, 'ajose', 'P@ssw0rd', 'Argie', 'Bañez', 'Jose', '', 0, 3, 'S', 'Active'),
(39, 'dtan', 'P@ssw0rd', 'Duizelle Rose Ann', 'Sy', 'Tan', '', 0, 3, 'S', 'Active'),
(40, 'rpineda', 'P@ssw0rd', 'Rhobe', 'Lanto', 'Pineda', '', 0, 3, 'P', 'Active'),
(41, 'meroyla', 'P@ssw0rd', 'Maruel', 'Ventolina', 'Eroyla', '', 0, 3, 'P', 'Active'),
(42, 'jdejesus', 'P@ssw0rd', 'Jim Aldwin', 'Laka', 'De Jesus', '', 0, 3, 'P', 'Active'),
(43, 'kvelasco', 'P@ssw0rd', 'Kimverly', 'Panit', 'Velasco', '', 0, 3, 'AU', 'Active'),
(44, 'mdionisio', 'P@ssw0rd', 'Maricar', 'Jacang', 'Dionisio', '', 2, 3, 'D', 'Active'),
(45, 'jhernal', 'P@ssw0rd', 'Jose Michael', 'Geralde', 'Hernal', '', 3, 3, 'X', 'Active'),
(46, 'mjorolan', 'P@ssw0rd', 'Mark Louie ', 'Rosario', 'Jorolan', '', 0, 4, 'C', 'Active'),
(47, 'klavadia', 'P@ssw0rd', 'Kevin Louie ', 'Cedro', 'Lavadia', '', 0, 4, 'A', 'Active'),
(48, 'dbaltazar', 'P@ssw0rd', 'Danah Dainielle ', 'Casaldan', 'Baltazar', '', 0, 4, 'S', 'Active'),
(49, 'jamparo', 'P@ssw0rd', 'Jolly', 'Marquez', 'Amparo', '', 0, 4, 'M', 'Active'),
(50, 'jramos', 'P@ssw0rd', 'Jervin Roi', 'Carrao', 'Ramos', '', 0, 4, 'E', 'Active'),
(51, 'jocampo', 'P@ssw0rd', 'John Paolo ', 'Luna', 'Ocampo', '', 0, 4, 'P', 'Active'),
(52, 'rpagilagan', 'P@ssw0rd', 'Rashleigh Rhon', 'Vallestero', 'Pagilagan', '', 0, 4, 'E', 'Active'),
(53, 'ebardelosa', 'P@ssw0rd', 'Elaine Aubrey', 'Paghubasan', 'Bardelosa', '', 0, 4, 'F', 'Active'),
(54, 'jpanganiban', 'P@ssw0rd', 'James Mchenry', 'Batiquin', 'Panganiban', '', 0, 4, 'S', 'Active'),
(55, 'nbendulo', 'P@ssw0rd', 'Nestor', 'Montuno', 'Bendulo', '', 0, 4, 'S', 'Active'),
(56, 'hesteban', 'P@ssw0rd', 'Hazel', 'Ventolero', 'Esteban', '', 0, 4, 'C', 'Active'),
(57, 'imorfe', 'P@ssw0rd', 'Ingria Shean', 'Tejones', 'Morfe', '', 0, 4, 'A', 'Active'),
(58, 'alasam', 'P@ssw0rd', 'Angel', 'Villasana', 'Lasam', '', 0, 4, 'C', 'Active'),
(59, 'dgavino', 'P@ssw0rd', 'Dylan', 'Cantalejo', 'Gavino', '', 0, 4, 'S', 'Active'),
(60, 'cpascual', 'P@ssw0rd', 'Carla Niña', 'Furigay', 'Pascual', '', 3, 4, 'X', 'Active'),
(61, 'hgaton', 'P@ssw0rd', 'Hana Myka', 'Claridad', 'Gaton', '', 0, 4, 'S', 'Active'),
(62, 'jcasile', 'P@ssw0rd', 'John David', 'Esteban', 'Casile', '', 0, 4, 'A', 'Active'),
(63, 'rorpia', 'P@ssw0rd', 'Ronald Roger', 'Amaro', 'Orpia', '', 0, 4, 'S', 'Active'),
(64, 'nruzgal', 'P@ssw0rd', 'Normalyn', 'Baluran', 'Ruzgal', '', 0, 4, 'C', 'Active'),
(65, 'jtorejas', 'P@ssw0rd', 'Jesty', 'Eballe', 'Torejas', '', 0, 4, 'P', 'Active'),
(66, 'rcruz', 'P@ssw0rd', 'Raziel Samantha', 'Del Mundo', 'Cruz', '', 3, 4, 'X', 'Active'),
(67, 'rlogdat', 'P@ssw0rd', 'Revindell', 'De Guzman', 'Logdat', '', 0, 4, 'C', 'Active'),
(68, 'rramirez', 'P@ssw0rd', 'Rodge Kristofer', 'De Ocampo', 'Ramirez', '', 0, 4, 'AU', 'Active'),
(69, 'ebajarias', 'P@ssw0rd', 'Esclabanan', 'Ma. Christina', 'Bajarias', '', 0, 4, 'A', 'Active'),
(70, 'aremigio', 'P@ssw0rd', 'Aizza Zerina ', 'Fernandez', 'Remigio', '', 3, 4, 'X', 'Active'),
(71, 'klagata', 'P@ssw0rd', 'Kelvin Klein', 'Francisco', 'Lagata', '', 0, 5, 'S', 'Active'),
(72, 'cferriol', 'P@ssw0rd', 'Christine Ann', 'Sanchez', 'Ferriol', '', 0, 5, 'S', 'Active'),
(73, 'kcubacub', 'P@ssw0rd', 'Katrina', 'Gorgonio', 'Cubacub', '', 3, 5, 'X', 'Active'),
(74, 'lgimenez', 'P@ssw0rd', 'Louie, Jr.', 'Raña', 'Gimenez', '', 0, 5, 'C', 'Active'),
(75, 'pesguerra', 'P@ssw0rd', 'Patricia Erika', 'Bacsibio', 'Esguerra', '', 3, 5, 'X', 'Active'),
(76, 'lviterbo', 'P@ssw0rd', 'Leoniel', 'Cabuyao', 'Viterbo', '', 0, 5, 'S', 'Active'),
(77, 'mgualberto', 'P@ssw0rd', 'Maricris', 'Pecson', 'Gualberto', '', 0, 5, 'AU', 'Active'),
(78, 'rbusano', 'P@ssw0rd', 'Romeo', 'Butlig', 'Busano', '', 0, 5, 'C', 'Active'),
(79, 'pbulosan', 'P@ssw0rd', 'Patricia Mae', 'Olete', 'Bulosan', '', 0, 5, 'C', 'Active'),
(80, 'icortez', 'P@ssw0rd', 'Imee', 'Bulan', 'Cortez', '', 0, 5, 'P', 'Active'),
(81, 'mbron', 'P@ssw0rd', 'Martin Teotimo', 'Clemeña', 'Bron', '', 0, 5, 'S', 'Active'),
(82, 'jlaude', 'P@ssw0rd', 'John Kevin Heim', 'Maguad', 'Laude', '', 0, 5, 'P', 'Active'),
(83, 'alim', 'P@ssw0rd', 'Antonette', 'Manlapaz', 'Lim', '', 0, 5, 'S', 'Active'),
(84, 'mmangaliman', 'P@ssw0rd', 'Mandy', 'Diwa', 'Mangaliman', '', 0, 5, 'S', 'Active'),
(85, 'jobon', 'P@ssw0rd', 'Janine Kayzelyn', 'Balsomo', 'Obon', '', 2, 5, 'D', 'Active'),
(86, 'hdevera', 'P@ssw0rd', 'Hazzel Joyce', 'Bautista', 'De Vera', '', 0, 5, 'E', 'Active'),
(87, 'jpactao', 'P@ssw0rd', 'John Embhee', '', 'Pactao', '', 0, 5, 'C', 'Active'),
(88, 'mmartinez', 'P@ssw0rd', 'Mariel Eunice', 'Cate', 'Martinez', '', 0, 5, 'C', 'Active'),
(89, 'hdelrio', 'P@ssw0rd', 'Hanna Georgia', 'Dinaranan', 'Del Rio', '', 2, 5, 'D', 'Active'),
(90, 'jsepnio', 'P@ssw0rd', 'Jonalyn', 'Garcia', 'Sepnio', '', 3, 5, 'X', 'Active'),
(91, 'abella', 'P@ssw0rd', 'Anthony Jerome', '', 'Bella', '', 0, 6, 'S', 'Active'),
(92, 'jsanchez', 'P@ssw0rd', 'Jonathan Alfonso', 'Dellosa', 'Sanchez', '', 0, 6, 'S', 'Active'),
(93, 'rdevilla', 'P@ssw0rd', 'Reymark  ', 'Furto', 'De Villa', '', 0, 6, 'S', 'Active'),
(94, 'ereario', 'P@ssw0rd', 'Edgardo Jose', 'Marzalado', 'Reario', '', 0, 6, 'S', 'Active'),
(95, 'dobias', 'P@ssw0rd', 'Danzel', 'Capulong', 'Obias', '', 5, 7, 'AC', 'Active'),
(96, 'fmandac', 'P@ssw0rd', 'Frederick', 'Gutierrez', 'Mandac', '', 5, 7, 'AC', 'Active'),
(97, 'jquinito', 'P@ssw0rd', 'Jollif Fe', 'Apuya', 'Quinito', '', 5, 7, 'AC', 'Active'),
(98, 'mcastillo', 'P@ssw0rd', 'Marie Claire Suerte', 'Casiano', 'Castillo', '', 5, 7, 'AC', 'Active'),
(99, 'rdeguzman', 'P@ssw0rd', 'Rosabel', 'Salonga', 'De Guzman', '', 5, 7, 'AC', 'Active'),
(100, 'njavier', 'P@ssw0rd', 'Nercia', 'Panes', 'Javier', '', 6, 8, 'B', 'Active'),
(101, 'lllagas', 'P@ssw0rd', 'Lorena ', 'Lawaan', 'Llagas', '', 6, 8, 'B', 'Active'),
(102, 'vcoronel ', 'P@ssw0rd', 'Vianca', 'Ayroso', 'Coronel ', '', 6, 8, 'B', 'Active'),
(103, 'jaquino', 'P@ssw0rd', 'Jairo', 'Manalili', 'Aquino', '', 6, 8, 'B', 'Active'),
(104, 'amartinez', 'P@ssw0rd', 'Annamae', 'Calingasan', 'Martinez', '', 6, 8, 'B', 'Active'),
(105, 'jenojo', 'P@ssw0rd', 'Jennyfer', 'Balingit', 'Enojo', '', 6, 8, 'B', 'Active'),
(106, 'yyanez', 'P@ssw0rd', 'Yanze Makarius', 'Ty', 'Yañez', '', 6, 8, 'B', 'Active'),
(107, 'ksamonte', 'P@ssw0rd', 'Kristian Ken', 'Medina', 'Samonte', '', 6, 8, 'B', 'Active'),
(108, 'aocampo', 'P@ssw0rd', 'Angelica Lynn', 'Amor', 'Ocampo', '', 6, 8, 'B', 'Active'),
(109, 'rbonilla', 'P@ssw0rd', 'Robel', 'Ordanez', 'Bonilla', '', 6, 8, 'B', 'Active'),
(110, 'aong', 'P@ssw0rd', 'Alden', 'Chua', 'Ong', '', 6, 8, 'B', 'Inactive'),
(111, 'jpatulot', 'P@ssw0rd', 'Jerraemie Nikka', 'Cipres', 'Patulot', '', 6, 8, 'B', 'Active'),
(112, 'pyanong', 'P@ssw0rd', 'Peter Jan', 'Diabo', 'Yanong', '', 7, 9, 'I', 'Active'),
(113, 'npayongayong', 'P@ssw0rd', 'Niclieyet', 'Cideria', 'Payongayong', '', 7, 9, 'AD', 'Active'),
(114, 'stalatayod', 'P@ssw0rd', 'Sarah', 'Cabaltera', 'Talatayod', '', 7, 9, 'AD', 'Active'),
(115, 'pmaggay', 'P@ssw0rd', 'Pedrito Jr.', 'De Guzman', 'Maggay', '', 7, 9, 'AD', 'Active'),
(116, 'anortugal', 'P@ssw0rd', 'Anzeneth Marie', 'Lamadrid', 'Portugal', '', 7, 9, 'AD', 'Active'),
(117, 'jalmariego', 'P@ssw0rd', 'Jobel', 'Abapo', 'Almariego', '', 7, 9, 'I', 'Active'),
(118, 'barenas', 'P@ssw0rd', 'Brian Francis', 'Bernardino', 'Arenas', '', 7, 9, 'AD', 'Active'),
(119, 'vgalvoso', 'P@ssw0rd', 'Van Hudson', '', 'Galvoso', '', 7, 9, 'I', 'Active'),
(120, 'jbarraza', 'P@ssw0rd', 'Jocelyn', 'Paluya', 'Barraza', '', 7, 9, 'AD', 'Active'),
(121, 'ekagingin', 'P@ssw0rd', 'Earl Louis ', 'Rebulado', 'Kagingin', '', 7, 9, 'AD', 'Active'),
(122, 'rpenaflor', 'P@ssw0rd', 'Rubie Anne', 'Gorgonia', 'Penaflor', '', 7, 9, 'AD', 'Active'),
(123, 'ptrinidad', 'P@ssw0rd', 'Paula May', 'Murillo', 'Trinidad', '', 7, 9, 'AD', 'Active'),
(124, 'mmanago', 'P@ssw0rd', 'Mae Kareen', 'Bahillo', 'Mañago', '', 1, 10, 'S', 'Active'),
(125, 'calcantara', 'P@ssw0rd', 'Conrado', 'Cudal', 'Alcantara', '', 1, 10, 'F', 'Active'),
(126, 'ecruz', 'P@ssw0rd', 'Ernesto', 'Francisco', 'Cruz', '', 1, 10, 'S', 'Active'),
(127, 'ptamayo', 'P@ssw0rd', 'Pablo Jr.', 'Pama', 'Tamayo', 'default.png', 1, 10, 'P', 'Active'),
(128, 'rnavarro', 'P@ssw0rd', 'Ronald', 'Bernardo', 'Navarro', '', 1, 10, 'AU', 'Active'),
(129, 'msalvador', 'P@ssw0rd', 'Michelangela', 'Ortiz', 'Salvador', '', 1, 10, 'A', 'Active'),
(130, 'jventura', 'P@ssw0rd', 'Joaquin ', 'Jabal', 'Ventura', '', 1, 10, 'C', 'Active'),
(131, 'amerillo', 'P@ssw0rd', 'Arthur', 'Quiros', 'Merillo', '', 1, 10, 'M', 'Active'),
(132, 'acabanas', 'P@ssw0rd', 'Arminda', '', 'Cabañas', '', 1, 10, 'S', 'Active'),
(133, 'rsuarez', 'P@ssw0rd', 'Raphael Louis', 'Ong', 'Suarez', '', 4, 11, 'X', 'Active'),
(134, 'rdamiar', 'P@ssw0rd', 'Russel ', 'Javines', 'Damiar', '', 4, 11, 'S', 'Active'),
(135, 'mong', 'P@ssw0rd', 'Ma. Cynthia', 'Mandac', 'Ong', '', 4, 11, '0', 'Inactive'),
(136, 'eabetchuela', 'P@ssw0rd', 'Elona ', 'Valdez', 'Abetchuela', '', 4, 11, '0', 'Active'),
(137, 'aesguerra', 'P@ssw0rd', 'Alyssa Jennica', 'Lopez', 'Esguerra', '', 4, 11, 'X', 'Active'),
(138, 'rmanabat', 'P@ssw0rd', 'Rodel', 'Bautista', 'Manabat', '', 4, 11, '0', 'Active'),
(139, 'esinuhin', 'P@ssw0rd', 'Eula', 'De Guzman', 'Sinuhin', '', 0, 13, 'S', 'Active'),
(140, 'mcabrera', 'P@ssw0rd', 'Maria Rosaly', 'Ariola', 'Cabrera', '', 0, 13, 'S', 'Active'),
(141, 'rsalvador', 'P@ssw0rd', 'Regine Raulen', 'Diola', 'Salvador', '', 0, 13, 'S', 'Active'),
(142, 'mhernandez', 'P@ssw0rd', 'Mariz', 'Castor', 'Hernandez', '', 0, 13, 'S', 'Active'),
(143, 'kdelarosa', 'P@ssw0rd', 'Karl Dale', 'Queliza', 'Dela Rosa', '', 0, 14, 'C', 'Active'),
(144, 'jambrocio', 'P@ssw0rd', 'Jopet', 'Dizon', 'Ambrocio', '', 0, 14, 'C', 'Active'),
(145, 'lcollamar', 'P@ssw0rd', 'Lym John', 'Toballas', 'Collamar', '', 0, 14, 'C', 'Active'),
(146, 'jtacsiat', 'P@ssw0rd', 'Jhunnel', 'Frisco', 'Tacsiat', '', 0, 14, 'C', 'Active'),
(147, 'cdungca', 'P@ssw0rd', 'Christopher', 'Aparejada', 'Dungca', '', 0, 14, 'C', 'Active'),
(148, 'vpoblete', 'P@ssw0rd', 'Vhernon', 'Pascua', 'Poblete', '', 0, 14, 'C', 'Active'),
(149, 'r_rmanabat', 'P@ssw0rd', 'Rodel', 'Bautista', 'Manabat', '', 1, 11, 'E', 'Active'),
(150, 'cadmanager', 'P@ssw0rd', 'Cad ', '', 'Manager', '', 8, 1, 'P', 'Active'),
(151, 'cadtechnician', 'P@ssw0rd', 'Cad', '', 'Technician', '', 9, 1, 'p', 'Active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD UNIQUE KEY `ID` (`ID`),
  ADD KEY `Designer_ID` (`Designer_ID`);

--
-- Indexes for table `dc_documents`
--
ALTER TABLE `dc_documents`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `dc_email_log`
--
ALTER TABLE `dc_email_log`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `dc_meeting_log`
--
ALTER TABLE `dc_meeting_log`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `dc_submittal`
--
ALTER TABLE `dc_submittal`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `designer_notif`
--
ALTER TABLE `designer_notif`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `external_deadline`
--
ALTER TABLE `external_deadline`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Project_ID` (`Project_ID`);

--
-- Indexes for table `internal_deadline`
--
ALTER TABLE `internal_deadline`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Externaldl_ID` (`Externaldl_ID`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `User_ID` (`User_ID`),
  ADD KEY `Internaldl_ID` (`Internaldl_ID`),
  ADD KEY `Internaldl_ID_2` (`Internaldl_ID`),
  ADD KEY `Internaldl_ID_3` (`Internaldl_ID`),
  ADD KEY `Internaldl_ID_4` (`Internaldl_ID`),
  ADD KEY `Internaldl_ID_5` (`Internaldl_ID`);

--
-- Indexes for table `overtime`
--
ALTER TABLE `overtime`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `User_ID` (`User_ID`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Team_ID` (`Team_ID`);

--
-- Indexes for table `review_logs`
--
ALTER TABLE `review_logs`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Reviewer_ID` (`Reviewer_ID`),
  ADD KEY `Designer_ID` (`Designer_ID`),
  ADD KEY `Project_ID` (`Project_ID`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Team_Name` (`Team_Name`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Project_ID` (`Project_ID`),
  ADD KEY `Internaldl_ID` (`Internaldl_ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Team_ID` (`Team_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_logs`
--
ALTER TABLE `activity_logs`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dc_documents`
--
ALTER TABLE `dc_documents`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dc_email_log`
--
ALTER TABLE `dc_email_log`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `dc_meeting_log`
--
ALTER TABLE `dc_meeting_log`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `dc_submittal`
--
ALTER TABLE `dc_submittal`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `designer_notif`
--
ALTER TABLE `designer_notif`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `external_deadline`
--
ALTER TABLE `external_deadline`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `internal_deadline`
--
ALTER TABLE `internal_deadline`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `overtime`
--
ALTER TABLE `overtime`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;

--
-- AUTO_INCREMENT for table `review_logs`
--
ALTER TABLE `review_logs`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD CONSTRAINT `activity_logs_ibfk_1` FOREIGN KEY (`Designer_ID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE;

--
-- Constraints for table `external_deadline`
--
ALTER TABLE `external_deadline`
  ADD CONSTRAINT `external_deadline_ibfk_1` FOREIGN KEY (`Project_ID`) REFERENCES `project` (`ID`) ON UPDATE CASCADE;

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`User_ID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE;

--
-- Constraints for table `overtime`
--
ALTER TABLE `overtime`
  ADD CONSTRAINT `overtime_ibfk_1` FOREIGN KEY (`User_ID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE;

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_ibfk_1` FOREIGN KEY (`Team_ID`) REFERENCES `team` (`ID`) ON UPDATE CASCADE;

--
-- Constraints for table `review_logs`
--
ALTER TABLE `review_logs`
  ADD CONSTRAINT `review_logs_ibfk_1` FOREIGN KEY (`Reviewer_ID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `review_logs_ibfk_2` FOREIGN KEY (`Designer_ID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `review_logs_ibfk_3` FOREIGN KEY (`Project_ID`) REFERENCES `project` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
