<?php
session_start();
include("connect.php");
$today = date("F j, Y");	
	$tradeString = $_SESSION['Trade'];
  $shortcut_name = $_SESSION["Lastname"].", ".$_SESSION["Firstname"];
if(isset($_SESSION["User_Type"]))
{
	switch($_SESSION["User_Type"])
	{
		case 0:	//DESIGNER
			header("Location:home.php");
			break;
		case 1:	//Reviewer
			header("Location:reviewer.php");
			break;
		case 2:	//DC
			header("Location:dc.php");
			break;
		case 3:	//PM
			header("Location:pm.php");
			break;
		case 4:	//OPERATIONS
			header("Location:ops.php");
			break;
		case 5:	//Admin
			header("Location:admin.php");
			break;
	}
}
else
{
	header("Location:index.php");
}
?>

<html>
<head>
	<title> Activity Monitoring </title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/cadManager.css" />
	<link rel="icon" href="img/logoblue.png">

	<script src="script.js"></script>
	<script src="jquery.js"></script>
	<script src="scripts/internal_deadline_computation.js"></script>
	<script src="jquery-3.2.1.min.js"></script>
	<script src="scripts/javaScript.js"></script>
  
  <style>
      
    #assign_btn{
        display:none;
    }
    
    #navbar tr td#tasks{
    background-color: #deebff;
    color:#0747a6;
    }
    
    #sel_cad_tech_container{
      display:inline-block;
    }
    
    #cad_tech_container{
      position:fixed;
      display:none;
      width:250px;
      margin-top:0px;
    }
    
    #mainform input[type=text]#select_cad_tbox{
      width:250px;
      margin-bottom:0px;
    }
    
    body{
        font-family:"Product Sans",sans-serif;
    }
    
  #navbar tr td#des_tracking{
    background-color: #deebff;
    color:#0747a6;
  }
    
  #navbar tr td#timesheet{
    background-color: #0747a6;
    color:#deebff;
  }
      
  #list{
    height:90%;
    width:80%;
    min-height:300px;
    margin-right:0px;
    resize:none;
  }

  .general_table th{
      padding:2px;
  }
  
  .miniheader{
      background-color:#eef1f2;
      margin:0px;
      position:fixed;
      left:0px;
      top:60px;
      width:100%;
      height:100px;
      border-bottom:solid 2px #ececec;
  }
  
  .container2{
      margin-top:130px;
  }
  
  .tbl-tab-group{
      position:fixed;
      padding-left:25px;
      top:115px;
  }
  
  .tbl-tab{
      font-size:16px;
      font-weight:bold;
      display:inline-block;
      background-color:#f7f9fa;
      border-radius:4px;
      padding:10px 17px;
      cursor:pointer;
      filter:brightness(100%);
  }
  
  .tbl-tab:hover{
      filter:brightness(75%);
  }
  
  .active-tbl-tab{
      border-radius:4px 4px 0px 0px;
      background-color:#f7f9fa;
      border-bottom: solid 2px #f7f9fa;
      border-top:solid 2px #ececec;
      border-left:solid 2px #ececec;
      border-right:solid 2px #ececec;
  }
  
  #shortcut_name{
      font-size:30px;
      font-weight:bold;
      margin-left:30px;
  }
  
  #percent_holder{
      position:fixed;
      right:50px;
      top:80px;
  }
  
  #percent_bar{
      border-radius:4px;
      border:solid 4px #515151;
      width:208px;
      height:22px;
      display:inline-block;
      
  }
  
  #progress{
      background-color:red;
      width:0px;
      height:22px;
  }
  
  #percent_number{
      font-size:50px;
  }
  
  #pending_count{
      
  }
</style>
</head>
<body onload="updateReport();">
	<?php include('header.php'); ?>
	<div id="mainform">
      
<div id="list" class="width-99pc">
    <div class="miniheader">
      <label id="shortcut_name"><?php echo $shortcut_name; ?></label>
      <ul class="tbl-tab-group">
          <li class="tbl-tab active-tbl-tab" id="standby_tab"> Standby <span id="standby_count">(0)</span> </li>
          <li class="tbl-tab" id="assigned_tab"> Assigned <span id="assigned_count">(0)</span> </li>
          <li class="tbl-tab" id="requested_tab"> Requested <span id="requested_count">(0)</span> </li>
          <li class="tbl-tab" id="rejected_tab"> Rejected <span id="rejected_count">(0)</span> </li>
          <li class="tbl-tab" id="approved_tab"> Approved <span id="approved_count">(0)</span> </li>
      </ul>
      <div id="percent_holder">
        <div id="percent_bar">
          <div id="progress"></div>
        </div>
        <label id="percent_number"> 30% </label>
      </div>
    </div>
    <div class="container2">
        <table class="general_table width-100pc">
            <thead>
                <tr>
                    <th> Designer </th>
                    <th> Deadline </th>
                    <th> Project </th>
                    <th> Requirements </th>
                    <th> Ticket </th>
                    <th> CAD Technician </th>
                </tr>
            </thead>
            <tbody id="design_list">
             <!-- Data will be sent from pushSchedule.php -->
            </tbody>
        </table>			
    </div>
</div>	
  <div id="white_bg">
    <div id="back_data" style="margin-top:60px;display: none;" class="width-20pc">
      <input type="text" id="selected_designer_id" />
      <input type="text" id="selected_cad_tech_id" />
      <input type="text" id="selected_project_id" />
      <input type="text" id="selected_des_req_id" />
    </div>
  </div>
  <div id="white_pop">
    <input type="submit"
           id="xbtn"
           class="btn-normal bg-red white emphasize"
           value="x"/>
    <div id="sel_cad_tech_container">
      <label> CAD Technician </label>
      <br />
      <input type="text" id="select_cad_tbox" class="drop_filter" placeholder="Select CAD Technician" />
      <div id="cad_tech_container" class="asselect">
        <p class="asoption"> Sample </p>
      </div>
    </div>
    <br />
    <br />
    <label> Deadline </label>
    <br />
    <input type="date" id="cad_tech_deadline" readonly="readonly" class="text_box" />
    <br />
    <br />
    <label> Budget </label>
    <br />
    <input type="text" id="cad_tech_budget" maxlength="1" class="bugdet_inp" placeholder="Hrs" />
    <input type="submit" id="assign_btn" value="Assign" class="btn-normal bg-blue white bold" />
    <input type="submit" id="done_btn" value="Done" class="btn-normal bg-green white bold" />
    
  </div>
  </div>
<script>
  init();
  function init(){
  $.ajax({
    url:'pushSchedule.php',
    type:'post',
    data:'for_cadmngr_designtracking=true',
    success: function(data)
    {
      $('#design_list').html(data);
      $('#percent_number').html(computeProgress()+ "%");
    },
    error:function(data){
      alert(data);
    }
  });
  
  $('#cad_tech_deadline').val(dateToday());
  }
  
       function computeProgress(){
        var standby = $('#standby_count').html().replace("(","");
         standby = parseInt(standby.replace(")",""));
        var assigned = $('#assigned_count').html().replace("(","");
         assigned = parseInt(assigned.replace(")",""));
        var requested = $('#requested_count').html().replace("(","");
         requested = parseInt(requested.replace(")",""));
        var rejected = $('#rejected_count').html().replace("(","");
         rejected = parseInt(rejected.replace(")",""));
        var approved = $('#approved_count').html().replace("(","");
         approved = parseInt(approved.replace(")",""));
         
         var total = parseInt(standby + assigned + requested + rejected + approved);
        var percent = parseInt(approved * (100/total));
        $('#progress').css("width",percent+"%");
        if(percent <= 15)
        {
          $('#progress').css("background-color","#ed1c26");
        }
        else if(percent <= 75)
        {
          $('#progress').css("background-color","#ffc107");
        }
        else if(percent <= 100)
        {
          $('#progress').css("background-color","#4caf50");
        }
        if(isNaN(percent))
        {
          percent = 0;
        }
        return percent;
  }
  function updateReport()
  {
    $.ajax(
            {
              url:'reviewtable.php',
              type:'post',
              data:'submit=true',
              success:function(data)
              {
              },
            });
  }

	$(document).ready(function(){
    $('#tasks').on('click',function(){
      location.reload();
    });
    $('#reviews').on('click',function(){
      var value = $(this).val();
      $.ajax(
      {
        url:'cadmngrReviews.php',
        type:'post',
        data:'pnumber='+value,
        success:function(data)
        {
          $('#mainform').html(data);
            $('#active_page').html("Reviews");
            $('#btn_hamburger').trigger('click');
        },
      });
    });
		$('#cad_tracking').on('click',function(){
			var value = $(this).val();
			$.ajax(
			{
				url:'cadmngrCadTracking.php',
				type:'post',
				data:'pnumber='+value,
				success:function(data)
				{
					$('#mainform').html(data);
          $.ajax({
            type:'get',
            url:'pushSchedule.php',
            data:'for_cadManager_cadTracking=true',
            success: function(data)
            {
              $('#weeklySchedule').html(data);
            }
          });
				},
			});
		});
		$('#assign_btn').on('click',function(){
			$.ajax(
			{
				url:'cadmngrSubmit.php',
				type:'post',
				data:'assign_cadTech=true'+
						 '&designer_id='+$('#selected_designer_id').val()+			
						 '&cadtech_id='+$('#selected_cad_tech_id').val()+			
						 '&project_id='+$('#selected_project_id').val()+
						 '&budget='+$('#cad_tech_budget').val()+
						 '&deadline='+$('#cad_tech_deadline').val()+
             '&pop_req_for_des_id='+$('#selected_des_req_id').val(),
				success:function(data)
				{
          location.reload();
				}
			});							
		});
	});

	$(document).ready(function(){
		$('#pop_cancel_btn').on('click',function(){
			hidePop();
		});
	});
	$(".searchbox").click(function (e) {
			$(".datalist").slideToggle();
			e.stopPropagation();
		});

		$(".datalist").click(function (e) {
			e.stopPropagation();
		});

		$(document).click(function () {
			$(".datalist").slideUp();
		});

  $('#pop_cadtech').on('click',function(){
    getNames();
    ('.datalist').slideDown();
  });
  
  $('#done_btn').on('click',function(){
    hidePop();
  });
    
  
  function getNames(){
    $.ajax({
      url:'getNames.php',
      type:'post',
      data:'from_cadManager=true',
      success:function(data){
        $('#cad_tech_list').html(data);
      }
    });
  }

  $('#white_bg').on('click',function(){
    hidePop();
  });

  function showPop(){
    $('#white_bg').fadeIn();
    $('#white_pop').slideDown();
  }
  
  function hidePop(){
    clearPopData();
    $('#white_bg').fadeOut();
    $('#white_pop').slideUp();
    $('#cad_tech_container').slideUp();
  }
  
  $('#select_cad_tbox').on('input change',function(){
      if($(this).val() != "")
      {
       $('#assign_btn').fadeIn();
       $('#done_btn').fadeIn();
      }
      else
      {
        $('#assign_btn').fadeOut();
        $('#done_btn').fadeOut();
      }
    });
    
    $('#select_cad_tbox').on('click',function(){
      getNames();
      $('#cad_tech_container').slideToggle();
    });
    
    function getNames(){
      $.ajax({
        url:'getNames.php',
        type:'post',
        data:'from_cadManager=true',
        success:function(data){
          $('#cad_tech_container').html(data);
        }
      });
    }
    
    function clearPopData(){
      $('#cad_tech_budget').val('');
      $('#select_cad_tbox').val('');
    }
    
    $('#standby_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#assigned_tab').prop("class","tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
      $('#approved_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $.ajax({
        url:'cadmngrGetData.php',
        type:'post',
        data:'get_cad_tracking=true'+
             '&status=Standby',
        success:function(data){
          $('#design_list').html(data);
        },
        
        error:function(data){
          alert(data);
        }
      });
    });
    
    $('#assigned_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#standby_tab').prop("class","tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
      $('#approved_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $.ajax({
        url:'cadmngrGetData.php',
        type:'post',
        data:'get_cad_tracking=true'+
             '&status=Assigned',
        success:function(data){
          $('#design_list').html(data);
        }
      });
    });
    
    $('#requested_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#assigned_tab').prop("class","tbl-tab");
      $('#standby_tab').prop("class","tbl-tab");
      $('#approved_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $.ajax({
        url:'cadmngrGetData.php',
        type:'post',
        data:'get_cad_tracking=true'+
             '&status=CADRequested',
        success:function(data){
          $('#design_list').html(data);
        }
      });
    });
    
    $('#approved_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#assigned_tab').prop("class","tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
      $('#standby_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $.ajax({
        url:'cadmngrGetData.php',
        type:'post',
        data:'get_cad_tracking=true'+
             '&status=CADAccepted',
        success:function(data){
          $('#design_list').html(data);
        }
      });
    });
    
    $('#rejected_tab').on('click',function(){
        $(this).prop("class","tbl-tab active-tbl-tab");
      $('#assigned_tab').prop("class","tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
      $('#standby_tab').prop("class","tbl-tab");
      $('#approved_tab').prop("class","tbl-tab");
        $.ajax({
          url:'cadmngrGetData.php',
          type:'post',
          data:'get_cad_tracking=true'+
               '&status=CADRejected',
          success:function(data){
            $('#design_list').html(data);
          }
        });
      });
      
      $('#xbtn').on('click',function(){
        white_popClose();
      });
      
      function white_popClose(){
        $('#white_pop').slideUp();	
        $('#select_cad_tbox').val("");
        $('#cad_tech_budget').val("");
        $('#cad_tech_container').hide();
      }
</script>
</body>
</html>