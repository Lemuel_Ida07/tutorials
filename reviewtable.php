<?php
	include('connect.php');
	session_start();
	$today = date("F j, Y");
	if(isset($_POST["submit"]))
	{
		$output = '';
		$output .= "<table id='reviewlist'>
		<thead>
			<tr>
				<th class='row-projno'>Project No.</th>
				<th class='row-ticketno'>Ticket No.</th>
				<th class='row-designer'>Designer</th>
				<th class='row-comments'>Comments</th>
				<th class='row-rating'>Rating</th>
				<th class='row-date'>Date</th>
				<th class='row-timein'>Time In</th>
				<th class='row-timeout'>Time Out</th>
			</tr>
		</thead>";
		
		$reviewer_id = $_SESSION["ID"];
		
		$sql = "SELECT 
					review_logs.ID, 
					review_logs.Reviewer_ID, 
					review_logs.Project_ID, 
					review_logs.Ticket_Number, 
					CONCAT(user.Firstname , ' ' , user.Middlename , ' ' , user.Lastname) AS Name , 
					review_logs.Comments, 
					review_logs.Rating, 
					review_logs.Date, 
					review_logs.Time_In, 
					review_logs.Time_Out,
					project.Project_Number
				 FROM review_logs 
					INNER JOIN project ON review_logs.Project_ID = project.ID 
					INNER JOIN user ON review_logs.Designer_ID = user.ID
				where Reviewer_ID = $reviewer_id";
			
		$result = mysqli_query($conn,$sql);
 	    if(mysqli_num_rows($result) > 0)
		{
			while($rows = mysqli_fetch_assoc($result))
			{
				$output.= "	<tr>
				<td>". $rows["Project_Number"] ."</td>
				<td>". $rows["Ticket_Number"] ."</td>
				<td>". $rows["Name"] ."</td>
				<td>". $rows["Comments"] ."</td>
				<td>". $rows["Rating"] ."</td>
				<td>". $rows["Date"] ."</td>
				<td>". $rows["Time_In"] ."</td>
				<td>". $rows["Time_Out"] ."</td>
				</td>
				</tr>";
			}
			$output .= "</table>";
										// header("Content-Type: application/xls");
										//header("Content-Disposition: attachment; filename=".preg_replace('/[[:space:]]+/', '_', $_SESSION['name'])."(".preg_replace('/[[:space:]]+/', '', $today).").xls");
										//file_put_contents('//192.168.0.254/i$/Production Reference/DC/'.preg_replace('/[[:space:]]+/', '_', $_SESSION['name']).'('.preg_replace('/[[:space:]]+/', '', $today).').xls', $output);
										//file_put_contents('//192.168.0.254/Users/Reports/Reviews/'.preg_replace('/[[:space:]]+/', '_', $_SESSION['name']).'('.preg_replace('/[[:space:]]+/', '', $today).').xls', $output);
			 file_put_contents('//192.168.0.254/Users/Copy_Reports/Reviews/'.preg_replace('/[[:space:]]+/', '_', $_SESSION['Name']).'.xls', $output);
			echo $output;
		}
	}
?>