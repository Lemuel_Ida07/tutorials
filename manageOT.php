<?php
		include('connect.php');
		$today = date("F j, Y");
?>
	<style>
	#menu_item_logo3
	{
		background-color:#f2f2f2;
	}
	#menu_item_logo3:hover
	{
		background-color:#f2f2f2;
	}
	#tab3
	{
		color:#515151;
		font-weight: bold;
		text-shadow: none;
	}
</style>
			<div id="info">
				<h3><?php echo $today; ?></h3>
				<h1> Authorize Overtime </h1>
				<form method="post">
					<input type="text" name="RequestedBy" list="requestedbylist" placeholder="Requested by" />
					<datalist id="requestedbylist">
						<?php
							$sql = "SELECT ID,Name FROM designer";
							$result = mysqli_query($conn,$sql);
							if(mysqli_num_rows($result) > 0)
							{
								while($rows = mysqli_fetch_assoc($result))
								{
									echo "<option value='".$rows['ID']."'> ".$rows['Name']."  </option>";
								}
							}
						?>
					</datalist>
					<input type="text" name="ControlNumber" placeholder="Input Control Number" required="required" />
					<input type="text" id="p_number" name="Project_Number" list="projectlist" placeholder="Select Project No." />
					<datalist id="projectlist">
						<?php
							$sql = "SELECT ID,ProjectNumber,Project_Name FROM projects";
							$result = mysqli_query($conn,$sql);
							if(mysqli_num_rows($result) > 0)
							{
								while($rows = mysqli_fetch_assoc($result))
								{
									echo "<option value='".$rows['ID']."'> ".$rows['ProjectNumber']." - ".$rows['Project_Name']."  </option>";
								}
							}
						?>
					</datalist>
					<div id="otickets">
					</div>
				</form>
			</div>
			<div id="list">
				
			</div>
			<script>
			var project_id = document.getElementById('p_number').value;
				$(document).ready(function(){
$('#p_number').on('input change',function(){
    var value = $(this).val();
        $.ajax(
        {
            url:'otickets.php',
            type:'post',
            data:'request='+value,
            success:function(data)
            {   
                $('#otickets').html(data);
            },
        });
});
});

			</script>