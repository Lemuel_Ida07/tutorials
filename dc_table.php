<?php
	include('connect.php');
		session_start();
		$today = date("F j, Y");
		echo "<style>
				button{
					border:0; 
					padding:10px; 
					background-color:#0090e9; 
					color: white; 
					border-radius: 5px; 
					cursor: pointer; 
					box-shadow: 5px 5px 0px #3278bc;
				}
				button:active
				{
					box-shadow:0px 0px 5px 1px yellow;;
					margin:5px;
				}
				</style>";
		if(isset($_POST["submittal"]))
			{
				$output ="";
				$readsql = "SELECT dc_submittal.ID, 
												project.Project_Number, 
												project.Project_Name, 
												dc_submittal.Ticket_Number, 
												dc_submittal.Deadline, 
												dc_submittal.Drawings, 
												dc_submittal.Drawing_List, 
												dc_submittal.Blueprint_Request, 
												dc_submittal.Specifications, 
												dc_submittal.Calculations, 
												dc_submittal.Reports, 
												dc_submittal.Outgoing_Email, 
												dc_submittal.Submittal_Form_Transmittal, 
												dc_submittal.Deliverables_List
											FROM `dc_submittal`
											INNER JOIN project 
												ON dc_submittal.Project_ID = project.ID";
					$result = mysqli_query($conn,$readsql);
					if(mysqli_num_rows($result) > 0)
					{
						$output.= "<table id='tbl' class='general_table' style='border-collapse: collapse;'>";
						$output.= "<thead>";
						$output.= "<tr>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Project Number</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Project Name</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Ticket Number</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Deadline</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Drawings</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Drawing List</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Blueprint Request</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Specifications</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Calculations</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Reports</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Outgoing Email</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Submittal Form Transmittal</th>";			
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Deliverables List</th>";
						$output.= "</tr>";
						$output.= "</thead>";
						$output.= "<tbody>";
						$row = 0;
						while($rows = mysqli_fetch_assoc($result))
						{
							$row+=1;
							if($row % 2 == 0){
							$output.= "<tr style='background-color: #dfdfdf;'>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'> ".$rows['Project_Number']." </td>";	
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'> ".$rows['Project_Name']." </td>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'> ".$rows['Ticket_Number']." </td>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'> ".$rows['Deadline']." </td>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'><a> ".$rows['Drawings']." </a></td>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'><a> ".$rows['Drawing_List']." </a></td>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'><a> ".$rows['Blueprint_Request']." </a></td>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'><a> ".$rows['Specifications']." </a></td>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'><a> ".$rows['Calculations']." </a></td>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'><a> ".$rows['Reports']." </a></td>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'><a> ".$rows['Outgoing_Email']." </a></td>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'><a> ".$rows['Submittal_Form_Transmittal']." </a></td>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'><a> ".$rows['Deliverables_List']." </a></td>";
							}
							else
							{	
							$output.= "<tr style='background-color: white;  border-top: none;'>";
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'> ".$rows['Project_Number']." </td>";	
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'> ".$rows['Project_Name']." </td>";
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'> ".$rows['Ticket_Number']." </td>";
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'> ".$rows['Deadline']." </td>";
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'><a> ".$rows['Drawings']." </a></td>";
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'><a> ".$rows['Drawing_List']." </a></td>";
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'><a> ".$rows['Blueprint_Request']." </a></td>";
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'><a> ".$rows['Specifications']." </a></td>";
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'><a> ".$rows['Calculations']." </a></td>";
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'><a> ".$rows['Reports']." </a></td>";
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'><a> ".$rows['Outgoing_Email']." </a></td>";
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'><a> ".$rows['Submittal_Form_Transmittal']." </a></td>";
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'><a> ".$rows['Deliverables_List']." </a></td>";
							}
							$output.="</tr>";
						}
						$output.= "</tbody>";
						$output.= "</table>";
						file_put_contents('//192.168.0.254/Users/Copy_Reports/DC/Submittal_Summary/'.preg_replace('/[[:space:]]+/', '_', $_SESSION['Name']).'.xls', $output);
						echo $output;
					}
			}
			//Email Logs table
			if(isset($_POST["email"]))
			{
				$output ="";
				$readsql = "SELECT project.Project_Number, 
														 project.Project_Name,
														 dc_email_log.ID,
														 dc_email_log.Email_Link, 
														 dc_email_log.Classification 
											FROM dc_email_log 
											INNER JOIN project ON dc_email_log.Project_ID = project.ID";
					$result = mysqli_query($conn,$readsql);
					if(mysqli_num_rows($result) > 0)
					{
						$output.= "<table id='tbl' class='general_table' style='border-collapse: collapse;'>";
						$output.= "<thead>";
						$output.= "<tr>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Project</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Email Link</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Classification</th>";
						$output.= "</tr>";
						$output.= "</thead>";
						$output.= "<tbody>";
						$row = 0;
						while($rows = mysqli_fetch_assoc($result))
						{
							$row+=1;
							if($row % 2 == 0){
							$output.= "<tr style='background-color: #dfdfdf;'>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'> ".$rows['Project_Number']." - ".$rows['Project_Name']." </td>";	
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'> ".$rows['Email_Link']." </td>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'> ".$rows['Classification']." </td>";
							}
							else
							{	
							$output.= "<tr style='background-color: white;  border-top: none;'>";
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'> ".$rows['Project_Number']." - ".$rows['Project_Name']." </td>";	
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'> ".$rows['Email_Link']." </td>";
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'> ".$rows['Classification']." </td>";
							}
							$output.="</tr>";
						}
						$output.= "</tbody>";
						$output.= "</table>";
						file_put_contents('//192.168.0.254/Users/Copy_Reports/DC/E-mail_Log/'.preg_replace('/[[:space:]]+/', '_', $_SESSION['Name']).'.xls', $output);
						echo $output;
					}
			}
			//Meeting Log table
			if(isset($_POST['meeting_log']))
			{
				$output ="";
				$readsql = "SELECT project.Project_Number, 
														 project.Project_Name,
														 dc_meeting_log.ID,
														 dc_meeting_log.Scanned_Copy_Link, 
														 dc_meeting_log.MC_No, 
														 dc_meeting_log.Commitments 
											FROM dc_meeting_log 
											INNER JOIN project ON dc_meeting_log.Project_ID = project.ID";
					$result = mysqli_query($conn,$readsql);
					if(mysqli_num_rows($result) > 0)
					{
						$output.= "<table id='tbl' class='general_table' style='border-collapse: collapse;'>";
						$output.= "<thead>";
						$output.= "<tr>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Project</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Scanned Copy Link</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Meeting Confirmation No.</th>";	
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Commitments</th>";
						$output.= "</tr>";
						$output.= "</thead>";
						$output.= "<tbody>";
						$row = 0;
						while($rows = mysqli_fetch_assoc($result))
						{
							$row+=1;
							if($row % 2 == 0){
							$output.= "<tr style='background-color: #dfdfdf;'>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'> ".$rows['Project_Number']." - ".$rows['Project_Name']." </td>";	
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'> ".$rows['Scanned_Copy_Link']." </td>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'> ".$rows['MC_No']." </td>"; 
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'> ".$rows['Commitments']." </td>";
							}
							else
							{	
							$output.= "<tr style='background-color: white;  border-top: none;'>";												 
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'> ".$rows['Project_Number']." - ".$rows['Project_Name']." </td>";	
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'> ".$rows['Scanned_Copy_Link']." </td>";
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'> ".$rows['MC_No']." </td>"; 
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'> ".$rows['Commitments']." </td>";
							}
							$output.="</tr>";
						}
						$output.= "</tbody>";
						$output.= "</table>";
						file_put_contents('//192.168.0.254/Users/Copy_Reports/DC/Meeting_Log/'.preg_replace('/[[:space:]]+/', '_', $_SESSION['Name']).'.xls', $output);
						echo $output;
					}
			}
			//Documents table
			if(isset($_POST['documents']))
			{
				$output ="";
				$readsql = "SELECT dc_documents.ID,
														dc_documents.Status, 
														dc_documents.Project_ID, 
														dc_documents.From_To, 
														dc_documents.Attached_Scanned_Submittal,
														project.Project_Number,
														project.Project_Name
											FROM dc_documents 
											INNER JOIN project ON dc_documents.Project_ID = project.ID";
				 $result = mysqli_query($conn,$readsql);
					if(mysqli_num_rows($result) > 0)
					{
						$output.= "<table id='tbl' class='general_table' style='border-collapse: collapse;'>";
						$output.= "<thead>";
						$output.= "<tr>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Status</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Project</th>";
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>From/To</th>";	
						$output.= "	<th style='background-color: #3498db; width: auto;	border: solid 2px white; padding: 10px; font-size: 18px; color: white;'>Attached Scanned Submittal</th>";
						$output.= "</tr>";
						$output.= "</thead>";
						$output.= "<tbody>";
						$row = 0;
						while($rows = mysqli_fetch_assoc($result))
						{
							$row+=1;
							if($row % 2 == 0){
							$output.= "<tr style='background-color: #dfdfdf;'>";									 
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'> ".$rows['Status']." </td>";
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'> ".$rows['Project_Number']." - ".$rows['Project_Name']." </td>";	
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'> ".$rows['From_To']." </td>"; 
							$output.= "<td style='border: solid 2px white; border-top: none; border-bottom: none;'> ".$rows['Attached_Scanned_Submittal']." </td>";
							}
							else
							{	
							$output.= "<tr style='background-color: white;  border-top: none;'>";				
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'> ".$rows['Status']." </td>";									 
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'> ".$rows['Project_Number']." - ".$rows['Project_Name']." </td>";
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'> ".$rows['From_To']." </td>"; 
							$output.= "<td style='border: solid 2px #dfdfdf; border-top: none; border-bottom: none;'> ".$rows['Attached_Scanned_Submittal']." </td>";
							}
							$output.="</tr>";
						}
						$output.= "</tbody>";
						$output.= "</table>";
						file_put_contents('//192.168.0.254/Users/Copy_Reports/DC/Documents/'.preg_replace('/[[:space:]]+/', '_', $_SESSION['Name']).'.xls', $output);
						echo $output;
					}
			}
			echo "<button type='button' onclick='gohome()'>Back</button>";
			echo "<script> function gohome(){location.replace('http://192.168.0.30/Copy_New_ActivityMonitoring/dc.php');} </script>";

?>