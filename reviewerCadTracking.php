<?php
  session_start();
  include("connect.php");
  include("phpScript.php");			
  $designer_id =  $_SESSION["ID"];
  $shortcut_name = $_SESSION["Lastname"].", ".$_SESSION["Firstname"];
  $sql = "";
  $today = date("F j, Y");
  $date = date("Y-m-d");
  $curr_time = 0;
  ?>
<style>
  #navbar tr td#cad_tracking{
    background-color: #deebff;
    color:#0747a6;
  }
  
  #navbar tr td#des_tracking{
    background-color: #0747a6;
    color:#deebff;
  }
  
  .width-100{
      width:100%;
  }

  #list{
      height:90%;
      width:80%;
      min-height:300px;
      margin-right:0px;
      resize:none;
  }

  .general_table th{
      padding:2px;
  }
</style>
<div id="list" class="width-99pc">
    <div class="miniheader">
      <label id="shortcut_name"><?php echo $shortcut_name; ?></label>
      <ul class="tbl-tab-group">
          <li class="tbl-tab active-tbl-tab" id="standby_tab"> Standby <span id="standby_count">(0)</span> </li>
          <li class="tbl-tab" id="assigned_tab"> Assigned <span id="assigned_count">(0)</span> </li>
          <li class="tbl-tab" id="requested_tab"> Requested <span id="requested_count">(0)</span> </li>
          <li class="tbl-tab" id="rejected_tab"> Rejected <span id="rejected_count">(0)</span> </li>
          <li class="tbl-tab" id="approved_tab"> Approved <span id="approved_count">(0)</span> </li>
      </ul>
      <div id="percent_holder">
        <div id="percent_bar">
          <div id="progress"></div>
        </div>
        <label id="percent_number"> 30% </label>
      </div>
    </div>
    <div class="container2">
        <table class="general_table width-100pc">
            <thead>
                <tr>
                    <th> Designer </th>
                    <th> Deadline </th>
                    <th> Project </th>
                    <th> Requirements </th>
                    <th> Budget </th>
                    <th> Ticket </th>
                    <th> Status </th>
                    <th> CAD Technician </th>
                </tr>
            </thead>
            <tbody id="weeklySchedule">
             <!-- Data will be sent from pushSchedule.php -->
            </tbody>
        </table>			
    </div>
</div>				
<div id="message">
</div>
<script>
  
     initCadTracking();
    function initCadTracking(){
      $.ajax({
        type:'get',
        url:'pushSchedule.php',
        data:'dnotif_id='+$('#dnotif_idInfo').val()+
             '&for_reviewer_cadTracking=true',
        success: function(data)
        {
          $('#weeklySchedule').html(data);
          $('#percent_number').html(computeProgress()+ "%");
        }
      });
    }
      
    function computeProgress(){
        var standby = $('#standby_count').html().replace("(","");
         standby = parseInt(standby.replace(")",""));
        var assigned = $('#assigned_count').html().replace("(","");
         assigned = parseInt(assigned.replace(")",""));
        var requested = $('#requested_count').html().replace("(","");
         requested = parseInt(requested.replace(")",""));
        var rejected = $('#rejected_count').html().replace("(","");
         rejected = parseInt(rejected.replace(")",""));
        var approved = $('#approved_count').html().replace("(","");
         approved = parseInt(approved.replace(")",""));
         
        var total = parseInt(standby + assigned + requested + rejected + approved);
        var percent = parseInt(approved * (100/total));
        $('#progress').css("width",percent+"%");
        if(percent <= 15)
        {
          $('#progress').css("background-color","#ed1c26");
        }
        else if(percent <= 75)
        {
          $('#progress').css("background-color","#ffc107");
        }
        else if(percent <= 100)
        {
          $('#progress').css("background-color","#4caf50");
        }
        if(isNaN(percent))
        {
          percent = 0;
        }
        return percent;
    }
    
    function initialize()
    {								
        hidePop();
        hideSubmitPop();
        updateReport();	
        $('#extra_activity_bg').hide();
    }

    function updateReport()
    {
        $.ajax(
        {
            url:'table.php',
            type:'post',
            data:'submit=true',
            success:function()
            {}
        });
    }

    $(document).ready(function(){

      $('#startBtn').on('click',function(){
        $.ajax(
        {
          url:'designerSubmit.php',
          type:'post',
          data:'start=true'+
                '&project_id='+$('#project_idInfo').val()+
                '&ticket_number='+$('#ticketInfo').val()+
                '&activity='+$('#activityInfo').val()+
                '&time_in='+$("#time_inInfo").val()+
                '&dnotif_id='+$('#dnotif_idInfo').val(),							
          success:function(data)
          {
            $('#actualActivity').html(data);	
            clearInfo();
          }
        });
      });

      $('#breakBtn').on('click',function(){
        $.ajax(
        {
          url:'designerSubmit.php',
          type:'post',
          data:'break=true'+
              '&project_id='+$('#project_idInfo').val()+
              '&ticket_number='+$('#ticketInfo').val()+
              '&activity='+$('#activityInfo').val()+
              '&time_in='+$("#time_inInfo").val()+
              '&dnotif_id='+$('#dnotif_idInfo').val(),	
          success:function(data){
          },
          error:function(){}
        });
      });

      $('#meetingBtn').on('click',function(){
        $.ajax(
        {
          url:'designerSubmit.php',
          type:'post',
          data:'meeting=true'+
              '&project_id='+$('#project_idInfo').val()+
              '&ticket_number='+$('#ticketInfo').val()+
              '&activity='+$('#activityInfo').val()+
              '&time_in='+$("#time_inInfo").val()+
              '&dnotif_id='+$('#dnotif_idInfo').val(),	
          success:function(data){
          },
          error:function(){}
        });
      });

      $('#extra_meeting_btn').on('click',function(){
        $.ajax(
        {
          url:'designerSubmit.php',
          type:'post',
          data:'meeting=true'+
              '&project_id='+$('#project_idInfo').val()+
              '&ticket_number='+$('#ticketInfo').val()+
              '&activity='+$('#activityInfo').val()+
              '&time_in='+$("#time_inInfo").val()+
              '&dnotif_id='+$('#dnotif_idInfo').val(),	
          success:function(data){
            alert(data);
          },
          error:function(){}
        });
      });

      $('#extra_break_btn').on('click',function(){
        $.ajax(
        {
          url:'designerSubmit.php',
          type:'post',
          data:'break=true'+
              '&project_id='+$('#project_idInfo').val()+
              '&ticket_number='+$('#ticketInfo').val()+
              '&activity='+$('#activityInfo').val()+
              '&time_in='+$("#time_inInfo").val()+
              '&dnotif_id='+$('#dnotif_idInfo').val(),	
          success:function(data){
            alert(data);
          },
          error:function(){}
        });
      });

      $('#start_review_btn').on('click',function(){
        var checklist = [];
        $('.budget_inp').each(function(){
          if($(this).prop("checked") == true)
          {
            checklist.push($(this).val());
          }
        });
        $.ajax({
          beforeSend:function(){
            return confirm("Start Review?");
          },
          url:'designerSubmit.php',
            type:'post',
            data:
              {
                start_review:true,
                reqfordes_id:checklist
              },
          success:function(){
           alert(data); 
          }
        });
      });

      $('#switchBtn,#extra_switch_btn').on('click',function(){
          showPop();
      });

      $('#popBtnNo').on('click',function(){
          hidePop();
      });

      $('#pop_okBtn').on('click',function(){
        if($('#pop_project_id').val() !== "")
        {
          $.ajax(
          {
            url:'designerSubmit.php',
            type:'post',
            data:'ok_switch=true'+
                '&project_id='+$('#pop_project_id').val()+
                '&ticket_number='+$('#pop_ticket').html()+
                '&activity='+$('#pop_activity').val()+
                '&dnotif_id='+$('#pop_dnotif_id').val()+
                '&time_in='+$("#time_inInfo").val(),
            success:function(){
              $('#extra_activity_bg').hide();
              $('#extra_activity_div').hide();
              clearSwitchPop();	
            }
          });
        }
      });

        $('#pop_cancelBtn').on('click',function(){
            clearSwitchPop();
        });						

        function clearSwitchPop()
        {
            $('#activity_edit_pop').hide();
            $('#activity_edit_pop_bg').hide();	
            $('#pop_project_id').val("");
            $('#pop_project').val("");
            $('#pop_dnotif_id').val("");
            $('#pop_date').html("");
            $('#pop_ticket').html("");
            $('#pop_activity').val("");
        }

        $('#subPop_startReview').on('click',function(){
            $.ajax({
                url:'designerSubmit.php',
                type:'post',
                data:'submitDesign=true'+
                    '&activitylogs_id='+$('#activitylogs_idInfo').val(),
                success:function(){}
            });

            $.ajax({
                url:'getTotalHours.php',
                type:'post',
                data:'get=true',
                success:function(data)
                {
                    $('#total_time').html(data);
                    hideSubmitPop();
                }
            });
        });

        $('#subPop_switch').on('click',function(){
                document.getElementById('switchBtn').click();
                $('#design_submit_pop').hide();
        });

        $('#save_edit').on('click',function(){
            $.ajax({
                url:'designerSubmit.php',
                type:'post',
                data:'edit=true'+
                    '&activity='+$('#pop_activity').val()+	
                    '&time_in='+$('#pop_time_in').val()+	
                    '&time_out='+$('#pop_time_out').val()+
                    '&activitylog_id='+$('#pop_activitylog_id').val(),
                success:function(data){
                        $('#message').html(data);
                }
            });
        });
    });

    function clearInfo()
    {																									 
        $("#dnotif_idInfo").val("");	 
        $("#project_idInfo").val("");
    }	

    function hidePop()
    {											 
        $(".popUpBg").hide();
        $(".popUp").hide();
    }

    function showPop()
    {											 
        $(".popUpBg").show();
        $(".popUp").show();
    }		

    function hideSubmitPop()
    {											 
        $(".popUpBg").hide();
        $(".popUpSubmit").hide();
    }

    function showSubmitPop()
    {											 
        $(".popUpBg").show();
        $(".popUpSubmit").show();
    }		

    $(".searchbox").click(function (e) {
        $(".datalist").show();
        e.stopPropagation();
    });

    $(".datalist").click(function (e) {
        e.stopPropagation();
    });

    $(document).click(function () {
        $(".datalist").hide();
    });

    $('#pop_project').on('click',function(){
        $.ajax(
        {
            url:'designerSubmit.php',
            type:'post',
            data:'getAvailableProj=true',
            success:function(data)
            {
                $('#pop_schedule').html(data);	
            }
        });
    });
    
    $('#standby_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#assigned_tab').prop("class","tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
      $('#approved_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $.ajax({
        url:'reviewerGetData.php',
        type:'post',
        data:'get_cad_tracking=true'+
             '&status=Approved',
        success:function(data){
          $('#weeklySchedule').html(data);
        }
      });
    });
    
    $('#assigned_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#standby_tab').prop("class","tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
      $('#approved_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $.ajax({
        url:'reviewerGetData.php',
        type:'post',
        data:'get_cad_tracking=true'+
             '&status=Assigned',
        success:function(data){
          $('#weeklySchedule').html(data);
        }
      });
    });
    
    $('#requested_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#assigned_tab').prop("class","tbl-tab");
      $('#standby_tab').prop("class","tbl-tab");
      $('#approved_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $.ajax({
        url:'reviewerGetData.php',
        type:'post',
        data:'get_cad_tracking=true'+
             '&status=CADRequested',
        success:function(data){
          $('#weeklySchedule').html(data);
        }
      });
    });
    
    $('#approved_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#assigned_tab').prop("class","tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
      $('#standby_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $.ajax({
        url:'reviewerGetData.php',
        type:'post',
        data:'get_cad_tracking=true'+
             '&status=CADAccepted',
        success:function(data){
          $('#weeklySchedule').html(data);
        }
      });
    });
    
    $('#rejected_tab').on('click',function(){
       $(this).prop("class","tbl-tab active-tbl-tab");
     $('#assigned_tab').prop("class","tbl-tab");
     $('#requested_tab').prop("class","tbl-tab");
     $('#standby_tab').prop("class","tbl-tab");
     $('#approved_tab').prop("class","tbl-tab");
       $.ajax({
         url:'reviewerGetData.php',
         type:'post',
         data:'get_cad_tracking=true'+
              '&status=CADRejected',
         success:function(data){
           $('#weeklySchedule').html(data);
         }
       });
     });
    
    
    
    
    
    
    
    
    
    
    
</script>