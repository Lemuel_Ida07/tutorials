<?php
  session_start();
  include("connect.php");
  include("phpGlobal.php");
  $today = date("F j, Y");
  $shortcut_name = $_SESSION["Lastname"].", ".$_SESSION["Firstname"];
  $user_id = $_SESSION["ID"];
?>

<!DOCTYPE html>
<html>
  <head>
    <title> Activity Monitoring </title>
    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/modals.css" />
    <link rel="stylesheet" type="text/css" href="css/print.min.css" />
    <link rel="stylesheet" type="text/css" href="css/box.css" />
    <link rel="stylesheet" type="text/css" href="css/newStyle.css" />
    <link rel="icon" href="img/logoblue.png">
    
    <script src="jquery-3.2.1.min.js"></script>
    <script src="scripts/print.min.js"></script>
    
    <style>  
      #list{
        height:90%;
        width:80%;
        min-height:300px;
        margin-right:0px;
        resize:none;
      }
      
      .miniheader{
      background-color:#eef1f2;
      margin:0px;
      position:fixed;
      left:0px;
      top:60px;
      width:100%;
      height:100px;
      border-bottom:solid 2px #ececec;
      }
      
      #th_endorced_client,#th_submission,#th_invoice,#th_reason{
          display:none;
      }
      
      
      #reject_pop{
          display:none;
          position:fixed;
          width:20%;
          margin:15% 40%;
          background-color:white;
          border-radius:4px;
          padding:4px;
          box-shadow:0px 0px 4px 0px;
      }
    </style>
  </head>
  <body>
    <?php include('header.php'); ?>
    <div id="mainform">
      <div id="list" class="width-99pc">
        <div class="miniheader">
          <label id="shortcut_name"><?php echo $shortcut_name; ?></label>
          <ul class="tbl-tab-group">
            <li class="tbl-tab active-tbl-tab" id="requested_tab"> Requested <span id="requested_count">(0)</span> </li>
            <li class="tbl-tab" id="processing_tab"> Processing <span id="processing_count">(0)</span> </li>
            <li class="tbl-tab" id="rejected_tab"> Rejected <span id="rejected_count">(0)</span> </li>
            <li class="tbl-tab" id="paid_tab"> Paid <span id="paid_count">(0)</span> </li>
          </ul>
          <div id="percent_holder">
            <label id="total_amount"> 0 </label>
          </div>
        </div>
        <div class="container2">
          <table class="general_table width-100pc">
            <thead>
              <tr>
                <th> Project </th>
                <th> % </th>
                <th> Amount </th>
                <th> Team </th>
                <th id="th_endorced_client"> Date Endorced to Client </th>
                <th id="th_submission"> Submission Form No.  </th>
                <th id="th_acc"> Date Endorced to Accounting </th>
                <th id="th_or"> OR No. </th>
                <th id="th_invoice"> Invoice No. </th>
                <th id="th_amountpaid"> Amount Paid </th>
                <th id="th_paymenttype"> Payment Type </th>
                <th id="th_action"> Action </th>
                <th id="th_reason"> Reason </th>
              </tr>
            </thead>
            <tbody id="weeklySchedule">
             <!-- Data will be sent from pdRead.php -->
             <?php
              $sql = "SELECT project.ID,
										project.Project_Name, 
										project.Team_ID, 
										project.Project_Number,
                    project.Amount,
                    external_deadline.Percent,
										external_deadline.Phase,	
										external_deadline.ID AS exid ,
                    external_deadline.External_Deadline,
                    external_deadline.Status,
                    team.Team_Name
									FROM project 
									INNER JOIN external_deadline 
										ON project.ID = external_deadline.Project_ID 
                  INNER JOIN team
                    ON project.Team_ID = team.ID
									WHERE external_deadline.Status = 'Done'";
              $result = mysqli_query($conn,$sql);
              if(mysqli_num_rows($result) > 0)
              {
                while($rows = mysqli_fetch_assoc($result)){
                  echo "<tr>
                          <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                          <td> ".$rows["Percent"]." </td>
                          <td> &#8369; ".$rows["Amount"]." </td>
                          <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>
                          <td>
                            <input type='button' class='btn-normal white bg-green' value='Create' id='create".$rows["ID"]."'>
                            <script>
                              $('#create".$rows["ID"]."').on('click',function(){
                                $('#billing_externaldl_id').val('".$rows["exid"]."');
                                showBilling();
                              });
                            </script>
                          </td>";
                  echo"</tr>";
                }
              }
             ?>
            </tbody>
          </table>			
        </div>
      </div>
        <div id="pop_bnc_bg">
</div>

<div id="pop_bnc">
    <input type="text" id="bnc_id" style="display:none;" />
    <label class="title"> OR No. </label>
    <input type="text" id="bnc_or_no" placeholder="OR Number" />
    <label class="title"> Amount Paid </label>
    <input type="text" id="bnc_amount_paid" placeholder="Amount Paid" />
    <label class="title"> Payment Type </label>
    <select id="bnc_payment_type" class="dropdown">
        <option id="Check"> Check </option>
        <option id="Cash"> Cash </option>
        <option id="Online"> Online  </option>
    </select> 
    <label class="title"> Date Endorced to Accounting. </label>
    <input type="date" class="classy-date" id="bnc_endorced_date" value="<?php echo $today_datepicker;?>"/>
    <br /><br />
    <input type="button" class="btn-normal white bg-green width-45pc" value="OK" id="bnc_okbtn"/>
    <input type="button" class="btn-normal white bg-red width-45pc" value="Cancel" id="bnc_cancelbtn" />
</div>
        <div id="pop_billing_bg"></div>
   <div id="pop_billing">
       <input type="text" id="billing_externaldl_id" style="display:none;"/>
       <label class="title"> Billing Invoice No. </label>
       <input type="text" id="billing_invoice_no" placeholder="Billing Invoice No." />
       <label class="title"> Date Endorced to Client. </label>
       <input type="date" class="classy-date" id="billing_endorced_date" value="<?php echo $today_datepicker;?>"/>
       <br /><br />
       <input type="button" class="btn-normal white bg-green width-45pc" value="OK" id="billing_okbtn"/>
       <input type="button" class="btn-normal white bg-red width-45pc" value="Cancel" id="billing_cancelbtn" />
   </div>
    
    <div id="reject_pop">
      <textarea id="reject_reason" placeholder="Reason for reject"></textarea>
      <input type="button" class="btn-normal white bg-green width-45pc" value="OK" id="reject_okbtn"/>
      <input type="button" class="btn-normal white bg-red width-45pc" value="Cancel" id="reject_cancelbtn" />
    </div>
    </div>
    <script>
    countBnc();
    
    //pending
    $('#requested_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#paid_tab').prop("class","tbl-tab");
      $('#processing_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $.ajax({
        url:'bncRead.php',
        type:'post',
        data:'requested_bnc=true',
        success:function(data){
          $('#th_acc').fadeOut();
          $('#th_or').fadeOut();
          $('#th_amountpaid').fadeOut();
          $('#th_paymenttype').fadeOut();
          $('#th_action').fadeOut();
          $('#th_endorced_client').fadeOut();
          $('#th_invoice').fadeOut();
          $('#th_reason').fadeOut();
          $('#weeklySchedule').html(data);
          $('#total_amount').html("&#8369;"+countAmount(2));
          countBnc();
        }
      });
    });
    
    //parocessing
    $('#processing_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
      $('#paid_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $.ajax({
        url:'bncRead.php',
        type:'post',
        data:'pending_bnc=true',
        success:function(data){
          $('#th_acc').fadeOut();
          $('#th_or').fadeOut();
          $('#th_invoice').fadeIn();
          $('#th_endorced_client').fadeIn();
          $('#th_amountpaid').fadeOut();
          $('#th_paymenttype').fadeOut();
          $('#th_action').fadeOut();
          $('#th_reason').fadeOut();
          $('#weeklySchedule').html(data);
          $('#total_amount').html("&#8369;"+countAmount(2));
          countBnc();
        }
      });
    });
    
    //rejected
    $('#rejected_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
      $('#processing_tab').prop("class","tbl-tab");
      $('#paid_tab').prop("class","tbl-tab");
      $.ajax({
        url:'bncRead.php',
        type:'post',
        data:'rejected_bnc=true',
        success:function(data){
          $('#th_acc').fadeOut();
          $('#th_or').fadeOut();
          $('#th_invoice').fadeIn();
          $('#th_endorced_client').fadeIn();
          $('#th_amountpaid').fadeOut();
          $('#th_paymenttype').fadeOut();
          $('#th_action').fadeOut();
          $('#th_reason').fadeIn();
          $('#weeklySchedule').html(data);
          $('#total_amount').html("&#8369;"+countAmount(2));
          countBnc();
        }
      });
    });
    
    //paid
    $('#paid_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
      $('#processing_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $.ajax({
        url:'bncRead.php',
        type:'post',
        data:'paid_bnc=true',
        success:function(data){
          $('#th_acc').fadeIn();
          $('#th_or').fadeIn();
          $('#th_amountpaid').fadeIn();
          $('#th_paymenttype').fadeIn();
          $('#th_action').fadeOut();
          $('#th_endorced_client').fadeOut();
          $('#th_invoice').fadeOut();
          $('#th_reason').fadeOut();
          $('#weeklySchedule').html(data);
          $('#total_amount').html("&#8369;"+countAmount(6));
          countBnc();
        }
      });
    });
    
    //pop cancel
    $('#billing_cancelbtn').on('click',function(){
      hideBilling();
    });
    
    //pop ok
    $('#billing_okbtn').on('click',function(){
      $.ajax({
        beforeSend:function(){
          return confirm('Are you sure you want to save?');
        },
        url:'pdSubmit.php',
        type:'post',
        data:'insert_bnc=true'+
          '&externaldl_id='+$('#billing_externaldl_id').val()+
          '&invoice_number='+$('#billing_invoice_no').val()+
          '&date_endorced_clnt='+$('#billing_endorced_date').val(),
        success:function(){
          refresh();
        },
        error:function(data){
          alert(data);
        }
      });
    });
    
    function showBncPop(){
      $('#pop_bnc_bg').fadeIn();
      $('#pop_bnc').slideDown();
    }
    
    function hideBncPop(){
      $('#pop_bnc_bg').fadeOut();
      $('#pop_bnc').slideUp();
    }

    $('#bnc_okbtn').on('click',function(){
    $.ajax({
      beforeSend:function(){
        return confirm('Are you sure you want to save?');
      },
      url:'bncSubmit.php',
      type:'post',
      data:'paid_bnc=true'+
            '&or_number='+$('#bnc_or_no').val()+
            '&endorced_to_accounting='+$('#bnc_endorced_date').val()+
            '&amount_paid='+$('#bnc_amount_paid').val()+
            '&payment_type='+$('#bnc_payment_type').val()+
            '&bnc_id='+$('#bnc_id').val(),
      success:function(){
        hideBncPop();
        clearBncpop();
        $('#processing_tab').trigger('click');
      },
      error:function(data){
        alert(data);
      }
    });
    });
      
    $('#bnc_cancelbtn').on('click',function(){
      hideBncPop();
    });
      
    $('#reject_cancelbtn').on('click',function(){
      $('#reject_pop').slideUp();
    });
    
    $('#reject_okbtn').on('click',function(){
      $.ajax({
        beforeSend:function(){
          return confirm('Are you sure?');
        },
        url:'bncSubmit.php',
        type:'post',
        data:'reject_bnc'+
              '&bnc_id='+$('#bnc_id').val()+
              '&or_number='+$('#bnc_or_no').val()+
              '&endorced_to_accounting='+$('#bnc_endorced_date').val()+
              '&amount_paid='+$('#bnc_amount_paid').val()+
              '&payment_type='+$('#bnc_payment_type').val()+
              '&reason='+$('#reject_reason').val(),
        success:function(){
          hideBncPop();
          clearBncpop();
          $('#standby_tab').trigger('click');
        }
      });
    });
    
    
    function showBncPop(){
      $('#pop_bnc_bg').fadeIn();
      $('#pop_bnc').slideDown();
    }
    
    function hideBncPop(){
      $('#pop_bnc_bg').fadeOut();
      $('#pop_bnc').slideUp();
    }


    function clearBncpop(){
      $('#bnc_or_no').val('');
      $('#bnc_endorced_date').val('');
    }
    function countBnc(){

      $.ajax({
        url:'bncRead.php',
        type:'post',
        data:'requested_count=true',
        success:function(data){
          $('#requested_count').html('('+data+')');
        }
      });
      
      $.ajax({
        url:'bncRead.php',
        type:'post',
        data:'pending_count=true',
        success:function(data){
          $('#processing_count').html('('+data+')');
        }
      });

      $.ajax({
        url:'bncRead.php',
        type:'post',
        data:'reject_count=true',
        success:function(data){
          $('#rejected_count').html('('+data+')');
        }
      });
      
      $.ajax({
        url:'bncRead.php',
        type:'post',
        data:'paid_count=true',
        success:function(data){
          $('#paid_count').html('('+data+')');
        }
      });
      
    }
    
    function showBilling(){
      $('#pop_billing_bg').fadeIn();
      $('#pop_billing').slideDown();
    }
    
    function hideBilling(){
      $('#pop_billing_bg').fadeOut();
      $('#pop_billing').slideUp();
    }
    
    function refresh(){
      location.reload();
    }
    
    function countAmount(cell){
    var count = 0;
    var total = 0;
    $("#weeklySchedule tr").each(function(){
      var amount =document.getElementById("weeklySchedule").rows[count].cells[cell].innerHTML;
        
        amount = amount.trim();
        amount = amount.replace(/,/g,"");
        amount = parseFloat(amount.substring(1,amount.length));
        total = Number(total + amount);
        count++;
    });
    total = total.toLocaleString('en');
    return total;
  }
    </script>
  </body>
</html>