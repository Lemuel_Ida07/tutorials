<?php
include("connect.php");
include("phpGlobal.php");
/* Van Hudson Galvoso
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//count
if(isset($_POST["collected_count"]))
{
  $readsql = "SELECT
                          COUNT(bnc.ID) AS COLLECTED_COUNT 
                          FROM bnc
                          INNER JOIN external_deadline
                            ON bnc.Externaldl_ID = external_deadline.ID
                          INNER JOIN project 
                            ON external_deadline.Project_ID = project.ID
                          INNER JOIN team 
                            ON team.ID = project.Team_ID
                          WHERE bnc.Status = 'Paid'
                            AND bnc.Bank = ''";
              $result = mysqli_query($conn,$readsql);
              if(mysqli_num_rows($result) > 0)
              {
                $row = mysqli_fetch_assoc($result);
                echo $row["COLLECTED_COUNT"];
              }
}

if(isset($_POST["deposit_count"]))
{
  $bank = $_POST["bank"];
  $readsql = "SELECT
                          COUNT(bnc.ID) AS Deposit_Count
                          FROM bnc
                          INNER JOIN external_deadline
                            ON bnc.Externaldl_ID = external_deadline.ID
                          INNER JOIN project 
                            ON external_deadline.Project_ID = project.ID
                          INNER JOIN team 
                            ON team.ID = project.Team_ID
                          WHERE bnc.Status = 'Paid'
                            AND bnc.Bank = '$bank'";
              $result = mysqli_query($conn,$readsql);
              if(mysqli_num_rows($result) > 0)
              {
                $row = mysqli_fetch_assoc($result);
                echo $row["Deposit_Count"];
              }
}


//list
if(isset($_POST["get_collected"]))
{
  $readsql = "SELECT
                bnc.ID,
                project.Project_Number,
                project.Project_Name,
                external_deadline.External_Deadline,
                external_deadline.Percent,
                external_deadline.Phase,
                project.Amount,
                project.Team_ID,
                bnc.Endorced_To_Client,
                bnc.Endorced_To_Accounting,
                bnc.Invoice_Number,
                bnc.OR_Number,
                bnc.Amount_Paid,
                bnc.Payment_Type,
                team.Team_Name
              FROM bnc
              INNER JOIN external_deadline
                ON bnc.Externaldl_ID = external_deadline.ID
              INNER JOIN project 
                ON external_deadline.Project_ID = project.ID
              INNER JOIN team 
                ON team.ID = project.Team_ID
              WHERE bnc.Status = 'Paid'
                AND bnc.Bank = ''";
  $result = mysqli_query($conn,$readsql);
  if(mysqli_num_rows($result) > 0)
  {
    while($rows = mysqli_fetch_assoc($result)){
      echo "<tr>
              <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
              <td> ".$rows["Percent"]." </td>
              <td> &#8369; ".$rows["Amount_Paid"]." </td>
              <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>
              <td> ".$rows["Endorced_To_Accounting"]." </td>
              <td> ".$rows["OR_Number"]." </td>
              <td> ".$rows["Payment_Type"]." </td>
              <td>
                <select id='bank".$rows["ID"]."'>
                  <option value='EASTWEST'> EASTWEST </option>
                  <option value='BDO'> BDO </option>
                  <option value='LANDBANK'> LANDBANK </option>
                </select>
                <input type='button' class='btn-normal white bg-blue' value='Deposit' id='deposit".$rows["ID"]."' />
                <script>
                  $('#deposit".$rows["ID"]."').on('click',function(){
                      $.ajax({
                        beforeSend:function(){
                          return confirm('Deposit to ' + $('#bank".$rows["ID"]."').val() + ' ?');
                        },
                        url:'accountingSubmit.php',
                        type:'post',
                        data:'deposit=true'+
                             '&bank='+ $('#bank".$rows["ID"]."').val()+
                             '&bnc_id=".$rows["ID"]."',
                        success:function(data){
                          refresh();
                        }
                      });
                  });
                </script>
              </td>
            </tr>";
    }
  }
}

if(isset($_POST["by_bank"]))
{
  $bank = $_POST["bank"];
  $readsql = "SELECT
                            bnc.ID,
                            project.Project_Number,
                            project.Project_Name,
                            external_deadline.External_Deadline,
                            external_deadline.Percent,
                            external_deadline.Phase,
                            project.Amount,
                            project.Team_ID,
                            bnc.Endorced_To_Client,
                            bnc.Endorced_To_Accounting,
                            bnc.Invoice_Number,
                            bnc.OR_Number,
                            bnc.Amount_Paid,
                            bnc.Payment_Type,
                            bnc.Deposited,
                            team.Team_Name
                          FROM bnc
                          INNER JOIN external_deadline
                            ON bnc.Externaldl_ID = external_deadline.ID
                          INNER JOIN project 
                            ON external_deadline.Project_ID = project.ID
                          INNER JOIN team 
                            ON team.ID = project.Team_ID
                          WHERE bnc.Status = 'Paid'
                            AND bnc.Bank = '$bank'";
              $result = mysqli_query($conn,$readsql);
              if(mysqli_num_rows($result) > 0)
              {
                while($rows = mysqli_fetch_assoc($result)){
                  echo "<tr>
                          <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                          <td> ".$rows["Percent"]." </td>
                          <td> &#8369; ".$rows["Amount_Paid"]." </td>
                          <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>
                          <td> ".$rows["Endorced_To_Accounting"]." </td>
                          <td> ".$rows["Deposited"]." </td>
                          <td> ".$rows["OR_Number"]." </td>
                          <td> ".$rows["Payment_Type"]." </td>
                        </tr>";
                }
              }
}
