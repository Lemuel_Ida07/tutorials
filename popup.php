<?php
	session_start();

	include("connect.php");
	
	$today = date("F j, Y");
?>

<html>
	<head>
		<title> Activity Monitoring </title>
		<link rel="stylesheet" type="text/css" href = "css/style.css"/>
		<link rel="stylesheet" type="text/css" href = "css/sidebar.css"/>
		<link rel="stylesheet" type="text/css" href = "css/calendar.css"/>
	</head>
	<body>
		<div onclick="hideElement('logout','edit_password','activity_summary')" id="mainform">
			<div id="pop_up">
				<h1> <?php echo $today; ?> </h1>
				
			</div>
		</div>
	</body>
</html>
