<?php 
    session_start();
    include('connect.php');

	$today = date("F j, Y");
?>
<style>
	#menu_item_logo4
	{
		background-color:#f2f2f2;
	}
	#menu_item_logo4:hover
	{
		background-color:#f2f2f2;
	}
	#tab4
	{
		color:#515151;
		font-weight: bold;
		text-shadow: none;
	}

    #topic{
        border:none;
    }
</style>
<div id="info" style="width:100%;margin-left:0;padding-left:0;max-width:100%;">
				<h2> Minutes of Meeting Log </h2>
				<h2><?php echo $_SESSION['Team_Name']; ?></h2>
				<h3 style="padding-bottom:0;margin-bottom:0;"><?php echo $today; ?></h3>
					<div id="topic" >
					<h2 style="visibility:hidden;"> dsdasds </h2>
					<label> Project </label>
					<select style="padding:3%;"name="ProjectNumber" id="ProjectNumber" required="required">
                    <option> Select Project Number </option>
							<?php
								 $designer_id = $_SESSION["ID"];
								 $team_id = $_SESSION["Team_ID"];
								 $sql = "SELECT ID,Project_Number ,Project_Name FROM project WHERE Team_ID = '".$team_id."'";
								 $result = mysqli_query($conn,$sql);
								 if(mysqli_num_rows($result) > 0)
								 {
									 while($rows = mysqli_fetch_assoc($result))
									 {
										 echo "<option value='".$rows["ID"]."'> ".$rows["Project_Number"]." - ".$rows["Project_Name"]."</option>";
									 }
								 }
							?>
					</select>
					</div>
					<div id="topic">
                         <h2 style="visibility:hidden;">ds </h2>   
												 <label> Link of Scanned Copy </label>
					    <input type="text" id="scanned_copy_link" placeholder="Paste Link of Scanned Copy Here" />
					</div>
					<div id="topic">
						<h2 style="visibility:hidden;">df</h2>
							<label> Meeting Confirmation Number </label>
					    <input type="text" id="mc_no" placeholder="Meeting Confirmation No." />
					</div>
					<div id="topic">
						<h2 style="visibility:hidden;">df</h2>
						<label> Commitments </label>
					    <input type="text" id="commitments" placeholder="Commitments" />
					</div>      
					    <button style="display:inline-block; width:15%; margin-left:0;" id="submit1" class="submit_meeting_log">Submit</button>
							<button style="display:inline-block; width:15%; margin:0;" id="submit1" class="generate_report">Generate Report</button>
			<div style="width:99%;height:30%;display:block;max-width:10000px;" id="list">
				<?php
					$readsql = "SELECT project.Project_Number, 
														 project.Project_Name,
														 dc_meeting_log.ID,
														 dc_meeting_log.Scanned_Copy_Link, 
														 dc_meeting_log.MC_No, 
														 dc_meeting_log.Commitments 
											FROM dc_meeting_log 
											INNER JOIN project ON dc_meeting_log.Project_ID = project.ID";
					$result = mysqli_query($conn,$readsql);
					if(mysqli_num_rows($result) > 0)
					{
						echo "<table class='general_table width-100pc'>";
							echo "<thead>";
							echo "<tr>";
							echo "	<th>Project</th>";
							echo "	<th>Scanned Copy Link</th>";			 
							echo "	<th>Meeting Confirmation No.</th>";
							echo "	<th>Commitments</th>";
							echo "  <th>Actions</th>";
							echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
						while($rows = mysqli_fetch_assoc($result))
						{
							echo "<tr>";											
							echo "	<td> ".$rows['Project_Number']." - ".$rows['Project_Name']."</td>";							
							echo "	<td><a> ".$rows['Scanned_Copy_Link']." </a></td>";		 
							echo "	<td> ".$rows['MC_No']." </td>";
							echo "	<td> ".$rows['Commitments']." </td>";
							echo "<td><input id='deletebtn' class='".$rows['ID']."' type='submit' value='Delete' name='delete' /></td>";
							echo "<script>
											$(document).ready(function(){
												$('.".$rows['ID']."').on('click',function(){
													var value = $(this).val();
													$.ajax(
														{																					 
															url:'dc_submit.php',
															type:'post',
															data:'delete_meeting_log='+value+
																	 '&meeting_log_id=".$rows['ID']."',
															success:function(data)
																{
																	$('#list').html(data);
																},
														});
												});
											});
										</script>";
							echo "</tr>";
						}				
							echo "</tbody>";
							echo "</table>";
					}
				?>
			</div>						 
			<script>
				$(document).ready(function(){
					$('.submit_meeting_log').on('click',function(){
						var value = $(this).val();
						$.ajax(
							{
								url:'dc_submit.php',
								type:'post',
								data:'meeting_log='+value+
										 '&project_id=' + document.getElementById('ProjectNumber').value +	
										 '&scanned_copy_link=' + document.getElementById('scanned_copy_link').value +	
										 '&commitments=' + document.getElementById('commitments').value +
										 '&mc_no=' + document.getElementById('mc_no').value,
								success:function(data)
								{
									$('#list').html(data);
								},
							});
					});
				});
				
				$(document).ready(function(){
					$('.generate_report').on('click',function(){
						var value = $(this).val();
						$.ajax(
							{
								url:'dc_table.php',
								type:'post',
								data:'meeting_log='+value,
								success:function(data)
								{
									$('#mainform').html(data);
								},
							});
					});
				});
			</script>