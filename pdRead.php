<?php
  if(session_status() == PHP_SESSION_NONE)
  {
    session_start();
  }
  include("connect.php");
  include("phpGlobal.php");
  $user_id =  $_SESSION["ID"];

  //for bnc
  //for counts
  if(isset($_POST["requested_count"]))
  {
    $sql = "SELECT 
              COUNT(external_deadline.ID) AS requested_count
            FROM project 
            INNER JOIN external_deadline 
              ON project.ID = external_deadline.Project_ID 
            INNER JOIN team
              ON project.Team_ID = team.ID
            WHERE (";  
            if(isset($_SESSION["team_list_id"]))
            {
              $team_list_id=$_SESSION["team_list_id"];
              $teamlist_count = count($_SESSION["team_list_id"]);
              for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
              {
                $sql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
              }
                $sql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
            }
            else
            {
              $sql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
            }
        $sql.=") AND external_deadline.Status = 'Done'";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result) > 0)
        {
          $row = mysqli_fetch_assoc($result);
          echo $row["requested_count"];
        }
  }
  
  if(isset($_POST["pending_count"])){
    $readsql = "SELECT
                  COUNT(bnc.ID) AS Pending_Count
                FROM bnc
                INNER JOIN external_deadline
                  ON bnc.Externaldl_ID = external_deadline.ID
                INNER JOIN project 
                  ON external_deadline.Project_ID = project.ID
                WHERE (";  
            if(isset($_SESSION["team_list_id"]))
            {
              $team_list_id=$_SESSION["team_list_id"];
              $teamlist_count = count($_SESSION["team_list_id"]);
              for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
              {
                $readsql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
              }
                $readsql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
            }
            else
            {
              $readsql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
            }
        $readsql.=") AND bnc.Status = 'Pending'";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      $row = mysqli_fetch_assoc($result);
      echo $row["Pending_Count"];
    }
  }
  
  if(isset($_POST["reject_count"])){
    $readsql = "SELECT
                  COUNT(bnc.ID) AS Rejected_Count
                FROM bnc
                INNER JOIN external_deadline
                  ON bnc.Externaldl_ID = external_deadline.ID
                INNER JOIN project 
                  ON external_deadline.Project_ID = project.ID
                INNER JOIN team
                  ON project.Team_ID = team.ID
                WHERE (";  
            if(isset($_SESSION["team_list_id"]))
            {
              $team_list_id=$_SESSION["team_list_id"];
              $teamlist_count = count($_SESSION["team_list_id"]);
              for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
              {
                $readsql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
              }
                $readsql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
            }
            else
            {
              $readsql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
            }
        $readsql.=") and bnc.Status = 'Rejected'";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      $row = mysqli_fetch_assoc($result);
      echo $row["Rejected_Count"];
    }
  }
  
  if(isset($_POST["paid_count"])){
    $readsql = "SELECT
                  COUNT(bnc.ID) AS Paid_Count
                FROM bnc
                INNER JOIN external_deadline
                  ON bnc.Externaldl_ID = external_deadline.ID
                INNER JOIN project 
                  ON external_deadline.Project_ID = project.ID
                WHERE (";  
            if(isset($_SESSION["team_list_id"]))
            {
              $team_list_id=$_SESSION["team_list_id"];
              $teamlist_count = count($_SESSION["team_list_id"]);
              for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
              {
                $readsql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
              }
                $readsql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
            }
            else
            {
              $readsql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
            }
        $readsql.=") AND bnc.Status = 'Paid'";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      $row = mysqli_fetch_assoc($result);
      echo $row["Paid_Count"];
    }
  }

  //for list
  if(isset($_POST["pending_bnc"]))
  {
    $sql = "SELECT
                  bnc.ID,
                  project.Project_Number,
                  project.Project_Name,
                  external_deadline.External_Deadline,
                  external_deadline.Percent,
                  external_deadline.Phase,
                  project.Amount,
                  project.Team_ID,
                  bnc.Endorced_To_Client,
                  bnc.Invoice_Number,
                  team.Team_Name
                FROM bnc
                INNER JOIN external_deadline
                  ON bnc.Externaldl_ID = external_deadline.ID
                INNER JOIN project 
                  ON external_deadline.Project_ID = project.ID
                INNER JOIN team
                  ON project.Team_ID = team.ID
                WHERE (";  
            if(isset($_SESSION["team_list_id"]))
            {
              $team_list_id=$_SESSION["team_list_id"];
              $teamlist_count = count($_SESSION["team_list_id"]);
              for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
              {
                $sql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
              }
                $sql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
            }
            else
            {
              $sql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
            }
        $sql.=") AND bnc.Status = 'Pending'";
    $result = mysqli_query($conn,$sql);
    if(mysqli_num_rows($result) > 0)
    {
      while($rows = mysqli_fetch_assoc($result)){
        echo "<tr>
                <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                <td> ".$rows["Percent"]." </td>
                <td> &#8369; ".$rows["Amount"]." </td>
                <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>
                <td> ".$rows["Endorced_To_Client"]." </td>
                <td> ".$rows["Invoice_Number"]." </td>
              </tr>";
      }
    }
  }
    //reject
  if(isset($_POST["rejected_bnc"]))
  {
    $readsql = "SELECT
                  bnc.ID,
                  project.Project_Number,
                  project.Project_Name,
                  external_deadline.External_Deadline,
                  external_deadline.Percent,
                  external_deadline.Phase,
                  external_deadline.ID AS exid,
                  project.Amount,
                  project.Team_ID,
                  bnc.Endorced_To_Client,
                  bnc.Invoice_Number,
                  bnc.Reason,
                  team.Team_Name
                FROM bnc
                INNER JOIN external_deadline
                  ON bnc.Externaldl_ID = external_deadline.ID
                INNER JOIN project 
                  ON external_deadline.Project_ID = project.ID
                INNER JOIN team
                  ON project.Team_ID = team.ID
                WHERE (";  
            if(isset($_SESSION["team_list_id"]))
            {
              $team_list_id=$_SESSION["team_list_id"];
              $teamlist_count = count($_SESSION["team_list_id"]);
              for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
              {
                $readsql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
              }
                $readsql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
            }
            else
            {
              $readsql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
            }
        $readsql.=") and bnc.Status = 'Rejected'";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      while($rows = mysqli_fetch_assoc($result)){
        echo "<tr>
                <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                <td> ".$rows["Percent"]." </td>
                <td> &#8369; ".$rows["Amount"]." </td>
                <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>
                <td> ".$rows["Endorced_To_Client"]." </td>
                <td> ".$rows["Invoice_Number"]." </td>
                <td> ".$rows["Reason"]." </td>
                <td> <input type='button' class='btn-normal white bg-blue' id='request".$rows["ID"]."' value='Request'>
                  <script>
                    $('#request".$rows["ID"]."').on('click',function(){
                      $.ajax({
                        beforeSend:function(){
                          return confirm('Are you sure?');
                        },
                        url:'pdSubmit.php',
                        type:'post',
                        data:'request_bnc=true'+
                            '&bnc_id=".$rows["ID"]."'+
                            '&external_id=".$rows["exid"]."',
                        success:function(data){
                        }
                      });
                    });
                  </script>
                </td>
                  
              </tr>";
      }
    }
  }
  
  if(isset($_POST["get_paid_bnc"]))
  {
    $readsql = "SELECT
                  bnc.ID,
                  project.Project_Number,
                  project.Project_Name,
                  external_deadline.Percent,
                  external_deadline.Phase,
                  project.Amount,
                  project.Team_ID,
                  bnc.Endorced_To_Client,
                  bnc.Invoice_Number,
                  bnc.OR_Number,
                  bnc.Endorced_To_Accounting,
                  bnc.Amount_Paid,
                  bnc.Payment_Type,
                  team.Team_Name
                FROM bnc
                INNER JOIN external_deadline
                  ON bnc.Externaldl_ID = external_deadline.ID
                INNER JOIN project 
                  ON external_deadline.Project_ID = project.ID
                INNER JOIN team
                  ON project.Team_ID = team.ID
                WHERE (";  
            if(isset($_SESSION["team_list_id"]))
            {
              $team_list_id=$_SESSION["team_list_id"];
              $teamlist_count = count($_SESSION["team_list_id"]);
              for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
              {
                $readsql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
              }
                $readsql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
            }
            else
            {
              $readsql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
            }
        $readsql.=") AND bnc.Status = 'Paid'";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      while($rows = mysqli_fetch_assoc($result)){
        echo "<tr>
                <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                <td> ".$rows["Percent"]." </td>
                <td> ".$rows["Amount"]." </td>
                <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>
                <td> ".$rows["Endorced_To_Client"]." </td>
                <td> ".$rows["Invoice_Number"]." </td>
                <td> ".$rows["Endorced_To_Accounting"]." </td>
                <td> ".$rows["OR_Number"]." </td>
                <td> &#8369; ".$rows["Amount_Paid"]." </td>
                <td> ".$rows["Payment_Type"]." </td>
              </tr>";
      }
    }
  }
  
  //for requested
  if(isset($_POST["get_requested_bnc"]))
  {
    $sql = "SELECT project.ID,
              project.Project_Name, 
              project.Team_ID, 
              project.Project_Number,
              project.Amount,
              external_deadline.Percent,
              external_deadline.Phase,	
              external_deadline.ID AS exid ,
              external_deadline.External_Deadline,
              external_deadline.Status,
              team.Team_Name
            FROM project 
            INNER JOIN external_deadline 
              ON project.ID = external_deadline.Project_ID 
            INNER JOIN team
              ON project.Team_ID = team.ID
            WHERE (";  
            if(isset($_SESSION["team_list_id"]))
            {
              $team_list_id=$_SESSION["team_list_id"];
              $teamlist_count = count($_SESSION["team_list_id"]);
              for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
              {
                $sql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
              }
                $sql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
            }
            else
            {
              $sql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
            }
        $sql.=")and external_deadline.Status = 'Done'";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result) > 0)
        {
          while($rows = mysqli_fetch_assoc($result)){
            echo "<tr>
                    <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                    <td> ".$rows["Percent"]." </td>
                    <td> &#8369; ".$rows["Amount"]." </td>
                    <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>";
            echo"</tr>";
          }
        }
  }
  
  //for total amount
  if(isset($_POST["total_pending_amount"]))
  {
     $readsql = "SELECT
                  FORMAT(SUM(REPLACE(project.Amount,',','')),2) AS Total_Pending_Amount
                FROM bnc
                INNER JOIN external_deadline
                  ON bnc.Externaldl_ID = external_deadline.ID
                INNER JOIN project 
                  ON external_deadline.Project_ID = project.ID
                WHERE (";  
            if(isset($_SESSION["team_list_id"]))
            {
              $team_list_id=$_SESSION["team_list_id"];
              $teamlist_count = count($_SESSION["team_list_id"]);
              for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
              {
                $readsql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
              }
                $readsql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
            }
            else
            {
              $readsql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
            }
        $readsql.=") AND bnc.Status = 'Pending'";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      $row = mysqli_fetch_assoc($result);
      echo $row["Total_Pending_Amount"];
    }
  }
  
  if(isset($_POST["total_paid_amount"]))
  {
    $readsql = "SELECT
                  FORMAT(SUM(REPLACE(bnc.Amount_Paid,',','')),2) AS Total_Paid_Amount
                FROM bnc
                INNER JOIN external_deadline
                  ON bnc.Externaldl_ID = external_deadline.ID
                INNER JOIN project 
                  ON external_deadline.Project_ID = project.ID
                WHERE (";  
            if(isset($_SESSION["team_list_id"]))
            {
              $team_list_id=$_SESSION["team_list_id"];
              $teamlist_count = count($_SESSION["team_list_id"]);
              for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
              {
                $readsql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
              }
                $readsql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
            }
            else
            {
              $readsql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
            }
        $readsql.=") AND bnc.Status = 'Paid'";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      $row = mysqli_fetch_assoc($result);
      echo $row["Total_Paid_Amount"];
    }
  }
  
  //for pdTrakcing
  //for count
  if(isset($_POST["pendint_count"])){
    $readsql = "SELECT 
                    COUNT(internal_deadline.ID) AS pendint_count
									FROM project
									INNER JOIN external_deadline 
										ON project.ID = external_deadline.Project_ID
									INNER JOIN internal_deadline 
										ON external_deadline.ID = internal_deadline.Externaldl_ID
									LEFT JOIN team 
										ON project.Team_ID = team.ID
									WHERE (";  
          if(isset($_SESSION["team_list_id"]))
          {
            $team_list_id=$_SESSION["team_list_id"];
            $teamlist_count = count($_SESSION["team_list_id"]);
            $tl_count = 0;
            for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
            {
              $readsql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
            }
              $readsql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
          }
          else
          {
            $readsql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
          }
          $readsql.=") AND internal_deadline.Status = 'Pending'";
              $result = mysqli_query($conn,$readsql);
              if(mysqli_num_rows($result) > 0)
              {
      $row = mysqli_fetch_assoc($result);
      echo $row["pendint_count"];
    }
  }
  
  if(isset($_POST["pendext_count"])){
    $sql = "SELECT 
										COUNT(external_deadline.ID) AS pendext_count 
									FROM project 
									INNER JOIN external_deadline 
										ON project.ID = external_deadline.Project_ID 
                  INNER JOIN team
                    ON project.Team_ID = team.ID
									WHERE (";  
                  if(isset($_SESSION["team_list_id"]))
                  {
                    $team_list_id=$_SESSION["team_list_id"];
                    $teamlist_count = count($_SESSION["team_list_id"]);
                    for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
                    {
                      $sql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
                    }
                      $sql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
                  }
                  else
                  {
                    $sql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
                  }
              $sql.=")and external_deadline.Status = 'Pending'";
					$result = mysqli_query($conn,$sql);
              if(mysqli_num_rows($result) > 0)
              {
      $row = mysqli_fetch_assoc($result);
      echo $row["pendext_count"];
    }
  }
  
  if(isset($_POST["doneint_count"])){
    $readsql ="SELECT 
                    COUNT(internal_deadline.ID) AS doneint_count
									FROM project
									INNER JOIN external_deadline 
										ON project.ID = external_deadline.Project_ID
									INNER JOIN internal_deadline 
										ON external_deadline.ID = internal_deadline.Externaldl_ID
									LEFT JOIN team 
										ON project.Team_ID = team.ID
									WHERE (";  
          if(isset($_SESSION["team_list_id"]))
          {
            $team_list_id=$_SESSION["team_list_id"];
            $teamlist_count = count($_SESSION["team_list_id"]);
            $tl_count = 0;
            for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
            {
              $readsql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
            }
              $readsql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
          }
          else
          {
            $readsql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
          }
          $readsql.=") AND internal_deadline.Status = 'Done'";
              $result = mysqli_query($conn,$readsql);
              if(mysqli_num_rows($result) > 0)
              {
      $row = mysqli_fetch_assoc($result);
      echo $row["doneint_count"];
    }
  }
  
  if(isset($_POST["doneext_count"])){
    $sql = "SELECT 
									COUNT(external_deadline.ID) AS donexet_count 
									FROM project 
									INNER JOIN external_deadline 
										ON project.ID = external_deadline.Project_ID 
                  INNER JOIN team
                    ON project.Team_ID = team.ID
									WHERE (";  
                  if(isset($_SESSION["team_list_id"]))
                  {
                    $team_list_id=$_SESSION["team_list_id"];
                    $teamlist_count = count($_SESSION["team_list_id"]);
                    for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
                    {
                      $sql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
                    }
                      $sql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
                  }
                  else
                  {
                    $sql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
                  }
              $sql.=")and (external_deadline.Status = 'Done' OR external_deadline.Status = 'Billed')";
					$result = mysqli_query($conn,$sql);
              if(mysqli_num_rows($result) > 0)
              {
      $row = mysqli_fetch_assoc($result);
      echo $row["donexet_count"];
    }
  }
  
  //for list
  if(isset($_POST["pending_external"]))
  {
    $sql = "SELECT project.ID,
										project.Project_Name, 
										project.Team_ID, 
										project.Project_Number,
										external_deadline.Phase,	
										external_deadline.ID AS exid ,
                    external_deadline.External_Deadline,
                    external_deadline.Remarks,
                    team.Team_Name
									FROM project 
									INNER JOIN external_deadline 
										ON project.ID = external_deadline.Project_ID 
                  INNER JOIN team
                    ON project.Team_ID = team.ID
									WHERE (";  
                  if(isset($_SESSION["team_list_id"]))
                  {
                    $team_list_id=$_SESSION["team_list_id"];
                    $teamlist_count = count($_SESSION["team_list_id"]);
                    for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
                    {
                      $sql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
                    }
                      $sql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
                  }
                  else
                  {
                    $sql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
                  }
              $sql.=")and external_deadline.Status = 'Pending'";
					$result = mysqli_query($conn,$sql);
          if(mysqli_num_rows($result) > 0)
          {
            while($rows = mysqli_fetch_assoc($result)){
              echo "<tr>
                      <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                      <td> ".$rows["External_Deadline"]." </td>
                      <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>
                      <td> <span class='red'>".countdoneindl($conn,$rows["exid"],$rows["ID"])."</span> / ".countindl($conn,$rows["exid"],$rows["ID"])." 
                      (";
                        if(countindl($conn,$rows["exid"],$rows["ID"]) <= 0)
                        {
                              echo "0";
                        }
                        else
                        {
                          echo (round((countdoneindl($conn,$rows["exid"],$rows["ID"]) / countindl($conn,$rows["exid"],$rows["ID"])) * 100));
                        }
                      echo "%)</td>";
                          if($rows["Remarks"] == "Delayed")
                          {
                            echo "<td class='red bold'> ".$rows["Remarks"]." </td>";
                          }
                          else if($rows["Remarks"] == "On Time")
                          {
                            echo "<td class='green bold'> ".$rows["Remarks"]." </td>";
                          }
                        echo"</tr>";
            }
          }
  }
  
  if(isset($_POST["pending_internal"]))
  {
    $readsql ="SELECT 
                    internal_deadline.ID,
										project.Project_Name,
										project.Team_ID,
										project.Project_Number,
										external_deadline.Day_ED,
										external_deadline.Month_ED,
										external_deadline.Year_ED,	
                    external_deadline.External_Deadline,
                    external_deadline.ID AS exid,
										internal_deadline.Externaldl_ID,
										internal_deadline.Trade,
										internal_deadline.Phase, 
										internal_deadline.Day_ID,
										internal_deadline.Month_ID,
										internal_deadline.Year_ID,
										internal_deadline.Ticket_Number,
                    internal_deadline.Remarks,
										team.Team_Name,
                    internal_deadline.Internal_Deadline
									FROM project
									INNER JOIN external_deadline 
										ON project.ID = external_deadline.Project_ID
									INNER JOIN internal_deadline 
										ON external_deadline.ID = internal_deadline.Externaldl_ID
									LEFT JOIN team 
										ON project.Team_ID = team.ID
									WHERE (";  
          if(isset($_SESSION["team_list_id"]))
          {
            $team_list_id=$_SESSION["team_list_id"];
            $teamlist_count = count($_SESSION["team_list_id"]);
            $tl_count = 0;
            for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
            {
              $readsql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
            }
              $readsql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
          }
          else
          {
            $readsql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
          }
          $readsql.=") AND internal_deadline.Status = 'Pending'";
              $result = mysqli_query($conn,$readsql);
              if(mysqli_num_rows($result) > 0)
              {
                while($rows = mysqli_fetch_assoc($result)){
                  echo "<tr>
                          <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                          <td> ".$rows["Internal_Deadline"]." </td>
                          <td> ".$rows["External_Deadline"]." </td>
                          <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>
                          <td> ".$rows["Trade"]." </td>
                          <td>
                            <input type='button' class='btn-normal white bg-blue' value='Done' id='done".$rows["ID"]."'>
                            <script>
                              $('#done".$rows["ID"]."').on('click',function(){
                                $.ajax({
                                  beforeSend:function(){
                                    return confirm('Are you sure?');
                                  },
                                  url:'pdSubmit.php',
                                  type:'post',
                                  data:'update_internal=true'+
                                    '&status=Done'+
                                    '&internal_id=".$rows["ID"]."'+
                                    '&externaldl_id=".$rows["exid"]."',
                                  success:function(data){
                                    updateTracking();
                                  }
                                });
                              });
                            </script>
                          </td>";
                          if($rows["Remarks"] == "Delayed")
                          {
                            echo "<td class='red bold'> ".$rows["Remarks"]." </td>";
                          }
                          else if($rows["Remarks"] == "On Time")
                          {
                            echo "<td class='green bold'> ".$rows["Remarks"]." </td>";
                          }
                        echo"</tr>";
                }
              }
  }
  
  if(isset($_POST["done_external"]))
  {
    $sql = "SELECT project.ID,
										project.Project_Name, 
										project.Team_ID, 
										project.Project_Number,
										external_deadline.Phase,	
										external_deadline.ID AS exid ,
                    external_deadline.External_Deadline,
                    external_deadline.Status,
                    external_deadline.Remarks,
                    external_deadline.Actual_Done,
                    team.Team_Name
									FROM project 
									INNER JOIN external_deadline 
										ON project.ID = external_deadline.Project_ID 
                  INNER JOIN team
                    ON project.Team_ID = team.ID
									WHERE (";  
                  if(isset($_SESSION["team_list_id"]))
                  {
                    $team_list_id=$_SESSION["team_list_id"];
                    $teamlist_count = count($_SESSION["team_list_id"]);
                    for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
                    {
                      $sql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
                    }
                      $sql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
                  }
                  else
                  {
                    $sql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
                  }
              $sql.=")and (external_deadline.Status = 'Done' OR external_deadline.Status = 'Billed')";
					$result = mysqli_query($conn,$sql);
          if(mysqli_num_rows($result) > 0)
          {
            while($rows = mysqli_fetch_assoc($result)){
              echo "<tr>
                      <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                      <td> ".$rows["Actual_Done"]." </td>
                      <td> ".$rows["External_Deadline"]." </td>
                      <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>
                      <td> ".countdoneindl($conn,$rows["exid"],$rows["ID"])." / ".countindl($conn,$rows["exid"],$rows["ID"])."</td>";
                        if($rows["Remarks"] == "Delayed")
                        {
                          echo "<td class='red bold'> ".$rows["Remarks"]." </td>";
                        }
                        else if($rows["Remarks"] == "On Time")
                        {
                          echo "<td class='green bold'> ".$rows["Remarks"]." </td>";
                        }
                      echo"</tr>";
            }
          }
  }
  
  if(isset($_POST["done_internal"]))
  {
    $readsql ="SELECT 
                    internal_deadline.ID,
										project.Project_Name,
										project.Team_ID,
										project.Project_Number,
										external_deadline.Day_ED,
										external_deadline.Month_ED,
										external_deadline.Year_ED,	
                    external_deadline.External_Deadline,
                    external_deadline.ID AS exid,
                    external_deadline.Status AS exStatus,
										internal_deadline.Externaldl_ID,
										internal_deadline.Trade,
										internal_deadline.Phase, 
										internal_deadline.Day_ID,
										internal_deadline.Month_ID,
										internal_deadline.Year_ID,
										internal_deadline.Ticket_Number,
                    internal_deadline.Actual_Done,
                    internal_deadline.Remarks,
										team.Team_Name,
                    team.ID AS Team_ID,
                    internal_deadline.Internal_Deadline
									FROM project
									INNER JOIN external_deadline 
										ON project.ID = external_deadline.Project_ID
									INNER JOIN internal_deadline 
										ON external_deadline.ID = internal_deadline.Externaldl_ID
									LEFT JOIN team 
										ON project.Team_ID = team.ID
									WHERE (";  
          if(isset($_SESSION["team_list_id"]))
          {
            $team_list_id=$_SESSION["team_list_id"];
            $teamlist_count = count($_SESSION["team_list_id"]);
            $tl_count = 0;
            for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
            {
              $readsql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
            }
              $readsql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
          }
          else
          {
            $readsql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
          }
          $readsql.=") AND internal_deadline.Status = 'Done'";
              $result = mysqli_query($conn,$readsql);
              if(mysqli_num_rows($result) > 0)
              {
                while($rows = mysqli_fetch_assoc($result)){
                  echo "<tr>
                          <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                          <td> ".$rows["Internal_Deadline"]." </td>
                          <td> ".$rows["Actual_Done"]." </td>
                          <td> ".$rows["External_Deadline"]." </td>
                          <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>
                          <td> ".$rows["Trade"]." </td>
                          <td>";
                          if($rows["exStatus"] == "Pending" || $rows["exStatus"] == "Done")
                          {
                            echo "<input type='button' class='btn-normal white bg-red' value='Undo' id='done".$rows["ID"]."'>
                          <script>
                            $('#done".$rows["ID"]."').on('click',function(){
                              $.ajax({
                                beforeSend:function(){
                                  return confirm('Are you sure?');
                                },
                                url:'pdSubmit.php',
                                type:'post',
                                data:'update_internal=true'+
                                  '&status=Pending'+
                                  '&internal_id=".$rows["ID"]."'+
                                  '&externaldl_id=".$rows["exid"]."',
                                success:function(data){
                                  updateDoneInternal();
                                }
                              });
                            });
                          </script>";
                          }
                          else
                          {
                            echo "<input type='button' class='btn-no bg-white green' value='Billed' />";
                          }
                          echo "</td>";
                          if($rows["Remarks"] == "Delayed")
                          {
                            echo "<td class='red bold'> ".$rows["Remarks"]." </td>";
                          }
                          else if($rows["Remarks"] == "On Time")
                          {
                            echo "<td class='green bold'> ".$rows["Remarks"]." </td>";
                          }
                        echo "</tr>";
                  
                }
              }
  }
  
  function countindl($conn,$externaldl_id,$project_id){
    $count = 0;
    $readsql = "SELECT 
                    COUNT(internal_deadline.ID) AS pendint_count
									FROM project
									INNER JOIN external_deadline 
										ON project.ID = external_deadline.Project_ID
									INNER JOIN internal_deadline 
										ON external_deadline.ID = internal_deadline.Externaldl_ID
									LEFT JOIN team 
										ON project.Team_ID = team.ID
									WHERE (";  
          if(isset($_SESSION["team_list_id"]))
          {
            $team_list_id=$_SESSION["team_list_id"];
            $teamlist_count = count($_SESSION["team_list_id"]);
            $tl_count = 0;
            for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
            {
              $readsql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
            }
              $readsql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
          }
          else
          {
            $readsql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
          }
          $readsql.=")
              AND external_deadline.ID = $externaldl_id";
              $result = mysqli_query($conn,$readsql);
              if(mysqli_num_rows($result) > 0)
              {
      $row = mysqli_fetch_assoc($result);
      $count = $row["pendint_count"];
    }
    return $count;
  }
  
  function countdoneindl($conn,$externaldl_id,$project_id){
    $count = 0;
    $readsql = "SELECT 
                    COUNT(internal_deadline.ID) AS pendint_count
									FROM project
									INNER JOIN external_deadline 
										ON project.ID = external_deadline.Project_ID
									INNER JOIN internal_deadline 
										ON external_deadline.ID = internal_deadline.Externaldl_ID
									LEFT JOIN team 
										ON project.Team_ID = team.ID
									WHERE (";  
          if(isset($_SESSION["team_list_id"]))
          {
            $team_list_id=$_SESSION["team_list_id"];
            $teamlist_count = count($_SESSION["team_list_id"]);
            $tl_count = 0;
            for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
            {
              $readsql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
            }
              $readsql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
          }
          else
          {
            $readsql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
          }
          $readsql.=") AND internal_deadline.Status = 'Done'
              AND external_deadline.ID = $externaldl_id";
              $result = mysqli_query($conn,$readsql);
              if(mysqli_num_rows($result) > 0)
              {
      $row = mysqli_fetch_assoc($result);
      $count = $row["pendint_count"];
    }
    return $count;
  }
