<?php
	session_start();
	include('connect.php');
		$curr_time = date("h:i");
		$cadtech_id = $_SESSION['ID'];
		$curr_year = (int)date("Y");
		$curr_month = (int)date("m");
		$curr_day = (int)date("d");
		if($curr_day <= 15)
		$day_limit = 15;																																																																													 
		if($curr_day > 15)
		$day_limit = 31;

		$sql = "SELECT
							project.ID AS Project_ID,
							project.Project_Number,
							project.Project_Name,
							review_logs.Ticket_Number,
							review_logs.ActivityLogs_ID,
							cadtech_notif.ID,
							cadtech_notif.Budget,
							cadtech_notif.Deadline,
							cadtech_notif.Status
						FROM cadtech_notif
						INNER JOIN project
							ON project.ID = cadtech_notif.Project_ID
						INNER JOIN review_logs
							ON review_logs.ID = cadtech_notif.ReviewLogs_ID
						WHERE cadtech_notif.CadTech_ID = $cadtech_id";
	
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			while($rows = mysqli_fetch_assoc($result))
			{										 
				//remove click event in weekly schedule if the activity is in progress
				$activitysql = "SELECT Activity
												FROM activity_logs
												WHERE ID = ".$rows["ActivityLogs_ID"]."";
				$activityresult = mysqli_query($conn,$activitysql);
				$activityrow = mysqli_fetch_assoc($activityresult);

				if($rows["Status"] == "In Progress")
				{
					echo "<tr id='tr".$rows['ID']."' class='non-clickable'>";
					echo"	<td id='projectSched".$rows['ID']."'> ".$rows['Project_Number']." ".$rows['Project_Name']." </td>
								<td id='ticketSched".$rows['ID']."'> ".$rows["Ticket_Number"]." </td>
								<td> ".$rows['Budget']." hrs </td>
								<td id='activitySched".$rows['ID']."'> ".$activityrow['Activity']." </td>
								<td> ".$rows['Deadline']." </td>
								<td id='dnotif_id".$rows['ID']."' class='display-none yellow bold'> ".$rows['ID']." </td>
								<td class='display-none yellow bold' id='project_id".$rows['ID']."'> ".$rows['Project_ID']." </td>
								<td class='yellow bold'> ".$rows['Status']." </td>
									<script>
										$('#switchBtn').show();
									</script>
								</tr>";
				}
				else if( $rows["Status"] == "Hold")
				{																																
					echo "<tr id='tr".$rows['ID']."' class='clickable' onclick='fillInfo".$rows['ID']."()'>";
					echo"	<td id='projectSched".$rows['ID']."'> ".$rows['Project_Number']." ".$rows['Project_Name']." </td>
								<td id='ticketSched".$rows['ID']."'> ".$rows["Ticket_Number"]." </td>
								<td> ".$rows['Duration']." </td>
								<td id='activitySched".$rows['ID']."'> ".$activityrow['Activity']." </td>		
								<td> ".$rows['Deadline']." </td>
								<td id='dnotif_id".$rows['ID']."' class='display-none'> ".$rows['ID']." </td>
								<td class='display-none' id='project_id".$rows['ID']."'> ".$rows['Project_ID']." </td>
								<td class='gray bold'> ".$rows['Status']." </td> ";	 
								echo "<script>
												function fillInfo".$rows['ID']."()
												{																						 
													innerHTMLToValue('projectSched".$rows['ID']."','projectInfo');	
													innerHTMLToValue('activitySched".$rows['ID']."','activityInfo'); 
													innerHTMLToValue('ticketSched".$rows['ID']."','ticketInfo');	 
													innerHTMLToValue('project_id".$rows['ID']."','project_idInfo');	
													innerHTMLToValue('dnotif_id".$rows['ID']."','dnotif_idInfo');	
												}
											</script></tr>";
				}
				else if( $rows["Status"] == "Standby")
				{
					echo "<tr id='tr".$rows['ID']."' class='clickable' onclick='fillInfo".$rows['ID']."()'>";
					echo"	<td id='projectSched".$rows['ID']."'> ".$rows['Project_Number']." ".$rows['Project_Name']." </td>
								<td id='ticketSched".$rows['ID']."'> ".$rows["Ticket_Number"]." </td>
								<td> ".$rows['Budget']." hrs </td>
								<td id='activitySched".$rows['ID']."'> ".$activityrow['Activity']." </td>
								<td> ".$rows['Deadline']." </td>
								<td id='dnotif_id".$rows['ID']."' class='display-none'> ".$rows['ID']." </td>
								<td class='display-none' id='project_id".$rows['ID']."'> ".$rows['Project_ID']." </td>
								<td> ".$rows['Status']." </td>";	 
								echo "<script>
												function fillInfo".$rows['ID']."()
												{																						 
													innerHTMLToValue('projectSched".$rows['ID']."','projectInfo');	
													innerHTMLToValue('activitySched".$rows['ID']."','activityInfo'); 
													innerHTMLToValue('ticketSched".$rows['ID']."','ticketInfo');	 
													innerHTMLToValue('project_id".$rows['ID']."','project_idInfo');	
													innerHTMLToValue('dnotif_id".$rows['ID']."','dnotif_idInfo');	
												}
											</script></tr>";
				}
				else if( $rows["Status"] == "Incomplete")
				{																																	 
					echo "<tr id='tr".$rows['ID']."' class='clickable'>";
					echo"	<td id='projectSched".$rows['ID']."'> ".$rows['Project_Number']." ".$rows['Project_Name']." </td>
								<td id='ticketSched".$rows['ID']."'> ".$rows["Ticket_Number"]." </td>
								<td> ".$rows['Duration']." </td>
								<td id='activitySched".$rows['ID']."'> ".$activityrow['Activity']." </td>		
								<td> ".$rows['Deadline']." </td>
								<td id='dnotif_id".$rows['ID']."' class='display-none'> ".$rows['ID']." </td>
								<td class='display-none' id='project_id".$rows['ID']."'> ".$rows['Project_ID']." </td>
								<td class='red bold'> ".$rows['Status']." </td>
								</tr>";
				}
				else if( $rows["Status"] == "On Review")
				{																																	 
					echo "<tr id='tr".$rows['ID']."' class='non-clickable'>";
					echo"	<td id='projectSched".$rows['ID']."'> ".$rows['Project_Number']." ".$rows['Project_Name']." </td>
								<td id='ticketSched".$rows['ID']."'> ".$rows["Ticket_Number"]." </td>
								<td> ".$rows['Budget']." hrs </td>
								<td id='activitySched".$rows['ID']."'> ".$activityrow['Activity']." </td>				 
								<td> ".$rows['Deadline']." </td>
								<td id='dnotif_id".$rows['ID']."' class='display-none'> ".$rows['ID']." </td>
								<td class='display-none' id='project_id".$rows['ID']."'> ".$rows['Project_ID']." </td>
								<td class='green bold'> ".$rows['Status']." </td>
								</tr>";
				}
			}
		}
		flush();
?>