<?php 
    session_start();
    include('connect.php');

	$today = date("F j, Y");
?>
<style>
	#menu_item_logo3
	{
		background-color:#f2f2f2;
	}
	#menu_item_logo3:hover
	{
		background-color:#f2f2f2;
	}
	#tab3
	{
		color:#515151;
		font-weight: bold;
		text-shadow: none;
	}

    #topic{
        border:none;
    }
</style>
<div id="info" style="width:100%;margin-left:0;padding-left:0;max-width:100%;">
				
					<h2> Incoming E-mail Log </h2>
				<h2><?php echo $_SESSION['Team_Name']; ?></h2>
				<h3 style="padding-bottom:0;margin-bottom:0;"><?php echo $today; ?></h3>					  
					<div id="topic" >											
                         <h2 style="visibility:hidden;">ds </h2>  
												 <label> Project </label>
					<select style="padding:3%;"name="ProjectNumber" id="ProjectNumber" required="required">
						<option> Select Project Number </option>
                        <option> Select Project Number </option>
							<?php
								 $designer_id = $_SESSION["ID"];
								 $team_id = $_SESSION["Team_ID"];
								 $sql = "SELECT ID,Project_Number ,Project_Name FROM project WHERE Team_ID = '".$team_id."'";
								 $result = mysqli_query($conn,$sql);
								 if(mysqli_num_rows($result) > 0)
								 {
									 while($rows = mysqli_fetch_assoc($result))
									 {
										 echo "<option value='".$rows["ID"]."'> ".$rows["Project_Number"]." - ".$rows["Project_Name"]."</option>";
									 }
								 }
							?>
					</select>
					</div>
					<div id="topic">
                         <h2 style="visibility:hidden;">ds </h2> 
												 <label> E-mail Link </label>   
					    <input type="text" id="email_link" placeholder="Paste Link of E-mail Here" />
					</div>
					<div id="topic">
						<h2 style="visibility:hidden;">df</h2>	 
												 <label> Classification </label>
                        <select id="classification">
                            <option> Classification </option>
                            <option value="I"> I </option>
                            <option value="RFI/A"> RFI/A </option>
                            <option value="F"> F </option>
                            <option value="C"> C </option>
                            <option value="MC"> MC </option>
                            <option value="0"> 0 </option>
                        </select>
					</div>
                    <div id="topic">
                         <h2 style="visibility:hidden;">ds </h2>   
												<label style="visibility:hidden;"> dfsdfs</label>     
					    <button style="display:inline-block; width:43%; margin:0;" id="submit1" class="submit_email">Submit</button>	
							<button style="display:inline-block; width:43%; margin:0;" id="submit1" class="generate_report">Generate Report</button>
                    </div>
			<div style="width:99%;height:30%;display:block;max-width:10000px;" id="list">
				<?php
					$readsql = "SELECT project.Project_Number, 
														 project.Project_Name,
														 dc_email_log.ID,
														 dc_email_log.Email_Link, 
														 dc_email_log.Classification 
											FROM dc_email_log 
											INNER JOIN project ON dc_email_log.Project_ID = project.ID";
					$result = mysqli_query($conn,$readsql);
					if(mysqli_num_rows($result) > 0)
					{
						echo "<table class='general_table width-100pc'>";
							echo "<thead>";
							echo "<tr>";
							echo "	<th>Project</th>";
							echo "	<th>Email Link</th>";
							echo "	<th>Classification</th>";
							echo "  <th>Action</th>";
							echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
						while($rows = mysqli_fetch_assoc($result))
						{
							echo "<tr>";											
							echo "	<td> ".$rows['Project_Number']." - ".$rows['Project_Name']."</td>";
							echo "	<td><a> ".$rows['Email_Link']." </a></td>";
							echo "	<td> ".$rows['Classification']." </td>";
							echo "<td><input id='deletebtn' class='".$rows['ID']."' type='submit' value='Delete' name='delete' /></td>";
							echo "<script>
											$(document).ready(function(){
												$('.".$rows['ID']."').on('click',function(){
													var value = $(this).val();
													$.ajax(
														{																					 
															url:'dc_submit.php',
															type:'post',
															data:'delete_email='+value+
																	 '&email_id=".$rows['ID']."',
															success:function(data)
																{
																	$('#list').html(data);
																},
														});
												});
											});
										</script>";
							echo "</tr>";
						}				
							echo "</tbody>";
							echo "</table>";
					}
				?>
			</div>
			<script>
				$(document).ready(function(){
					$('.submit_email').on('click',function(){
						var value = $(this).val();
						$.ajax(
							{
								url:'dc_submit.php',
								type:'post',
								data:'email_log='+value+
										 '&project_id=' + document.getElementById('ProjectNumber').value +	
										 '&email_link=' + document.getElementById('email_link').value +
										 '&classification=' + document.getElementById('classification').value,
								success:function(data)
								{
									$('#list').html(data);
								},
							});
					});
				});
				
				$(document).ready(function(){
					$('.generate_report').on('click',function(){
						var value = $(this).val();
						$.ajax(
							{
								url:'dc_table.php',
								type:'post',
								data:'email='+value,
								success:function(data)
								{
									$('#mainform').html(data);
								},
							});
					});
				});
				
			</script>