<?php
  session_start();
  include("connect.php");
  include("phpScript.php");			
  $designer_id =  $_SESSION["ID"];
  $sql = "";
  $today = date("F j, Y");
  $date = date("Y-m-d");
  $curr_time = 0;
  ?>
<style>
  #navbar tr td#cad_tracking{
    background-color: #deebff;
    color:#0747a6;
  }
  #navbar tr td#tasks{
    background-color: #0747a6;
    color:#deebff;
  }
  
  .width-100{
      width:100%;
  }

  #list{
      height:90%;
      width:80%;
      min-height:300px;
      margin-right:0px;
      resize:none;
  }

  .general_table th{
      padding:2px;
  }
</style>
<div id="list" class="width-99pc">
    <div class="miniheader">
      <label class="h2"><?php echo $today; ?></label>
    </div>
    <div class="container2">
        <table class="general_table width-100pc">
            <thead>
                <tr>
                    <th> Designer </th>
                    <th> Deadline </th>
                    <th> Project </th>
                    <th> Requirements </th>
                    <th> Budget </th>
                    <th> Ticket </th>
                    <th> Actions </th>
                </tr>
            </thead>
            <tbody id="weeklySchedule">
             <!-- Data will be sent from pushSchedule.php -->
            </tbody>
        </table>			
    </div>
</div>				
<div id="message">
</div>
<script>

    function updateReport()
    {
        $.ajax(
        {
            url:'table.php',
            type:'post',
            data:'submit=true',
            success:function()
            {}
        });
    }

    $(document).ready(function(){
      
    });
    
</script>