<?php
		include('connect.php');
		session_start();
		$today = date("F j, Y");
		echo "<style>
				body{
					background-color:#34495e;
				}
				button{
					border:0; 
					padding:10px; 
					background-color:#0090e9; 
					color: white; 
					border-radius: 5px; 
					cursor: pointer; 
					box-shadow: 5px 5px 0px #3278bc;
				}
				button:active
				{
					box-shadow:0px 0px 5px 1px yellow;;
					margin:5px;
				}
				</style>";
				
			echo "<button style='margin-bottom:10px; position:fixed; z-index:1; opacity:0.9;' type='button' onclick='gohome()'>Back</button>";
		if(isset($_POST["submit"]))
			{
				$output = '';
				$output .= "<table style='border-collapse: collapse; margin-left:auto; margin-right:auto; box-shadow: 0px 0px 7px 0px; opacity:0.9;'>
					<tr><th style='background-color: #2874A6; width: auto; padding: 10px; font-size: 18px; color: white;'>Project</th>
						<th style='background-color: #2874A6; width: auto; padding: 10px; font-size: 18px; color: white;'>Ticket No.</th>
						<th style='background-color: #2874A6; width: auto; padding: 10px; font-size: 18px; color: white;'>Activity</th>
						<th style='background-color: #2874A6; width: auto; padding: 10px; font-size: 18px; color: white;'>Date</th>
						<th style='background-color: #2874A6; width: auto; padding: 10px; font-size: 18px; color: white;'>Time In</th>
						<th style='background-color: #2874A6; width: auto; padding: 10px; font-size: 18px; color: white;'>Time Out</th>
						<th style='background-color: #2874A6; width: auto; padding: 10px; font-size: 18px; color: white;'>Duration</th>
					</tr>
					";
					$designer_id = $_SESSION["ID"];
					$name = $_SESSION["Name"];
					$sql = "SELECT 
										project.Project_Number,
										project.Project_Name,
										activity_logs.Ticket_Number,
										activity_logs.Activity,
										activity_logs.Date,
										activity_logs.Time_In,
										activity_logs.Time_Out,
										activity_logs.Duration
									FROM activity_logs
									INNER JOIN project 
										ON project.ID = activity_logs.Project_ID
									WHERE 
										activity_logs.Designer_ID = $designer_id	
									ORDER BY 
										activity_logs.Date DESC,
										activity_logs.Time_In ASC";
					$result = mysqli_query($conn,$sql);
					if(mysqli_num_rows($result) > 0)
          {
            $row_count = 0;
            while($rows = mysqli_fetch_assoc($result))
              {
                $row_count++;
                if($row_count % 2 == 0){
                $output.= "
                  <tr style='background-color: #dfdfdf;'>
                    <td style=' border-top: none; border-bottom: none;'>". $rows["Project_Number"] ." - ". $rows["Project_Name"] ."</td>
                    <td style=' border-top: none; border-bottom: none;'>". $rows["Ticket_Number"] ."</td>
                    <td style=' border-top: none; border-bottom: none;'>". $rows["Activity"] ."</td>
                    <td style=' border-top: none; border-bottom: none;'>". $rows["Date"] ."</td>
                    <td style=' border-top: none; border-bottom: none;'>". $rows["Time_In"] ."</td>
                    <td style=' border-top: none; border-bottom: none;'>". $rows["Time_Out"] ."</td>
                    <td style=' border-top: none; border-bottom: none;'><p>'". $rows["Duration"] ."' hrs</p></td>
                  </tr>"; 
                }
                else
                {
                  $output.= "
                    <tr style='background-color: white;  border-top: none;'>
                      <td style=' border-top: none; border-bottom: none;'>". $rows["Project_Number"] ." - ". $rows["Project_Name"] ."</td>
                      <td style=' border-top: none; border-bottom: none;'>". $rows["Ticket_Number"] ."</td>
                      <td style=' border-top: none; border-bottom: none;'>". $rows["Activity"] ."</td>
                      <td style=' border-top: none; border-bottom: none;'>". $rows["Date"] ."</td>
                      <td style=' border-top: none; border-bottom: none;'>". $rows["Time_In"] ."</td>
                      <td style=' border-top: none; border-bottom: none;'>". $rows["Time_Out"] ."</td>
                      <td style=' border-top: none; border-bottom: none;'><p>'". $rows["Duration"] ."' hrs</p></td>
                    </tr>"; 	
                }
              }
            $output .= "</table>";
            // header("Content-Type: application/xls");
            //header("Content-Disposition: attachment; filename=".preg_replace('/[[:space:]]+/', '_', $_SESSION['name'])."(".preg_replace('/[[:space:]]+/', '', $today).").xls");
            //file_put_contents('//192.168.0.254/i$/Production Reference/DC/'.preg_replace('/[[:space:]]+/', '_', $_SESSION['name']).'('.preg_replace('/[[:space:]]+/', '', $today).').xls', $output);
            file_put_contents('//192.168.0.254/Users/Copy_Reports/'.$_SESSION['Team_Name'].'/'.preg_replace('/[[:space:]]+/', '_', $_SESSION['Name']).'.xls', $output);
            echo $output;
           }
			}