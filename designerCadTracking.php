<?php
  session_start();
  include("connect.php");
  
  $today = date("F j, Y");
  $shortcut_name = $_SESSION["Lastname"].", ".$_SESSION["Firstname"];
?>
<style>
  #navbar tr td#cad_tracking{
    background-color: #deebff;
    color:#0747a6;
  }
    
  #navbar tr td#timesheet,#navbar tr td#des_tracking{
    background-color: #0747a6;
    color:#deebff;
  }
  
  #list{
      height:90%;
      width:80%;
      min-height:300px;
      margin-right:0px;
      resize:none;
  }
  
  .general_table th{
      padding:2px;
  }
  
  body{
      font-family:"Product Sans",sans-serif;
  }
    
      
  #list{
    height:90%;
    width:80%;
    min-height:300px;
    margin-right:0px;
    resize:none;
  }

  .general_table th{
      padding:2px;
  }
  
  .miniheader{
      background-color:#eef1f2;
      margin:0px;
      position:fixed;
      left:0px;
      top:60px;
      width:100%;
      height:100px;
      border-bottom:solid 2px #ececec;
  }
  
  .container2{
      margin-top:130px;
  }
  
  .tbl-tab-group{
      position:fixed;
      padding-left:25px;
      top:115px;
  }
  
  .tbl-tab{
      font-size:16px;
      font-weight:bold;
      display:inline-block;
      background-color:#f7f9fa;
      border-radius:4px;
      padding:10px 17px;
      cursor:pointer;
      filter:brightness(100%);
  }
  
  .tbl-tab:hover{
      filter:brightness(75%);
  }
  
  .active-tbl-tab{
      border-radius:4px 4px 0px 0px;
      background-color:#f7f9fa;
      border-bottom: solid 2px #f7f9fa;
      border-top:solid 2px #ececec;
      border-left:solid 2px #ececec;
      border-right:solid 2px #ececec;
  }
  
  #shortcut_name{
      font-size:30px;
      font-weight:bold;
      margin-left:30px;
  }
  
  #percent_holder{
      position:fixed;
      right:50px;
      top:70px;
  }
  
  #percent_bar{
      border-radius:4px;
      border:solid 4px #515151;
      width:208px;
      height:22px;
      display:inline-block;
      
  }
  
  #progress{
      background-color:red;
      width:0px;
      height:22px;
  }
  
  #percent_number{
      font-size:50px;
  }
  
  #pending_count{
      
  }
</style>

<div id="list" class="width-99pc">
    <div class="miniheader">
      <label id="shortcut_name"><?php echo $shortcut_name; ?></label>
      <ul class="tbl-tab-group">
          <li class="tbl-tab active-tbl-tab" id="standby_tab"> Standby <span id="standby_count">(0)</span> </li>
          <li class="tbl-tab" id="assigned_tab"> Assigned <span id="assigned_count">(0)</span> </li>
          <li class="tbl-tab" id="requested_tab"> Requested <span id="requested_count">(0)</span> </li>
          <li class="tbl-tab" id="rejected_tab"> Rejected <span id="rejected_count">(0)</span> </li>
          <li class="tbl-tab" id="approved_tab"> Approved <span id="approved_count">(0)</span> </li>
      </ul>
      <div id="percent_holder">
        <div id="percent_bar">
          <div id="progress"></div>
        </div>
        <label id="percent_number"> 30% </label>
      </div>
    </div>
    <div class="container2">
        <table class="general_table width-100pc">
            <thead>
                <tr>
                    <th> Designer </th>
                    <th> Deadline </th>
                    <th> Project </th>
                    <th> Requirements </th>
                    <th> Budget </th>
                    <th> Ticket </th>
                    <th> Status </th>
                    <th> CAD Technician </th>
                </tr>
            </thead>
            <tbody id="weeklySchedule">
             <!-- Data will be sent from pushSchedule.php -->
            </tbody>
        </table>			
    </div>
</div>				
<script>
  
    $('#standby_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#assigned_tab').prop("class","tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
      $('#approved_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $.ajax({
        url:'designerGetData.php',
        type:'post',
        data:'get_cad_tracking=true'+
             '&status=Approved',
        success:function(data){
          $('#weeklySchedule').html(data);
        }
      });
    });
    
    $('#assigned_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#standby_tab').prop("class","tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
      $('#approved_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $.ajax({
        url:'designerGetData.php',
        type:'post',
        data:'get_cad_tracking=true'+
             '&status=Assigned',
        success:function(data){
          $('#weeklySchedule').html(data);
        }
      });
    });
    
    $('#requested_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#assigned_tab').prop("class","tbl-tab");
      $('#standby_tab').prop("class","tbl-tab");
      $('#approved_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $.ajax({
        url:'designerGetData.php',
        type:'post',
        data:'get_cad_tracking=true'+
             '&status=CADRequested',
        success:function(data){
          $('#weeklySchedule').html(data);
        }
      });
    });
    
    $('#approved_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#assigned_tab').prop("class","tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
      $('#standby_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $.ajax({
        url:'designerGetData.php',
        type:'post',
        data:'get_cad_tracking=true'+
             '&status=CADAccepted',
        success:function(data){
          $('#weeklySchedule').html(data);
        }
      });
    });
    
    $('#rejected_tab').on('click',function(){
       $(this).prop("class","tbl-tab active-tbl-tab");
      $('#assigned_tab').prop("class","tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
      $('#standby_tab').prop("class","tbl-tab");
      $('#approved_tab').prop("class","tbl-tab");
       $.ajax({
         url:'designerGetData.php',
         type:'post',
         data:'get_cad_tracking=true'+
              '&status=CADRejected',
         success:function(data){
           $('#weeklySchedule').html(data);
         }
       });
     });
</script>
