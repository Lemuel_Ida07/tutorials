<?php
include("connect.php");
include("phpGlobal.php");
$NoLastDays = (int)$_POST['NoLastDays'];
$month_selected = (int)$_POST['month_selected'];
$year_selected = (int)$_POST['year_selected'];
$tradeString = $_POST['tradeString'];
$designer_id = $_POST['designer_id'];
$current_day = date("d");

$totaldays=cal_days_in_month(CAL_GREGORIAN,$month_selected,$year_selected);
$loop = ($NoLastDays + $totaldays) / 7;
$extradays = ($NoLastDays + $totaldays) % 7;
if($extradays != 0)
	$loop ++;
$daycounter = $NoLastDays - (($NoLastDays * 2)-1);
$dayweek = 7 -$NoLastDays;
$count4 = 0; // para sa count ng kung ilan yung event per day

$lastinternaldl = "";
$phase = "";

echo "<tr>
			<th id='weekends' class='title'> Sunday </th>
			<th id='weekdays' class='title'> Monday </th>
			<th id='weekdays' class='title'> Tuesday </th>
			<th id='weekdays' class='title'> Wednesday </th>
			<th id='weekdays' class='title'> Thursday </th>
			<th id='weekdays' class='title'> Friday </th>
			<th id='weekends' class='title'> Saturday </th>
		 </tr>";
		 // loop to create weeks
for($count = 1; $count <= $loop; $count ++)
{
  $weekday = 0;
	echo"<tr>";
	// loop to create days
	for($count2 = $daycounter; $count2 <= $dayweek; $count2 ++)
	{
      $weekday++;
		$sql2 = "SELECT internal_deadline.ID,
									project.Project_Name,
									project.Team_ID,
									project.Project_Number,
									internal_deadline.Trade,
									internal_deadline.Day_ID,
									internal_deadline.Phase
								FROM project
								INNER JOIN external_deadline 
									ON project.ID = external_deadline.Project_ID
                INNER JOIN internal_deadline 
									ON external_deadline.ID = internal_deadline.Externaldl_ID
								WHERE 
									internal_deadline.Day_ID = $count2 
									AND internal_deadline.Month_ID = $month_selected 
									AND internal_deadline.Year_ID = $year_selected and internal_deadline.Trade ='$tradeString'";

		$result2 = mysqli_query($conn,$sql2);

		$dnotifsql = "SELECT CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Name,
										designer_notif.ID,
										project.Project_Number,
										project.Project_Name,
										project.Team_ID,
										designer_notif.Budget,
										designer_notif.Internaldl_ID,
										internal_deadline.Phase
									FROM designer_notif
										INNER JOIN user ON user.ID = designer_notif.Designer_ID
										INNER JOIN internal_deadline ON internal_deadline.ID = designer_notif.Internaldl_ID
										INNER JOIN project ON designer_notif.Project_ID = project.ID
									WHERE 
										designer_notif.Designer_ID = $designer_id 
										AND EXTRACT(DAY FROM(designer_notif.Deadline)) = $count2 
										AND EXTRACT(MONTH FROM(designer_notif.Deadline)) = $month_selected 
										AND EXTRACT(YEAR FROM(designer_notif.Deadline)) = $year_selected 
										AND internal_deadline.Trade = '$tradeString'";

		$dnotifresult = mysqli_query($conn,$dnotifsql);

		if(mysqli_num_rows($result2) > 0)
		{
			if($NoLastDays > 0 || $count2 == $daycounter || $count2 == $daycounter + 6)
			{
				if($count2 <= 0)
				{
					echo "<td id='days' >  </td>";
				}
				else
				{
					echo "<td id='days' > $count2 </td>";
				}
				$NoLastDays = $NoLastDays-1;
			}
			else{
				echo "<td id='days'";
							if($count2 == $current_day)
							{				
								echo "style = 'border:dotted 3px #3396d9;'";
							}
              $readsql3 = "SELECT MAX(Day_ID) as Day_ID  FROM internal_deadline WHERE Month_ID = $month_selected and Year_ID = $year_selected and Trade ='$tradeString'";
              $result3 = mysqli_query($conn,$readsql3);
              $row = mysqli_fetch_assoc($result3);
              $lastinternaldl = $row['Day_ID'];
              if($count2 <= $lastinternaldl)
              {
                if($count2 >= $current_day)
                {
                echo "onClick='openPop();  getDate($count2,$month_selected,$year_selected,$weekday)'";
                
                }
              }	
							echo "> $count2
						<div class='$count2' id='daily_container'>";

				// SHOW INTERNAL DEADLINES
				while($rows2 = mysqli_fetch_assoc($result2))
				{
					$INcurID = $rows2['Team_ID']; // get TEAM ID
					$phase = $rows2["Phase"];
          echo "<p style='background-color: $in_dead_bg[$INcurID];' id='indeadline".$rows2['ID']."' class='event'>  ".$rows2['Project_Name']." - ".$rows2["Phase"]." (".$rows2['Trade'].") </p>";	
					
					echo "<script>
									$(document).ready(function(){
												$('#indeadline".$rows2['ID']."').on('click',function(){
													var value = $(this).val();
														$.ajax(
														{
															url:'deadline.php',
															type:'post',
															data:'internal_details=true&internaldl_id=".$rows2['ID']."&phase=".$rows2['Phase']."',
															success:function(data)
															{   
																$('#content_tray').html(data);
															},
															error:function(data)
															{
																	alert(data);
															}
														});
												});
											});
										  </script>";
				}
				// SHOW DESIGNER DEADLINES
				while($rows3 = mysqli_fetch_assoc($dnotifresult))
				{
          $INcurID = $rows3["Team_ID"];
          echo "<p style='background-color: white; color:black; border:solid 2px $in_dead_bg[$INcurID]; width:94%; border-radius:5px;' id='designer_deadline".$rows3['ID']."' class='event'>  ".$rows3['Project_Name']." - ".$rows3["Phase"]." (".$rows3["Budget"]." hrs) </p>";

					echo "<script>
									$(document).ready(function(){
                      draggableMe('designer_deadline".$rows3['ID']."');
											$('#designer_deadline".$rows3['ID']."').on('click',function(e){
												var value = $(this).val();
														$.ajax(
														{
															url:'deadline.php',
															type:'post',
															data:'designerdl_id='+".$rows3['ID']."+'&internaldeadline='+".$rows3['Internaldl_ID'].",
															success:function(data)
															{
																$('#content_tray').html(data);
                                placeMeCursor('designerdl_pop',e);
                                showDesignerdlPop();
                                $('#dnotif_id_designerdl').val('".$rows3["ID"]."');
															},
														});
												});
												});
										  </script>";
				}
				echo"</div>";
					echo "<script>
									$(document).ready(function(){
											$('.add$count2').on('click',function(){
												var value = $(this).val();
														$.ajax(
														{
															url:'dnotif_table.php',
															type:'post',
															data:'designer_id='+document.getElementById('designerid').value+'&trade=$tradeString&day_dn=$count2',
															success:function(data)
															{
																$('#budget').val('1');
																$('#dnotiftable').html(data);
															},
														});
														$.ajax(
														{
															url:'rGetProject.php',
															type:'post',
															data:'day_dn=$count2'+
																	 '&phase=$phase',
															success:function(data)
															{
																$('#datalist').html(data); 
															},
														});
												});
												});
										  </script>";
			}
		}
		else
		{
			if($NoLastDays > 0 || $count2 == $daycounter || $count2 == $daycounter + 6)
			{
				if($count2 <= 0)
				{
					echo "<td id='days' >  </td>";
				}
				else
				{
					echo "<td id='days' > $count2 </td>";
				}
				$NoLastDays = $NoLastDays-1;
			}
			else{
			echo "<td id='days'";
							if($count2 == $current_day)
							{		
								echo "style = 'border:dotted 3px #3396d9;'";
							}
              $readsql3 = "SELECT MAX(Day_ID) as Day_ID  FROM internal_deadline WHERE Month_ID = $month_selected and Year_ID = $year_selected and Trade ='$tradeString'";
              $result3 = mysqli_query($conn,$readsql3);
              $row = mysqli_fetch_assoc($result3);
              $lastinternaldl = $row['Day_ID'];
              if($count2 <= $lastinternaldl)
              {
                if($count2 >= $current_day)
                {
                  echo "onClick='openPop();  getDate($count2,$month_selected,$year_selected,$weekday)'";
                }
              }
			echo "> $count2
			
						<div class='$count2' id='daily_container'>";
						
								while($rows3 = mysqli_fetch_assoc($dnotifresult))
								{
									$INcurID = $rows3["Team_ID"];
									echo "<p style='background-color: white; color:black; border:solid 2px $in_dead_bg[$INcurID]; width:94%; border-radius:5px;' id='designer_deadline".$rows3['ID']."' class='event'>  ".$rows3['Project_Name']." - ".$rows3["Phase"]." (".$rows3["Budget"]." hrs) </p>";
									echo "<script>
									$(document).ready(function(){
                      draggableMe('designer_deadline".$rows3['ID']."');
											$('#designer_deadline".$rows3['ID']."').on('click',function(e){
												var value = $(this).val();
														$.ajax(
														{
															url:'deadline.php',
															type:'post',
															data:'designerdl_id='+".$rows3['ID']."+'&internaldeadline='+".$rows3['Internaldl_ID'].",
															success:function(data)
															{
																$('#content_tray').html(data);
                                placeMeCursor('designerdl_pop',e);
                                showDesignerdlPop();
                                $('#dnotif_id_designerdl').val('".$rows3["ID"]."');
															},
														});
												});
												});
										  </script>";
								}
								echo " </div>";
				echo "<script>
									$(document).ready(function(){
											$('.add$count2').on('click',function(){
												var value = $(this).val();
														$.ajax(
														{
															url:'dnotif_table.php',
															type:'post',
															data:'designer_id='+document.getElementById('designerid').value+'&trade=$tradeString&day_dn=$count2',
															success:function(data)
															{
																$('#budget').val('1');
																$('#dnotiftable').html(data);
															},
														});
														$.ajax(
														{
															url:'rGetProject.php',
															type:'post',
															data:'day_dn=$count2'+
																	 '&phase=$phase',
															success:function(data)
															{
																$('#datalist').html(data);
															},
														});	
												});
												});
										  </script>";

			}
		}
	}
	//echo $sql2;
	echo "</tr>";
	
	$daycounter = $count2;
	if($extradays!=0)
	{
		if($count < $loop - 2)
		{
			$dayweek = $count2 + 6;
		}
		else
		{
			$dayweek = $totaldays;
			echo "<script> document.getElementById('extraDaysPrevMonth').value ='$totaldays'; </script>";
		}
	}
	else
	{
		$dayweek = $count2 + 6;
	}
}
echo "<script>
        $('.event').click(function (e) {
          e.stopPropagation();
        });
    </script>";
?>
