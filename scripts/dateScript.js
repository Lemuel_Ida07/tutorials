function getSaturdays(year,month)
{
    var saturdays = new Array();
    var total_days = daysInMonth(month,year);
    var day = 0;
    
    for(day = 1; day <= total_days; day++)
    {
        var date = new Date(year,month,day);
        if(date.getDay() === 6)
        {
            saturdays.push(day);
        }
    }
    return saturdays;
}

function getSundays(year,month)
{
    var sundays = new Array();
    var total_days = daysInMonth(month,year);
    var day = 0;
    
    for(day = 1; day <= total_days; day++)
    {
        var date = new Date(year,month,day);
        if(date.getDay() === 0)
        {
            sundays.push(day);
        }
    }
    return sundays;
}

//get days in a month
function getDaysInMonth(month,year){
	return new Date(year,month,0).getDate();
}

//get days of the current week
function getWeekDays(){
    var date = new Date();
    var day = date.getDay();
    var array = []
    for(var i=0;i<7;i++){
    if(i-day!=0){
      var days = i-day;
      var newDate =   new Date(date.getTime()+(days * 24 * 60 * 60 * 1000));
      array.push(newDate.getDate());
    }
    else 
    array.push(date.getDate());
    }
    return array;
}

//gets 7 days of selected week number of current month and year
    function getWeek(week){
        var selected = "";
        var year = new Date().getFullYear();
        var month = new Date().getMonth() + 1;
        if(week === 1)
        {
            selected = year+"-"+month+"-01";
        }
        if(week === 2)
        {
            selected = year+"-"+month+"-08";
        }
        if(week === 3)
        {
            selected = year+"-"+month+"-15";
        }
        if(week === 4)
        {
            selected = year+"-"+month+"-22";
        }
        if(week === 5)
        {
            selected = year+"-"+month+"-29";
        }
        if(week === 6)
        {
            selected = year+"-"+month+"-31";    
        }
        var date = new Date(selected);
        var day = date.getDay();
        var array = [];
        for(var i=0;i<7;i++){
            if(i-day!==0){
              var days = i-day;
              var newDate = new Date(date.getTime()+(days * 24 * 60 * 60 * 1000));
              array.push(newDate.getDate());
            }
            else 
            {
            array.push(date.getDate());
            }
        }
        return array;
    }

//gets 7 days of selected week number of specified month and year
    function getWeekOfMonth(month,week){
        var selected = "";
        var year = new Date().getFullYear();
        //var month = new Date().getMonth() + 1;
        if(week === 1)
        {
            selected = year+"-"+month+"-01";
        }
        if(week === 2)
        {
            selected = year+"-"+month+"-08";
        }
        if(week === 3)
        {
            selected = year+"-"+month+"-15";
        }
        if(week === 4)
        {
            selected = year+"-"+month+"-22";
        }
        if(week === 5)
        {
            selected = year+"-"+month+"-29";
        }
        if(week === 6)
        {
            selected = year+"-"+month+"-31";    
        }
        var date = new Date(selected);
        var day = date.getDay();
        var array = [];
        for(var i=0;i<7;i++){
            if(i-day!==0){
              var days = i-day;
              var newDate = new Date(date.getTime()+(days * 24 * 60 * 60 * 1000));
              array.push(newDate.getDate());
            }
            else 
            {
            array.push(date.getDate());
            }
        }
        return array;
    }
            
function getMonFri(){
    var date = new Date();
    var day = date.getDay();
    var array = [];
    for(var i=1;i<6;i++){
      var days = i-day;
      var newDate =   new Date(date.getTime()+(days * 24 * 60 * 60 * 1000));
      array.push(newDate.getDate());    
    }
    return array;
}

function getHalfMonthDays(month,year,period){
	var total_days = getDaysInMonth(month,year);
	var half_month = [];
	if(period == 1)
	{
		var count = 1;
		for(count = 1;count<=15;count++)
		{
			half_month.push(count);
		}
	}
	else if(period == 2)
	{
		var count = 16;
		for(count = 16;count<=total_days;count++)
		{
			half_month.push(count);
		}
	}
	return half_month;
}

var dayofweek = [["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
                 ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]];
				 
//day of the week color (class)
var daycolor = ["red","","","","","","red"];

var monfri = ["Monday","Tuesday","Wednesday","Thursday","Friday"];

var month = ["January","February","March","April","May","June","July","August","September","October","November","December"];

var currdayborder = "";

//get current full string date
function today(){
    var d = new Date();
    var today = month[d.getMonth()] + 
                " " + d.getDate() + 
                ", " + d.getFullYear();
    return today;
}

//concatenate date from year month and day (yyyy-mm-dd)
function concatDate(year,month,day){
	var date = year + "-" + addZero(month) + "-" + addZero(day);
	return date;
}

//identify day of the week 
function identifyWeekDay(year,month,day){
  var week_day = 0;
  week_day = new Date(concatDate(year,month,day)).getDay();
  return week_day;
}

//get current day
function day_today(){
    var d = new Date();
    var day = d.getDate();
    return day;
}

//get current month
function month_today(){
    var d = new Date();
    var month = d.getMonth()+1;
    return month;
}

function year_today(){
    var d = new Date();
    var year = d.getFullYear();
    return year;
}

function addZero(number){
	if(number <= 9 && number > 0)
	{
		number = "0"+number;
	}
	return number;
}

function yearList(span){
	var  year = year_today() + 1;
	var new_year = [];
	for(count = 1; count <= span; count++)
	{
		year--;
		new_year.push(year)
	}
	return new_year;
}
var car = {run:function(){alert('The Car is running.');},stop:function(){alert('The Car is stoped.');}};