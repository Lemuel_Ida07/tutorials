﻿// #region INITIALIZE
	
//#endregion

// #region FOR POP ADD EXTERNAL DEADLINE
	$(document).ready(function () {
		
	});
// #endregion

// #region FOR EXTERNAL DEADLINE
	$(document).ready(function(){

		/* Hide External Deadline Pop */
		$('#pop_cancel_btn').on('click',function(){
			hidePop();
		});

		/* Add External Deadline */
		$('.add_externaldl').on('click', function () {
			var trades = [];
			var indead = [];
			var tickets = [];

			var trade = ["C","E","A","AU","S","P","M","FP","QS"];
			var tradeCBox = ["#tradeC", "#tradeE", "#tradeA", "#tradeAU", "#tradeS", "#tradeP", "#tradeM", "#tradeFP", "#tradeQS"];
			var tradeInDl = ["#C_in_dead", "#E_in_dead", "#A_in_dead", "#AU_in_dead", "#S_in_dead", "#P_in_dead", "#M_in_dead", "#FP_in_dead", "#QS_in_dead"];
			var tradeTicket = ["#Cticket", "#Eticket", "#Aticket", "#AUticket", "#Sticket", "#Pticket", "#Mticket", "#FPticket", "#QSticket"];
			for (var count = 0; count < trade.length; count++) {
				
				if ($(tradeCBox[count]).prop("checked")) {
					trades.push(trade[count]);
					var date = $(tradeInDl[count]).val();
					var ticket = $(tradeTicket[count]).val();
					indead.push(date);
					tickets.push(ticket);
				}
			}
			$.ajax(
				{
					beforeSend: function (request) {
						return confirm("Are you sure you want to generate this deadline?");
					},
					url: 'opsSubmit.php',
					type: 'post',
					data: {
                                            add_external_deadline: true,
                                            project_id: $('#projectid').val(),
                                            external_deadline: $('#external_deadline').val(),
                                            deliverables: $('#deliverables').val(),
                                            phase: $('#phaselist').val(),
                                            trades: trades,
                                            internal_deadline: indead,
                                            tickets: tickets
                                            },
					success: function (data) {
						alert(data);
						refreshCalendar();
						closePop();
					},
					error: function (data) {
						alert(data);
					}
			});
		});

		/* Edit External Deadline */
		$('#pop_save_btn').on('click',function(){
			$.ajax(
			{
				beforeSend:function(request){
					return confirm('Are you sure you want to save changes?');
				},
				url:'opsSubmit.php',
				type:'post',
				data:'edit_external_dl=true'+
						 '&externaldl_id='+$('#pop_externaldl_id').val()+
						 '&external_deadline='+$('#pop_external_deadline').val()+
						 '&phase='+$('#pop_phase').val()+
                                                 '&percent='+$('#pop_project_percent').val()+
						 '&deliverables='+$('#pop_deliverables').val(),
				success:function(data)
				{
					$.ajax(
					{
						url:'main_calendar.php',
						type:'post',
						data:'by_all=true'+
								'&month='+monthnumber+
								'&year='+year+
								'&lastday='+getDayName(monthnumber,year,1,0),
						success:function(data){
							refreshInternalDeadlineList();
							$('#pop_internal_deadline').attr('max', $('#pop_external_deadline').val());
							$('#edit_idl_internal_deadline').attr('max', $('#pop_external_deadline').val()); 
							$('#calendar').html(data);
						}
					});
				}
			});
		});

		/* Delete External Deadline */
		$('#pop_delete_btn').on('click',function(){
			$.ajax(
			{
				beforeSend:function(request){
					return confirm('Are you sure you want to DELETE?');
				},
				url:'opsSubmit.php',
				type:'post',
				data:'delete_externaldl=true'+
						 '&externaldl_id='+$('#pop_externaldl_id').val()+
						 '&phase='+$('#pop_phase').val(),
				success:function(data)
				{
						$.ajax(
						{
							url:'main_calendar.php',
							type:'post',
							data:'by_all=true'+
									'&month='+monthnumber+
									'&year='+year+
									'&lastday='+getDayName(monthnumber,year,1,0),
							success:function(data){
								$('#calendar').html(data);
								hidePop();
							}
						});
				}
			});
		});	
	});
// #endregion

// #region FOR INTERNAL DEADLINE
$(document).ready(function () {
	/* Add Internal Deadline */
	$('#add_internal_deadline').on('click', function () {
		$.ajax(
			{
				beforeSend: function (request) {
					return confirm('Are you sure you want to add this internal deadline?');
				},
				url: 'opsSubmit.php',
				type: 'post',
				data: 'add_internal_deadline=true' +
				'&internal_deadline=' + $('#pop_internal_deadline').val() +
				'&ticket=' + $('#pop_ticket_number').val() +
				'&trade=' + $('#pop_trade').val() +
				'&project_id=' + $('#pop_project_id').val() +
				'&phase=' + $('#pop_phase').val(),
				success: function (data) {
					if (data == "no_internal_deadline") {
						alert("No Internal Deadline!");
					}
					else if (data == "no_ticket") {
						alert("No Ticket Number");
					}
					else if (data == "ticket_already_exists") {
						alert('Ticket Number already used!');
					}
					else {
						refreshInternalDeadlineList();

						refreshCalendar();
					}
				}
			});
	});

	/* Edit Internal Deadline */
	$('#edit_idl_save').on('click', function () {
		$.ajax(
			{
				beforeSend: function (request) {
					return confirm('Are you sure you want to edit?');
				},
				url: 'opsSubmit.php',
				type: 'post',
				data: 'edit_internal_deadline=true' +
				'&trade=' + $('#edit_idl_trade').val() +
				'&ticket=' + $('#edit_idl_ticket').val() +
				'&internaldl_id=' + $('#edit_idl_inid').val() +
				'&internal_deadline=' + $('#edit_idl_internal_deadline').val(),
				success: function (data) {
					refreshCalendar();
				},
			});
	});

	/* Delete Internal Deadline */
	$('#edit_idl_delete').on('click', function () {
		$.ajax(
			{
				beforeSend: function (request) {
					return confirm('Are you sure you want to delete?');
				},
				url: 'opsSubmit.php',
				type: 'post',
				data: 'delete_internal_deadline=true' +
				'&internaldl_id=' + $('#edit_idl_inid').val() +
				'&internal_deadline=' + $('#edit_idl_internal_deadline').val(),
				success: function (data) {
					refreshInternalDeadlineList();

					refreshCalendar();

					hideIdlPop();
				},
			});
	});
});
// #endregion

// #region FOR POP INTERNAL DEADLINE
$(function () {
	$('#pop_edit_fieldset_idl').on('mousedown', function (e) {
		e.stopPropagation();
		$('#pop_edit_fieldset_idl').css("cursor", "default");
	});

	$('#fieldset_externaldl').on('mousedown', function (e) {
		e.stopPropagation();
		$('#fieldset_externaldl').css("cursor", "default");
	});
});
// #endregion

// #region FOR DRAGGABLE ELEMENTS
	$(document).ready(function () {
		/* Draggable Edit Internal Deadline */
		var draggable = $('#pop_edit_idl'); //element

		draggable.on('mousedown', function (e) {
			e.stopPropagation();
			var dr = $(this).addClass("drag").css("cursor", "move");
			height = dr.outerHeight();
			width = dr.outerWidth();
			ypos = dr.offset().top + height - e.pageY,
				xpos = dr.offset().left + width - e.pageX;
			$(document.body).on('mousemove', function (e) {
				var itop = e.pageY + ypos - height;
				var ileft = e.pageX + xpos - width;
				if (dr.hasClass("drag")) {
					dr.offset({ top: itop, left: ileft });
				}
			}).on('mouseup', function (e) {
				dr.removeClass("drag");
			});
		});

		var draggable = $('#activity_edit_pop'); //element

		draggable.on('mousedown', function (e) {
			e.stopPropagation();
			var dr = $(this).addClass("drag").css("cursor", "move");
			height = dr.outerHeight();
			width = dr.outerWidth();
			ypos = dr.offset().top + height - e.pageY,
				xpos = dr.offset().left + width - e.pageX;
			$(document.body).on('mousemove', function (e) {
				var itop = e.pageY + ypos - height;
				var ileft = e.pageX + xpos - width;
				if (dr.hasClass("drag")) {
					dr.offset({ top: itop, left: ileft });
				}
			}).on('mouseup', function (e) {
				dr.removeClass("drag");
			});
		});
                
                var draggable = $('#pop_markasdone'); //element

		draggable.on('mousedown', function (e) {
			e.stopPropagation();
			var dr = $(this).addClass("drag").css("cursor", "move");
			height = dr.outerHeight();
			width = dr.outerWidth();
			ypos = dr.offset().top + height - e.pageY,
				xpos = dr.offset().left + width - e.pageX;
			$(document.body).on('mousemove', function (e) {
				var itop = e.pageY + ypos - height;
				var ileft = e.pageX + xpos - width;
				if (dr.hasClass("drag")) {
					dr.offset({ top: itop, left: ileft });
				}
			}).on('mouseup', function (e) {
				dr.removeClass("drag");
			});
		});
	});
// #endregion

// #region VOID FUNCTIONS
	function refreshInternalDeadlineList()
	{
		$.ajax(
		{
			url:'deadline.php',
			type:'post',
			data:'internal_deadlines=true'+
						'&externaldl_id='+$('#pop_externaldl_id').val(),
			success:function(data)
			{
				$('#internal_list').html(data);
			}
		});
	}

	function refreshCalendar() {
		$.ajax(
			{
				url: 'main_calendar.php',
				type: 'post',
				data: 'by_all=true' +
				'&month=' + monthnumber +
				'&year=' + year +
				'&lastday=' + getDayName(monthnumber, year, 1, 0),
				success: function (data) {
					refreshInternalDeadlineList()
					$('#calendar').html(data);
				}
			});
	}
// #endregion

// #region ELEMENT CONTROLERS
	function hidePop() {
            $(".popUpBg").fadeOut();
            $(".popUp").fadeOut();
            $('#pop_project_percent').val(0);
	}

	function showPop() {
            $(".popUpBg").fadeIn();
            $(".popUp").fadeIn();
	}	
        
	function hideIdlPop() {
            $('#pop_edit_idl').slideUp();
	}

	function showIdlPop() {
            $('#pop_edit_idl').slideDown();
	}
// #endregion