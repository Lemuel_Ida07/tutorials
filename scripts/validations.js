/* Before submit validations */

//assign_designer(reviewSchedule.php)
function vAssignDesigner(){
    var result = false;
    var to_date = $('#to_date').val();
    var total_budget = $('#budget').val();
    if(isNoVal(to_date) || isNoVal(total_budget))
    {
        result = false;
    }
    else
    {
        result = true;
    }
    return result;
}

//functions for validation

//check if a field's value is null,whitespace or empty string
function isNoVal(val){
    var result = false;
    switch(val)
    {
        case "":
            result = true;
            break;
        case null:
            result = true;
            break;
        case " ":
            result = true;
            break;
        default:
            result = false;
            break;
    }
    return result;
}
