function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;

	return true;
}

function triggerClick(id) {
	document.getElementById(id).click();
}

function checkAllTrade() {
	

	if (document.getElementById('checkalltrade').innerHTML == "Check All") {
		document.getElementById('checkalltrade').innerHTML = "Uncheck All";
		if (document.getElementById('tradeC').checked == false) {
			triggerClick('tradeC');
		}
		if (document.getElementById('tradeE').checked == false) {
			triggerClick('tradeE');
		}
		if (document.getElementById('tradeA').checked == false) {
			triggerClick('tradeA');
		}
		if (document.getElementById('tradeAU').checked == false) {
			triggerClick('tradeAU');
		}
		if (document.getElementById('tradeS').checked == false) {
			triggerClick('tradeS');
		}
		if (document.getElementById('tradeP').checked == false) {
			triggerClick('tradeP');
		}
		if (document.getElementById('tradeM').checked == false) {
			triggerClick('tradeM');
		}
		if (document.getElementById('tradeFP').checked == false) {
			triggerClick('tradeFP');
		}
		if (document.getElementById('tradeQS').checked == false) {
			triggerClick('tradeQS');
		}
	}
	else {
		document.getElementById('checkalltrade').innerHTML = "Check All";
		if (document.getElementById('tradeC').checked == true) {
			triggerClick('tradeC');
		}
		if (document.getElementById('tradeE').checked == true) {
			triggerClick('tradeE');
		}
		if (document.getElementById('tradeA').checked == true) {
			triggerClick('tradeA');
		}
		if (document.getElementById('tradeAU').checked == true) {
			triggerClick('tradeAU');
		}
		if (document.getElementById('tradeS').checked == true) {
			triggerClick('tradeS');
		}
		if (document.getElementById('tradeP').checked == true) {
			triggerClick('tradeP');
		}
		if (document.getElementById('tradeM').checked == true) {
			triggerClick('tradeM');
		}
		if (document.getElementById('tradeFP').checked == true) {
			triggerClick('tradeFP');
		}
		if (document.getElementById('tradeQS').checked == true) {
			triggerClick('tradeQS');
		}
	}
}

function confirmSubmit() {
	return confirm("Please double check. Do you really want to do this action?");
}

function showDataList() {
	var element = document.getElementById('datalist').style.display = "block";
}

function hideDataList() {
	if (document.getElementById('datalist').style.display == "block")
		var element = document.getElementById('datalist').style.display = "none";
}

function hideDiv() {
	var element = document.getElementById('datalist').style.display;
	if (element == "block") { element = "none"; }
}

function filter(inputid, divid) {
	var input, filter, list, count;
	input = document.getElementById(inputid);
	filter = input.value.toUpperCase();
	div = document.getElementById(divid);
	list = div.getElementsByTagName("h4");
	for (count = 0; count < list.length; count++) {
		if (list[count].innerHTML.toUpperCase().indexOf(filter) > -1) {
			list[count].style.display = "";
		} else {
			list[count].style.display = "none";
		}
	}
}

//Check duplicate ticket numbers
function checkTicket(id) {
	var trades = ["C", "E", "A", "AU", "P", "M", "FP", "S","QS"];
	var selectedTicket = document.getElementById(id).value;
	var otherTicket = document.getElementById(trades[0] + 'ticket').value;
	for (i = 0; i < 8; i++) {

		if (selectedTicket != document.getElementById(trades[i] + 'ticket').value && id != trades[i] + 'ticket') {
			$('#' + id).css("box-shadow", "none");
		}
	}
	for (i = 0; i < 8; i++) {
		if (selectedTicket == document.getElementById(trades[i] + 'ticket').value && id != trades[i] + 'ticket' && selectedTicket != "") {
			alert("Ticket number (" + document.getElementById(trades[i] + 'ticket').value + ") already used. \n Ticket number must be unique.");
			$('#' + id).css("box-shadow", "0px 0px 2px 1px red");
		}
	}
}

//Validate submit
window.issubmit = false;
function validateSubmit() {
	var status = false;
	validateElement("#Activity");
	validateElement("#project_id");
	validateElement("#inptext1");
	validateElement("#inptext2");
	if (validateElement("#Activity") && validateElement("#project_id") && validateElement("#inptext1") && validateElement("#inptext2")) {
		status = true;
	}
	return status;
}

//validation for each element for validateSubmit()
function validateElement(id) {
	var value = $(id).val();
	if (!$.trim(value)) {
		$('#'+id).css("box-shadow", "0px 0px 2px 1px red");
		issubmit = false;
	}
	else if (value.includes("'"))
	{
		$('#' +id).css("box-shadow", "0px 0px 2px 1px red");
		issubmit = false;
	}
	else {
		$('#' +id).css("box-shadow", "1px 0px 2px 1px green");
		issubmit = true;
	}

	return issubmit;
}

//validation for each element for key up
function validateActivity() {
	var value = $("#Activity").val();

	if (!$.trim(value)) {
		$("#Activity").css("box-shadow", "none");
	}
	else {
		$("#Activity").css("box-shadow", "1px 0px 2px 1px green");
	}
}

function validatesearchbox() {
	var value = $("#project_id").val();
	if (!$.trim(value)) {
		if (!$.trim($("#searchbox").val())) {
			$("#searchbox").css("box-shadow", "none");
		}
		else {
			$("#searchbox").css("box-shadow", "0px 0px 2px 1px red");
		}
		
	}
	else if (value.includes("'")) {
		$("#searchbox").css("box-shadow", "0px 0px 2px 1px red");
	}
	else {
		$("#searchbox").css("box-shadow", "1px 0px 2px 1px green");
	}
}

function clearProjectID() {
	$("#project_id").val("");
	var pival = $("project_id").val();
	var sbval = $("#searchbox").val();

	if (!$.trim(pival) && !$.trim(sbval)) {
		$("#searchbox").css("box-shadow", "none");
	}
	else if (!$.trim(pival)) {
		$("#searchbox").css("box-shadow", "0px 0px 2px 1px red");
	}
	else { }
}
function validateinptext1() {
	var value = $("#inptext1").val();

	if (!$.trim(value)) {
		$("#inptext1").css("box-shadow", "none");
	}
	else if (value.includes("'")) {
		$("#inptext1").css("box-shadow", "0px 0px 2px 1px red");
	}
	else {
		$("#inptext1").css("box-shadow", "1px 0px 2px 1px green");
	}
}

function validateinptext2() {
	var value = $("#inptext2").val();

	if (!$.trim(value)) {
		$("#inptext2").css("box-shadow", "none");
	}
	else if (value.includes("'")) {
		$("#inptext2").css("box-shadow", "0px 0px 2px 1px red");
	}
	else {
		$("#inptext2").css("box-shadow", "1px 0px 2px 1px green");
	}
}

function loadSubmitFunctions() {
	$('#activityForm').submit(validateSubmit);
	$("#Activity").bind('keyup', validateActivity);
	$("#searchbox").bind('blur keyup', validatesearchbox);
	$("#searchbox").bind('keyup', clearProjectID);
	$("#inptext1").bind('keyup', validateinptext1);
	$("#inptext2").bind('keyup', validateinptext2);
}

/* Pass innerHTML to input */
function innerHTMLToValue(idFrom,idTo) {
	var text1 = document.getElementById(idFrom).innerHTML;
	document.getElementById(idTo).value = text1;
}

/* Pass input value to inner HTML */
function valueToInnerHTML(idFrom,idTo) {
	var text = document.getElementById(idFrom).value;
	document.getElementById(idTo).innerHTML = text;
}

/* Validations */
$(document).ready(function () {
    $("input[type=number]").keypres(function (e) {
        var keyCode = e.which;
        if (!(keyCode >= 48 && keyCode <= 57)) {
                e.preventDefault();
        }
    });
});

/*For reviewer.php*/
function calculateHours(task_hour,id){
    if(task_hour == '' || isNaN(task_hour))
    {
        $('#'+id).val("0");
        var total_budget = 0;
        $(".budget_inp").each(function(){
            total_budget = total_budget + parseInt($(this).val());
        });
        $('#budget').val(parseInt(total_budget));
    }
    
    if((task_hour !== null & task_hour !== '') && !isNaN(task_hour))
    {
        var total_budget = 0;
        $(".budget_inp").each(function(){
            var value = $(this).val();
            if(value == '' || isNaN(value))
            {
                $(this).val("0");
                $('#assign_designer').css('visibility','visible');
                $('#assign_designer').slideDown();
                $('#edit_designer').slideUp();
                value = 0;
            }
            if(value != 0)
            {
                $(this).css("box-shadow","0px 0px 4px #4caf50");
                $('#assign_designer').css('visibility','visible');
                $('#assign_designer').slideDown();
                $('#edit_designer').slideUp();
            }
            total_budget = total_budget + parseInt(value);
        });
        $('#budget').val(parseInt(total_budget));
        $('#assign_designer').css('visibility','visible');
        $('#assign_designer').slideDown();
        $('#edit_designer').slideUp();
    }
    
    var days = 0;
    var extra = 0;
    var total_hours = $('#budget').val();
    days = total_hours / 8;
    extra = total_hours % 8;
    if(days > 1 && extra > 0)
        days = parseInt(days + 1);
    if(days > 1)
    {
      $('#to_date').val(currDate(days-1));
    }
    else{
      $('#to_date').val(currDate(0));
    }
    
    if($('#to_date').val() > $('#temp_indeadline').val())
    {
        $('#'+id).css("box-shadow","0px 0px 4px #ed1c26");
        $('#assign_designer').css('visibility','hidden');
    }
    
    if($('#budget').val() == '0')
    {
        $('#assign_designer').css('visibility','hidden');
    }
}


function calculateHours2(task_hour,id){
    if(task_hour == '' || isNaN(task_hour))
    {
        $('#'+id).val("0");
        var total_budget = 0;
        $(".budget_inp").each(function(){
            total_budget = total_budget + parseInt($(this).val());
        });
        $('#budget').val(parseInt(total_budget));
    }
    
    if((task_hour !== null & task_hour !== '') && !isNaN(task_hour))
    {
        var total_budget = 0;
        $(".budget_inp").each(function(){
            var value = $(this).val();
            if(value == '' || isNaN(value))
            {
                $(this).val("0");
                value = 0;
            }
            if(value != 0)
            {
                $(this).css("box-shadow","0px 0px 4px #4caf50");
            }
            total_budget = total_budget + parseInt(value);
        });
        $('#budget').val(parseInt(total_budget));
    }
    
    var days = 0;
    var extra = 0;
    var total_hours = $('#budget').val();
    days = total_hours / 8;
    extra = total_hours % 8;
    if(days > 1 && extra > 0)
        days = parseInt(days + 1);
    if(days > 1)
    {
      $('#to_date').val(currDate(days-1));
    }
    else{
      $('#to_date').val(currDate(0));
    }
    
    if($('#to_date').val() > $('#temp_indeadline').val())
    {
        $('#'+id).css("box-shadow","0px 0px 4px #ed1c26");
        $('#assign_designer').css('visibility','hidden');
    }
    
    if($('#budget').val() == '0')
    {
        $('#assign_designer').css('visibility','hidden');
    }
}

function clearValue(id){
    var value = $('#'+id).val();
        $('#'+id).css("box-shadow","none");
    
    if(value == "0" || isNaN(value))
    {
        $('#'+id).val("");
    }
    
    if(value === '')
    {
        $('#'+id).val("0");
        $(".budget_inp").each(function(){
        var value = $(this).val();
        if(value == '' || isNaN(value))
        {
            $(this).val("0");
        $('#'+id).css("box-shadow","none");
        }
    });
    }
}

function currDate(day){
    var selected_date = $('#from_date').val();
    var today = new Date(selected_date);
    var selected_day = today.getDate();
    var selected_month = today.getMonth()+1;
    var selected_year = today.getFullYear();
    //get total of days 
    var total_days = daysInMonth(selected_month,selected_year);
    
    if(day > 0)
    {
        selected_day += day;
    }

    while(selected_day > total_days)
    {
        selected_day = selected_day - total_days;
        selected_month += 1;
    }
    
    while(selected_month > 12)
    {
        selected_month = selected_month - 12;
        selected_year += 1;
    }
    
    if(selected_day < 10)
        selected_day = "0"+selected_day;
    if(selected_month < 10)
        selected_month = "0"+selected_month;
    
    var saturdays = getSaturdays(selected_year,selected_month);
    var sundays = getSundays(selected_year,selected_month);
    var count = 0;
    for(count = 0; count < saturdays.length; count++)
    {
        if(selected_day === saturdays[count])
        {
            selected_day += 2;
            if(selected_day > total_days)
            {
                selected_day = total_days;
                if(selected_day === saturdays[count])
                {
                    selected_day -= 1;
                }
            }
            //alert(selected_day + " - saturday " + saturdays[count]);
        }
    }
    for(count = 0; count < sundays.length; count++)
    {
        if(selected_day === sundays[count])
        {
            selected_day += 1;
            if(selected_day > total_days)
            {
                selected_day = total_days;
                if(selected_day === sundays[count])
                {
                    selected_day -= 2;
                }
            }
            //alert(selected_day + " - sunday " + sundays[count]);
        }
    }
    
    
    return selected_year + "-"+selected_month+ "-"+selected_day; 
}

function daysInMonth(month,year) {
  return new Date(year, month, 0).getDate();
}


function dateToday(){
  var now = new Date();

  var day = ("0" + now.getDate()).slice(-2);
  var month = ("0" + (now.getMonth() + 1)).slice(-2);

  var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
  
  return today;
}

function draggableMe(id){
    var draggable = $('#'+id); //element

    draggable.on('mousedown', function (e) {
            e.stopPropagation();
            var dr = $(this).addClass("drag").css("cursor", "move");
            height = dr.outerHeight();
            width = dr.outerWidth();
            ypos = dr.offset().top + height - e.pageY,
                    xpos = dr.offset().left + width - e.pageX;
            $(document.body).on('mousemove', function (e) {
                    var itop = e.pageY + ypos - height;
                    var ileft = e.pageX + xpos - width;
                    if (dr.hasClass("drag")) {
                            dr.offset({ top: itop, left: ileft });
                    }
            }).on('mouseup', function (e) {
                    dr.removeClass("drag");
            });
    });
}

function placeMeCursor(triggered,e){
        var element = $('#'+triggered).height();
        $('#'+triggered).css({top: e.pageY - element, left: e.pageX});
}

function chooseImg(id) {
    document.getElementById(id).src = URL.createObjectURL(event.target.files[0]);
}