var sundays = new Array();
var saturdays = new Array();

function addToArrSun(day) {
	sundays.push(day);
}
function addToArrSat(day) {
	saturdays.push(day);
}

function clearArray() {
    saturdays = new Array();
    sundays = new Array();
}

var selectedDay;
var selectedMonth;
var selectedYear;
// para macheck kung anong date yung sinelect

function setSelected(id, mm, yyyy) {
    var day = id;
    var month = mm;
    var year = yyyy;
    selectedDay = day;
    selectedMonth = month;
    selectedYear = year;
}

var date_diff_indays = function (date1, date2) {
    dt1 = new Date(date1);//date of input
    dt2 = new Date(date2);//external deadline
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
}

// INTERNAL DEADLINES
// DATA ARE STATIC ONLY NOT RESPONSIVE

// COMPUTATION FOR C

function COut() {
	var today = new Date();
	var dd = parseInt(selectedDay); // day selected
	var dayToday = parseInt(today.getDate()); // current day
	var monthToday = parseInt(today.getMonth() + 1); //current month
	var yyyy = parseInt(today.getFullYear()); // current year
	var diff = (date_diff_indays(monthToday + '/' + dayToday + '/' + yyyy, selectedMonth + '/' + selectedDay + '/' + selectedYear));
	var internalDead; // value ng internal deadline
	var day;
	var month;

	//for sundays array length
	var sundaylen = sundays.length;
	//for saturdays array length
	var saturdaylen = saturdays.length;

	// compute the internal deadline
	if (diff <= 6) {
		res = dd - 1; // if deadline is 3 days ahead from the current date subtract 1 for the internal deadline
	}
	else if (diff <= 9) {
		res = dd - 2; // if deadline is 7 days ahead from the current date subtract 2 for the internal deadline
	}
	else if (diff <= 13) {
		res = dd - 3; // if deadline is 14 days ahead from the current date subtract 4 for the internal deadline
	}
	else if (diff <= 16) {
		res = dd - 4; // if deadline is 21 days ahead from the current date subtract 6 for the internal deadline
	}
	else if (diff <= 20) {
		res = dd - 5; // if deadline is 28 days ahead from the current date subtract 8 for the internal deadline
	}
	else if (diff <= 23) {
		res = dd - 6; // if deadline is 45 days ahead from the current date subtract 12 for the internal deadline
	}
	else if (diff <= 27) {
		res = dd - 7; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 30) {
		res = dd - 8; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 34) {
		res = dd - 9; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 37) {
		res = dd - 10; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 41) {
		res = dd - 11; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 44) {
		res = dd - 12; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 48) {
		res = dd - 13; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 51) {
		res = dd - 14; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 55) {
		res = dd - 15; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 58) {
		res = dd - 16; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 62) {
		res = dd - 17; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 66) {
		res = dd - 18; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 69) {
		res = dd - 19; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff >= 70) {
		res = dd - 20; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}

	var mm = document.getElementById('month').value;

	if (res < 10) {
		if (res <= 0)
			res = 1;
		res = '0' + res; // para hindi single digit yung lalabas
	}
	if (mm < 10) {
		mm = '0' + mm; // para hindi single digit yung lalabas
	}
	//adjust dayToday and res for this variables not to fall in sundays
	for (count = 0; count < sundaylen; count++) {
		if (dayToday == sundays[count]) {
			dayToday -= 2;
		}
		if (res == sundays[count]) {
			res -= 2;
		}
	}
	//adjust dayToday and res for this variables not to fall in saturdays
	for (count = 0; count < saturdaylen; count++) {
		if (dayToday == saturdays[count]) {
			dayToday -= 1;
		}
		if (res == saturdays[count]) {
			res -= 1;
			if (res <= 0)
				res = 3;
		}
	}
	// to check para hindi mapunta sa past lol
	if (res <= dayToday) {
		if (mm > monthToday) //if selected month is greater than or lapse the current month
		{
			day = res;
			month = mm;
		}
		else {
			day = dayToday;
			month = mm;
		}
		internalDead = yyyy + '-' + mm + '-' + day;
		//alert(res + " "+ today.getMonth() + 1 );
	}
	else {
		internalDead = yyyy + '-' + mm + '-' + res;
		day = res;
		month = mm;
	}

	if (document.getElementById('tradeC').checked == true) {
		document.getElementById("C_in_dead").value = internalDead;
		document.getElementById('Cinday').value = day;
		document.getElementById('Cinmonth').value = month;
	}
	else {
		document.getElementById('C_in_dead').value = "";
		document.getElementById('Cinday').value = "";
		document.getElementById('Cinmonth').value = "";
	}
}

// COMPUTATION FOR E
function EOut() {
	var today = new Date();
	var dd = parseInt(selectedDay); // day selected
	var dayToday = parseInt(today.getDate()); // current day
	var monthToday = parseInt(today.getMonth() + 1); //current month
	var yyyy = parseInt(today.getFullYear()); // current year
	var diff = (date_diff_indays(monthToday + '/' + dayToday + '/' + yyyy, selectedMonth + '/' + selectedDay + '/' + selectedYear));
	var internalDead; // value ng internal deadline
	var day;
	var month;

	//for sundays array length
	var sundaylen = sundays.length;
	//for saturdays array length
	var saturdaylen = saturdays.length;

	// compute the internal deadline
	if (diff <= 6) {
		res = dd - 1; // if deadline is 3 days ahead from the current date subtract 1 for the internal deadline
	}
	else if (diff <= 9) {
		res = dd - 2; // if deadline is 7 days ahead from the current date subtract 2 for the internal deadline
	}
	else if (diff <= 13) {
		res = dd - 3; // if deadline is 14 days ahead from the current date subtract 4 for the internal deadline
	}
	else if (diff <= 16) {
		res = dd - 4; // if deadline is 21 days ahead from the current date subtract 6 for the internal deadline
	}
	else if (diff <= 20) {
		res = dd - 5; // if deadline is 28 days ahead from the current date subtract 8 for the internal deadline
	}
	else if (diff <= 23) {
		res = dd - 6; // if deadline is 45 days ahead from the current date subtract 12 for the internal deadline
	}
	else if (diff <= 27) {
		res = dd - 7; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 30) {
		res = dd - 8; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 34) {
		res = dd - 9; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 37) {
		res = dd - 10; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 41) {
		res = dd - 11; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 44) {
		res = dd - 12; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 48) {
		res = dd - 13; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 51) {
		res = dd - 14; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 55) {
		res = dd - 15; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 58) {
		res = dd - 16; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 62) {
		res = dd - 17; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 66) {
		res = dd - 18; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 69) {
		res = dd - 19; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff >= 70) {
		res = dd - 20; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}

	var mm = document.getElementById('month').value;

	if (res < 10) {
		if (res <= 0)
			res = 1;
		res = '0' + res; // para hindi single digit yung lalabas
	}
	if (mm < 10) {
		mm = '0' + mm; // para hindi single digit yung lalabas
	}
	//adjust dayToday and res for this variables not to fall in sundays
	for (count = 0; count < sundaylen; count++) {
		if (dayToday == sundays[count]) {
			dayToday -= 2;
		}
		if (res == sundays[count]) {
			res -= 2;
		}
	}
	//adjust dayToday and res for this variables not to fall in saturdays
	for (count = 0; count < saturdaylen; count++) {
		if (dayToday == saturdays[count]) {
			dayToday -= 1;
		}
		if (res == saturdays[count]) {
			res -= 1;
			if (res <= 0)
				res = 3;
		}
	}
	// to check para hindi mapunta sa past lol
	if (res <= dayToday) {
		if (mm > monthToday) //if selected month is greater than or lapse the current month
		{
			day = res;
			month = mm;
		}
		else {
			day = dayToday;
			month = mm;
		}
		internalDead = yyyy + '-' + mm + '-' + day;
		//alert(res + " "+ today.getMonth() + 1 );
	}
	else {
		internalDead = yyyy + '-' + mm + '-' + res;
		day = res;
		month = mm;
	}

	if (document.getElementById('tradeE').checked == true) {
		document.getElementById("E_in_dead").value = internalDead;
		document.getElementById('Einday').value = day;
		document.getElementById('Einmonth').value = month;
	}
	else {
		document.getElementById('E_in_dead').value = "";
		document.getElementById('Einday').value = "";
		document.getElementById('Einmonth').value = "";
	}
}

// COMPUTATION FOR A
function AOut() {
	var today = new Date();
	var dd = parseInt(selectedDay); // day selected
	var dayToday = parseInt(today.getDate()); // current day
	var monthToday = parseInt(today.getMonth() + 1); //current month
	var yyyy = parseInt(today.getFullYear()); // current year
	var diff = (date_diff_indays(monthToday + '/' + dayToday + '/' + yyyy, selectedMonth + '/' + selectedDay + '/' + selectedYear));
	var internalDead; // value ng internal deadline
	var day;
	var month;

	//for sundays array length
	var sundaylen = sundays.length;
	//for saturdays array length
	var saturdaylen = saturdays.length;

	// compute the internal deadline
	if (diff <= 6) {
		res = dd - 2; // if deadline is 3 days ahead from the current date subtract 1 for the internal deadline
	}
	else if (diff <= 9) {
		res = dd - 5; // if deadline is 7 days ahead from the current date subtract 2 for the internal deadline
	}
	else if (diff <= 13) {
		res = dd - 7; // if deadline is 14 days ahead from the current date subtract 4 for the internal deadline
	}
	else if (diff <= 16) {
		res = dd - 9; // if deadline is 21 days ahead from the current date subtract 6 for the internal deadline
	}
	else if (diff <= 20) {
		res = dd - 12; // if deadline is 28 days ahead from the current date subtract 8 for the internal deadline
	}
	else if (diff <= 23) {
		res = dd - 15; // if deadline is 45 days ahead from the current date subtract 12 for the internal deadline
	}
	else if (diff <= 27) {
		res = dd - 18; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 30) {
		res = dd - 20; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 34) {
		res = dd - 23; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 37) {
		res = dd - 25; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 41) {
		res = dd - 28; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 44) {
		res = dd - 30; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 48) {
		res = dd - 33; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 51) {
		res = dd - 35; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 55) {
		res = dd - 38; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 58) {
		res = dd - 40; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 62) {
		res = dd - 43; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 66) {
		res = dd - 45; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 69) {
		res = dd - 48; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff >= 70) {
		res = dd - 50; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}

	var mm = document.getElementById('month').value;

	if (res < 10) {
		if (res <= 0)
			res = 1;
		res = '0' + res; // para hindi single digit yung lalabas
	}
	if (mm < 10) {
		mm = '0' + mm; // para hindi single digit yung lalabas
	}
	//adjust dayToday and res for this variables not to fall in sundays
	for (count = 0; count < sundaylen; count++) {
		if (dayToday == sundays[count]) {
			dayToday -= 2;
		}
		if (res == sundays[count]) {
			res -= 2;
		}
	}
	//adjust dayToday and res for this variables not to fall in saturdays
	for (count = 0; count < saturdaylen; count++) {
		if (dayToday == saturdays[count]) {
			dayToday -= 1;
		}
		if (res == saturdays[count]) {
			res -= 1;
			if (res <= 0)
				res = 3;
		}
	}
	// to check para hindi mapunta sa past lol
	if (res <= dayToday) {
		if (mm > monthToday) //if selected month is greater than or lapse the current month
		{
			day = res;
			month = mm;
		}
		else {
			day = dayToday;
			month = mm;
		}
		internalDead = yyyy + '-' + mm + '-' + day;
		//alert(res + " "+ today.getMonth() + 1 );
	}
	else {
		internalDead = yyyy + '-' + mm + '-' + res;
		day = res;
		month = mm;
	}

	if (document.getElementById('tradeA').checked == true) {
		document.getElementById("A_in_dead").value = internalDead;
		document.getElementById('Ainday').value = day;
		document.getElementById('Ainmonth').value = month;
	}
	else {
		document.getElementById('A_in_dead').value = "";
		document.getElementById('Ainday').value = "";
		document.getElementById('Ainmonth').value = "";
	}
}

// COMPUTATION FOR AU
function AUOut() {
	var today = new Date();
	var dd = parseInt(selectedDay); // day selected
	var dayToday = parseInt(today.getDate()); // current day
	var monthToday = parseInt(today.getMonth() + 1); //current month
	var yyyy = parseInt(today.getFullYear()); // current year
	var diff = (date_diff_indays(monthToday + '/' + dayToday + '/' + yyyy, selectedMonth + '/' + selectedDay + '/' + selectedYear));
	var internalDead; // value ng internal deadline
	var day;
	var month;

	//for sundays array length
	var sundaylen = sundays.length;
	//for saturdays array length
	var saturdaylen = saturdays.length;

	// compute the internal deadline
	if (diff <= 6) {
		res = dd - 1; // if deadline is 3 days ahead from the current date subtract 1 for the internal deadline
	}
	else if (diff <= 9) {
		res = dd - 2; // if deadline is 7 days ahead from the current date subtract 2 for the internal deadline
	}
	else if (diff <= 13) {
		res = dd - 3; // if deadline is 14 days ahead from the current date subtract 4 for the internal deadline
	}
	else if (diff <= 16) {
		res = dd - 4; // if deadline is 21 days ahead from the current date subtract 6 for the internal deadline
	}
	else if (diff <= 20) {
		res = dd - 5; // if deadline is 28 days ahead from the current date subtract 8 for the internal deadline
	}
	else if (diff <= 23) {
		res = dd - 6; // if deadline is 45 days ahead from the current date subtract 12 for the internal deadline
	}
	else if (diff <= 27) {
		res = dd - 7; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 30) {
		res = dd - 8; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 34) {
		res = dd - 9; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 37) {
		res = dd - 10; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 41) {
		res = dd - 11; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 44) {
		res = dd - 12; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 48) {
		res = dd - 13; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 51) {
		res = dd - 14; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 55) {
		res = dd - 15; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 58) {
		res = dd - 16; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 62) {
		res = dd - 17; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 66) {
		res = dd - 18; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 69) {
		res = dd - 19; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff >= 70) {
		res = dd - 20; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}

	var mm = document.getElementById('month').value;

	if (res < 10) {
		if (res <= 0)
			res = 1;
		res = '0' + res; // para hindi single digit yung lalabas
	}
	if (mm < 10) {
		mm = '0' + mm; // para hindi single digit yung lalabas
	}
	//adjust dayToday and res for this variables not to fall in sundays
	for (count = 0; count < sundaylen; count++) {
		if (dayToday == sundays[count]) {
			dayToday -= 2;
		}
		if (res == sundays[count]) {
			res -= 2;
		}
	}
	//adjust dayToday and res for this variables not to fall in saturdays
	for (count = 0; count < saturdaylen; count++) {
		if (dayToday == saturdays[count]) {
			dayToday -= 1;
		}
		if (res == saturdays[count]) {
			res -= 1;
			if (res <= 0)
				res = 3;
		}
	}
	// to check para hindi mapunta sa past lol
	if (res <= dayToday) {
		if (mm > monthToday) //if selected month is greater than or lapse the current month
		{

			day = res;
			month = mm;
		}
		else {
			day = dayToday;
			month = mm;
		}
		internalDead = yyyy + '-' + mm + '-' + day;
		//alert(res + " "+ today.getMonth() + 1 );
	}
	else {
		internalDead = yyyy + '-' + mm + '-' + res;
		day = res;
		month = mm;
	}

	if (document.getElementById('tradeAU').checked == true) {
		document.getElementById("AU_in_dead").value = internalDead;
		document.getElementById('AUinday').value = day;
		document.getElementById('AUinmonth').value = month;
	}
	else {
		document.getElementById('AU_in_dead').value = "";
		document.getElementById('AUinday').value = "";
		document.getElementById('AUinmonth').value = "";
	}
}

// COMPUTATION FOR P
function POut() {
	var today = new Date();
	var dd = parseInt(selectedDay); // day selected
	var dayToday = parseInt(today.getDate()); // current day
	var monthToday = parseInt(today.getMonth() + 1); //current month
	var yyyy = parseInt(today.getFullYear()); // current year
	var diff = (date_diff_indays(monthToday + '/' + dayToday + '/' + yyyy, selectedMonth + '/' + selectedDay + '/' + selectedYear));
	var internalDead; // value ng internal deadline
	var day;
	var month;

	//for sundays array length
	var sundaylen = sundays.length;
	//for saturdays array length
	var saturdaylen = saturdays.length;

	// compute the internal deadline
	if (diff <= 6) {
		res = dd - 1; // if deadline is 3 days ahead from the current date subtract 1 for the internal deadline
	}
	else if (diff <= 9) {
		res = dd - 3; // if deadline is 7 days ahead from the current date subtract 2 for the internal deadline
	}
	else if (diff <= 13) {
		res = dd - 5; // if deadline is 14 days ahead from the current date subtract 4 for the internal deadline
	}
	else if (diff <= 16) {
		res = dd - 6; // if deadline is 21 days ahead from the current date subtract 6 for the internal deadline
	}
	else if (diff <= 20) {
		res = dd - 8; // if deadline is 28 days ahead from the current date subtract 8 for the internal deadline
	}
	else if (diff <= 23) {
		res = dd - 9; // if deadline is 45 days ahead from the current date subtract 12 for the internal deadline
	}
	else if (diff <= 27) {
		res = dd - 11; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 30) {
		res = dd - 12; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 34) {
		res = dd - 14; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 37) {
		res = dd - 15; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 41) {
		res = dd - 17; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 44) {
		res = dd - 18; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 48) {
		res = dd - 20; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 51) {
		res = dd - 21; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 55) {
		res = dd - 23; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 58) {
		res = dd - 24; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 62) {
		res = dd - 26; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 66) {
		res = dd - 27; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 69) {
		res = dd - 29; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff >= 70) {
		res = dd - 30; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}

	var mm = document.getElementById('month').value;

	if (res < 10) {
		if (res <= 0)
			res = 1;
		res = '0' + res; // para hindi single digit yung lalabas
	}
	if (mm < 10) {
		mm = '0' + mm; // para hindi single digit yung lalabas
	}
	//adjust dayToday and res for this variables not to fall in sundays
	for (count = 0; count < sundaylen; count++) {
		if (dayToday == sundays[count]) {
			dayToday -= 2;
		}
		if (res == sundays[count]) {
			res -= 2;
		}
	}
	//adjust dayToday and res for this variables not to fall in saturdays
	for (count = 0; count < saturdaylen; count++) {
		if (dayToday == saturdays[count]) {
			dayToday -= 1;
		}
		if (res == saturdays[count]) {
			res -= 1;
			if (res <= 0)
				res = 3;
		}
	}
	// to check para hindi mapunta sa past lol
	if (res <= dayToday) {
		if (mm > monthToday) //if selected month is greater than or lapse the current month
		{
			day = res;
			month = mm;
		}
		else {
			day = dayToday;
			month = mm;
		}
		internalDead = yyyy + '-' + mm + '-' + day;
		//alert(res + " "+ today.getMonth() + 1 );
	}
	else {

		internalDead = yyyy + '-' + mm + '-' + res;
		day = res;
		month = mm;
	}

	if (document.getElementById('tradeP').checked == true) {
		document.getElementById("P_in_dead").value = internalDead;
		document.getElementById('Pinday').value = day;
		document.getElementById('Pinmonth').value = month;
	}
	else {
		document.getElementById('P_in_dead').value = "";
		document.getElementById('Pinday').value = "";
		document.getElementById('Pinmonth').value = "";
	}
}

// COMPUTATION FOR M
function MOut() {
	var today = new Date();
	var dd = parseInt(selectedDay); // day selected
	var dayToday = parseInt(today.getDate()); // current day
	var monthToday = parseInt(today.getMonth() + 1); //current month
	var yyyy = parseInt(today.getFullYear()); // current year
	var diff = (date_diff_indays(monthToday + '/' + dayToday + '/' + yyyy, selectedMonth + '/' + selectedDay + '/' + selectedYear));
	var internalDead; // value ng internal deadline
	var day;
	var month;

	//for sundays array length
	var sundaylen = sundays.length;
	//for saturdays array length
	var saturdaylen = saturdays.length;

	// compute the internal deadline
	if (diff <= 6) {
		res = dd - 1; // if deadline is 3 days ahead from the current date subtract 1 for the internal deadline
	}
	else if (diff <= 9) {
		res = dd - 3; // if deadline is 7 days ahead from the current date subtract 2 for the internal deadline
	}
	else if (diff <= 13) {
		res = dd - 5; // if deadline is 14 days ahead from the current date subtract 4 for the internal deadline
	}
	else if (diff <= 16) {
		res = dd - 6; // if deadline is 21 days ahead from the current date subtract 6 for the internal deadline
	}
	else if (diff <= 20) {
		res = dd - 8; // if deadline is 28 days ahead from the current date subtract 8 for the internal deadline
	}
	else if (diff <= 23) {
		res = dd - 9; // if deadline is 45 days ahead from the current date subtract 12 for the internal deadline
	}
	else if (diff <= 27) {
		res = dd - 11; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 30) {
		res = dd - 12; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 34) {
		res = dd - 14; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 37) {
		res = dd - 15; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 41) {
		res = dd - 17; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 44) {
		res = dd - 18; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 48) {
		res = dd - 20; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 51) {
		res = dd - 21; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 55) {
		res = dd - 23; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 58) {
		res = dd - 24; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 62) {
		res = dd - 26; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 66) {
		res = dd - 27; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 69) {
		res = dd - 29; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff >= 70) {
		res = dd - 30; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}

	var mm = document.getElementById('month').value;

	if (res < 10) {
		if (res <= 0)
			res = 1;
		res = '0' + res; // para hindi single digit yung lalabas
	}
	if (mm < 10) {
		mm = '0' + mm; // para hindi single digit yung lalabas
	}
	//adjust dayToday and res for this variables not to fall in sundays
	for (count = 0; count < sundaylen; count++) {
		if (dayToday == sundays[count]) {
			dayToday -= 2;
		}
		if (res == sundays[count]) {
			res -= 2;
		}
	}
	//adjust dayToday and res for this variables not to fall in saturdays
	for (count = 0; count < saturdaylen; count++) {
		if (dayToday == saturdays[count]) {
			dayToday -= 1;
		}
		if (res == saturdays[count]) {
			res -= 1;
			if (res <= 0)
				res = 3;
		}
	}
	// to check para hindi mapunta sa past lol
	if (res <= dayToday) {
		if (mm > monthToday) //if selected month is greater than or lapse the current month
		{
			day = res;
			month = mm;
		}
		else {
			day = dayToday;
			month = mm;
		}
		internalDead = yyyy + '-' + mm + '-' + day;
		//alert(res + " "+ today.getMonth() + 1 );
	}
	else {
		internalDead = yyyy + '-' + mm + '-' + res;
		day = res;
		month = mm;
	}

	if (document.getElementById('tradeM').checked == true) {
		document.getElementById("M_in_dead").value = internalDead;
		document.getElementById('Minday').value = day;
		document.getElementById('Minmonth').value = month;
	}
	else {
		document.getElementById('M_in_dead').value = "";
		document.getElementById('Minday').value = "";
		document.getElementById('Minmonth').value = "";
	}
}

// COMPUTATION FOR FP
function FPOut() {
	var today = new Date();
	var dd = parseInt(selectedDay); // day selected
	var dayToday = parseInt(today.getDate()); // current day
	var monthToday = parseInt(today.getMonth() + 1); //current month
	var yyyy = parseInt(today.getFullYear()); // current year
	var diff = (date_diff_indays(monthToday + '/' + dayToday + '/' + yyyy, selectedMonth + '/' + selectedDay + '/' + selectedYear));
	var internalDead; // value ng internal deadline
	var day;
	var month;

	//for sundays array length
	var sundaylen = sundays.length;
	//for saturdays array length
	var saturdaylen = saturdays.length;

	// compute the internal deadline
	if (diff <= 6) {
		res = dd - 1; // if deadline is 3 days ahead from the current date subtract 1 for the internal deadline
	}
	else if (diff <= 9) {
		res = dd - 3; // if deadline is 7 days ahead from the current date subtract 2 for the internal deadline
	}
	else if (diff <= 13) {
		res = dd - 5; // if deadline is 14 days ahead from the current date subtract 4 for the internal deadline
	}
	else if (diff <= 16) {
		res = dd - 6; // if deadline is 21 days ahead from the current date subtract 6 for the internal deadline
	}
	else if (diff <= 20) {
		res = dd - 8; // if deadline is 28 days ahead from the current date subtract 8 for the internal deadline
	}
	else if (diff <= 23) {
		res = dd - 9; // if deadline is 45 days ahead from the current date subtract 12 for the internal deadline
	}
	else if (diff <= 27) {
		res = dd - 11; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 30) {
		res = dd - 12; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 34) {
		res = dd - 14; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 37) {
		res = dd - 15; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 41) {
		res = dd - 17; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 44) {
		res = dd - 18; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 48) {
		res = dd - 20; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 51) {
		res = dd - 21; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 55) {
		res = dd - 23; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 58) {
		res = dd - 24; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 62) {
		res = dd - 26; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 66) {
		res = dd - 27; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 69) {
		res = dd - 29; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff >= 70) {
		res = dd - 30; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}

	var mm = document.getElementById('month').value;

	if (res < 10) {
		if (res <= 0)
			res = 1;
		res = '0' + res; // para hindi single digit yung lalabas
	}
	if (mm < 10) {
		mm = '0' + mm; // para hindi single digit yung lalabas
	}
	//adjust dayToday and res for this variables not to fall in sundays
	for (count = 0; count < sundaylen; count++) {
		if (dayToday == sundays[count]) {
			dayToday -= 2;
		}
		if (res == sundays[count]) {
			res -= 2;
		}
	}
	//adjust dayToday and res for this variables not to fall in saturdays
	for (count = 0; count < saturdaylen; count++) {
		if (dayToday == saturdays[count]) {
			dayToday -= 1;
		}
		if (res == saturdays[count]) {
			res -= 1;
			if (res <= 0)
				res = 3;
		}
	}
	// to check para hindi mapunta sa past lol
	if (res <= dayToday) {
		if (mm > monthToday) //if selected month is greater than or lapse the current month
		{
			day = res;
			month = mm;
		}
		else {
			day = dayToday;
			month = mm;
		}
		internalDead = yyyy + '-' + mm + '-' + day;
		//alert(res + " "+ today.getMonth() + 1 );
	}
	else {
		internalDead = yyyy + '-' + mm + '-' + res;
		day = res;
		month = mm;
	}

	if (document.getElementById('tradeFP').checked == true) {
		document.getElementById("FP_in_dead").value = internalDead;
		document.getElementById('FPinday').value = day;
		document.getElementById('FPinmonth').value = month;
	}
	else {
		document.getElementById('FP_in_dead').value = "";
		document.getElementById('FPinday').value = "";
		document.getElementById('FPinmonth').value = "";
	}
}

// COMPUTATION FOR S
function SOut() {
	var today = new Date();
	var dd = parseInt(selectedDay); // day selected
	var dayToday = parseInt(today.getDate()); // current day
	var monthToday = parseInt(today.getMonth() + 1); //current month
	var yyyy = parseInt(today.getFullYear()); // current year
	var diff = (date_diff_indays(monthToday + '/' + dayToday + '/' + yyyy, selectedMonth + '/' + selectedDay + '/' + selectedYear));
	var internalDead; // value ng internal deadline
	var day;
	var month;

	//for sundays array length
	var sundaylen = sundays.length;
	//for saturdays array length
	var saturdaylen = saturdays.length;

	// compute the internal deadline
	if (diff <= 6) {
		res = dd - 1; // if deadline is 3 days ahead from the current date subtract 1 for the internal deadline
	}
	else if (diff <= 9) {
		res = dd - 2; // if deadline is 7 days ahead from the current date subtract 2 for the internal deadline
	}
	else if (diff <= 13) {
		res = dd - 3; // if deadline is 14 days ahead from the current date subtract 4 for the internal deadline
	}
	else if (diff <= 16) {
		res = dd - 4; // if deadline is 21 days ahead from the current date subtract 6 for the internal deadline
	}
	else if (diff <= 20) {
		res = dd - 5; // if deadline is 28 days ahead from the current date subtract 8 for the internal deadline
	}
	else if (diff <= 23) {
		res = dd - 6; // if deadline is 45 days ahead from the current date subtract 12 for the internal deadline
	}
	else if (diff <= 27) {
		res = dd - 7; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 30) {
		res = dd - 8; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 34) {
		res = dd - 9; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 37) {
		res = dd - 10; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 41) {
		res = dd - 11; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 44) {
		res = dd - 12; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 48) {
		res = dd - 13; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 51) {
		res = dd - 14; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 55) {
		res = dd - 15; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 58) {
		res = dd - 16; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 62) {
		res = dd - 17; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 66) {
		res = dd - 18; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 69) {
		res = dd - 19; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff >= 70) {
		res = dd - 20; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}

	var mm = document.getElementById('month').value;

	if (res < 10) {
		if (res <= 0)
			res = 1;
		res = '0' + res; // para hindi single digit yung lalabas
	}
	if (mm < 10) {
		mm = '0' + mm; // para hindi single digit yung lalabas
	}
	//adjust dayToday and res for this variables not to fall in sundays
	for (count = 0; count < sundaylen; count++) {
		if (dayToday == sundays[count]) {
			dayToday -= 2;
		}
		if (res == sundays[count]) {
			res -= 2;
		}
	}
	//adjust dayToday and res for this variables not to fall in saturdays
	for (count = 0; count < saturdaylen; count++) {
		if (dayToday == saturdays[count]) {
			dayToday -= 1;
		}
		if (res == saturdays[count]) {
			res -= 1;
			if (res <= 0)
				res = 3;
		}
	}
	// to check para hindi mapunta sa past lol
	if (res <= dayToday) {
		if (mm > monthToday) //if selected month is greater than or lapse the current month
		{
			day = res;
			month = mm;
		}
		else {
			day = dayToday;
			month = mm;
		}
		internalDead = yyyy + '-' + mm + '-' + day;
		//alert(res + " "+ today.getMonth() + 1 );
	}
	else {
		internalDead = yyyy + '-' + mm + '-' + res;
		day = res;
		month = mm;
	}

	if (document.getElementById('tradeS').checked == true) {
		document.getElementById("S_in_dead").value = internalDead;
		document.getElementById('Sinday').value = day;
		document.getElementById('Sinmonth').value = month;
	}
	else {
		document.getElementById('S_in_dead').value = "";
		document.getElementById('Sinday').value = "";
		document.getElementById('Sinmonth').value = "";
	}
}

// COMPUTATION FOR QS
function QSOut() {
	var today = new Date();
	var dd = parseInt(selectedDay); // day selected
	var dayToday = parseInt(today.getDate()); // current day
	var monthToday = parseInt(today.getMonth() + 1); //current month
	var yyyy = parseInt(today.getFullYear()); // current year
	var diff = (date_diff_indays(monthToday + '/' + dayToday + '/' + yyyy, selectedMonth + '/' + selectedDay + '/' + selectedYear));
	var internalDead; // value ng internal deadline
	var day;
	var month;

	//for sundays array length
	var sundaylen = sundays.length;
	//for saturdays array length
	var saturdaylen = saturdays.length;

	// compute the internal deadline
	if (diff <= 6) {
		res = dd - 1; // if deadline is 3 days ahead from the current date subtract 1 for the internal deadline
	}
	else if (diff <= 9) {
		res = dd - 2; // if deadline is 7 days ahead from the current date subtract 2 for the internal deadline
	}
	else if (diff <= 13) {
		res = dd - 3; // if deadline is 14 days ahead from the current date subtract 4 for the internal deadline
	}
	else if (diff <= 16) {
		res = dd - 4; // if deadline is 21 days ahead from the current date subtract 6 for the internal deadline
	}
	else if (diff <= 20) {
		res = dd - 5; // if deadline is 28 days ahead from the current date subtract 8 for the internal deadline
	}
	else if (diff <= 23) {
		res = dd - 6; // if deadline is 45 days ahead from the current date subtract 12 for the internal deadline
	}
	else if (diff <= 27) {
		res = dd - 7; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 30) {
		res = dd - 8; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 34) {
		res = dd - 9; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 37) {
		res = dd - 10; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 41) {
		res = dd - 11; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 44) {
		res = dd - 12; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 48) {
		res = dd - 13; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 51) {
		res = dd - 14; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 55) {
		res = dd - 15; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 58) {
		res = dd - 16; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 62) {
		res = dd - 17; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 66) {
		res = dd - 18; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff <= 69) {
		res = dd - 19; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}
	else if (diff >= 70) {
		res = dd - 20; // if deadline is 60 days ahead from the current date subtract 16 for the internal deadline
	}

	var mm = document.getElementById('month').value;

	if (res < 10) {
		if (res <= 0)
			res = 1;
		res = '0' + res; // para hindi single digit yung lalabas
	}
	if (mm < 10) {
		mm = '0' + mm; // para hindi single digit yung lalabas
	}
	//adjust dayToday and res for this variables not to fall in sundays
	for (count = 0; count < sundaylen; count++) {
		if (dayToday == sundays[count]) {
			dayToday -= 2;
		}
		if (res == sundays[count]) {
			res -= 2;
		}
	}
	//adjust dayToday and res for this variables not to fall in saturdays
	for (count = 0; count < saturdaylen; count++) {
		if (dayToday == saturdays[count]) {
			dayToday -= 1;
		}
		if (res == saturdays[count]) {
			res -= 1;
			if (res <= 0)
				res = 3;
		}
	}
	// to check para hindi mapunta sa past lol
	if (res <= dayToday) {
		if (mm > monthToday) //if selected month is greater than or lapse the current month
		{
			day = res;
			month = mm;
		}
		else {
			day = dayToday;
			month = mm;
		}
		internalDead = yyyy + '-' + mm + '-' + day;
		//alert(res + " "+ today.getMonth() + 1 );
	}
	else {
		internalDead = yyyy + '-' + mm + '-' + res;
		day = res;
		month = mm;
	}

	if (document.getElementById('tradeQS').checked == true) {
		document.getElementById("QS_in_dead").value = internalDead;
		document.getElementById('QSinday').value = day;
		document.getElementById('QSinmonth').value = month;
	}
	else {
		document.getElementById('QS_in_dead').value = "";
		document.getElementById('QSinday').value = "";
		document.getElementById('QSinmonth').value = "";
	}
}

