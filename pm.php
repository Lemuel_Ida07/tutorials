
<?php
session_start();
if(isset($_SESSION["User_Type"]))
{
	switch($_SESSION["User_Type"])
	{
		case 0:	//Designer
			header("Location:home.php");
			break;
		case 1:	//Reviewer
			header("Location:reviewer.php");
			break;
		case 2:	//DC
			header("Location:dc.php");
			break;
		case 4:	//OPS
			header("Location:ops.php");
			break;
		case 5:	//Admin
			header("Location:admin.php");
			break;
	}
}
else
{
	header("Location:index.php");
}

include('connect.php');
include('phpGlobal.php');
$today = date("F j, Y");
  
?>
<html>
<head>
	<title> Activity Monitoring </title>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/calendar.css" />
	<link rel="icon" href="img/logoblue.png">

	<script src="script.js"></script>
	<script src="scripts/javaScript.js"></script>
	<script src="scripts/internal_deadline_computation.js"></script>
	<script src="jquery-3.2.1.min.js"></script>
	
	<style>
      
    p {
      text-align: center;
		}
    
		#menu_item_logo1
		{
			background-color:#f2f2f2;
		}
    
		#menu_item_logo1:hover
		{
			background-color:#f2f2f2;
		}
    
		#tab1
		{
			color:#515151;
			font-weight: bold;
			text-shadow: none;	
    }
    
    #navbar tr td#cal{
      background-color:#f7f9fa;
      color:#0747a6;
      border-color:#0747a6;
      box-shadow: inset 0px 2px 2px gray;
    }
    
  #navbar tr td#projects{
      background-color:#0747a6;
      color:#deebff;
      box-shadow: 0px 0px 1px 2px #deebff inset;
    }  	
    
</style>
</head>
<body>
	<?php include('header.php'); ?>
	<div id="mainform">
		<?php
include("phpScript.php");
$today = date("Y-m-d");
$current_day = (int)date("d");
$month_selected = (int)date("m");
$year_selected = (int)date("Y");
$month_string = $month_array[(int)date("m")];
$NoLastDays = 0;
?>

<script src="scripts/calendarPhp.js"></script>

<!-- LOADER -->
<div id="loader_bg">
	<center>
	<div class="loader" id="cir">
	</div>
	<progress style="display: none;" value="0" max="1" id="loading"></progress>
	</center>
</div>
		
<!-- POP UP -->
<div id="pop_up_bg">
	<div id="pop_up" style="overflow-y:auto;">
      
		<div id="close_button" onClick="closePop()"> 
      <label>x</label> 
    </div>
			<h1 id="date"> April 5, 2018 </h1>

			<label> Please Select a Project Name. </label><br>
			<input type="text" name="searchbox" id="searchbox" style="width:100%;padding:20px;" placeholder="Select a project." autocomplete="off" onkeyup="filter('searchbox','datalist')" />
			<div id="datalist" class="datalist"><?php
				$sql = "SELECT ID,Project_Number,Project_Name FROM project where Remarks = 1";
				$result = mysqli_query($conn,$sql);
				if(mysqli_num_rows($result) > 0)
				{
          while($rows = mysqli_fetch_assoc($result))
          {
          echo "<h4 id='".$rows['ID']."' onmousedown='hideList();passData".$rows['ID']."();'> ".$rows['Project_Number']." - ".$rows['Project_Name']." </h4>";
          echo "<script>
          function passData".$rows['ID']."()
          {
          document.getElementById('searchbox').value = document.getElementById('".$rows['ID']."').innerHTML;
          document.getElementById('projectid').value='".$rows['ID']."';
          }
          </script>";
          }
				}
				?>
			</div>
			<label> Phase </label>
			<select name="phase" id="phaselist" style="width:100px;padding:10px 5px;">
				<option> None </option>
				<option value="SD"> SD </option>
				<option value="DD"> DD </option>
				<option value="CD"> CD </option>
				<option value="AB"> AB </option>
				<option value="MT"> MT </option>
				<option value="TR"> TR </option>
			</select>
			<br />
			<u class="clickme" id="checkalltrade" onclick="checkAllTrade()">Check All</u>
			<input type="text" name="projectid" id="projectid" style="display:none;" />
			<input type="date" name="external_deadline" id="external_deadline" style="display:none;"/>
			<input type="text" name="day" id="day" style="display:none;" />
			<input type="text" name="month" id="month" style="display:none;" />
			<input type="text" name="year" id="year" style="display:none" />


			<!-- FOR INTERNAL DEADLINES DAY AND MONTH --><?php
			$trade = array("C","E","A","AU","S","P","M","FP","QS");
			foreach($trade as $value)
			{
				echo "<input type='text' name='".$value."inday' id='".$value."inday' style='display:none;' />";
				echo "<input type='text' name='".$value."inmonth' id='".$value."inmonth' style='display:none;'/>";
			}
			?>
			<br>
			<table id="checklist1" border=0 style="margin-top: 10px; table-layout: fixed;">
				<!-- Trades -->
				<tr><?php
					foreach($trade as $value)
					{
					echo "<th>";
					echo "<input id='trade$value' onClick='".$value."Out()' type='checkbox' name='trade'/> $value";
					echo "<input type='text' name='".$value."trade' style='display:none;' value='$value'/>";
					echo "</th>";
					}
					?>
				</tr>
				<!-- Internal Deadline Date and Ticket Number -->
				<tr><?php
					foreach($trade as $value)
					{
					echo "<td>";
					echo "<input id='".$value."_in_dead' class='textbox' name='".$value."internal_deadline' type='text' placeholder='yyyy-mm-dd' focusable='false' readonly/>";
					echo "<input class='textbox' type='text'  onkeyup='checkTicket(`".$value."ticket`);' name='".$value."ticket' id='".$value."ticket' placeholder='Ticket Number' />";
					echo "</td>";
					}
					?>
				</tr>
				<tr>
					<td colspan="9"><textarea name='deliverables' id='deliverables' placeholder='Deliverables' style="padding:1%;" rows="3"></textarea></td>
				</tr>
				<tr>
					<td colspan="7">
					<td colspan="2"><input id="save_button" class="add_externaldl" type="submit" value="Generate" style="vertical-align: bottom;" /></td>
				</tr>
			</table>
      
	</div>
</div>
<div id="bg_bg">
</div>

<div id="for_script" style="display:none;">
</div>
<!-- END POP UP -->


<div id="info">
  <img class="prev" id="toogle_button_sml" onClick="prevMonth()" src="img/left_arrow.png" />
	<img class="next" id="toogle_button_sml" onClick="nextMonth()" src="img/left_arrow.png" style="transform: rotate(180deg);" />
	<p id="monthnumber" style="display: none;"><?php echo $month_selected; ?></p>
  <label class="h1"> <span id="currentMonth"> <?php echo $month_string; ?> </span><span id="currentYear"><?php echo $year_selected; ?></span> </label>
	
	<center>
		<table id="prev_mo_head" border="0" style="display:none;">
			<tr>
				<th id='pmh' colspan=1>
					<button onclick='prevMonth()' id='btn_prev_mon'></button>
				</th>
				<th id='pmh' colspan=5>
					<label id='previous_month_ind'>March</label>
				</th>
				<th id='pmh' colspan=1>
					<button onclick='nextMonth()' id='btn_next_mon'> > </button>
				</th>
			</tr>
		</table>
	</center>	

	<br/>
										 
	<fieldset id="fset_filter_calendar">
	<legend> <label class="title">Filter Calendar</label> </legend>
	<input type="text" id="team_id" value="<?php echo $_SESSION['Team_ID']; ?>" style="display:none;"/>
	<label class="title"> Search by Type </label>
	<div>
		<input type="submit" id="by_external" value="External" class="btn-normal bg-green white bold width-45pc margin-2px" />
		<input type="submit" id="by_internal" value="Internal" class="btn-normal bg-green float-right white bold width-45pc margin-2px" />
		<input type="submit" id="by_all" value="All" class="btn-normal bg-green white bold width-100pc margin-2px" />
	</div>
	<label class="title"> Search by Project </label>
	<input type="text" id="by_project" placeholder="Select a project." autocomplete="off" onkeyup="filter('by_project','datalist2')"/>
	<div id="datalist2" class="datalist2"><?php
		$sql = "SELECT 
					project.ID,
					project.Project_Number,
					project.Project_Name 
				FROM project
				INNER JOIN external_deadline
					ON project.ID = external_deadline.Project_ID
				WHERE 
          project.team_ID = ".$_SESSION['Team_ID']."
					and project.Remarks = 1";
    $result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
		while($rows = mysqli_fetch_assoc($result))
		{
			echo "<h4 id='by_proj".$rows['ID']."' onmousedown='hideList();passData_by_proj".$rows['ID']."();'> ".$rows['Project_Number']." - ".$rows['Project_Name']." </h4>";
			echo "<script>
			function passData_by_proj".$rows['ID']."()
			{
				document.getElementById('by_project').value = document.getElementById('by_proj".$rows['ID']."').innerHTML;
				$.ajax(
				{
					url:'pmCalendarFilter.php',
					type:'post',
					data:'by_project=true'+
              '&team_id='+$('#team_id').val()+
						 '&month='+monthnumber+
						 '&year='+year+
						 '&lastday='+getDayName(monthnumber,year,1,0)+
						 '&project_id=".$rows['ID']."',
					success:function(data){
						$('#calendar').html(data);
						$('#datalist2').hide();
					}
				});
			}
			</script>";
		}
		}
		?>
	</div>
	</fieldset>
															
	<fieldset id="fset_legends">
	<legend> <label class="title">Legends</label> </legend>
	<table width="100%" class="legends">
		<thead>
			<tr>
				<th><label class="title">External Deadline</label></th>
				<th><label class="title">Internal Deadline</label></th>
			</tr>
		</thead>
		<tbody>
			<tr>																	 
				<td> <div id="arryn_exdl" class="event" > House Arryn </div> </td>
				<td> <div id="arryn_indl" class="event" > House Arryn </div> </td>
			</tr>
			<tr>																 
				<td> <div id="stark_exdl" class="event" > House Stark </div> </td>
				<td> <div id="stark_indl" class="event" > House Stark </div> </td>
			</tr>
			<tr>																 
				<td> <div id="lannister_exdl" class="event" > House Lannister </div> </td>
				<td> <div id="lannister_indl" class="event" > House Lannister </div> </td>
			</tr>
			<tr>																 
				<td> <div id="tyrell_exdl" class="event" > House Tyrell </div> </td>
				<td> <div id="tyrell_indl" class="event" > House Tyrell </div> </td>
			</tr>	 
			<tr>																 
				<td> <div id="baratheon_exdl" class="event" > House Baratheon </div> </td>
				<td> <div id="baratheon_indl" class="event" > House Baratheon </div> </td>
			</tr>	 
			<tr>																 
				<td> <div id="targaryen_exdl" class="event" > House Targaryen </div> </td>
				<td> <div id="targaryen_indl" class="event" > House Targaryen </div> </td>
			</tr>
			<tr>																 
				<td> <div id="martell_exdl" class="event" > House Martell </div> </td>
				<td> <div id="martell_indl" class="event" > House Martell </div> </td>
			</tr>
			<tr>																 
				<td> <div id="mormont_exdl" class="event" > House Mormont </div> </td>
				<td> <div id="mormont_indl" class="event" > House Mormont </div> </td>
			</tr>
			<tr>																 
				<td> <div id="tully_exdl" class="event" > House Tully </div> </td>
				<td> <div id="tully_indl" class="event" > House Tully </div> </td>
			</tr>
			<tr>																 
				<td> <div id="greyjoy_exdl" class="event" > House Greyjoy </div> </td>
				<td> <div id="greyjoy_indl" class="event" > House Greyjoy </div> </td>
			</tr>
		</tbody>
	</table>
	</fieldset>
	<br />


	<div id="content_tray" style="display:none;">
		THIS IS THE CONTENT TRAY
	</div>

	<input type="text" id="extraDaysPrevMonth" style="display:none" />
	<input type="text" id="one" style="display:none" />
</div>

<div id="list">
	<table id="weeks">
		
	</table>
	<div id="dates">
		<table id="calendar">
			<tr>
			<th id="weekends" class="title"> Sunday </th>
			<th id="weekdays" class="title"> Monday </th>
			<th id="weekdays" class="title"> Tuesday </th>
			<th id="weekdays" class="title"> Wednesday </th>
			<th id="weekdays" class="title"> Thursday </th>
			<th id="weekdays" class="title"> Friday </th>
			<th id="weekends" class="title"> Saturday </th>
		</tr>			
<?php
				/* GET LAST DAYS FROM PREVIOUS  MONTH */
	$NoLastDays = 0;
	$base_month = 4; //default month (don't cahnge)
	$base_year = 2018; //default year (don't cahnge)
	$month_selected = (int)date("m") -1;
	$year_selected = (int)date("Y");
	for($date_count = $base_month; $year_selected >= $base_year; $date_count++)
	{
		$NoLastDays = getPrevMonthNoLastDays($base_month,$base_year,$NoLastDays);	 
		$base_month ++;
		if($base_month > 12)
		{
			$base_year++; 
			$base_month = 1 ;
		}						
		//stops the loop				
		if($base_year >= $year_selected && $base_month > $month_selected)
		{
			break;
		}
	}

	/* END OF GET LAST DAYS FROM PREV MONTH */
			$month_selected = (int)date("m");
			$totaldays=cal_days_in_month(CAL_GREGORIAN,$month_selected,$year_selected);
			$loop = ($NoLastDays + $totaldays) / 7;
			$extradays = ($NoLastDays + $totaldays) % 7;
			if($extradays != 0)
			$loop ++;
			$daycounter = $NoLastDays - (($NoLastDays * 2)-1);
			$dayweek = 7 -$NoLastDays;
			$count4 = 0; // para sa count ng kung ilan yung event per day
			$sundays = array();
			$saturdays = array();
			for($count = 1; $count <= $loop; $count ++)
			{
				echo"<tr>";
				for($count2 = $daycounter; $count2 <= $dayweek; $count2 ++)
				{
					$sql = "SELECT project.ID,
										project.Project_Name, 
										project.Team_ID, 
										project.Project_Number,
										external_deadline.Phase,	
										external_deadline.ID AS exid 
									FROM project 
									INNER JOIN external_deadline 
										ON project.ID = external_deadline.Project_ID 
									WHERE 
                    project.Team_ID = ".$_SESSION['Team_ID']."
										and Day_ED = $count2 
										and Month_ED = $month_selected 
										and Year_ED = $year_selected";
					$result = mysqli_query($conn,$sql);
					$sql2 ="SELECT internal_deadline.ID,
										project.Project_Name,
										project.Team_ID,
										project.Project_Number,
										external_deadline.Day_ED,
										external_deadline.Month_ED,
										external_deadline.Year_ED,	
										internal_deadline.Externaldl_ID,
										internal_deadline.Trade,
										internal_deadline.Phase, 
										internal_deadline.Day_ID,
										internal_deadline.Month_ID,
										internal_deadline.Year_ID,
										internal_deadline.Ticket_Number,
										team.Team_Name
									FROM project
									INNER JOIN external_deadline 
										ON project.ID = external_deadline.Project_ID
									INNER JOIN internal_deadline 
										ON external_deadline.ID = internal_deadline.Externaldl_ID
									LEFT JOIN team 
										ON project.Team_ID = team.ID
									WHERE  
                    project.Team_ID = ".$_SESSION['Team_ID']."
										and internal_deadline.Day_ID = $count2 
										and internal_deadline.Month_ID = $month_selected 
										and internal_deadline.Year_ID = $year_selected";
					$result2 = mysqli_query($conn,$sql2);
					if(mysqli_num_rows($result) > 0)
					{
						if($NoLastDays > 0 || $count2 == $daycounter || $count2 == $daycounter + 6)
						{
							if($count2 <= 0)
							{
								echo "<td id='days' >  </td>";
							}
							else
							{
								echo "<td id='days'";
								if($count2 == $current_day)
								{
									
									echo "style = 'border:dotted 3px #3396d9;'";
								}
								echo "> $count2 </td>";
							}
							$NoLastDays = $NoLastDays-1;
						}
						else{
							echo "<td id='days'";
							if($count2 == $current_day)
							{																						 
									
								echo "style = 'border:dotted 3px #3396d9;'";
							}
							echo "> $count2
						<div class='$count2' id='daily_container'>";
						// SHOW EXTERNAL DEADLINES
						while($rows = mysqli_fetch_assoc($result))
						{
							$EXcurID = $rows['Team_ID']; // get TEAM ID
							if($rows['Phase'] != "")
								{
									echo "<p style='background-color: ".$ex_dead_bg[$EXcurID]."; border:solid 2px black;' id='deadline".$rows['exid']."' class='event'>  ".$rows['Project_Name']." - ".$rows['Phase']." </p>";
								}
								else
								{
									echo "<p style='background-color: ".$ex_dead_bg[$EXcurID]."; border:solid 2px black;' id='deadline".$rows['exid']."' class='event'>  ".$rows['Project_Name']." </p>";
								}
								echo "<script>
									$(document).ready(function(){
										$('#deadline".$rows['exid']."').on('click',function(){
										var value = $(this).val();
											$.ajax(
											{
												url:'deadline.php',
												type:'post',
												data:'request=+".$rows['ID']."&phase=".$rows['Phase']."',
												success:function(data)
												{
													$('#for_script').html(data);
													$('#pop_project').val(forPop.project); 	 
													$('#pop_project_id').val(forPop.project_id); 
													$('#pop_phase').val(forPop.phase);					 
													$('#pop_external_deadline').val(forPop.external_deadline);			
													$('#pop_internal_deadline').attr('max',forPop.external_deadline);									 
													$('#pop_deliverables').val(forPop.deliverables);
													$('#pop_externaldl_id').val(forPop.externaldl_id);
				
													showPop();

													$.ajax(
													{
														url:'deadline.php',
														type:'post',
														data:'internal_deadlines_for_pm=true'+
																	'&externaldl_id='+$('#pop_externaldl_id').val(),
														success:function(data)
														{
															$('#internal_list').html(data);
														}
													});
												},
											});
										});
									});
									</script>";
						}
						while($rows2 = mysqli_fetch_assoc($result2))
						{
							$INcurID = $rows2['Team_ID']; // get TEAM ID
							if($rows2['Phase'] != "")
								{
									echo "<p style='background-color: ".$in_dead_bg[$INcurID].";' id='indeadline".$rows2['ID']."' class='event' onClick=' '>  ".$rows2['Project_Name']." (".$rows2['Trade'].") - ".$rows2['Phase']." </p>";
								}
								else
								{
									echo "<p style='background-color: ".$in_dead_bg[$INcurID].";' id='indeadline".$rows2['ID']."' class='event' onClick=' '>  ".$rows2['Project_Name']." (".$rows2['Trade'].") </p>";
								}
								$ex_day =	$rows2["Day_ED"];
								$ex_month =	$rows2["Month_ED"];
								$ex_year =	$rows2["Year_ED"];
								
								if($ex_day < 10)
									$ex_day = "0".$ex_day;
								if($ex_month < 10)
									$ex_month = "0".$ex_month;		

								$in_day =	$rows2["Day_ID"];
								$in_month =	$rows2["Month_ID"];
								$in_year =	$rows2["Year_ID"];
								
								if($in_day < 10)
									$in_day = "0".$in_day;
								if($in_month < 10)
									$in_month = "0".$in_month;

								echo "<script>
										$(document).ready(function(){
											$('#indeadline".$rows2['ID']."').on('click',function(){
												$('#edit_idl_project').html('".$rows2["Project_Number"]." - ".$rows2["Project_Name"]."');
												$('#edit_idl_phase').html('".$rows2["Phase"]."');									
												$('#edit_idl_team').html('".$rows2["Team_Name"]."');		 						
												$('#edit_idl_trade').val('".$rows2["Trade"]."');		
												$('#edit_idl_external_deadline').val('$ex_year-$ex_month-$ex_day');	 
												$('#edit_idl_internal_deadline').val('$in_year-$in_month-$in_day');	 
												$('#edit_idl_internal_deadline').attr('max','$ex_year-$ex_month-$ex_day');
												$('#edit_idl_ticket').val('".$rows2["Ticket_Number"]."');		 
												$('#edit_idl_inid').val('".$rows2["ID"]."');		
												$('#edit_idl_exid').val('".$rows2["Externaldl_ID"]."');
												showIdlPop();
											});
										});
										</script>";
						}
						echo"</div></td>";
						}
					}
					else if(mysqli_num_rows($result2) > 0)
					{
						if($NoLastDays > 0 || $count2 == $daycounter || $count2 == $daycounter + 6)
						{
							if($count2 <= 0)
							{
								echo "<td id='days' >  </td>";
							}
							else
							{
								echo "<td id='days''";
								if($count2 == $current_day)
								{
									
									echo "style = 'border:dotted 3px #3396d9;'";
								}
								echo "> $count2 </td>";
							}
							$NoLastDays = $NoLastDays-1;
						}
						else
						{
							echo "<td id='days'";
							if($count2 == $current_day)
							{
																
									echo "style = 'border:dotted 3px #3396d9;'";
							}
							echo "> $count2
							<div class='$count2' id='daily_container'>";
							// SHOW INTERNAL DEADLINES
							while($rows2 = mysqli_fetch_assoc($result2))
							{
							$INcurID = $rows2['Team_ID']; // get TEAM ID
								if($rows2['Phase'] != "")
								{
									echo "<p style='background-color: ".$in_dead_bg[$INcurID].";' id='indeadline".$rows2['ID']."' class='event' onClick=' '>  ".$rows2['Project_Name']." (".$rows2['Trade'].") - ".$rows2['Phase']." </p>";
								}
								else
								{
									echo "<p style='background-color: ".$in_dead_bg[$INcurID].";' id='indeadline".$rows2['ID']."' class='event' onClick=' '>  ".$rows2['Project_Name']." (".$rows2['Trade'].") </p>";
								}
								
								$ex_day =	$rows2["Day_ED"];
								$ex_month =	$rows2["Month_ED"];
								$ex_year =	$rows2["Year_ED"];
								
								if($ex_day < 10)
									$ex_day = "0".$ex_day;
								if($ex_month < 10)
									$ex_month = "0".$ex_month;		

								$in_day =	$rows2["Day_ID"];
								$in_month =	$rows2["Month_ID"];
								$in_year =	$rows2["Year_ID"];
								
								if($in_day < 10)
									$in_day = "0".$in_day;
								if($in_month < 10)
									$in_month = "0".$in_month;

								echo "<script>
										$(document).ready(function(){
											$('#indeadline".$rows2['ID']."').on('click',function(){
												$('#edit_idl_project').html('".$rows2["Project_Number"]." - ".$rows2["Project_Name"]."');
												$('#edit_idl_phase').html('".$rows2["Phase"]."');									
												$('#edit_idl_team').html('".$rows2["Team_Name"]."');		 						
												$('#edit_idl_trade').val('".$rows2["Trade"]."');		
												$('#edit_idl_external_deadline').val('$ex_year-$ex_month-$ex_day');	 
												$('#edit_idl_internal_deadline').val('$in_year-$in_month-$in_day');	 
												$('#edit_idl_internal_deadline').attr('max','$ex_year-$ex_month-$ex_day');
												$('#edit_idl_ticket').val('".$rows2["Ticket_Number"]."');
												$('#edit_idl_inid').val('".$rows2["ID"]."');
												$('#edit_idl_exid').val('".$rows2["Externaldl_ID"]."');
												showIdlPop();
											});
										});
										</script>";
							}
							echo"</div></td>";
						}
					}
					else
					{
						if($NoLastDays > 0 || $count2 == $daycounter || $count2 == $daycounter + 6)
						{
							if($count2 <= 0)
							{
							echo "<td id='days' >   </td>";
							}
							else
							{
								echo "<td id='days'";
								if($count2 == $current_day)
								{
									
								echo "style = 'border:dotted 3px #3396d9;'";
								}
								echo "> $count2 </td>";
							}
							$NoLastDays = $NoLastDays-1;
						}
						else{
							echo "
							<td id='days'";
							if($count2 == $current_day)
							{
								
								echo "style = 'border:dotted 3px #3396d9;'";
							}
							echo "> $count2 <div class='$count2' id='daily_container'></div></td>";
						}
					}
				}
				echo "</tr>";
				if($daycounter > 0)
				array_push($sundays,$daycounter);
				if($dayweek - $daycounter == 6)
				array_push($saturdays,$dayweek);
				$daycounter = $count2;
				if($extradays!=0)
				{
					if($count < $loop - 2)
					{
					$dayweek = $count2 + 6;
					}
					else
					{
						$dayweek = $totaldays;
						echo "<script> document.getElementById('extraDaysPrevMonth').value ='$totaldays'; </script>";
					}
				}
				else
				{
				$dayweek = $count2 + 6;
				}
			}
			echo "<tr style='display:none;'><td> Sundays<script>";
			foreach($sundays as $sundayvalue)
			{
			echo "addToArrSun($sundayvalue);";
			}
			echo "</script></td></tr>";
			echo "<tr style='display:none;'><td> Saturdays<script>";
			foreach($saturdays as $saturdayvalue)
			{
			echo "addToArrSat($saturdayvalue);";
			}
			echo "</script></td></tr>";
?>

		</table>
	</div>
</div>
  <div id="activity_edit_pop_bg" onclick="hidePop()" class="popUpBg bg-gray opacity-7" style="z-index:10;">	
		</div>											 
  <div id="activity_edit_pop" class="popUp bg-gray width-50pc padding-4px" style="z-index:11;">
    <fieldset class='height-95pc bg-white' id="fieldset_externaldl" style='border:solid 1px #34495e;overflow-y:auto'>
      <legend class='bg-white' style='border-radius:5px;color:#34495e'><h2 class="margin-0"> External Deadline </h2></legend>
      <input type="submit" class='btn-normal' style="width:50px;position:absolute; top:0px;right:0px; background-color:red;color:white;" id="pop_cancel_btn" value="X" /> 
      <h3 class="float-left width-70pc"> Project: <input type="text" id="pop_project" style="padding:4px;width:70%;" placeholder="Select Project" readonly="readonly"/></h3>
      <br /><br/>
      <div id="project_list" class="datalistPop"><?php
      // onkeyup="filter('pop_project','project_list')"
      $sql = "SELECT ID,Project_Number,Project_Name FROM project where Remarks = 1";
      $result = mysqli_query($conn,$sql);
      if(mysqli_num_rows($result) > 0)
      {
      while($rows = mysqli_fetch_assoc($result))
      {
      echo "<h4 id='".$rows['ID']."Pop' onmousedown='hideListPop();passDataPop".$rows['ID']."();'> ".$rows['Project_Number']." - ".$rows['Project_Name']." </h4>";
      echo "<script>
      function passDataPop".$rows['ID']."()
      {
      document.getElementById('pop_project').value = document.getElementById('".$rows['ID']."Pop').innerHTML;
      }
      </script>";
      }
      }
      ?>
    </div>																	 
    <input type="text" id="pop_project_id" style="display:none;" />
    <br />	
    <br />
      <h3 class='float-left'> Phase: 
        <select id="pop_phase">  
          <?php
            $sql = "SELECT DISTINCT(Phase) FROM External_Deadline";
            $result = mysqli_query($conn,$sql);
            while($rows = mysqli_fetch_assoc($result))
            {
              echo "<option value='".$rows["Phase"]."'> ".$rows["Phase"]." </option>";	
            }
          ?>
        </select> 
      </h3>		
      <br /><br /> 
      <h3 class="float-left"> External Deadline: 
        <input type="date" id="pop_external_deadline" style="padding:4px;width:auto;"/>
        <input type="text" id="pop_externaldl_id" style="padding:4px;width:auto;"/>
      </h3>
      <h3 class="clear-left margin-0"> Deliverables: </h3>
      <textarea class="padding-4px margin-0 lightGray width-70pc" id="pop_deliverables" placeholder="Some Deliverables..."></textarea>

      <div class="float-right">																																																	
      </div>
      <br />					 
      <hr />
      <h2 class="margin-0 popBgColor"> Internal Deadlines </h2>
      <table class="general_table width-100pc">
        <thead>
          <tr>
            <th> Trade </th>
            <th> Internal Deadline </th>
            <th> Ticket Number </th>
          </tr>	
        </thead>
        <tbody id="internal_list">
          <!-- data from deadline.php -->
        </tbody>
      </table>	
    </fieldset>
  </div>
  <div id="pop_edit_idl" class="bg-gray padding-4px" style="display:none;position:fixed;width:400px;z-index:9;">
			<fieldset id="pop_edit_fieldset_idl" class="bg-white" style='border:solid 1px #34495e;overflow-y:auto;cursor:arrow;'>
				<legend class='bg-white' style='border-radius:5px;color:#34495e'><h2 class="margin-0"> Internal Deadline </h2></legend>
				<input type="submit" class='btn-normal' onclick="hideIdlPop()" style="width:50px;position:absolute; top:0px;right:0px; background-color:red;color:white;" value="X" /> 
				<h3> Project: <span id="edit_idl_project" class="blue"></span> </h3>
				<h3> Phase: <span id="edit_idl_phase"  class="blue"></span> </h3>
				<h3> Team: <span id="edit_idl_team" class="blue"></span> </h3>	 
				<h3 class="float-left"> Trade: </h3>
					<select id="edit_idl_trade" class="blue">
						<option value="All"> All </option>
							<?php
								$sql = "SELECT DISTINCT(Trade) FROM user";
								$result = mysqli_query($conn,$sql);
								if(mysqli_num_rows($result) > 0)
								{
									while($rows = mysqli_fetch_assoc($result))
									{
										echo "<option value='".$rows["Trade"]."'> ".$rows["Trade"]." </option>";
									}		
								}	
							?>
					</select>
				<h3 class="clear-left"> External Deadline: <input type="date" id="edit_idl_external_deadline" readonly="readonly" style="padding:4px; width:130px;margin:0px;"></span> </h3>
				<h3> Internal Deadline: <input type="date" id="edit_idl_internal_deadline" min="<?php echo date('Y-m-d'); ?>" style="padding:4px; width:130px;margin:0px;"/></h3> 
				<h3> Ticket Number: <input type="text" id="edit_idl_ticket" class="blue" style="padding:4px; width:130px;margin:0px;" /> </h3>	
				<h3 style="display:none;"> Internal Deadline ID: <input type="text" id="edit_idl_inid" style="display:none;" /></h3>
				<h3 style="display:none;"> External Deadline ID: <input type="text" id="edit_idl_exid" style="display:none;" /></h3>
			</fieldset>
		</div>
</div>
<script>

	var monthnumber = document.getElementById('monthnumber').innerHTML;
	var months = ["index","January","February","March","April","May","June","July","August","September","October","November","December"];
	var year = document.getElementById('currentYear').innerHTML;
	var one = 1;
	var prevyear = 0;
	var extraDaysPrevMonth = document.getElementById('extraDaysPrevMonth').value;
	var monthholder = document.getElementById('currentMonth').innerHTML;

	//INITIALIZE	
	$(document).ready(function(){
		hidePop();
	});
	// FINISH THE SCRIPT FOR THE MONTH TOOGLE CHANGE
	$(document).ready(function(){
		$('.prev').on('click',function(){
			var value = $(this).val();
			$.ajax(
			{
				url:'pmAjaxCalendar.php',
				type:'post',
				data:'&team_id='+$('#team_id').val()+'&month='+monthnumber+'&year='+year+'&lastday='+getDayName(monthnumber,year,1,0),
				success:function(data)
				{
					clearArray();
					$('#calendar').html(data);
				},
			});
		});
	});

	$(document).ready(function(){
		$('.next').on('click',function(){
			var value = $(this).val();
			$.ajax(
			{
				url:'pmAjaxCalendar.php',
				type:'post',
				data:'&team_id='+$('#team_id').val()+'&month='+monthnumber+'&year='+year+'&lastday='+getDayName(monthnumber-one,year-prevyear,document.getElementById('extraDaysPrevMonth').value,1),
				success:function(data)
				{
					clearArray();
					$('#calendar').html(data);
				},
			});
		});
	});

function getDayName(month,year,day,numberDay)
{
	var d = new Date(year+"-"+month+"-"+day);
	//alert(year+"-"+month+"-"+day);
	var firstDay = d.getDay()+numberDay;
	return firstDay;

}
function prevMonth() {

	if(monthnumber <= 1)
	{
		monthnumber = 12;
		year--;
	}
	else
	{
		monthnumber--;
	}
	document.getElementById('monthnumber').innerHTML = monthnumber;
	document.getElementById('previous_month_ind').innerHTML = months[monthnumber-1];
	document.getElementById('currentMonth').innerHTML = months[monthnumber];
	document.getElementById('currentYear').innerHTML = year;
}

function nextMonth() {

	if(monthnumber >= 12)
	{
		prevyear = 1;
		monthnumber = 1;
		year ++;
		one = -11;
	}
	else
	{
		prevyear = 0;
		one = 1;
		monthnumber++;
	}
	document.getElementById('monthnumber').innerHTML = monthnumber;
	document.getElementById('previous_month_ind').innerHTML = months[monthnumber-1];
	document.getElementById('currentMonth').innerHTML = months[monthnumber];
	document.getElementById('currentYear').innerHTML = " "+year;
	document.getElementById('one').value = monthnumber+" "+one;
}

function openPop() {
	document.getElementById('pop_up').style.visibility = "visible";
	document.getElementById('pop_up_bg').style.visibility = "visible";
	document.getElementById('bg_bg').style.visibility = "visible";
}

$(document).ready(function(){
		$('#close_button').on('click',function(){
			var value = $(this).val();
			$.ajax(
			{
				url:'submit.php',
				type:'post',
				data:'del_temp_notif='+value,
				success:function(data)
				{
					<?php
					foreach($trade as $value)
					{
						echo "$('#person_holder$value').html('');";
					}
          ?>
				},
			});
		});
	});

function closePop() {
	document.getElementById('pop_up').style.visibility = "hidden";
	document.getElementById('pop_up_bg').style.visibility = "hidden";
	document.getElementById('bg_bg').style.visibility = "hidden";
	hideList();

	// RESET ALL DATA
	document.getElementById('searchbox').value = "";
	document.getElementById('projectid').value = "";
	document.getElementById('external_deadline').value = "";
	document.getElementById('day').value = "";
	document.getElementById('month').value = "";
	document.getElementById('year').value = "";

	document.getElementById('deliverables').value = "";
	
	document.getElementById('checkalltrade').innerHTML = "Check All";

	<?php
		foreach($trade as $value)
		{
			echo "document.getElementById('".$value."inday').value = '';";
			echo "document.getElementById('".$value."inmonth').value = '';";
			echo "document.getElementById('".$value."_in_dead').value = '';";
			echo "document.getElementById('trade$value').checked = false;";
			echo "document.getElementById('".$value."ticket').value = '';";
		}
    ?>
}

function toogle() {
	var x = document.getElementById("toogle_view_side_menu");
    if (x.style.transform === "rotate(180deg") {
        x.style.transform = "rotate(-180deg)";
    } else {
        x.style.transform = "rotate(180deg)";
    }
}

function getDate(id)
{
  var d2 = monthnumber;
  var d3 = id;
  
	document.getElementById('date').innerHTML = months[monthnumber]+" "+id+", "+year;
  
  if(d2 < 10)
  {
    d2 = "0"+d2;
  }
  if(d3 < 10)
  {
    d3 = "0"+d3;
  }
  
	document.getElementById('external_deadline').value = year+"-"+d2+"-"+d3;
	document.getElementById('day').value = id;
	document.getElementById('month').value = monthnumber;
	document.getElementById('year').value = year;
}

// BACKGROUND LOADING
function clock() {

var timeleft = 1;

var downloadTimer = setInterval(function(){

	//DATE VALIDATOR
	var x = new Date(); // DATE TODAY
	var y = x.getDate(); // EXTRACT THE DAY
	var z = x.getFullYear();//EXTRACTY YEAR
	var m = x.getMonth()+1; //EXTRACT MONTH

  document.getElementById("loading").value = 1 - --timeleft;
  if(timeleft <= 0)
	//COMPARE
	if(m >= monthnumber)
	{ // selected day from the js file
		if(m > monthnumber && year <= z)
		{
			alert('ERROR: Cannot Create Deadline Schedule \n\n Deadline Schedule should be beyond the current date.');
		}
		else if (m == monthnumber)
		{
			if(y > selectedDay)
			{
				alert('ERROR: Cannot Create Deadline Schedule \n\n Deadline Schedule should be beyond the current date.');
			}
			else
			{
				openPop();
			}
		}
		else if(year > z)
		{
			openPop();
		}
	}
	else
	{
		openPop();
	}
    clearInterval(downloadTimer);

},10);

}

// SHOW YUNG SIDE MENU
var drawer = true;

function toogle_side_menu() {

	if(drawer == true) {
		document.getElementById("toogle_button").style.transform = "rotate(180deg)";
		document.getElementById("info").style.transform = "translateX(-300px)";
		document.getElementById("list").style.transform = "translateX(-50px)";
		document.getElementById("list").style.width = "99%";
		document.getElementById("weeks").style.width = "98%";
		drawer = false;
	}

	else {
		document.getElementById("toogle_button").style.transform = "rotate(-360deg)";
		document.getElementById("info").style.transform = "translateX(2px)";
		document.getElementById("list").style.width = "84%";
		document.getElementById("weeks").style.width = "90%";
		drawer = true;
	}
}

// SHOW YUNG MGA LEGENDS SA INFO
var legendOn = false;
function showLegends() {
	if(legendOn == false) {
		document.getElementById("legends").style.height = "733px";
		document.getElementById("legends").style.backgroundColor = "#FFFFFF";
		document.getElementById("legends").style.overflow = "auto";
		document.getElementById("lol").style.textShadow = " 3px 3px 10px #000000";
		legendOn = true;
	}
	else if(legendOn == true) {
		document.getElementById("legends").style.height = "0px";
		document.getElementById("legends").style.backgroundColor = "#00008B";
		legendOn = false;
	}
}

function showList() {
	var element = document.getElementById('datalist').style.display = "block";
}

	function hideList() {
	if(document.getElementById('datalist').style.display == "block")
	var element = document.getElementById('datalist').style.display = "none";
	}

	function hideListPop() {
	if(document.getElementById('project_list').style.display == "block")
	var element = document.getElementById('project_list').style.display = "none";
	}

	function hideDiv() {
		var element = document.getElementById('datalist').style.display;
		if (element == "block") { element = "none"; }
	}

function filter(inputid,divid) {
	var input, filter, list, count;
	input = document.getElementById(inputid);
	filter = input.value.toUpperCase();
	div = document.getElementById(divid);
	list = div.getElementsByTagName("h4");
	for(count = 0; count < list.length; count ++)
	{
		if (list[count].innerHTML.toUpperCase().indexOf(filter) > -1)
		{
			list[count].style.display = "";
		} else {
			list[count].style.display = "none";
		}
	}
}

function removeName(node) {
	var child = node;

	parent = child.parentNode;

	$('#' + parent.id).remove();

}
$("#searchbox").click(function (e) {
			$(".datalist").show();
			e.stopPropagation();
		});

		$(".datalist").click(function (e) {
			e.stopPropagation();
		});

		$(document).click(function () {
			$(".datalist").hide();
		});

$("#pop_project").click(function (e) {
			//$("#project_list").show();
			e.stopPropagation();
		});

		$("#project_list").click(function (e) {
			e.stopPropagation();
		});

		$(document).click(function () {
			$("#project_list").hide();
		});

	
	$(document).ready(function(){
		$('#by_external').on('click',function(){
			$.ajax(
			{
				url:'pmCalendarFilter.php',
				type:'post',
				data:'by_external=true'+
           '&team_id='+$('#team_id').val()+
					 '&month='+monthnumber+
					 '&year='+year+
					 '&lastday='+getDayName(monthnumber,year,1,0),
				success:function(data){
					$('#calendar').html(data);
				}
			});
		});
	});
	
	$(document).ready(function(){
		$('#by_internal').on('click',function(){
			$.ajax(
			{
				url:'pmCalendarFilter.php',
				type:'post',
				data:'by_internal=true'+
           '&team_id='+$('#team_id').val()+
					 '&month='+monthnumber+
					 '&year='+year+
					 '&lastday='+getDayName(monthnumber,year,1,0),
				success:function(data){
					$('#calendar').html(data);
				}
			});
		});
	});
	
	$(document).ready(function(){
		$('#by_all').on('click',function(){
			$.ajax(
			{
				url:'pmCalendarFilter.php',
				type:'post',
				data:'by_team=true'+
           '&team_id='+$('#team_id').val()+
					 '&month='+monthnumber+
					 '&year='+year+
					 '&lastday='+getDayName(monthnumber,year,1,0),
				success:function(data){
					$('#calendar').html(data);
				}
			});
		});
	});
	
	$(document).ready(function(){
		$('#by_team').on('change',function(){
			var team_id = $(this).val();
			$.ajax(
			{
				url:'pmCalendarFilter.php',
				type:'post',
				data:'by_team=true'+
					 '&month='+monthnumber+
					 '&year='+year+
					 '&lastday='+getDayName(monthnumber,year,1,0)+
					 '&team_id='+team_id,
				success:function(data){
					$('#calendar').html(data);
				}
			});
		});
	});
	
	$("#by_project").click(function (e) {
			$(".datalist2").show();
			e.stopPropagation();
		});

		$(".datalist2").click(function (e) {
			e.stopPropagation();
		});


    $(".event").click(function (e) {
			e.stopPropagation();
		});
    
		$(document).click(function () {
			$(".datalist2").hide();
		});
		function hideIdlPop(){			
		$('#pop_edit_idl').hide();
	}

	function showIdlPop(){
		$('#pop_edit_idl').show();
	}

</script>
</html>