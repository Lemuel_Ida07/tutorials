<?php
  include("connect.php");
  
  $today = date("F j, Y");
?>
<style>
  #navbar tr td#assigned{
    background-color: #deebff;
    color:#0747a6;
  }
  
  #navbar tr td#des_tracking{
    background-color: #0747a6;
    color:#deebff;
  }
</style>

<div id="list" class="width-99pc">
  <div class="miniheader">
    <label class="h2"><?php echo $today; ?></label>
  </div>
  <div class="container2">
    <table class="general_table width-100pc">
      <thead>
        <tr>
          <th> Designer </th>
          <th> Deadline </th>
          <th> Project </th>
          <th> Requirements </th>
          <th> Budget </th>
          <th> Ticket </th>
          <th> Status </th>
        </tr>
      </thead>
      <tbody id="weeklySchedule">
       <!-- Data will be sent from pushSchedule.php -->
      </tbody>
    </table>
  </div>			
</div>				