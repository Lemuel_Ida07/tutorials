<?php
	session_start();
	include('connect.php');
	include("phpScript.php");
	if(isset($_SESSION["User_Type"]))
	{
	$user_type = $_SESSION['User_Type'];
	switch($user_type)
	{
		case 0:	//Designer
			header("Location:home.php");
			break;
		case 1:	//Reviewer
			header("Location:reviewer.php");
			break;
		case 2:	//DC
			header("Location:dc.php");
			break;
		case 3:	//PM
			header("Location:pm.php");
			break;
		case 4:	//Operations
			header("Location:ops.php");
			break;
		case 8: //CAD Manager
			header("Location:cadManager.php");
			break;
		case 9: //CAD Technician
			header("Location:cadTechnician.php");	
			break;
		default:
			break;
	}
            
	
	$today = date("F j, Y");
	$date_today = date("Y-m-d");
	$total_time = getTotalHours();

	if(isset($_POST['add']))
	{
		$Designer_ID = $_SESSION['ID'];
		$Date = $date_today;
		$Project_ID = $_POST['project_id'];
		$Ticket_Number = addslashes($_POST['TicketNumber']);
		$Activity = addslashes($_POST['Activity']);
		$Time_In = $_POST['TimeIn'];
		$Time_Out = $_POST['TimeOut']; 
		$Duration = get_time_difference($Time_In,$Time_Out);
		$Status = "Normal";

		$sql = "INSERT INTO activity_logs(Designer_ID, Date, Project_ID, Ticket_Number, Activity, Time_In, Time_Out, Duration, Status)
				    VALUES($Designer_ID,CURDATE(),$Project_ID,'$Ticket_Number','$Activity','$Time_In','$Time_Out','$Duration','$Status')";
		if(mysqli_query($conn,$sql))
		{
			$total_time = getTotalHours();
			header("Location:admin.php");
		}
		}

	
	if(isset($_POST["edit"]))
	{
		$id = $_POST['editID'];
		$activity = $_POST["editActivity"];
		$date = $_POST['editDate'];
		$timein = date($_POST["editTimeIn"]);
		$timeout = date($_POST["editTimeOut"]);
		$duration = get_time_difference($timein,$timeout);
		$designer_id =  $_SESSION["ID"];

		$sql = "UPDATE activity_logs SET Activity = '$activity', Date = CURDATE(), Time_In = '$timein',Time_Out = '$timeout',Duration = '$duration' WHERE Designer_ID = '$designer_id' AND ID = '$id'";
		if(mysqli_query($conn,$sql))
		{
			$total_time = getTotalHours();
			header("Location:admin.php");
		}
	}
	
	if(isset($_POST["delete"]))
	{
		$id = $_POST['editID'];
		$designer_id =  $_SESSION["ID"];
		
		$deletesql = "DELETE FROM activity_logs WHERE Designer_ID = '$designer_id' AND ID = '$id' ";
		if(mysqli_query($conn,$deletesql))
		{	
			$total_time = getTotalHours();
			header("Location:admin.php");
		}
	}
}
else
	{
                session_destroy();
		header("Location:index.php");
	}
?>
<html>
	<head>
		<title> Activity Monitoring </title>
		<link rel="stylesheet" type="text/css" href="css/style.css"/>
		<link rel="stylesheet" type="text/css" href = "css/sidebar.css"/>
		<link rel="icon" href="img/logoblue.png">
		<script src="scripts/javaScript.js"></script>
		<script src="jquery-3.2.1.min.js"></script>
	</head>
	<style>
		#menu_item_logo1
		{
			background-color:#f2f2f2;
		}
		#menu_item_logo1:hover
		{
			background-color:#f2f2f2;
		}
		#tab1
		{
			color:#515151;
			font-weight: bold;
			text-shadow: none;
		}
	</style>
	<body onload="Init()">
            <?php include('header.php') ?>
            <div id="side_bar">
            <!-- SIDE BAR MENU -->
                <table id="sidebar_menu" border=0>
                    <tr>
                        <td id="menu_item_logo1" nowrap><label id="tab1"> Activity </label></td>
                    </tr>
                    <tr>
                        <td id="menu_item_logo2" nowrap><label id="tab2"> History </label></td>
                    </tr>
                </table>
            </div>
            <div id="mainform">
		<div id="info">						
		<form id="activityForm" method="post" onsubmit="return confirm('Are you sure you want to Add this Activity?');">
			<h2>My Accomplishment Report </h2>
			<h2><?php echo $_SESSION['Team_Name']; ?></h2>
			<h3><?php echo $today; ?></h3>
			<h1><?php echo $total_time; ?></h1>
				<textarea id="Activity" name="Activity" rows="2" cols="20" placeholder="What did you work on?"></textarea>
				<label> Please Select a Project Name. </label><br>
				<input type="text" name="searchbox" id="searchbox" style="width:100%;padding:20px;" placeholder="Select a project." autocomplete="off" onkeyup="filter('searchbox','datalist')"/>			
				<input type="text" name="project_id" id="project_id" style="display:none;" />
				<div id="datalist" class="datalist"><?php
					$sql = "SELECT ID,Project_Number,Project_Name FROM project";
					$result = mysqli_query($conn,$sql);
					if(mysqli_num_rows($result) > 0)
					{
					while($rows = mysqli_fetch_assoc($result))
					{
					echo "<h4 id='".$rows['ID']."' onmousedown='hideList();passData".$rows['ID']."();'> ".$rows['Project_Number']." - ".$rows['Project_Name']." </h4>";
					echo "<script>
					function passData".$rows['ID']."()
					{
					document.getElementById('searchbox').value = document.getElementById('".$rows['ID']."').innerHTML;
					document.getElementById('project_id').value='".$rows['ID']."';
					}
					</script>";
					}
					}
					?>
				</div>
				<input type="text"  name="TicketNumber" id="ticket"  placeholder="Ticket Number" />
				<label id="label1"> From </label><label id="label2"> To </label><br/> 
				<input id="inptext1" type="time" name="TimeIn" /><input id="inptext2" type="time" name="TimeOut" />
				<input type="submit" value="Add" name="add"   id="addbtn"/>
				</form>
			</div>
			<div id="list">
				<form method="post" onsubmit="return confirm('Are you sure you want to save changes?');">
					<table class='general_table width-100pc'>
						<?php
						$output = '';
						$output .= "
							<thead>
							<tr><th>Project </th><th>Ticket No.</th><th>Activity</th><th>Date</th><th>Time In</th><th>Time Out</th><th>Duration</th><th>Actions</th></tr>
							</thead>
								<tbody>";
							$designer_id = $_SESSION["ID"];
							$sql = "select project.Project_Number,
                project.Project_Name,
                activity_logs.ID,
                activity_logs.Project_ID,
                activity_logs.Ticket_Number,
                activity_logs.Activity,
                activity_logs.Date,
                activity_logs.Time_In,
                activity_logs.Time_Out,
                activity_logs.Duration 
                FROM activity_logs 
                INNER JOIN project ON activity_logs.Project_ID = project.ID 
                where Designer_ID = $designer_id and Date = CURDATE()";
							$result = mysqli_query($conn,$sql);
							if(mysqli_num_rows($result) > 0)
							{
							while($rows = mysqli_fetch_assoc($result))
							{
							$output.= "
								<tr>
								<td style='display:none'><input type='text' value='". $rows["ID"] ."' name='editID'></td>
								<td>". $rows["Project_Number"] ." - ". $rows["Project_Name"] ."</td>
								<td>". $rows["Ticket_Number"] ."</td>
								<td><input type='text' value='". $rows["Activity"] ."' name='editActivity'></td>
								<td><input type='text' value='". $rows["Date"] ."' name='editDate'></td>
								<td><input type='time' value='". $rows["Time_In"] ."' name = 'editTimeIn'></td>
								<td><input type='time' value='". $rows["Time_Out"] ."' name = 'editTimeOut'></td>
								<td><input type='text' value='". $rows["Duration"] ."' name = 'editDuration' readonly='readonly' /></td>
								<td><input id='editbtn' type='submit' value='Edit' name='edit' /><input id='deletebtn' type='submit' value='Delete' name='delete' /></td>
								</tr>";
							}
							$output .="
							</tbody>";
							echo $output;
						} 
						?>
					</table>
				</form>
			</div>	
		</div>
	</body>

	<script>
	function Init(){
		updateReport();
	}

	function updateReport()
	{
		$.ajax(
		{
			url:'table.php',
			type:'post',
			data:'submit=true',
			success:function(data)
			{
			},
		});
	}

	$(document).ready(function(){
		$('#menu_item_logo1').on('click',function(){
			location.reload();
		});
	});
			
	$(document).ready(function(){
		$('#menu_item_logo2').on('click',function(){
			var value = $(this).val();
				$.ajax(
				{
					url:'activityHistory.php',
					type:'post',
					data:'request='+value,
					success:function(data)
					{   	   		
						styleTt();
						$('#mainform').html(data);
						document.getElementById('Activity').style.display = "block";
					},
				});
		});
	});

	function styleTt()
	{
		document.getElementById('menu_item_logo1').style.backgroundColor = '#3498db';
		document.getElementById('tab1').style.textShadow = '1px 1px 5px #000000';
		document.getElementById('tab1').style.color = 'white';			 
		document.getElementById('tab1').style.fontWeight = 'normal';
	}

	function unstyleTt()
	{
		document.getElementById('menu_item_logo1').style.backgroundColor = '#f2f2f2'; 
		document.getElementById('tab1').style.textShadow = 'none';
		document.getElementById('tab1').style.color = '#515151';			 
		document.getElementById('tab1').style.fontWeight = 'bold';
	}

	function hideList() {
		if(document.getElementById('datalist').style.display == "block")
		var element = document.getElementById('datalist').style.display = "none";
	}


	function filter(inputid,divid) {
		var input, filter, list, count;
		input = document.getElementById(inputid);
		filter = input.value.toUpperCase();
		div = document.getElementById(divid);
		list = div.getElementsByTagName("h4");
		for(count = 0; count < list.length; count ++)
		{
			if (list[count].innerHTML.toUpperCase().indexOf(filter) > -1)
			{
				list[count].style.display = "";
			} else {
				list[count].style.display = "none";
			}
		}
	}

function removeName(node) {
	var child = node;

	parent = child.parentNode;

	$('#' + parent.id).remove();

}

$("#searchbox").keypress(function (e) {
			$(".datalist").show();
			e.stopPropagation();
		});

$("#searchbox").click(function (e) {
			$(".datalist").show();
			e.stopPropagation();
		});

		$(".datalist").click(function (e) {
			e.stopPropagation();
		});

		$(document).click(function () {
			$(".datalist").hide();
		});
						
		loadSubmitFunctions();										
</script>
</html>