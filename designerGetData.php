<?php
  session_start();
  include("connect.php");
  
  if(isset($_POST["get_design_tracking"]))
  {
    $curr_time = date("h:i");
		$designer_id = $_SESSION['ID'];
		$dnotif_id = 0;
		$curr_year = date("Y");
		$curr_month = date("m");
		$curr_day = date("d");
    $tradeString = $_SESSION["Trade"];
		if($curr_day <= 15)
    {
      $day_limit = 15;																																																																													 
    }
    
    if($curr_day > 15)
		{
      $day_limit = 31; 
    }
        
    $status = $_POST["status"];
   
    $sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        requirements_for_des.Budget,
        designer_notif.Deadline,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID
      INNER JOIN requirements_for_des
        ON designer_notif.ID = requirements_for_des.DNotif_ID
      INNER JOIN review_checklist
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID
      WHERE designer_notif.Designer_ID = $designer_id	
        AND requirements_for_des.Budget > 0
        AND review_checklist.Trade = '$tradeString'
      ORDER BY designer_notif.ID ASC";
    
    $result = mysqli_query($conn,$sql);

    if(mysqli_num_rows($result) > 0)
    {
      $count = 0;
      $requested_count = 0;
      $approved_count = 0;
      while($rows = mysqli_fetch_assoc($result))
      {
        switch($status){
          case "Pending":
            if($rows["Status"] == "Pending")
            {
            $count ++;
            echo "<tr id='tr".$rows["DNotif_ID"]."'>";
        echo "
              <td class='emphasize'> ".$rows["Deadline"]." </td>
              <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
              <td> ".$rows["Check_Item"]." </td>
              <td class='emphasize'> ".$rows["Budget"]." Hours </td>
              <td> ".$rows["Ticket_Number"]." </td>
              <td>";
            echo"<input 
                  id='".$rows["req_for_des_id"]."'
                  type='submit' 
                  value='Request' 
                  class='btn-normal bg-blue white request_btn bold'
                  />";
                echo "<script>
                        $('#pending_count').html('($count)');
                        $('#".$rows["req_for_des_id"]."').on('click',function(){
                          $.ajax({
                            beforeSend:function(){
                              return confirm('Request ".$rows["Check_Item"]." for review?');
                            },
                            url:'designerSubmit.php',
                              type:'post',
                              data:
                                {
                                  start_review:true,
                                  reqfordes_id:".$rows["req_for_des_id"]."
                                },
                            success:function(data){
                              $('#des_tracking').click();
                            },
                            error:function(data){
                              alert(data);
                            }
                          });
                        });
                      </script>";       
                echo "</td>
                      </tr>";
            }
            break;
          case "Requested":
            if($rows["Status"] == "Requested")
            {
              $requested_count ++;
              echo "<tr id='tr".$rows["DNotif_ID"]."'>";
              echo "
                    <td class='emphasize'> ".$rows["Deadline"]." </td>
                    <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                    <td> ".$rows["Check_Item"]." </td>
                    <td class='emphasize'> ".$rows["Budget"]." Hours </td>
                    <td> ".$rows["Ticket_Number"]." </td>
                    <td style='display:none;'>";

              echo"<script>
                    $('#requested_count').html('($requested_count)');
                </script>";    
              echo "</td>
                    </tr>";
            }
          break;
          case "Approved":
            if($rows["Status"] == "Approved" || $rows["Status"] == "Assigned"
               || $rows["Status"] == "CAD Requested" || $rows["Status"] == "CAD Accepted"
               || $rows["Status"] == "Designer CAD Accepted" || $rows["Status"] == "Reviewer CAD Accepted")
            {
              $approved_count ++;
            echo "<tr id='tr".$rows["DNotif_ID"]."'>";
        echo "
              <td class='emphasize'> ".$rows["Deadline"]." </td>
              <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
              <td> ".$rows["Check_Item"]." </td>
              <td class='emphasize'> ".$rows["Budget"]." Hours </td>
              <td> ".$rows["Ticket_Number"]." </td>
              <td style='display:none;'>";
            echo"<script>
                  $('#approved_count').html('($approved_count)');
                </script>";    
              echo "</td>
                    </tr>";
            }
            break;
          case "Rejected":
            if($rows["Status"] == "Rejected")
            {
            $count ++;
            echo "<tr id='tr".$rows["DNotif_ID"]."'>";
        echo "
              <td class='emphasize'> ".$rows["Deadline"]." </td>
              <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
              <td> ".$rows["Check_Item"]." </td>
              <td class='emphasize'> ".$rows["Budget"]." Hours </td>
              <td> ".$rows["Ticket_Number"]." </td>
              <td>";
            echo"<input 
                  id='".$rows["req_for_des_id"]."'
                  type='submit' 
                  value='Re-request' 
                  class='btn-normal bg-blue white request_btn bold'
                  />";
                echo "<script>
                        $('#rejected_count').html('($count)');
                        $('#".$rows["req_for_des_id"]."').on('click',function(){
                          $.ajax({
                            beforeSend:function(){
                              return confirm('Re-request ".$rows["Check_Item"]." for review?');
                            },
                            url:'designerSubmit.php',
                              type:'post',
                              data:
                                {
                                  start_review:true,
                                  reqfordes_id:".$rows["req_for_des_id"]."
                                },
                            success:function(data){
                              $('#des_tracking').click();
                            },
                            error:function(data){
                              alert(data);
                            }
                          });
                        });
                      </script>";       
                echo "</td>
                      </tr>";
            }
            
          break;
        }
      }
    }
    else
    {
      echo "<tr>
            <td>
              <script>
                $('#pending_count').html('(0)');
                $('#requested_count').html('(0)');
                $('#rejected_count').html('(0)');
                $('#approved_count').html('(0)');
              </script>
            </td>
          </tr>";
    }
  }
  
  if(isset($_POST["get_cad_tracking"]))
  {
    $status = $_POST["status"];
    
    $curr_time = date("h:i");
		$designer_id = $_SESSION['ID'];
		$dnotif_id = 0;
		$curr_year = date("Y");
		$curr_month = date("m");
		$curr_day = date("d");
    $tradeString = $_SESSION["Trade"];
		
    if($curr_day <= 15)
    {
      $day_limit = 15;																																																																													 
    }
    
    if($curr_day > 15)
		{
      $day_limit = 31; 
    }
    
    /*$sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        requirements_for_des.Budget,
        designer_notif.Deadline,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status,
        CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID 
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID 
      INNER JOIN requirements_for_des 
        ON designer_notif.ID = requirements_for_des.DNotif_ID 
      INNER JOIN review_checklist 
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID 
      INNER JOIN user 
        ON user.ID = designer_notif.Designer_ID 
      WHERE 
        user.ID = $designer_id
        AND EXTRACT(YEAR FROM(designer_notif.Deadline)) <= $curr_year 
        AND EXTRACT(MONTH FROM(designer_notif.Deadline)) <= $curr_month 
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) <= $day_limit 
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) >= EXTRACT(DAY FROM(CURDATE())) 
        AND requirements_for_des.Budget > 0 
        AND review_checklist.Trade = '$tradeString' 
      ORDER BY designer_notif.Deadline";*/
    $sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        requirements_for_des.Budget,
        designer_notif.Deadline,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status,
        CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID 
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID 
      INNER JOIN requirements_for_des 
        ON designer_notif.ID = requirements_for_des.DNotif_ID 
      INNER JOIN review_checklist 
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID 
      INNER JOIN user 
        ON user.ID = designer_notif.Designer_ID 
      WHERE 
        user.ID = $designer_id
        AND requirements_for_des.Budget > 0 
        AND review_checklist.Trade = '$tradeString' 
      ORDER BY designer_notif.Deadline";
		$result = mysqli_query($conn,$sql); 
		if(mysqli_num_rows($result) > 0)
		{
                $standby_count = 0;
                $assigned_count = 0;
                $approved_count = 0;
                $requested_count = 0;
                $rejected_count = 0;
                $accepted_count = 0;
			while($rows = mysqli_fetch_assoc($result))
			{
        switch($status){
          case "Approved":
            if($rows["Status"] == "Approved")
            {
              $cadtechsql 
              = "SELECT 
                  user.Username, 
                  user.ID AS CADTech_ID, 
                  cadtech_notif.Status 
                FROM cadtech_notif 
                INNER JOIN user 
                  ON cadtech_notif.CadTech_ID = user.ID 
                WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"].""; 
              $cadtechresult = mysqli_query($conn,$cadtechsql); 
              if(mysqli_num_rows($cadtechresult) > 0){ 
                while($cadtechrow = mysqli_fetch_assoc($cadtechresult))
                {
                switch($cadtechrow["Status"]) 
                { 
                  case "Standby": 
                    $assigned_count ++; 
                    echo "<tr style='display:none'> 
                            <td> 
                              <script> 
                                $('#assigned_count').html('($assigned_count)'); 
                              </script>
                            </td>
                          </tr>";
                    break;
                  case "Working": 
                    $assigned_count ++; 
                    echo "<tr style='display:none'> 
                            <td> 
                              <script> 
                                $('#assigned_count').html('($assigned_count)'); 
                              </script>
                            </td>
                          </tr>";
                    break;
                  case "CAD Requested": 
                    $requested_count ++; 
                    echo "<tr style='display:none'> 
                            <td> 
                              <script> 
                                $('#requested_count').html('($requested_count)'); 
                              </script>
                            </td>
                          </tr>";
                    break;
                  case "CAD Rejected": 
                    $rejected_count ++; 
                    echo "<tr style='display:none'> 
                            <td> 
                              <script> 
                                $('#rejected_count').html('($rejected_count)'); 
                              </script>
                            </td>
                          </tr>";
                    break;
                  case "CAD Accepted": 
                    $accepted_count ++; 
                    echo "<tr style='display:none'> 
                            <td> 
                              <script> 
                                $('#approved_count').html('($accepted_count)'); 
                              </script>
                            </td>
                          </tr>";
                    $requested_count ++;
                    echo "<tr style='display:none;'>
                          <td>
                            <script>
                              $('#requested_count').html('($requested_count)');
                            </script>
                          </td>
                        </tr>";
                    break;
                }
                }
            }
            else
            {
              $standby_count ++;
              echo "<tr id='tr".$rows["DNotif_ID"]."'>";
              echo "
                <td> ".$rows["Designer"]." </td>
                <td class='emphasize'> ".$rows["Deadline"]." </td>
                <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                <td> ".$rows["Check_Item"]." </td>
                <td class='red emphasize'> N/A</td>
                <td> ".$rows["Ticket_Number"]." </td>";
              echo "<td style='display:none;'>
                      <script>
                        $('#standby_count').html('($standby_count)');
                      </script>
                    </td>";
              echo"
                <td><input
                type='button' 
                value='Standby' 
                class='btn-no bg-white red emphasize'
                /></td>
                <td><input
                type='button' 
                value='N/A' 
                class='btn-no bg-white red emphasize'
                /></td>
                ";
            }
            }
        break;
          case "Assigned":
            if($rows["Status"] == "Approved")
            {
              $cadtechsql1 
              = "SELECT 
                  user.Username,
                  user.ID AS CADTech_ID,
                  cadtech_notif.Status,
                  cadtech_notif.Budget
                FROM cadtech_notif 
                INNER JOIN user 
                  ON cadtech_notif.CadTech_ID = user.ID
                WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"]."
                ORDER BY cadtech_notif.Status";
              $cadtechresult1 = mysqli_query($conn,$cadtechsql1);
              if(mysqli_num_rows($cadtechresult1) > 0){
                while($cadtechrow1 = mysqli_fetch_assoc($cadtechresult1)){
                  switch($cadtechrow1["Status"])
                  {
                    case "Standby":
                      $assigned_count ++;
                      echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                      echo "
                        <td> ".$rows["Designer"]."</td>
                        <td class='emphasize'> ".$rows["Deadline"]." </td>
                        <td> ".$rows["Project_Number"]." - 
                             ".$rows["Project_Name"]." - 
                             ".$rows["Phase"]." </td>
                        <td> ".$rows["Check_Item"]." </td>
                          <td class='emphasize'> ".$cadtechrow1["Budget"]." Hours </td>
                        <td> ".$rows["Ticket_Number"]." </td>";
                      echo"
                          <td><input
                          type='button' 
                          value='Standby' 
                          class='btn-no bg-white gray emphasize'
                          /></td>";
                        echo "<td> 
                                <label id='assigned'>";
                        echo " <span class='bg-gray' style='position:absolute;width:12px; height:12px; border-radius:100%; margin:0px; padding:0px;'> </span> 
                               <span style='margin-left:16px;'>".$cadtechrow1["Username"]."<br /></span><br />";   
                         
                        echo "</label>
                                <script> 
                                  $('#assigned_count').html('($assigned_count)');
                                </script>
                              </td>
                              </tr>";

                    break;
                    case "Working":
                      $assigned_count ++;
                      echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                      echo "
                        <td> ".$rows["Designer"]."</td>
                        <td class='emphasize'> ".$rows["Deadline"]." </td>
                        <td> ".$rows["Project_Number"]." - 
                             ".$rows["Project_Name"]." - 
                             ".$rows["Phase"]." </td>
                        <td> ".$rows["Check_Item"]." </td>
                          <td class='emphasize'> ".$cadtechrow1["Budget"]." Hours </td>
                        <td> ".$rows["Ticket_Number"]." </td>";
                      echo"
                          <td><input
                          type='button' 
                          value='Working' 
                          class='btn-no bg-white yellow emphasize'
                          /></td>";
                        echo "<td> 
                                <label id='assigned'>"; 
                        echo " <span class='bg-yellow' style='position:absolute;width:12px; height:12px;  border-radius:100%; margin:0px; padding:0px;'> </span> 
                               <span style='margin-left:16px;'>".$cadtechrow1["Username"]."<br /></span><br />";   
        
                        echo "</label>
                                <script> 
                                  $('#assigned_count').html('($assigned_count)');
                                </script>
                              </td>
                              </tr>";

                    break;
                  }
                }
              }
            }
          break;
          case "CADRequested":
            if($rows["Status"] == "Approved")
            {
        $cadtechsql 
          = "SELECT 
              user.Username, 
              user.ID AS CADTech_ID, 
              cadtech_notif.Status, 
              cadtech_notif.Budget,
              cadtech_notif.ID
            FROM cadtech_notif 
            INNER JOIN user 
              ON cadtech_notif.CadTech_ID = user.ID 
            WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"].""; 
        $cadtechresult = mysqli_query($conn,$cadtechsql); 
        if(mysqli_num_rows($cadtechresult) > 0){ 
          while($cadtechrows = mysqli_fetch_assoc($cadtechresult)) 
          { 
            switch($cadtechrows["Status"]) 
            { 
              case "CAD Requested": 
                $requested_count ++; 
                echo "<tr id='tr".$rows["DNotif_ID"]."'>"; 
                echo " 
                      <td> ".$rows["Designer"]." </td> 
                      <td class='emphasize'> ".$rows["Deadline"]." </td> 
                      <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td> 
                      <td> ".$rows["Check_Item"]." </td> 
                      <td class='emphasize'> ".$cadtechrows["Budget"]." Hours </td> 
                      <td> ".$rows["Ticket_Number"]." </td>"; 
                echo" <td> 
                        <input 
                        type='button' 
                        value='C' 
                        class='btn-no bg-blue white emphasize' 
                        /> 

                        <input 
                        type='button' 
                        value='D' 
                        class='btn-no bg-gray white emphasize' 
                        />

                        <input 
                        type='button' 
                        value='R' 
                        class='btn-no bg-gray white emphasize' 
                        />
                      </td>";  
                echo "<td>
                        <label id='assigned'>";
                echo "    <span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />";   
                echo "  </label>
                        <script> 
                          $('#requested_count').html('($requested_count)');
                        </script>
                      </td>
                      </tr>";   
                break;
              case "CAD Accepted":
                $requested_count ++;
                echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                echo "
                      <td> ".$rows["Designer"]." </td>
                      <td class='emphasize'> ".$rows["Deadline"]." </td>
                      <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                      <td> ".$rows["Check_Item"]." </td>
                      <td> ".$cadtechrows["Budget"]." Hours</td>
                      <td> ".$rows["Ticket_Number"]." </td>";
                echo"
                    <td>
                    <input type='submit'
                             id='accept_btn".$rows["req_for_des_id"]."'
                             value='Accept'
                             class='btn-normal bg-green white emphasize' />

                      <input type='submit'
                             id='reject_btn".$rows["req_for_des_id"]."'
                             value='Reject'
                             class='btn-normal bg-red white emphasize' />

                      <script> 
                        $('#requested_count').html('($requested_count)');
                          $('#accept_btn".$rows["req_for_des_id"]."').on('click',function(){
                            $.ajax({
                              beforeSend:function(){
                                return confirm('Are yous sure you want to Accept?');
                              },
                              url:'designerSubmit.php',
                              type:'post',
                              data:'accept_cad=true'+
                                   '&cadtech_notif_id=".$cadtechrows["ID"]."'+
                                   '&req_for_des_id=".$rows["req_for_des_id"]."',
                              success:function(data){
                                location.reload();
                              }
                            });
                          });

                          $('#reject_btn".$rows["req_for_des_id"]."').on('click',function(){
                            $.ajax({
                              beforeSend:function(){
                                return confirm('Are you sure you want to Reject?');
                              },
                              url:'designerSubmit.php',
                              type:'post',
                              data:'reject_cad=true'+
                                   '&cadtech_notif_id=".$cadtechrows["ID"]."'+
                                   '&req_for_des_id=".$rows["req_for_des_id"]."',
                              success:function(data){
                              }
                            });
                          });
                      </script></td>";
                echo "<td>
                        <label id='assigned'>";
                echo "    <span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />";   
                echo "  </label>
                        <script> 
                          $('#requested_count').html('($requested_count)');
                        </script>
                      </td>
                      </tr>";   
                break;
            } 
          }
        }
            }
          break;
          case "CADAccepted":
            if($rows["Status"] == "Approved")
            {
              $cadtechsql 
              = "SELECT 
                  user.Username, 
                  user.ID AS CADTech_ID, 
                  cadtech_notif.Status,
                  cadtech_notif.Budget
                FROM cadtech_notif 
                INNER JOIN user 
                  ON cadtech_notif.CadTech_ID = user.ID 
                WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"].""; 
              $cadtechresult = mysqli_query($conn,$cadtechsql); 
              if(mysqli_num_rows($cadtechresult) > 0){
                while($cadtechrows = mysqli_fetch_assoc($cadtechresult))
                {
                  switch($cadtechrows["Status"])
                  {
                    case "CAD Accepted":
                       $accepted_count ++; 
                        echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                        echo "<td> ".$rows["Designer"]." </td>
                              <td class='emphasize'> ".$rows["Deadline"]." </td>
                              <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                              <td> ".$rows["Check_Item"]." </td>
                              <td class='emphasize'> ".$cadtechrows["Budget"]." Hours </td>
                              <td> ".$rows["Ticket_Number"]." </td>";
                        echo" <td> 
                                <input 
                                type='button' 
                                value='C' 
                                class='btn-no bg-green white emphasize' 
                                /> 

                                <input 
                                type='button' 
                                value='D' 
                                class='btn-no bg-gray white emphasize' 
                                /> 

                                <input 
                                type='button' 
                                value='R' 
                                class='btn-no bg-gray white emphasize' 
                                />
                              </td>";  

                        echo "<td>
                                <label id='assigned'>";

                        echo "<span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />"; 

                        echo "</label>
                                 <script> 
                              $('#approved_count').html('($accepted_count)'); 
                                 </script> 
                               </td>
                               </tr>";
                    break;
                    case "Designer CAD Accepted":
                      $approved_count ++;
                      echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                      echo "<td> ".$rows["Designer"]." </td>
                            <td class='emphasize'> ".$rows["Deadline"]." </td>
                            <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                            <td> ".$rows["Check_Item"]." </td>
                            <td class='emphasize'> ".$cadtechrows["Budget"]." Hours </td>
                            <td> ".$rows["Ticket_Number"]." </td>";
                      echo" <td>
                              <input 
                              type='button' 
                              value='C' 
                              class='btn-no bg-green white emphasize' 
                              />

                              <input 
                              type='button' 
                              value='D' 
                              class='btn-no bg-green white emphasize' 
                              />

                              <input 
                              type='button' 
                              value='R' 
                              class='btn-no bg-gray white emphasize' 
                              />
                            </td>";  
                      echo "<td>
                              <label id='assigned'>";
                      echo "<span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />"; 
                      echo "</label>
                               <script> 
                                  $('#approved_count').html('($approved_count)');
                               </script>
                             </td>
                            </tr>";  
                      break;
                    case "Reviewer CAD Accepted":
                      $approved_count ++;
                      echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                      echo "<td> ".$rows["Designer"]." </td>
                            <td class='emphasize'> ".$rows["Deadline"]." </td>
                            <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                            <td> ".$rows["Check_Item"]." </td>
                            <td class='emphasize'> ".$cadtechrows["Budget"]." Hours </td>
                            <td> ".$rows["Ticket_Number"]." </td>";
                      echo" <td>
                              <input 
                              type='button' 
                              value='C' 
                              class='btn-no bg-green white emphasize' 
                              />

                              <input 
                              type='button' 
                              value='D' 
                              class='btn-no bg-green white emphasize' 
                              />

                              <input 
                              type='button' 
                              value='R' 
                              class='btn-no bg-green white emphasize' 
                              />
                            </td>";  
                      echo "<td>
                              <label id='assigned'>";
                      echo "<span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />"; 
                      echo "</label>
                               <script> 
                                  $('#approved_count').html('($approved_count)');
                               </script>
                             </td>
                            </tr>";  
                      break;
                  } 
                }
              }
            }
          break;
          case "CADRejected":
            if($rows["Status"] == "Approved")
            {
              $cadtechsql 
          = "SELECT 
              user.Username, 
              user.ID AS CADTech_ID, 
              cadtech_notif.Status,
              cadtech_notif.Budget
            FROM cadtech_notif 
            INNER JOIN user 
              ON cadtech_notif.CadTech_ID = user.ID 
            WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"].""; 
        $cadtechresult = mysqli_query($conn,$cadtechsql); 
        if(mysqli_num_rows($cadtechresult) > 0){
          while($cadtechrows = mysqli_fetch_assoc($cadtechresult))
          {
            if($cadtechrows["Status"] == "CAD Rejected")
            {
                 $rejected_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                  echo "<td> ".$rows["Designer"]." </td>
                        <td class='emphasize'> ".$rows["Deadline"]." </td>
                        <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                        <td> ".$rows["Check_Item"]." </td>
                        <td class='emphasize'> ".$cadtechrows["Budget"]." Hours </td>
                        <td> ".$rows["Ticket_Number"]." </td>";
                  echo" <td>
                          <input 
                          type='button' 
                          value='C' 
                          class='btn-no bg-red white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='D' 
                          class='btn-no bg-gray white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='R' 
                          class='btn-no bg-gray white emphasize' 
                          />
                        </td>";  
                  echo "<td>
                          <label id='assigned'>";
                  echo "<span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />"; 
                  echo "</label>
                           <script> 
                        $('#rejected_count').html('($rejected_count)');
                           </script>
                         </td>
                         </tr>";
              }  
            if($cadtechrows["Status"] == "Designer CAD Rejected")
            {
                 $rejected_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                  echo "<td> ".$rows["Designer"]." </td>
                        <td class='emphasize'> ".$rows["Deadline"]." </td>
                        <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                        <td> ".$rows["Check_Item"]." </td>
                        <td class='emphasize'> ".$cadtechrows["Budget"]." Hours </td>
                        <td> ".$rows["Ticket_Number"]." </td>";
                  echo" <td>
                          <input 
                          type='button' 
                          value='C' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='D' 
                          class='btn-no bg-red white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='R' 
                          class='btn-no bg-gray white emphasize' 
                          />
                        </td>";  
                  echo "<td>
                          <label id='assigned'>";
                  echo "<span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />"; 
                  echo "</label>
                           <script> 
                        $('#rejected_count').html('($rejected_count)');
                           </script>
                         </td>
                         </tr>";
              }  
            if($cadtechrows["Status"] == "Reviewer CAD Rejected")
            {
                 $rejected_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                  echo "<td> ".$rows["Designer"]." </td>
                        <td class='emphasize'> ".$rows["Deadline"]." </td>
                        <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                        <td> ".$rows["Check_Item"]." </td>
                        <td class='emphasize'> ".$cadtechrows["Budget"]." Hours </td>
                        <td> ".$rows["Ticket_Number"]." </td>";
                  echo" <td>
                          <input 
                          type='button' 
                          value='C' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='D' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='R' 
                          class='btn-no bg-red white emphasize' 
                          />
                        </td>";  
                  echo "<td>
                          <label id='assigned'>";
                  echo "<span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />"; 
                  echo "</label>
                           <script> 
                        $('#rejected_count').html('($rejected_count)');
                           </script>
                         </td>
                         </tr>";
              } 
            }
          }
        }
        break;
        } 
			}
		}
  }