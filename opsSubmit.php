<?php
    session_start();
    include("connect.php");

    // <editor-fold defaultstate="collapsed" desc="FOR USER"> 
  if (isset($_POST['edit_user'])) 	{
        $user_id = $_POST['editid'];
        $user_username = $_POST['editdusername'];
        $user_fname = $_POST['editdfname'];
        $user_mname = $_POST['editdmname'];
        $user_lname = $_POST['editdlname'];
        $user_user_type = $_POST['edituser_type'];
        $user_team = $_POST['editteam'];
        $user_trade = $_POST['edittrade'];
        $user_status = $_POST['editstatus'];
        $sql = "UPDATE user 
						SET Username = '$user_username',
								Firstname = '$user_fname',
								Middlename = '$user_mname',
								Lastname = '$user_lname',
								User_Type = $user_user_type,
								Team_ID = $user_team,
								Trade = '$user_trade',
								Status = '$user_status' 
                        WHERE ID = '$user_id'";
        echo $sql;

        if (mysqli_query($conn, $sql)) 		{
                    }
	}
	// </editor-fold>


	#region FOR EXTERNAL DEADLINE
	/*Add External Deadline*/
	if(isset($_POST["add_external_deadline"]))
	{
		$project_id = $_POST["project_id"];
		$external_deadline = $_POST["external_deadline"];
		$external_deadline = date_create_from_format("Y-m-d",$external_deadline);
		$day_ed = (int)date_format($external_deadline,"d");
		$month_ed = (int)date_format($external_deadline,"m");
		$year_ed = (int)date_format($external_deadline,"Y");
		$external_deadline = date_format($external_deadline,"Y-m-d");
		$deliverables = $_POST["deliverables"];
		$phase = $_POST["phase"];

		//selects simillar data in database and inputted data
		$sql = "SELECT 
							ID 
						FROM external_deadline
						WHERE 
							Project_ID = $project_id
							AND Phase = '$phase'";
		$result = mysqli_query($conn,$sql);
		//If Inputted data didnt exists in database
		if(mysqli_num_rows($result) <= 0)
		{
			$savesql = "INSERT INTO external_deadline(
										Project_ID,
										External_Deadline,
										Day_ED,
										Month_ED,
										Year_ED,
										Deliverables,
										Input_Date,
										Phase)
									VALUES(
										$project_id,
										'$external_deadline',
										$day_ed,
										$month_ed,
										$year_ed,
										'$deliverables',
										CURDATE(),
										'$phase')";
				if(mysqli_query($conn,$savesql))
				{
					//select new inserted external deadline
					$readsql = "SELECT 
												MAX(ID) AS Externaldl_ID 
											FROM external_deadline";

					$result = mysqli_query($conn,$readsql);

					if(mysqli_num_rows($result) > 0)
					{
						$row = mysqli_fetch_assoc($result);
						
						$externaldl_id = $row["Externaldl_ID"];
						$internal_deadline = str_replace(" ","",$_POST["internal_deadline"]);
						$trades = $_POST["trades"];
						$tickets = $_POST["tickets"];
						for($count = 0; $count <= (count($tickets) - 1); $count++)
						{
							$trade = $trades[$count];
							$idl = $internal_deadline[$count];
							$ticket = $tickets[$count];
							insertInternalDeadline($externaldl_id,$trade,$ticket,$idl,$phase);

							insertNotification(getInternalDlId($ticket),$trade);
						}
					}
				}
		}
		else
		{
			echo "already exists!";
		}
	}
	/*Edit External Deadline*/
	if(isset($_POST["edit_external_dl"]))
	{
		$external_deadline = $_POST["external_deadline"];
		$phase = $_POST["phase"];
		$deliverables = $_POST["deliverables"];
		$externaldl_id = $_POST["externaldl_id"];
    $percent = $_POST["percent"];

		$date = date_create_from_format('Y-m-d', $external_deadline);

		$day_ed = (int)date_format($date,"d");
		$month_ed =(int) date_format($date,"m");
		$year_ed = (int)date_format($date,"Y");

		$sql = "UPDATE external_deadline
						SET External_Deadline = '$external_deadline',
								Phase = '$phase',
								Deliverables = '$deliverables',
								Day_ED = $day_ed,
								Month_ED = $month_ed,
								Year_ED = $year_ed,
                  Percent = $percent
						WHERE ID = $externaldl_id";
		if(mysqli_query($conn,$sql))
		{
			$updatesql = "UPDATE internal_deadline
										SET Phase = '$phase'
										WHERE Externaldl_ID = $externaldl_id";
			if(mysqli_query($conn,$updatesql))
			{
			}
		}
	}
	/*Delete External Deadline*/
	if(isset($_POST['delete_externaldl']))
	{
		$externaldl_ID = $_POST['externaldl_id'];
		$phase = $_POST['phase'];
		$delsql = "DELETE FROM external_deadline WHERE ID = $externaldl_ID AND Phase = '$phase'";
		if(mysqli_query($conn,$delsql))
		{
			$readsql = "SELECT ID from internal_deadline WHERE Externaldl_ID = $externaldl_ID  AND Phase = '$phase'";
			$result = mysqli_query($conn,$readsql);
			if(mysqli_num_rows($result) > 0)
			{
				while($rows = mysqli_fetch_assoc($result))
				{
					$delnotif = "DELETE FROM notification WHERE Internaldl_ID = ".$rows['ID']."";
					if(mysqli_query($conn,$delnotif))
					{}
					$delnotif2 = "DELETE FROM ticket WHERE Internaldl_ID = ".$rows['ID']."";
					if(mysqli_query($conn,$delnotif2))
					{}
					$deletenotif3 = "DELETE FROM designer_notif WHERE Internaldl_ID = ".$rows['ID']."";
					if(mysqli_query($conn,$deletenotif3))
					{}
				}
			}
			$delnotif = "DELETE FROM internal_deadline WHERE Externaldl_ID = $externaldl_ID  AND Phase = '$phase'";
			if(mysqli_query($conn,$delnotif))
			{}
		}
	}
	#endregion

	#region FOR INTERNAL DEADLINE
	
	/*Add Internal Deadline*/
	if(isset($_POST["add_internal_deadline"]))
	{
		$project_id = $_POST["project_id"];
		$phase = $_POST["phase"];
		if($_POST["internal_deadline"] != "")
		{										 
			$internal_deadline = DateTime::createFromFormat('Y-m-d',$_POST["internal_deadline"], new DateTimeZone('Asia/Manila'));
			$day_id = $internal_deadline->format("d");
			$month =	$internal_deadline->format("m");
			$year = $internal_deadline->format("Y");
			$ticket = trim(str_replace(' ','',$_POST['ticket']));
			$trade = trim(str_replace(' ','',$_POST['trade']));
																		
			$internal_deadline = $internal_deadline->format("Y-m-d");
		
		
			$readsql = "SELECT 
										ID,
										Project_ID,
										Month_ED 
									FROM external_deadline 
									WHERE 
										Project_ID = $project_id  
										AND Phase = '$phase'";
			$result = mysqli_query($conn,$readsql);
			$row = mysqli_fetch_assoc($result);
			$externaldl = $row['ID'];
			$dbmonth = $row['Month_ED'];
			if($ticket != "")
			{
				$readsql = "SELECT ID FROM internal_deadline WHERE TRIM(REPLACE(Ticket_Number,' ','')) = '$ticket'";
				$result = mysqli_query($conn,$readsql);
				if(!mysqli_num_rows($result) > 0 && $month == $dbmonth) // search Ticket Number in db like in the input
				{						
					$sql2 = "INSERT INTO internal_deadline
											(Externaldl_ID,
											Internal_Deadline,
											Day_ID,
											Month_ID,
											Year_ID,
											Ticket_Number,
											Trade,
											Input_Date,
											Phase) 
									VALUES
											($externaldl,
											'$internal_deadline',
											$day_id,
											$month,
											$year,
											'$ticket',
											'$trade',
											CURDATE(),
											'$phase')";
					if(mysqli_query($conn,$sql2))
					{
						$readinternaldlsql = "SELECT ID FROM internal_deadline WHERE Ticket_Number = '$ticket'";
						$readinternaldlresult = mysqli_query($conn,$readsql);
						if(!mysqli_num_rows($result) > 0)
						{
							$row = mysqli_fetch_assoc($readinternaldlresult);
							$notifsql = "UPDATE notification SET Internaldl_ID = ".$row['ID']." WHERE Internaldl_ID = 0 AND Trade = 'E'";
							if(mysqli_query($conn,$notifsql))
							{}
						}
						$ticketsql = "INSERT INTO ticket(Ticket_Number,Project_ID,Internaldl_ID) VALUES('$ticket',$externaldl,".$row['ID'].")";
						if(mysqli_query($conn,$ticketsql))
						{
						}
					}
				}
				else
				{
					echo "ticket_already_exists";
				}
			}
			else
			{
				echo "no_ticket";
			}
		}
		else
		{
			echo "no_internal_deadline";
		}
	}
	//Edit Internal Deadline
	if(isset($_POST["edit_internal_deadline"]))
	{
		$trade = $_POST["trade"];
		$ticket = $_POST["ticket"];
		$internaldl_id = $_POST["internaldl_id"];
		$internal_deadline = DateTime::createFromFormat('Y-m-d',$_POST["internal_deadline"], new DateTimeZone('Asia/Manila'));
		$day_id = $internal_deadline->format("d");
		$month_id =	$internal_deadline->format("m");
		$year_id = $internal_deadline->format("Y");
		$internal_deadline = $internal_deadline->format("Y-m-d");
		$updatesql = "UPDATE internal_deadline
									SET 
										Internal_Deadline = '$internal_deadline',
										Day_ID = $day_id,
										Month_ID = $month_id,
										Year_ID = $year_id,
										Ticket_Number = '$ticket'
									WHERE ID = $internaldl_id";
									echo $updatesql; 
		if(mysqli_query($conn,$updatesql))
		{}
	}
	//Delete Internal Deadline
	if(isset($_POST["delete_internal_deadline"]))
	{
		$internaldl_ID = $_POST['internaldl_id'];
		
		$delsql = "DELETE FROM internal_deadline WHERE ID = $internaldl_ID";
		if(mysqli_query($conn,$delsql))
		{
			$delnotif = "DELETE FROM notification WHERE Internaldl_ID = $internaldl_ID";
			if(mysqli_query($conn,$delnotif))
			{}
			$delnotif2 = "DELETE FROM ticket WHERE Internaldl_ID = $internaldl_ID";
			if(mysqli_query($conn,$delnotif2))
			{}
			$deletenotif3 = "DELETE FROM designer_notif WHERE Internaldl_ID = $internaldl_ID";
			if(mysqli_query($conn,$deletenotif3))
			{}
		}
	}
	#endregion

	#region FUNCTIONS
	function insertInternalDeadline($externaldl_id,$trade,$ticket_number,$internal_deadline,$phase)
	{
		include("connect.php");
		$internal_deadline = date_create_from_format("Y-m-d",$internal_deadline);
		$day_id = (int)date_format($internal_deadline,"d");
		$month_id = (int)date_format($internal_deadline,"m");
		$year_id = (int)date_format($internal_deadline,"Y");
		$internal_deadline = date_format($internal_deadline,"Y-m-d");

		$internaldlsql = "INSERT INTO internal_deadline(
												Externaldl_ID,
												Internal_Deadline,
												Day_ID,
												Month_ID,
												Year_ID,
												Trade,
												Ticket_Number,
												Input_Date,
												Phase)
											VALUES(
												$externaldl_id,
												'$internal_deadline',
												$day_id,
												$month_id,
												$year_id,
												'$trade',
												'$ticket_number',
												CURDATE(),
												'$phase')";
	if(mysqli_query($conn,$internaldlsql))
		{
		}
	}

	function insertNotification($internaldl_ID,$trade){
		include("connect.php");
		$readsql = "SELECT 
								ID 
							FROM user
							WHERE
								User_Type = 1
								AND Trade = '$trade'";
			$result = mysqli_query($conn,$readsql);

			if(mysqli_num_rows($result) > 0)
			{
				while($rows = mysqli_fetch_assoc($result))
				{
					$user_id = $rows["ID"];
					$sql = "INSERT INTO notification(
							User_ID,
							Internaldl_ID,
							Trade,
							Input_Date)
						VALUES(
							$user_id,
							$internaldl_ID,
							'$trade',
							CURDATE())";
					if(mysqli_query($conn,$sql))
					{

					}
				}
			}
	}
	#endregion

	#region FUNCTIONS W/ RETURN VALUES

		/* Get internal_deadline ID */
		function getInternalDlId($ticket){
			include("connect.php");
			
			$internaldl_id = 0;

			$sql = "SELECT 
								ID 
							FROM internal_deadline
							WHERE 
								Ticket_Number = '$ticket'";
			$result = mysqli_query($conn,$sql);
			if(mysqli_num_rows($result) > 0)
			{
				$row = mysqli_fetch_assoc($result);
				$internaldl_id = $row["ID"];
			}

			return $internaldl_id;
		}
		
	#endregion
?>