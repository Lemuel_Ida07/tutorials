<?php
  include('connect.php');
?>

<style>
    
    #mainform{
      overflow:auto;
      height:85%;
      padding-left:1%;
    }
    
    .left_box{
	  position:fixed;
      width:47%;
      border-collapse: separate;
      box-shadow: 0px 0px 2px 0px;
      padding:10px;
      border-radius:2px;
      margin-top:10px;
      margin-bottom:70px;
      margin-right:1%;
      display:inline-block;
	  overflow:auto;
    }
    
	.right_box{
	  position:fixed;
      width:47%;
      border-collapse: separate;
      box-shadow: 0px 0px 2px 0px;
      padding:10px;
      border-radius:2px;
      margin-top:10px;
      margin-bottom:70px;
      margin-right:1%;
      display:inline-block;
	  right:1%;
	  z-index:1;
	  max-height:85%;
	  overflow:auto;
    }
	
    .th_timesheet{
      width: 1px;
      white-space: nowrap;
      font-size: 12px;
    }
    
    .thead_summary{
      border-bottom:1px #cccccc;
    }
    
    .inp_hours{
      background-color: #52d292;
      color: white;
      border-radius: 0px;
      text-align: center;
      padding-top: 10px;
      padding-bottom: 10px;
      font-weight: bold;
      cursor: pointer;
    }
    
    .inp_hours:hover{
      filter:brightness(75%);
    }
    
    .th_project{
      width:25%;
      padding-top: 10px;
      padding-right: 10px;
      padding-bottom: 10px;
      text-align:left;
      font-weight:normal;
    }
    
    .th_day{
      width:2%;
    }
	
    #mainform .tbl_timesheet tbody tr td.td_hours input[type="text"].hour_first{
      border-radius: 4px 0px 0px 4px;
    }
    
    #mainform .tbl_timesheet tbody tr td.td_hours input[type="text"].hour_last{
      border-radius: 0px 4px 4px 0px;
    }
    
    .td_project{
      padding-top: 10px;
      padding-right: 10px;
      padding-bottom: 10px;
      font-size:12px;
    }
    
    #mainform .tbl_timesheet tbody tr td.td_hours input[type="text"] {
      color: white;
      border: 0px;
      border-radius: 0px;
      text-align: center;
      padding-top: 10px;
      padding-bottom: 10px;
      font-weight: bold;
      cursor: pointer;
	  width:40px;
	  min-witdh:40px;
    }
    
    caption{
      text-align:left;
      font-weight: bold;  
    }
    
    #footer{
      position:fixed;
      bottom:0px;
      height:50px;
      width:100%;
      padding:0px;
      margin:0px;
      left:0px;
      background-color:#f7f9fa;
      box-shadow:0px 0px 2px 0px;
      padding:10px;
	  z-index:2;
    }
    
    .inline{
      display:inline-block;
    }
    
    .inp_hours_unu{
      background-color: #cccccc;
      color: white;
      border-radius: 0px;
      text-align: center;
      padding-top: 10px;
      padding-bottom: 10px;
      font-weight: bold;
      cursor: pointer;
    }
    
    .inp_hours_unu:hover{
      filter:brightness(75%);
    }
	
	#mainform input[type=date]{
		width:auto;
	}
	
	#btn_calculate{
		float:right;
		margin-right:100px;
		display:none;
	}
    
</style>

<div id="footer">
	<label for="drop_month" class="title"> Month: </label>
	<select id="drop_month">
	</select>
	<label for="drop_period" class="title"> Period: </label>
	<select id="drop_period">
	</select>
	<label for="drop_year" class="title"> Year: </label>
	<select id="drop_year">
	</select>
	<input type="submit" id="btn_go" class="btn-normal bg-blue white bold"value="Go" />
    <h1 class='inline' style="margin-left:20%;"> Utilization Rate: <span id="utilization">0</span>% </h1>
    <h1 class='inline' style='margin-left:10px;display:none;'> For Approval: 10 </h1>
	<input type="submit" id="btn_calculate" class="btn-normal bg-green white bold h1 align-right" value="Calculate Utilization" />
</div>
<div class="left_box">
<table class="tbl_timesheet">
  <caption>Utilized</caption>
  <thead id="th_timesheet">
  </thead>
  <tbody id="tb_timesheet">
    <!-- data from timesheetAjax.php -->
  </tbody>
</table>
</div>
<div class="right_box">
<table class="tbl_timesheet">
  <caption>Unutilized</caption>
  <thead id="th_timesheet2">
  </thead>
  <tbody id="tb_timesheet2">
    <!-- data from timesheetAjax.php -->
  </tbody>
</table>
</div>
<script src="scripts/timesheet.js"></script>

<script>
 //populate period dropdown on load
 $(document).ready(function(){
	 generatePeriod(month_today(),year_today());
 });
 
 //populate month dropdown
  $.each(month,function(index,value){
	var drop_month = $('#drop_month').html();
	$('#drop_month').html(drop_month + "<option value='" + (index + 1) + "'> " + value + " </option>");
  });

  //populate year dropdown
  $.each(yearList(20),function(index,value){
	var year_list = $('#drop_year').html();
	$('#drop_year').html(year_list + "<option value='"+value+"'> "+value+" </option>");
  });
  
  //populate period dropdown on month select
  $('#drop_month').on('change',function(){
	var month = $('#drop_month').val();
	var year = $('#drop_year').val();
	generatePeriod(month,year);
  });
  
  $('#btn_go').on('click',function(){
	  
	//selected month from dropdown
	var month = $('#drop_month').val();
	
	//selected year from dropdown
	var year = $('#drop_year').val();
	
	//selected period from dropdown
	var period = $('#drop_period').val();
	
	//for utilized table
	$('#th_timesheet').html("<th class='th_project'>Project</th>");
  
	  $.each(getHalfMonthDays(month,year,period),function(index,value){
		var weekDay = dayofweek[1][identifyWeekDay(year,month,value)];
		var html = $('#th_timesheet').html();
		$('#th_timesheet').html(html + "<th class='th_timesheet th_day "+redSatSun(identifyWeekDay(year,month,value))+"'><div>"+weekDay+"</div>"+ value +"</th>");
	  });
	  
	  //for unutilized table
	  $('#th_timesheet2').html("<th class='th_project'>Project</th>");
	  $.each(getHalfMonthDays(month,year,period),function(index,value){
		var weekDay = dayofweek[1][identifyWeekDay(year,month,value)];
		var html = $('#th_timesheet2').html();
		$('#th_timesheet2').html(html + "<th class='th_timesheet th_day "+redSatSun(identifyWeekDay(year,month,value))+"'><div>"+weekDay+"</div>"+ value +"</th>");
	  });
	  
	  //generate list for utilized
	  $.ajax({
		  url:'timesheetAjax.php',
		  type:'post',
		  data:'proj_state=utilized'+
			   '&date_from='+concatDate(year,month,getHalfMonthDays(month,year,period)[0])+
			   '&date_to='+concatDate(year,month,getHalfMonthDays(month,year,period)[getHalfMonthDays(month,year,period).length -1])+
			   '&year='+year+
			   '&month='+month,
		  success:function(data){
			  $('#tb_timesheet').html(data);
		  }
	  });
	  
	  //generate list for unutilized
	  $.ajax({
		  url:'timesheetAjax.php',
		  type:'post',
		  data:'proj_state=unutilized'+
			   '&date_from='+concatDate(year,month,getHalfMonthDays(month,year,period)[0])+
			   '&date_to='+concatDate(year,month,getHalfMonthDays(month,year,period)[getHalfMonthDays(month,year,period).length -1])+
			   '&year='+year+
			   '&month='+month,
		  success:function(data){
			  $('#tb_timesheet2').html(data);
		  }
	  });
  });
  
  function generatePeriod(month,year){
	var last_day = getDaysInMonth(month,year);
	$('#drop_period').html("<option value='1'> 1 - 15 </option><option value='2'> 16 - "+ last_day +" </option>");
  }
  
  function redSatSun(weekDay){
	 var color = "";
	  if(weekDay == 0 || weekDay == 6)
	  {
		  color = "red";
	  }
	  return color;
  }
</script>