<?php
	session_start();
	include('connect.php');
	$designer_id = $_SESSION["ID"];
	$curr_year = (int)date("Y");
	$curr_month = (int)date("m");
	$curr_day = (int)date("d");
	/* Start clicked */
/* @var $_POST type */
if(filter_input(INPUT_POST,'start_review') == true)
{
  $reqfordes_id = $_POST["reqfordes_id"];
    $sql = "UPDATE requirements_for_des
            SET Status = 'Requested'
            WHERE ID = $reqfordes_id";
    if(mysqli_query($conn,$sql)){

    }
}


/* for cad */

if(isset($_POST["accept_cad"]))
{
  $cadtech_notif_id = $_POST["cadtech_notif_id"];
    $sql = "UPDATE cadtech_notif
            SET Status = 'Designer CAD Accepted'
            WHERE ID = $cadtech_notif_id";
    if(mysqli_query($conn,$sql)){
    }
}

if(isset($_POST["reject_cad"]))
{
  $cadtech_notif_id = $_POST["cadtech_notif_id"];
  $sql = "UPDATE cadtech_notif
          SET Status = 'Designer CAD Rejected'
          WHERE ID = $cadtech_notif_id";
  if(mysqli_query($conn,$sql)){
  }
}
	//EDIT ACTIVITY
	if(isset($_POST["edit"]))
	{
		$activity = $_POST["activity"];
		$time_in = date($_POST["time_in"]);
		$time_out = date($_POST["time_out"]);
		if($time_in > $time_out)
		{
			echo "<script> alert('Invalid Time In! \\n New Time in must not be greater than Time Out.') </script>";
		}
		else
		{
			$activity_logs_id = $_POST["activitylog_id"];
			$sql = "UPDATE activity_logs SET Activity = '$activity',
																			 Time_In = '$time_in',
																			 Duration = TIMEDIFF(Time_Out,'$time_in') 
							WHERE ID = $activity_logs_id";
	  	if(mysqli_query($conn,$sql))
			{
				echo "<script>
								alert('Edit Success!');
								hidePop();
							</script>";
			}
		}
	}

	/* GET SCHEDULE */
	if(isset($_POST["getSchedule"]))
	{
		$dnotif_id = $_POST["dnotif_id"];
		$readsql = "SELECT 
                  EXTRACT(DAY FROM(designer_notif.Deadline)) AS Day_DN,
                  EXTRACT(MONTH FROM(designer_notif.Deadline)) AS Month_DN,
                  EXTRACT(YEAR FROM(designer_notif.Deadline)) AS Year_DN,
                  internal_deadline.Ticket_Number,
                  designer_notif.Activity,
                  designer_notif.Project_ID
								FROM designer_notif
								INNER JOIN internal_deadline
									ON designer_notif.Internaldl_ID = internal_deadline.ID
								WHERE designer_notif.ID = $dnotif_id;";
		$result = mysqli_query($conn,$readsql);
		if(mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_assoc($result);
			$month = $row["Month_DN"];
			$day = $row["Day_DN"];
			if($month < 10)
			$month = "0".$month;
			if($day < 10)
			$day = "0".$day;
			echo "<script>
							$('#pop_date').html('".$row["Year_DN"]."-$month-$day');
							$('#pop_ticket').html('".$row["Ticket_Number"]."');
							$('#pop_activity').val('".$row["Activity"]."');
							$('#pop_project_id').val('".$row["Project_ID"]."');
						</script>";
		}
	}
	
	if(isset($_POST["getAvailableProj"]))
	{
		$sql = "SELECT
					project.Project_Number,
					project.Project_Name,
					project.ID AS Project_ID,
					designer_notif.ID AS DNotif_ID
				FROM
					designer_notif
				INNER JOIN project ON project.ID = designer_notif.Project_ID
				WHERE
					designer_notif.Designer_ID = $designer_id AND(
						designer_notif.Status = 'Standby' OR designer_notif.Status = 'On Hold' OR designer_notif.Status = 'Rejected'
					)
					AND EXTRACT(YEAR FROM(designer_notif.Deadline)) = $curr_year 
					AND EXTRACT(MONTH FROM(designer_notif.Deadline)) = $curr_month 
					AND EXTRACT(DAY FROM(designer_notif.Deadline)) >= EXTRACT(DAY FROM(CURDATE()))"; 
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			while($rows = mysqli_fetch_assoc($result))
			{
				echo "<h4 id='h4Proj".$rows["Project_ID"]."' onclick='fillProjecth4Proj".$rows["DNotif_ID"]."()'> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." </h4>";
				echo "<script>
								function fillProjecth4Proj".$rows["DNotif_ID"]."()
								{
									$('#pop_project').val($('#h4Proj".$rows["Project_ID"]."').html());
									$('#pop_dnotif_id').val('".$rows["DNotif_ID"]."');
									$('.datalist').hide();

									$.ajax(
									{
										url:'designerSubmit.php',
										type:'post',
										data:'getSchedule=true'+
													'&dnotif_id='+$('#pop_dnotif_id').val(),
										success:function(data)
										{																	 
											$('#message').html(data);
										},
										error:function(data)
										{
											alert(data);
										}	
									});
								}
							</script>";
			}
		}
	}