<?php
  include('phpScript.php');
  $user_pic = userPic($_SESSION["Picture"]);
  $full_name = $_SESSION["Name"];
/* Van Hudson Galvoso
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<ul id="dropdown1" class="dropdown-content">
  <li><img src="../images/profile_pictures/<?php echo $user_pic; ?>" alt="<?php echo $full_name; ?>" width="150" height="150" class="circle center-block"></li>
  <li><a href="../manageAccount.php" class="blue-text"><i class="material-icons">settings</i>Account Settings</a></li>
  <li><a href="#!" class="blue-text"><i class="material-icons">exit_to_app</i>Logout</a></li>
</ul>
<nav class="blue darken-4">
  <div class="nav-wrapper">
    <a href="../index.php" class="brand-logo"><img class="responsive-img right-aligned" width="150" src="../img/logo.png" alt="acong logo"/></a>
    <ul class="right hide-on-med-and-down">
      <li><a href="#!" ><i class="material-icons">notifications_none</i></a></li>
      <li><a href="#!" class="dropdown-trigger" data-target="dropdown1" ><i class="material-icons">arrow_drop_down</i></a></li>
    </ul>
  </div>
</nav>