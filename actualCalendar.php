<?php
	$current_day = (int)date("d");
	$current_month = (int)date("m");
	$current_year = (int)date("Y");
	$tradeString = $_SESSION['Trade'];
	$designer_id = $_SESSION['ID'];
	
	$month_selected = (int)date("m");
	$year_selected = (int)date("Y");

	$NoLastDays = 0;
	$lastweekdays = 0;
	$totaldays=cal_days_in_month(CAL_GREGORIAN,$month_selected - 1,$year_selected);
	$loop = $totaldays / 7; //get no. of weeks(row)
	$extradays = $totaldays % 7; //get last row if any
	if($extradays != 0)
		$loop ++;
	$daycounter = 1; //start day of week
	$dayweek = 7; //last day of week
	$count4 = 0; // para sa count ng kung ilan yung event per day
	for($count = 1; $count < $loop; $count ++)
	{
		$lastweekdays = 0;
		for($count2 = $daycounter; $count2 <= $dayweek; $count2 ++)
		{
			$lastweekdays ++;
			//echo $count2." ";
		}
		$daycounter = $count2;
		if($extradays != 0)
		{
			if($count < $loop - 2)
			{
				$dayweek = $count2 + 6;
			}
			else
			{

				$NoLastDays = $lastweekdays;
				$dayweek = $totaldays;
			}
		}
		else
		{
			$dayweek = $count2 + 6;
		}
		//echo "<br />";
	}
	
	$NoLastDays = 7;
?>
<div id="calendar">
	<table>
		<caption> <span class="subheader">Actual</span> </caption>
		<tbody>
			<?php
				$in_dead_bg = array ("none", "#8f0ea0", "00B0F0", "#FF0000", "#92D050", "#FFFF00", "#A5A5A5");
				$ex_dead_bg = array ("none", "#4c0056", "#005878", "#800000", "#497E10", "#808000", "#545454");
				$totaldays=cal_days_in_month(CAL_GREGORIAN,$month_selected,$year_selected);
				$loop = ($NoLastDays + $totaldays) / 7;
				$extradays = ($NoLastDays + $totaldays) % 7;
				if($extradays != 0)
				$loop ++;
				$daycounter = $NoLastDays - (($NoLastDays * 2)-1);
				$dayweek = 7 -$NoLastDays;
				$count4 = 0; // para sa count ng kung ilan yung event per day

				$lastinternaldl = "";
				$current_day = (int)date("d");
				for($count = 1; $count <= $loop; $count ++)
				{
					echo"<tr>";
					for($count2 = $daycounter; $count2 <= $dayweek; $count2 ++)
					{
						$sql2 = "SELECT internal_deadline.ID,
									project.Project_Name,
									project.Team_ID,
									project.Project_Number,
									internal_deadline.Trade,
									internal_deadline.Day_ID
								FROM project
									INNER JOIN external_deadline ON project.ID = external_deadline.Project_ID
									INNER JOIN internal_deadline ON external_deadline.ID = internal_deadline.Externaldl_ID
								WHERE internal_deadline.Day_ID = $count2 and internal_deadline.Month_ID = $month_selected and internal_deadline.Year_ID = $year_selected and internal_deadline.Trade ='$tradeString'";

						$result2 = mysqli_query($conn,$sql2);

						$dnotifsql = "SELECT 
											designer_notif.ID,
											DATE_FORMAT(activity_logs.Time_In, '%h:%i %p') AS Time_From,
											DATE_FORMAT(activity_logs.Time_Out, '%h:%i %p') AS Time_To,
											DATE_FORMAT(activity_logs.Time_In, '%h:%i') AS Time_In,
											DATE_FORMAT(activity_logs.Time_Out, '%h:%i') AS Time_Out,
											designer_notif.Internaldl_ID
										FROM designer_notif
											INNER JOIN internal_deadline ON internal_deadline.ID = designer_notif.Internaldl_ID
											INNER JOIN activity_logs ON designer_notif.Project_ID = activity_logs.Project_ID
										WHERE designer_notif.Designer_ID = $designer_id AND designer_notif.Day_DN = $count2 AND designer_notif.Month_DN = $month_selected AND designer_notif.Year_DN = $year_selected AND internal_deadline.Trade = '$tradeString' AND activity_logs.Date = '".date("Y-m-d")."'";

						$dnotifresult = mysqli_query($conn,$dnotifsql);

						if(mysqli_num_rows($result2) > 0)
						{
							if($NoLastDays > 0 || $count2 == $daycounter || $count2 == $daycounter + 6)
							{
								if($count2 <= 0)
								{
									echo "<td class='days'>  ";
								}
								else
								{
									echo "<td class='days'> $count2 ";
								}
								$NoLastDays = $NoLastDays-1;
							}
							else
							{
								echo "<td class='days'> $count2
										<div class='$count2' id='daily_container'>";
											// SHOW INTERNAL DEADLINES
											while($rows2 = mysqli_fetch_assoc($result2))
											{
												$INcurID = $rows2['Team_ID']; // get TEAM ID
												echo "<p style='background-color: red;'  class='indeadline'>  ".$rows2['Project_Number']." (".$rows2['Trade'].") </p>";

											}
											// SHOW DESIGNER DEADLINES
											try
											{
												if(mysqli_num_rows($dnotifresult) > 0)
												{
													$firstrows3 = mysqli_fetch_assoc($dnotifresult);
													$id = $firstrows3['ID'];		
													echo "<p style='background-color: #00b0f0;' id='designer_actuakdeadline".$firstrows3['ID']."' class='indeadline'>  ".$firstrows3['Time_From']." - ".$firstrows3['Time_To']." </p>";
																			
													while($rows3 = mysqli_fetch_assoc($dnotifresult))
													{
														if($rows3['ID'] == $id)
														{
															echo "<p style='background-color: #00b0f0;' id='designer_actuakdeadline".$rows3['ID']."' class='indeadline'>  ".$rows3['Time_From']." - ".$rows3['Time_To']." </p>";
															$id = $rows3['ID'];
														}
													}
													throw new Exception("Error");
												}
											}
											catch (Exception $e)
											{
											}
											echo"</div>";
							}
						}
						else
						{
							if($NoLastDays > 0 || $count2 == $daycounter || $count2 == $daycounter + 6)//generates blank spaces before the start of the month
							{
								if($count2 <= 0)
								{
									echo "<td class='days'>  ";
								}
								else
								{
									echo "<td class='days'> $count2";
									
								}
								$NoLastDays = $NoLastDays-1;
							}
							else
							{
								echo "	<td class='days'> $count2<div class='$count2' id='daily_container'>";
									// SHOW DESIGNER DEADLINES
									try
									{
										if(mysqli_num_rows($dnotifresult) > 0)
										{			
											$firstrows3 = mysqli_fetch_assoc($dnotifresult);
											$id = $firstrows3['ID'];		
											echo "<p style='background-color: #00b0f0;' id='designer_actuakdeadline".$firstrows3['ID']."' class='indeadline'>  ".$firstrows3['Time_From']." - ".$firstrows3['Time_To']." </p>";
																			
											while($rows3 = mysqli_fetch_assoc($dnotifresult))
											{
												if($rows3['ID'] == $id)
												{
													echo "<p style='background-color: #00b0f0;' id='designer_actuakdeadline".$rows3['ID']."' class='indeadline'>  ".$rows3['Time_From']." - ".$rows3['Time_To']." </p>";
													$id = $rows3['ID'];
												}
											}
											throw new Exception("Error");
										}
									}
									catch (Exception $e)
									{
									}
								echo " </div>";
								$readsql3 = "SELECT MAX(Day_ID) as Day_ID  FROM internal_deadline WHERE Month_ID = $month_selected and Year_ID = $year_selected and Trade ='$tradeString'";
								$result3 = mysqli_query($conn,$readsql3);
								$row = mysqli_fetch_assoc($result3);
								$lastinternaldl = $row['Day_ID'];

								echo "<script>
										$(document).ready(function(){
											$('.add$count2').on('click',function(){
												var value = $(this).val();
												$.ajax(
												{
													url:'dnotif_table.php',
													type:'post',
													data:'designer_id='+document.getElementById('designerid').value+'&trade=$tradeString&day_dn=$count2',
													success:function(data)
													{
														$('#updata').html(data);
													},
												});
											});
										});
								</script>";
							}
						}
					}
					echo "</tr>";
					$daycounter = $count2;
					if($extradays!=0)
					{
						if($count < $loop - 2)
						{
							$dayweek = $count2 + 6;
						}
						else
						{
							$dayweek = $totaldays;
						}
					}
					else
					{
						$dayweek = $count2 + 6;
					}
				}
			?>
		</tbody>
	</table>
</div>