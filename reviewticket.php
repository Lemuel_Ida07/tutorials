<?php
	include('connect.php');
	session_start();
	$project_id = (int)$_POST['pnumber'];
	$trade = $_SESSION['Trade'];

?>
	<input type="text" name="ticket_number" list='ticketlist' autocomplete="off" placeholder="Ticket Number" />
	<datalist id="ticketlist">
		<?php
			$designer_id = $_SESSION["ID"];
			$sql = "SELECT ticket.Ticket_Number FROM ticket INNER JOIN internal_deadline ON ticket.Internaldl_ID = internal_deadline.ID WHERE ticket.Project_ID = $project_id AND internal_deadline.Trade = '$trade'";
			$result = mysqli_query($conn,$sql);
			if(mysqli_num_rows($result) > 0)
			{
			    while($rows = mysqli_fetch_assoc($result))
			    {
					echo "<option value='".$rows["Ticket_Number"]."'> ".$rows["Ticket_Number"]."</option>";
			    }
		   	}
		?>
	</datalist>
	<input type="text" id="designersearch" class="designersearch" placeholder="Select Designer" autocomplete="off" onkeyup="filter('designersearch','designerdatalist')"/>
		<input type="text" name="designer_id" id="designer_id" style="display:none;" />
				
		<div id="designerdatalist" class="designerdatalist">
			<?php
				$usersql = "SELECT ID,CONCAT(Firstname,' ',Middlename,' ',Lastname) AS Name FROM user WHERE User_Type = 0";
				$userresult = mysqli_query($conn,$usersql);
				if(mysqli_num_rows($userresult) > 0)
				{
					while($userrows = mysqli_fetch_assoc($userresult))
					{
						echo "<h4 id='des".$userrows['ID']."' onclick='passData".$userrows['ID']."();'> ".$userrows['Name']." </h4>";
						echo "<script>
								function passData".$userrows['ID']."()
								{
									document.getElementById('designersearch').value = document.getElementById('des".$userrows['ID']."').innerHTML;
									document.getElementById('designer_id').value='".$userrows['ID']."';
									document.getElementById('designerdatalist').style.display='none';
								}
									
							  </script>";
					}
				}
			?>
		</div>
	<textarea name="Comments" rows="4" cols="20" placeholder="Comments..." style="width: 100%; 
	font-family: ' Roboto', 'Roboto', sans-serif; font-size: 16px;"></textarea>
	<img id="star1" onclick="clickStar('star1')" src="img/gstar.png"/>
	<img id="star2" onclick="clickStar('star2')"src="img/gstar.png"/>
	<img id="star3" onclick="clickStar('star3')"src="img/gstar.png"/>
	<img id="star4" onclick="clickStar('star4')"src="img/gstar.png"/>
	<img id="star5" onclick="clickStar('star5')"src="img/gstar.png"/>
	<input id="rating" type="text" name="Rating" style="display:none;"/>
	<br/>
	<label id="label1"> Date </label><br/> 
	<input type="date" name="Date" style="font-size: 1.2rem; width:100%;" required/><br/>
	<label id="label1"> From </label><label id="label2"> To </label><br/> 
	<input id="inptext1" type="time" name="TimeIn" style="width:37%"  required="required" />
	<input id="inptext2" type="time" name="TimeOut" style="width:37%" required="required" />
	<input type="submit" value="Add" name="add" id="addbtn"/>
	
<script>
	function clickStar(id)
	{
		var star = document.getElementById(id);
		var star2 = document.getElementById('star2');
		var star3 = document.getElementById('star3');
		var star4 = document.getElementById('star4');
		var star5 = document.getElementById('star5');
		var count = 0;
		switch(id)
		{
			case'star1':
					star.src = "img/ystar.png";
					star2.src = "img/gstar.png";
					star3.src = "img/gstar.png";
					star4.src = "img/gstar.png";
					star5.src = "img/gstar.png";
					document.getElementById('rating').value = "1";
			break;
			case'star2':
			count = 0;
				star1.src = "img/ystar.png";
				star.src = "img/ystar.png";
				star3.src = "img/gstar.png";
				star4.src = "img/gstar.png";
				star5.src = "img/gstar.png";
					document.getElementById('rating').value = "2";
			break;
			case'star3':
			count = 0;
				star1.src = "img/ystar.png";
				star2.src = "img/ystar.png";
				star.src = "img/ystar.png";
				star4.src = "img/gstar.png";
				star5.src = "img/gstar.png";
					document.getElementById('rating').value = "3";
			break;
			case'star4':
			count = 0;
				star1.src = "img/ystar.png";
				star2.src = "img/ystar.png";
				star3.src = "img/ystar.png";
				star.src = "img/ystar.png";
				star5.src = "img/gstar.png";
					document.getElementById('rating').value = "4";
			break;
			case'star5':
			count = 0;
				star1.src = "img/ystar.png";
				star2.src = "img/ystar.png";
				star3.src = "img/ystar.png";
				star4.src = "img/ystar.png";
					document.getElementById('rating').value = "5";
				star.src = "img/ystar.png";
			break;
		}
	}

	
		$(".designersearch").click(function (e) {
			$(".designerdatalist").show();
			e.stopPropagation();
		});

		$(".designerdatalist").click(function (e) {
			e.stopPropagation();
		});

		$(document).click(function () {
			$(".designerdatalist").hide();
		});
</script>