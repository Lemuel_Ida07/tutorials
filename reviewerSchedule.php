<?php
session_start();
include("connect.php");
include("phpGlobal.php");
$today = date("F j, Y");	
$curr_date = date("Y-m-d");	
$tradeString = $_SESSION['Trade'];
include('phpScript.php');
$month_string = $month_array[(int)date("m")];
$current_day = date("d");

$trade = array("C","E","A","AU","S","P","M","FP");
								 
/* GET LAST DAYS FROM PREVIOUS  MONTH */
	$NoLastDays = 0;
	$base_month = 4; //default month (don't cahnge)
	$base_year = 2018; //default year (don't cahnge)
	$month_selected = (int)date("m") -1;
	$year_selected = (int)date("Y");
	for($date_count = $base_month; $year_selected >= $base_year; $date_count++)
	{
		$NoLastDays = getPrevMonthNoLastDays($base_month,$base_year,$NoLastDays);	 
		$base_month ++;
		if($base_month > 12)
		{
			$base_year++;
			$base_month = 1;
		}			
 
		//stops the loop				
		if($base_year >= $year_selected && $base_month > $month_selected)
		{
			break;
		}
	}

	$month_selected = (int)date("m");
?>

<style>
    #navbar tr td#schedule{
    background-color: #deebff;
    color:#0747a6;
  }
  #navbar tr td#des_tracking{
    background-color: #0747a6;
    color:#deebff;
  }
    body{
        ovetrflow-y:scroll;
    }
  #pop_form{
      padding:0px;
      margin:0px;
      width:auto;
      display:inline-block;
      float:right;
      margin-top:12px;
      margin-right:5%;
  }
	 #mainform #sel_proj_tbox,#mainform #budget,#mainform #from_date,#mainform #to_date{
		background: #fafafa;
    color: #444f5d;
    border: 1px solid #eaeaea;
    box-shadow: inset 0 1px 3px 0 rgba(0,0,0,.09);
    border-radius: 4px;
    padding: 10px 17px;
    font-size: 14px;
    font-weight: 600;
    transition: box-shadow .1s ease-out;
	}
  #mainform #sel_proj_tbox{
    width: 300px;
  }
  #mainform #budget{
    width: 60px;
	}
  #mainform #from_date{
      width:160px;
  }
  #mainform #to_date{
      width:160px;
  }

  #mainform #edit_designer{
    color: #fff;
    border-color: transparent;
    border-radius: 4px;
    font-size: 13px;
    box-shadow: none;
    cursor: pointer;
    font-weight: 700;
    text-decoration: none;
    display: none;
    padding: 8px 14px;
  }
  
  
  #mainform #assign_designer{
    color: #fff;
    border-color: transparent;
    border-radius: 4px;
    font-size: 13px;
    box-shadow: none;
    cursor: pointer;
    font-weight: 700;
    text-decoration: none;
    display: none;
    padding: 8px 14px;
    visibility:hidden;
  }
  
  #mainform #assign_designer:hover,#mainform #edit_designer:hover{
    filter:brightness(75%);
  }
  
  #mainform #back_btn{
    margin-left:5%;
    border-color: transparent;
    border-radius: 4px;
    font-size: 13px;
    box-shadow: none;
    cursor: pointer;
    font-weight: 700;
    text-decoration: none;display: inline-block;

padding: 8px 14px;
  }
  #mainform #back_btn:hover{
      filter:brightness(75%);
  }
  
	#mainform textarea{
		width:390px;
		margin:0px;
	}

	#mainform h3{
		margin:0px;
	}
  
  #pop_up_bg{
      position:fixed;
      top:0px;
      left:0px;
      width:99%;
      height:99%;
      z-index:20;
      padding:0px;
  }
  
  #pop_up{
      position:fixed;
      top:0px;
      left:0px;
      width:99%;
      height:100%;
      z-index:20;
      margin:0px;
      background-color:#f7f9fa;
      overflow-y:auto;
  }
  #mainform .pop_header{
      background-color:#eef1f2;
      position:fixed;
      width:100%;
      height:80px;
      left:0px;
      top:0px;
      border-bottom:solid 2px #ececec;
      z-index:25;
  }
  #mainform #pop_dname{
      margin-top: 25px;
      margin-left:2%;
      font-weight: 900;
      font-size: 18px;
      font-family:"Avenir",sans-serif;
      display:inline-block;
  }
  .check_box{
      border-radius: 4px;
      background-color: #fff;
      border: 1px solid #eaeaea;
      padding: 26px 34px;
      margin-bottom: 8px;
  }
  .check_box ul li{
      margin-top:4px;
      margin-bottom:4px;
  }
  #checklist_container{
    position:fixed;
      margin-top:80px;
      width:57%;
    height:85%;
      margin-left:41%;
      background-color:transparent;
      border:none;
      box-shadow:none;
  }
  
  #weekly{
    position:fixed;
    left:10px;
    margin-top:80px;
    min-height:140px;
    background-color:#ffffff;
    border-radius:4px;
    border: 1px solid #eaeaea;
    color: #6e848b;
    font-size: 12px;
    align-items: center;
    vertical-align: baseline;
    width:35%;
    height:79%;
    z-index:1;
    padding: 26px 34px;
    overflow-y:auto;
  }
  
  .summary_holder{
    border-radius:4px;
    width:100%;
    border-collapse: collapse;
    min-width:200px;
  }
  
  .summary_holder tr th.th_project{
      min-width:150px;
      text-align:left;
  }
  
  .summary_holder tr th.th_days{
      font-weight:normal;
      padding:0px 17px;
      font-size:12px;
  }
  
  #mainform .summary_holder tbody tr td.hours input[type=text]{
    background-color:#52d292;
    color:white;
    border:0px;
    border-radius:0px;
    text-align:center;
    padding-top:10px;
    padding-bottom:10px;
    font-weight:bold;
    cursor:pointer;
  }
  
  #mainform .summary_holder tbody tr td.hours input[type=text]:hover{
    filter:brightness(75%);
  }
 
  #mainform .summary_holder tbody tr td.hours input[type=text].day1{
      border-radius:4px 0px 0px 4px;
  }
  
  #mainform .summary_holder tbody tr td.hours input[type=text].day7{
      border-radius:0px 4px 4px 0px;
  }
  
  #mainform .summary_holder tbody tr td.project{
      padding-top:10px;
      padding-right:10px;
      padding-bottom:10px;
      font-size:12px;
  }
  
  #proj_summary tr{
      border-top:solid 1px #ececec;
  }
  
  #pname_head{
      display:inline-block;
      font-size:14px;
      padding-right:50px;
  }
  
  #week_head{
    display:inline-block;
    border:solid 1px black;
  }
  
  .day_head{
    display:inline-block;
    font-size:14px;
    padding:10px 17px;
  }
  
  .tag{
    border:none;
    border-bottom:1px solid #ececec;
  }
  
  #mainform input[type="text"].budget_inp{
    width:35px;
    background: #fafafa;
    color: #444f5d;
    border: 1px solid #eaeaea;
    box-shadow: inset 0 1px 3px 0 rgba(0,0,0,.09);
    border-radius: 4px;
    padding: 4px;
    font-size: 14px;
    display:inline-block;
    margin-right:10px;
    cursor:not-allowed;
  }
  
  .check_item{
    width:70%;
  }
  
  #monthnumber{
    display:none;
    float:left;
  }
  
  /*For Select Designer Pop*/
  #sel_des_pop_bg{
    position:fixed;
    top:0px;
    left:0px;
    width:100%;
    height:100%;
    background-color:#f7f9fa;
    opacity:0.7;
    z-index:21;
  }
  
  #sel_des_pop{
    background-color:#ffffff;
    position:fixed;
    margin:10% 23% ;
    width:50%;
    height:40%;
    border-radius:4px;
    border: solid 1px #eaeaea;
    box-shadow:0px 0px 3px;
    z-index:22;
  }
  
  #mainform #sel_des_pop #sel_des_tbox{
    background: #f8f9fa;
    color: #444f5d;
    border: 2px solid #e2e8ee;
    border-radius: 4px;
    padding: 10px 17px;
    font-size: 14px;
    font-weight: 600;
    min-width:400px;
    width:60%;
    margin-left:20%;
    margin-top:15%;
    margin-bottom:0px;
  }
  
  ::placeholder{
    color: #444f5d;
    opacity:1;
  }
  
  .select_holder{
  }
  
  #sel_des_btn{
    border-color: transparent;
    border-radius: 4px;
    font-size: 13px;
    box-shadow: 0px 4px 4px 0px #e2e8ee;
    cursor: pointer;
    font-weight: 700;
    text-decoration: none;
    padding: 8px 14px;
    display:none;
  }
  
  #sel_des_btn:hover{
      filter:brightness(75%);
  }
  
  #sel_des_btn:active{
      filter:brightness(100%);
      margin-top:4px;
      margin-left:4px;
      box-shadow:none;
  }
 
  #proj_select{
    position:fixed;
    width: 300px;
    margin:0px;
    background-color:white;
    box-shadow:0px 2px 4px 4px #e2e8ee;
    border-radius:4px;
    min-width:300px;
    max-height:300px;
    overflow-y:auto;
    top:80px;
    display:none;
  }
  
  .proj_option{
    cursor:pointer;
    margin:0px;
    padding:5px;
    font-size: 14px;
    font-weight: normal;
    text-align:left;
    padding-left:20px;
    color: #444f5d;
    border-bottom:solid 1px #ececec;
  }
  
  .proj_option:hover{
      background-color:#3276B1;
      color:white;
  }

  #mainform #list{
      height:auto;
  }
  
  #asselect{
    min-width: 400px;
    width: 60%;
    margin-left: 20%;
  }
  
  .asselect.asoption{
    cursor:pointer;
    margin:0px;
    padding:5px;
    font-size: 14px;
    font-weight: 600;
    text-align:left;
    padding-left:20px;
    color: #444f5d;
    width:300px;
    border-bottom:solid 1px #ececec;
  }
  
  #designerdl_pop{
    display:none;
  }
  
  #pop_tbl tr td{
      padding:0px;
  }
</style>
		
<!-- POP UP -->
<!-- Select Designer Pop -->
<div id="sel_des_pop_bg">.</div>

<div id="sel_des_pop">
  <div class="select_holder">
    <input type="text" id="sel_des_tbox" placeholder="Select Designer"/>
    <input type="button" id="sel_des_btn" class="bg-green white" value="Select"/>
    <div id="asselect">
      <p class="asoption"> Sample Name </p>
    </div>
  </div>
</div>

<div id="temp_data_container">
    <input type="date" id="temp_indeadline" style="display:none;"/>
</div>

<div id="pop_up_bg">
	<div id="pop_up">
		<div class="pop_header">
        <input id="back_btn" class="bg-green white" type="submit" value="Back" />
        <h1 id="pop_dname" onclick="selectDesigner();"> Van Hudson Galvoso </h1>
        <div id="pop_form">
          <table id="pop_tbl">
          <tr>
            <td>
              <label class="title"> Select Project </label>
            </td>
            <td>
              <label class="title"> Budget </label>
            </td>
            <td>
              <label class="title"> From </label>
            </td>
            <td>
              <label class="title"> To </label>
            </td>
            <td>
            </td>
          </tr>
          <tr>
          <td>
          <input type="text"  
                 id="sel_proj_tbox"  
                 placeholder="Select a project." 
                 autocomplete="off" 
                 onkeyup="filter('sel_proj_tbox','datalist')" />
          <div id="proj_select">
            <!-- data from reviewer.php(getProjects) -->
            <p class="proj_option"> SO-14-012 - Philippine Genome Center Building Phase 1 </p>  
            <p class="proj_option"> SO-14-012 - Philippine Genome Center Building Phase 1 </p>  
          </div>
          </td>
          <td>
          <input type="text" id="budget" placeholder="Hrs" value="0" readonly="readonly"/>
          </td>
          <td>
          <input type="date" id="from_date" readonly="readonly" />
          </td>
          <td>
          <input type="date" id="to_date" readonly="readonly"/>
          </td>
          <td>
          <input id="assign_designer" class="bg-blue white" type="submit" value="Assign"/>
          <input id="edit_designer" class="bg-green white" type="submit" value="Edit" />
          <input id="delete_designer" class="bg-red white" type="submit" value="Delete" />
          </td>
          </tr>
          </table>
        </div>
    </div>
    <div id="weekly">
      <table class="summary_holder">
        <thead>
          <tr id="summary_head">
            <!-- data from click Date (reviewer.php) -->
          </tr>
        </thead>
        <tbody id="proj_summary">
          <!-- data from reviewer.php(getProjectsSummary) -->
          <tr>
            <td class="project text"> SO-14-012 - Philippine Genome Center Building Phase 1 </td>
            <td class="hours"><input type="text" value="100" class="day1"/></td>
            <td class="hours"><input type="text" value="0"/></td>
            <td class="hours"><input type="text" value="0"/></td>
            <td class="hours"><input type="text" value="0"/></td>
            <td class="hours"><input type="text" value="0"/></td>
            <td class="hours"><input type="text" value="0"/></td>
            <td class="hours"><input type="text" value="0" class="day7"/></td>
          </tr>
        </tbody>
      </table>
    </div>
		<div id="checklist_container">
			<?php
        $readsql = "SELECT 
                      DISTINCT Tag,
                      (SELECT COUNT(Check_Item)
                        FROM review_checklist t2
                        WHERE Trade = '$tradeString'
                          AND Tag = t1.Tag) AS Tag_Length 
                    FROM review_checklist t1
                    WHERE 
                      Trade = '$tradeString'
                    ORDER BY Tag_Length";
        $result = mysqli_query($conn,$readsql);
        if(mysqli_num_rows($result) > 0)
        {
          while($rows = mysqli_fetch_assoc($result))
          {
            echo "<div class='check_box'>
                    <label class='title'> ".$rows["Tag"]." </label>
                    <hr class='tag'/>";
            $itemsql 
            = "SELECT ID,Check_Item
                FROM review_checklist
                WHERE 
                  Tag = '".$rows["Tag"]."'
                  AND Trade = '$tradeString'";
            $itemresult = mysqli_query($conn,$itemsql);
            if(mysqli_num_rows($itemresult) > 0)
            {
              echo "<ul>";
              while($itemrows = mysqli_fetch_assoc($itemresult))
              {
                echo 
                "<li id='".$itemrows["ID"]."' class='text check_item'> 
                  <input type='text' 
                         id='check_item".$itemrows["ID"]."'
                         class='budget_inp' 
                         onkeydown='clearValue(this.id)'
                         onkeyup='calculateHours(this.value,this.id)' 
                         placeholder='0'
                         value='0'
                         disabled='disabled'
                         maxlength='2'/>
                  ".$itemrows["Check_Item"]."
                 </li>";
              }
              echo "</ul>";
            }
            echo "</div>";
          }
        }
      ?>
		</div>
	</div>
</div>

<!-- END POP UP -->
<div id="info">
    <p id="monthnumber"><?php echo $month_selected; ?></p>
	<label class="h1 float-left"> 
    <span id="currentMonth"> <?php echo $month_string; ?> </span>
    <span id="currentYear"><?php echo $year_selected; ?></span> 
  </label>
  <br />
  <div id="select_holder">
	<input type="text" 
         id="designer_name" 
         placeholder="Select Designer"  
         readonly="readonly"
         onclick="selectDesigner();"
  />
    <input type="text" id="designer_id" style="display:none;" value="0" />
    <input type="text" id="projectid" style="display:none;" value="0" />
    <input type="text" id="internaldl_id" style="display:none;" value="0" />
  </div>
<fieldset id="fset_legends">
	<legend> <label class="title">Legends</label> </legend>
	<table width="100%" class="legends">
		<thead>
			<tr>
				<th><label class="h3">Internal Deadline</label></th>
				<th><label class="h3"> Budget Allocation </label></th>
			</tr>
		</thead>
		<tbody>
			<tr>																	 
				<td> <div id="arryn_indl" class="event" > House Arryn </div> </td>
				<td> <div id="arryn_budget" class="event" > Budget </div> </td>
			</tr>
			<tr>																 
				<td> <div id="stark_indl" class="event" > House Stark </div> </td>
				<td> <div id="stark_budget" class="event" > Budget </div> </td>
			</tr>
			<tr>																 
				<td> <div id="lannister_indl" class="event" > House Lannister </div> </td>
				<td> <div id="lannister_budget" class="event" > Budget </div> </td>
			</tr>
			<tr>																 
				<td> <div id="tyrell_indl" class="event" > House Tyrell </div> </td>
				<td> <div id="tyrell_budget" class="event" > Budget </div> </td>
			</tr>	 
			<tr>																 
				<td> <div id="baratheon_indl" class="event" > House Baratheon </div> </td>
				<td> <div id="baratheon_budget" class="event" > Budget </div> </td>
			</tr>	 
			<tr>																 
				<td> <div id="targaryen_indl" class="event" > House Targaryen </div> </td>
				<td> <div id="targaryen_budget" class="event" > Budget </div> </td>
			</tr>	 
			<tr>																 
				<td> <div id="martell_indl" class="event" > House Martell </div> </td>
				<td> <div id="martell_budget" class="event" > Budget </div> </td>
			</tr>	 
			<tr>																 
				<td> <div id="mormont_indl" class="event" > House Mormont </div> </td>
				<td> <div id="mormont_budget" class="event" > Budget </div> </td>
			</tr>	 
			<tr>																 
				<td> <div id="greyjoy_indl" class="event" > House Greyjoy </div> </td>
				<td> <div id="greyjoy_budget" class="event" > Budget </div> </td>
			</tr>	 
			<tr>																 
				<td> <div id="tully_indl" class="event" > House Tully </div> </td>
				<td> <div id="tully_budget" class="event" > Budget </div> </td>
			</tr>
		</tbody>
	</table>
	</fieldset>
	<fieldset id="fset_legends">
<legend> <label class="title"> Deadline Details </label> </legend>
	</fieldset>
<div id="content_tray">
</div>
<input type="text" id="extraDaysPrevMonth" style="display:none" />
<input type="text" id="one" style="display:none" />	
<input type="text" id="tradeString" style="display:none" value="<?php echo $tradeString; ?>"/>	
<input type="text" id="NoLastDays" style="display:none" value="<?php echo $NoLastDays; ?>"/>	
<input type="text" id="dnotif_id" style="display:none;" />
</div>

<div id="list" onmousedown="hideList('designer_list')" >


<style>
	p
	{
		text-align:center;
	}
</style>
<div id="dates">
<table id="calendar" style="height:500px;">
	<tr>
			<th id="weekends" class="title"> Sunday </th>
			<th id="weekdays" class="title"> Monday </th>
			<th id="weekdays" class="title"> Tuesday </th>
			<th id="weekdays" class="title"> Wednesday </th>
			<th id="weekdays" class="title"> Thursday </th>
			<th id="weekdays" class="title"> Friday </th>
			<th id="weekends" class="title"> Saturday </th>
	</tr>
	<?php
	/* GET LAST DAYS FROM PREVIOUS  MONTH */
	$NoLastDays = 0;
	$base_month = 4; //default month (don't cahnge)
	$base_year = 2018; //default year (don't cahnge)
	$month_selected = (int)date("m") -1;
	$year_selected = (int)date("Y");
	for($date_count = $base_month; $year_selected >= $base_year; $date_count++)
	{
		$NoLastDays = getPrevMonthNoLastDays($base_month,$base_year,$NoLastDays);	 
		$base_month ++;
		if($base_month > 12)
		{
			$base_year++;
			$base_month = 1;
		}				
		//stops the loop				
		if($base_year >= $year_selected && $base_month > $month_selected)
		{
			break;
		}
	}

	/* END OF GET LAST DAYS FROM PREV MONTH */
	$month_selected = (int)date("m");
  /* get total days of current month*/
	$totaldays=cal_days_in_month(CAL_GREGORIAN,$month_selected,$year_selected);
  /* number of weeks generator */
  $loop = ($NoLastDays + $totaldays) / 7;
	$extradays = ($NoLastDays + $totaldays) % 7;
	if($extradays != 0)
		$loop ++;
	$daycounter = $NoLastDays - (($NoLastDays * 2)-1);
	$dayweek = 7 -$NoLastDays;
	$count4 = 0; // para sa count ng kung ilan yung event per day
  //create weeks(rows)
	for($count = 1; $count <= $loop; $count ++)
	{
		echo"<tr>";
    //create days (coolumns)
    $weekday = 0;
		for($count2 = $daycounter; $count2 <= $dayweek; $count2 ++)
		{
      $weekday++;
			$sql2 = "SELECT internal_deadline.ID,
									project.Project_Name,
									project.Team_ID,
									project.Project_Number,
									internal_deadline.Trade,
									internal_deadline.Day_ID,
									internal_deadline.Phase
								FROM project
								INNER JOIN external_deadline 
									ON project.ID = external_deadline.Project_ID
                INNER JOIN internal_deadline 
									ON external_deadline.ID = internal_deadline.Externaldl_ID
								WHERE 
									internal_deadline.Day_ID = $count2 
									AND internal_deadline.Month_ID = $month_selected 
									AND internal_deadline.Year_ID = $year_selected and internal_deadline.Trade ='$tradeString'";	
      $result2 = mysqli_query($conn,$sql2);
			
			if(mysqli_num_rows($result2) > 0)
			{
				if($NoLastDays > 0 || $count2 == $daycounter || $count2 == $daycounter + 6)
				{
					if($count2 <= 0)
					{
						echo "<td id='days' >  </td>";
					}
					else
					{
						echo "<td id='days' > $count2 </td>";
					}
					$NoLastDays = $NoLastDays-1;
				}
				else{
					echo "<td id='days'";
							if($count2 == $current_day)
							{				
								echo "style = 'border:dotted 3px #3396d9;'";
							}
              $readsql3 = "SELECT MAX(Day_ID) as Day_ID  FROM internal_deadline WHERE Month_ID = $month_selected and Year_ID = $year_selected and Trade ='$tradeString'";
              $result3 = mysqli_query($conn,$readsql3);
              $row = mysqli_fetch_assoc($result3);
              $lastinternaldl = $row['Day_ID'];
              if($count2 <= $lastinternaldl)
              {
                if($count2 >= $current_day)
                echo "onClick='checkDesigner(); getDate($count2,$month_selected,$year_selected,$weekday);'";
              }	
							echo "> $count2 
						<div class='$count2' id='daily_container'>";
					
					// SHOW INTERNAL DEADLINES
					while($rows2 = mysqli_fetch_assoc($result2))
					{
						$INcurID = $rows2['Team_ID']; // get TEAM ID
						$font_color = "white";
						
						if($INcurID == 5)
            {
              $font_color = "black";
            }
						echo "<p style='background-color: $in_dead_bg[$INcurID]; color:$font_color;' id='indeadline".$rows2['ID']."' class='event'>  ".$rows2['Project_Name']." - ".$rows2["Phase"]." (".$rows2['Trade'].") </p>";	
					
						echo "<script> 
											$(document).ready(function(){
												$('#indeadline".$rows2['ID']."').on('click',function(){
													var value = $(this).val();
														$.ajax(
														{
															url:'deadline.php',
															type:'post',
															data:'internal_details=true&internaldl_id=".$rows2['ID']."&phase=".$rows2['Phase']."',
															success:function(data)
															{   
																$('#content_tray').html(data);
															},
															error:function(data)
															{
                                alert(data);
															}
														});
												});
												});
										  </script>";
					}	
					echo"</div>
						</td>";
				}

			}
			
			else
			{
				if($NoLastDays > 0 || $count2 == $daycounter || $count2 == $daycounter + 6)
				{
					if($count2 <= 0)
					{
						echo "<td id='days' >  </td>";
					}
					else
					{
            //saturdays and sundays
						echo "<td id='days' > $count2 </td>";
					}
					$NoLastDays = $NoLastDays-1;
				}
				else{
				echo "<td id='days'";
							if($count2 == $current_day)
							{				
								echo "style = 'border:dotted 3px #3396d9;'";
							}
              $readsql3 = "SELECT MAX(Day_ID) as Day_ID  FROM internal_deadline WHERE Month_ID = $month_selected and Year_ID = $year_selected and Trade ='$tradeString'";
              $result3 = mysqli_query($conn,$readsql3);
              $row = mysqli_fetch_assoc($result3);
              $lastinternaldl = $row['Day_ID'];
              if($count2 <= $lastinternaldl)
              {
                if($count2 >= $current_day)
                echo "onClick='checkDesigner(); getDate($count2,$month_selected,$year_selected,$weekday);'";
              }	
							echo "> $count2
              <div class='$count2' id='daily_container'>";
              echo " </div>";
				}
			}
		}
		echo "</tr>";
		$daycounter = $count2;
		if($extradays!=0)
		{
			if($count < $loop - 2)
			{
				$dayweek = $count2 + 6; 
			}
			else
			{
				$dayweek = $totaldays;
				echo "<script> document.getElementById('extraDaysPrevMonth').value ='$totaldays'; </script>";
			}
		}
		else
		{
			$dayweek = $count2 + 6; 
		}
	}
    ?>

</table>
</div>
<div id="designerdl_pop" class="width-20pc">
  <input type="text" id="dnotif_id_designerdl" style="display:none;"/>
  <label class="title"> Move Deadline </label>
  <input type="date" id="edit_designerdl" class="display-block width-100pc"/>
  <input type="button" class="btn-normal white bg-green width-48pc" value="Save" id="save_designerdl_btn"/>
  <input type="button" class="btn-normal white bg-red width-48pc" value="Cancel" id="cancel_designerdl_btn"/>
</div>
</div>

<script>
  init();
  function init(){
    $('#edit_designer').hide();
    $('#delete_designer').hide();
    draggableMe('designerdl_pop');
  }
  
  $('#sel_des_pop_bg').on('click',function(){
    $(this).fadeOut();
    $('#sel_des_pop').slideUp();
  });
  
  $('#sel_des_tbox').on('click',function(e){
    getNames();
    e.stopPropagation();
    $('#asselect').slideToggle();
  });
  
  $('#sel_des_tbox').on('keyup',function(e){
    filter("sel_des_tbox","asselect");
    $('#asselect').slideDown();

    if($('#designer_id').val() === "0")
    {
      $('#sel_des_btn').fadeOut();
    }
  });
      //designer selected
  $('#sel_des_btn').on('click',function(){
    
    if($('#pop_up').css("visibility") != "hidden")
    {
      var id = $('#back_data_getDate_id').val();
      var month = $('#back_data_getDate_month').val();
      var year = $('#back_data_getDate_year').val();
      var weekday = $('#back_data_getDate_weekday').val();
      getDate(id,month,year,weekday);
    }
    $('#sel_des_pop_bg').fadeOut();
    $('#sel_des_pop').slideUp();
    $('#designer_name').val($('#sel_des_tbox').val());
    $('#pop_dname').html($('#sel_des_tbox').val());
      refreshUpdate();
      getChecklist();
      $('#sel_proj_tbox').val('');
  });
  
  $(document).ready(function(){
    $('#sel_proj_tbox').on('click',function(){
      $('#proj_select').slideToggle();
    });
  });
  
  $(".event").click(function (e) {
        e.stopPropagation();
      });
      
      function showLoader(){
        $('.loader_bg').show();
        $('.loader').show();
      }
      function hideLoader(){
        $('.loader_bg').hide();
        $('.loader').hide();
      }
      
  /* Manage Assign */
  
$(document).ready(function(){
	/* Assign Clicked */
	$('#assign_designer').on('click',function(){
    if(vAssignDesigner())
    {
		var value = $(this).val();
		/* gets budget in the list and place it into array */
		$('.budget_inp').each(function(e){
			var value = $(this).val();
			budget.push(value);
      if(value != 0)
      {
        showLoader();
      }
		});
    
    /* gets budget in the list and place it into array */
		$('.check_item').each(function(e){
			var value = $(this).prop("id");
			ReviewChecklist_id.push(value);
		});
		$.ajax(
		{
			beforeSend:function(){
				var send = false;
        var value = $('#budget').val();
					if(value > 0)
					{
						send = true;
					}
					if(value == 0)
					{
							send = false;
					}
				return send;
			},
			url:'reviewSubmit.php',
			type:'post',
			data:{ 
							assign_designer:value,
	            project_id: $('#projectid').val(),
	            internaldl_id: $('#internaldl_id').val(),
	            designer_id: $('#designer_id').val(),
	            total_budget: $('#budget').val(),
	            deadline: $('#to_date').val(),					
							activity: " ",
							budgets:budget,
              reviewchecklist_id:ReviewChecklist_id
						},
			success:function(data)
			{ 
        refreshUpdate();
        closePop();
        clearAssignDes();
        hideLoader();
	    },
			error:function(data)
			{
				alert(data);
			}
		});
  }
	});

	/* Edit Clicked */
	$('#edit_designer').on('click',function(){
		if(vAssignDesigner())
    {
		var value = $(this).val();
		/* gets budget in the list and place it into array */
		$('.budget_inp').each(function(e){
			var value = $(this).val();
			budget.push(value);
      if(value != 0)
      {
        showLoader();
      }
		});
    
    /* gets budget in the list and place it into array */
		$('.check_item').each(function(e){
			var value = $(this).prop("id");
			ReviewChecklist_id.push(value);
		});
    
    /* gets sched_dnotif_id in the list and place it into array */
		$('.sched_dnotif_id').each(function(e){
			var value = $(this).val();
			sched_dnotif_id.push(value);
      if(value != 0)
      {
        showLoader();
      }
		});
		$.ajax(
		{
			beforeSend:function(){
				var send = false;
        var value = $('#budget').val();
					if(value > 0)
					{
						send = true;
					}
					if(value == 0)
					{
							send = false;
					}
				return send;
			},
			url:'reviewSubmit.php',
			type:'post',
			data:{ 
							edit_designer:value,
	            project_id: $('#projectid').val(),
	            internaldl_id: $('#internaldl_id').val(),
	            designer_id: $('#designer_id').val(),
	            total_budget: $('#budget').val(),
	            deadline: $('#to_date').val(),					
							activity: " ",
							budgets:budget,
              reviewchecklist_id:ReviewChecklist_id,
              sched_dnotif_id:sched_dnotif_id
						},
			success:function(data)
			{ 
        alert(data);
        refreshUpdate();
        closePop();
        clearAssignDes();
        hideLoader();
	    },
			error:function(data)
			{
				alert(data);
			}
		});
  }
	});

	/* Delete Clicked */
	$('#delete_designer').on('click',function(){
		$.ajax(
		{
			beforeSend:function(){
				return confirm('Are you sure you want to delete?');
			},
			url:'reviewSubmit.php',
			type:'post',
			data:'delete_designer=true'+
					 '&dnotif_id='+$('#dnotif_id').val(),
			success:function(data){
				refreshUpdate();
			},
		});
	});
});


function clearAssignDes(){
  $('#sel_proj_tbox').val("");
  $('#budget').val("");
  $('#from_date').val("");
  $('#to_date').val("");

  /* clears budget in the list and place it into array */
  $('.budget_inp').each(function(e){
    $(this).val("0");
    $(this).css("box-shadow","none");
    $(this).css("color","#444f5d");
    $(this).css("font-weight","normal");
    $(this).prop('disabled',true);
    $(this).css('cursor','not-allowed');
    budget = [];
  });

  /* c;ears budget in the list and place it into array */
  $('.check_item').each(function(e){
    ReviewChecklist_id = [];
  });
  
  $('.sched_dnotif_id').each(function(e){
    sched_dnotif_id = [];
  });
}
  
function refreshUpdate(){
	var date  = new Date();
  var selected_day = date.getDate();
  var selected_month = date.getMonth() + 1;
  var selected_year = date.getFullYear();

  /* Refresh calendar */
  $.ajax(
  {
    url:'main_reviewerDeadline.php',
    type:'post',
    data:'month_selected='+selected_month+
         '&year_selected='+selected_year+
         '&tradeString='+$('#tradeString').val()+	
         '&NoLastDays='+$('#NoLastDays').val()+
         '&designer_id='+$('#designer_id').val(),
    success:function(data)
    {
      $('#calendar').html(data);
    },
  });
  budget = [];
  ReviewChecklist_id = [];

}
//save_designerdl_btn
$('#save_designerdl_btn').on('click',function(){
  $.ajax(
  {
    beforeSend:function(){
      return confirm('Confirm move deadline.');
    },
    url:'reviewSubmit.php',
    type:'post',
    data:'move_desdl=true'+
         '&deadline='+$('#edit_designerdl').val()+
         '&dnotif_id='+$('#dnotif_id_designerdl').val(),
    success:function(){
      refreshUpdate();
      hideDesignerdlPop();
    },
    error:function(data){
      alert(data);
    }
  });
});

//cancel_designerdl_btn
$('#cancel_designerdl_btn').on('click',function(){
  hideDesignerdlPop();
});

function showDesignerdlPop(){
  $('#designerdl_pop').slideDown();
}

function hideDesignerdlPop(){
  $('#designerdl_pop').slideUp();
}

$('#back_btn').on('click',function(){
  closePop();
  clearAssignDes();
  getChecklist();
  $('#to_date').val($('#from_date').val());
  $('#assign_designer').css('visibility','hidden');
  $('#edit_designer').fadeOut();
});

function getChecklist(){
  $.ajax({
    url:'rGetChecklist.php',
    type:'post',
    data:'from_reviewer=true',
    success:function(data){
      $('#checklist_container').html(data);
    }
  });
}

</script>
