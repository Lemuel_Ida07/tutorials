<?php 
    session_start();
    include('connect.php');

	$today = date("F j, Y");
?>
<style>
	
	#menu_item_logo2
	{
		background-color:#f2f2f2;
	}
	#menu_item_logo2:hover
	{
		background-color:#f2f2f2;
	}
	#tab2
	{
		color:#515151;
		font-weight: bold;
		text-shadow: none;
	}

	#info
	{
		height:auto;
		width:100%;
		margin-left:0;
		padding-left:0;
		max-width:100%;
	}

	#list
	{
		width:auto;
		display:block;
		max-width:10000px;
		max-height:100px;
		overflow:auto;
	}

	#editbtn,#deletebtn,#donebtn
	{
		width:90px;
	}

	#donebtn
	{
		display:none;
	}
</style>
<div id="info">
				<h2>My Daily Submittal Summary </h2>
				<h2><?php echo $_SESSION['Team_Name']; ?></h2>
				<h3 style="padding-bottom:0;margin-bottom:0;"><?php echo $today; ?></h3>
				<div id="topic" >
					<h2> Submittal Details </h2>
					<select style="padding:3%;"name="ProjectNumber" id="ProjectNumber" required="required">
						<option> Select Project Number </option>
							<?php
								 $designer_id = $_SESSION["ID"];
								 $team_id = $_SESSION["Team_ID"];
								 $sql = "SELECT ID,Project_Number ,Project_Name FROM project WHERE Team_ID = '".$team_id."'";
								 $result = mysqli_query($conn,$sql);
								 if(mysqli_num_rows($result) > 0)
								 {
									 while($rows = mysqli_fetch_assoc($result))
									 {
										 echo "<option value='".$rows["ID"]."'> ".$rows["Project_Number"]." - ".$rows["Project_Name"]."</option>";
									 }
								 }
							?>
					</select>
					<input type="text" list="ticketN"  name="TicketNumber" id="ticket"  placeholder="RFI Ticket Number" />
					<input type="date" name="Deadline" value="<?php echo date("Y-m-d"); ?>" id="Deadline" />	
				</div>
				<div id="topic">
					<h2> Deliverables </h2>
					<input type="text" id="drawings" placeholder="Paste Link of Drawings Here" />
					<input type="text" id="drawing_list" placeholder="Paste Link of Drawing List Here" />
					<input type="text" id="blueprint_request" placeholder="Paste Link of Blueprint Request Here" />	
				</div>
				<div id="topic">
					<h2 style="visibility:hidden;">df</h2>
					<input type="text" id="specifications" placeholder="Paste Link of Specifications Here" />
					<input type="text" id="calculations" placeholder="Paste Link of Calculations Here" />
					<input type="text" id="reports" placeholder="Paste Link of Reports Here" />		
				</div>
				<div id="topic">	
					<h2> Validation </h2>
					<input type="text" id="outgoing_email" placeholder="Paste Link of Outgoing E-mail here" />						
					<input type="text" id="submittal_form_transmittal" placeholder="Paste Link of Submittal Form and Transmittal here" />		
					<input type="text" id="deliverables_list" placeholder="Paste Link of Deliverables List" />
				</div>
				<button style="display:inline-block; width:15%; margin-left:0;" id="submit1" class="submit_submittal" >Submit</button>
				<button style="display:inline-block; width:15%; margin-left:0;" id="submit1" class="generate_report">Generate Report</button>
			</div>
			<div id="list" style="max-height:200px;">
				<?php
					$readsql = "SELECT dc_submittal.ID, 
												project.Project_Number, 
												project.Project_Name, 
												dc_submittal.Ticket_Number, 
												dc_submittal.Deadline, 
												dc_submittal.Drawings, 
												dc_submittal.Drawing_List, 
												dc_submittal.Blueprint_Request, 
												dc_submittal.Specifications, 
												dc_submittal.Calculations, 
												dc_submittal.Reports, 
												dc_submittal.Outgoing_Email, 
												dc_submittal.Submittal_Form_Transmittal, 
												dc_submittal.Deliverables_List
											FROM `dc_submittal`
											INNER JOIN project 
												ON dc_submittal.Project_ID = project.ID";
					$result = mysqli_query($conn,$readsql);
					if(mysqli_num_rows($result) > 0)
					{
						echo "<table id='tbl' class='general_table width-100pc'>";
						echo "<thead>";
						echo "<tr>";
						echo "	<th>Project Number</th>";
						echo "	<th>Project Name</th>";
						echo "	<th>Ticket Number</th>";
						echo "	<th>Deadline</th>";
						echo "	<th>Drawings</th>";
						echo "	<th>Drawing List</th>";
						echo "	<th>Blueprint Request</th>";
						echo "	<th>Specifications</th>";
						echo "	<th>Calculations</th>";
						echo "	<th>Reports</th>";
						echo "	<th>Outgoing Email</th>";
						echo "	<th>Submittal Form Transmittal</th>";			
						echo "	<th>Deliverables List</th>";
						echo "  <th>Actions</th>";
						echo "</tr>";
						echo "</thead>";
						echo "<tbody>";
						while($rows = mysqli_fetch_assoc($result))
						{
							echo "<tr>";
							echo "<td> ".$rows['Project_Number']." </td>";	
							echo "<td> ".$rows['Project_Name']." </td>";
							echo "<td> ".$rows['Ticket_Number']." </td>";
							echo "<td> ".$rows['Deadline']." </td>";
							echo "<td><a> ".$rows['Drawings']." </a></td>";
							echo "<td><a> ".$rows['Drawing_List']." </a></td>";
							echo "<td><a> ".$rows['Blueprint_Request']." </a></td>";
							echo "<td><a> ".$rows['Specifications']." </a></td>";
							echo "<td><a> ".$rows['Calculations']." </a></td>";
							echo "<td><a> ".$rows['Reports']." </a></td>";
							echo "<td><a> ".$rows['Outgoing_Email']." </a></td>";
							echo "<td><a> ".$rows['Submittal_Form_Transmittal']." </a></td>";
							echo "<td><a> ".$rows['Deliverables_List']." </a></td>";
							echo "<td><input id='deletebtn' class='".$rows['ID']."' type='submit' value='Delete' name='delete' /></td>";
							echo "<script>
											$(document).ready(function(){
												$('.".$rows['ID']."').on('click',function(){
													var value = $(this).val();
													$.ajax(
														{
															url:'dc_submit.php',
															type:'post',
															data:'delete_submittal='+value+
																	 '&submittal_id=".$rows['ID']."',
															success:function(data)
																{
																	$('#list').html(data);
																},
														});
												});
											});
										</script>";
							echo"</tr>";
						}
						echo "</tbody>";
						echo "</table>";
					}
				?>
			</div>
			<script>
				$(document).ready(function(){
					$('.submit_submittal').on('click',function(){
						var value = $(this).val();
						$.ajax(
							{
								url:'dc_submit.php',
								type:'post',
								data:'submittal='+value+
										 '&project_id=' + document.getElementById('ProjectNumber').value +	
										 '&ticket_number=' + document.getElementById('ticket').value +
										 '&deadline=' + document.getElementById('Deadline').value +
										 '&drawings=' + document.getElementById('drawings').value +
										 '&drawing_list=' + document.getElementById('drawing_list').value +
										 '&blueprint_request=' + document.getElementById('blueprint_request').value +
										 '&specifications=' + document.getElementById('specifications').value +
										 '&calculations=' + document.getElementById('calculations').value +
										 '&reports=' + document.getElementById('reports').value +
										 '&outgoing_email=' + document.getElementById('outgoing_email').value +
										 '&submittal_form_transmittal=' + document.getElementById('submittal_form_transmittal').value +
										 '&deliverables_list=' + document.getElementById('deliverables_list').value,
								success:function(data)
								{
									$('#list').html(data);
								},
							});
					});
				});

				$(document).ready(function(){
					$('.generate_report').on('click',function(){
						var value = $(this).val();
						$.ajax(
							{
								url:'dc_table.php',
								type:'post',
								data:'submittal='+value,
								success:function(data)
								{
									$('#mainform').html(data);
								},
							});
					});
				});
				
			</script>