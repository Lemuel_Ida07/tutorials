<?php
		include('connect.php');
		$today = date("F j, Y");

		if(isset($_POST['add_project']))
		{
			$project_number = $_POST['ProjectNumber'];
			$project_name = $_POST['ProjectName'];
			//$ticket_number = $_POST['TicketNumber'];
			$team_id = $_POST['team_id'];
			$sql = "select Project_Number from project where Project_Number = '$project_number'";
			$result = mysqli_query($conn,$sql);
			if(mysqli_num_rows($result) > 0)
			{
				echo "<script>alert('Project number ($project_number) already exists!');</script>";
			}
			else
			{
				$sql = "insert into project (Project_Number,Project_Name,Team_ID) values('$project_number','$project_name','$team_id')";
				if(mysqli_query($conn,$sql))
				{
					echo "<script>alert('Project $project_name successfuly added!');</script>";
				}
			}
		}

		if(isset($_POST['add_designer']))
		{
			$designer_username = $_POST['dusername'];
			$designer_fname = $_POST['fname'];
			$designer_mname = $_POST['mname'];
			$designer_lname = $_POST['lname'];
			$designer_teamid = $_POST['team_id'];
			
			$sql = "select Username from user where Username = '$designer_username'";
			$result = mysqli_query($conn,$sql);
			$sql2 = "select Firstname,Middlename,Lastname from user where Firstname = '$designer_fname' AND Middlename = '$designer_mname' AND Lastname = '$designer_lname' ";
			$result2 = mysqli_query($conn,$sql2);
			if(mysqli_num_rows($result) > 0)
			{
				echo "<script>alert('($designer_username) Username already exists!');</script>";
			}
			elseif(mysqli_num_rows($result2) > 0) 
			{
				echo "<script>alert('Name already exists!');</script>";
			}
			else
			{	
				$sql = "INSERT INTO user(Username,Firstname,Middlename,Lastname,User_Type,Team_ID) VALUES('$designer_username','$designer_fname','$designer_mname','$designer_lname',0,'$designer_teamid')";
				if(mysqli_query($conn,$sql))
				{echo "<script>alert('Designer $designer_fname $designer_mname $designer_lname successfuly added!');</script>";}
			}
		}
		
		if(isset($_POST['edit_designer']))
		{
			$designer_id = $_POST['editid'];
			$designer_username = $_POST['editdusername'];
			$designer_fname = $_POST['editdfname'];
			$designer_mname = $_POST['editdmname'];
			$designer_lname = $_POST['editdlname'];
			$sql = "UPDATE user SET Username = '$designer_username',Firstname = '$designer_fname',Middlename = '$designer_mname',Lastname = '$designer_lname' WHERE ID = '$designer_id'";
			if(mysqli_query($conn,$sql))
			{}
		}
		
		if(isset($_POST['edit_project']))
		{
			$project_number = $_POST['editproject_number'];
			$remarks = $_POST['remarks'];
			$sql = "UPDATE project SET Remarks = '$remarks' WHERE Project_Number = '$project_number'";
			if(mysqli_query($conn,$sql))
			{}
		}
?>
<style>
	#menu_item_logo1
	{
		background-color:#f2f2f2;
	}
	#menu_item_logo1:hover
	{
		background-color:#f2f2f2;
	}
	#tab1
	{
		color:#515151;
		font-weight: bold;
		text-shadow: none;
	}
</style>
<div id="info">
	<h3><?php echo $today; ?></h3>
	<h1> New Project </h1>
	<form method="post">
		<textarea name="ProjectName" rows="2" cols="20" placeholder="Project Name" required="required"></textarea>
		<input type="text" id="projectnumber" name="ProjectNumber" placeholder="Project Number" required="required"/><br/>
		<select name="team_id" required>
			<option> - Select Team - </option>
			<?php
			$sql = "SELECT * FROM team";
			$result = mysqli_query($conn,$sql);
			if(mysqli_num_rows($result) > 0)
			{
				while($rows = mysqli_fetch_assoc($result))
				{
					echo "<option value='".$rows['ID']."'> ".$rows['Team_Name']." </option>";
				}
			}
            ?>
		</select>
		<input type="submit" value="Add Project" name="add_project" id="addbtn" style = "margin-left:15%; width:70%"/>
	</form>
	<hr/>
	<h1> New Designer </h1>
	
	<form method="post">
		<input type="text" name="dusername" placeholder="Username" required/><br/>
		<input type="text" name="fname" placeholder="Firstname" required/><br/>
		<input type="text" name="mname" placeholder="Middlename" required/><br/>
		<input type="text" name="lname" placeholder="Lastname" required/><br/>
		<select name="team_id" required>
			<option> - Select Team - </option>
			<?php
			$sql = "SELECT * FROM team";
			$result = mysqli_query($conn,$sql);
			if(mysqli_num_rows($result) > 0)
			{
				while($rows = mysqli_fetch_assoc($result))
				{
					echo "<option value='".$rows['ID']."'> ".$rows['Team_Name']." </option>";
				}
			}
            ?>
		</select>
		<input type="submit" value="Add Designer" name="add_designer" id="addbtn" style = "margin-left:15%; width:70%"/>
	</form>
	
	<form method="post" action="table.php" style="visibility:hidden">
		<button value="Submit" name="submit" id="submit2">Submit</button>
	</form>
</div>
<div id="list">
	<div class="table_container">
	<h2 class="table_title"> Projects </h2>
	<div class="search_group">
		<input type="text" class="searchbox" id="search_project" placeholder="Search..." />
		<input type="submit" value="" class="searchbtn" />
	</div>
		<label id="result"> </label>
	<table id="activitylist">
	<!--<tr><th> Project Number </th><th> Project Name </th><th> Team </th><th> Remarks </th><th> Actions </th></tr>-->
	<tr><th> Project Number </th><th> Project Name </th><th> Remarks </th><th> Actions </th></tr>
	<?php
		//$sql = "SELECT Project_Number,Project_Name,Team_ID FROM project";
		$sql = "SELECT Project_Number,Project_Name FROM project";
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			while($rows = mysqli_fetch_assoc($result))
			{
				/*$sql2 = "SELECT * FROM team WHERE ID = ".$rows['Team_ID']."";
				$result2 = mysqli_query($conn,$sql2);
				$row = mysqli_fetch_assoc($result2);
				$listed_team_name = $row['Team_Name'];
*/				
				echo "<form method='post'>
						<tr>
							<td><input type='text' readonly style='padding:1%;' value='".$rows['Project_Number']."' name='editproject_number'/></td>
							<td>".$rows['Project_Name']."</td>
							<td>
						<select name='remarks'>";
							$sql3 = "SELECT Remarks FROM project WHERE Project_Number = '".$rows['Project_Number']."'";
							$result3 = mysqli_query($conn,$sql3);
							if(mysqli_num_rows($result3) > 0)
							{
								while($rows3 = mysqli_fetch_assoc($result3))
								{
									if($rows3['Remarks'] == "0")
									{
										echo "<option value=".$rows3['Remarks']."> RFI </option>
																  <option value='1'> Billable </option>";
									}
									else
									{
										echo "<option value=".$rows3['Remarks']."> Billable </option>
																  <option value='0'> RFI </option>";
									}
								}
							}
				echo "</select>
						</td>
						<td style='width: 50px;'><input style='text-align: center;' style='width: 50px;' id='editbtn' type='submit' value='Edit' name='edit_project' /></td>
					</tr>
					</form>";
			}
		}
    ?>
	</table>
		</div>
	<div class="table_container">
	<h2 class="table_title"> Tickets </h2>
	<table id="activitylist">
		<tr><th>Ticket Numbers</th><th>Project</th></tr>
		<?php
		$sql = "SELECT  Ticket_Number, CONCAT(Project_Number,' - ',Project_Name) as Project  FROM ticket INNER JOIN project ON ticket.Project_ID = project.ID";
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			while($rows = mysqli_fetch_assoc($result))
			{
				echo "<form method='post'>
						<tr>
							<td><input type='text' style='padding:1%;' value='".$rows['Ticket_Number']."' name='editproject_number'/></td>
							<td><input type='text' style='padding:1%;' value='".$rows['Project']."' name='editproject_name'/></td>
						</tr>
						  </form>";
			}
		}
        ?>
		</table>
		</div>
		<div class="table_container">
		<h2 class="table_title"> Designers </h2>
		<table id="activitylist">
			<tr><th>Username</th><th>Firstname</th><th>Middlename</th><th>Lastname</th><th>Status</th><th>Action</th></tr>
			<?php
			$sql = "SELECT ID,Username,Firstname,Middlename,Lastname,Status FROM user WHERE User_Type = 0";
			$result = mysqli_query($conn,$sql);
			if(mysqli_num_rows($result) > 0)
			{
				while($rows = mysqli_fetch_assoc($result))
				{
					echo "<form method='post'>
					<tr>
					<td><input type='text' style='padding:1%;' value='".$rows['Username']."' name='editdusername'/></td>
					<td><input type='text' style='padding:1%;' value='".$rows['Firstname']."' name='editdfname'/></td>
					<td><input type='text' style='padding:1%;' value='".$rows['Middlename']."' name='editdmname'/></td>
					<td><input type='text' style='padding:1%;' value='".$rows['Lastname']."' name='editdlname'/></td>	
					<td><input type='text' style='padding:1%;' value='".$rows['Status']."' name='editdstatus'/></td>
					<td><input type='submit'  value='Edit' name='edit_designer' id='editbtn'/></td>
					<td><input type='text' style='display:none;' value='".$rows['ID']."' name='editid'/></td>
					</tr>
					 </form>";
				}
			}
            ?>
		</table>
	</div>
</div>

<script>
$(document).ready(function(){
		$('#searchbtn').on('click',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+document.getElementById('search_project').value,
				success:function(data)
				{   
					$('#activitylist').html(data);
				},
			});
		});
	});
	
	$(document).ready(function(){
		$('#search_project').on('input',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+document.getElementById('search_project').value,
				success:function(data)
				{   
					$('#activitylist').html(data);
				},
			});
		});
	});
</script>