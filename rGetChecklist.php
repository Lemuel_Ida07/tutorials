<?php
  session_start();
  include('connect.php');
  $tradeString = $_SESSION["Trade"];
if(isset($_POST["from_reviewer"]))
{
$readsql = "SELECT 
                      DISTINCT Tag,
                      (SELECT COUNT(Check_Item)
                        FROM review_checklist t2
                        WHERE Trade = '$tradeString'
                          AND Tag = t1.Tag) AS Tag_Length 
                    FROM review_checklist t1
                    WHERE 
                      Trade = '$tradeString'
                    ORDER BY Tag_Length";
        $result = mysqli_query($conn,$readsql);
        if(mysqli_num_rows($result) > 0)
        {
          while($rows = mysqli_fetch_assoc($result))
          {
            echo "<div class='check_box'>
                    <label class='title'> ".$rows["Tag"]." </label>
                    <hr class='tag'/>";
            $itemsql 
            = "SELECT ID,Check_Item
                FROM review_checklist
                WHERE 
                  Tag = '".$rows["Tag"]."'
                  AND Trade = '$tradeString'";
            $itemresult = mysqli_query($conn,$itemsql);
            if(mysqli_num_rows($itemresult) > 0)
            {
              echo "<ul>";
              while($itemrows = mysqli_fetch_assoc($itemresult))
              {
                echo 
                "<li id='".$itemrows["ID"]."' class='text check_item'> 
                  <input type='text' 
                         id='check_item".$itemrows["ID"]."'
                         class='budget_inp' 
                         onkeydown='clearValue(this.id)'
                         onkeyup='calculateHours(this.value,this.id)' 
                         placeholder='0'
                         value='0'
                         disabled='disabled'
                         maxlength='2'/>
                  ".$itemrows["Check_Item"]."
                 </li>";
              }
              echo "</ul>";
            }
            echo "</div>";
          }
        }
}