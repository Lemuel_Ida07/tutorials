<?php
  session_start();
  include("connect.php");
  
  if(isset($_POST["get_cad_tracking"]))
  {
    $status = $_POST["status"];
    
    $curr_time = date("h:i");
		$cadmngr_id = $_SESSION['ID'];
		$dnotif_id = 0;
		$curr_year = date("Y");
		$curr_month = date("m");
		$curr_day = date("d");
    $tradeString = $_SESSION["Trade"];
		
    if($curr_day <= 15)
    {
      $day_limit = 15;																																																																													 
    }
    
    if($curr_day > 15)
		{
      $day_limit = 31; 
    }
    
    /*$sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        requirements_for_des.Budget,
        designer_notif.Deadline,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status,
        CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer,
        user.Trade,
        user.ID as CADTech_ID
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID
      INNER JOIN requirements_for_des
        ON designer_notif.ID = requirements_for_des.DNotif_ID
      INNER JOIN review_checklist
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID
      INNER JOIN user 
        ON user.ID = designer_notif.Designer_ID
      WHERE 
        EXTRACT(YEAR FROM(designer_notif.Deadline)) <= $curr_year
        AND EXTRACT(MONTH FROM(designer_notif.Deadline)) <= $curr_month
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) <= $day_limit
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) >= EXTRACT(DAY FROM(CURDATE()))
        AND requirements_for_des.Budget > 0
      ORDER BY designer_notif.Deadline";*/
    $sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        requirements_for_des.Budget,
        designer_notif.Deadline,
        designer_notif.Designer_ID,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status,
        CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer,
        user.Trade,
        user.ID as CADTech_ID
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID
      INNER JOIN requirements_for_des
        ON designer_notif.ID = requirements_for_des.DNotif_ID
      INNER JOIN review_checklist
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID
      INNER JOIN user 
        ON user.ID = designer_notif.Designer_ID
      WHERE 
        requirements_for_des.Budget > 0
      ORDER BY designer_notif.Deadline";
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
      $standby_count = 0;
      $assigned_count = 0;
      $requested_count = 0;
      $approved_count = 0;
      $rejected_count = 0;
			while($rows = mysqli_fetch_assoc($result))
			{
        switch($status){
          case "Standby": 
            if($rows["Status"] == "Approved")
            {
              $cadtechsql 
              = "SELECT 
                  user.Username,
                  user.ID AS CADTech_ID,
                  cadtech_notif.Status
                FROM cadtech_notif 
                INNER JOIN user 
                  ON cadtech_notif.CadTech_ID = user.ID
                WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"]."";
              $cadtechresult = mysqli_query($conn,$cadtechsql);
              $cadtechrow = mysqli_fetch_assoc($cadtechresult);
              if(mysqli_num_rows($cadtechresult) > 0){
                switch($cadtechrow["Status"])
                {
                  case "Standby":
                    $assigned_count ++;
                    echo "<tr style='display:none;'>
                            <td>
                              <script> 
                                $('#assignment_count').html('($assigned_count)');
                              </script>
                            </td>
                          </tr>";
                    break;
                  case "Working":
                    $assigned_count ++;
                    echo "<tr style='display:none;'>
                            <td>
                              <script> 
                                $('#assignment_count').html('($assigned_count)');
                              </script>
                            </td>
                          </tr>";
                    break;
                }
              }
              else
              {
                $standby_count ++;
                echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                echo "
                      <td> ".$rows["Designer"]." </td>
                      <td class='emphasize'> ".$rows["Deadline"]." </td>
                      <td> ".$rows["Project_Number"]." - 
                           ".$rows["Project_Name"]." - 
                           ".$rows["Phase"]." </td>
                      <td> ".$rows["Check_Item"]." </td>
                      <td> ".$rows["Ticket_Number"]." </td>";
                echo "<td>
                        <label id='assigned'>";
                echo "</label>
                  <a 
                  type='submit'
                  id='assign_link".$rows["req_for_des_id"]."'
                  class='assign_link'>
                  +
                  </a>
                  <script> 
                    $('#standby_count').html('($standby_count)');
                    $(document).ready(function(){
                      $('#assign_link".$rows["req_for_des_id"]."').on('click',
                      function(e){
                        $('#white_pop').css({'top':e.pageY,'left':e.pageX-300});
                        $('#white_pop').slideDown();
                        $('#selected_designer_id').val('".$rows["Designer_ID"]."'); 
                        $('#selected_des_req_id').val('".$rows["req_for_des_id"]."'); 
                        $('#selected_project_id').val('".$rows["Project_ID"]."'); 
                      });
                    });
                  </script>
                </td>
                </tr>";
        }
            }
        break;
          case "Assigned":
            if($rows["Status"] == "Approved")
            {
            $cadtechsql 
            = "SELECT 
              user.Username, 
              user.ID AS CADTech_ID, 
              cadtech_notif.Status,
              cadtech_notif.Budget,
              cadtech_notif.ID AS CNotif_ID
            FROM cadtech_notif 
            INNER JOIN user 
              ON cadtech_notif.CadTech_ID = user.ID
            WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"]."";
        $cadtechresult = mysqli_query($conn,$cadtechsql);
        if(mysqli_num_rows($cadtechresult) > 0){
          while($cadtechrows = mysqli_fetch_assoc($cadtechresult))
          {
            switch($cadtechrows["Status"])
              {
                case "Working":
                  $assigned_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."$assigned_count'>";
                  echo "
                      <td> ".$rows["Designer"]." </td>
                      <td class='emphasize'> ".$rows["Deadline"]." </td>
                      <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                      <td> ".$rows["Check_Item"]." </td>
                      <td> ".$rows["Ticket_Number"]." </td>";
                  echo "<td>
                  <label id='assigned'>";
                  echo " <span class='bg-yellow' style='position:absolute;width:12px; height:12px;  border-radius:100%; margin:0px; padding:0px;'> </span> 
                         <span style='margin-left:16px;'>".$cadtechrows["Username"]." ( ".$cadtechrows["Budget"]." )<br /></span><br />";   
                  echo "</label>
                  <a 
                  type='submit'
                  id='assign_link".$rows["req_for_des_id"]."$assigned_count'
                  class='assign_link'>
                  +
                  </a>
                  <a 
                  type='submit'
                  id='delete_link".$rows["req_for_des_id"]."$assigned_count'
                  class='delete_link'>
                  &#10006
                  </a>
                     <script>
                       $('#assigned_count').html('($assigned_count)');
                    $(document).ready(function(){
                      $('#assign_link".$rows["req_for_des_id"]."$assigned_count').on('click',function(e){
                        $('#white_pop').css({'top':e.pageY,'left':e.pageX-300});
                        $('#white_pop').slideDown();
                        $('#selected_designer_id').val('".$rows["Designer_ID"]."');
                        $('#selected_des_req_id').val('".$rows["req_for_des_id"]."'); 
                        $('#selected_project_id').val('".$rows["Project_ID"]."'); 
                      });
                      
                      $('#delete_link".$rows["req_for_des_id"]."$assigned_count').on('click',function(e){
                        $.ajax({
                          beforeSend:function(data){
                            return confirm('Are you sure you want to unassign?');
                          },
                          url:'cadmngrSubmit.php',
                          type:'post',
                          data:'delete_assigned=true'+
                               '&cadtech_notif_id=".$cadtechrows["CNotif_ID"]."',
                          success:function(data){
                            location.reload();
                          }
                        }); 
                      });
                    });
                     </script>
                   </td>
                  </tr>";
                  break;
                case "Standby":
                  $assigned_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."$assigned_count'>";
                  echo "
                      <td> ".$rows["Designer"]." </td>
                      <td class='emphasize'> ".$rows["Deadline"]." </td>
                      <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                      <td> ".$rows["Check_Item"]." </td>
                      <td> ".$rows["Ticket_Number"]." </td>";
                  echo "<td>
                  <label id='assigned'>";
                  echo " <span class='bg-gray' style='position:absolute;width:12px; height:12px; border-radius:100%; margin:0px; padding:0px;'> </span> 
                         <span style='margin-left:16px;'>".$cadtechrows["Username"]." ( ".$cadtechrows["Budget"]." )<br /></span><br />";   
                  echo "</label>
                  <a 
                  type='submit'
                  id='assign_link".$rows["req_for_des_id"]."$assigned_count'
                  class='assign_link'>
                  +
                  </a>
                  <a 
                  type='submit'
                  id='delete_link".$rows["req_for_des_id"]."$assigned_count'
                  class='delete_link'>
                  &#10006
                  </a>
                     <script>
                       $('#assigned_count').html('($assigned_count)');
                    $(document).ready(function(){
                      $('#assign_link".$rows["req_for_des_id"]."$assigned_count').on('click',function(e){
                        $('#white_pop').css({'top':e.pageY,'left':e.pageX-300});
                        $('#white_pop').slideDown();
                        $('#selected_designer_id').val('".$rows["Designer_ID"]."');
                        $('#selected_des_req_id').val('".$rows["req_for_des_id"]."'); 
                        $('#selected_project_id').val('".$rows["Project_ID"]."'); 
                      });
                      
                      $('#delete_link".$rows["req_for_des_id"]."$assigned_count').on('click',function(e){
                        $.ajax({
                          beforeSend:function(data){
                            return confirm('Are you sure you want to unassign?');
                          },
                          url:'cadmngrSubmit.php',
                          type:'post',
                          data:'delete_assigned=true'+
                               '&cadtech_notif_id=".$cadtechrows["CNotif_ID"]."',
                          success:function(data){
                            location.reload();
                          }
                        }); 
                      });
                    });
                     </script>
                   </td>
                  </tr>";
                  break;
              } 
          }
          
            }
            }
          break;
          case "CADRequested":
            if($rows["Status"] == "Approved")
            {
              $cadtechsql 
              = "SELECT 
                user.Username, 
                user.ID AS CADTech_ID, 
                cadtech_notif.Status,
                cadtech_notif.Budget,cadtech_notif.ID AS cadtech_notif_id
              FROM cadtech_notif 
              INNER JOIN user 
                ON cadtech_notif.CadTech_ID = user.ID
              WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"]."";
        $cadtechresult = mysqli_query($conn,$cadtechsql);
        if(mysqli_num_rows($cadtechresult) > 0){
          while($cadtechrows = mysqli_fetch_assoc($cadtechresult))
          { 
            switch($cadtechrows["Status"])
            {
              case "CAD Requested":
                $requested_count ++;
                echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                echo "
                    <td> ".$rows["Designer"]." </td>
                    <td class='emphasize'> ".$rows["Deadline"]." </td>
                    <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                    <td> ".$rows["Check_Item"]." </td>
                    <td> ".$rows["Ticket_Number"]." </td>";
                echo "<td>
                        <label id='assigned'>";

                echo "<span style='margin-left:16px;'>".$cadtechrows["Username"]." ( ".$cadtechrows["Budget"]." )<br /></span><br />";   
                echo "</label>
                      </td>
                      <td>
                        <input type='submit'
                               id='accept_btn".$cadtechrows["cadtech_notif_id"]."'
                               value='Accept'
                               class='btn-normal bg-green white emphasize' />

                        <input type='submit'
                               id='reject_btn".$cadtechrows["cadtech_notif_id"]."'
                               value='Reject'
                               class='btn-normal bg-red white emphasize' />

                        <script> 
                          $('#requested_count').html('($requested_count)');
                            $('#accept_btn".$cadtechrows["cadtech_notif_id"]."').on('click',function(){
                              $.ajax({
                                beforeSend:function(){
                                  return confirm('Are yous sure you want to Accept?');
                                },
                                url:'cadmngrSubmit.php',
                                type:'post',
                                data:'accept_cad=true'+
                                     '&cadtech_notif_id=".$cadtechrows["cadtech_notif_id"]."'+
                                     '&req_for_des_id=".$rows["req_for_des_id"]."',
                                success:function(data){
                                  location.reload();
                                }
                              });
                            });

                            $('#reject_btn".$cadtechrows["cadtech_notif_id"]."').on('click',function(){
                              $.ajax({
                                beforeSend:function(){
                                  return confirm('Are yous sure you want to Reject?');
                                },
                                url:'cadmngrSubmit.php',
                                type:'post',
                                data:'reject_cad=true'+
                                     '&cadtech_notif_id=".$cadtechrows["cadtech_notif_id"]."'+
                                     '&req_for_des_id=".$rows["req_for_des_id"]."',
                                success:function(data){
                                  location.reload();
                                }
                              });
                            });
                        </script>
                      </td>
                      </tr>";
                break;
              }
            }
          }
          }
          break;
          case "CADRejected":
            if($rows["Status"] == "Approved")
            {
            $cadtechsql 
            = "SELECT 
              user.Username, 
              user.ID AS CADTech_ID, 
              cadtech_notif.Status,
              cadtech_notif.Budget
            FROM cadtech_notif 
            INNER JOIN user 
              ON cadtech_notif.CadTech_ID = user.ID
            WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"]."";
            $cadtechresult = mysqli_query($conn,$cadtechsql);
            if(mysqli_num_rows($cadtechresult) > 0){
              while($cadtechrows = mysqli_fetch_assoc($cadtechresult))
              {
                if($cadtechrows["Status"] == "CAD Rejected")
                {
                  $rejected_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                  echo "
                      <td> ".$rows["Designer"]." </td>
                      <td class='emphasize'> ".$rows["Deadline"]." </td>
                      <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                      <td> ".$rows["Check_Item"]." </td>
                      <td> ".$rows["Ticket_Number"]." </td>";
                  echo "<td>
                          <label id='assigned'>";
                  echo " 
                    <span style='margin-left:16px;'>".$cadtechrows["Username"]." ( ".$cadtechrows["Budget"]." )<br /></span><br />";   
                   echo "</label>
                     <script>
                       $('#rejected_count').html('($rejected_count)');
                     </script>
                   </td>";
                   echo" <td>
                          <input 
                          type='button' 
                          value='C' 
                          class='btn-no bg-red white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='D' 
                          class='btn-no bg-gray white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='R' 
                          class='btn-no bg-gray white emphasize' 
                          />
                        </td>  
                  </tr>";
              } 
                if($cadtechrows["Status"] == "Designer CAD Rejected")
                {
                  $rejected_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                  echo "
                      <td> ".$rows["Designer"]." </td>
                      <td class='emphasize'> ".$rows["Deadline"]." </td>
                      <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                      <td> ".$rows["Check_Item"]." </td>
                      <td> ".$rows["Ticket_Number"]." </td>";
                  echo "<td>
                          <label id='assigned'>";
                  echo " 
                    <span style='margin-left:16px;'>".$cadtechrows["Username"]." ( ".$cadtechrows["Budget"]." )<br /></span><br />";   
                   echo "</label>
                     <script>
                       $('#rejected_count').html('($rejected_count)');
                     </script>
                   </td>";
                   echo" <td>
                          <input 
                          type='button' 
                          value='C' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='D' 
                          class='btn-no bg-red white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='R' 
                          class='btn-no bg-gray white emphasize' 
                          />
                        </td>  
                  </tr>";
              } 
                if($cadtechrows["Status"] == "Reviewer CAD Rejected")
                {
                  $rejected_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                  echo "
                      <td> ".$rows["Designer"]." </td>
                      <td class='emphasize'> ".$rows["Deadline"]." </td>
                      <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                      <td> ".$rows["Check_Item"]." </td>
                      <td> ".$rows["Ticket_Number"]." </td>";
                  echo "<td>
                          <label id='assigned'>";
                  echo " 
                    <span style='margin-left:16px;'>".$cadtechrows["Username"]." ( ".$cadtechrows["Budget"]." )<br /></span><br />";   
                   echo "</label>
                     <script>
                       $('#rejected_count').html('($rejected_count)');
                     </script>
                   </td>";
                   echo" <td>
                          <input 
                          type='button' 
                          value='C' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='D' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='R' 
                          class='btn-no bg-red white emphasize' 
                          />
                        </td>  
                  </tr>";
              } 
              }
            }
          }
          break;
          case "CADAccepted":
            if($rows["Status"] == "Approved")
            {
            $cadtechsql 
            = "SELECT 
              user.Username, 
              user.ID AS CADTech_ID, 
              cadtech_notif.Status,
              cadtech_notif.Budget
            FROM cadtech_notif 
            INNER JOIN user 
              ON cadtech_notif.CadTech_ID = user.ID
              WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"]."";
            $cadtechresult = mysqli_query($conn,$cadtechsql);
            if(mysqli_num_rows($cadtechresult) > 0){
              while($cadtechrow = mysqli_fetch_assoc($cadtechresult))
              {
                switch($cadtechrow["Status"]) 
                { 
                  case "CAD Accepted":
                    $approved_count ++;
                    echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                    echo "
                        <td> ".$rows["Designer"]." </td>
                        <td class='emphasize'> ".$rows["Deadline"]." </td>
                        <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                        <td> ".$rows["Check_Item"]." </td>
                        <td> ".$rows["Ticket_Number"]." </td>";
                    echo "<td>
                            <label id='assigned'>";
                              echo " 
                                <span style='margin-left:16px;'>".$cadtechrow["Username"]." ( ".$cadtechrow["Budget"]." )<br /></span><br />";   
                    echo "</label>
                               <script>
                                 $('#approved_count').html('($approved_count)');
                               </script>
                             </td>";
                    echo" <td>
                          <input 
                          type='button' 
                          value='C' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='D' 
                          class='btn-no bg-gray white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='R' 
                          class='btn-no bg-gray white emphasize' 
                          />
                        </td>
                        </tr>";  
                  break;
                  case "Designer CAD Accepted":
                  $approved_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                  echo "<td> ".$rows["Designer"]." </td>
                        <td class='emphasize'> ".$rows["Deadline"]." </td>
                        <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                        <td> ".$rows["Check_Item"]." </td>
                        <td> ".$rows["Ticket_Number"]." </td>";
                  echo "<td>
                          <label id='assigned'>";
                  echo "<span style='margin-left:16px;'>".$cadtechrow["Username"]." ( ".$cadtechrow["Budget"]." )<br /></span><br />"; 
                  echo "</label>
                           <script> 
                              $('#approved_count').html('($approved_count)');
                           </script>
                         </td>";  
                  echo" <td>
                          <input 
                          type='button' 
                          value='C' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='D' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='R' 
                          class='btn-no bg-gray white emphasize' 
                          />
                        </td>
                        </tr>";  
                  break;
                  case "Reviewer CAD Accepted":
                  $approved_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                  echo "<td> ".$rows["Designer"]." </td>
                        <td class='emphasize'> ".$rows["Deadline"]." </td>
                        <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                        <td> ".$rows["Check_Item"]." </td>
                        <td> ".$rows["Ticket_Number"]." </td>";
                  echo "<td>
                          <label id='assigned'>";
                  echo "<span style='margin-left:16px;'>".$cadtechrow["Username"]." ( ".$cadtechrow["Budget"]." )<br /></span><br />"; 
                  echo "</label>
                           <script> 
                              $('#approved_count').html('($approved_count)');
                           </script>
                         </td>";  
                  echo" <td>
                          <input 
                          type='button' 
                          value='C' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='D' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='R' 
                          class='btn-no bg-green white emphasize' 
                          />
                        </td>
                        </tr>";  
                  break;
                }
              }
            }
            }
          break;
        } 
			}
		}
  }