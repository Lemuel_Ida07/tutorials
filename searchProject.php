<?php
	include('connect.php');
	$query = $_POST['query'];
	$count = 0;

	if($_POST["for"] == "tickets")
	{
		echo "<thead>
						<tr>
							<th>Ticket Numbers</th>
							<th>Project</th>
						</tr>
					</thead>
					<tbody>";
						$sql = "SELECT  Ticket_Number, 
														CONCAT(Project_Number,' - ',Project_Name) as Project  
										FROM ticket 
										INNER JOIN project 
											ON ticket.Project_ID = project.ID 
										WHERE ticket.Ticket_Number LIKE '%$query%' 
											OR project.Project_Name LIKE '%$query%' 
											OR project.Project_Number LIKE '%$query%'
										ORDER BY project.Project_Number";
								$result = mysqli_query($conn,$sql);
								if(mysqli_num_rows($result) > 0)
								{
									while($rows = mysqli_fetch_assoc($result))
									{
										$count ++;
										echo "<tr>
										<td><input type='text' style='padding:1%;' value='".$rows['Ticket_Number']."' name='editproject_number'/></td>
										<td><input type='text' style='padding:1%;' value='".$rows['Project']."' name='editproject_name'/></td>
										</tr>";
									}
								}
			echo"</tbody>
			<script>
				document.getElementById('result_tickets').innerHTML = '$count rows';
			</script>";
	}
	else if($_POST["for"] == "users")
	{
						$sql = "SELECT 
											user.ID,
											user.Username,
											user.Firstname,
											user.Middlename,
											user.Lastname,
											user.Status,
											user.User_Type,
											team.Team_Name,
											user.Trade 
										FROM user
										RIGHT JOIN team 
											ON user.Team_ID = team.ID 
										WHERE 
											user.Username LIKE '%$query%'
											OR user.Firstname LIKE '%$query%'
											OR user.Middlename LIKE '%$query%'
											OR user.Lastname LIKE '%$query%'
										ORDER BY user.Firstname";
						$result = mysqli_query($conn,$sql);
						if(mysqli_num_rows($result) > 0)
						{
							while($rows = mysqli_fetch_assoc($result))
							{ 
								$count ++;
								echo "
								<tr>
								<td><input type='text' style='padding:1%;' value='".$rows['Username']."' id='editdusername".$rows['ID']."' name='editdusername'/></td>
								<td><input type='text' style='padding:1%;' value='".$rows['Firstname']."' id='editdfname".$rows['ID']."' name='editdfname'/></td>
								<td><input type='text' style='padding:1%;' value='".$rows['Middlename']."' id='editdmname".$rows['ID']."' name='editdmname'/></td>
								<td><input type='text' style='padding:1%;' value='".$rows['Lastname']."' id='editdlname".$rows['ID']."' name='editdlname'/></td>";
								$user_type = "";
								switch($rows["User_Type"])
								{
									case 0:	//Designer
										$user_type="Designer";
										break;
									case 1:	//Reviewer								 
										$user_type="Reviewer";
										break;
									case 2:	//DC								 
										$user_type="DC";
										break;
									case 3:	//PM								 
										$user_type="PM";
										break;
									case 4:	//OPERATIONS					
										$user_type="OPERATIONS";
										break;
									case 5:	//Admin									
										$user_type="Admin";
										break;
									case 8: //CAD Manager								
										$user_type="CAD Manager";
										break;
									case 9: //CAD Technician								
										$user_type="CAD Technician";
										break;
									case 12: //Project Director								
										$user_type="Project Director";
										break;
								}
								//for user_type
								echo "<td><select id='select_user_type".$rows["ID"]."' name='edituser_type' class='margin-0'>";
								$remarkssql = "SELECT DISTINCT user_type.ID,user_type.Type 
															FROM user_type 
															LEFT JOIN user 
																ON user.User_Type = user_type.ID 
															WHERE user_type.ID != '".$rows["User_Type"]."'";
								$remarks_result = mysqli_query($conn,$remarkssql);
								if(mysqli_num_rows($remarks_result) > 0)
								{
									echo "<option value='".$rows["User_Type"]."'> $user_type </option>";
									while($result_row = mysqli_fetch_assoc($remarks_result))
									{
										echo "<option value='".$result_row["ID"]."'> ".$result_row["Type"]." </option>";																					
									}
								}
								echo "</select></td>";
								//for team
								echo "<td><select id='select_team".$rows["ID"]."' name='editteam' class='margin-0'>";
								$remarkssql = "SELECT DISTINCT team.ID,team.Team_Name 
															FROM user 
															RIGHT JOIN team 
																ON user.Team_ID = team.ID 
															WHERE team.ID != '".$rows["Team_ID"]."'";
								$remarks_result = mysqli_query($conn,$remarkssql);
								if(mysqli_num_rows($remarks_result) > 0)
								{
									echo "<option value='".$rows["Team_ID"]."'> ".$rows["Team_Name"]." </option>";
									while($result_row = mysqli_fetch_assoc($remarks_result))
									{
										echo "<option value='".$result_row["ID"]."'> ".$result_row["Team_Name"]." </option>";																					
									}
								}
								echo "</select></td>";
							 //for trade
								echo "<td><select id='select_trade".$rows["ID"]."' name='edittrade' class='margin-0'>";
								$remarkssql = "SELECT T_Code FROM trades WHERE T_Code != '".$rows["Trade"]."' AND T_Code != ''";
								$remarks_result = mysqli_query($conn,$remarkssql);
								if(mysqli_num_rows($remarks_result) > 0)
								{
									echo "<option value='".$rows["Trade"]."'> ".$rows["Trade"]." </option>";
									while($result_row = mysqli_fetch_assoc($remarks_result))
									{
										echo "<option value='".$result_row["T_Code"]."'> ".$result_row["T_Code"]." </option>";																					
									}
								}
								echo "</select></td>";
								//for status
								echo "<td><select id='select_status".$rows["ID"]."' name='editstatus' class='margin-0'>";
									if($rows['Status'] == "Active")
									{								
										echo "<option value='Active'> Active </option>";																					
										echo "<option value='Inactive'  style='color:red;'> Inactive </option>";	
									}
									else
									{
										echo "<script> $('#select_status".$rows["ID"]."').attr('style','color:red; font-weight:bold;'); </script>";
										echo "<option value='Inactive'> Inactive </option>";																					
										echo "<option value='Active' style='color:black;'> Active </option>";	
									}
								echo "</select></td>";

								echo"
								<td><input type='submit'  value='Edit' name='edit_user' class='editbtn' id='editbtn".$rows["ID"]."'/></td>
								<td style='display:none;'><input type='text' value='".$rows['ID']."' name='editid'/></td>
								<script>
									$(document).ready(function(){
										$('#editbtn".$rows["ID"]."').on('click',function(){
											$.ajax(
											{
												url:'opsSubmit.php',
												type:'post',
												data:'edit_user=true'+
													 '&editid=".$rows['ID']."'+
													 '&editdusername='+$('#editdusername".$rows['ID']."').val()+
													 '&editdfname='+$('#editdfname".$rows["ID"]."').val()+
													 '&editdmname='+$('#editdmname".$rows["ID"]."').val()+
													 '&editdlname='+$('#editdlname".$rows["ID"]."').val()+
													 '&edituser_type='+$('#select_user_type".$rows["ID"]."').val()+
													 '&editteam='+$('#select_team".$rows["ID"]."').val()+
													 '&edittrade='+$('#select_trade".$rows["ID"]."').val()+
													 '&editstatus='+$('#select_status".$rows["ID"]."').val(),
												success:function(data){
													alert(data);
												},
												error:function(data){
													alert(data);
												}
											});
										});
									});	
								</script></tr>";
							}
						}
				echo"<tr>
							<td colspan='9' style='text-align:center;'>
								No more results.
								<script>
									document.getElementById('result_users').innerHTML = '$count rows';
								</script>
							</td>
						</tr>";
	}
	else if($_POST["for"] == "users_status" || $_POST["for"] == "users_trade" || $_POST["for"] == "users_team" || $_POST["for"] == "users_user_type")
	{
						if($query == "All")
						{
							$sql = "SELECT 
											user.ID,
											user.Username,
											user.Firstname,
											user.Middlename,
											user.Lastname,
											user.Status,
											user.User_Type,
											team.Team_Name,
											user.Trade 
										FROM user
										INNER JOIN team 
											ON user.Team_ID = team.ID 
										ORDER BY user.Firstname";
						}
						else
						{
							$sql = "SELECT 
											user.ID,
											user.Username,
											user.Firstname,
											user.Middlename,
											user.Lastname,
											user.Status,
											user.User_Type,
											user.Team_ID,
											team.Team_Name,
											user.Trade 
										FROM user
										RIGHT JOIN team 
											ON user.Team_ID = team.ID 
										WHERE "; 
							switch($_POST["for"])
							{
								case "users_status":
									$sql .="user.Status = '$query'";
									echo "<tr style='display:none;'>
													<td>
														<script> 
															$('#drop_user_type').val('All');
															$('#drop_team').val('All');
															$('#drop_trade').val('All'); 
														</script>
													</td>
												</tr>";
								break;
								case "users_trade":								
									$sql .="user.Trade = '$query'";	
									echo "<tr style='display:none;'>
													<td>
														<script> 
															$('#drop_user_type').val('All');
															$('#drop_team').val('All');
															$('#drop_status').val('All'); 
														</script>
													</td>
												</tr>";
								break;
								case "users_team":								
									$sql .="user.Team_ID = '$query'";	
									echo "<tr style='display:none;'>
													<td>
														<script> 
															$('#drop_user_type').val('All');
															$('#drop_status').val('All');
															$('#drop_trade').val('All'); 
														</script>
													</td>
												</tr>";
								break;
								case "users_user_type":						
									$sql .="user.User_Type = '$query'";
									echo "<tr style='display:none;'>
													<td>
														<script> 
															$('#drop_status').val('All');
															$('#drop_team').val('All');
															$('#drop_trade').val('All'); 
														</script>
													</td>
												</tr>";
								break;
							}		
							$sql .="ORDER BY user.Firstname";
						}
						$result = mysqli_query($conn,$sql);
						if(mysqli_num_rows($result) > 0)
						{
							while($rows = mysqli_fetch_assoc($result))
							{				
								$count ++;
								echo "
								<tr>
								<td><input type='text' style='padding:1%;' value='".$rows['Username']."' id='editdusername".$rows['ID']."' name='editdusername'/></td>
								<td><input type='text' style='padding:1%;' value='".$rows['Firstname']."' id='editdfname".$rows['ID']."' name='editdfname'/></td>
								<td><input type='text' style='padding:1%;' value='".$rows['Middlename']."' id='editdmname".$rows['ID']."' name='editdmname'/></td>
								<td><input type='text' style='padding:1%;' value='".$rows['Lastname']."' id='editdlname".$rows['ID']."' name='editdlname'/></td>";
								$user_type = "";
								switch($rows["User_Type"])
								{
									case 0:	//Designer
										$user_type="Designer";
										break;
									case 1:	//Reviewer								 
										$user_type="Reviewer";
										break;
									case 2:	//DC								 
										$user_type="DC";
										break;
									case 3:	//PM								 
										$user_type="PM";
										break;
									case 4:	//OPERATIONS					
										$user_type="OPERATIONS";
										break;
									case 5:	//Admin									
										$user_type="Admin";
										break;
									case 8: //CAD Manager								
										$user_type="CAD Manager";
										break;
									case 9: //CAD Technician								
										$user_type="CAD Technician";
										break;
									case 12: //Project Director								
										$user_type="Project Director";
										break;
								}
								
								//for user_type
								echo "<td><select id='select_user_type".$rows["ID"]."' name='edituser_type' class='margin-0'>";
								$remarkssql = "SELECT DISTINCT user_type.ID,user_type.Type 
															FROM user_type 
															LEFT JOIN user 
																ON user.User_Type = user_type.ID 
															WHERE user_type.ID != '".$rows["User_Type"]."'";
								$remarks_result = mysqli_query($conn,$remarkssql);
								if(mysqli_num_rows($remarks_result) > 0)
								{
									echo "<option value='".$rows["User_Type"]."'> $user_type </option>";
									while($result_row = mysqli_fetch_assoc($remarks_result))
									{
										echo "<option value='".$result_row["ID"]."'> ".$result_row["Type"]." </option>";																					
									}
								}
								echo "</select></td>";
								//for team
								echo "<td><select id='select_team".$rows["ID"]."' name='editteam' class='margin-0'>";
								$remarkssql = "SELECT DISTINCT team.ID,team.Team_Name 
															FROM user 
															RIGHT JOIN team 
																ON user.Team_ID = team.ID 
															WHERE team.ID != '".$rows["Team_ID"]."'";
								$remarks_result = mysqli_query($conn,$remarkssql);
								if(mysqli_num_rows($remarks_result) > 0)
								{
									echo "<option value='".$rows["Team_ID"]."'> ".$rows["Team_Name"]." </option>";
									while($result_row = mysqli_fetch_assoc($remarks_result))
									{
										echo "<option value='".$result_row["ID"]."'> ".$result_row["Team_Name"]." </option>";																					
									}
								}
								echo "</select></td>";
							 //for trade
								echo "<td><select id='select_trade".$rows["ID"]."' name='edittrade' class='margin-0'>";
								$remarkssql = "SELECT T_Code FROM trades WHERE T_Code != '".$rows["Trade"]."' AND T_CODE != ''";
								$remarks_result = mysqli_query($conn,$remarkssql);
								if(mysqli_num_rows($remarks_result) > 0)
								{
									echo "<option value='".$rows["Trade"]."'> ".$rows["Trade"]." </option>";
									while($result_row = mysqli_fetch_assoc($remarks_result))
									{
										echo "<option value='".$result_row["T_Code"]."'> ".$result_row["T_Code"]." </option>";																					
									}
								}
								echo "</select></td>";
								//for status
								echo "<td><select id='select_status".$rows["ID"]."' name='editstatus' class='margin-0'>";
									if($rows['Status'] == "Active")
									{								
										echo "<option value='Active'> Active </option>";																					
										echo "<option value='Inactive'  style='color:red;'> Inactive </option>";	
									}
									else
									{
										echo "<script> $('#select_status".$rows["ID"]."').attr('style','color:red; font-weight:bold;'); </script>";
										echo "<option value='Inactive'> Inactive </option>";																					
										echo "<option value='Active' style='color:black;'> Active </option>";	
									}
								echo "</select></td>";

								echo"
								<td><input type='submit'  value='Edit' name='edit_user' class='editbtn' id='editbtn".$rows["ID"]."'/></td>
								<td style='display:none;'><input type='text' value='".$rows['ID']."' name='editid'/></td>
								<script>
									$(document).ready(function(){
										$('#editbtn".$rows["ID"]."').on('click',function(){
											$.ajax(
											{
												url:'opsSubmit.php',
												type:'post',
												data:'edit_user=true'+
													 '&editid=".$rows['ID']."'+
													 '&editdusername='+$('#editdusername".$rows['ID']."').val()+
													 '&editdfname='+$('#editdfname".$rows["ID"]."').val()+
													 '&editdmname='+$('#editdmname".$rows["ID"]."').val()+
													 '&editdlname='+$('#editdlname".$rows["ID"]."').val()+
													 '&edituser_type='+$('#select_user_type".$rows["ID"]."').val()+
													 '&editteam='+$('#select_team".$rows["ID"]."').val()+
													 '&edittrade='+$('#select_trade".$rows["ID"]."').val()+
													 '&editstatus='+$('#select_status".$rows["ID"]."').val(),
												success:function(data){
													alert(data);
												},
												error:function(data){
													alert(data);
												}
											});
										});
									});	
								</script></tr>";
							}
						}
		
				echo"<tr>
						<td colspan='9' style='text-align:center;'>
							No more results.
							<script>
								document.getElementById('result_users').innerHTML = '$count rows';
							</script>
						</td>
					</tr>";
	}
	else
	{
		echo "<thead>
				<tr>
					<th> Project Number </th>
					<th> Project Name </th>
          <th> Amount </th>
					<th> Team </th>
					<th> Remarks </th>
					<th> Actions </th>
				</tr>
			</thead>
			<tbody>";
			$sql = "SELECT 
              project.ID,
              project.Project_Number,
              project.Project_Name, 
              project.Amount,
              project.Team_ID,
              team.Team_Name
					FROM project 
					RIGHT JOIN team
						ON project.Team_ID = team.ID
					WHERE project.Project_Number LIKE '%$query%' 
							OR project.Project_Name LIKE '%$query%'
					ORDER BY project.Project_Number";
			$result = mysqli_query($conn,$sql);
			if(mysqli_num_rows($result) > 0)
			{
				while($rows = mysqli_fetch_assoc($result))
				{
					echo "
					<tr>
          <td class='padding-0' style='display:none;'><input type='text' value='".$rows['ID']."' name='editproject_id'/></td>
					<td class='padding-0'><input type='text' readonly='readonly' style='padding:1%;' value='".$rows['Project_Number']."' name='editproject_number'/></td>
						<td class='padding-0'><input type='text' value='".$rows['Project_Name']."' name='editprojname'/></td>
          <td class='padding-0'><input type='text' id='amount".$rows["ID"]."' value='".$rows['Amount']."' name='editamount'/> </td>";
					//for team
					echo "
					<td>
						<select id='select_team_proj".$rows["ID"]."' name='editteam' class='margin-0'>";
							$teamsql = "SELECT 
											ID,
											Team_Name
										FROM team 
										WHERE team.ID != '".$rows["Team_ID"]."'";
					$team_result = mysqli_query($conn,$teamsql);
					if(mysqli_num_rows($team_result) > 0)
					{
						echo "<option value='".$rows["Team_ID"]."'> ".$rows["Team_Name"]." </option>";
						while($result_row = mysqli_fetch_assoc($team_result))
						{
							echo "<option value='".$result_row["ID"]."'> ".$result_row["Team_Name"]." </option>";																					
						}
					}
					echo"
					<td class='padding-0'>
						<select name='remarks' id='remarks".$rows["ID"]."' class='margin-0'>";
						$sql3 = "SELECT Remarks FROM project WHERE Project_Number = '".$rows['Project_Number']."'";
						$result3 = mysqli_query($conn,$sql3);
						if(mysqli_num_rows($result3) > 0)
						{
							while($rows3 = mysqli_fetch_assoc($result3))
							{
								if($rows3['Remarks'] == "0")
								{
									echo "<option value=".$rows3['Remarks']."> RFI </option>
									<option value='1'> Billable </option>";
								}
								else
								{
									echo "<option value=".$rows3['Remarks']."> Billable </option>
									<option value='0'> RFI </option>";
								}
							}
						}
					echo "</select>
					</td>
					<td style='width: 50px;'><input class='width-90pc' id='editbtn' type='submit' onclick='submitEdit".$rows["ID"]."();' value='Edit' name='edit_project' /></td>
					<script>
						function submitEdit".$rows["ID"]."()
						{
							$.ajax(
							{
								url:'ops.php',
								type:'post',
								data:'edit_project=true'+
									 '&editproject_number=".$rows['Project_Number']."'+
									 '&editteam='+$('#select_team_proj".$rows["ID"]."').val()+
									 '&remarks='+$('#remarks".$rows["ID"]."').val()+
                   '&editamount='+$('#amount".$rows["ID"]."').val(),
								success:function(data)
								{
									alert('Edit Success');
								}
							});
						}
					</script>
					</tr>";
				}
			}
			echo"</tbody>
					<script>
						document.getElementById('result').innerHTML = '$count rows';
					</script>";
	}
?>