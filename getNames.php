<?php
  session_start();
  include("connect.php");

  if(isset($_POST["from_reviewer"]))
  {
    $trade = $_SESSION["Trade"];
    $sql = "SELECT 
              ID,
              CONCAT(Firstname,' ',Middlename,' ',Lastname) AS Name,
              Username
              FROM user
            WHERE 
              Status = 'Active'
              AND User_Type = 0
              AND Trade = '$trade'
            ORDER BY Firstname";
    $result = mysqli_query($conn,$sql);
    if(mysqli_num_rows($result) > 0)
    {
      while($rows = mysqli_fetch_assoc($result))
      {
        echo "<p class='asoption' id='optdes".$rows["ID"]."'> 
                ".$rows["Name"]." 
                <span class='username'>( ".$rows["Username"]." )</span>
              </p>";
        echo "<script>
                $('#optdes".$rows["ID"]."').on('click',function(){
                  $('#sel_des_tbox').val('".$rows["Name"]."');
                  $('#designer_id').val('".$rows["ID"]."');
                  $('#designer_name').val($('#sel_des_tbox').val());
                  $('#sel_des_pop_bg').fadeOut();
                  $('#sel_des_pop').slideUp();
                  $('#pop_dname').html($('#sel_des_tbox').val());
                  refreshUpdate();
                  getChecklist();
                });
              </script>";
      }
    }
  }
  
 if(isset($_POST["from_cadManager"]))
  {
    $trade = $_SESSION["Trade"];
    $sql = "SELECT 
              ID,
              CONCAT(Firstname,' ',Middlename,' ',Lastname) AS Name,
              Username
              FROM user
            WHERE 
              Status = 'Active'
              AND User_Type = 9
            ORDER BY Firstname";
    $result = mysqli_query($conn,$sql);
    if(mysqli_num_rows($result) > 0)
    {
      while($rows = mysqli_fetch_assoc($result))
      {
        echo "<p class='asoption' id='optdes".$rows["ID"]."'> 
                ".$rows["Name"]." 
                <br />
                <span class='username'> ".$rows["Username"]."</span>
              </p>";
        echo "<script>
                $('#optdes".$rows["ID"]."').on('click',function(){
                  $('#select_cad_tbox').val('".$rows["Name"]."');
                  $('#selected_cad_tech_id').val('".$rows["ID"]."');
                  $('#assign_btn').fadeIn();
                  $('#done_btn').fadeIn();
                  $('#cad_tech_container').slideUp();
                });
              </script>";
      }
    }
  }
