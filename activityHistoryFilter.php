<?php
    session_start();
    include('connect.php');
    
    if(isset($_POST["filter_date"]))
    {
        $designer_id = $_SESSION["ID"];
        $from_date = $_POST["from_date"];
        $to_date = $_POST["to_date"];
        
        $sql = "SELECT 
             project.Project_Number,
             project.Project_Name,
             activity_logs.ID,
             activity_logs.Project_ID,
             activity_logs.Ticket_Number,
             activity_logs.Activity,
             activity_logs.Date,
             activity_logs.Time_In,
             activity_logs.Time_Out,
             activity_logs.Duration 
         FROM activity_logs 
         INNER JOIN project 
             ON activity_logs.Project_ID = project.ID 
         WHERE 
             Designer_ID = $designer_id 
             AND Date >= '$from_date'
             AND Date <= '$to_date'
          ORDER BY Date DESC";
                
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result) > 0)
        {
           while($rows = mysqli_fetch_assoc($result))
            {
            echo "
                    <tr>
                    <td style='display:none'><input type='text' value='". $rows["ID"] ."' name='editID'></td>
                    <td>". $rows["Project_Number"] ." - ". $rows["Project_Name"] ."</td>
                    <td>". $rows["Ticket_Number"] ."</td>
                    <td><input type='text' value='". $rows["Activity"] ."' name='editActivity'></td>
                    <td><input type='text' value='". $rows["Date"] ."' name='editDate'></td>
                    <td><input type='time' value='". $rows["Time_In"] ."' name = 'editTimeIn'></td>
                    <td><input type='time' value='". $rows["Time_Out"] ."' name = 'editTimeOut'></td>
                    <td><input type='text' value='". $rows["Duration"] ."' name = 'editDuration' readonly='readonly' /></td>
                    </tr>";
            } 
        }
    }
?>