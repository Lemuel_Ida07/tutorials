<?php
  session_start();
  include("connect.php");
  
  if(isset($_POST["get_design_tracking"]))
  {
    $status = $_POST["status"];
    $curr_time = date("h:i");
		$designer_id = $_SESSION['ID'];
		$dnotif_id = 0;
		$curr_year = date("Y");
		$curr_month = date("m");
		$curr_day = date("d");
    $tradeString = $_SESSION["Trade"];
    
		if($curr_day <= 15)
    {
      $day_limit = 15;																																																																													 
    }
    
    if($curr_day > 15)
		{
      $day_limit = 31; 
    }
    
    /*$sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        designer_notif.Budget,
        designer_notif.Deadline,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status,
        CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID
      INNER JOIN requirements_for_des
        ON designer_notif.ID = requirements_for_des.DNotif_ID
      INNER JOIN review_checklist 
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID 
      INNER JOIN user 
        ON user.ID = designer_notif.Designer_ID 
      WHERE 
        EXTRACT(YEAR FROM(designer_notif.Deadline)) <= $curr_year 
        AND EXTRACT(MONTH FROM(designer_notif.Deadline)) <= $curr_month 
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) <= $day_limit 
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) >= EXTRACT(DAY FROM(CURDATE()))
        AND requirements_for_des.Budget > 0
        AND review_checklist.Trade = '$tradeString'
      ORDER BY requirements_for_des.Status DESC,
        designer_notif.ID ASC";*/
    $sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        designer_notif.Budget,
        designer_notif.Deadline,
        designer_notif.Designer_ID,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status,
        CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID
      INNER JOIN requirements_for_des
        ON designer_notif.ID = requirements_for_des.DNotif_ID
      INNER JOIN review_checklist 
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID 
      INNER JOIN user 
        ON user.ID = designer_notif.Designer_ID 
      WHERE 
        requirements_for_des.Budget > 0
        AND review_checklist.Trade = '$tradeString'
      ORDER BY requirements_for_des.Status DESC,
        designer_notif.ID ASC";
    
		$result = mysqli_query($conn,$sql);
		if(isset($_GET["dnotif_id"]))
		{
			$dnotif_id = $_GET["dnotif_id"];
		}
    
		if(mysqli_num_rows($result) > 0)
		{
      $requested_count = 0;
      $approved_count = 0;
      $pending_count = 0;
      $rejected_count = 0;
			while($rows = mysqli_fetch_assoc($result))
			{
        switch($status)
        {
          case "Requested":
            switch($rows["Status"])
            {
              case "Requested":
              $requested_count ++;
				echo "<tr id='tr".$rows["DNotif_ID"]."'>";
				echo "
              <td> ".$rows["Designer"]." </td>
              <td class='emphasize'> ".$rows["Deadline"]." </td>
              <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
              <td> ".$rows["Check_Item"]." </td>
              <td class='emphasize'> ".$rows["Budget"]." Hours </td>
              <td> ".$rows["Ticket_Number"]." </td>
              <td>";
            echo"<input
                  id='approve".$rows["req_for_des_id"]."'
                  type='submit' 
                  value='Approve' 
                  class='btn-normal bg-green white bold'
                  />";  
            echo"<input 
                  id='reject".$rows["req_for_des_id"]."'
                  type='submit' 
                  value='Reject' 
                  class='btn-normal bg-red white bold'
                  />"; 
            echo 
            "<script>
              $('#requested_count').html('($requested_count)');
              $('#approve".$rows["req_for_des_id"]."').on('click',function(){
                showRating();
                $('#req_for_des_id').val('".$rows["req_for_des_id"]."');
                $('#back_data_project_id').val('".$rows["Project_ID"]."');
                $('#back_data_ticket_number').val('".$rows["Ticket_Number"]."');
                $('#back_data_designer_id').val('".$rows["Designer_ID"]."');
              });
              
              $('#reject".$rows["req_for_des_id"]."').on('click',function(){
                $.ajax({
                  beforeSend:function(data){
                    return confirm('Are you sure you want to Reject?');
                  },
                  url:'reviewSubmit.php',
                  type:'post',
                  data:'reject_requirement=true'+
                       '&rfd_id=".$rows["req_for_des_id"]."',
                  success:function(){
                    location.reload();
                  }
                });  
              });
            </script>"; 
            break;
              case "Pending":
                $pending_count ++;
                echo 
              "<td style='display:none;'><script>
                $('#pending_count').html('($pending_count)');
              </script>"; 
              break;
            }
          break;
          case "Approved":
            if($rows["Status"] == "Approved" || $rows["Status"] == "Assigned"
                || $rows["Status"] == "CAD Requested" || $rows["Status"] == "CAD Accepted"
                || $rows["Status"] == "Designer CAD Accepted" 
                || $rows["Status"] == "Reviewer CAD Accepted")
            {
              $approved_count ++;
				echo "<tr id='tr".$rows["DNotif_ID"]."'>";
				echo "
              <td> ".$rows["Designer"]." </td>
              <td class='emphasize'> ".$rows["Deadline"]." </td>
              <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
              <td> ".$rows["Check_Item"]." </td>
              <td class='emphasize'> ".$rows["Budget"]." Hours </td>
              <td> ".$rows["Ticket_Number"]." </td>
              <td>";
              echo "<script>
                      $('#approved_count').html('($approved_count)');
                    </script>";
              echo "<input type='button'
                       value='Approved'
                       class='btn-no bd-white green emphasize />'  ";
            }
            break;
          case "Pending":
            if($rows["Status"] == "Pending")
            {
            $pending_count ++;
            echo "<tr id='tr".$rows["DNotif_ID"]."'>";
            echo "<td style='display:none;'><script>
                    $('#pending_count').html('($pending_count)');
                  </script></td>";
            echo "
                  <td> ".$rows["Designer"]." </td>
                  <td class='emphasize'> ".$rows["Deadline"]." </td>
                  <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                  <td> ".$rows["Check_Item"]." </td>
                  <td class='emphasize'> ".$rows["Budget"]." Hours </td>
                  <td> ".$rows["Ticket_Number"]." </td>
                  <td>";
              echo "<input type='button'
                       value='".$rows["Status"]."'
                       class='btn-no bd-white blue emphasize />'  ";
            }
            break;
          case "Rejected":
            if($rows["Status"] == "Rejected")
            {
            $rejected_count ++;
            echo "<tr id='tr".$rows["DNotif_ID"]."'>";
            echo "<td style='display:none;'><script>
                    $('#rejected_count').html('($rejected_count)');
                  </script></td>";
            echo "
                  <td> ".$rows["Designer"]." </td>
                  <td class='emphasize'> ".$rows["Deadline"]." </td>
                  <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                  <td> ".$rows["Check_Item"]." </td>
                  <td class='emphasize'> ".$rows["Budget"]." Hours </td>
                  <td> ".$rows["Ticket_Number"]." </td>
                  <td>";
              echo "<input type='button'
                       value='".$rows["Status"]."'
                       class='btn-no bd-white red emphasize />'  ";
            }
          break;
        }
        echo" </td>
            </tr>";
			}
		}
  }
  
  
  if(isset($_POST["get_cad_tracking"]))
  {
    $status = $_POST["status"];
    
    $curr_time = date("h:i");
		$cadmngr_id = $_SESSION['ID'];
		$dnotif_id = 0;
		$curr_year = date("Y");
		$curr_month = date("m");
		$curr_day = date("d");
    $tradeString = $_SESSION["Trade"];
		
    if($curr_day <= 15)
    {
      $day_limit = 15;																																																																													 
    }
    
    if($curr_day > 15)
		{
      $day_limit = 31; 
    }
    
    /*$sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        designer_notif.Deadline,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status,
        CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer,
        user.Trade,
        user.ID as CADTech_ID
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID
      INNER JOIN requirements_for_des
        ON designer_notif.ID = requirements_for_des.DNotif_ID
      INNER JOIN review_checklist
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID
      INNER JOIN user 
        ON user.ID = designer_notif.Designer_ID
      WHERE 
        EXTRACT(YEAR FROM(designer_notif.Deadline)) <= $curr_year
        AND EXTRACT(MONTH FROM(designer_notif.Deadline)) <= $curr_month
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) <= $day_limit
        AND EXTRACT(DAY FROM(designer_notif.Deadline)) >= EXTRACT(DAY FROM(CURDATE()))
        AND internal_deadline.Trade = '$tradeString'
        AND requirements_for_des.Budget > 0
      ORDER BY designer_notif.Deadline";*/
    $sql = "SELECT project.ID AS Project_ID,
        project.Project_Number,
        project.Project_Name,
        internal_deadline.Ticket_Number,
        internal_deadline.Phase,
        designer_notif.ID AS DNotif_ID,
        designer_notif.Deadline,
        review_checklist.Check_Item,
        requirements_for_des.ID AS req_for_des_id,
        requirements_for_des.Status,
        CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer,
        user.Trade,
        user.ID as CADTech_ID
      FROM designer_notif 
      INNER JOIN project 
        ON designer_notif.Project_ID = project.ID
      INNER JOIN internal_deadline 
        ON designer_notif.Internaldl_ID = internal_deadline.ID 
      INNER JOIN requirements_for_des 
        ON designer_notif.ID = requirements_for_des.DNotif_ID 
      INNER JOIN review_checklist 
        ON review_checklist.ID = requirements_for_des.ReviewChecklist_ID 
      INNER JOIN user 
        ON user.ID = designer_notif.Designer_ID
      WHERE 
        requirements_for_des.Budget > 0
        AND review_checklist.Trade = '$tradeString'
      ORDER BY designer_notif.Deadline";
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
      $standby_count = 0;
      $assigned_count = 0;
      $requested_count = 0;
      $approved_count = 0;
      $rejected_count = 0;
      $accepted_count = 0;
			while($rows = mysqli_fetch_assoc($result))
			{
        switch($status){
          case "Approved":
            if($rows["Status"] == "Approved")
            { 
              $cadtechsql 
              = "SELECT 
                  user.Username,
                  user.ID AS CADTech_ID,
                  cadtech_notif.Status
                FROM cadtech_notif 
                INNER JOIN user 
                  ON cadtech_notif.CadTech_ID = user.ID
                WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"]."";
              $cadtechresult = mysqli_query($conn,$cadtechsql);
              if(mysqli_num_rows($cadtechresult) > 0){
                while($cadtechrow = mysqli_fetch_assoc($cadtechresult)){
                  switch($cadtechrow["Status"])
                  {
                    case "Standby":
                      $assigned_count ++;
                      echo "<tr style='display:none'>
                              <td>
                                <script>
                                  $('#assigned_count').html('($assigned_count)');
                                </script>
                              </td>
                            </tr>";
                      break;
                    case "Working":
                      $assigned_count ++;
                      echo "<tr style='display:none'>
                              <td>
                                <script>
                                  $('#assigned_count').html('($assigned_count)');
                                </script>
                              </td>
                            </tr>";
                      break;
                    case "CAD Requested":
                      $requested_count ++;
                      echo "<tr style='display:none'>
                              <td>
                                <script>
                                  $('#requested_count').html('($requested_count)');
                                </script>
                              </td>
                            </tr>";
                      break;
                    case "CAD Rejected":
                      $rejected_count ++;
                      echo "<tr style='display:none'>
                              <td>
                                <script>
                                  $('#rejected_count').html('($rejected_count)');
                                </script>
                              </td>
                            </tr>";
                      break;
                    case "CAD Accepted":
                      $accepted_count ++;
                      echo "<tr style='display:none'>
                              <td>
                                <script>
                                  $('#accepted_count').html('($accepted_count)');
                                </script>
                              </td>
                            </tr>";
                      break;
                    case "Designer CAD Accepted":
                      $accepted_count ++;
                      echo "<tr style='display:none'>
                              <td>
                                <script>
                                  $('#accepted_count').html('($accepted_count)');
                                </script>
                              </td>
                            </tr>";
                      break;
                  }
                }
              }
            else
            {
              $standby_count ++;
              echo "<tr id='tr".$rows["DNotif_ID"]."'>";
              echo "
                <td> ".$rows["Designer"]." </td>
                <td class='emphasize'> ".$rows["Deadline"]." </td>
                <td> ".$rows["Project_Number"]." - 
                     ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                <td> ".$rows["Check_Item"]." </td>
                <td class='red emphasize'> N/A</td>
                <td> ".$rows["Ticket_Number"]." </td>";
              echo "<td style='display:none;'>
                      <script>
                        $('#standby_count').html('($standby_count)');
                      </script>
                    </td>";
              echo"
                <td><input
                type='button' 
                value='Standby' 
                class='btn-no bg-white red emphasize'
                /></td>
                <td><input
                type='button' 
                value='N/A' 
                class='btn-no bg-white red emphasize'
                /></td>
                ";
            }
            }
        break;
          case "Assigned":
            if($rows["Status"] == "Approved")
            {
        $cadtechsql 
          = "SELECT 
              user.Username, 
              user.ID AS CADTech_ID, 
              cadtech_notif.Status, 
              cadtech_notif.Budget 
            FROM cadtech_notif 
            INNER JOIN user 
              ON cadtech_notif.CadTech_ID = user.ID 
            WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"]."
            ORDER BY cadtech_notif.Status"; 
        $cadtechresult = mysqli_query($conn,$cadtechsql); 
        if(mysqli_num_rows($cadtechresult) > 0){
          while($cadtechrows = mysqli_fetch_assoc($cadtechresult))
          {
            switch($cadtechrows["Status"])
              {
                case "Standby":
                  $assigned_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                  echo "
                    <td> ".$rows["Designer"]." </td>
                    <td class='emphasize'> ".$rows["Deadline"]." </td>
                    <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                    <td> ".$rows["Check_Item"]." </td>
                    <td class='emphasize'> ".$cadtechrows["Budget"]." Hours</td>
                    <td> ".$rows["Ticket_Number"]." </td>";
                  echo"
                      <td><input
                      type='button' 
                      value='Standby' 
                      class='btn-no bg-white gray emphasize'
                      /></td>";  
                  echo "<td>
                          <label id='assigned'>";
                  echo " <span class='bg-gray' style='position:absolute;width:12px; height:12px; border-radius:100%; margin:0px; padding:0px;'> </span> 
                         <span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />";   
                  echo "</label>
                        <script> 
                          $('#assigned_count').html('($assigned_count)');
                        </script>
                      </td>
                      </tr>";
                  break;
                case "Working":
                  $assigned_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                  echo "
                    <td> ".$rows["Designer"]." </td>
                    <td class='emphasize'> ".$rows["Deadline"]." </td>
                    <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                    <td> ".$rows["Check_Item"]." </td>
                    <td class='emphasize'> ".$cadtechrows["Budget"]." Hours </td>
                    <td> ".$rows["Ticket_Number"]." </td>";
                  echo"
                      <td><input
                      type='button' 
                      value='Working' 
                      class='btn-no bg-white yellow emphasize'
                      /></td>";  
                  echo "<td>
                          <label id='assigned'>";
                  echo "<span class='bg-yellow' style='position:absolute;width:12px; height:12px; border-radius:100%; margin:0px; padding:0px;'> </span>
                        <span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />";   
                  echo "</label>
                        <script> 
                          $('#assigned_count').html('($assigned_count)');
                        </script>
                      </td>
                      </tr>";
                  break;
              } 
          } 
        }
            }
          break;
          case "CADRequested":
          if($rows["Status"] == "Approved")
            {
              $cadtechsql 
                = "SELECT 
                    user.Username, 
                    user.ID AS CADTech_ID,
                    cadtech_notif.ID,
                    cadtech_notif.Status, 
                    cadtech_notif.Budget 
                  FROM cadtech_notif  
                  INNER JOIN user  
                    ON cadtech_notif.CadTech_ID = user.ID 
                  WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"].""; 
              $cadtechresult = mysqli_query($conn,$cadtechsql); 
              if(mysqli_num_rows($cadtechresult) > 0){ 
                while($cadtechrows = mysqli_fetch_assoc($cadtechresult)) 
                { 
                  switch($cadtechrows["Status"]) 
                    { 
                      case "CAD Requested": 
                        $requested_count ++; 
                        echo "<tr id='tr".$rows["DNotif_ID"]."'>"; 
                        echo " 
                          <td> ".$rows["Designer"]." </td> 
                          <td class='emphasize'> ".$rows["Deadline"]." </td> 
                          <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td> 
                          <td> ".$rows["Check_Item"]." </td> 
                          <td class='emphasize'> ".$cadtechrows["Budget"]." Hours </td> 
                          <td> ".$rows["Ticket_Number"]." </td>"; 
                        echo" 
                            <td>
                            <input 
                            type='button' 
                            value='C' 
                            class='btn-no bg-blue white emphasize' 
                            />
                            <input 
                            type='button' 
                            value='D' 
                            class='btn-no bg-gray white emphasize' 
                            />
                            <input 
                            type='button' 
                            value='R' 
                            class='btn-no bg-gray white emphasize' 
                            /></td>";  
                        echo "<td>
                                <label id='assigned'>";
                        echo "<span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />";   
                        echo "</label>
                              <script> 
                                $('#requested_count').html('($requested_count)');
                              </script>
                            </td>
                            </tr>";
                        break;
                      case "Designer CAD Accepted": 
                        $requested_count ++; 
                        echo "<tr id='tr".$rows["DNotif_ID"]."'>"; 
                        echo " 
                          <td> ".$rows["Designer"]." </td> 
                          <td class='emphasize'> ".$rows["Deadline"]." </td> 
                          <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td> 
                          <td> ".$rows["Check_Item"]." </td> 
                          <td class='emphasize'> ".$cadtechrows["Budget"]." Hours </td> 
                          <td> ".$rows["Ticket_Number"]." </td>"; 
                        echo"<td>
                              <input
                              id='approve".$rows["req_for_des_id"]."'
                              type='submit' 
                              value='Approve' 
                              class='btn-normal bg-green white bold'
                              />";  
                        echo"<input 
                              id='reject".$rows["req_for_des_id"]."'
                              type='submit' 
                              value='Reject' 
                              class='btn-normal bg-red white bold'
                              />"; 
                        echo 
                        "<script>
                          $('#requested_count').html('($requested_count)');
                          $('#approve".$rows["req_for_des_id"]."').on('click',function(){
                            $.ajax({
                              beforeSend:function(){
                                return confirm('Are you sure you want to approve?');
                              },
                              url:'reviewSubmit.php',
                              type:'post',
                              data:'accept_cad=true'+
                                   '&cadtech_notif_id=".$cadtechrows["ID"]."',
                              success:function(data){
                                location.reload();
                              }
                            });
                          });

                          $('#reject".$rows["req_for_des_id"]."').on('click',function(){
                            $.ajax({
                              beforeSend:function(data){
                                return confirm('Are you sure you want to Reject?');
                              },
                              url:'reviewSubmit.php',
                              type:'post',
                              data:'reject_cad=true'+
                                   '&cadtech_notif_id=".$cadtechrows["ID"]."',
                              success:function(data){
                                location.reload();
                              }
                            });  
                          });
                        </script></td>"; 
                        echo "<td>
                                <label id='assigned'>";
                        echo "<span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />";   
                        echo "</label>
                              <script> 
                                $('#requested_count').html('($requested_count)');
                              </script>
                            </td>
                            </tr>";
                        break;
                    } 
                } 
              }
            }
          break;
          case "CADAccepted":
              if($rows["Status"] == "Approved")
            {
              $cadtechsql 
          = "SELECT 
              user.Username, 
              user.ID AS CADTech_ID, 
              cadtech_notif.Status,
              cadtech_notif.Budget
            FROM cadtech_notif 
            INNER JOIN user 
              ON cadtech_notif.CadTech_ID = user.ID 
            WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"].""; 
        $cadtechresult = mysqli_query($conn,$cadtechsql); 
        if(mysqli_num_rows($cadtechresult) > 0){
          while($cadtechrows = mysqli_fetch_assoc($cadtechresult))
          {
            switch($cadtechrows["Status"])
              {
                case "CAD Accepted":
                  $accepted_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                  echo "<td> ".$rows["Designer"]." </td>
                        <td class='emphasize'> ".$rows["Deadline"]." </td>
                        <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                        <td> ".$rows["Check_Item"]." </td>
                        <td class='emphasize'> ".$cadtechrows["Budget"]." Hours </td>
                        <td> ".$rows["Ticket_Number"]." </td>";
                  echo" <td>
                          <input 
                          type='button' 
                          value='C' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='D' 
                          class='btn-no bg-gray white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='R' 
                          class='btn-no bg-gray white emphasize' 
                          />
                        </td>";  
        
                  echo "<td>
                          <label id='assigned'>";
                  
                  echo "<span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />"; 
                  
                  echo "</label>
                           <script> 
                        $('#approved_count').html('($accepted_count)');
                           </script>
                         </td>
                         </tr>";  
                  break;
                case "Designer CAD Accepted":
                  $approved_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                  echo "<td> ".$rows["Designer"]." </td>
                        <td class='emphasize'> ".$rows["Deadline"]." </td>
                        <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                        <td> ".$rows["Check_Item"]." </td>
                        <td class='emphasize'> ".$cadtechrows["Budget"]." Hours </td>
                        <td> ".$rows["Ticket_Number"]." </td>";
                  echo" <td>
                          <input 
                          type='button' 
                          value='C' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='D' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='R' 
                          class='btn-no bg-gray white emphasize' 
                          />
                        </td>";  
                  echo "<td>
                          <label id='assigned'>";
                  echo "<span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />"; 
                  echo "</label>
                           <script> 
                              $('#approved_count').html('($approved_count)');
                           </script>
                         </td>
                        </tr>";  
                  break;
                case "Reviewer CAD Accepted":
                  $approved_count ++;
                  echo "<tr id='tr".$rows["DNotif_ID"]."'>";
                  echo "<td> ".$rows["Designer"]." </td>
                        <td class='emphasize'> ".$rows["Deadline"]." </td>
                        <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                        <td> ".$rows["Check_Item"]." </td>
                        <td class='emphasize'> ".$cadtechrows["Budget"]." Hours </td>
                        <td> ".$rows["Ticket_Number"]." </td>";
                  echo" <td>
                          <input 
                          type='button' 
                          value='C' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='D' 
                          class='btn-no bg-green white emphasize' 
                          />

                          <input 
                          type='button' 
                          value='R' 
                          class='btn-no bg-green white emphasize' 
                          />
                        </td>";  
                  echo "<td>
                          <label id='assigned'>";
                  echo "<span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />"; 
                  echo "</label>
                           <script> 
                              $('#approved_count').html('($approved_count)');
                           </script>
                         </td>
                        </tr>";  
                  break;
              } 
          }
            
        }
            }
          break;
          case "CADRejected":
            if($rows["Status"] == "Approved")
            {
              $cadtechsql 
          = "SELECT 
              user.Username, 
              user.ID AS CADTech_ID, 
              cadtech_notif.Status,
              cadtech_notif.Budget
            FROM cadtech_notif 
            INNER JOIN user 
              ON cadtech_notif.CadTech_ID = user.ID 
            WHERE cadtech_notif.ReqForDes_ID = ".$rows["req_for_des_id"].""; 
        $cadtechresult = mysqli_query($conn,$cadtechsql); 
        if(mysqli_num_rows($cadtechresult) > 0){
          while($cadtechrows = mysqli_fetch_assoc($cadtechresult))
          {
            if($cadtechrows["Status"] == "CAD Rejected")
            {
              $rejected_count ++;
              echo "<tr id='tr".$rows["DNotif_ID"]."'>";
              echo "<td> ".$rows["Designer"]." </td>
                    <td class='emphasize'> ".$rows["Deadline"]." </td>
                    <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                    <td> ".$rows["Check_Item"]." </td>
                    <td class='emphasize'> ".$cadtechrows["Budget"]." Hours </td>
                    <td> ".$rows["Ticket_Number"]." </td>";
              echo" <td>
                      <input 
                      type='button' 
                      value='C' 
                      class='btn-no bg-red white emphasize' 
                      />

                      <input 
                      type='button' 
                      value='D' 
                      class='btn-no bg-gray white emphasize' 
                      />

                      <input 
                      type='button' 
                      value='R' 
                      class='btn-no bg-gray white emphasize' 
                      />
                    </td>";  

              echo "<td>
                      <label id='assigned'>";
              echo "<span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />"; 
              echo "</label>
                       <script> 
                    $('#rejected_count').html('($rejected_count)');
                       </script>
                     </td>
                     </tr>";  
            } 
            if($cadtechrows["Status"] == "Designer CAD Rejected")
            {
              $rejected_count ++;
              echo "<tr id='tr".$rows["DNotif_ID"]."'>";
              echo "<td> ".$rows["Designer"]." </td>
                    <td class='emphasize'> ".$rows["Deadline"]." </td>
                    <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                    <td> ".$rows["Check_Item"]." </td>
                    <td class='emphasize'> ".$cadtechrows["Budget"]." Hours </td>
                    <td> ".$rows["Ticket_Number"]." </td>";
              echo" <td>
                      <input 
                      type='button' 
                      value='C' 
                      class='btn-no bg-green white emphasize' 
                      />

                      <input 
                      type='button' 
                      value='D' 
                      class='btn-no bg-red white emphasize' 
                      />

                      <input 
                      type='button' 
                      value='R' 
                      class='btn-no bg-gray white emphasize' 
                      />
                    </td>";  

              echo "<td>
                      <label id='assigned'>";
              echo "<span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />"; 
              echo "</label>
                       <script> 
                    $('#rejected_count').html('($rejected_count)');
                       </script>
                     </td>
                     </tr>";  
            } 
            if($cadtechrows["Status"] == "Reviewer CAD Rejected")
            {
              $rejected_count ++;
              echo "<tr id='tr".$rows["DNotif_ID"]."'>";
              echo "<td> ".$rows["Designer"]." </td>
                    <td class='emphasize'> ".$rows["Deadline"]." </td>
                    <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                    <td> ".$rows["Check_Item"]." </td>
                    <td class='emphasize'> ".$cadtechrows["Budget"]." Hours </td>
                    <td> ".$rows["Ticket_Number"]." </td>";
              echo" <td>
                      <input 
                      type='button' 
                      value='C' 
                      class='btn-no bg-green white emphasize' 
                      />

                      <input 
                      type='button' 
                      value='D' 
                      class='btn-no bg-green white emphasize' 
                      />

                      <input 
                      type='button' 
                      value='R' 
                      class='btn-no bg-red white emphasize' 
                      />
                    </td>";  

              echo "<td>
                      <label id='assigned'>";
              echo "<span style='margin-left:16px;'>".$cadtechrows["Username"]."<br /></span><br />"; 
              echo "</label>
                       <script> 
                    $('#rejected_count').html('($rejected_count)');
                       </script>
                     </td>
                     </tr>";  
            } 
          }
            
        }
            }
          break;
        } 
			}
		}
  }