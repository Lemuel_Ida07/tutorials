<?php
  session_start();
  include("connect.php");
  
  $team_id = $_SESSION["Team_ID"];
  $tradeString = $_SESSION["Trade"];
  //for project dropdown (reviewer.php)
  if(isset($_POST["from_reviewer"]))
  {
    $selected_day = $_POST["selected_day"];
    $selected_month = $_POST["selected_month"];
    $selected_year = $_POST["selected_year"];
    $sql = "SELECT 
				project.ID,
				project.Project_Number,
				project.Project_Name,
				internal_deadline.ID AS internaldl_id,
				external_deadline.Phase,
        internal_deadline.Internal_Deadline
			FROM project
			INNER JOIN external_deadline 
				ON project.ID = external_deadline.Project_ID
			INNER JOIN internal_deadline 
				ON external_deadline.ID = internal_deadline.Externaldl_ID
			WHERE 
				internal_deadline.Trade = '$tradeString' 
				AND internal_deadline.Day_ID >= $selected_day
				AND internal_deadline.Month_ID = $selected_month
				AND internal_deadline.Year_ID = $selected_year";
    $result = mysqli_query($conn,$sql);
    if(mysqli_num_rows($result) > 0)
    {
      while($rows = mysqli_fetch_assoc($result))
      {
        
        echo "<p class='proj_option' id='proj_opt".$rows["ID"]."'> 
              ".$rows['Project_Number']." - ".$rows['Project_Name']." 
               - ".$rows['Phase']." 
            </p>";
       
        echo "<script>
                $('#proj_opt".$rows["ID"]."').on('click',function(){
                    
                    $.ajax({
                      url:'rGetSchedule.php',
                      type:'post',
                      data:'from_sel_proj_tbox=true'+
                            '&internaldl_id=".$rows["internaldl_id"]."'+
                            '&designer_id='+$('#designer_id').val(),
                      success:function(data)
                      {
                        $('#checklist_container').html(data);
                      }
                    });
                    
                    $('#sel_proj_tbox').val('".$rows["Project_Number"]
                        ." - ".$rows["Project_Name"]
                        ." - ".$rows["Phase"]."');
                    $('#projectid').val('".$rows["ID"]."');
                    $('#internaldl_id').val('".$rows["internaldl_id"]."');
                    $('#temp_indeadline').val('".$rows["Internal_Deadline"]."');
                    $('#to_date').val($('#from_date').val());
                    $('#budget').val('0');
                    $('#proj_select').slideUp();
                    $('.budget_inp').each(function(){
                      $(this).prop('disabled',false);
                      $(this).css('cursor','text');
                    });
                    $('#assign_designer').css('visibility','hidden');
                    $('#assign_designer').slideDown();
                    $('#edit_designer').slideUp();
                });
              </script>";
      }
    }
  }
  
  //for main_reviewerDeadline.php
  if(isset($_POST["from_reviewer_summary"]))
  {
    $selected_day = $_POST["selected_day"];
    $selected_month = $_POST["selected_month"];
    $selected_year = $_POST["selected_year"];
    $designer_id = $_POST["designer_id"];
    $from_date = $_POST["from_date"];
    $weekday = $_POST["weekday"];
    $sql = "SELECT 
				project.ID,
				project.Project_Number,
				project.Project_Name,
				internal_deadline.ID AS internaldl_id,
				external_deadline.Phase,
        internal_deadline.Internal_Deadline
			FROM project
			INNER JOIN external_deadline 
				ON project.ID = external_deadline.Project_ID
			INNER JOIN internal_deadline 
				ON external_deadline.ID = internal_deadline.Externaldl_ID
			WHERE 
				internal_deadline.Trade = '$tradeString' 
				AND internal_deadline.Day_ID >= $selected_day
				AND internal_deadline.Month_ID = $selected_month
				AND internal_deadline.Year_ID = $selected_year";
    $result = mysqli_query($conn,$sql);
    if(mysqli_num_rows($result) > 0)
    {
      while($rows = mysqli_fetch_assoc($result))
      {
        if($selected_day <= 9)
        {
          $selected_day = (int)("0"+$selected_day);
        }
        if($selected_month <= 9)
        {
          $selected_month = (int)("0"+$selected_month);
        }
       
          echo "<tr>";
          echo "<td class='project text'> ".$rows['Project_Number']." - ".$rows['Project_Name']." 
               - ".$rows['Phase']." </td>";
        $sql2 = "SELECT designer_notif.Budget
                 FROM designer_notif 
                 WHERE designer_notif.Project_ID = ".$rows['ID']."
                   AND designer_notif.Deadline = '$selected_year-$selected_month-$selected_day'";
         $result2 = mysqli_query($conn,$sql2);
          $sunday = ($selected_day - $weekday) + 1;
          $rows2 = mysqli_fetch_assoc($result2);
          for($count = 1;$count<=7;$count++)
          {
            $sql3 = "SELECT SUM(designer_notif.Budget) AS Budget,
                            designer_notif.ID,
                            designer_notif.Designer_ID,
                            designer_notif.Input_Date,
                            designer_notif.Deadline
                 FROM designer_notif 
                 WHERE designer_notif.Project_ID = ".$rows['ID']."
                   AND designer_notif.Deadline = '$selected_year-$selected_month-$sunday'
                   AND designer_notif.Designer_ID = $designer_id";
            $result3 = mysqli_query($conn,$sql3);
            $rows3 = mysqli_fetch_assoc($result3);
            if($count == 1)
            {
              echo "<td class='hours'><input type='text' value='' class='day1' readonly='readonly'/></td>";
            }
            else if($count == 7)
            {
              echo "<td class='hours'><input type='text' value='' class='day7' readonly='readonly'/></td>";
            }
            else if($rows3["Budget"] == '')
            {
              echo "<td class='hours'><input type='text' value='0' readonly='readonly'/></td>";
            }
            else
            {
              echo "<td class='hours'>
                      <input type='text'  id='sched".$rows3["ID"]."' value='".$rows3["Budget"]."' readonly='readonly'/>
                      <script>
                        $('#sched".$rows3["ID"]."').on('click',function(){
                          $.ajax({
                            url:'rGetSchedule.php',
                            type:'post',
                            data:'get_budget_allo=true'+
                                  '&internaldl_id=".$rows["internaldl_id"]."'+
                                  '&designer_id=".$rows3["Designer_ID"]."'+
                                  '&deadline=$selected_year-$selected_month-$sunday',
                            success:function(data)
                            {
                              $('#sel_proj_tbox').val('".$rows["Project_Number"]
                                  ." - ".$rows["Project_Name"]
                                  ." - ".$rows["Phase"]."');
                              $('#projectid').val('".$rows["ID"]."');
                              $('#internaldl_id').val('".$rows["internaldl_id"]."');
                              $('#budget').val('".$rows3["Budget"]."');
                              $('#temp_indeadline').val('".$rows["Internal_Deadline"]."');
                              $('#from_date').val('".$rows3["Input_Date"]."');
                              $('#to_date').val('".$rows3["Deadline"]."');
                              $('#checklist_container').html(data);
                              $('#assign_designer').css('visibility','hidden');
                              $('#assign_designer').slideUp();
                              $('#edit_designer').slideDown();
                            }
                          });
                        });
                      </script>
                    </td>";
            }
            $sunday++;
          }
          echo "</tr>";
        $curr_day++;
      }
    }
  }