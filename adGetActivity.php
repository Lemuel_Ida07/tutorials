<?php
  session_start();
  include('connect.php');
  include('phpGlobal.php');
  include('phpScript.php');
  if(isset($_POST["from_admin"]))
  {
    $designer_id = $_SESSION["ID"];
    $year = date("Y");
    $month = date("m");
    $day = $_POST["day"];
    $today = date("d");
    $weekday_index = $_POST["index"];
    $sql = "select project.Project_Number,
                project.Project_Name,
                activity_logs.ID,
                activity_logs.Project_ID,
                activity_logs.Ticket_Number,
                activity_logs.Activity,
                activity_logs.Date,
                activity_logs.Status,
                TIME_FORMAT(activity_logs.Time_In, '%H:%i') AS Time_In_tp,
                TIME_FORMAT(activity_logs.Time_Out, '%H:%i') AS Time_Out_tp,
                CONCAT(CAST(TIME_FORMAT(activity_logs.Time_In, '%H') AS INTEGER),':',TIME_FORMAT(activity_logs.Time_In, '%i'),' ',TIME_FORMAT(activity_logs.Time_In, '%p')) AS Time_In,
                CONCAT(CAST(TIME_FORMAT(activity_logs.Time_Out, '%H') AS INTEGER),':',TIME_FORMAT(activity_logs.Time_Out, '%i'),' ',TIME_FORMAT(activity_logs.Time_Out, '%p')) AS Time_Out,
                CAST(TIME_FORMAT(activity_logs.Duration,'%H') AS INTEGER) AS Total_Hour,
                TIME_FORMAT(activity_logs.Duration,'%i') AS Total_Minute 
                FROM activity_logs 
                INNER JOIN project ON activity_logs.Project_ID = project.ID 
                where Designer_ID = $designer_id and Date = '$year-$month-$day'";
    $result = mysqli_query($conn,$sql);
    if(mysqli_num_rows($result) > 0)
    {
      while($rows = mysqli_fetch_assoc($result))
      {
        if($day == $today)
        {
          if($rows["Status"] == "Normal")
          {
        echo "<div id='activity_box".$rows["ID"]."' class='activity_box' style='background-color:#B3E5FC;'>
                <div class='ab_head' style='background-color:#81d4fa;'> 
                  <span id='ticket_no".$rows["ID"]."' class='title'> ".$rows["Ticket_Number"]." </span>
                  <img id='ab_deletebtn".$rows["ID"]."' class='ab_icon margin-r-10px' src='img/trash.png'/>
                  <img id='ab_editbtn".$rows["ID"]."' class='ab_icon' src='img/edit.png'/>
                  <img id='ab_cancelbtn".$rows["ID"]."' class='ab_icon' src='img/cancel.png'/>
                  <img id='ab_savebtn".$rows["ID"]."' class='ab_icon' src='img/save.png'/>
                </div>
                <div class='ab_body'> 
                  <label style='color:black;font-size:14px;'> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." </label>
                  <textarea style='background-color:#B3E5FC;font-size:14px;' id='ab_textarea".$rows["ID"]."' class='ab_textarea'>".$rows["Activity"]."</textarea>
                </div>
                <div class='ab_footer'>
                  <span id='ab_in".$rows["ID"]."' class='ab_time'>".$rows["Time_In"]."</span> -
                  <span id='ab_out".$rows["ID"]."' class='ab_time'>".$rows["Time_Out"]."</span>  
                  <span class='total_hours' style='font-size:18px;float:right;'> ".$rows["Total_Hour"].":".$rows["Total_Minute"]."</span>
                </div>
                <script>
                  offEdit".$rows["ID"]."();
                    
                  $('#ab_savebtn".$rows["ID"]."').on('click',function(){
                    $.ajax({
                      beforeSend:function(){
                        return confirm('Are you sure you want to edit?');
                      },
                      url:'adSubmit.php',
                      type:'post',
                      data:'edit_activity=true'+
                           '&editID=".$rows["ID"]."'+
                           '&editActivity='+$('#ab_textarea".$rows["ID"]."').val()+
                           '&editTimeIn='+$('#ab_time_in".$rows["ID"]."').val()+
                           '&editTimeOut='+$('#ab_time_out".$rows["ID"]."').val(),
                      success:function(data){
                        location.reload();
                      }
                    });
                  });

                  $('#ab_editbtn".$rows["ID"]."').on('click',function(){
                      onEdit".$rows["ID"]."();
                  });
                  
                  $('#ab_cancelbtn".$rows["ID"]."').on('click',function(){
                    offEdit".$rows["ID"]."();
                  });
                  
                  $('#ab_deletebtn".$rows["ID"]."').on('click',function(){
                    $.ajax({
                      beforeSend:function(){
                        return confirm('Are you sure you want to delete?');
                      },
                      url:'adSubmit.php',
                      type:'post',
                      data:'delete_activity=true'+
                           '&activity_id=".$rows["ID"]."',
                      success:function(){
                        $('#activity_box".$rows["ID"]."').slideUp();
                      }
                    });
                  });
                  
                  function onEdit".$rows["ID"]."(){
                    $('#ab_editbtn".$rows["ID"]."').fadeOut();
                    $('#ab_cancelbtn".$rows["ID"]."').fadeIn();
                    $('#ab_savebtn".$rows["ID"]."').fadeIn();
                      
                    $('#ab_textarea".$rows["ID"]."').prop('readonly',false);
                    $('#ab_textarea".$rows["ID"]."').css('background-color','white');
                    $('#ab_textarea".$rows["ID"]."').css('border-radius','2px');

                    $('#ab_in".$rows["ID"]."').html(`<input type='time' id='ab_time_in".$rows["ID"]."' value='".$rows["Time_In_tp"]."'/>`);
                    $('#ab_out".$rows["ID"]."').html(`<input type='time' id='ab_time_out".$rows["ID"]."' value='".$rows["Time_Out_tp"]."'/>`);
                  }
                  
                  function offEdit".$rows["ID"]."(){
                    $('#ab_editbtn".$rows["ID"]."').fadeIn();
                    $('#ab_cancelbtn".$rows["ID"]."').fadeOut();
                    $('#ab_savebtn".$rows["ID"]."').fadeOut();
                  
                    $('#ab_textarea".$rows["ID"]."').prop('readonly',true);
                    $('#ab_textarea".$rows["ID"]."').css('dragable',false);
                    $('#ab_textarea".$rows["ID"]."').css('background-color','#B3E5FC');
                    $('#ab_textarea".$rows["ID"]."').css('border-radius','0px');

                    $('#ab_in".$rows["ID"]."').html(`".$rows["Time_In"]."`);
                    $('#ab_out".$rows["ID"]."').html(`".$rows["Time_Out"]."`);
                  }
                  
                </script>
            </div>";
          }
          else if($rows["Status"] == "For Approval"){
            echo "<div id='activity_box".$rows["ID"]."' class='activity_box' style='background-color:#eceff1;'>
                <div class='ab_head' style='background-color:#cfd8dc;'> 
                  <span id='ticket_no".$rows["ID"]."' class='title'> ".$rows["Ticket_Number"]." </span>
                  <img id='ab_deletebtn".$rows["ID"]."' class='ab_icon margin-r-10px' src='img/trash.png'/>
                  <img id='ab_editbtn".$rows["ID"]."' class='ab_icon' src='img/edit.png'/>
                  <img id='ab_cancelbtn".$rows["ID"]."' class='ab_icon' src='img/cancel.png'/>
                  <img id='ab_savebtn".$rows["ID"]."' class='ab_icon' src='img/save.png'/>
                </div>
                <div class='ab_body'> 
                  <label style='color:black;font-size:14px;'> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." </label>
                  <textarea style='background-color:#cfd8dc;font-size:14px;' id='ab_textarea".$rows["ID"]."' class='ab_textarea'>".$rows["Activity"]."</textarea>
                </div>
                <div class='ab_footer'>
                  <span id='ab_in".$rows["ID"]."' class='ab_time'>".$rows["Time_In"]."</span> -
                  <span id='ab_out".$rows["ID"]."' class='ab_time'>".$rows["Time_Out"]."</span>  
                  <span class='total_hours' style='font-size:18px;float:right;'> ".$rows["Total_Hour"].":".$rows["Total_Minute"]."</span>
                </div>
                <script>
                  offEdit".$rows["ID"]."();
                    
                  $('#ab_savebtn".$rows["ID"]."').on('click',function(){
                    $.ajax({
                      beforeSend:function(){
                        return confirm('Are you sure you want to edit?');
                      },
                      url:'adSubmit.php',
                      type:'post',
                      data:'edit_activity=true'+
                           '&editID=".$rows["ID"]."'+
                           '&editActivity='+$('#ab_textarea".$rows["ID"]."').val()+
                           '&editTimeIn='+$('#ab_time_in".$rows["ID"]."').val()+
                           '&editTimeOut='+$('#ab_time_out".$rows["ID"]."').val(),
                      success:function(data){
                        location.reload();
                      }
                    });
                  });

                  $('#ab_editbtn".$rows["ID"]."').on('click',function(){
                      onEdit".$rows["ID"]."();
                  });
                  
                  $('#ab_cancelbtn".$rows["ID"]."').on('click',function(){
                    offEdit".$rows["ID"]."();
                  });
                  
                  $('#ab_deletebtn".$rows["ID"]."').on('click',function(){
                    $.ajax({
                      beforeSend:function(){
                        return confirm('Are you sure you want to delete?');
                      },
                      url:'adSubmit.php',
                      type:'post',
                      data:'delete_activity=true'+
                           '&activity_id=".$rows["ID"]."',
                      success:function(){
                        $('#activity_box".$rows["ID"]."').slideUp();
                      }
                    });
                  });
                  
                  function onEdit".$rows["ID"]."(){
                    $('#ab_editbtn".$rows["ID"]."').fadeOut();
                    $('#ab_cancelbtn".$rows["ID"]."').fadeIn();
                    $('#ab_savebtn".$rows["ID"]."').fadeIn();
                      
                    $('#ab_textarea".$rows["ID"]."').prop('readonly',false);
                    $('#ab_textarea".$rows["ID"]."').css('background-color','white');
                    $('#ab_textarea".$rows["ID"]."').css('border-radius','2px');

                    $('#ab_in".$rows["ID"]."').html(`<input type='time' id='ab_time_in".$rows["ID"]."' value='".$rows["Time_In_tp"]."'/>`);
                    $('#ab_out".$rows["ID"]."').html(`<input type='time' id='ab_time_out".$rows["ID"]."' value='".$rows["Time_Out_tp"]."'/>`);
                  }
                  
                  function offEdit".$rows["ID"]."(){
                    $('#ab_editbtn".$rows["ID"]."').fadeIn();
                    $('#ab_cancelbtn".$rows["ID"]."').fadeOut();
                    $('#ab_savebtn".$rows["ID"]."').fadeOut();
                  
                    $('#ab_textarea".$rows["ID"]."').prop('readonly',true);
                    $('#ab_textarea".$rows["ID"]."').css('dragable',false);
                    $('#ab_textarea".$rows["ID"]."').css('background-color','#eceff1');
                    $('#ab_textarea".$rows["ID"]."').css('border-radius','0px');

                    $('#ab_in".$rows["ID"]."').html(`".$rows["Time_In"]."`);
                    $('#ab_out".$rows["ID"]."').html(`".$rows["Time_Out"]."`);
                  }
                  
                </script>
            </div>";
          }
        }
        else
        {
          if($rows["Status"] == "Normal")
          {
        echo "<div id='activity_box".$rows["ID"]."' class='activity_box' style='background-color:#B3E5FC;'>
                <div class='ab_head' style='background-color:#81d4fa;'> 
                  <span class='title'> ".$rows["Ticket_Number"]." </span>
                </div>
                <div class='ab_body'> 
                  <label style='color:black;font-size:14px;'>  - ".$rows["Project_Name"]." </label>
                  <p style='background-color:#b3e5fc;font-size:14px;' class='ab_textarea'>
                    ".$rows["Activity"]."
                  </p>
                </div>
                <div class='ab_footer'>
                  <span id='ab_time_in' class='ab_time'>".$rows["Time_In"]."</span> -
                  <span id='ab_time_out' class='ab_time'>".$rows["Time_Out"]."</span>  
                  <span class='total_hours' style='font-size:18px;float:right;'> ".$rows["Total_Hour"].":".$rows["Total_Minute"]."</span>
                </div>
            </div>";
          }
          else if($rows["Status"] == "For Approval")
          {
            echo "<div id='activity_box".$rows["ID"]."' class='activity_box' style='background-color:#eceff1;'>
                <div class='ab_head' style='background-color:#cfd8dc;'> 
                  <span class='title'> ".$rows["Ticket_Number"]." </span>
                </div>
                <div class='ab_body'> 
                  <label style='color:black;font-size:14px;'>  - ".$rows["Project_Name"]." </label>
                  <p style='background-color:#eceff1;font-size:14px;' class='ab_textarea'>
                    ".$rows["Activity"]."
                  </p>
                </div>
                <div class='ab_footer'>
                  <span id='ab_time_in' class='ab_time'>".$rows["Time_In"]."</span> -
                  <span id='ab_time_out' class='ab_time'>".$rows["Time_Out"]."</span>  
                  <span class='total_hours' style='font-size:18px;float:right;'> ".$rows["Total_Hour"].":".$rows["Total_Minute"]."</span>
                </div>
            </div>";
          }
        }
        echo "<script>
                $('#activity_box".$rows["ID"]."').slideDown();
              </script>";
      }
    }
  }