<?php
  session_start();
  include("connect.php");
  include("phpScript.php");			
  $designer_id =  $_SESSION["ID"];
  $shortcut_name = $_SESSION["Lastname"].", ".$_SESSION["Firstname"];
  $sql = "";
  $today = date("F j, Y");
  $date = date("Y-m-d");
  $curr_time = 0;
  ?>
<style>
    
  body{
      font-family:"Product Sans",sans-serif;
  }
  
  #navbar tr td#des_tracking{
    background-color: #deebff;
    color:#0747a6;
  }
    
  #navbar tr td#timesheet{
    background-color: #0747a6;
    color:#deebff;
  }
      
  #list{
    height:90%;
    width:80%;
    min-height:300px;
    margin-right:0px;
    resize:none;
  }

  .general_table th{
      padding:2px;
  }
  
  .miniheader{
      background-color:#eef1f2;
      margin:0px;
      position:fixed;
      left:0px;
      top:60px;
      width:100%;
      height:100px;
      border-bottom:solid 2px #ececec;
  }
  
  .container2{
      margin-top:130px;
  }
  
  .tbl-tab-group{
      position:fixed;
      padding-left:25px;
      top:115px;
  }
  
  .tbl-tab{
      font-size:16px;
      font-weight:bold;
      display:inline-block;
      background-color:#f7f9fa;
      border-radius:4px;
      padding:10px 17px;
      cursor:pointer;
      filter:brightness(100%);
  }
  
  .tbl-tab:hover{
      filter:brightness(75%);
  }
  
  .active-tbl-tab{
      border-radius:4px 4px 0px 0px;
      background-color:#f7f9fa;
      border-bottom: solid 2px #f7f9fa;
      border-top:solid 2px #ececec;
      border-left:solid 2px #ececec;
      border-right:solid 2px #ececec;
  }
  
  #shortcut_name{
      font-size:30px;
      font-weight:bold;
      margin-left:30px;
  }
  
  #percent_holder{
      position:fixed;
      right:50px;
      top:70px;
  }
  
  #percent_bar{
      border-radius:4px;
      border:solid 4px #515151;
      width:208px;
      height:22px;
      display:inline-block;
      
  }
  
  #progress{
      background-color:red;
      width:0px;
      height:22px;
  }
  
  #percent_number{
      font-size:50px;
  }
  
  #pending_count{
      
  }
</style>
<div id="list" class="width-99pc">
    <div class="miniheader">
      <label id="shortcut_name"><?php echo $shortcut_name; ?></label>
      <ul class="tbl-tab-group">
          <li class="tbl-tab active-tbl-tab" id="pending_tab"> Pending <span id="pending_count">(0)</span> </li>
          <li class="tbl-tab" id="requested_tab"> Requested <span id="requested_count">(0)</span> </li>
          <li class="tbl-tab" id="rejected_tab"> Rejected <span id="rejected_count">(0)</span> </li>
          <li class="tbl-tab" id="approved_tab"> Approved <span id="approved_count">(0)</span> </li>
      </ul>
      <div id="percent_holder">
        <div id="percent_bar">
          <div id="progress"></div>
        </div>
        <label id="percent_number"> 30% </label>
      </div>
    </div>
    <div class="container2">
        <table class="general_table width-100pc">
            <thead>
                <tr>
                    <th> Deadline </th>
                    <th> Project </th>
                    <th> Requirements </th>
                    <th> Budget </th>
                    <th> Ticket </th>
                </tr>
            </thead>
            <tbody id="weeklySchedule">
             <!-- Data will be sent from pushSchedule.php -->
            </tbody>
        </table>			
    </div>
</div>				
<div id="message">
</div>
<script>
  
    function initialize()
    {								
        hidePop();
        hideSubmitPop();
        updateReport();	
        $('#extra_activity_bg').hide();
    }

    function updateReport()
    {
        $.ajax(
        {
            url:'table.php',
            type:'post',
            data:'submit=true',
            success:function()
            {}
        });
    }

    $(document).ready(function(){
      $('#pending_tab').on('click',function(){
        $(this).prop("class","tbl-tab active-tbl-tab");
        $('#requested_tab').prop("class","tbl-tab");
        $('#approved_tab').prop("class","tbl-tab");
        $('#rejected_tab').prop("class","tbl-tab");
        $.ajax({
          url:'designerGetData.php',
          type:'post',
          data:'get_design_tracking=true'+
               '&status=Pending',
          success:function(data){
            $('#weeklySchedule').html(data);
          }
        });
      });
      
      $('#requested_tab').on('click',function(){
        $(this).prop("class","tbl-tab active-tbl-tab");
        $('#pending_tab').prop("class","tbl-tab");
        $('#approved_tab').prop("class","tbl-tab");
        $('#rejected_tab').prop("class","tbl-tab");
        $.ajax({
          url:'designerGetData.php',
          type:'post',
          data:'get_design_tracking=true'+
               '&status=Requested',
          success:function(data){
            $('#weeklySchedule').html(data);
          }
        });
      });
      
      $('#approved_tab').on('click',function(){
        $(this).prop("class","tbl-tab active-tbl-tab");
        $('#pending_tab').prop("class","tbl-tab");
        $('#requested_tab').prop("class","tbl-tab");
        $('#rejected_tab').prop("class","tbl-tab");
        $.ajax({
          url:'designerGetData.php',
          type:'post',
          data:'get_design_tracking=true'+
               '&status=Approved',
          success:function(data){
            $('#weeklySchedule').html(data);
          }
        });
      });
      
      $('#rejected_tab').on('click',function(){
        $(this).prop("class","tbl-tab active-tbl-tab");
        $('#pending_tab').prop("class","tbl-tab");
        $('#requested_tab').prop("class","tbl-tab");
        $('#approved_tab').prop("class","tbl-tab");
        $.ajax({
          url:'designerGetData.php',
          type:'post',
          data:'get_design_tracking=true'+
               '&status=Rejected',
          success:function(data){
            $('#weeklySchedule').html(data);
          }
        });
      });
    });

    $(document).ready(function(){

      $('#startBtn').on('click',function(){
        $.ajax(
        {
          url:'designerSubmit.php',
          type:'post',
          data:'start=true'+
                '&project_id='+$('#project_idInfo').val()+
                '&ticket_number='+$('#ticketInfo').val()+
                '&activity='+$('#activityInfo').val()+
                '&time_in='+$("#time_inInfo").val()+
                '&dnotif_id='+$('#dnotif_idInfo').val(),							
          success:function(data)
          {
            $('#actualActivity').html(data);	
            clearInfo();
          }
        });
      });

      $('#switchBtn,#extra_switch_btn').on('click',function(){
          showPop();
      });

      $('#popBtnNo').on('click',function(){
          hidePop();
      });

      $('#pop_okBtn').on('click',function(){
        if($('#pop_project_id').val() !== "")
        {
          $.ajax(
          {
            url:'designerSubmit.php',
            type:'post',
            data:'ok_switch=true'+
                '&project_id='+$('#pop_project_id').val()+
                '&ticket_number='+$('#pop_ticket').html()+
                '&activity='+$('#pop_activity').val()+
                '&dnotif_id='+$('#pop_dnotif_id').val()+
                '&time_in='+$("#time_inInfo").val(),
            success:function(){
              $('#extra_activity_bg').hide();
              $('#extra_activity_div').hide();
              clearSwitchPop();	
            }
          });
        }
      });

        $('#pop_cancelBtn').on('click',function(){
            clearSwitchPop();
        });						

        function clearSwitchPop()
        {
            $('#activity_edit_pop').hide();
            $('#activity_edit_pop_bg').hide();	
            $('#pop_project_id').val("");
            $('#pop_project').val("");
            $('#pop_dnotif_id').val("");
            $('#pop_date').html("");
            $('#pop_ticket').html("");
            $('#pop_activity').val("");
        }

        $('#subPop_startReview').on('click',function(){
            $.ajax({
                url:'designerSubmit.php',
                type:'post',
                data:'submitDesign=true'+
                    '&activitylogs_id='+$('#activitylogs_idInfo').val(),
                success:function(){}
            });

            $.ajax({
                url:'getTotalHours.php',
                type:'post',
                data:'get=true',
                success:function(data)
                {
                    $('#total_time').html(data);
                    hideSubmitPop();
                }
            });
        });

        $('#subPop_switch').on('click',function(){
                document.getElementById('switchBtn').click();
                $('#design_submit_pop').hide();
        });

        $('#save_edit').on('click',function(){
            $.ajax({
                url:'designerSubmit.php',
                type:'post',
                data:'edit=true'+
                    '&activity='+$('#pop_activity').val()+	
                    '&time_in='+$('#pop_time_in').val()+	
                    '&time_out='+$('#pop_time_out').val()+
                    '&activitylog_id='+$('#pop_activitylog_id').val(),
                success:function(data){
                        $('#message').html(data);
                }
            });
        });
    });

    function clearInfo()
    {																									 
        $("#dnotif_idInfo").val("");	 
        $("#project_idInfo").val("");
    }	

    function hidePop()
    {											 
        $(".popUpBg").hide();
        $(".popUp").hide();
    }

    function showPop()
    {											 
        $(".popUpBg").show();
        $(".popUp").show();
    }		

    function hideSubmitPop()
    {											 
        $(".popUpBg").hide();
        $(".popUpSubmit").hide();
    }

    function showSubmitPop()
    {											 
        $(".popUpBg").show();
        $(".popUpSubmit").show();
    }		

    $(".searchbox").click(function (e) {
        $(".datalist").show();
        e.stopPropagation();
    });

    $(".datalist").click(function (e) {
        e.stopPropagation();
    });

    $(document).click(function () {
        $(".datalist").hide();
    });

    $('#pop_project').on('click',function(){
        $.ajax(
        {
            url:'designerSubmit.php',
            type:'post',
            data:'getAvailableProj=true',
            success:function(data)
            {
                $('#pop_schedule').html(data);	
            }
        });
    });
    
</script>