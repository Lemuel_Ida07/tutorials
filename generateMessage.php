<?php
/* @var $session_start type */
$session_start = session_start();
include('connect.php');
$current_month = date("m");
$trade = $_SESSION["Trade"];

$curr_year = (int)date("Y");
$curr_month = (int)date("m");
$curr_day = (int)date("d");
if($curr_day <= 15)
{
    $day_limit = 15;																																
}
if(isset($_POST["User_Type"]))
{
	$id = $_SESSION["ID"];
}																																													 
if($curr_day > 15)
{
    $day_limit = 31;
}
switch($_SESSION['User_Type'])
{
    case 0;
        $id = $_SESSION["ID"];
        $readsql = "SELECT 
                        designer_notif.ID,
                        designer_notif.Budget,
                        designer_notif.Deadline,
                        project.Project_Number,
                        project.Project_Name
                    FROM designer_notif
                    INNER JOIN project 
                        ON designer_notif.Project_ID = project.ID
                    WHERE designer_notif.Designer_ID = $id
                        AND EXTRACT(YEAR FROM(designer_notif.Deadline)) <= $curr_year
                        AND EXTRACT(MONTH FROM(designer_notif.Deadline)) <= $curr_month
                        AND EXTRACT(DAY FROM(designer_notif.Deadline)) <= $day_limit
                        AND EXTRACT(DAY FROM(designer_notif.Deadline)) >= EXTRACT(DAY FROM(CURDATE()))";
		$result = mysqli_query($conn,$readsql);
		if(mysqli_num_rows($result) > 0)
		{
                    while($rows = mysqli_fetch_assoc($result))
                    {
                        echo "<tr>
                                <td>
                                    <table id='notif_info' onclick='clickIt()'>
                                        <tr>
                                            <td> <b>New Deadline</b> ( ".$rows['Deadline']." )</td>
                                        </tr>
                                        <tr>
                                            <td> <b>Budget</b> ( ".$rows['Budget']." Hours )</td>
                                        </tr>
                                        <tr>
                                            <td> ".$rows['Project_Number']." - ".$rows['Project_Name']."</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>";
                    }
                    echo "<script>
                            function clickIt()
                            {
                                document.getElementById('menu_item_logo0').click();
                                document.getElementById('notif').click();
                                $('#pop_dnotif_id').val('".$rows["ID"]."');
                            }
                         </script>";
		}
    break;
    case 1:
        $id = $_SESSION["ID"];
        $readsql = "SELECT 
                        notification.ID,
                        notification.Status,
                        project.Project_Number,
                        project.Project_Name,
                        internal_deadline.ID AS Internal_ID,
                        internal_deadline.Internal_Deadline,
                        external_deadline.Phase
                    FROM notification
                    INNER JOIN internal_deadline 
                        ON notification.Internaldl_ID = internal_deadline.ID
                    INNER JOIN external_deadline 
                        ON internal_deadline.Externaldl_ID = external_deadline.ID
                    INNER JOIN project 
                        ON external_deadline.Project_ID = project.ID
                    WHERE 
                        notification.User_ID = $id 
                        AND internal_deadline.Month_ID = $current_month 
                        AND internal_deadline.Trade = '$trade'
                    ORDER BY
                        notification.Status ASC,
                        internal_deadline.Internal_Deadline ASC";
        $result = mysqli_query($conn,$readsql);
        if(mysqli_num_rows($result) > 0)
        {
            while($rows = mysqli_fetch_assoc($result))
            {
                echo "<tr><td>
                        <table id='notif_info'";
                        if($rows["Status"] == 0)
                        {
                                echo "style = 'background-color:#ffecb3;'";
                        }					
                        else
                        {
                                echo "style = 'background-color:#f2f2f2;border-bottom:solid 1px #b7b7b7;'";
                        }
                        echo"class='notif_info".$rows['ID']."' onclick='clickIt(".$rows['ID'].",".$rows['Status'].")'>
                            <tr>
                                <td>New Internal Deadline (".$rows['Internal_Deadline'].")</td>
                            </tr>
                            <tr>
                                <td> ".$rows['Project_Number']." - ".$rows['Project_Name']." - ".$rows['Phase']."</td>
                            </tr>
                        </table>
                    </td></tr>";
            }
            echo "<script>
                    function clickIt(notif_id,status)
                    {
                        document.getElementById('menu_item_logo1').click();
                        document.getElementById('notif').click();
                        notified(notif_id,status);
                    }

                    function notified(notif_id,status)
                    {
                        $.ajax(
                        {
                            beforeSend:function(data){
                                var proceed = false;

                                if(status == 1)
                                proceed = false;

                                if(status == 0)
                                proceed = true;

                                return proceed;
                            },
                            url:'notifSubmit.php',
                            type:'post',
                            data:'notified=true'+
                                                    '&id='+notif_id,
                            success:function(data){
                            }
                        });
                    }
                </script>";
            }
            break;
}
?>