<?php
	session_start();
	include('connect.php');
	
	$inprogress = false;
	$cadtech_id = $_SESSION["ID"];
	$date = date("Y-m-d");				
	$curr_year = date("Y");
	$curr_month = date("m");	
		if($curr_month < 10)
		$curr_month = "0".$curr_month;
	$curr_day = date("d");					
		if($curr_day < 10)
		$curr_day = "0".$curr_day;
		 
	$readsql = "SELECT
									cad_logs.ID,
									cad_logs.CadTech_ID,
									cad_logs.Time_Out,
									cadtech_notif.Status
							FROM
									cad_logs
							INNER JOIN cadtech_notif 
								ON cad_logs.CTNotif_ID = cadtech_notif.ID
							WHERE
									cad_logs.CadTech_ID = $cadtech_id 
									AND cadtech_notif.Deadline >= '$curr_year-$curr_month-$curr_day'";
	$readresult = mysqli_query($conn,$readsql);
	if(mysqli_num_rows($readresult) > 0)
	{
		while($rows = mysqli_fetch_assoc($readresult))
		{
			if($rows["Status"] == "In Progress")
			{
					$updatesql = "UPDATE cad_logs 
													SET Duration = ADDTIME(DATE_FORMAT(CAST(TIMEDIFF(Time_End,Time_In) AS DATETIME),'%T'),DATE_FORMAT(CAST(TIMEDIFF(CURTIME(),Time_Start) AS DATETIME),'%T'))
												WHERE ID = ".$rows["ID"]."";
				if(mysqli_query($conn,$updatesql))
				{
				}
				$inprogress = true;
			}
		}
		$readsql2 = "SELECT
											project.Project_Number,
											project.Project_Name,
											cad_logs.ID,
											cad_logs.Ticket_Number,
											cad_logs.Date,
											cad_logs.Activity,
											cad_logs.Time_In,
											cad_logs.Time_Out,
											cad_logs.Duration,
											cad_logs.CTNotif_ID,
											cad_logs.Time_Start
									FROM
											cad_logs
									INNER JOIN project ON project.ID = cad_logs.Project_ID
									INNER JOIN cadtech_notif ON cad_logs.CTNotif_ID = cadtech_notif.ID
									WHERE
										cad_logs.CadTech_ID = $cadtech_id 				
										AND cadtech_notif.Deadline >= '$curr_year-$curr_month-$curr_day'";
							$result = mysqli_query($conn,$readsql2);
							
							if(mysqli_num_rows($result) > 0)
							{
								echo "<table class='general_table width-100pc'>
											<thead>
												<th> Project </th>
												<th> Ticket No. </th>
												<th> Activity </th>
												<th> Date	</th>
												<th> Time In </th>
												<th> Time Out </th> 
												<th> Time Used </th>
												<th> Status </th>	
												<th> Actions </th>
											</thead>
											<tbody>";
								while($rows = mysqli_fetch_assoc($result))
								{
									$startsql = "SELECT	Status 
																FROM cadtech_notif 
															WHERE ID = ".$rows["CTNotif_ID"]."";
									$resultstart = mysqli_query($conn,$startsql);
									if(mysqli_num_rows($resultstart) > 0)
									{							
										$row = mysqli_fetch_assoc($resultstart);
										if($row["Status"] == "In Progress")
										{
										echo "<tr>";
										echo "	<td class='yellow bold'> ".$rows["Project_Number"]." ".$rows["Project_Name"]."</td>
														<td class='yellow bold'> ".$rows["Ticket_Number"]." </td>
														<td class='yellow bold'> ".$rows["Activity"]." </td>
														<td class='yellow bold'> ".$rows["Date"]." </td> 
														<td class='yellow bold'> ".$rows["Time_In"]." </td>	
														<td class='yellow bold'> ".$rows["Time_Out"]." </td>
														<td class='yellow bold' id='time_used".$rows["ID"]."'> ".$rows["Duration"]." </td>
														<td class='yellow bold'> ".$row["Status"]." </td>
														<td class='yellow bold'>
															<!--<input type='submit' id='editBtn".$rows["ID"]."' onclick='showPop();fillPop".$rows["ID"]."();' class='btn-normal bg-blue white' value='Edit'/>-->
															<!--<input type='submit' class='btn-normal bg-red white margin-l-4px display-none' id='deleteBtn".$rows["ID"]."' value='Delete' />-->
														</td>";	
										}
										else if($row["Status"] == "For Drafting")
										{
										echo "<tr>";
										echo "	<td> ".$rows["Project_Number"]." ".$rows["Project_Name"]."</td>
														<td> ".$rows["Ticket_Number"]." </td>
														<td> ".$rows["Activity"]." </td>
														<td> ".$rows["Date"]." </td> 
														<td> ".$rows["Time_In"]." </td>	
														<td> ".$rows["Time_Out"]." </td>
														<td id='time_used".$rows["ID"]."'> ".$rows["Duration"]." </td>
														<td class='green bold'> ".$row["Status"]." </td>
														<td>
														</td>";	
										}
										else if($row["Status"] == "Done")
										{
											echo "	<td> ".$rows["Project_Number"]." ".$rows["Project_Name"]."</td>
															<td> ".$rows["Ticket_Number"]." </td>
															<td> ".$rows["Activity"]." </td>
															<td> ".$rows["Date"]." </td> 
															<td> ".$rows["Time_In"]." </td>	
															<td> ".$rows["Time_Out"]." </td>
															<td id='time_used".$rows["ID"]."'> ".$rows["Duration"]." </td>
															<td class='blue bold'> ".$row["Status"]." </td>
															<td></td>";
										}
										else if($row["Status"] == "On Review")
										{
											echo "	<td> ".$rows["Project_Number"]." ".$rows["Project_Name"]."</td>
															<td> ".$rows["Ticket_Number"]." </td>
															<td> ".$rows["Activity"]." </td>
															<td> ".$rows["Date"]." </td> 
															<td> ".$rows["Time_In"]." </td>	
															<td> ".$rows["Time_Out"]." </td>
															<td id='time_used".$rows["ID"]."'> ".$rows["Duration"]." </td>
															<td class='green bold'> ".$row["Status"]." </td>
															<td>
																<!--<input type='submit' id='startReviewBtn".$rows["ID"]."' class='btn-normal bg-blue white' value='Start Review' />-->";
															echo"</td>";
										}
										else if($row["Status"] == "Hold")
										{
											echo "	<td> ".$rows["Project_Number"]." ".$rows["Project_Name"]."</td>
															<td> ".$rows["Ticket_Number"]." </td>
															<td> ".$rows["Activity"]." </td>
															<td> ".$rows["Date"]." </td> 
															<td> ".$rows["Time_In"]." </td>	
															<td> ".$rows["Time_Out"]." </td>
															<td id='time_used".$rows["ID"]."'> ".$rows["Duration"]." </td>
															<td class='gray bold'> ".$row["Status"]." </td>
															<td>
																<!--<input type='submit' id='editBtn".$rows["ID"]."' onclick='showPop();fillPop".$rows["ID"]."();' class='btn-normal bg-blue white' value='Edit'/>-->";
															echo"</td>";
										}
										else if($row["Status"] == "Incomplete")
										{
											echo "	<td> ".$rows["Project_Number"]." ".$rows["Project_Name"]."</td>
															<td> ".$rows["Ticket_Number"]." </td>
															<td> ".$rows["Activity"]." </td>
															<td> ".$rows["Date"]." </td> 
															<td> ".$rows["Time_In"]." </td>	
															<td> ".$rows["Time_Out"]." </td>
															<td id='time_used".$rows["ID"]."'> ".$rows["Duration"]." </td>
															<td class='red bold'> ".$row["Status"]." </td>
															<td>";
															if(!$inprogress)
															{
																echo "<button class='btn-normal bg-green white margin-l-4px' id='continueBtn".$rows["ID"]."' value='".$rows["CTNotif_ID"]."' onclick='continueTime".$rows["CTNotif_ID"]."()'> Continue </button>";
															}
															echo"</td>";
										}

										echo "<script>
													function fillPop".$rows["ID"]."()
													{																																									 
														$('#pop_project').html('".$rows["Project_Number"]." ".$rows["Project_Name"]."');
														$('#pop_designer').html('".$_SESSION["Name"]."');
														$('#pop_date').html('".$rows["Date"]."');
														$('#pop_ticket').html('".$rows["Ticket_Number"]."');
														$('#pop_activity').val('".$rows["Activity"]."');	 
														var time_in = '".$rows["Time_In"]."';
														var time_in = time_in.substring(0,5);
														$('#pop_time_in').val(time_in);	 
														var time_out = '".$rows["Time_Out"]."';
														var time_out = time_out.substring(0,5);
														$('#pop_time_out').val(time_out);	
														$('#pop_activitylog_id').val('".$rows["ID"]."'); ;

													}
													function continueTime".$rows["CTNotif_ID"]."()
													{
															$.ajax({
																url:'cadtechSubmit.php',
																type:'post',
																data:'continue=true'+
																		 '&cadtechnotif_id=".$rows["CTNotif_ID"]."'+
																		 '&cadlogs_id=".$rows["ID"]."',
																success:function(data){
														
																},
																error:function(data){
																	 alert(data);
																}
															});
													}

													function startReview".$rows["ID"]."()
													{
														 $.ajax({
																url:'designerSubmit.php',
																type:'post',
																data:'start_review=true'+
																		 '&dnotif_id=".$rows["CTNotif_ID"]."'+
																		 '&activitylogs_id=".$rows["ID"]."',
																success:function(data){
														
																},
																error:function(data){
																	 alert(data);
																}
															});
													}
												</script>
												</tr>";
									}
								}
							 echo "</tbody></table>";
							 }
	}

	$startsql = "SELECT cad_logs.ID,cad_logs.CadTech_ID 
								FROM cad_logs 
							INNER JOIN cadtech_notif 
								ON cad_logs.CTNotif_ID = cadtech_notif.ID
							WHERE cad_logs.CadTech_ID = $cadtech_id
								AND cadtech_notif.Status = 'In Progress'";
	$resultstart = mysqli_query($conn,$startsql);
	if(mysqli_num_rows($resultstart) > 0)
	{
		$row = mysqli_fetch_assoc($resultstart);
		echo "<script> 
						$('#startBtn').hide();
						$('#submitBtn').show();	 
						$('#activitylogs_idInfo').val('".$row["ID"]."'); 
						$('#total_time').val();
					</script>";
	}
	else
	{																									 
		echo "<script>
						if($('#projectInfo').val() == '')
						{
							$('#startBtn').hide(); 
						} 
						else
						{
							$('#startBtn').show(); 
						}
						$('#switchBtn').hide(); 
						$('#submitBtn').hide();
					</script>";
	}

	$total_time = "";
	$time = new DateTime('', new DateTimeZone('Asia/Manila'));
	$time = $time->format('H:i');

	$sql = "SELECT SEC_TO_TIME( SUM( TIME_TO_SEC(Duration) ) ) AS Duration FROM cad_logs WHERE CadTech_ID = '$cadtech_id' and Date = CURDATE()";
    $result = mysqli_query($conn,$sql);
	if(mysqli_num_rows($result) > 0)
	{
		$row = mysqli_fetch_assoc($result);
		if($row["Duration"] == "")
		{		 
	$total_time = "00:00:00";
		}
		else
		{
			$total_time = $row["Duration"];
		}
	}
	else
	{
		$total_time = "00:00:00";
	}
	echo "<script> 
					$('#total_time').html('$total_time');
					$('#time_inInfo').val('$time');
				</script>";

?>