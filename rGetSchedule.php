<?php
  session_start();
  include('connect.php');
  $tradeString = $_SESSION["Trade"];
  if(isset($_POST["get_budget_allo"]))
  {
    
    //form rGetProject.php
    $internaldl_id = $_POST["internaldl_id"];
    $designer_id = $_POST["designer_id"];
    $deadline = $_POST["deadline"];
    $readsql = "SELECT 
                  DISTINCT Tag,
                  (SELECT COUNT(Check_Item)
                    FROM review_checklist t2
                    WHERE Trade = '$tradeString'
                      AND Tag = t1.Tag) AS Tag_Length 
                FROM review_checklist t1
                WHERE 
                  Trade = '$tradeString'
                ORDER BY Tag_Length";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      while($rows = mysqli_fetch_assoc($result))
      {
        echo "<div class='check_box'>
                <label class='title'> ".$rows["Tag"]." </label>
                <hr class='tag'/>";
        $itemsql 
        = "SELECT ID,Check_Item 
            FROM review_checklist 
            WHERE 
              Tag = '".$rows["Tag"]."' 
              AND Trade = '$tradeString'"; 
        $itemresult = mysqli_query($conn,$itemsql); 
        if(mysqli_num_rows($itemresult) > 0)
        {
          echo "<ul>";
          while($itemrows = mysqli_fetch_assoc($itemresult))
          {
            $rfordesql = "SELECT requirements_for_des.ID,
                            requirements_for_des.Budget,
                            designer_notif.ID AS dnotif_id,
                            CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer
                          FROM requirements_for_des
                          INNER JOIN designer_notif
                            ON requirements_for_des.DNotif_ID = designer_notif.ID
                          INNER JOIN user
                            ON designer_notif.Designer_ID = user.ID
                          WHERE requirements_for_des.ReviewChecklist_ID = ".$itemrows["ID"]."
                            AND designer_notif.Internaldl_ID = $internaldl_id
                            AND designer_notif.Designer_ID = $designer_id
                            AND designer_notif.Deadline = '$deadline'";
            $rfordesresult = mysqli_query($conn,$rfordesql);
            if(mysqli_num_rows($rfordesresult) > 0)
            {
              $row = mysqli_fetch_assoc($rfordesresult);
              if($row["Budget"] != 0)
              {
              echo 
              "<li id='".$itemrows["ID"]."' class='text check_item'> 
                <input type='text' class='sched_dnotif_id' style='display:none;' id='sched_dnotif_id".$itemrows["ID"]."' value='".$row["dnotif_id"]."'/>
                <input type='text' 
                       id='check_item".$itemrows["ID"]."'
                       class='budget_inp' 
                       onkeydown='clearValue(this.id)'
                       onkeyup='calculateHours2(this.value,this.id)' 
                       placeholder='0'
                       value='".$row["Budget"]."'
                       style='color:green;font-weight:bold;cursor:text;'
                       maxlength='2'/>
                ".$itemrows["Check_Item"]."
                <span class='green'>(".$row["Designer"].")</span>
               </li>";
              }
            }
            else
            {
              /*
              echo 
              "<li id='".$itemrows["ID"]."' class='text check_item'> 
                <input type='text' 
                  id='check_item".$itemrows["ID"]."'
                  class='budget_inp' 
                  onkeydown='clearValue(this.id)'
                  onkeyup='calculateHours(this.value,this.id)' 
                  placeholder='0'
                  value='0'
                  style='cursor:text;'
                  maxlength='2'/>
                ".$itemrows["Check_Item"]."
              </li>";
               */
            }
          }
          echo "</ul>";
        }
        echo "</div>";
      }
    }
  }
  
  //from sel_proj_tbox
  
  if(isset($_POST["from_sel_proj_tbox"]))
  {
    //form rGetProject.php
    $internaldl_id = $_POST["internaldl_id"];
    $designer_id = $_POST["designer_id"];
    $readsql = "SELECT 
                  DISTINCT Tag,
                  (SELECT COUNT(Check_Item)
                    FROM review_checklist t2
                    WHERE Trade = '$tradeString'
                      AND Tag = t1.Tag) AS Tag_Length 
                FROM review_checklist t1
                WHERE 
                  Trade = '$tradeString'
                ORDER BY Tag_Length";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      while($rows = mysqli_fetch_assoc($result))
      {
        echo "<div class='check_box'>
                <label class='title'> ".$rows["Tag"]." </label>
                <hr class='tag'/>";
        $itemsql 
        = "SELECT ID,Check_Item
            FROM review_checklist
            WHERE 
              Tag = '".$rows["Tag"]."'
              AND Trade = '$tradeString'";
        $itemresult = mysqli_query($conn,$itemsql);
        if(mysqli_num_rows($itemresult) > 0)
        {
          echo "<ul>";
          while($itemrows = mysqli_fetch_assoc($itemresult))
          {
            $rfordesql = "SELECT requirements_for_des.ID,
                            requirements_for_des.Budget,
                            CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Designer
                          FROM requirements_for_des
                          INNER JOIN designer_notif
                            ON requirements_for_des.DNotif_ID = designer_notif.ID
                          INNER JOIN user
                            ON designer_notif.Designer_ID = user.ID
                          WHERE requirements_for_des.ReviewChecklist_ID = ".$itemrows["ID"]."
                            AND designer_notif.Internaldl_ID = $internaldl_id
                            AND designer_notif.Designer_ID = $designer_id";
            $rfordesresult = mysqli_query($conn,$rfordesql);
            if(mysqli_num_rows($rfordesresult) > 0)
            {
              $row = mysqli_fetch_assoc($rfordesresult);
              if($row["Budget"] != 0)
              {
                echo 
                "<li class='text'> 
                  <input type='text' 
                         value='".$row["Budget"]."'
                         style='color:green;font-weight:bold;cursor:text;width:20px;height:20px;padding:4px;'
                         maxlength='2'
                         readonly='readonly'/>
                  ".$itemrows["Check_Item"]."
                  <span class='green'></span>
                 </li>";
              }
              else
              {  
                echo 
                "<li id='".$itemrows["ID"]."' class='text check_item'> 
                  <input type='text' 
                         id='check_item".$itemrows["ID"]."'
                         class='budget_inp' 
                         onkeydown='clearValue(this.id)'
                         onkeyup='calculateHours(this.value,this.id)' 
                         placeholder='0'
                         value='".$row["Budget"]."'
                         style='color:green;font-weight:bold;cursor:text;'
                         maxlength='2'/>
                  ".$itemrows["Check_Item"]."
                  <span class='green'>(".$row["Designer"].")</span>
                 </li>";
              }
            }
            else
            {
              if(isReqForDesAssigned($conn,$itemrows["ID"]))
              {
              }
              else
              {
              echo 
              "<li id='".$itemrows["ID"]."' class='text check_item'> 
                <input type='text' 
                  id='check_item".$itemrows["ID"]."'
                  class='budget_inp' 
                  onkeydown='clearValue(this.id)'
                  onkeyup='calculateHours(this.value,this.id)' 
                  placeholder='0'
                  value='0'
                  style='cursor:text;'
                  maxlength='2'/>
                ".$itemrows["Check_Item"]."
              </li>";
              }
            }
          }
          echo "</ul>";
        }
        echo "</div>";
      }
    }
  }
  
  function isReqForDesAssigned($conn,$rc_id){
    $value = false;
    $sql = "SELECT Budget
            FROM requirements_for_des
            WHERE ReviewChecklist_ID = $rc_id
              AND Budget != 0";
    $result = mysqli_query($conn,$sql);
    if(mysqli_num_rows($result) > 0)
    {
      $value = true;
    }
    else{
      $value = false;
    }
      return $value;
  }