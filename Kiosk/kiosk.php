<?php
  session_start();
  include('../phpScript.php');
  include('../sql.php');
  $user_pic = userPic(getItem("SELECT Picture FROM user WHERE ID = $user_id"));
  $first_name = getItem("SELECT Firstname FROM user WHERE ID = $user_id");
	$middle_name = getItem("SELECT Middlename FROM user WHERE ID = $user_id");
	$last_name = getItem("SELECT Lastname FROM user WHERE ID = $user_id");
  $full_name = $first_name." ".$middle_name." ".$last_name;
  $username = getItem("SELECT Username FROM user WHERE ID = $user_id");
?>
<html>
  <head>
    <title> Activity Monitoring </title>
    <!-- logo -->
    <link rel="icon" href="../img/logoblue.png">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"/>
	<!-- text-style.css -->
    <link type="text/css" rel="stylesheet" href="css/text-style.css"/>
    <!--Import kiosk.css-->
    <link type="text/css" rel="stylesheet" href="css/kiosk.css"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  
  <body>
    <!-- Navbar -->
    <ul id="dropdown1" class="dropdown-content">
      <li><img src="../images/profile_pictures/<?php echo $user_pic; ?>" alt="<?php echo $full_name; ?>" width="150" height="150" class="circle center-block"></li>
      <li><a href="#!" class="blue-text"><?php echo $full_name; ?></a></li>
      <li class="divider"><a href="#!" class="blue-text"><?php echo $full_name; ?></a></li>
      <li><a href="../manageAccount.php" class="blue-text"><i class="material-icons">settings</i>Settings</a></li>
      <li class="divider"><a href="#!" class="blue-text"><?php echo $full_name; ?></a></li>
      <li><a href="../logout.php" class="blue-text"><i class="material-icons">exit_to_app</i>Logout</a></li>
    </ul>
    <nav class="blue darken-4">
      <div class="nav-wrapper">
        <a href="../index.php" class="brand-logo"><img class="responsive-img right-aligned" width="150" src="../img/logo.png" alt="acong logo"/></a>
        <ul class="right hide-on-med-and-down">
          <li><a href="#!" ><i class="large material-icons">notifications</i></a></li>
          <li><a href="#!" class="dropdown-trigger" data-target="dropdown1" ><i class="material-icons">arrow_drop_down</i></a></li>
        </ul>
      </div>
    </nav>
	<div class="container">
		<div class="row">
			<div id="department"> Ticketing </div>
			<div class="links">
				<a href="#"> I.T. </a>
				<a href="#"> Purchasing </a>
				<a href="#"> H.R. </a>
				<a href="#"> Accounting </a>
				<a href="#"> Q.M.D. </a>
			</div>
		</div>
		<div class="row" id="request_container">
			<div class="row">
			<div class="teal-text lighten-2 col s4 bold"> I.T. Job Order Request </div>
			<div class="col s10"></div>
			</div>
			<div class="row">
				<div class="col s1"></div>
				<form method="post" class="col s10">
					<div class="input-field">
					  <textarea id="request_deatails" class="materialize-textarea"></textarea>
					  <label for="request_deatails">Request details :</label>
					</div>
					<div class="file-field input-field">
					  <div class="btn">
						<span>Attach File</span>
						<input type="file" multiple>
					  </div>
					  <div class="file-path-wrapper">
						<input class="file-path validate" type="text" placeholder="Attach supporting document.">
					  </div>
					</div>
				</form>
				<div class="col s1"></div>
			</div>
		</div>
	</div>
    <!--JavaScript at end of body for optimized loading-->
		<script src="../jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/kiosk.js"></script>
  </body>
</html>