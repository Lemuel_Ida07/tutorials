/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
  $('.dropdown-trigger').dropdown();
});

$('#nav_menu').on('click',function(){
    var animationName = 'animated rubberBand';
    var animationend = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
    $('#nav_menu').addClass(animationName).one(animationend,function(){
        $('#nav_menu').removeClass(animationName);
    });    
});
