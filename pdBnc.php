<?php
  session_start();
  include("connect.php");
  include('phpGlobal.php');
  $today = date("F j, Y");
$shortcut_name = $_SESSION["Lastname"].", ".$_SESSION["Firstname"];
$user_id = $_SESSION["ID"];
?>
<style>
  #navbar tr td#cad_tracking{
    background-color: #deebff;
    color:#0747a6;
  }
    
  #navbar tr td#timesheet,#navbar tr td#des_tracking{
    background-color: #0747a6;
    color:#deebff;
  }
  
  #list{
      height:90%;
      width:80%;
      min-height:300px;
      margin-right:0px;
      resize:none;
  }
  
  .general_table th{
      padding:2px;
  }
  
  body{
      font-family:"Product Sans",sans-serif;
  }
    
      
  #list{
    height:90%;
    width:80%;
    min-height:300px;
    margin-right:0px;
    resize:none;
  }

  .general_table th{
      padding:2px;
  }
  
  .miniheader{
      background-color:#eef1f2;
      margin:0px;
      position:fixed;
      left:0px;
      top:60px;
      width:100%;
      height:100px;
      border-bottom:solid 2px #ececec;
  }
  
  .container2{
      margin-top:130px;
  }
  
  .tbl-tab-group{
      position:fixed;
      padding-left:25px;
      top:115px;
  }
  
  .tbl-tab{
      font-size:16px;
      font-weight:bold;
      display:inline-block;
      background-color:#f7f9fa;
      border-radius:4px;
      padding:10px 17px;
      cursor:pointer;
      filter:brightness(100%);
  }
  
  .tbl-tab:hover{
      filter:brightness(75%);
  }
  
  .active-tbl-tab{
      border-radius:4px 4px 0px 0px;
      background-color:#f7f9fa;
      border-bottom: solid 2px #f7f9fa;
      border-top:solid 2px #ececec;
      border-left:solid 2px #ececec;
      border-right:solid 2px #ececec;
  }
  
  #shortcut_name{
      font-size:30px;
      font-weight:bold;
      margin-left:30px;
  }
  
  #percent_holder{
      position:fixed;
      right:50px;
      top:70px;
  }
  
  #percent_bar{
      border-radius:4px;
      border:solid 4px #515151;
      width:208px;
      height:22px;
      display:inline-block;
      
  }
  
  #progress{
      background-color:red;
      width:0px;
      height:22px;
  }
  
  #total_amount{
      font-size:50px;
  }
  
  #pending_count{
      
  }
  #th_endorced_client,#th_submission,#th_invoice,#th_reason,#th_paymenttype{
      display:none;
  }
</style>

<div id="list" class="width-99pc">
    <div class="miniheader">
      <label id="shortcut_name"><?php echo $shortcut_name; ?></label>
      <ul class="tbl-tab-group">
          <li class="tbl-tab active-tbl-tab" id="standby_tab"> Requested <span id="standby_count">(0)</span> </li>
          <li class="tbl-tab" id="processing_tab"> Processing <span id="processing_count">(0)</span> </li>
          <li class="tbl-tab" id="rejected_tab"> Rejected <span id="rejected_count">(0)</span> </li>
          <li class="tbl-tab" id="assigned_tab"> Paid <span id="assigned_count">(0)</span> </li>
      </ul>
      <div id="percent_holder">
        <label id="total_amount"> 0 </label>
      </div>
    </div>
    <div class="container2">
        <table class="general_table width-100pc">
            <thead>
              <tr>
                <th> Project </th>
                <th> % </th>
                <th> Amount </th>
                <th> Team </th>
                <th id="th_endorced_client"> Date Endorced to Client </th>
                <th id="th_submission"> Submission Form No.  </th>
                <th id="th_invoice"> Invoice No. </th>
                <th id="th_acc"> Date Endorced to Accounting </th>
                <th id="th_or"> OR No. </th>
                <th id="th_amountpaid"> Amount Paid </th>
                <th id="th_paymenttype"> Payment Type </th>
                <th id="th_action"> Action </th>
                <th id="th_reason"> Reason </th>
              </tr>
            </thead>
            <tbody id="weeklySchedule">
             <!-- Data will be sent from pdRead.php -->
             <?php
              $sql = "SELECT project.ID,
										project.Project_Name, 
										project.Team_ID, 
										project.Project_Number,
                    project.Amount,
                    external_deadline.Percent,
										external_deadline.Phase,	
										external_deadline.ID AS exid ,
                    external_deadline.External_Deadline,
                    external_deadline.Status,
                    team.Team_Name
									FROM project 
									INNER JOIN external_deadline 
										ON project.ID = external_deadline.Project_ID 
                  INNER JOIN team
                    ON project.Team_ID = team.ID
									WHERE (";  
                  if(isset($_SESSION["team_list_id"]))
                  {
                    $team_list_id=$_SESSION["team_list_id"];
                    $teamlist_count = count($_SESSION["team_list_id"]);
                    for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
                    {
                      $sql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
                    }
                      $sql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
                  }
                  else
                  {
                    $sql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
                  }
              $sql.=")and external_deadline.Status = 'Done'";
              $result = mysqli_query($conn,$sql);
              if(mysqli_num_rows($result) > 0)
              {
                while($rows = mysqli_fetch_assoc($result)){
                  echo "<tr>
                          <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                          <td> ".$rows["Percent"]." </td>
                          <td> &#8369; ".$rows["Amount"]." </td>
                          <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>";
                  echo"</tr>";
                }
              }
             ?>
            </tbody>
        </table>			
    </div>
</div>			

<div id="pop_bnc_bg">
</div>

<div id="pop_bnc">
    <input type="text" id="bnc_id" style="display:none;" />
    <label class="title"> Project: </label>
    <span id="bnc_project">  </span>
    <br /><br />
    <label class="title"> External Deadline: </label>
    <span class="red" id="bnc_external_deadline">  </span>
    <br /><br />
    <label class="title"> Date Endorced to Client: </label>
    <span class="blue" id="bnc_endorced_client"></span>
    <br /><br />
    <label class="title"> OR No. </label>
    <input type="text" id="bnc_or_no" placeholder="OR Number" />
    <label class="title"> Amount Paid </label>
    <input type="text" id="bnc_amount_paid" placeholder="Amount Paid" />
    <label class="title"> Payment Type </label>
    <select id="bnc_payment_type" class="dropdown">
        <option id="Check"> Check </option>
        <option id="Cash"> Cash </option>
        <option id="Online"> Online  </option>
    </select> 
    <label class="title"> Date Endorced to Accounting. </label>
    <input type="date" class="classy-date" id="bnc_endorced_date" value="<?php echo $today_datepicker;?>"/>
    <br /><br />
    <input type="button" class="btn-normal white bg-green width-45pc" value="OK" id="bnc_okbtn"/>
    <input type="button" class="btn-normal white bg-red width-45pc" value="Cancel" id="bnc_cancelbtn" />
</div>
<script>
  //pending
    $('#standby_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#assigned_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $('#processing_tab').prop("class","tbl-tab");
      $.ajax({
        url:'pdRead.php',
        type:'post',
        data:'get_requested_bnc=true',
        success:function(data){
          $('#th_acc').fadeOut();
          $('#th_or').fadeOut();
          $('#th_amountpaid').fadeOut();
          $('#th_paymenttype').fadeOut();
          $('#th_action').fadeOut();
          $('#th_endorced_client').fadeOut();
          $('#th_invoice').fadeOut();
          $('#th_reason').fadeOut();
          $('#weeklySchedule').html(data);
          $('#total_amount').html("&#8369;"+countAmount(2));
          countBnc();
        }
      });
    });
    //parocessing
    $('#processing_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#assigned_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $('#standby_tab').prop("class","tbl-tab");
      $.ajax({
        url:'pdRead.php',
        type:'post',
        data:'pending_bnc=true',
        success:function(data){
          $('#th_acc').fadeOut();
          $('#th_or').fadeOut();
          $('#th_invoice').fadeIn();
          $('#th_endorced_client').fadeIn();
          $('#th_amountpaid').fadeOut();
          $('#th_paymenttype').fadeOut();
          $('#th_action').fadeOut();
          $('#th_reason').fadeOut();
          $('#weeklySchedule').html(data);
          $('#total_amount').html("&#8369;"+countAmount(2));
          countBnc();
        }
      });
    });
    //rejected
    $('#rejected_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#assigned_tab').prop("class","tbl-tab");
      $('#standby_tab').prop("class","tbl-tab");
      $('#processing_tab').prop("class","tbl-tab");
      $.ajax({
        url:'pdRead.php',
        type:'post',
        data:'rejected_bnc=true',
        success:function(data){
          $('#th_acc').fadeOut();
          $('#th_or').fadeOut();
          $('#th_invoice').fadeIn();
          $('#th_endorced_client').fadeIn();
          $('#th_amountpaid').fadeOut();
          $('#th_paymenttype').fadeOut();
          $('#th_action').fadeOut();
          $('#th_reason').fadeIn();
          $('#weeklySchedule').html(data);
          $('#total_amount').html("&#8369;"+countAmount(2));
          countBnc();
        }
      });
    });
    //paid
    $('#assigned_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#standby_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
      $('#processing_tab').prop("class","tbl-tab");
      $.ajax({
        url:'pdRead.php',
        type:'post',
        data:'get_paid_bnc=true',
        success:function(data){
          $('#th_acc').fadeIn();
          $('#th_or').fadeIn();
          $('#th_amountpaid').fadeIn();
          $('#th_paymenttype').fadeIn();
          $('#th_endorced_client').fadeIn();
          $('#th_invoice').fadeIn();
          $('#th_action').fadeOut();
          $('#th_reason').fadeOut();
          $('#weeklySchedule').html(data);
          $('#total_amount').html("&#8369;"+countAmount(8));
          countBnc();
        }
      });
    });
    
    $('#bnc_okbtn').on('click',function(){
  $.ajax({
    beforeSend:function(){
      return confirm('Are you sure you want to save?');
    },
    url:'pdSubmit.php',
    type:'post',
    data:'edit_bnc=true'+
          '&or_number='+$('#bnc_or_no').val()+
          '&endorced_to_accounting='+$('#bnc_endorced_date').val()+
          '&amount_paid='+$('#bnc_amount_paid').val()+
          '&payment_type='+$('#bnc_payment_type').val()+
          '&bnc_id='+$('#bnc_id').val(),
    success:function(){
      hideBncPop();
      clearBncpop();$
      $('#standby_tab').trigger('click');
    },
    error:function(data){
      alert(data);
    }
  });
});
    
    $('#bnc_cancelbtn').on('click',function(){
      hideBncPop();
    });
    
    function showBncPop(){
      $('#pop_bnc_bg').fadeIn();
      $('#pop_bnc').slideDown();
    }
    
    function hideBncPop(){
  $('#pop_bnc_bg').fadeOut();
  $('#pop_bnc').slideUp();
}


function clearBncpop(){
  $('#bnc_or_no').val('');
  $('#bnc_endorced_date').val('');
}

function countBnc(){
  //reuested
  $.ajax({
    url:'pdRead.php',
    type:'post',
    data:'requested_count=true',
    success:function(data){
      $('#standby_count').html('('+data+')');
    }
  });
  //pending
  $.ajax({
    url:'pdRead.php',
    type:'post',
    data:'pending_count=true',
    success:function(data){
      $('#processing_count').html('('+data+')');
    }
  });
  //rejected
  $.ajax({
    url:'pdRead.php',
    type:'post',
    data:'reject_count=true',
    success:function(data){
      $('#rejected_count').html('('+data+')');
    }
  });
  //paid
  $.ajax({
    url:'pdRead.php',
    type:'post',
    data:'paid_count=true',
    success:function(data){
      $('#assigned_count').html('('+data+')');
    }
  });
}

function countAmount(cell){
    var count = 0;
    var total = 0;
    $("#weeklySchedule tr").each(function(){
      var amount =document.getElementById("weeklySchedule").rows[count].cells[cell].innerHTML;
        
        amount = amount.trim();
        amount = amount.replace(/,/g,"");
        amount = parseFloat(amount.substring(1,amount.length));
        total = Number(total + amount);
        count++;
    });
    total = total.toLocaleString('en');
    return total;
  }

</script>
