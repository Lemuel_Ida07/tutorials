
<?php
session_start();
include("connect.php");
$today = date("F j, Y");	
$curr_date = date("Y-m-d");	
$tradeString = $_SESSION['Trade'];
  $shortcut_name = $_SESSION["Lastname"].", ".$_SESSION["Firstname"];
  $reviewer_id = $_SESSION["ID"];
if(isset($_SESSION["User_Type"]))
{
	switch($_SESSION["User_Type"])
	{
		case 0:	//DESIGNER
			header("Location:home.php");
			break;
		case 2:	//DC
			header("Location:dc.php");
			break;
		case 3:	//PM
			header("Location:pm.php");
			break;
		case 4:	//OPERATIONS
			header("Location:ops.php");
			break;
		case 5:	//Admin
			header("Location:admin.php");
			break;
	}
}
else
{
	header("Location:index.php");
}
?>

<html>
<head>
	<title> Activity Monitoring </title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" type="text/css" href="css/calendar.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/reviewerStyle.css" />
	<link rel="icon" href="img/logoblue.png">

	<script src="script.js"></script>
	<script src="scripts/internal_deadline_computation.js"></script>
	<script src="jquery-3.2.1.min.js"></script>
	<script src="scripts/javaScript.js"></script>
	<script src="scripts/validations.js"></script>
	<script src="scripts/dateScript.js"></script>

<style>
    
  #list{
    height:90%;
    width:80%;
    min-height:300px;
    margin-right:0px;
    resize:none;
  }
  
  #navbar tr td#des_tracking{
    background-color: #deebff;
    color:#0747a6;
  }
  
    body{
        font-family:"Product Sans",sans-serif;
    }
  #navbar tr td#des_tracking{
    background-color: #deebff;
    color:#0747a6;
  }
    
  #navbar tr td#timesheet{
    background-color: #0747a6;
    color:#deebff;
  }
      
  #list{
    height:90%;
    width:80%;
    min-height:300px;
    margin-right:0px;
    resize:none;
  }

  .general_table th{
      padding:2px;
  }
  
  .miniheader{
      background-color:#eef1f2;
      margin:0px;
      position:fixed;
      left:0px;
      top:60px;
      width:100%;
      height:100px;
      border-bottom:solid 2px #ececec;
  }
  
  .container2{
      margin-top:130px;
  }
  
  .tbl-tab-group{
      position:fixed;
      padding-left:25px;
      top:115px;
  }
  
  .tbl-tab{
      font-size:16px;
      font-weight:bold;
      display:inline-block;
      background-color:#f7f9fa;
      border-radius:4px;
      padding:10px 17px;
      cursor:pointer;
      filter:brightness(100%);
  }
  
  .tbl-tab:hover{
      filter:brightness(75%);
  }
  
  .active-tbl-tab{
      border-radius:4px 4px 0px 0px;
      background-color:#f7f9fa;
      border-bottom: solid 2px #f7f9fa;
      border-top:solid 2px #ececec;
      border-left:solid 2px #ececec;
      border-right:solid 2px #ececec;
  }
  
  #shortcut_name{
      font-size:30px;
      font-weight:bold;
      margin-left:30px;
  }
  
  #percent_holder{
      position:fixed;
      right:50px;
      top:70px;
  }
  
  #percent_bar{
      border-radius:4px;
      border:solid 4px #515151;
      width:208px;
      height:22px;
      display:inline-block;
      
  }
  
  #progress{
      background-color:red;
      width:0px;
      height:22px;
  }
  
  #percent_number{
      font-size:50px;
  }
  
  #pending_count{
      
  }
  
  #save_rating_btn{
    color:white;
    position:absolute;
    left:10px;
    font-size:21px;
    padding:10px 17px;
  }
  
  #cancel_rating_btn{
    color:white;
    position:absolute;
    right:10px;
    font-size:21px;
    padding:10px 17px;
  }
</style>
</head>
<body onload="updateReport();">
	<?php include('header.php'); ?>
  <div id="mainform">
  <div id="list" class="width-99pc">
    <div class="miniheader">
      <label id="shortcut_name"><?php echo $shortcut_name; ?></label>
      <ul class="tbl-tab-group">
          <li class="tbl-tab active-tbl-tab" id="pending_tab"> Pending <span id="pending_count">(0)</span> </li>
          <li class="tbl-tab" id="requested_tab"> Requested <span id="requested_count">(0)</span> </li>
          <li class="tbl-tab" id="rejected_tab"> Rejected <span id="rejected_count">(0)</span> </li>
          <li class="tbl-tab" id="approved_tab"> Approved <span id="approved_count">(0)</span> </li>
      </ul>
      <div id="percent_holder">
        <div id="percent_bar">
          <div id="progress"></div>
        </div>
        <label id="percent_number"> 30% </label>
      </div>
    </div>
    <div class="container2">
        <table class="general_table width-100pc">
            <thead>
                <tr>
                    <th> Designer </th>
                    <th> Deadline </th>
                    <th> Project </th>
                    <th> Requirements </th>
                    <th> Budget </th>
                    <th> Ticket </th>
                    <th> Status </th>
                </tr>
            </thead>
            <tbody id="weeklySchedule">
             <!-- Data will be sent from pushSchedule.php -->
            </tbody>
        </table>			
    </div>
  </div>		
  </div>
  <div id="rating_bg">
  </div>
  <div id="rating_container">
    <div class="average_container">
      <input type="button" 
             class="btn-normal bg-green"
             id="save_rating_btn"
             value="Save"/>
      <input type="button" 
             class="btn-normal bg-red"
             id="cancel_rating_btn"
             value="Cancel"/>
      <div id="average_star">
        <div id="average_lbl"> 5 </div>
      </div>
    </div>
    <div id="rating_group">
        <div class="rating">
            <img id="tstar1" class="star timeliness-star" alt="star" src="img/small-star-y.png" />
            <img id="tstar2" class="star timeliness-star" alt="star" src="img/small-star-y.png" />
            <img id="tstar3" class="star timeliness-star" alt="star" src="img/small-star-y.png" />
            <img id="tstar4" class="star timeliness-star" alt="star" src="img/small-star-y.png" />
            <img id="tstar5" class="star timeliness-star" alt="star" src="img/small-star-y.png" />
            <br />
            <label class="h2"> Timeliness </label>
        </div>
        <div class="rating">
            <img id="cstar1" class="star completeness-star" alt="star" src="img/small-star-y.png" />
            <img id="cstar2" class="star completeness-star" alt="star" src="img/small-star-y.png" />
            <img id="cstar3" class="star completeness-star" alt="star" src="img/small-star-y.png" />
            <img id="cstar4" class="star completeness-star" alt="star" src="img/small-star-y.png" />
            <img id="cstar5" class="star completeness-star" alt="star" src="img/small-star-y.png" />
            <br />
            <label class="h2"> Completeness </label>
        </div>
        <div class="rating">
            <img id="dastar1" class="star design-accuracy-star" alt="star" src="img/small-star-y.png" />
            <img id="dastar2" class="star design-accuracy-star" alt="star" src="img/small-star-y.png" />
            <img id="dastar3" class="star design-accuracy-star" alt="star" src="img/small-star-y.png" />
            <img id="dastar4" class="star design-accuracy-star" alt="star" src="img/small-star-y.png" />
            <img id="dastar5" class="star design-accuracy-star" alt="star" src="img/small-star-y.png" />
            <br />
            <label class="h2"> Design Accuracy </label>
        </div>
    </div>
  </div>
  <div class="loader_bg">
  </div>
  <div class="loader"></div>
  <div id="background_data">
    <input type="text" id="back_data_timeliness" value="5"/>
    <input type="text" id="back_data_completeness" value="5"/>
    <input type="text" id="back_data_design_accuracy" value="5"/>    
    <input type="text" id="req_for_des_id" />
    <input type="text" id="back_data_reviewer_id" value="<?php echo $reviewer_id; ?>"/>
    <input type="text" id="back_data_project_id" />
    <input type="text" id="back_data_ticket_number" />
    <input type="text" id="back_data_designer_id" />
    <!-- for passGetDateData-->
    <input type="text" id="back_data_getDate_id" />
    <input type="text" id="back_data_getDate_month" />
    <input type="text" id="back_data_getDate_year" />
    <input type="text" id="back_data_getDate_weekday" />
  </div>
<script>
  init();
  function init(){
  $.ajax({
    type:'get',
    url:'pushSchedule.php',
    data:'dnotif_id='+$('#dnotif_idInfo').val()+
         '&for_reviewer=true',
    success: function(data)
    {
      $('#weeklySchedule').html(data);
      $('#percent_number').html(computeProgress()+ "%");
    }
  });
  }
var string_month = ["index","January","February","March","April","May","June","July","August","September","October","November","December"];

var budget = [];
var ReviewChecklist_id = [];
var sched_dnotif_id = [];
$(document).ready(function(){
	$('#review_checklist').hide();
	$('#edit_designer').hide();
	$('#delete_designer').hide();
	$('.assign_activity').hide();
	$('.assign_activity_text').hide();

	$("input[type=number]").on('keydown',function (e) {
		var keyCode = e.which;
		// restrict alpha characters
		if (keyCode == 173 ||keyCode == 61 || keyCode == 59 || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 106 && keyCode <= 111) || (keyCode >= 186 && keyCode <= 222)) {
			e.preventDefault();
		}
	});
	/* hide and Unhide the textarea */
	$('#activity_cbox').on('click',function(){
		if($('#activity_cbox').prop("checked"))
		{
			$('.assign_activity').show();
			$('.assign_activity_text').show();
		}
		else
		{
			$('.assign_activity').hide();
			$('.assign_activity_text').hide();
		}
			$('.assign_activity_text').val("Sheets");
	});
});

function resetAssignPop(){
	$('#review_checklist').hide();
	$('.assign_activity').hide();
	$('.assign_activity_text').hide();
	$('#code').html("");
	$('#budget').val("");
	$('#activity_cbox').prop("checked",false);
	$('#assign_designer').show();
	$('#edit_designer').hide();
	$('#delete_designer').hide();
}

function openPop(){
	document.getElementById('pop_up').style.visibility = "visible";
  $('#pop_up').hide();
  $('#pop_up').fadeToggle();
  $('body').css("overflow","hidden");
  $('#assign_designer').fadeIn();
}

function closePop() {
  $('#pop_up').fadeToggle();
  $('body').css("overflow","scroll");
}

function clearPop()
{															 
  $('#sel_proj_tbox').val("");
}

function passGetDateData(id,month,year,weekday){
  $('#back_data_getDate_id').val(id);
  $('#back_data_getDate_month').val(month);
  $('#back_data_getDate_year').val(year);
  $('#back_data_getDate_weekday').val(weekday);
}

function getDate(id,month,year,weekday)
{
  var tbl_date = id;
  var day_today =  new Date().getDate();
  if(id < 9)
    id = "0"+id;
  if(month < 9)
    month = "0"+month;
	$('#date').html(string_month[month]+" "+id+", "+year);
	$('#from_date').val(year+"-"+month+"-"+id);
    getProjects(year,month,tbl_date);
    getProjectsSummary(year,month,tbl_date,weekday);
    var sunday = (id - weekday)+1;
  $('#summary_head').html("<th class='th_project title'> Project </th>");
      var weekDays = getWeekDays();
      $.each(weekDays,function(index,value){
        if(value == day_today)
        {
          $('#summary_head').html($('#summary_head').html()+"<th class='th_days bold red'> "+value+". </th>");
        }
        else if(value === id || value == day_today)
        {
          $('#summary_head').html($('#summary_head').html()+"<th class='th_days bold blue'> "+value+". </th>");
        }
        else{
          $('#summary_head').html($('#summary_head').html()+"<th class='th_days'> "+value+". </th>");
        }
      });
  passGetDateData(id,month,year,weekday);
}

function getProjects(year,month,day){
  $.ajax({
    url:'rGetProject.php',
    type:'post',
    data:'from_reviewer=true'+
         '&selected_year='+year+
         '&selected_month='+month+
         '&selected_day='+day,
    success:function(data){
      $('#proj_select').html(data);
    }    
  });
}

function getProjectsSummary(year,month,day,weekday){
  $.ajax({
    url:'rGetProject.php',
    type:'post',
    data:'from_reviewer_summary=true'+
         '&selected_year='+year+
         '&selected_month='+month+
         '&selected_day='+day+
         '&designer_id='+$('#designer_id').val()+
         '&weekday='+weekday+
         '&from_date='+$('#from_date').val(),
    success:function(data){
      $('#proj_summary').html(data);
    }    
  });
}

function showList(id) {
	var element = document.getElementById(id).style.display = "block";
}

	function hideList(id) {
	if(document.getElementById(id).style.display == "block")
	var element = document.getElementById(id).style.display = "none";
	}

function filter(inputid,divid) {
	var input, filter, list, count;
	input = document.getElementById(inputid);
	filter = input.value.toUpperCase();
	div = document.getElementById(divid);
	list = div.getElementsByClassName("asoption");
	for(count = 0; count < list.length; count ++)
	{
		if (list[count].innerHTML.toUpperCase().indexOf(filter) > -1)
		{
			list[count].style.display = "";
		} else {
			list[count].style.display = "none";
		}
	}
}
$(document).ready(function(){
	/* Assign Clicked */
	$('#assign_designer').on('click',function(){
		var value = $(this).val();
		/* gets budget in the list and place it into array */
		$('.budget_inp').each(function(e){
			var value = $(this).val();
			budget.push(value);
		});
    
    /* gets budget in the list and place it into array */
		$('.check_item').each(function(e){
			var value = $(this).prop("id");
			ReviewChecklist_id.push(value);
		});

		$.ajax(
		{
			beforeSend:function(){
				var send = false;
        var value = $('#budget').val();
					if(value > 0)
					{
						send = true;
					}
					if(value == 0)
					{
							send = false;
					}
				return send;
			},
			url:'reviewSubmit.php',
			type:'post',
			data:{ 
							assign_designer:value,
	            project_id: $('#projectid').val(),
	            internaldl_id: $('#internaldl_id').val(),
	            designer_id: $('#designer_id').val(),
	            total_budget: $('#budget').val(),
	            deadline: $('#from_date').val(),					
							activity: " ",
							budgets:budget,
              reviewchecklist_id:ReviewChecklist_id
						},
			success:function(data)
			{ 
        alert(data);
        refreshUpdate();
	    },
			error:function(data)
			{
				alert(data);
			}
		});
	});

	/* Edit Clicked */
	$('#edit_designer').on('click',function(){
		/* gets requirements in the table and place it into array */
		$('.simple_table #drawing_sheets tr td:nth-child(1)').each(function(e){
			var value = $(this).html();
			requirements.push(value);
		});
		
		/* gets expected sheets in the table and place it into array */
		$('.simple_table #drawing_sheets tr td:nth-child(2) input[type=number]').each(function(e){
			var value = $(this).val();
			expected_sheets.push(value);
		});
		$.ajax(
		{
			beforeSend:function(){
				return confirm('Are you sure you want to edit?');
			},
			url:'reviewSubmit.php',
			type:'post',
			data:
					{
						edit_designer: true,
						dnotif_id: $('#dnotif_id').val(),
					  budget: $('#budget').val(),
						activity: $('.assign_activity_text').val(),
						requirement:requirements,
						expected:expected_sheets,
						code: $('#code').val()
					},
			success:function(data){
				refreshUpdate();
			},
			error:function(data)
			{
				alert(data);
			}
		});
	});

	/* Delete Clicked */
	$('#delete_designer').on('click',function(){
		$.ajax(
		{
			beforeSend:function(){
				return confirm('Are you sure you want to delete?');
			},
			url:'reviewSubmit.php',
			type:'post',
			data:'delete_designer=true'+
					 '&dnotif_id='+$('#dnotif_id').val(),
			success:function(data){
				refreshUpdate();
			},
		});
	});
});

function refreshUpdate(){
	var date  = new Date();
				var selected_day = date.getDate();
				var selected_month = date.getMonth() + 1;
				var selected_year = date.getFullYear();

				/* Refresh calendar */
				$.ajax(
				{
					url:'main_reviewerDeadline.php',
					type:'post',
					data:'month_selected='+selected_month+
							 '&year_selected='+selected_year+
							 '&tradeString='+$('#tradeString').val()+	
							 '&NoLastDays='+$('#NoLastDays').val()+
							 '&designer_id='+$('#designer_id').val(),
					success:function(data)
					{
						$('#calendar').html(data);
					},
				});
				budget = [];
				ReviewChecklist_id = [];
}

$("#designer_name").click(function (e) {
			$("#designer_list").show();
			e.stopPropagation();
		});

		$("#designer_list").click(function (e) {
			e.stopPropagation();
		});

		$(document).click(function () {
			$("#designer_list").hide();
		});
    
      function checkDesigner(){
      if ($('#designer_name').val().length <= 0)
      {
        alert("Please select designer!");
      }
      else{
        openPop();
      }
    }
</script>
  <script>

  function updateReport()
  {
    $.ajax(
            {
              url:'reviewtable.php',
              type:'post',
              data:'submit=true',
              success:function(data)
              {
              },
            });
  }

  $(document).ready(function(){
  $('#schedule').on('click',function(){
    $.ajax(
        {
          url:'reviewerSchedule.php',
          type:'post',
          data:'schedule=true',
          success:function(data)
          {
            $('#mainform').html(data);
            $('#active_page').html("Schedule");
            $('#btn_hamburger').trigger('click');
          },
        });
  });
  });

    $(document).ready(function()
    {
      $('#des_tracking').on('click',function()
      {
        location.reload();
      });
    });

    $(document).ready(function(){
      $('#cad_tracking').on('click',function(){
        $.ajax({
          url:'reviewerCadTracking.php',
          type:'post',
          data:'cad_tracking=true',
          success:function(data){
            $('#mainform').html(data);
            $('#active_page').html("CAD Tracking");
            $('#btn_hamburger').trigger('click');
          }
        });
      });
    });

    $(document).ready(function(){
      $('#view_all').on('click',function(){
        $.ajax({
          url:'getReviews.php',
          type:'post',
          data:'designer_id=1 OR 1 = 1',
          success:function(data){
            $('#review_list').html(data);
          },
        });
      });
    });

    $(".sel_proj_tbox").click(function (e) {
        $(".datalist").show();
        e.stopPropagation();
      });

      $(".datalist").click(function (e) {
        e.stopPropagation();
      });

      $(document).click(function () {
        $(".datalist").hide();
      });

      $(".event").click(function (e) {
        e.stopPropagation();
      });
      
      $('#sel_des_tbox').on('click',function(e){
        getNames();
        e.stopPropagation();
        $('#asselect').slideToggle();
      });
      
      $(document).on('click',function(){
        $('#asselect').slideUp();
      });
      
      $('#sel_des_tbox').on('keyup',function(e){
        filter("sel_des_tbox","asselect");
        $('#asselect').slideDown();
        
        if($('#designer_id').val() === "0")
        {
          $('#sel_des_btn').fadeOut();
        }
      });
      
      $('#sel_des_btn').on('click',function(){
        $('#sel_des_pop_bg').fadeOut();
        $('#sel_des_pop').slideUp();
        $('#designer_name').val($('#sel_des_tbox').val());
        $('#pop_dname').html($('#sel_des_tbox').val());
        refreshUpdate();
      });
      
      function getNames(){
        $.ajax({
          url:'getNames.php',
          type:'post',
          data:'from_reviewer=true',
          success:function(data){
            $('#asselect').html(data);
          }
        });
      }
      
      function selectDesigner(){
        $('#sel_des_pop_bg').fadeIn();
        $('#sel_des_pop').slideDown();
      }
      
      $(document).ready(function(){
        $('#sel_proj_tbox').on('click',function(){
          $('#proj_select').slideToggle();
        });
      });
      
      $('#sel_des_pop_bg').on('click',function(){
        $('#sel_des_pop_bg').fadeOut();
        $('#sel_des_pop').slideUp();
      });
      
      $('#pending_tab').on('click',function(){
        $(this).prop("class","tbl-tab active-tbl-tab");
        $('#requested_tab').prop("class","tbl-tab");
        $('#approved_tab').prop("class","tbl-tab");
        $('#rejected_tab').prop("class","tbl-tab");
        $.ajax({
          url:'reviewerGetData.php',
          type:'post',
          data:'get_design_tracking=true'+
               '&status=Pending',
          success:function(data){
            $('#weeklySchedule').html(data);
          }
        });
      });
      
       $('#requested_tab').on('click',function(){
        $(this).prop("class","tbl-tab active-tbl-tab");
        $('#pending_tab').prop("class","tbl-tab");
        $('#approved_tab').prop("class","tbl-tab");
        $('#rejected_tab').prop("class","tbl-tab");
        $.ajax({
          url:'reviewerGetData.php',
          type:'post',
          data:'get_design_tracking=true'+
               '&status=Requested',
          success:function(data){
            $('#weeklySchedule').html(data);
          }
        });
      });
      
      $('#approved_tab').on('click',function(){
        $(this).prop("class","tbl-tab active-tbl-tab");
        $('#pending_tab').prop("class","tbl-tab");
        $('#requested_tab').prop("class","tbl-tab");
        $('#rejected_tab').prop("class","tbl-tab");
        $.ajax({
          url:'reviewerGetData.php',
          type:'post',
          data:'get_design_tracking=true'+
               '&status=Approved',
          success:function(data){
            $('#weeklySchedule').html(data);
          }
        });
      });
      
      $('#rejected_tab').on('click',function(){
        $(this).prop("class","tbl-tab active-tbl-tab");
        $('#pending_tab').prop("class","tbl-tab");
        $('#requested_tab').prop("class","tbl-tab");
        $('#approved_tab').prop("class","tbl-tab");
        $.ajax({
          url:'reviewerGetData.php',
          type:'post',
          data:'get_design_tracking=true'+
               '&status=Rejected',
          success:function(data){
            $('#weeklySchedule').html(data);
          }
        });
      });
      
      function computeProgress(){
        var pending = $('#pending_count').html().replace("(","");
         pending = parseInt(pending.replace(")",""));
        var requested = $('#requested_count').html().replace("(","");
         requested = parseInt(requested.replace(")",""));
        var rejected = $('#rejected_count').html().replace("(","");
         rejected = parseInt(rejected.replace(")",""));
        var approved = $('#approved_count').html().replace("(","");
         approved = parseInt(approved.replace(")",""));
         var total = parseInt(pending + requested + approved + rejected);
        var percent = parseInt(approved * (100/total));
        $('#progress').css("width",percent+"%");
        if(percent <= 15)
        {
          $('#progress').css("background-color","#ed1c26");
        }
        else if(percent <= 75)
        {
          $('#progress').css("background-color","#ffc107");
        }
        else if(percent <= 100)
        {
          $('#progress').css("background-color","#4caf50");
        }
        if(isNaN(percent))
        {
          percent = 0;
        }
        return percent;
  }
  
  function hideRating(){
    $('#rating_bg').fadeOut();
    $('#rating_container').fadeOut();
  }
  
  function showRating(){
    $('#rating_bg').fadeIn();
    $('#rating_container').fadeIn();
  }
  
  $('#rating_bg').on('click',function(){
    hideRating();
  });
  
  var timeliness = parseInt($('#back_data_timeliness').val());
  var completeness = parseInt($('#back_data_completeness').val());
  var design_accuracy = parseInt($('#back_data_design_accuracy').val());
  
  <?php 
  for($count = 1;$count <= 5; $count++)
  {
    echo "$('#tstar$count').on('click',function(){
      var src = $('#tstar$count').prop('src');
      src = src.replace('http://localhost/activitymonitoring/','');
      src = src.replace('http://192.168.0.30/activitymonitoring/','');";
       for($count2 = 1; $count2 <= $count; $count2++)
      {
        echo "$('#tstar$count2').prop('src','img/small-star-y.png');";
      }
      
       for($count2 = 5; $count2 > $count; $count2--)
      {
        echo "$('#tstar$count2').prop('src','img/small-star-g.png');";
      }

        echo "timeliness = $count;";

      echo "$('#back_data_ttimelitimelinesmeliness').val(timeliness);
      $('#average_lbl').html(aveRating());
    });";
      
    echo "$('#cstar$count').on('click',function(){
      var src = $('#cstar$count').prop('src');
      src = src.replace('http://localhost/activitymonitoring/','');
      src = src.replace('http://192.168.0.30/activitymonitoring/','');";
       for($count2 = 1; $count2 <= $count; $count2++)
      {
        echo "$('#cstar$count2').prop('src','img/small-star-y.png');";
      }
      
       for($count2 = 5; $count2 > $count; $count2--)
      {
        echo "$('#cstar$count2').prop('src','img/small-star-g.png');";
      }

        echo "completeness = $count;";

    echo "$('#back_data_ttimelitimelinesmeliness').val(timeliness);
      $('#average_lbl').html(aveRating());
    });";
      
      echo "$('#dastar$count').on('click',function(){
      var src = $('#dastar$count').prop('src');
      src = src.replace('http://localhost/activitymonitoring/','');
      src = src.replace('http://192.168.0.30/activitymonitoring/','');";
      
      for($count2 = 1; $count2 <= $count; $count2++)
      {
        echo "$('#dastar$count2').prop('src','img/small-star-y.png');";
      }
      
      for($count2 = 5; $count2 > $count; $count2--)
      {
        echo "$('#dastar$count2').prop('src','img/small-star-g.png');";
      }
      
      echo "design_accuracy = $count;";

      echo "$('#back_data_ttimelitimelinesmeliness').val(timeliness);
      $('#average_lbl').html(aveRating());
    });";
  }
  ?>
    
  function aveRating(){
    var average = (timeliness + completeness + design_accuracy) / 3;
    return average.toFixed(1).replace(".0","");
  }
  
  $('#cancel_rating_btn').on('click',function(){
    hideRating();
  });
  
  $('#save_rating_btn').on('click',function(){
    $.ajax({
      beforeSend:function(data){
        return confirm('Are you sure you want to Approve?');
      },
      url:'reviewSubmit.php',
      type:'post',
      data:'approve_requirement=true'+
           '&rfd_id='+$('#req_for_des_id').val()+
           '&reviewer_id='+$('#back_data_reviewer_id').val()+
           '&project_id='+$('#back_data_project_id').val()+
           '&ticket_number='+$('#back_data_ticket_number').val()+
           '&designer_id='+$('#back_data_designer_id').val()+
           '&design_accuracy='+$('#back_data_design_accuracy').val()+
           '&timeliness='+$('#back_data_timeliness').val()+
           '&completeness='+$('#back_data_completeness').val()+
           '&rating='+$('#average_lbl').html(),
      success:function(data){
        alert(data);
        location.reload();
      }
    });
  });
  </script>
</body>
</html>