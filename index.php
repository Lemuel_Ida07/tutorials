<?php
	session_start();

	include("connect.php");

	$user_type = "";

	if(isset($_SESSION["User_Type"]))
	{
		switch($_SESSION["User_Type"])
		{
			case 0:	//Designer
				header("Location:home.php");
				break;
			case 1:	//Reviewer
				header("Location:reviewer.php");
				break;
			case 2:	//DC
				header("Location:dc.php");
				break;
			case 3:	//PM
				header("Location:pm.php");
				break;
			case 4:	//OPERATIONS
				header("Location:ops.php");
				break;
			case 5:	//Admin
				header("Location:home.php");
				break;
			case 8: //CAD Manager
				header("Location:cadManager.php");
				break;
			case 9: //CAD Technician
				header("Location:cadTechnician.php");
				break;
			case 11: //Super Admin
				header("Location:superAdmin.php");
				break;
			case 12: //Project Director
				header("Location:pd.php");
				break;
			case 14: //BNC
				header("Location:bnc.php");
				break;
		}
	}

	if(isset($_POST['LogIn']))
	{
		$username = $_POST['username'];
		$password = $_POST['password'];
		$readsql = "SELECT ID,Firstname,Middlename,Lastname,Picture,User_Type,Team_ID,Trade,Username FROM user WHERE Username = '$username' AND Password = '$password'";
		$result = mysqli_query($conn,$readsql);

		if(mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_assoc($result);

			$_SESSION['ID'] = $row['ID'];
			$_SESSION['Firstname'] = $row['Firstname'];
			$_SESSION['Middlename'] = $row['Middlename'];
			$_SESSION['Lastname'] = $row['Lastname'];
			$_SESSION['Picture'] = $row['Picture'];
			$_SESSION['User_Type'] = $row['User_Type'];
			$_SESSION['Team_ID'] = $row['Team_ID'];
			$_SESSION['Trade'] = $row['Trade'];
      $_SESSION['Username'] = $row['Username'];
			$_SESSION['Name'] = $_SESSION['Firstname']." ".$_SESSION['Middlename']." ".$_SESSION['Lastname'];

			$user_type = $_SESSION['User_Type'];
      //for multiple team
      if($row['Team_ID'] == 19)
      {
        $team_list = [];
        $team_list_id = [];
        $readteams = "SELECT 
                        team.ID,
                        team.Team_Name
                      FROM team
                      INNER JOIN team_assigned
                      ON team_assigned.Team_ID = team.ID
                      WHERE team_assigned.User_ID = ".$row['ID']."";
        $readteamresult = mysqli_query($conn,$readteams);
        if(mysqli_num_rows($readteamresult) > 0)
        {
          $count = 0;
          while($readteamrows = mysqli_fetch_assoc($readteamresult))
          {
            $team_list[$count] = $readteamrows["Team_Name"];
            $team_list_id[$count] = $readteamrows["ID"];
            $count++;
          }
        }
        $_SESSION["team_list"] = $team_list;
        $_SESSION["team_list_id"] = $team_list_id;
      }
			switch($user_type)
			{
				case 0:	//Designer
					header("Location:home.php");
					break;
				case 1:	//Reviewer
					header("Location:reviewer.php");
					break;
				case 2:	//DC
					header("Location:dc.php");
					break;
				case 3:	//PM
					header("Location:pm.php");
					break;
				case 4:	//Operations
					header("Location:ops.php");
					break;
				case 5:	//Admin
					header("Location:home.php");
					break;
				case 8: //CAD Manager
					header("Location:cadManager.php");
					break;
				case 9: //CAD Technician
					header("Location:cadTechnician.php");	
					break;
        case 11: //Super Admin
          header("Location:superAdmin.php");
          break;
        case 12: //Project Director
          header("Location:pd.php");
        	break;
        case 13: //Accounting
          header("Location:accounting.php");
        	break;
        case 14: //BNC
          header("Location:bnc.php");
        	break;
				default:
					break;
			}
					getTeamName($conn);
			//uncomment to test data
			/*
			echo $_SESSION['ID']."<br />";
			echo $_SESSION['Firstname']."<br />";
			echo $_SESSION['Middlename']."<br />";
			echo $_SESSION['Lastname']."<br />";
			echo $_SESSION['Picture']."<br />";
			echo $_SESSION['User_Type']."<br />";
			echo $_SESSION['Team_ID']."<br />";
			 */
		}
	}

	function getTeamName($conn)
	{
		$rdteamsql = "SELECT Team_Name FROM team WHERE ID = ".$_SESSION['Team_ID']."";
		$resultteamsql = mysqli_query($conn,$rdteamsql);
		$row = mysqli_fetch_assoc($resultteamsql);
		$_SESSION['Team_Name'] = $row['Team_Name'];
	}
?>
<html>
	<head>
		<title> Activity Monitoring </title>

		<meta charset="UTF-8" />

		<meta name="keywords" content="Activity, Monitoring, ActivityMonitoring" />

		<link rel="stylesheet" type="text/css" href="css/style.css"/>
		<link rel="icon" href="img/logoblue.png">
    <style>
        body{
          background-image:url("img/index_bg.jpg");
          background-attachment: fixed;
          background-size: 100% 100%;
        }
    </style>
  </head>
	<body>
	<?php include('header.php'); ?>
	<div id="margin_top">
	<br/>
	</div>
  <div id="wholebg">
  </div>
      <label id="introtext">
        Making it easy.
      </label>
	<div id="login">
		<form method="post">
      <h2 id="loginlbl"> Sign in to your account </h2>
			<input id="loginuname" type="text" name="username" placeholder="Username"/>
      <br />
			<input id="loginpassword" type="password" name="password" placeholder="Password"/>
      <br />
			<input id="loginbtn" type="submit" value="Login" name="LogIn"/>
		</form>
	</div>
	
</html>