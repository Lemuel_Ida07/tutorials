<?php
	include("connect.php");
	$designer_id = $_POST['designer_id'];
	$trade = $_POST['trade'];
	$day_dn = $_POST['day_dn'];
	
	$sql = "SELECT 
				project.Project_Number,
				project.Project_Name,
				designer_notif.ID,
				designer_notif.Activity,
				designer_notif.Budget,
				designer_notif.ReviewChecklist_ID,
				review_checklist.Code,
				internal_deadline.Phase
			FROM designer_notif
			INNER JOIN project
				ON designer_notif.Project_ID = project.ID
			INNER JOIN internal_deadline
				ON internal_deadline.ID = designer_notif.Internaldl_ID
			LEFT JOIN review_checklist 
				ON designer_notif.ReviewChecklist_ID = review_checklist.ID
			WHERE 
				designer_notif.Designer_ID = '$designer_id'
				AND EXTRACT(DAY FROM(designer_notif.Deadline)) = $day_dn";
	$result = mysqli_query($conn,$sql);
	if(mysqli_num_rows($result) > 0)
	{
		while($rows = mysqli_fetch_assoc($result))
		{
			echo "<tr id='tr".$rows["ID"]."' class='clickable'>";
			echo "	<td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>";
			echo "	<td> ".$rows["Activity"]." </td>";
			echo "	<td> ".$rows["Budget"]." hrs </td>";
			echo "  <td style='display:none;'>
								<script>
									$(document).ready(function(){
										$('#tr".$rows["ID"]."').on('click',function(){
											$('#searchbox').val('".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]."');
											$('#budget').val('".$rows["Budget"]."');
											$('#activity_cbox').prop('checked',true);
											
											$('#dnotif_id').val('".$rows["ID"]."');
										
											$('#assign_designer').hide();
											$('#edit_designer').show();
											$('#delete_designer').show();
											$('.assign_activity').show();
											$('.assign_activity_text').show();
											$('.assign_activity_text').val(' ".$rows["Activity"]."');
											$.ajax({
												url:'reviewerAssignDes.php',
												type:'post',
												data:'get_code=true'+
															'&trade=$trade'+
															'&phase=".$rows['Phase']."',
												success:function(data){
													$('#code').html(data);
													$('#code').val('".$rows["ReviewChecklist_ID"]."');
												},
												error:function(data)
												{
													alert(data);
												}
											});

											$.ajax({
												url:'reviewerAssignDes.php',
												type:'post',
												data:'getRequirementsForDes=true'+
														 '&dnotif_id=".$rows["ID"]."',
												success:function(data){
													$('#review_checklist').show();
													$('#drawing_sheets').html(data);
													requirements = [];
													expected_sheets = [];
												}
											});
										});
									});
								</script>
							</td>";
			echo "</tr>";
		}
	}
?>