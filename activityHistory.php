<?php 
    session_start();
    include('connect.php');

	$today = date("F j, Y");
?>
<style>
#menu_item_logo2
	{
		background-color:#f2f2f2;
	}
	#menu_item_logo2:hover
	{
		background-color:#f2f2f2;
	}
	#tab2
	{
		color:#515151;
		font-weight: bold;
		text-shadow: none;
	}
	</style>
<div id="info">
				<h2>My Activity History </h2>
				<fieldset id="info_module">
					<legend> <label><b>Filter Reports</b></label> </legend>
					<table>
						<tr>
							<td><h3> From </h3></td>
							<td><h3> To </h3></td>
						</tr>
						<tr>
              <td><input type="date" id="from_date" value="<?php echo date('Y-m-d');?>"/></td>
							<td><input type="date" id="to_date" value="<?php echo date('Y-m-d');?>"/></td>
						
						</tr>
					</table>
				</fieldset>
				
</div>
<div id="list">
				<table class="general_table width-100pc">
					<thead>
						<tr><th>Project</th>
							<th>Ticket No.</th>
							<th>Activity</th>
							<th>Date</th>
							<th>Time In</th>
							<th>Time Out</th>
							<th>Duration</th>
						</tr>
					</thead>
					<tbody id='activity_list'>
					<?php
						$output = '';
							$designer_id = $_SESSION["ID"];
							$sql = "SELECT 
												project.Project_Number,
												project.Project_Name,
												activity_logs.ID,
												activity_logs.Project_ID,
												activity_logs.Ticket_Number,
												activity_logs.Activity,
												activity_logs.Date,
												activity_logs.Time_In,
												activity_logs.Time_Out,
												activity_logs.Duration 
											FROM activity_logs 
											INNER JOIN project 
												ON activity_logs.Project_ID = project.ID 
											WHERE 
												Designer_ID = $designer_id 
												AND Date = CURDATE()
                                                                                        ORDER BY Date DESC";
							$result = mysqli_query($conn,$sql);
							if(mysqli_num_rows($result) > 0)
							{
							while($rows = mysqli_fetch_assoc($result))
							{
							$output.= "
								<tr>
								<td style='display:none'><input type='text' value='". $rows["ID"] ."' name='editID'></td>
								<td>". $rows["Project_Number"] ." - ". $rows["Project_Name"] ."</td>
								<td>". $rows["Ticket_Number"] ."</td>
								<td><input type='text' value='". $rows["Activity"] ."' name='editActivity'></td>
								<td><input type='text' value='". $rows["Date"] ."' name='editDate'></td>
								<td><input type='time' value='". $rows["Time_In"] ."' name = 'editTimeIn'></td>
								<td><input type='time' value='". $rows["Time_Out"] ."' name = 'editTimeOut'></td>
								<td><input type='text' value='". $rows["Duration"] ."' name = 'editDuration' readonly='readonly' /></td>
								</tr>";
							}
							echo $output;
						} 
						?>
					</tbody>
				</table>
				<script>
					$(document).ready(function(){
						$('#monthly_date').on('click',function(){
							$('#monthly_date').datepicker();
						});
                                                $('#from_date,#to_date').on('change',function(){
                                                  $.ajax({
                                                      url:'activityHistoryFilter.php',
                                                      type:'post',
                                                      data:'filter_date=true'+
                                                           '&from_date='+$('#from_date').val()+
                                                           '&to_date='+$('#to_date').val(),
                                                      success:function(data){
                                                          $('#activity_list').html(data);
                                                      },
                                                      error:function(){
                                                          alert("error");
                                                      }
                                                  });  
                                                });
					});
					</script>
</div>