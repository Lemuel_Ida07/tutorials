<?php
session_start();
include('sql.php');
if(isset($_SESSION["User_Type"]))
{
	switch($_SESSION["User_Type"])
	{
		case 0:	//Designer
			header("Location:home.php");
			break;
		case 1:	//Reviewer
			header("Location:reviewer.php");
			break;
		case 2:	//DC
			header("Location:dc.php");
			break;
		case 3:	//PM
			header("Location:pm.php");
			break;
		case 4:	//Operations
			header("Location:ops.php");
			break;
		case 5:	//Admin
			header("Location:admin.php");
			break;
	}
}
else
{
	header("Location:index.php");
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title> Activity Monitoring </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/box.css" />
    <link rel="stylesheet" type="text/css" href="css/newStyle.css" />
    <link rel="icon" href="img/logoblue.png" />
    
    
    <script src="jquery-3.2.1.min.js">  </script>
  </head>
  <body>
    <?php include("header.php"); ?>
    <div id="mainform">
      <fieldset id="pd_bnc" class="box-container wauto">
        <legend class="title"> PD & BNC </legend>
        <button id="reset_bnc" class="btn-normal bg-red white bold"> Reset BNC </button>
        <button id="reset_bnctracking" class="btn-normal bg-red white bold"> Reset BNC Tracking </button>
      </fieldset>
      <fieldset id="all" class="box-container wauto">
        <legend> All </legend>
        <button id="reset_all" class="btn-normal bg-red white bold"> Reset All </button>
      </fieldset>
	  <fieldset id="project_to_team">
		<legend> Assign Project to Team </legend>
			<?php
				$team_list = getList("SELECT * FROM team");
				for($count = 0; $count < count($team_list); $count ++)
				{
					foreach($team_list as $value)
					{
						echo $value . "</br>";
					}
				}
			?>
	  </fieldset>
    </div>
    <script>
      $('#reset_bnc').on('click',function(){
        $.ajax({
          beforeSend:function(){
            return confirm('Do you realy want to TRUNCATE bnc?');
          },
          url:'superAdminSubmit.php',
          type:'post',
          data:'reset_bnc=true',
          success:function(){
          }
        });
      });
      
      $('#reset_bnctracking').on('click',function(){
        $.ajax({
          beforeSend:function(){
            return confirm('Do you realy want to Reset BNC Tracking?');
          },
          url:'superAdminSubmit.php',
          type:'post',
          data:'reset_bnctracking=true',
          success:function(){
          }
        });
      });

      $('#reset_all').on('click',function(){
        $.ajax({
          beforeSend:function(){
            return confirm('Truncate whole database?');
          },
          url:'superAdminSubmit.php',
          type:'post',
          data:'reset_all=true',
          success:function(){

          }
        });
      });
    </script>
  </body>
</html>

