<?php
include('connect.php');
session_start();
if(isset($_SESSION['User_Type']))
{
	$id = $_SESSION['ID'];
	$password = "";
	$profile_pic = "";
	$passsql = "SELECT Password,Picture FROM user WHERE ID = $id";
	$result = mysqli_query($conn,$passsql);
	if(mysqli_num_rows($result) > 0)
	{
		$row = mysqli_fetch_assoc($result);
		$password = $row['Password'];
		$profile_pic = $row['Picture'];
	}
}
if(isset($_GET['change_pass']))
{
			$designer_id = $_SESSION["ID"];
			$designer_pass = $_GET['password'];

			//insert profile pic update here


			$image = addslashes(file_get_contents($_FILES['image']['tmp_name'])); //SQL Injection defence!
			$image_name = addslashes($_FILES['image']['name']);

			$imagefile = $_FILES['image']['name'];
			$tmp_dir = $_FILES['image']['tmp_name'];
			$uploaddir = "images/profile_pictures/";
			$imagename = pathinfo($imagefile,PATHINFO_BASENAME);

			if($image_name == '')
			{
				$image_name = $profile_pic;
			}
			else
			{
				move_uploaded_file($tmp_dir,$uploaddir.$imagename);
			}
			$sql = "UPDATE user SET Password = '$designer_pass', Picture = '".$image_name."' WHERE ID = '$designer_id'";
			if(mysqli_query($conn,$sql))
			{
				header("Location:index.php");
			}
}
if(isset($_POST['action']))
{
  if($_POST["action"] == "update")
  {
			$designer_id = $_SESSION["ID"];
			$designer_pass = $_POST['password'];
      $first_name = $_POST['Firstname'];
      $middle_name = $_POST['Middlename'];
      $last_name = $_POST['Lastname'];
      $username = $_POST['Username'];
			//insert profile pic update here

			$image = file_get_contents($_FILES['input_user_pic']['tmp_name']);
			$image_name = $_FILES['input_user_pic']['name'];

			$imagefile = $_FILES['input_user_pic']['name'];
			$tmp_dir = $_FILES['input_user_pic']['tmp_name'];
			$uploaddir = "images/profile_pictures/";
			$imagename = pathinfo($imagefile,PATHINFO_BASENAM).$designer_id.".jpg";

			if($image_name == '')
			{
				$image_name = $profile_pic;
			}
			else
			{
				move_uploaded_file($tmp_dir,$uploaddir.$imagename);
			}
			$sql = "UPDATE user SET Password = '$designer_pass', Picture = '".$imagename."',Firstname ='$first_name',Middlename ='$middle_name',Lastname ='$last_name' WHERE ID = '$designer_id'";
			if(mysqli_query($conn,$sql))
			{
				header("Location:manageAccount.php");
			}
  }
}
if(isset($_GET['cancel']))
{
	switch($_SESSION["User_Type"])
	{
		case 0:	//Designer
      header("Location:home.php");
      break;
    case 1:	//Reviewer
      header("Location:reviewer.php");
      break;
    case 2:	//DC
      header("Location:dc.php");
      break;
    case 3:	//PM
      header("Location:pm.php");
      break;
    case 4:	//OPERATIONS
      header("Location:ops.php");
      break;
    case 5:	//Admin
      header("Location:home.php");
      break;
    case 8: //CAD Manager
      header("Location:cadManager.php");
      break;
    case 9: //CAD Technician
      header("Location:cadTechnician.php");
      break;
    case 11: //Super Admin
      header("Location:superAdmin.php");
      break;
    case 12: //Project Director
      header("Location:pd.php");
      break;
    case 14: //BNC
      header("Location:bnc.php");
      break;
	}
}
?>
<html>
<head>
	<title> Activity Monitoring | Manage Account </title>
	<link rel="stylesheet" type="text/css" href="css/changepass.css" />
</head>
<body background="drafts/bg001.jpg">
	<div id="edit_account">
		<form id="m_a" method="post" enctype="multipart/form-data">
			<legend>
				<h3> Manage Account </h3>
			</legend>
			<center>
				<?php

				$id = $_SESSION["ID"];

				$sql = "SELECT Picture FROM user WHERE ID = $id";

				$result = mysqli_query($conn,$sql);
				if(mysqli_num_rows($result) > 0)
				{
					$row = mysqli_fetch_assoc($result);
					$checkPic = $row['Picture'];

					if($checkPic == '') {
						$checkPic = 'default.png';
					}
					echo "<img id='pic_preview' src='images/profile_pictures/$checkPic'>";
				}

                ?>
			</center>
			<br />
			<table border=0>
				<tr>
					<td>
						<label id="labels"> Change Profile Picture : </label>
					</td>
					<td>
						<input id="pic_selected" type="file" accept="image/*" name="image" onchange="(chooseImg('pic_preview'))" />
					</td>
				</tr>

				<tr>
					<td>
						<label id="labels"> New Password : </label>
					</td>
					<td>
						<input id="newpassword" type="password" name="password" value="<?php echo $password; ?>" />
					</td>
				</tr>
				<td colspan="2">
					<input id="hints" type="checkbox" onclick="myFunction()" />
					<label id="hints">Show Password</label>
				</td>
				<tr>
			</table>
			<center>
				<input id="form_action" type="submit" value="Update" name="change_pass" />
			</center>
		</form>

		<form method="GET">
			<center>
				<input id="form_action" type="submit" value="Cancel" name="cancel" />
			</center>
		</form>
	</div>
	<div id="footer"></div>
</body>
<script>
	function myFunction() {
		var x = document.getElementById("newpassword");
		if (x.type === "password") {
			x.type = "text";
		}
		else {
			x.type = "password";
		}
	}


</script>
    <script type="text/javascript" src="scripts/javaScript.js"></script>
</html>
