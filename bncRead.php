<?php
  session_start();
  include("connect.php");
  include("phpGlobal.php");
  $user_id =  $_SESSION["ID"];
  //for bnc
  //for counts
  if(isset($_POST["requested_count"]))
  {
    $sql = "SELECT 
              COUNT(external_deadline.ID) AS requested_count
            FROM project 
            INNER JOIN external_deadline 
              ON project.ID = external_deadline.Project_ID 
            INNER JOIN team
              ON project.Team_ID = team.ID
            WHERE external_deadline.Status = 'Done'";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result) > 0)
        {
          $row = mysqli_fetch_assoc($result);
          echo $row["requested_count"];
        }
  }
  
  if(isset($_POST["pending_count"])){
    $readsql = "SELECT
                  COUNT(bnc.ID) AS Pending_Count
                FROM bnc
                INNER JOIN external_deadline
                  ON bnc.Externaldl_ID = external_deadline.ID
                INNER JOIN project 
                  ON external_deadline.Project_ID = project.ID
                WHERE bnc.Status = 'Pending'";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      $row = mysqli_fetch_assoc($result);
      echo $row["Pending_Count"];
    }
  }
  
  if(isset($_POST["reject_count"])){
    $readsql = "SELECT
                  COUNT(bnc.ID) AS Rejected_Count
                FROM bnc
                INNER JOIN external_deadline
                  ON bnc.Externaldl_ID = external_deadline.ID
                INNER JOIN project 
                  ON external_deadline.Project_ID = project.ID
                WHERE bnc.Status = 'Rejected'";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      $row = mysqli_fetch_assoc($result);
      echo $row["Rejected_Count"];
    }
  }
  
  if(isset($_POST["paid_count"])){
    $readsql = "SELECT
                  COUNT(bnc.ID) AS Paid_Count
                FROM bnc
                INNER JOIN external_deadline
                  ON bnc.Externaldl_ID = external_deadline.ID
                INNER JOIN project 
                  ON external_deadline.Project_ID = project.ID
                WHERE bnc.Status = 'Paid'";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      $row = mysqli_fetch_assoc($result);
      echo $row["Paid_Count"];
    }
  }

  //for list
  //requested
  if(isset($_POST["requested_bnc"]))
  {
    $readsql = "SELECT project.ID,
										project.Project_Name, 
										project.Team_ID, 
										project.Project_Number,
                    project.Amount,
                    external_deadline.Percent,
										external_deadline.Phase,	
										external_deadline.ID AS exid ,
                    external_deadline.External_Deadline,
                    external_deadline.Status,
                    team.Team_Name
									FROM project 
									INNER JOIN external_deadline 
										ON project.ID = external_deadline.Project_ID 
                  INNER JOIN team
                    ON project.Team_ID = team.ID
									WHERE external_deadline.Status = 'Done'";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      while($rows = mysqli_fetch_assoc($result)){
        echo "<tr>
                <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                <td> ".$rows["Percent"]." </td>
                <td> &#8369; ".$rows["Amount"]." </td>
                <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>
                <td>
                  <input type='button' class='btn-normal white bg-green' value='Create' id='create".$rows["ID"]."'>
                  <script>
                    $('#create".$rows["ID"]."').on('click',function(){
                      $('#billing_externaldl_id').val('".$rows["exid"]."');
                      showBilling();
                    });
                  </script>
                </td>
              </tr>";
      }
    }
  }
  //processing
  if(isset($_POST["pending_bnc"]))
  {
    $readsql = "SELECT
                  bnc.ID,
                  project.Project_Number,
                  project.Project_Name,
                  external_deadline.External_Deadline,
                  external_deadline.Percent,
                  external_deadline.Phase,
                  project.Amount,
                  project.Team_ID,
                  bnc.Endorced_To_Client,
                  bnc.Invoice_Number,
                  team.Team_Name
                FROM bnc
                INNER JOIN external_deadline
                  ON bnc.Externaldl_ID = external_deadline.ID
                INNER JOIN project 
                  ON external_deadline.Project_ID = project.ID
                INNER JOIN team
                  ON project.Team_ID = team.ID
                WHERE bnc.Status = 'Pending'";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      while($rows = mysqli_fetch_assoc($result)){
        echo "<tr>
                <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                <td> ".$rows["Percent"]." </td>
                <td> &#8369; ".$rows["Amount"]." </td>
                <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>
                <td> ".$rows["Endorced_To_Client"]." </td>
                <td> ".$rows["Invoice_Number"]." </td>
                <td>
                  <input type='button' class='btn-normal white bg-green' id='paid".$rows["ID"]."' value='Paid' />
                  <input type='button' class='btn-normal white bg-red' id='reject".$rows["ID"]."' value='Reject' />
                  <script>
                    $('#paid".$rows["ID"]."').on('click',function(){
                      $('#bnc_amount_paid').val('".$rows["Amount"]."');
                      $('#bnc_id').val('".$rows["ID"]."');
                      showBncPop();
                    });
                    $('#reject".$rows["ID"]."').on('click',function(){
                      $('#bnc_id').val('".$rows["ID"]."');
                      $('#reject_pop').slideDown();
                    });
                  </script>
                </td>
              </tr>";
      }
    }
  }
  
  //reject
  if(isset($_POST["rejected_bnc"]))
  {
    $readsql = "SELECT
                  bnc.ID,
                  project.Project_Number,
                  project.Project_Name,
                  external_deadline.External_Deadline,
                  external_deadline.Percent,
                  external_deadline.Phase,
                  project.Amount,
                  project.Team_ID,
                  bnc.Endorced_To_Client,
                  bnc.Invoice_Number,
                  bnc.Reason,
                  team.Team_Name
                FROM bnc
                INNER JOIN external_deadline
                  ON bnc.Externaldl_ID = external_deadline.ID
                INNER JOIN project 
                  ON external_deadline.Project_ID = project.ID
                INNER JOIN team
                  ON project.Team_ID = team.ID
                WHERE bnc.Status = 'Rejected'";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      while($rows = mysqli_fetch_assoc($result)){
        echo "<tr>
                <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                <td> ".$rows["Percent"]." </td>
                <td> &#8369; ".$rows["Amount"]." </td>
                <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>
                <td> ".$rows["Endorced_To_Client"]." </td>
                <td> ".$rows["Invoice_Number"]." </td>
                <td> ".$rows["Reason"]." </td>
                
              </tr>";
      }
    }
  }
  //paid
  if(isset($_POST["paid_bnc"]))
  {
    $readsql = "SELECT
                  bnc.ID,
                  project.Project_Number,
                  project.Project_Name,
                  external_deadline.External_Deadline,
                  external_deadline.Percent,
                  external_deadline.Phase,
                  project.Amount,
                  project.Team_ID,
                  bnc.Endorced_To_Client,
                  bnc.Endorced_To_Accounting,
                  bnc.Invoice_Number,
                  bnc.OR_Number,
                  bnc.Amount_Paid,
                  bnc.Payment_Type,
                  team.Team_Name
                FROM bnc
                INNER JOIN external_deadline
                  ON bnc.Externaldl_ID = external_deadline.ID
                INNER JOIN project 
                  ON external_deadline.Project_ID = project.ID
                INNER JOIN team 
                  ON team.ID = project.Team_ID
                WHERE bnc.Status = 'Paid'";
    $result = mysqli_query($conn,$readsql);
    if(mysqli_num_rows($result) > 0)
    {
      while($rows = mysqli_fetch_assoc($result)){
        echo "<tr>
                <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                <td> ".$rows["Percent"]." </td>
                <td> ".$rows["Amount"]." </td>
                <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>
                <td> ".$rows["Endorced_To_Accounting"]." </td>
                <td> ".$rows["OR_Number"]." </td>
                <td> &#8369; ".$rows["Amount_Paid"]." </td>
                <td> ".$rows["Payment_Type"]." </td>
              </tr>";
      }
    }
  }
/* Van Hudson Galvoso
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

