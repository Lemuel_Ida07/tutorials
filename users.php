<?php
include('connect.php');
	$today = date("F j, Y");
?>
<style>
  #navbar tr td#users{
      background-color:#f7f9fa;
      color:#0747a6;
      border-color:#0747a6;
      box-shadow: inset 0px 2px 2px gray;
    }
    
  #navbar tr td#projects{
      background-color:#0747a6;
      color:#deebff;
      box-shadow: 0px 0px 1px 2px #deebff inset;
    }  
  
</style>
  <div id="info">
    <label class="h2"><?php echo $today; ?></label>
    <br />
    <label class="title"> New User </label>
    <form method="post">
      <input type="text" name="dusername" placeholder="Username" required="required" /><br />
      <input type="text" name="fname" placeholder="Firstname" required="required" /><br />
      <input type="text" name="mname" placeholder="Middlename" /><br />
      <input type="text" name="lname" placeholder="Lastname" required="required" /><br />
      <!-- User Type -->
      <select id="add_user_type" name="add_user_type" required="required" class="option text dropdown">
              <option> - Select User Type - </option>
              <option value="All"> All </option>
              <?php
                $sql = "SELECT ID,Type FROM User_Type";
                $result = mysqli_query($conn,$sql);
                if(mysqli_num_rows($result) > 0)
                {
                  while($rows = mysqli_fetch_assoc($result))
                  {
                    echo "<option value='".$rows["ID"]."'> ".$rows["Type"]." </option>";
                  }		
                }	
              ?>
      </select>
      <br /> 
      <br />
      <!-- Team -->
      <select id="add_team_id" name="add_team_id" required="required" class="option text dropdown">
        <option> - Select Team - </option><?php
        $sql = "SELECT * FROM team";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result) > 0)
        {
        while($rows = mysqli_fetch_assoc($result))
        {
          echo "<option value='".$rows['ID']."'> ".$rows['Team_Name']." </option>";
        }
        }
        ?>
      </select>	
      <br /> 
      <br />
      <!-- Trade -->
      <select id="add_trade" name="add_trade" class="option text dropdown">
          <option> - Select Trade - </option>		
          <option value="All"> All </option>
          <?php
            $sql = "SELECT T_Code FROM trades";
            $result = mysqli_query($conn,$sql);
            if(mysqli_num_rows($result) > 0)
            {
              while($rows = mysqli_fetch_assoc($result))
              {
                echo "<option value='".$rows["T_Code"]."'> ".$rows["T_Code"]." </option>";
              }		
            }	
          ?>
        </select>	
      <br />
      <input type="submit" value="Add User" name="add_user" id="addbtn" style="margin-left:15%; width:70%" />
    </form>
    <form method="post" action="table.php" style="visibility:hidden">
      <button value="Submit" name="submit" id="submit2">Submit</button>
    </form>
  </div>
  <div id="list">
    <label class="h1" style="padding-left:4px;padding-right:4px;float:left;"> Users </label>
    <div class="search_group">
      <input type="text" class="searchbox" style="border:none;" id="search_users" placeholder="Search..." />
      <label id="result_users" class="popBgColor bold"> </label>

    </div>
    <div style="overflow-y:auto;border-top:solid 2px #cccccc;">
    <table id="users_list" class="general_table width-100pc">
      <thead>
        <tr>
          <th>Username</th>
          <th>Firstname</th>
          <th>Middlename</th>
          <th>Lastname</th>
          <th>User Type
            <select id="drop_user_type" class="option bold">
              <option value="All"> All </option>
              <?php
                $sql = "SELECT ID,Type FROM User_Type";
                $result = mysqli_query($conn,$sql);
                if(mysqli_num_rows($result) > 0)
                {
                  while($rows = mysqli_fetch_assoc($result))
                  {
                    echo "<option value='".$rows["ID"]."'> ".$rows["Type"]." </option>";
                  }		
                }	
              ?>
            </select>
          </th>
          <th>Team
            <select id="drop_team" class="option bold">
              <option value="All"> All </option>
              <?php
                $sql = "SELECT ID,Team_Name FROM team";
                $result = mysqli_query($conn,$sql);
                if(mysqli_num_rows($result) > 0)
                {
                  while($rows = mysqli_fetch_assoc($result))
                  {
                    echo "<option value='".$rows["ID"]."'> ".$rows["Team_Name"]." </option>";
                  }		
                }	
              ?>
            </select>
          </th>
          <th>Trade	
            <select id="drop_trade" class="option bold">
              <option value="All"> All </option>
              <?php
                $sql = "SELECT T_Code FROM trades";
                $result = mysqli_query($conn,$sql);
                if(mysqli_num_rows($result) > 0)
                {
                  while($rows = mysqli_fetch_assoc($result))
                  {
                    echo "<option value='".$rows["T_Code"]."'> ".$rows["T_Code"]." </option>";
                  }		
                }	
              ?>
            </select>
          </th>
          <th>Status 
            <select id="drop_status" class="option bold">
              <option value="All"> All </option>
              <?php
                $sql = "SELECT DISTINCT(Status) FROM user";
                $result = mysqli_query($conn,$sql);
                if(mysqli_num_rows($result) > 0)
                {
                  while($rows = mysqli_fetch_assoc($result))
                  {
                    echo "<option value='".$rows["Status"]."'> ".$rows["Status"]." </option>";
                  }		
                }	
              ?>
            </select>
          </th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody id="users_body">				
        <?php
          $sql = "SELECT 
                    user.ID,
                    user.Username,
                    user.Firstname,
                    user.Middlename,
                    user.Lastname,
                    user.Status,
                    user.User_Type,
                    team.ID AS Team_ID,
                    team.Team_Name,
                    user.Trade 
                  FROM user
                  LEFT JOIN team 
                    ON user.Team_ID = team.ID 
                  ORDER BY user.Firstname";
          $result = mysqli_query($conn,$sql);
          if(mysqli_num_rows($result) > 0)
          {
            while($rows = mysqli_fetch_assoc($result))
            {
              echo "
              <tr>
              <td><input type='text' style='padding:1%;' value='".$rows['Username']."' name='editdusername'/></td>
              <td><input type='text' style='padding:1%;' value='".$rows['Firstname']."' name='editdfname'/></td>
              <td><input type='text' style='padding:1%;' value='".$rows['Middlename']."' name='editdmname'/></td>
              <td><input type='text' style='padding:1%;' value='".$rows['Lastname']."' name='editdlname'/></td>";
              $user_type = "";
              switch($rows["User_Type"])
              {
                case 0:	//Designer
                  $user_type="Designer";
                  break;
                case 1:	//Reviewer								 
                  $user_type="Reviewer";
                  break;
                case 2:	//DC								 
                  $user_type="DC";
                  break;
                case 3:	//PM								 
                  $user_type="PM";
                  break;
                case 4:	//OPERATIONS					
                  $user_type="Operations";
                  break;
                case 5:	//Admin									
                  $user_type="Admin";
                  break;
                case 8: //CAD Manager								
                  $user_type="CAD Manager";
                  break;
                case 9: //CAD Technician								
                  $user_type="CAD Technician";
                  break;
              }

              //for user_type
              echo "<td><select id='select_user_type".$rows["ID"]."' name='edituser_type' class='margin-0'>";
              $remarkssql = "SELECT DISTINCT user_type.ID,user_type.Type 
                            FROM user_type 
                            LEFT JOIN user 
                              ON user.User_Type = user_type.ID 
                            WHERE user_type.ID != '".$rows["User_Type"]."'";
              $remarks_result = mysqli_query($conn,$remarkssql);
              if(mysqli_num_rows($remarks_result) > 0)
              {
                echo "<option value='".$rows["User_Type"]."'> $user_type </option>";
                while($result_row = mysqli_fetch_assoc($remarks_result))
                {
                  echo "<option value='".$result_row["ID"]."'> ".$result_row["Type"]." </option>";																					
                }
              }
              echo "</select></td>";
              //for team
              echo "<td><select id='select_team".$rows["ID"]."' name='editteam' class='margin-0'>";
              $remarkssql = "SELECT DISTINCT team.ID,team.Team_Name 
                            FROM user 
                            RIGHT JOIN team 
                              ON user.Team_ID = team.ID 
                            WHERE team.ID != '".$rows["Team_ID"]."'";
              $remarks_result = mysqli_query($conn,$remarkssql);
              if(mysqli_num_rows($remarks_result) > 0)
              {
                echo "<option value='".$rows["Team_ID"]."'> ".$rows["Team_Name"]." </option>";
                while($result_row = mysqli_fetch_assoc($remarks_result))
                {
                  echo "<option value='".$result_row["ID"]."'> ".$result_row["Team_Name"]." </option>";																					
                }
              }
              echo "</select></td>";
              //for trade
              echo "<td><select id='select_trade".$rows["ID"]."' name='edittrade' class='margin-0'>";
              $remarkssql = "SELECT DISTINCT(Trade) FROM user WHERE Trade != '".$rows["Trade"]."'";
              $remarks_result = mysqli_query($conn,$remarkssql);
              if(mysqli_num_rows($remarks_result) > 0)
              {
                echo "<option value='".$rows["Trade"]."'> ".$rows["Trade"]." </option>";
                while($result_row = mysqli_fetch_assoc($remarks_result))
                {
                  echo "<option value='".$result_row["Trade"]."'> ".$result_row["Trade"]." </option>";																					
                }
              }
              echo "</select></td>";
              //for status
              echo "<td><select id='select_status".$rows["ID"]."' name='editstatus' class='margin-0'>";
                if($rows['Status'] == "Active")
                {				
                  echo "<option value='Active'> Active </option>";																					
                  echo "<option value='Inactive'  style='color:red;'> Inactive </option>";	
                }
                else
                {
                  echo "<script> $('#select_status".$rows["ID"]."').attr('style','color:red; font-weight:bold;'); </script>";
                  echo "<option value='Inactive'> Inactive </option>";																					
                  echo "<option value='Active' style='color:black;'> Active </option>";	
                }
              echo "</select></td>";


              echo"
              <td><input type='submit'  value='Edit' name='edit_user' class='editbtn' id='editbtn".$rows["ID"]."''/></td>
              <td style='display:none;'><input type='text' value='".$rows['ID']."' name='editid'/></td>
                <script>
									$(document).ready(function(){
										$('#editbtn".$rows["ID"]."').on('click',function(){
											$.ajax(
											{
												url:'opsSubmit.php',
												type:'post',
												data:'edit_user=true'+
													 '&editid=".$rows['ID']."'+
													 '&editdusername='+$('#editdusername".$rows['ID']."').val()+
													 '&editdfname='+$('#editdfname".$rows["ID"]."').val()+
													 '&editdmname='+$('#editdmname".$rows["ID"]."').val()+
													 '&editdlname='+$('#editdlname".$rows["ID"]."').val()+
													 '&edituser_type='+$('#select_user_type".$rows["ID"]."').val()+
													 '&editteam='+$('#select_team".$rows["ID"]."').val()+
													 '&edittrade='+$('#select_trade".$rows["ID"]."').val()+
													 '&editstatus='+$('#select_status".$rows["ID"]."').val(),
												success:function(data){
													alert(data);
                          $('#users').trigger('click');
												},
												error:function(data){
													alert(data);
												}
											});
										});
									});	
								</script></tr>";
            }
          }
        ?>
      </tbody>
    </table>
    </div>
  </div>
  <div id="script">
		</div>
<script>
	$(document).ready(function(){
	
		hidePop();
		$('#searchbtn').on('click',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+document.getElementById('search_project').value,
				success:function(data)
				{   
					$('#activitylist').html(data);
				},
			});
		});
	});
	
	$(document).ready(function(){
		$('#search_project').on('input',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+document.getElementById('search_project').value+
						 '&for=projects',
				success:function(data)
				{   
					$('#activitylist').html(data);
				},
			});
		});
    
		$('#search_tickets').on('input',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+document.getElementById('search_tickets').value+
						 '&for=tickets',
				success:function(data)
				{   
					$('#tickets_list').html(data);
				},
			});
		});
    
		$('#search_users').on('input',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+document.getElementById('search_users').value+
						 '&for=users',
				success:function(data)
				{   
					$('#users_body').html(data);
				},
			});
		});
    
		$('#drop_status').on('change',function(){
			var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+value+
						 '&for=users_status',
				success:function(data)
				{   
					$('#users_body').html(data);
				},
			});
		});
    
		$('#drop_trade').on('change',function(){
			var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+value+
						 '&for=users_trade',
				success:function(data)
				{   
					$('#users_body').html(data);
				},
			});
		});
    
		$('#drop_team').on('change',function(){
			var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+value+
						 '&for=users_team',
				success:function(data)
				{   
					$('#users_body').html(data);
				},
			});
		});

		$('#drop_user_type').on('change',function(){
			var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+value+
						 '&for=users_user_type',
				success:function(data)
				{   
					$('#users_body').html(data);
				},
			});
		});
	});

	$(document).ready(function(){
	$('#projects').on('click',function(){
		location.reload();
	});
	});

	$(document).ready(function(){
	$('#ob').on('click',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'manageOB.php',
				type:'post',
				data:'request='+value,
				success:function(data)
				{
					$('#mainform').html(data);
				},
			});
	});
	});

	$(document).ready(function(){
	$('#overtime').on('click',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'manageOT.php',
				type:'post',
				data:'request='+value,
				success:function(data)
        {
					$('#mainform').html(data);
				},
			});
	});
	});

	$(document).ready(function(){
	$('#cal').on('click',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'calendar.php',
				type:'post',
				data:'request='+value,
				success:function(data)
				{   
					$('#mainform').html(data);
				},
			});
	});
	});

	$(document).ready(function(){
	$('#users').on('click',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'users.php',
				type:'post',
				data:'request='+value,
				success:function(data)
				{   
					$('#mainform').html(data);
				},
			});
	});
	});

	$(".searchbox").click(function (e) {
			$(".datalist").show();
			e.stopPropagation();
		});

		$(".datalist").click(function (e) {
			e.stopPropagation();
		});

		$(document).click(function () {
			$(".datalist").hide();
		});

		//Disable submit on enter key
		$(document).bind('keydown', function(e) {
			if (e.keyCode == 13) {
				e.preventDefault();
			}
		});

</script>
</body>
</html>