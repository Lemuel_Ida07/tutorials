<?php
	include("connect.php");
	session_start();
	if(isset($_SESSION["User_Type"]))
	{
	$id = $_SESSION['ID'];
	$curr_year = (int)date("Y");
	$curr_month = (int)date("m");
	$curr_day = (int)date("d");
	if($curr_day <= 15)
	$day_limit = 15;																																																																													 
	if($curr_day > 15)
	$day_limit = 31;
	$notif_count = 0;

	switch($_SESSION['User_Type'])
	{
		case 0:
			$notifsql = "SELECT COUNT(Internaldl_ID) 
									 FROM designer_notif 
									 WHERE Designer_ID = $id
											AND EXTRACT(YEAR FROM(designer_notif.Deadline)) <= $curr_year
											AND EXTRACT(MONTH FROM(designer_notif.Deadline)) <= $curr_month
											AND EXTRACT(DAY FROM(designer_notif.Deadline)) <= $day_limit
											AND EXTRACT(DAY FROM(designer_notif.Deadline)) >= EXTRACT(DAY FROM(CURDATE()))";
			$result = mysqli_query($conn,$notifsql);
			$notif_count = 0;
			if(mysqli_num_rows($result) > 0)
			{
				$row = mysqli_fetch_assoc($result);
				$notif_count = $row['COUNT(Internaldl_ID)'];
				if($row['COUNT(Internaldl_ID)']!=0)
				{
					echo "#notif {
							-webkit-animation: ring 1s linear 0s infinite alternate;
							animation: ring 1s linear 0s infinite alternate;
						  }
						  @-moz-keyframes ring {
							0% {transform: rotate(45deg);}
							100% {transform: rotate(-45deg);}
						  }
						  @-webkit-keyframes ring {
							0%   {transform: rotate(45deg);}
							25%  {transform: rotate(-45deg);}
							50%  {transform: rotate(45deg);}
							75%  {transform: rotate(-45deg);}
							100% {transform: rotate(45deg);}
						  }";
				}
			}
			else
			{
				echo "";
			}
			break;
		case 1:
			$notifsql ="SELECT 
										COUNT(Internaldl_ID) 
									FROM notification 
									INNER JOIN internal_deadline 
										ON notification.Internaldl_ID =  internal_deadline.ID 
									WHERE 
										notification.User_ID = $id 
										AND Status = 0 
										AND internal_deadline.Month_ID = $curr_month";
			$result = mysqli_query($conn,$notifsql);
			$notif_count = 0;
			if(mysqli_num_rows($result) > 0)
			{
				$row = mysqli_fetch_assoc($result);
				$notif_count = $row['COUNT(Internaldl_ID)'];
				if($row['COUNT(Internaldl_ID)']!=0)
				{
				echo "#notif{
								-webkit-animation: ring 1s linear 0s infinite alternate;
								animation: ring 1s linear 0s infinite alternate;
							}
							@-moz-keyframes ring {
								0%{transform: rotate(45deg);}
								100% {transform: rotate(-45deg);}
							}
							@-webkit-keyframes ring {
								0%   {transform: rotate(45deg);}
								25%  {transform: rotate(-45deg);}
								50%  {transform: rotate(45deg);}
								75%  {transform: rotate(-45deg);}
								100% {transform: rotate(45deg);}
							}";
				}
			}
			else
			{
				echo "";
			}
		break;
	}
	}
?>
<script>
		document.getElementById('notifcount').innerHTML = "<?php echo $notif_count;?>";
		document.getElementById('notif_header').value = "Notifications (<?php echo $notif_count; ?>)";
</script>