<?php
	include('connect.php');
	session_start();
	$today = date("F j, Y");
?>
<style>
	#mainform {
		height:99%;
	}
</style>
<div id="info" style="width:100%;margin-left:0;padding-left:0;max-width:100%;">
	<h3 style="padding-bottom:0;margin-bottom:0;"><?php echo $today; ?></h3>
	<h2> Official Bussiness Request </h2>
	<form method="post" style="margin-top:0;padding-top:0;">
		
		<div id="topic" >
			<h2> Submittal Details </h2>
			<select style="padding:3%;"name="ProjectNumber" id="ProjectNumber" required="required">
				<option> Select Project Number </option>
				<?php
				$sql = "SELECT ProjectNumber ,Project_Name FROM projects";
				$result = mysqli_query($conn,$sql);
				if(mysqli_num_rows($result) > 0)
				{
					while($rows = mysqli_fetch_assoc($result))
					{
					  echo "<option value='".$rows["ProjectNumber"]."'> ".$rows["ProjectNumber"]." - " .$rows["Project_Name"]. "</option>";
					}
			    }
                ?>
			</select>
		
			<textarea type="text" placeholder="Activity/Description" rows="4"></textarea>
			<textarea type="text" placeholder="Plans/RFI/RFA/Equipment Needed" rows="2"></textarea>
		</div>
		
		<div id="topic">
			<h2> Requested for <img onClick="addName()" style="cursor: pointer; height: 30px; float: right;" src="img/plus_selected.png"></h2> 
			<div id="names" style="height: 300px; width: 100%; overflow: auto;">
				<i id="n1" style="font-size: 16px;"> Please click the ADD button above </i>
				<!-- INPUTS HERE -->
			</div>
		</div>
		
		<div id="topic">
			<h2> Travel Specifics </h2>
			<input type="text" placeholder="Destination" />
			<input type="text" placeholder="Model of transport" />	
			<h2> Cash Advance </h2>
			<style>
				table {
					margin-top: 20px;
					width: 90%;			
				}
				td {
					text-align: center;
				}	
				#CA {
					text-align: right;
				}
				#menu_item_logo2{
					background-color:#f2f2f2;
				}
				#menu_item_logo2:hover{
					background-color:#f2f2f2;
				}
				#tab2{
					color:#515151;
					font-weight: bold;
					text-shadow: none;
				}
			</style>
			<center>
				<table border=0 cellpadding="5">
					<tr>
						<td id="CA"> Food Allowance	</td>
						<td> <input type="checkbox" /> </td>
						<td id="CA"> Accommodation </td>
						<td> <input type="checkbox" /> </td>
					</tr>
					<tr>
						<td id="CA"> Fuel Allowance	</td>
						<td> <input type="checkbox" /> </td>
						<td id="CA"> Toll Allowance </td>
						<td> <input type="checkbox" /> </td>
					</tr>
				</table>
			</center>
		</div>
					
		<div id="topic">	
			<input type="date" name="ob_date" style="width: 82%; margin-top: 10%" />
			<br>
			<label id="label1"> From </label>
			<label id="label2"> To </label>
			<br/> 
			<input type="time" name="TimeIn" required/><input id="inptext2" type="time" name="TimeOut"required/>
			<br>
			<h1> Total Hours:<span> 0 </span> </h1> 
			<br>
			<input type="submit" value="Generate Official Bussiness Request" name="add" id="gen_ob"/>
		</div>
					
	</form>
	<form method="post" action="table.php" style="display:none;">
		<input type="submit" value="Add" name="submit" id="submit2"/>
	</form>
	<div style="width: 95%; height: 30%; display: block; float: none; border-radius: 0px;" id="list">
	</div>
</div>
	
<script>

	var nameCount = 1;
	
	function addName() {

		$("#n1").remove();

		var cnt = document.createElement("div");
		var inp = document.createElement("input");
		var ter = document.createElement("img");

		cnt.setAttribute("id", "nameContainer" + nameCount++);

		inp.setAttribute("type", "text");
		inp.setAttribute("id", "cand");
		inp.setAttribute("placeholder", "Add Candidate");
		inp.setAttribute("style", "width: 84%; padding: 10px; margin-top: 10px; margin-right: 5px;");

		ter.setAttribute("id", "ter");
		ter.setAttribute("src", "img/plus_selected.png");
		ter.setAttribute("style", "cursor: pointer; height: 30px; vertical-align: top; transform: rotate(45deg); margin-top: 13px; margin-left: 0px;");
		ter.setAttribute("onClick", "removeName(this)");

		cnt.appendChild(inp);
		cnt.appendChild(ter);

		document.getElementById("names").appendChild(cnt);

	}

	function removeName(node) {
		var child = node;

		parent = child.parentNode;

		$("#" + parent.id).remove();

	}

</script>