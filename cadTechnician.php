<?php
  session_start();
	include("connect.php");
  $shortcut_name = $_SESSION["Lastname"].", ".$_SESSION["Firstname"];
	if(isset($_SESSION["User_Type"]))
	{
		switch($_SESSION["User_Type"])
		{
			case 1:	//Reviewer
				header("Location:reviewer.php");
				break;
			case 2:	//DC
				header("Location:dc.php");
				break;
			case 3:	//PM
				header("Location:pm.php");
				break;
			case 4:	//OPERATIONS
				header("Location:ops.php");
				break;
			case 5:	//Admin
				header("Location:admin.php");
				break;
			case 8:	//CAD Manager
				header("Location:cadManager.php");
				break;
		}
	}
	else
	{
		header("Location:index.php");
	}

	include("phpScript.php");			
	$cadtech_id =  $_SESSION["ID"];
	$sql = "";
	$today = date("F j, Y");
	$date = date("Y-m-d");
	$curr_time;
	$total_time = "";

	
?>

<html>
	<head>
		<title> Activity Monitoring </title>
		<link rel="stylesheet" type="text/css" href = "css/style.css"/>
		<link rel="stylesheet" type="text/css" href = "css/sidebar.css"/>
		<link rel="icon" href="img/logoblue.png">
		<script src="script.js"></script>
		<script src="jquery-3.2.1.min.js"></script>
		<script src="scripts/javaScript.js"></script>
		<style>
       #navbar tr td#tasks{
      background-color: #deebff;
      color:#0747a6;
      }
    body{
        font-family:"Product Sans",sans-serif;
    }
    
  #navbar tr td#timesheet{
    background-color: #0747a6;
    color:#deebff;
  }
      
  #list{
    height:90%;
    width:80%;
    min-height:300px;
    margin-right:0px;
    resize:none;
  }

  .general_table th{
      padding:2px;
  }
  
  .miniheader{
      background-color:#eef1f2;
      margin:0px;
      position:fixed;
      left:0px;
      top:60px;
      width:100%;
      height:100px;
      border-bottom:solid 2px #ececec;
  }
  
  .container2{
      margin-top:130px;
  }
  
  .tbl-tab-group{
      position:fixed;
      padding-left:25px;
      top:115px;
  }
  
  .tbl-tab{
      font-size:16px;
      font-weight:bold;
      display:inline-block;
      background-color:#f7f9fa;
      border-radius:4px;
      padding:10px 17px;
      cursor:pointer;
      filter:brightness(100%);
  }
  
  .tbl-tab:hover{
      filter:brightness(75%);
  }
  
  .active-tbl-tab{
      border-radius:4px 4px 0px 0px;
      background-color:#f7f9fa;
      border-bottom: solid 2px #f7f9fa;
      border-top:solid 2px #ececec;
      border-left:solid 2px #ececec;
      border-right:solid 2px #ececec;
  }
  
  #shortcut_name{
      font-size:30px;
      font-weight:bold;
      margin-left:30px;
  }
  
  #percent_holder{
      position:fixed;
      right:50px;
      top:70px;
  }
  
  #percent_bar{
      border-radius:4px;
      border:solid 4px #515151;
      width:208px;
      height:22px;
      display:inline-block;
      
  }
  
  #progress{
      background-color:red;
      width:0px;
      height:22px;
  }
  
  #percent_number{
      font-size:50px;
  }
  
  #pending_count{
      
  }
</style>
	</head>
	<body onload="initialize();">
		<?php include('header.php')?>
		<div id="mainform">
    <div id="list" class="width-99pc">
    <div class="miniheader">
      <label id="shortcut_name"><?php echo $shortcut_name; ?></label>
      <ul class="tbl-tab-group">
          <li class="tbl-tab active-tbl-tab" id="pending_tab"> Pending <span id="pending_count">(0)</span> </li>
          <li class="tbl-tab" id="working_tab"> Working <span id="working_count">(0)</span> </li>
          <li class="tbl-tab" id="requested_tab"> Requested <span id="requested_count">(0)</span> </li>
          <li class="tbl-tab" id="rejected_tab"> Rejected <span id="rejected_count">(0)</span> </li>
          <li class="tbl-tab" id="approved_tab"> Approved <span id="approved_count">(0)</span> </li>
      </ul>
      <div id="percent_holder">
        <div id="percent_bar">
          <div id="progress"></div>
        </div>
        <label id="percent_number"> 30% </label>
      </div>
    </div>
    <div class="container2">
        <table class="general_table width-100pc">
            <thead>
                <tr>
                    <th> Designer </th>
                    <th> Deadline </th>
                    <th> Project </th>
                    <th> Requirements </th>
                    <th> Budget </th>
                    <th> Ticket </th>
                </tr>
            </thead>
            <tbody id="weeklySchedule">
             <!-- Data will be sent from pushSchedule.php -->
            </tbody>
        </table>			
    </div>
</div>		
		</div>
    <script>
      initCadTracking();
      function initCadTracking(){
      $.ajax({
        type:'get',
        url:'pushSchedule.php',
        data:'dnotif_id='+$('#dnotif_idInfo').val()+
             '&for_cadTech_tasks=true',
        success: function(data)
        {
          $('#weeklySchedule').html(data);
          $('#percent_number').html(computeProgress()+ "%");
        }
      });
      }
      
      function computeProgress(){
       var standby = $('#pending_count').html().replace("(","");
        standby = parseInt(standby.replace(")",""));
       var assigned = $('#working_count').html().replace("(","");
        assigned = parseInt(assigned.replace(")",""));
       var requested = $('#requested_count').html().replace("(","");
        requested = parseInt(requested.replace(")",""));
       var rejected = $('#rejected_count').html().replace("(","");
        rejected = parseInt(rejected.replace(")",""));
       var approved = $('#approved_count').html().replace("(","");
        approved = parseInt(approved.replace(")",""));

        var total = parseInt(standby + assigned + requested + rejected + approved);
       var percent = parseInt(approved * (100/total));
       $('#progress').css("width",percent+"%");
       if(percent <= 15)
       {
         $('#progress').css("background-color","#ed1c26");
       }
       else if(percent <= 75)
       {
         $('#progress').css("background-color","#ffc107");
       }
       else if(percent <= 100)
       {
         $('#progress').css("background-color","#4caf50");
       }
       if(isNaN(percent))
       {
         percent = 0;
       }
       return percent;
 }
      
    function updateReport()
    {
      $.ajax(
              {
                url:'table.php',
                type:'post',
                data:'submit=true',
                success:function(data)
                {
                },
              });
    }

    $(document).ready(function(){
      
      $('#tasks').on('click',function(){
        location.reload();
      });
      
      $('#cad_tracking').on('click',function(){
        var value = $(this).val();
        $.ajax(
        {
          url:'cadtechCadTracking.php',
          type:'post',
          data:'pnumber='+value,
          success:function(data)
          {
            $('#mainform').html(data);
            $.ajax({
              url:'pushSchedule.php',
              type:'get',
              data:'for_cadTech_cadTracking=true',
              success:function(data){
                $('#weeklySchedule').html(data);
              }
            });
          }
        });
      });
    });

$('#pending_tab').on('click',function(){
        $(this).prop("class","tbl-tab active-tbl-tab");
        $('#requested_tab').prop("class","tbl-tab");
        $('#approved_tab').prop("class","tbl-tab");
        $('#working_tab').prop("class","tbl-tab");
        $('#rejected_tab').prop("class","tbl-tab");
        $.ajax({
          url:'cadtechGetData.php',
          type:'post',
          data:'get_cad_tracking=true'+
               '&status=Standby',
          success:function(data){
            $('#weeklySchedule').html(data);
          }
        });
      });
      
      $('#requested_tab').on('click',function(){
        $(this).prop("class","tbl-tab active-tbl-tab");
        $('#pending_tab').prop("class","tbl-tab");
        $('#approved_tab').prop("class","tbl-tab");
        $('#working_tab').prop("class","tbl-tab");
        $('#rejected_tab').prop("class","tbl-tab");
        $.ajax({
          url:'cadtechGetData.php',
          type:'post',
          data:'get_cad_tracking=true'+
               '&status=CADRequested',
          success:function(data){
            $('#weeklySchedule').html(data);
          }
        });
      });
      
      $('#approved_tab').on('click',function(){
        $(this).prop("class","tbl-tab active-tbl-tab");
        $('#pending_tab').prop("class","tbl-tab");
        $('#requested_tab').prop("class","tbl-tab");
        $('#working_tab').prop("class","tbl-tab");
        $('#rejected_tab').prop("class","tbl-tab");
        $.ajax({
          url:'cadtechGetData.php',
          type:'post',
          data:'get_cad_tracking=true'+
               '&status=CADAccepted',
          success:function(data){
            $('#weeklySchedule').html(data);
          }
        });
      });
      
      $('#working_tab').on('click',function(){
        $(this).prop("class","tbl-tab active-tbl-tab");
        $('#pending_tab').prop("class","tbl-tab");
        $('#requested_tab').prop("class","tbl-tab");
        $('#rejected_tab').prop("class","tbl-tab");
        $('#approved_tab').prop("class","tbl-tab");
        $.ajax({
          url:'cadtechGetData.php',
          type:'post',
          data:'get_cad_tracking=true'+
               '&status=Working',
          success:function(data){
            $('#weeklySchedule').html(data);
          }
        });
      });
      
      $('#rejected_tab').on('click',function(){
        $(this).prop("class","tbl-tab active-tbl-tab");
        $('#pending_tab').prop("class","tbl-tab");
        $('#requested_tab').prop("class","tbl-tab");
        $('#working_tab').prop("class","tbl-tab");
        $('#approved_tab').prop("class","tbl-tab");
        $.ajax({
          url:'cadtechGetData.php',
          type:'post',
          data:'get_cad_tracking=true'+
               '&status=CADRejected',
          success:function(data){
            $('#weeklySchedule').html(data);
          }
        });
      });
    </script>
	</body>
</html>