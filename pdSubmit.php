<?php
  session_start();
  include("connect.php");
  include("phpGlobal.php");
  include("pdRead.php");
if(isset($_POST["insert_bnc"]))
{
  $pd_id = $_SESSION["ID"];
  $externaldl_id = $_POST["externaldl_id"];
  $invoice_number = $_POST["invoice_number"];
  $date_endorced_clnt = $_POST["date_endorced_clnt"];
  $sql = "INSERT INTO bnc(
            PD_ID,
            Externaldl_ID, 
            Invoice_Number, 
            Endorced_To_Client)
          VALUES(
            $pd_id,  
            $externaldl_id,
            '$invoice_number',
            '$date_endorced_clnt')";
  if(mysqli_query($conn,$sql))
  {
    $updatesql = "UPDATE external_deadline
                  SET Status = 'Billed'
                  WHERE ID = $externaldl_id";
    mysqli_query($conn,$updatesql);
  }
}

if(isset($_POST["edit_bnc"]))
{
  $bnc_id = $_POST["bnc_id"];
  $or_number = $_POST["or_number"];
  $endorced_to_accounting = $_POST["endorced_to_accounting"];
  $amount_paid = $_POST["amount_paid"];
  $payment_type = $_POST["payment_type"];
  $updatesql = "UPDATE bnc
                SET OR_Number = '$or_number',
                  Endorced_To_Accounting = '$endorced_to_accounting',
                  Amount_Paid = '$amount_paid',
                  Payment_Type = '$payment_type',
                  Status = 'Paid'
                WHERE ID=$bnc_id";
  if(mysqli_query($conn,$updatesql))
  {}
}

if(isset($_POST["request_bnc"]))
{
  $bnc_id = $_POST["bnc_id"];
  $externaldl_id = $_POST["external_id"];
  $sql = "DELETE FROM bnc
          WHERE ID = $bnc_id";
  if(mysqli_query($conn,$sql))
  {
    $updatesql = "UPDATE external_deadline
                  SET Status = 'Done'
                  WHERE ID = $externaldl_id";
    if(mysqli_query($conn,$updatesql))
    {}
  }
}

if(isset($_POST["update_internal"]))
{
  $status = $_POST["status"];
  $internal_id = $_POST["internal_id"];
  $externaldl_id = $_POST["externaldl_id"];
  
  $readextsql = "SELECT ID FROM external_deadline
                WHERE ID = $externaldl_id
                  AND (Status = 'Pending' OR Status = 'Done')";
  
  $extresult = mysqli_query($conn,$readextsql);
  if(mysqli_num_rows($extresult) > 0)
  {
    $updatesql = "UPDATE internal_deadline
                  SET Status = '$status',
                      Actual_Done = '$today_datepicker',
                      Remarks = IF((DATEDIFF(Internal_Deadline, '$today_datepicker') <= 0),'Delayed','On Time')
                  WHERE ID = $internal_id";
    if(mysqli_query($conn,$updatesql))
    {
      $readsql = "SELECT 
                    Status
                  FROM internal_deadline
                  WHERE Externaldl_ID = $externaldl_id";
      $result = mysqli_query($conn,$readsql);
      if(mysqli_num_rows($result) > 0)
      {
        $status = "";
        while($rows = mysqli_fetch_assoc($result))
        {
          if($rows["Status"] == "Pending"){
            $status = "Pending";
            break;
          }
          else if($rows["Status"] == "Done")
          {
            $status = "Done";
          }
        }
        if($status == "Done")
        {
          $updatesql = "UPDATE external_deadline
                        SET Status = '$status',
                            Actual_Done = '$today_datepicker',
                            Remarks = IF((DATEDIFF(External_Deadline, '$today_datepicker') <= 0),'Delayed','On Time')
                        WHERE ID = $externaldl_id";
          mysqli_query($conn,$updatesql);
        }
        else
        {
          $updatesql = "UPDATE external_deadline
                        SET Status = '$status'
                        WHERE ID = $externaldl_id";
          mysqli_query($conn,$updatesql);
        }
      }
    }
  }
}

//update remarks internal deadline
if(isset($_POST["update_remarks"]))
{
  $updatesql = "UPDATE
                    internal_deadline
                INNER JOIN external_deadline ON internal_deadline.Externaldl_ID = external_deadline.ID
                INNER JOIN project ON project.ID = external_deadline.Project_ID
                INNER JOIN team ON project.Team_ID = team.ID
                SET
                    internal_deadline.Actual_Done = '2019-02-08',
                    internal_deadline.Remarks = IF(
                        (
                            DATEDIFF(Internal_Deadline, '2019-02-08') <= 0
                        ),
                        'Delayed',
                        'On Time'
                    )
                WHERE(";  
                  if(isset($_SESSION["team_list_id"]))
                  {
                    $team_list_id=$_SESSION["team_list_id"];
                    $teamlist_count = count($_SESSION["team_list_id"]);
                    for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
                    {
                      $updatesql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
                    }
                      $updatesql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
                  }
                  else
                  {
                    $updatesql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
                  }
              $updatesql.=") and (internal_deadline.Status != 'Done' AND internal_deadline.Status != 'Billed')";
    if(mysqli_query($conn,$updatesql))
    {
    }
}

//update remarks external deadline
if(isset($_POST["update_remarks_external"]))
{
  $updatesql = "UPDATE external_deadline
                  SET Remarks = IF((DATEDIFF(External_Deadline, '$today_datepicker') <= 0),'Delayed','On Time')
                  WHERE (";  
                  if(isset($_SESSION["team_list_id"]))
                  {
                    $team_list_id=$_SESSION["team_list_id"];
                    $teamlist_count = count($_SESSION["team_list_id"]);
                    for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
                    {
                      $sql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
                    }
                      $sql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
                  }
                  else
                  {
                    $sql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
                  }
              $sql.=") AND Status != 'Done' AND Status != 'Billed'";
    if(mysqli_query($conn,$updatesql))
    {
    }
}