<?php
	session_start();
	include('connect.php');
	$tradeString = $_SESSION['Trade'];
	$today = date("F j, Y");
?>

<div id="info">
	<label class="h2"> Schedule Allocations </label>
	<br />
  <label class="h3"><?php echo $today; ?></label>
  <br />
	<button id="view_all" class="btn-normal bg-blue white bold"> View All </button>							 
	<br />
	<br />
	<input style="display:none;" type="text" placeholder="Search" />
	<div id="dname_list">
		<?php
		$sql = "SELECT 
							ID,
							CONCAT(Firstname, ' ' , Middlename ,' ', Lastname) AS Name 
						FROM user 
						WHERE 
							Status = 'Active' 
							AND User_Type = 9  
						ORDER BY Firstname";
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			echo "<table class='general_table width-100pc'><tbody>";
			while($rows = mysqli_fetch_assoc($result))
			{
				echo "<tr class='clickable' onclick='selectCAD".$rows['ID']."();' id='Des".$rows['ID']."'>
								<td><label class='clickable'> ".$rows['Name']." </label>
								<script>
											function selectCAD".$rows['ID']."()
											{
												$('#designerName').html('".$rows['Name']."');
												$.ajax(
													{
													url:'cadGetSchedulle.php',
													type:'post',
													data:'cadtech_id=".$rows['ID']."',
													success:function(data){
														$('#weeklySchedule').html(data);
													},
												});
											}
								</script></td>
							</tr>";
			}
			echo "</tbody></table>";
		}
        ?>
	</div>
</div>
<div id="list">
	<div class="subheader">
		<div class="float-left">
		<img class="prev btn-small" src="img/left_arrow.png" />
		<img class="next btn-small" src="img/left_arrow.png" style="transform: rotate(180deg);" />
		</div>
		<strong class="float-left"> September 2018 </strong>											
		<strong class="float-left margin-l-10pc"><i class="blue"> <span id="designerName">CAD Technician Name</span> </i></strong>
		<div id="utilization" class="float-right">
			<label><b> Total:</b><span id="total">0</span></label>
			<label><b> Billable:</b><span id="billable">0</span></label>
			<label><b> Non-Billable:</b><span id="nonBillable">0</span></label>
			<label><b> Utilization:</b><span id="utilization">0%</span></label>
		</div>
	</div>
	<table class="general_table width-100pc">
		<thead>							
			<tr>
				<th> CAD Technician </th>
				<th> Project </th>
				<th> Activity </th>			
				<th> Deadline </th>
				<th> Date Modified </th>	
				<th> Budget </th>
				<th> Status </th>
				<th> Time In </th>
				<th> Time Out </th>
				<th> Time Used </th>
			</tr>
		</thead>
		<tbody id="weeklySchedule">
			<!-- Data from cadGetSchedule --> 
			<tr>
					<td colspan='8'><div class='note'><label><strong>Note!</strong> Select a <strong>Name</strong> from the list on the left to view designer workloads.</label></div></td>
			</tr>
		</tbody>
	</table>
</div>
<div id="activity_edit_pop_bg" onclick="hidePop()" class="popUpBg bg-gray opacity-7">	
</div>											 
<div id="activity_edit_pop" class="popUp bg-gray width-50pc padding-4px">
	<fieldset class='height-95pc bg-white' style='border:solid 1px #34495e;overflow-y:auto'>
		<legend class='bg-white' style='border-radius:5px;color:#34495e'> Review </legend>
		<h3 class="float-left"> Designer: <span id="pop_designer" class="blue">  </span></h3>
		<h3 class="float-right"> Date: <span id="pop_date" class="blue">  </span></h3>		 
		<h3 class="float-left clear-left"> Project: <span id="pop_project" class="blue">  </span></h3>
		<h3 class="float-right"> Ticket: <span id="pop_ticket" class="blue">  </span></h3>
		<h3 class="clear-left margin-0"> Activity: </h3>
		<textarea class="padding-4px margin-0 lightGray" id="pop_activity" placeholder="Some Activity..." readonly="readonly"></textarea>
		<br />
		<br />
		<h3 class="margin-0"> Comments: </h3>
		<textarea class="padding-4px margin-0 lightGray" id="pop_comments" placeholder="Put comments here..."></textarea>
		<br /><br />
		<table class="width-100pc">
			<thead>
				<tr>
					<th> Accuracy </th>
					<th> Timeliness </th>
					<th> Completeness </th>
					<th style="text-align:left;"> Time In </th>
					<th style="text-align:left;"> Time Out </th>
				</tr>	
		</thead>
			<tbody>
				<tr>
					<td style="text-align:center"> 
						<label for="pop_design_accuracy1" class="clickme"><img src="img/gstar.png" alt="Gray Star" id="pop_design_accuracy1"/></label>	
						<label for="pop_design_accuracy2" class="clickme" ><img src="img/gstar.png" alt="Gray Star" id="pop_design_accuracy2" /></label>
						<label for="pop_design_accuracy3" class="clickme" ><img src="img/gstar.png" alt="Gray Star" id="pop_design_accuracy3" /></label>
						<label for="pop_design_accuracy4" class="clickme" ><img src="img/gstar.png" alt="Gray Star" id="pop_design_accuracy4" /></label>
						<label for="pop_design_accuracy5" class="clickme" ><img src="img/gstar.png" alt="Gray Star" id="pop_design_accuracy5" /></label>
					</td>
					<td style="text-align:center">
						<label for="pop_timeliness1" class="clickme" ><img src="img/gstar.png" alt="Gray Star" id="pop_timeliness1"/></label>	
						<label for="pop_timeliness2" class="clickme" ><img src="img/gstar.png" alt="Gray Star" id="pop_timeliness2" /></label>
						<label for="pop_timeliness3" class="clickme" ><img src="img/gstar.png" alt="Gray Star" id="pop_timeliness3" /></label>
						<label for="pop_timeliness4" class="clickme" ><img src="img/gstar.png" alt="Gray Star" id="pop_timeliness4" /></label>
						<label for="pop_timeliness5" class="clickme"><img src="img/gstar.png" alt="Gray Star" id="pop_timeliness5" /></label>
					</td>
					<td style="text-align:center">
						<label for="pop_completeness1" class="clickme" ><img src="img/gstar.png" alt="Gray Star" id="pop_completeness1" /></label>	
						<label for="pop_completeness2" class="clickme" ><img src="img/gstar.png" alt="Gray Star" id="pop_completeness2" /></label>
						<label for="pop_completeness3" class="clickme" ><img src="img/gstar.png" alt="Gray Star" id="pop_completeness3" /></label>
						<label for="pop_completeness4" class="clickme" ><img src="img/gstar.png" alt="Gray Star" id="pop_completeness4" /></label>
						<label for="pop_completeness5" class="clickme"><img src="img/gstar.png" alt="Gray Star" id="pop_completeness5" /></label>
					</td>									
					<td><input style="width:110px;" id="pop_time_in" type="time" value="09:00" /></td>
					<td><input style="width:110px;" id="pop_time_out" type="time" value="18:00" /></td>
				</tr>	 
				<tr>			 
					<td></td>
					<td></td>
					<td></td>
					<td><button class="btn-normal bg-green white width-100pc" id="pop_save"> Save </button></td>
					<td><button class="btn-normal bg-red white width-100pc" onclick="hidePop();"> Cancel </button></td>
				</tr>
			</tbody>
		</table>
	</fieldset>
</div>
<div class="bgDataHolder display-none" style="position:fixed; left:100px; top:100px; width:300px; height:auto; background-color:white;">	
<label for="bdh_project_id"> Project ID </label>
<input type="text" id="bdh_project_id" />
<label for="bdh_designer_id"> Designer ID </label>
<input type="text" id="bdh_designer_id" />				
<label for="bdh_rating"> Rating </label>	
<input type="text" id="bdh_rating" value="0"/>				
<label for="bdh_da"> Design_Accuracy </label>	
<input type="text" id="bdh_da" value="0"/>				
<label for="bdh_t"> Timeliness </label>	
<input type="text" id="bdh_t" value="0"/>				
<label for="bdh_c"> Completeness </label>	
<input type="text" id="bdh_c" value="0"/>				
<label for="bdh_dnotif_id"> designer_notif ID </label>	
<input type="text" id="bdh_dnotif_id" value="0"/>		
<label for="bdh_activitylogs_id"> activitylogs ID </label>	
<input type="text" id="bdh_activitylogs_id" value="0"/>
<textarea id="query" />				
</div>
<script>
	hidePop();
	
		var design_accuracy = 0;
		var timeliness = 0;
		var completeness = 0;

	function computeRating(da,t,c)
	{
		var rating = Math.round((da + t + c) / 3);
		$('#bdh_rating').val(rating);
	}

	function setStarValue(id,value){
		if(id == '#pop_design_accuracy')
		{
			design_accuracy = value;
			$('#bdh_da').val(value);
		}
		else if(id == '#pop_timeliness')
		{
			timeliness = value;			
			$('#bdh_t').val(value);
		}
		else if(id == '#pop_completeness')
		{
			completeness = value;		
			$('#bdh_c').val(value);
		}
		computeRating(design_accuracy,timeliness,completeness);
	}
	$(document).ready(function(){
		$('#pop_save').on('click',function(){
			$.ajax({
				url:'cadmngrReviewSubmit.php',
				type:'post',
				data:'add_review=true'+	 
							'&cadlogs_id='+$('#bdh_activitylogs_id').val()+
							'&project_id='+$('#bdh_project_id').val()+
							'&ticket_number='+$('#pop_ticket').html()+
							'&cadtech_id='+$('#bdh_designer_id').val()+
							'&comments='+$('#pop_comments').val()+
							'&accuracy='+$('#bdh_da').val()+
							'&timeliness='+$('#bdh_t').val()+
							'&completeness='+$('#bdh_c').val()+
							'&rating='+$('#bdh_rating').val()+
							'&time_in='+$('#pop_time_in').val()+
							'&time_out='+$('#pop_time_out').val()+
							'&cadtechnotif_id='+$('#bdh_dnotif_id').val(),
							success:function(data){
								alert(data);
							 $('#query').html(data);
							 hidePop();
							 var designer_id = $('#bdh_designer_id').val();
							 $('#Des'+designer_id).click();
				}
			});
		});
	});

	$(document).ready(function(){
		$('#view_all').on('click',function(){
		$('#designerName').html("");
			$.ajax({
				url:'cadGetSchedulle.php',
				type:'post',
				data:'cadtech_id=1 OR 1=1',
				success:function(data){
					$('#weeklySchedule').html(data);
				},
			});
		});
	});

	function hidePop()
	{											 
		$(".popUpBg").hide();
		$(".popUp").hide();
		clearPop();
	}
	
	function showPop()
	{											 
		$(".popUpBg").show();
		$(".popUp").show();
	}		

	function clearPop()
	{															 
		$('#pop_designer').html("");
		$('#pop_date').html("");	
		$('#pop_project').html("");
		$('#pop_ticket').html("");
		$('#pop_activity').val("");
		$('#pop_comments').val("");
		clearStar2('#pop_design_accuracy'); 
		clearStar2('#pop_timeliness');
		clearStar2('#pop_completeness');
		design_accuracy = 0;
		timeliness = 0;
		completeness = 0;
	}
	function clearStar2(id){
			$(id+'1').attr('src','img/gstar.png');
			$(id+'2').attr('src','img/gstar.png');
			$(id+'3').attr('src','img/gstar.png');
			$(id+'4').attr('src','img/gstar.png');
			$(id+'5').attr('src','img/gstar.png');
	}
	function clearStar(id){
		if($(id+'1').attr('src') != 'img/ystar.png')
		{
			$(id+'1').attr('src','img/gstar.png');
			$(id+'2').attr('src','img/gstar.png');
			$(id+'3').attr('src','img/gstar.png');
			$(id+'4').attr('src','img/gstar.png');
			$(id+'5').attr('src','img/gstar.png');
		}
	}

		$(document).ready(function(){
			$('#pop_design_accuracy1').on('click',function(){
				star1Click('#pop_design_accuracy');					
			});
			$('#pop_design_accuracy2').on('click',function(){
				star2Click('#pop_design_accuracy');
			});											
			$('#pop_design_accuracy3').on('click',function(){
				star3Click('#pop_design_accuracy');
			});												
			$('#pop_design_accuracy4').on('click',function(){
				star4Click('#pop_design_accuracy');
			});																										
			$('#pop_design_accuracy5').on('click',function(){
				star5Click('#pop_design_accuracy');			
			});

			$('#pop_timeliness1').on('click',function(){
				star1Click('#pop_timeliness');					
			});
			$('#pop_timeliness2').on('click',function(){
				star2Click('#pop_timeliness');
			});											
			$('#pop_timeliness3').on('click',function(){
				star3Click('#pop_timeliness');
			});												
			$('#pop_timeliness4').on('click',function(){
				star4Click('#pop_timeliness');
			});																										
			$('#pop_timeliness5').on('click',function(){
				star5Click('#pop_timeliness');			
			});

			$('#pop_completeness1').on('click',function(){
				star1Click('#pop_completeness');					
			});
			$('#pop_completeness2').on('click',function(){
				star2Click('#pop_completeness');
			});											
			$('#pop_completeness3').on('click',function(){
				star3Click('#pop_completeness');
			});												
			$('#pop_completeness4').on('click',function(){
				star4Click('#pop_completeness');
			});																										
			$('#pop_completeness5').on('click',function(){
				star5Click('#pop_completeness');			
			});
		});

	function star1Click(id){ 
		$(id+'1').attr('src','img/ystar.png');
		$(id+'2').attr('src','img/gstar.png');
		$(id+'3').attr('src','img/gstar.png');
		$(id+'4').attr('src','img/gstar.png');
		$(id+'5').attr('src','img/gstar.png');
		setStarValue(id,1);
	}

	function star2Click(id){						 
		$(id+'1').attr('src','img/ystar.png');
		$(id+'2').attr('src','img/ystar.png');
		$(id+'3').attr('src','img/gstar.png');
		$(id+'4').attr('src','img/gstar.png');
		$(id+'5').attr('src','img/gstar.png'); 
		setStarValue(id,2);
	}

	function star3Click(id){					 			 
		$(id+'1').attr('src','img/ystar.png');
		$(id+'2').attr('src','img/ystar.png');
		$(id+'3').attr('src','img/ystar.png');
		$(id+'4').attr('src','img/gstar.png');
		$(id+'5').attr('src','img/gstar.png'); 
		setStarValue(id,3);
	}

	function star4Click(id){								 
		$(id+'1').attr('src','img/ystar.png');
		$(id+'2').attr('src','img/ystar.png');
		$(id+'3').attr('src','img/ystar.png');
		$(id+'4').attr('src','img/ystar.png');
		$(id+'5').attr('src','img/gstar.png');	
		setStarValue(id,4);
	}

	function star5Click(id){			 		 
		$(id+'1').attr('src','img/ystar.png');
		$(id+'2').attr('src','img/ystar.png');
		$(id+'3').attr('src','img/ystar.png');
		$(id+'4').attr('src','img/ystar.png');
		$(id+'5').attr('src','img/ystar.png');	
		setStarValue(id,5);
	}

	function star1Hover(id){ 
		if($(id+'1').attr('src') != 'img/ystar.png')
		{							 
			$(id+'1').attr('src','img/hstar.png');
			$(id+'2').attr('src','img/gstar.png');
			$(id+'3').attr('src','img/gstar.png');
			$(id+'4').attr('src','img/gstar.png');
			$(id+'5').attr('src','img/gstar.png');
		}
	}

	function star2Hover(id){						 
		if($(id+'1').attr('src') != 'img/ystar.png')
		{							 
			$(id+'1').attr('src','img/hstar.png');
			$(id+'2').attr('src','img/hstar.png');
			$(id+'3').attr('src','img/gstar.png');
			$(id+'4').attr('src','img/gstar.png');
			$(id+'5').attr('src','img/gstar.png');
		}
	}

	function star3Hover(id){					 			 
		if($(id+'1').attr('src') != 'img/ystar.png')
		{							 
			$(id+'1').attr('src','img/hstar.png');
			$(id+'2').attr('src','img/hstar.png');
			$(id+'3').attr('src','img/hstar.png');
			$(id+'4').attr('src','img/gstar.png');
			$(id+'5').attr('src','img/gstar.png');
		}
	}

	function star4Hover(id){	
		if($(id+'1').attr('src') != 'img/ystar.png')
		{							 
			$(id+'1').attr('src','img/hstar.png');
			$(id+'2').attr('src','img/hstar.png');
			$(id+'3').attr('src','img/hstar.png');
			$(id+'4').attr('src','img/hstar.png');
			$(id+'5').attr('src','img/gstar.png');
		}
	}

	function star5Hover(id){			 		 
		if($(id+'1').attr('src') != 'img/ystar.png')
		{							 
			$(id+'1').attr('src','img/hstar.png');
			$(id+'2').attr('src','img/hstar.png');
			$(id+'3').attr('src','img/hstar.png');
			$(id+'4').attr('src','img/hstar.png');
			$(id+'5').attr('src','img/hstar.png');
		}
	}	 

	$(document).ready(function(){
		$('#pop_design_accuracy1').on('mouseover',function(){
				star1Hover('#pop_design_accuracy');					
			});
			$('#pop_design_accuracy2').on('mouseover',function(){
				star2Hover('#pop_design_accuracy');
			});											
			$('#pop_design_accuracy3').on('mouseover',function(){
				star3Hover('#pop_design_accuracy');
			});												
			$('#pop_design_accuracy4').on('mouseover',function(){
				star4hover('#pop_design_accuracy');
			});																										
			$('#pop_design_accuracy5').on('mouseover',function(){
				star5Hover('#pop_design_accuracy');			
			});

			$('#pop_timeliness1').on('mouseover',function(){
				star1Hover('#pop_timeliness');					
			});
			$('#pop_timeliness2').on('mouseover',function(){
				star2Hover('#pop_timeliness');
			});											
			$('#pop_timeliness3').on('mouseover',function(){
				star3Hover('#pop_timeliness');
			});												
			$('#pop_timeliness4').on('mouseover',function(){
				star4Hover('#pop_timeliness');
			});																										
			$('#pop_timeliness5').on('mouseover',function(){
				star5Hover('#pop_timeliness');			
			});

			$('#pop_completeness1').on('mouseover',function(){
				star1Hover('#pop_completeness');					
			});
			$('#pop_completeness2').on('mouseover',function(){
				star2hover('#pop_completeness');
			});											
			$('#pop_completeness3').on('mouseover',function(){
				star3Hover('#pop_completeness');
			});												
			$('#pop_completeness4').on('mouseover',function(){
				star4Hover('#pop_completeness');
			});																										
			$('#pop_completeness5').on('mouseover',function(){
				star5Hover('#pop_completeness');			
			});

			$('#pop_design_accuracy1').on('mouseout',function(){
				clearStar('#pop_design_accuracy');					
			});
			$('#pop_design_accuracy2').on('mouseout',function(){
				clearStar('#pop_design_accuracy');
			});											
			$('#pop_design_accuracy3').on('mouseout',function(){
				clearStar('#pop_design_accuracy');
			});												
			$('#pop_design_accuracy4').on('mouseout',function(){
				clearStar('#pop_design_accuracy');
			});																										
			$('#pop_design_accuracy5').on('mouseout',function(){
				clearStar('#pop_design_accuracy');			
			});

			$('#pop_timeliness1').on('mouseout',function(){
				clearStar('#pop_timeliness');					
			});
			$('#pop_timeliness2').on('mouseout',function(){
				clearStar('#pop_timeliness');
			});											
			$('#pop_timeliness3').on('mouseout',function(){
				clearStar('#pop_timeliness');
			});												
			$('#pop_timeliness4').on('mouseout',function(){
				clearStar('#pop_timeliness');
			});																										
			$('#pop_timeliness5').on('mouseout',function(){
				clearStar('#pop_timeliness');			
			});

			$('#pop_completeness1').on('mouseout',function(){
				clearStar('#pop_completeness');					
			});
			$('#pop_completeness2').on('mouseout',function(){
				clearStar('#pop_completeness');
			});											
			$('#pop_completeness3').on('mouseout',function(){
				clearStar('#pop_completeness');
			});												
			$('#pop_completeness4').on('mouseout',function(){
				clearStar('#pop_completeness');
			});																										
			$('#pop_completeness5').on('mouseout',function(){
				clearStar('#pop_completeness');			
			});
		});
</script>