<?php
  session_start();
  include("connect.php");

  //superAdmin.php - reset_btn.click
  if(isset($_POST["reset_bnc"]))
  {
    $sql = "TRUNCATE TABLE bnc";
    if(mysqli_query($conn,$sql)){
      $updatesql = "UPDATE external_deadline
                    SET Status = 'Done'
                    WHERE Status = 'Billed'";
      mysqli_query($conn,$updatesql);
    }
  }
  
  if(isset($_POST["reset_bnctracking"]))
  {
    $sql = "TRUNCATE TABLE bnc";
    if(mysqli_query($conn,$sql)){
      $updatesql = "UPDATE external_deadline
                    SET Status = 'Pending'";
      mysqli_query($conn,$updatesql);
      $updatesql2 = "UPDATE internal_deadline
                    SET Status = 'Pending',
                      Remarks = ''";
      mysqli_query($conn,$updatesql2);
    }
  }
  
  

  