<?php
  clearstatcache() ;
  session_start();
  include('phpScript.php');
  include('sql.php');
  $user_pic = userPic(getItem("SELECT Picture FROM user WHERE ID = $user_id"));
  $first_name = getItem("SELECT Firstname FROM user WHERE ID = $user_id");
	$middle_name = getItem("SELECT Middlename FROM user WHERE ID = $user_id");
	$last_name = getItem("SELECT Lastname FROM user WHERE ID = $user_id");
  $full_name = $first_name." ".$middle_name." ".$last_name;
  $username = getItem("SELECT Username FROM user WHERE ID = $user_id");
?>
<html>
  <head>
    <title> Activity Monitoring </title>
    <!-- logo -->
    <link rel="icon" href="img/logoblue.png">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="Kiosk/css/materialize.min.css"/>
    <!--Import kiosk.css-->
    <link type="text/css" rel="stylesheet" href="Kiosk/css/kiosk.css"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  
  <body>
    <!-- Navbar -->
    <ul id="dropdown1" class="dropdown-content">
      <li><img src="images/profile_pictures/<?php echo $user_pic; ?>" alt="<?php echo $full_name; ?>" width="150" height="150" class="circle center-block"></li>
      <li><a href="#!" class="blue-text"><?php echo $full_name; ?></a></li>
      <li class="divider"><a href="#!" class="blue-text"><?php echo $full_name; ?></a></li>
      <li><a href="Kiosk/kiosk.php" class="blue-text"><i class="material-icons">developer_board</i>Kiosk</a></li>
      <li class="divider"><a href="#!" class="blue-text"><?php echo $full_name; ?></a></li>
      <li><a href="logout.php" class="blue-text"><i class="material-icons">exit_to_app</i>Logout</a></li>
    </ul>
    <nav class="blue darken-4">
      <div class="nav-wrapper">
        <a href="index.php" class="brand-logo"><img class="responsive-img right-aligned" width="150" src="img/logo.png" alt="acong logo"/></a>
        <ul class="right hide-on-med-and-down">
          <li><a href="#!" ><i class="large material-icons">notifications</i></a></li>
          <li><a href="#!" class="dropdown-trigger" data-target="dropdown1" ><i class="material-icons">arrow_drop_down</i></a></li>
        </ul>
      </div>
    </nav>
    
    <div class="container">
    <!-- form -->
    <form class="col s8" method="post" action="changepass.php" enctype="multipart/form-data">
        <div class="row">
        </div>
        <div class="row">
          <div class="col s1"></div>
          <div class="col s4">
            <div class="row">
            </div>
            <img id="input_user_pic" src="images/profile_pictures/<?php echo $user_pic; ?>" alt="<?php echo $full_name; ?>" width="150" height="150" class="circle center-block">
            <div class="file-field input-field">
              <div class="btn light-blue lighten-1">
                <span>Browse</span>
                <i class="material-icons right">folder_open</i>
                <input type="file" accept="image/*" onchange="(chooseImg('input_user_pic'))" name="input_user_pic">
              </div>
              <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="Upload image">
              </div>
            </div>
          </div>
          <div class="col s5">
          <div class="input-field col s12">
            <input id="first_name" type="text" class="validate" value="<?php echo $first_name;?>" name="Firstname">
            <label for="first_name">First Name</label>
          </div>
          <div class="input-field col s12">
            <input id="middle_name" type="text" class="validate" value="<?php echo $middle_name;?>" name="Middlename">
            <label for="middle_name">Middle Name</label>
          </div>
          <div class="input-field col s12">
            <input id="last_name" type="text" class="validate" value="<?php echo $last_name;?>" name="Lastname">
            <label for="last_name">Last Name</label>
          </div>
          </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
          <div class="col s1"></div>
          <div class="col s9">
          <div class="input-field col s6">
            <i class="material-icons prefix">account_circle</i>
            <input id="user_name" type="text" class="validate" value="<?php echo $username;?>" name="Username">
            <label for="user_name">User Name</label>
          </div>
          <div class="input-field col s6">
            <i class="material-icons prefix">lock_open</i>
            <input id="user_pass" type="password" class="validate" name="password">
            <label for="user_pass">Password</label>
          </div>
          <div class="row">
            <button class="btn waves-effect waves-light teal lighten-1 right" type="submit" name="action" value="update">Submit
              <i class="material-icons right">send</i>
            </button>
          </div>
          </div>
        </div>
      </form>
    </div>
    <!--JavaScript at end of body for optimized loading-->
		<script src="jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="Kiosk/js/materialize.min.js"></script>
    <script type="text/javascript" src="Kiosk/js/kiosk.js"></script>
    <script type="text/javascript" src="scripts/javaScript.js"></script>
  </body>
</html>