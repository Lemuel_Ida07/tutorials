<?php
	session_start();
	include("connect.php");
	$curr_date = date("Y-m-d");
	$cadtech_id = $_POST["cadtech_id"];
	$trade = $_SESSION["Trade"];
	//cad manager
	if($_SESSION["User_Type"] == 8)
	{
		$sql = "SELECT CONCAT(user.Firstname , ' ' , user.Middlename , ' ' , user.Lastname) AS CadTech,
									user.Trade,
									project.ID AS Project_ID,
									project.Project_Number,
									project.Project_Name,
									cadreview_logs.ID,	 
									cadreview_logs.Ticket_Number,
									cadreview_logs.Review_Date,
									cadreview_logs.Comments,
									cadreview_logs.Accuracy,
									cadreview_logs.Timeliness,
									cadreview_logs.Completeness,
									cad_logs.Time_In,
									cad_logs.Time_Out,
									cad_logs.Activity
						FROM cadreview_logs
							INNER JOIN project
								ON project.ID = cadreview_logs.Project_ID
							INNER JOIN user
								ON user.ID = cadreview_logs.CadTech_ID
							INNER JOIN cad_logs
								ON cad_logs.ID = cadreview_logs.CadLogs_ID
						WHERE cadreview_logs.CadTech_ID = $cadtech_id 
						ORDER BY cadreview_logs.CadTech_ID";
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			while($rows = mysqli_fetch_assoc($result))
			{
				echo "<tr>															
								<td> ".$rows["CadTech"]." </td>
								<td> ".$rows["Project_Number"]." ".$rows["Project_Name"]." </td> 	
								<td> ".$rows["Ticket_Number"]." </td>	 
								<td> ".$rows["Comments"]." </td>	 
								<td> ".$rows["Accuracy"]." </td>
								<td> ".$rows["Timeliness"]." </td>
								<td> ".$rows["Accuracy"]." </td>
								<td> ".$rows["Completeness"]." </td>	
								<td> ".$rows["Review_Date"]." </td>
								<td> ".$rows["Time_In"]." </td>
								<td> ".$rows["Time_Out"]." </td>
							</tr>";
			}
		}
		else
		{
			echo "<tr>
							<td colspan='11' style='text-align:center;'><label>No Results! </label></td>

						</tr>";
		}
					
	}	
?>