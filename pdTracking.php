<?php
  session_start();
  include("connect.php");
  include('phpGlobal.php');

  $today = date("F j, Y");
  $shortcut_name = $_SESSION["Lastname"].", ".$_SESSION["Firstname"];
  $user_id = $_SESSION["ID"];
?>
<style>
  #navbar tr td#cad_tracking{
    background-color: #deebff;
    color:#0747a6;
  }
    
  #navbar tr td#timesheet,#navbar tr td#des_tracking{
    background-color: #0747a6;
    color:#deebff;
  }
  
  #list{
      height:90%;
      width:80%;
      min-height:300px;
      margin-right:0px;
      resize:none;
  }
  
  .general_table th{
      padding:2px;
  }
  
  body{
      font-family:"Product Sans",sans-serif;
  }
    
      
  #list{
    height:90%;
    width:80%;
    min-height:300px;
    margin-right:0px;
    resize:none;
  }

  .general_table th{
      padding:2px;
  }
  
  .miniheader{
      background-color:#eef1f2;
      margin:0px;
      position:fixed;
      left:0px;
      top:60px;
      width:100%;
      height:100px;
      border-bottom:solid 2px #ececec;
  }
  
  .container2{
      margin-top:130px;
  }
  
  .tbl-tab-group{
      position:fixed;
      padding-left:25px;
      top:115px;
  }
  
  .tbl-tab{
      font-size:16px;
      font-weight:bold;
      display:inline-block;
      background-color:#f7f9fa;
      border-radius:4px;
      padding:10px 17px;
      cursor:pointer;
      filter:brightness(100%);
  }
  
  .tbl-tab:hover{
      filter:brightness(75%);
  }
  
  .active-tbl-tab{
      border-radius:4px 4px 0px 0px;
      background-color:#f7f9fa;
      border-bottom: solid 2px #f7f9fa;
      border-top:solid 2px #ececec;
      border-left:solid 2px #ececec;
      border-right:solid 2px #ececec;
  }
  
  #shortcut_name{
      font-size:30px;
      font-weight:bold;
      margin-left:30px;
  }
  
  #percent_holder{
      position:fixed;
      right:50px;
      top:70px;
  }
  
  #percent_bar{
      border-radius:4px;
      border:solid 4px #515151;
      width:208px;
      height:22px;
      display:inline-block;
      
  }
  
  #progress{
      background-color:red;
      width:0px;
      height:22px;
  }
  
  #done_rate{
      font-size:50px;
  }
  
  #pending_count{
      
  }
  
  #bnc_okbtn{
    display:none;
  }
  
  #percent_holder{
    display:none;
  }
  
  #th_progress,#th_actualdone{
    display:none;
  }
  #th_action{
      display:block;
  }
</style>

<div id="list" class="width-99pc">
    <div class="miniheader">
      <label id="shortcut_name"><?php echo $shortcut_name; ?></label>
      <ul class="tbl-tab-group">
          <li class="tbl-tab active-tbl-tab" id="pendint_tab"> Pending Internal <span id="pendint_count">(0)</span> </li>
          <li class="tbl-tab" id="pendext_tab"> Pending External <span id="pendext_count">(0)</span> </li>
          <li class="tbl-tab" id="doneint_tab"> Done Internal <span id="doneint_count">(0)</span> </li>
          <li class="tbl-tab" id="doneext_tab"> Done External <span id="doneext_count">(0)</span> </li>
      </ul>
      <div id="percent_holder">
        <label id="done_rate"> 0/2 Hit Rate 0% </label>
      </div>
    </div>
    <div class="container2">
        <table class="general_table width-100pc" id="general_table">
            <thead>
              <tr>
                <th id="th_proj"> Project </th>
                <th id="th_internal"> Internal Deadline </th>
                <th id="th_actualdone"> Actual </th>
                <th id="th_external"> External Deadline </th>
                <th id="th_team"> Team </th>
                <th id="th_trade"> Trade </th>
                <th id="th_progress"> Done Internal </th>
                <th id="th_action"> Action </th>
                <th id="th_remarks"> Remarks </th>
              </tr>
            </thead>
            <tbody id="weeklySchedule">
             <!-- Data will be sent from pdRead.php -->
             <?php
              $readsql ="SELECT 
                    internal_deadline.ID,
										project.Project_Name,
										project.Team_ID,
										project.Project_Number,
										external_deadline.Day_ED,
										external_deadline.Month_ED,
										external_deadline.Year_ED,	
                    external_deadline.External_Deadline,
                    external_deadline.ID AS exid,
										internal_deadline.Externaldl_ID,
										internal_deadline.Trade,
										internal_deadline.Phase, 
										internal_deadline.Day_ID,
										internal_deadline.Month_ID,
										internal_deadline.Year_ID,
										internal_deadline.Ticket_Number,
                    internal_deadline.Remarks,
										team.Team_Name,
                    internal_deadline.Internal_Deadline
									FROM project
									INNER JOIN external_deadline 
										ON project.ID = external_deadline.Project_ID
									INNER JOIN internal_deadline 
										ON external_deadline.ID = internal_deadline.Externaldl_ID
									LEFT JOIN team 
										ON project.Team_ID = team.ID
									WHERE (";  
          if(isset($_SESSION["team_list_id"]))
          {
            $team_list_id=$_SESSION["team_list_id"];
            $teamlist_count = count($_SESSION["team_list_id"]);
            $tl_count = 0;
            for($tl_count = 0; $tl_count <= $teamlist_count - 2; $tl_count++)
            {
              $readsql.="project.Team_ID = ".$team_list_id[$tl_count]." OR ";
            }
              $readsql.="project.Team_ID = ".$team_list_id[sizeof($_SESSION["team_list_id"]) - 1]." ";
          }
          else
          {
            $readsql.="project.Team_ID = ".$_SESSION['Team_ID']." ";					  
          }
          $readsql.=") AND internal_deadline.Status = 'Pending'";
              $result = mysqli_query($conn,$readsql);
              if(mysqli_num_rows($result) > 0)
              {
                while($rows = mysqli_fetch_assoc($result)){
                  echo "<tr>
                          <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                          <td> ".$rows["Internal_Deadline"]." </td>
                          <td> ".$rows["External_Deadline"]." </td>
                          <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>
                          <td> ".$rows["Trade"]." </td>
                          <td>
                            <input type='button' class='btn-normal white bg-blue' value='Done' id='done".$rows["ID"]."'>
                            <script>
                              $('#done".$rows["ID"]."').on('click',function(){
                                $.ajax({
                                  beforeSend:function(){
                                    return confirm('Are you sure?');
                                  },
                                  url:'pdSubmit.php',
                                  type:'post',
                                  data:'update_internal=true'+
                                    '&status=Done'+
                                    '&internal_id=".$rows["ID"]."'+
                                    '&externaldl_id=".$rows["exid"]."',
                                  success:function(data){
                                    updateTracking();
                                  }
                                });
                              });
                            </script>
                          </td>";
                          if($rows["Remarks"] == "Delayed")
                          {
                            echo "<td class='red bold'> ".$rows["Remarks"]." </td>";
                          }
                          else if($rows["Remarks"] == "On Time")
                          {
                            echo "<td class='green bold'> ".$rows["Remarks"]." </td>";
                          }
                        echo"</tr>";
                }
              }
             ?>
            </tbody>
        </table>			
    </div>
</div>			

<div id="pop_bnc_bg">
</div>

<div id="pop_bnc">
    <input type="text" id="bnc_id" style="display:none;" />
    <label class="title"> Project: </label>
    <span id="bnc_project">  </span>
    <br /><br />
    <label class="title"> External Deadline: </label>
    <span class="red" id="bnc_external_deadline">  </span>
    <br /><br />
    <label class="title"> Date Endorced to Client: </label>
    <span class="blue" id="bnc_endorced_client"></span>
    <br /><br />
    <label class="title"> OR No. </label>
    <input type="text" id="bnc_or_no" placeholder="OR Number" />
    <label class="title"> Date Endorced to Accounting. </label>
    <input type="date" class="classy-date" id="bnc_endorced_date"/>
    <br /><br />
    <input type="button" class="btn-normal white bg-green width-45pc" value="OK" id="bnc_okbtn"/>
    <input type="button" class="btn-normal white bg-red width-45pc" value="Cancel" id="bnc_cancelbtn" />
</div>

<div id="pop_billing_bg"></div>
   <div id="pop_billing">
       <label class="title"> Project: </label>
       <div id="filtered_select">
         <input type="text" id="billing_proj_id" style="display:none;"/>
         <input type="text" id="billing_externaldl_id" style="display:none;"/>
         <input type="text" placeholder="Project" id="billing_proj" readonly="readonly"/>
       </div>
       <label class="title"> Submission Form No. </label>
       <input type="text" id="billing_invoice_no" placeholder="Submission Form No." />
       <label class="title"> Date Endorced to Client. </label>
       <input type="date" class="classy-date" id="billing_endorced_date" value="<?php echo $today_datepicker;?>"/>
       <br /><br />
       <input type="button" class="btn-normal white bg-green width-45pc" value="OK" id="billing_okbtn"/>
       <input type="button" class="btn-normal white bg-red width-45pc" value="Cancel" id="billing_cancelbtn" />
   </div>
<script>
  
  init();
  
  //pending Internal
    $('#pendint_tab').on('click',function(){
      updatePendingInternal();
    });
  
    //Done Internal
    $('#doneint_tab').on('click',function(){
      updateDoneInternal();
    });
    
    //Done External
    $('#doneext_tab').on('click',function(){
      updateDoneExternal();
    });
      
    //Pending External
    $('#pendext_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#pendint_tab').prop("class","tbl-tab");
      $('#doneint_tab').prop("class","tbl-tab");
      $('#doneext_tab').prop("class","tbl-tab");
      $('#th_internal').fadeOut();
      $('#th_trade').fadeOut();
      $('#th_actualdone').fadeOut();
      $('#th_remarks').fadeIn();
      $('#th_action').fadeOut();
      $('#percent_holder').fadeOut();
      $('#th_progress').fadeIn();
      $.ajax({
        url:'pdRead.php',
        type:'post',
        data:'pending_external=true',
        success:function(data){
          $('#weeklySchedule').html(data);
          updateRemarksExternal();
        }
      });
    });
    
    //pop cancel
    $('#billing_cancelbtn').on('click',function(){
      hideBilling();
    });
    
    //pop ok
    $('#billing_okbtn').on('click',function(){
      $.ajax({
        beforeSend:function(){
          return confirm('Are you sure you want to save?');
        },
        url:'pdSubmit.php',
        type:'post',
        data:'insert_bnc=true'+
          '&externaldl_id='+$('#billing_externaldl_id').val()+
          '&invoice_number='+$('#billing_invoice_no').val()+
          '&date_endorced_clnt='+$('#billing_endorced_date').val(),
        success:function(){
          updateDoneExternal();
        },
        error:function(data){
          alert(data);
        }
      });
    });
    
    function init(){
      updateRemarks();
    }
    
    function updatePendingInternal(){
      $('#pendint_tab').prop("class","tbl-tab active-tbl-tab");
      $('#pendext_tab').prop("class","tbl-tab");
      $('#doneint_tab').prop("class","tbl-tab");
      $('#doneext_tab').prop("class","tbl-tab");
      $('#th_progress').fadeOut();
      $('#th_actualdone').fadeOut();
      $('#percent_holder').fadeOut();
      $('#th_internal').fadeIn();
      $('#th_trade').fadeIn();
      $('#th_action').fadeIn();
      $('#th_remarks').fadeIn();
      $.ajax({
        url:'pdRead.php',
        type:'post',
        data:'pending_internal=true',
        success:function(data){
          $('#weeklySchedule').html(data);
          updateRemarks();
        }
      });
    }
    
    function updateDoneExternal(){
      $('#doneext_tab').prop("class","tbl-tab active-tbl-tab");
      $('#th_internal').fadeOut();
      $('#th_trade').fadeOut();
      $('#pendext_tab').prop("class","tbl-tab");
      $('#pendint_tab').prop("class","tbl-tab");
      $('#doneint_tab').prop("class","tbl-tab");
      $('#th_actualdone').fadeIn();
      $('#th_remarks').fadeOut();
      $('#th_action').fadeOut();
      $('#th_progress').fadeIn();
      $('#th_progress').fadeOut();
      $.ajax({
        url:'pdRead.php',
        type:'post',
        data:'done_external=true',
        success:function(data){
          $('#percent_holder').fadeOut();
          $('#weeklySchedule').html(data);
          $('#done_rate').html(getHitRateExternal());
          hideBilling();
          $('#percent_holder').fadeIn();
        }
      });
    }
    
    function updateTracking(){
      countTracking();
      updatePendingInternal();
    }
    
    function updateDoneInternal(){
      countTracking();
      $('#doneint_tab').prop("class","tbl-tab active-tbl-tab");
      $('#th_internal').fadeIn();
      $('#th_trade').fadeIn();
      $('#pendext_tab').prop("class","tbl-tab");
      $('#pendint_tab').prop("class","tbl-tab");
      $('#doneext_tab').prop("class","tbl-tab");
      $('#th_actualdone').fadeIn();
      $('#th_remarks').fadeIn();
      $('#th_progress').fadeOut();
      $('#th_action').fadeIn();
      $.ajax({
        url:'pdRead.php',
        type:'post',
        data:'done_internal=true',
        success:function(data){
          $('#percent_holder').fadeOut();
          $('#weeklySchedule').html(data);
          $('#done_rate').html(getHitRate());
          $('#percent_holder').fadeIn();
        }
      });
    }
    
    function showBilling(){
      $('#pop_billing_bg').fadeIn();
      $('#pop_billing').slideDown();
    }
    
    function hideBilling(){
      $('#pop_billing_bg').fadeOut();
      $('#pop_billing').slideUp();
    }
    // for internal
    function getHitRate(){
      var pending_internal = $('#pendint_count').html();
      var pendint_count = pending_internal.length;
      pending_internal = parseInt(pending_internal.slice(1,pendint_count - 1));
      var done_internal = $('#doneint_count').html();
      var doneint_count = done_internal.length;
      done_internal = parseInt(done_internal.slice(1,doneint_count - 1));
      var total = 0;
      var hit = 0;
      var percent = 0;
      var count = 0;
      $('#weeklySchedule tr').each(function(){
        var remarks =document.getElementById("weeklySchedule").rows[count].cells[7].innerHTML;
        remarks = remarks.trim();
        if(remarks == "On Time")
        {
          hit ++;
        }
        count++; 
      });
      total = pending_internal + done_internal;
      percent = Math.round((hit / pending_internal) * 100);
      var hitrate = "<span class='red'>" + hit + "</span>/" + total + " Hit Rate <span class='blue'>" + percent + "%</span>"; 
      return hitrate;
    }
    // for external
    function getHitRateExternal(){
      var pending_external = $('#pendext_count').html();
      var pendint_count = pending_external.length;
      pending_external = parseInt(pending_external.slice(1,pendint_count - 1));
      var done_external = $('#doneext_count').html();
      var doneext_count = done_external.length;
      done_external = parseInt(done_external.slice(1,doneext_count - 1));
      var total = 0;
      var hit = 0;
      var percent = 0;
      var count = 0;
      $('#weeklySchedule tr').each(function(){
        var remarks =document.getElementById("weeklySchedule").rows[count].cells[5].innerHTML;
        remarks = remarks.trim();
        if(remarks == "On Time")
        {
          hit ++;
        }
        count++; 
      });
      total = pending_external + done_external;
      percent = Math.round((hit / pending_external) * 100);
      var hitrate = "<span class='red'>" + hit + "</span>/" + total + " Hit Rate <span class='blue'>" + percent + "%</span>"; 
      return hitrate;
    }
    // for internal
    function updateRemarks(){
      $.ajax({
        url:'pdSubmit.php',
        type:'post',
        data:'update_remarks=true',
        success:function(){
        }
      });
    }
    // for external
    function updateRemarksExternal(){
      $.ajax({
        url:'pdSubmit.php',
        type:'post',
        data:'update_remarks_external=true',
        success:function(){
        }
      });
    }
</script>
