<?php
	session_start();
	include('connect.php');
	
	$today = date("F j, Y");
	$curr_date = date("Y-m-d");	
	$tradeString = $_SESSION['Trade'];
?>
	<div id="info">
			<label class="h2">My Daily Review Summary </label>
      <br />
			<label class="h3"><?php echo $today; ?></label>					 
			<br />
			<br />
			
		</div>
		<div id="list">
			<table  class="general_table width-100pc">
				<thead>							 
					<th> Designer </th>
					<th> Project </th>
					<th> TIcket </th>
					<th> Comments </th>
					<th> Design Accuracy </th>
					<th> Timeliness </th>
					<th> Completeness </th>
					<th> Rating </th>
					<th> Review Date </th>
					<th> Time In </th>
					<th> Time Out </th>
				</thead>	
				<tbody id="review_list">		
					<!-- data from getReviews.php-->
					<tr>
						<td colspan='11'><div class='note'><label><strong>Note!</strong> Select a <strong>Name</strong> from the list on the left to view reviews.</label></div></td>
					</tr>
				</tbody>
			</table>
		</div>