<?php
	session_start();
	include('connect.php');
	include('phpScript.php');
	$trade = $_SESSION["Trade"];
	$curr_month = date("m");
	$curr_year = date("Y");

	$cadtech_id = $_POST["cadtech_id"];
	$sql = "SELECT CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Name,
								 project.ID AS Project_ID,
								 project.Project_Number,
								 project.Project_Name,
								 activity_logs.Activity,
								 activity_logs.Ticket_Number,
								 cadtech_notif.Deadline,
								 cad_logs.Date AS DateModified,
								 cadtech_notif.ID AS cadtechnotif_id,
								 cadtech_notif.Budget,
								 cadtech_notif.Status,
								 cad_logs.CadTech_ID,
								 cad_logs.ID,
								 cad_logs.Time_In,
								 cad_logs.Time_Out,
								 cad_logs.Duration AS Time_Used
					FROM cad_logs
					RIGHT JOIN cadtech_notif
						ON cad_logs.CTNotif_ID = cadtech_notif.ID
					INNER JOIN project 
						ON cadtech_notif.Project_ID = project.ID
					INNER JOIN user
						ON cadtech_notif.CadTech_ID = user.ID
                    LEFT JOIN review_logs
                    	ON cadtech_notif.ReviewLogs_ID = review_logs.ID
                    LEFT JOIN activity_logs
                    	ON review_logs.ActivityLogs_ID = activity_logs.ID
					WHERE cadtech_notif.CadTech_ID = $cadtech_id
            AND MONTH(cadtech_notif.Deadline) = MONTH(CURDATE())
            AND YEAR(cadtech_notif.Deadline) = YEAR(CURDATE())
	        ORDER BY cad_logs.Date";
	$result = mysqli_query($conn,$sql);
	if(mysqli_num_rows($result) > 0){
			while($rows = mysqli_fetch_assoc($result))
			{
				if(!($rows["Status"] == "For Drafting"))
				{
					echo "<tr>
									<td> ".$rows["Name"]." </td>
									<td> ".$rows["Project_Number"]." ".$rows["Project_Name"]."</td>";
						switch($rows["Status"])
						{
							case "Standby":
									echo "<td> ".$rows["Activity"]." </td>
												<td> ".$rows["Deadline"]." </td>
												<td> ".$rows["DateModified"]." </td>
												<td> ".$rows["Budget"]." hrs </td>
												<td> ".$rows["Status"]." </td>
												<td> ".$rows["Time_In"]." </td>
												<td> ".$rows["Time_Out"]." </td>";
								break;
							case "In Progress":
									echo "<td> ".$rows["Activity"]." </td> 
												<td> ".$rows["Deadline"]." </td>
												<td> ".$rows["DateModified"]." </td>
												<td> ".$rows["Budget"]." hrs </td>
												<td class='yellow bold'> ".$rows["Status"]." </td>
												<td> ".$rows["Time_In"]." </td>
												<td> ".$rows["Time_Out"]." </td>";
								break;
							case "Done":
									echo "<td> ".$rows["Activity"]." </td>		
												<td> ".$rows["Deadline"]." </td>
												<td> ".$rows["DateModified"]." </td>
												<td> ".$rows["Budget"]." hrs </td>
												<td class='blue bold'> ".$rows["Status"]."</td>
												<td> ".$rows["Time_In"]." </td>
												<td> ".$rows["Time_Out"]." </td>";
								break;																						 
							case "On Review":
									echo "<td> ".$rows["Activity"]." </td>	
												<td> ".$rows["Deadline"]." </td>
												<td> ".$rows["DateModified"]." </td>
												<td> ".$rows["Budget"]." hrs </td>
												<td class='green bold'>
													<button class='btn-normal bg-blue white bold' onclick='showPop();fillPop".$rows["ID"]."()'> Accept </button>
													<button class='btn-normal bg-red white bold' onclick='incomplete".$rows["ID"]."()'> Incomplete </button>
												</td>
												<td> ".$rows["Time_In"]." </td>
												<td> ".$rows["Time_Out"]." </td>";
								break;
							case "Hold":
									echo "<td> ".$rows["Activity"]." </td> 
												<td> ".$rows["Deadline"]." </td>
												<td> ".$rows["DateModified"]." </td>
												<td> ".$rows["Budget"]." hrs </td>
												<td class='bold'> ".$rows["Status"]."</td>
												<td> ".$rows["Time_In"]." </td>
												<td> ".$rows["Time_Out"]." </td>";
								break;
							case "Incomplete":
									echo "<td> ".$rows["Activity"]." </td>		
												<td> ".$rows["Deadline"]." </td>
												<td> ".$rows["DateModified"]." </td>
												<td> ".$rows["Budget"]." hrs </td>
												<td class='red bold'> ".$rows["Status"]."</td>
												<td> ".$rows["Time_In"]." </td>
												<td> ".$rows["Time_Out"]." </td>";
								break;
							case "Lapsed":
									echo "<td> ".$rows["Activity"]." </td>		 
												<td> ".$rows["Deadline"]." </td>
												<td> ".$rows["DateModified"]." </td>
												<td> ".$rows["Budget"]." hrs </td>
												<td class='red bold'> ".$rows["Status"]."</td>
												<td> ".$rows["Time_In"]." </td>
												<td> ".$rows["Time_Out"]." </td>";
								break;			
						}

					echo "<script>
									function fillPop".$rows["ID"]."(){
										$('#pop_designer').html('".$rows["Name"]."');
										$('#pop_date').html('".$rows["DateModified"]."');	
										$('#pop_project').html('".$rows["Project_Number"]." ".$rows["Project_Name"]."');
										$('#pop_ticket').html('".$rows["Ticket_Number"]."');
										$('#pop_activity').val('".$rows["Activity"]."');
										fillBGH".$rows["ID"]."();
									}
									function fillBGH".$rows["ID"]."()
									{
										$('#bdh_project_id').val('".$rows["Project_ID"]."');	 
										$('#bdh_designer_id').val('".$rows["CadTech_ID"]."');			
										$('#bdh_dnotif_id').val('".$rows["cadtechnotif_id"]."');	
										$('#bdh_activitylogs_id').val('".$rows["ID"]."');
									}
									
									function incomplete".$rows["ID"]."()
									{
										$.ajax(
										{
											url:'cadmngrReviewSubmit.php',
											type:'post',
											data:'incomplete=true&cadtechnotif_id=".$rows["cadtechnotif_id"]."',
											success:function(data)
											{
												 alert('Incomplete');
											}
										});	
									}
								
								</script>
								<td> ".$rows["Time_Used"]." </td>
							</tr>";
				}
			}
	}
	else
	{
		echo "<tr>
					<td colspan='8'><label>No Results!</label></td>
			</tr>";
	}
	checkCADStatus();
?>