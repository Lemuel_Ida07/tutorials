
<?php
session_start();
if(isset($_SESSION["User_Type"]))
{
	switch($_SESSION["User_Type"])
	{
		case 0:	//Designer
			header("Location:home.php");
			break;
		case 1:	//Reviewer
			header("Location:reviewer.php");
			break;
		case 2:	//DC
			header("Location:dc.php");
			break;
		case 3:	//PM
			header("Location:pm.php");
			break;
		case 5:	//Admin
			header("Location:admin.php");
			break;
	}
}
else
{
	header("Location:index.php");
}

include('connect.php');
include('phpGlobal.php');

	$today = date("F j, Y");
	$sql = "";
	
	if(isset($_POST['add_project']))
	{
		$project_number = $_POST['ProjectNumber'];
		$project_name = $_POST['ProjectName'];
		//$ticket_number = $_POST['TicketNumber'];
		$team_id = $_POST['team_id'];
		$sql = "select Project_Number from project where Project_Number = '$project_number'";
		$result = mysqli_query($conn,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			echo "<script> alert('Project number ($project_number) already exists!'); </script>";
		}
		else
		{
			$sql = "insert into project (Project_Number,Project_Name,Team_ID) values('$project_number','$project_name','$team_id')";
			if(mysqli_query($conn,$sql))
			{
				echo "<script> alert('Project $project_name successfuly added!'); </script>";
			}
		}
	}
	
	if(isset($_POST['add_user']))
	{
		$user_username = $_POST['dusername'];
		$user_fname = $_POST['fname'];
		$user_mname = $_POST['mname'];
		$user_lname = $_POST['lname'];
		$user_teamid = $_POST['add_team_id'];
		$user_user_type = $_POST['add_user_type'];
		$user_trade = $_POST['add_trade'];
		
		$sql = "select Username from user where Username = '$user_username'";
		$result = mysqli_query($conn,$sql);
		$sql2 = "select Firstname,Middlename,Lastname from user where Firstname = '$user_fname' AND Middlename = '$user_mname' AND Lastname = '$user_lname' ";
		$result2 = mysqli_query($conn,$sql2);
		if(mysqli_num_rows($result) > 0)
		{
			echo "<script> alert('($user_username) Username already exists!'); </script>";
		}
		elseif(mysqli_num_rows($result2) > 0) 
		{
			echo "<script> alert('Name already exists!'); </script>";
		}
		else
		{	
			$sql = "INSERT INTO user(
								Username,
								Firstname,
								Middlename,
								Lastname,
								User_Type,
								Team_ID,
								Trade) 
							VALUES(
								'$user_username',
								'$user_fname',
								'$user_mname',
								'$user_lname',	
								$user_user_type,
								'$user_teamid',
								'$user_trade')";
			if(mysqli_query($conn,$sql))
			{echo "<script> alert('User $user_fname $user_mname $user_lname successfuly added!'); </script>";}
		}
	}
	
	if(isset($_POST['edit_user']))
	{
		$user_id = $_POST['editid'];
		$user_username = $_POST['editdusername'];
		$user_fname = $_POST['editdfname'];
		$user_mname = $_POST['editdmname'];
		$user_lname = $_POST['editdlname'];
		$user_user_type = $_POST['edituser_type'];
		$user_team = $_POST['editteam'];
		$user_trade = $_POST['edittrade'];
		$user_status = $_POST['editstatus'];
		$sql = "UPDATE user 
						SET Username = '$user_username',
								Firstname = '$user_fname',
								Middlename = '$user_mname',
								Lastname = '$user_lname',
								User_Type = $user_user_type,
								Team_ID = $user_team,
								Trade = '$user_trade',
								Status = '$user_status' 
						WHERE ID = '$user_id'";
		echo "<script>alert('$sql');</script>";		
		if(mysqli_query($conn,$sql))
		{
		}
	}
	
	if(isset($_POST['edit_project']))
	{
    $project_number = $_POST["editproject_number"];
		$project_id = $_POST['editproject_id'];
		$team = $_POST["editteam"];
		$remarks = $_POST['remarks'];
    $amount = $_POST['editamount'];
    $project_name = $_POST["editprojname"];
		$sql = "UPDATE project 
							SET Remarks = '$remarks',
									Team_ID = $team,
                  Amount = '$amount',
                  Project_Name = '$project_name',
                  Project_Number = '$project_number'
						WHERE 
							ID = $project_id";
		if(mysqli_query($conn,$sql))
		{}
	}

	function dateDiff($start, $end) {

		$start_ts = strtotime($start);

		$end_ts = strtotime($end);

		$diff = $end_ts - $start_ts;

		return round($diff / 86400);
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title> Activity Monitoring </title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/calendar.css" />
	<link rel="icon" href="img/logoblue.png">
	
	<script src="jquery-3.2.1.min.js"></script>
	<script src="script.js"></script>
	<script src="scripts/javaScript.js"></script>
	<script src="scripts/internal_deadline_computation.js"></script>
	<script src="scripts/calendarPhp.js"></script>
  
  <style>
      
    #info{
      position:fixed;
      float:right;
      left:4px;
    }
    
    #list{
      position:absolute;
      right:10px;
    }
    
    #navbar tr td#projects{
      background-color:#f7f9fa;
      color:#0747a6;
      border-color:#0747a6;
      box-shadow: inset 0px 2px 2px gray;
    }
    
    #activity_edit_pop_bg,#activity_edit_pop{
      display:none;
    }
     
  </style>
</head>
<body>
	<?php include('header.php'); ?>
	<div onclick="hideElement('logout','edit_password')" id="mainform">
		<div id="info">
			<label class="h2"><?php echo $today; ?></label>
      <br />
			<label class="title"> New Project </label>
			<form method="post" enctype="form/multipart">
				<textarea name="ProjectName" rows="2" cols="20" placeholder="Project Name" required="required"></textarea>
				<input type="text" id="projectnumber" name="ProjectNumber" placeholder="Project Number" required /><br />
				<select name="team_id" class="dropdown" required="required">
					<option> - Select Team - </option><?php
					$sql = "SELECT * FROM team";
					$result = mysqli_query($conn,$sql);
					if(mysqli_num_rows($result) > 0)
					{
            while($rows = mysqli_fetch_assoc($result))
            {
              echo "<option value='".$rows['ID']."'> ".$rows['Team_Name']." </option>";
            }
					}
					?>
				</select>
				<input type="submit" value="Add Project" name="add_project" id="addbtn" style="margin-left:15%; width:70%" />
			</form>
			<form method="post" action="table.php" style="visibility:hidden">
				<button value="Submit" name="submit" id="submit2">Submit</button>
			</form>
		</div>
		<div id="list">
			<label class="h1" style="padding-left:4px;padding-right:4px;float:left;"> Projects </label>
			<div class="search_group">
				<input type="text" class="searchbox" style="border:none;" id="search_project" placeholder="Search..." />
        <label id="result" class="popBgColor bold"></label>
			</div>
			
      <div style="overflow-y:auto;border-bottom:solid 1px;border-top:solid 2px #cccccc;">
        <table id="activitylist" class="general_table width-100pc">
          <thead>
            <tr><th> Project Number </th><th> Project Name </th><th> Amount </th><th> Team </th><th> Remarks </th><th> Actions </th></tr>
          </thead>
          <tbody>
          <?php
          $sql = "SELECT 
                    project.ID,
                    project.Project_Number,
                    project.Project_Name, 
                    project.Amount,
                    project.Team_ID,
                    team.Team_Name
                  FROM project
                  LEFT JOIN team
                    ON project.Team_ID = team.ID";
          $result = mysqli_query($conn,$sql);
          if(mysqli_num_rows($result) > 0)
          {
            while($rows = mysqli_fetch_assoc($result))
            {
              echo "
              <tr>
              <form method='post' onsubmit='return confirm(`Are you sure you want to update?`);'>
              <td class='padding-0' style='display:none;'><input type='text' value='".$rows['ID']."' name='editproject_id'/></td>
              <td class='padding-0'><input type='text' readonly style='padding:1%;' value='".$rows['Project_Number']."' name='editproject_number'/></td>
              <td class='padding-0'><input type='text' value='".$rows['Project_Name']."' name='editprojname'/></td>
              <td class='padding-0'><input type='text' value='".$rows['Amount']."' name='editamount'/> </td>";
              //for team
                  echo "<td><select id='select_team_proj".$rows["ID"]."' name='editteam' class='margin-0'>";
                  $teamsql = "SELECT 
                                ID,
                                Team_Name
                              FROM team 
                              WHERE team.ID != '".$rows["Team_ID"]."'";
                  $team_result = mysqli_query($conn,$teamsql);
                  if(mysqli_num_rows($team_result) > 0)
                  {
                    echo "<option value='".$rows["Team_ID"]."'> ".$rows["Team_Name"]." </option>";
                    while($result_row = mysqli_fetch_assoc($team_result))
                    {
                      echo "<option value='".$result_row["ID"]."'> ".$result_row["Team_Name"]." </option>";																					
                    }
                  }
              echo"<td class='padding-0'>
              <select name='remarks' class='margin-0'>";
              $sql3 = "SELECT Remarks FROM project WHERE Project_Number = '".$rows['Project_Number']."'";
              $result3 = mysqli_query($conn,$sql3);
              if(mysqli_num_rows($result3) > 0)
              {
                while($rows3 = mysqli_fetch_assoc($result3))
                {
                  if($rows3['Remarks'] == "0")
                  {
                    echo "<option value=".$rows3['Remarks']."> RFI </option>
                    <option value='1'> Billable </option>";
                  }
                  else
                  {
                    echo "<option value=".$rows3['Remarks']."> Billable </option>
                    <option value='0'> RFI </option>";
                  }
                }
              }
              echo "</select>
              </td>
              <td style='width: 50px;'><input class='width-90pc' id='editbtn' type='submit' value='Edit' name='edit_project' /></td>
              </form>
              </tr>";
            }
          }
          ?>
          </tbody> 	
        </table>
			</div>
		</div>
		<div id="activity_edit_pop_bg" onclick="hidePop()" class="popUpBg bg-gray opacity-7">	
		</div>											 
		<div id="activity_edit_pop" class="popUp bg-gray width-50pc padding-4px">
			<fieldset class='height-95pc bg-white' style='border:solid 1px #34495e;overflow-y:auto'>
				<legend class='bg-white' style='border-radius:5px;color:#34495e'> Review </legend>
			</fieldset>
		</div>
		<div id="script">
		</div>
  </div>
  <script>

	$(document).ready(function(){
	
		hidePop();
		$('#searchbtn').on('click',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+document.getElementById('search_project').value,
				success:function(data)
				{   
					$('#activitylist').html(data);
				},
			});
		});
	});
	
	$(document).ready(function(){
		$('#search_project').on('input',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+document.getElementById('search_project').value+
						 '&for=projects',
				success:function(data)
				{   
					$('#activitylist').html(data);
				},
			});
		});
		$('#search_tickets').on('input',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+document.getElementById('search_tickets').value+
						 '&for=tickets',
				success:function(data)
				{   
					$('#tickets_list').html(data);
				},
			});
		});
		$('#search_users').on('input',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+document.getElementById('search_users').value+
						 '&for=users',
				success:function(data)
				{   
					$('#users_body').html(data);
				},
			});
		});
		$('#drop_status').on('change',function(){
			var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+value+
						 '&for=users_status',
				success:function(data)
				{   
					$('#users_body').html(data);
				},
			});
		});
		$('#drop_trade').on('change',function(){
			var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+value+
						 '&for=users_trade',
				success:function(data)
				{   
					$('#users_body').html(data);
				},
			});
		});
		$('#drop_team').on('change',function(){
			var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+value+
						 '&for=users_team',
				success:function(data)
				{   
					$('#users_body').html(data);
				},
			});
		});
		$('#drop_user_type').on('change',function(){
			var value = $(this).val();
			$.ajax(
			{
				url:'searchProject.php',
				type:'post',
				data:'query='+value+
						 '&for=users_user_type',
				success:function(data)
				{   
					$('#users_body').html(data);
				},
			});
		});
	});


	$(document).ready(function(){
	$('#projects').on('click',function(){
		location.reload();
	});
	});
	
	$(document).ready(function(){
	$('#ob').on('click',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'manageOB.php',
				type:'post',
				data:'request='+value,
				success:function(data)
				{
					$('#mainform').html(data);
          $('#btn_hamburger').trigger('click');
				},
			});
	});
	});
	
	$(document).ready(function(){
	$('#overtime').on('click',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'manageOT.php',
				type:'post',
				data:'request='+value,
				success:function(data)
        {
					$('#mainform').html(data);
          $('#btn_hamburger').trigger('click');
				},
			});
	});
	});
	
	$(document).ready(function(){
	$('#cal').on('click',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'calendar.php',
				type:'post',
				data:'request='+value,
				success:function(data)
				{   
          $('#btn_hamburger').trigger('click');
          $('#active_page').html("Calendar");
					$('#mainform').html(data);
				},
			});
	});
	});

	$(document).ready(function(){
	$('#users').on('click',function(){
		var value = $(this).val();
			$.ajax(
			{
				url:'users.php',
				type:'post',
				data:'request='+value,
				success:function(data)
				{   
          $('#btn_hamburger').trigger('click');
          $('#active_page').html("Users");
					$('#mainform').html(data);
				},
			});
	});
	});

	
	$(".searchbox").click(function (e) {
			$(".datalist").show();
			e.stopPropagation();
		});

		$(".datalist").click(function (e) {
			e.stopPropagation();
		});

		$(document).click(function () {
			$(".datalist").hide();
		});

		//Disable submit on enter key
		$(document).bind('keydown', function(e) {
			if (e.keyCode == 13) {
				e.preventDefault();
			}
		});

</script>
</body>
</html>