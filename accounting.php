<?php
  session_start();
  include("connect.php");
  include("phpGlobal.php");
  
  $shortcut_name = $_SESSION["Lastname"].", ".$_SESSION["Firstname"];
?>

<!DOCTYPE html>
<html>
  <head>
    <title> Activity Monitoring </title>
    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="icon" href="img/logoblue.png">
    
    <script src="jquery-3.2.1.min.js"></script>
    
    <style>  
      #list{
        height:90%;
        width:80%;
        min-height:300px;
        margin-right:0px;
        resize:none;
      }
      
      .miniheader{
      background-color:#eef1f2;
      margin:0px;
      position:fixed;
      left:0px;
      top:60px;
      width:100%;
      height:100px;
      border-bottom:solid 2px #ececec;
      }
      
      #th_endorced_client,#th_deposit,#th_invoice{
        display:none;
      }
      
      #th_or,#th_action{
        display:inline-block;
      }
    </style>
  </head>
  <body>
    <?php include('header.php'); ?>
    <div id="mainform">
      <div id="list" class="width-99pc">
        <div class="miniheader">
          <label id="shortcut_name"><?php echo $shortcut_name; ?></label>
          <ul class="tbl-tab-group">
            <li class="tbl-tab active-tbl-tab" id="requested_tab"> Collected <span id="collected_count">(0)</span> </li>
            <li class="tbl-tab" id="processing_tab"> EASTWEST <span id="eastwest_count">(0)</span> </li>
            <li class="tbl-tab" id="rejected_tab"> BDO <span id="bdo_count">(0)</span> </li>
            <li class="tbl-tab" id="paid_tab"> LANDBANK <span id="landbank_count">(0)</span> </li>
          </ul>
          <div id="percent_holder">
            <label id="total_amount"> &#8369; 0 </label>
          </div>
        </div>
        <div class="container2">
          <table class="general_table width-100pc">
            <thead>
              <tr>
                <th> Project </th>
                <th> % </th>
                <th> Amount </th>
                <th> Team </th>
                <th id="th_endorced_client"> Date Endorced to Client </th>
                <th id="th_endorced_acc"> Date Endorced to Accounting </th>
                <th id="th_deposit"> Deposit Date </th>
                <th id="th_invoice"> Invoice No. </th>
                <th id="th_acc"> Date Endorced to Accounting </th>
                <th id="th_or"> OR No. </th>
                <th id="th_paymenttype"> Payment Type </th>
                <th id="th_action"> Action </th>
              </tr>
            </thead>
            <tbody id="weeklySchedule">
             <!-- Data will be sent from pdRead.php -->
             <?php
              $readsql = "SELECT
                            bnc.ID,
                            project.Project_Number,
                            project.Project_Name,
                            external_deadline.External_Deadline,
                            external_deadline.Percent,
                            external_deadline.Phase,
                            project.Amount,
                            project.Team_ID,
                            bnc.Endorced_To_Client,
                            bnc.Endorced_To_Accounting,
                            bnc.Invoice_Number,
                            bnc.OR_Number,
                            bnc.Amount_Paid,
                            bnc.Payment_Type,
                            team.Team_Name
                          FROM bnc
                          INNER JOIN external_deadline
                            ON bnc.Externaldl_ID = external_deadline.ID
                          INNER JOIN project 
                            ON external_deadline.Project_ID = project.ID
                          INNER JOIN team 
                            ON team.ID = project.Team_ID
                          WHERE bnc.Status = 'Paid'
                            AND bnc.Bank = ''";
              $result = mysqli_query($conn,$readsql);
              if(mysqli_num_rows($result) > 0)
              {
                while($rows = mysqli_fetch_assoc($result)){
                  echo "<tr>
                          <td> ".$rows["Project_Number"]." - ".$rows["Project_Name"]." - ".$rows["Phase"]." </td>
                          <td> ".$rows["Percent"]." </td>
                          <td> &#8369; ".$rows["Amount_Paid"]." </td>
                          <td style='background-color:".$ex_dead_bg[$rows["Team_ID"]]."'> ".$rows["Team_Name"]." </td>
                          <td> ".$rows["Endorced_To_Accounting"]." </td>
                          <td> ".$rows["OR_Number"]." </td>
                          <td> ".$rows["Payment_Type"]." </td>
                          <td>
                            <select id='bank".$rows["ID"]."'>
                              <option value='EASTWEST'> EASTWEST </option>
                              <option value='BDO'> BDO </option>
                              <option value='LANDBANK'> LANDBANK </option>
                            </select>
                            <input type='button' class='btn-normal white bg-blue' value='Deposit' id='deposit".$rows["ID"]."' />
                            <script>
                              $('#deposit".$rows["ID"]."').on('click',function(){
                                  $.ajax({
                                    beforeSend:function(){
                                      return confirm('Deposit to ' + $('#bank".$rows["ID"]."').val() + ' ?');
                                    },
                                    url:'accountingSubmit.php',
                                    type:'post',
                                    data:'deposit=true'+
                                         '&bank='+ $('#bank".$rows["ID"]."').val()+
                                         '&bnc_id=".$rows["ID"]."',
                                    success:function(data){
                                      refresh();
                                    }
                                  });
                              });
                            </script>
                          </td>
                        </tr>";
                }
              }
             ?>
            </tbody>
          </table>			
        </div>
      </div>			
    </div>
    <script>  
      countAccounting();
      
      $('#requested_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#processing_tab').prop("class","tbl-tab");
      $('#paid_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
        $.ajax({
          url:'accountingRead.php',
          type:'post',
          data:'get_collected=true',
          success:function(data){
            $('#weeklySchedule').html(data);
            $('#total_amount').html("&#8369;"+countAmount());
          }
        });
      });
      
      $('#processing_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
      $('#paid_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
        $.ajax({
          url:'accountingRead.php',
          type:'post',
          data:'by_bank=true'+
                '&bank=EASTWEST',
          success:function(data){
            $('#weeklySchedule').html(data); 
            bankClicked();
          }
        });
      });
      
      $('#rejected_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#processing_tab').prop("class","tbl-tab");
      $('#paid_tab').prop("class","tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
        $.ajax({
          url:'accountingRead.php',
          type:'post',
          data:'by_bank=true'+
                '&bank=BDO',
          success:function(data){
            $('#weeklySchedule').html(data);
            bankClicked();
          }
        });
      });
      
      $('#paid_tab').on('click',function(){
      $(this).prop("class","tbl-tab active-tbl-tab");
      $('#processing_tab').prop("class","tbl-tab");
      $('#requested_tab').prop("class","tbl-tab");
      $('#rejected_tab').prop("class","tbl-tab");
        $.ajax({
          url:'accountingRead.php',
          type:'post',
          data:'by_bank=true'+
                '&bank=LANDBANK',
          success:function(data){
            $('#weeklySchedule').html(data);
            bankClicked();
          }
        });
      });
      
      function countAccounting(){
        //collected
        $.ajax({
        url:'accountingRead.php',
        type:'post',
        data:'collected_count=true',
        success:function(data){
          $('#collected_count').html('('+data+')');
        }
      });
      
      $.ajax({
        url:'accountingRead.php',
        type:'post',
        data:'deposit_count=true'+
                '&bank=EASTWEST',
        success:function(data){
          $('#eastwest_count').html('('+data+')');
        }
      });

      $.ajax({
        url:'accountingRead.php',
        type:'post',
        data:'deposit_count=true'+
                '&bank=BDO',
        success:function(data){
          $('#bdo_count').html('('+data+')');
        }
      });
      
      $.ajax({
        url:'accountingRead.php',
        type:'post',
        data:'deposit_count=true'+
                '&bank=LANDBANK',
        success:function(data){
          $('#landbank_count').html('('+data+')');
        }
      });
      }
      
      function refresh(){
        location.reload();
      }
      
      function bankClicked(){
        $('#th_deposit').fadeIn();
        $('#th_action').fadeOut();
        $('#total_amount').html("&#8369;"+countAmount());
      }
      
      
  function countAmount(){
    var count = 0;
    var total = 0;
    $("#weeklySchedule tr").each(function(){
      var amount =document.getElementById("weeklySchedule").rows[count].cells[2].innerHTML;
        amount = amount.trim();
        amount = amount.replace(/,/g,"");
        amount = parseFloat(amount.substring(1,amount.length));
        total = Number(total + amount);
        count++;
    });
    total = total.toLocaleString('en');
    return total;
  }
    </script>
  </body>
</html>