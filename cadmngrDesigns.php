<?php
	session_start();
	include("connect.php");	
	$today = date("F j, Y");	

?>
<style>
	#menu_item_logo1 {
		background-color: #f2f2f2;
	}

		#menu_item_logo1:hover {
			background-color: #f2f2f2;
		}

	#tab1 {
		color: #515151;
		font-weight: bold;
		text-shadow: none;
	}
</style>
<div id="info">
	<h2> Designs Ready for CAD </h2>
	<h2><?php echo $_SESSION['Name']; ?></h2>
	<h3><?php echo $today; ?></h3>	
</div>
<div id="list">
	<table  class="general_table width-100pc">
				<thead>							 
					<th> Designer </th>
					<th> Trade </th>
					<th> Project </th>
					<th> Activity </th>
					<th> Date Received </th>
					<th> CAD Technician </th>
					<th> Budget </th>
					<th> Deadline </th>
				</thead>	
				<tbody id="design_list">		
					<!-- data from getReviews.php -->
				</tbody>
			</table>
</div>
<div id="activity_edit_pop_bg" onclick="hidePop()" class="popUpBg bg-gray opacity-7">	
</div>											 
<div id="activity_edit_pop" class="popUp bg-gray width-50pc padding-4px">
	<fieldset class='height-95pc bg-white' style='border:solid 1px #34495e;overflow-y:auto'>
		<legend class='bg-white' style='border-radius:5px;color:#34495e'> Assign CAD Technician </legend>
		<table style="width:100%;">
			<thead>
				<tr>
					<th><h3 class="popBgColor margin-0"> Select CAD Technician: </h3></th>
					<th><h3 class="popBgColor margin-0"> Budget: </h3></th>
					<th><h3 class="popBgColor margin-0"> Deadline: </h3></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<input type="text" id="pop_cadtech_id" style="display:none;"/>
						<input type="text" id="pop_cadtech" class="searchbox " style="padding:4px;width:100%;min-width:200px;" placeholder = "Select CAD Technician..."/>
						<input type="text" id="trade_for_pop" />
						<div class="datalist" style="width:auto;min-width:200px;">
						<?php
							$sql = "SELECT ID,
														CONCAT(user.Firstname,' ',user.Middlename,' ',user.Lastname) AS Name
											FROM user
											WHERE User_Type = 9
														AND Status = 'Active'";
														
							$result = mysqli_query($conn,$sql);
							if(mysqli_num_rows($result) > 0)
							{
								while($rows = mysqli_fetch_assoc($result))
								{
									echo "<h4 onclick='selectCadTech".$rows["ID"]."();'> ".$rows["Name"]." </h4>";
									echo "<script>
													function selectCadTech".$rows["ID"]."()
													{
														$('#pop_cadtech').val('".$rows["Name"]."');	
														$('#pop_cadtech_id').val('".$rows["ID"]."');	
														$('.datalist').hide();
													}
												</script>";
								}
							}
						?>
						</div>
					</td>
					<td><input type="number" placeholder="Budget" style="padding:4px;width:80%;min-width:100px;margin:0px;" min="1" max="8"/> hrs</td>
					<td><input type="date" style="padding:4px;width:100%;min-width:100px;"/></td>
					<td><input type="submit" class="btn-normal bg-green white" value="Assign" /><input type="submit" class="btn-normal bg-red white" value="Cancel" /></td>
				</tr>
			</tbody>
		</table>
	</fieldset>
</div>

<script>

	hidePop();
	$(document).ready(function(){
		$.ajax({
				url:'getReviews.php',
				type:'post',
				data:'designer_id=1 OR 1 = 1',
				success:function(data){
					$('#design_list').html(data);
				},
			});
	});

	function hidePop()
	{											 
		$(".popUpBg").hide();
		$(".popUp").hide();
	}
	
	function showPop()
	{											 
		$(".popUpBg").show();
		$(".popUp").show();
	}		

	$(".searchbox").click(function (e) {
			$(".datalist").show();
			e.stopPropagation();
		});

		$(".datalist").click(function (e) {
			e.stopPropagation();
		});

		$(document).click(function () {
			$(".datalist").hide();
		});
</script>