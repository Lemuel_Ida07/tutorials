<?php
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "aco_webapp_db";
	
	$conn = mysqli_connect($servername,$username,$password,$dbname);
	
	if (!$conn) 
	{
		die("Connection failed: " . mysqli_connect_error());
	}
	$date = new DateTime('', new DateTimeZone('Asia/Manila'));
	$time_now =  $date->format('H:i');
  
  $month_array = ["index","January ","February ","March ","April ","May ","June ","July ","August ","September ","October ","November ","December "];
  